#include <stdio.h>

int main(void)
{
	int i_aav_number;
	int i_aav_number1;
	int i_aav_number2;
	int i_aav_number3;

	char c_aav_result1, c_aav_result2;
	int i_aav_result1, i_aav_result2;

	//Code 
	printf("\n\n");


	i_aav_number = 31;
	i_aav_number1 = 30;

	c_aav_result1 = (i_aav_number > i_aav_number1) ? 'A' : 'B';
	i_aav_result1 = (i_aav_number > i_aav_number1) ? i_aav_number : i_aav_number1;

	printf("Ternary Operator Answer : %c and %d.\n\n", c_aav_result1, i_aav_number);

	i_aav_number2 = 29;
	i_aav_number3 = 29;

	c_aav_result2 = (i_aav_number2 >= i_aav_number3) ? 'P' : 'Q';
	i_aav_result2 = (i_aav_number2 >= i_aav_number3) ? i_aav_number2 : i_aav_number3;

	printf("Ternary Operator Answer : %c and %d.\n\n", c_aav_result2, i_aav_result2);

	printf("\n\n");
	return(0);
}