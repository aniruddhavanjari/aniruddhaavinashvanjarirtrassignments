#include <stdio.h>
/* && , || , ! */
int main(void)
{
	int i_aav_number1;
	int i_aav_number2;
	int i_aav_number3;
	int i_aav_result;
	
	printf("Enter The Number 1:");
	scanf("%d", &i_aav_number1);

	printf("Enter The Number 2:");
	scanf("%d", &i_aav_number2);

	printf("Enter The Number 3:");
	scanf("%d", &i_aav_number3);

	printf("If The Result is 1 , then the Opeation is True \n If The Reult is 0 , the the Operation os 0");

	
	i_aav_result = (i_aav_number1 == i_aav_number2) && (i_aav_number2 >= i_aav_number3);/*eg: number1 = 10 ,number2 = 10, number3= 9*/
	printf("If Number1 Is Equal to Number2 is True Or False  And If Number2 Grater Then Number3 is Ture Or False , Result of AND Operation True Only WHen Both Conditon Are True\n\n");
	printf("If Number1 %d Is Equal to Number2 %d  And If Number2 %d Grater Then Equal Number3 %d , Result is %d\n\n", i_aav_number1, i_aav_number2,i_aav_number2 ,i_aav_number3,i_aav_result);

	i_aav_result = (i_aav_number2 == i_aav_number3) ||  (i_aav_number1 <= i_aav_number2);/*eg :number1 = 4 ,number2= 3, number3 = 3 */
	printf("If Number2 Is Equal to Number3 is True Or False  And If Number1 Less Then Number2 is Ture Or False ,Result of OR Operation False Only When Both Conditon Are False\n\n");
	printf("If Number2 %d Is Equal to Number3 %d  And If Number1 %d Grater Then Equal Number2 %d , Result is %d\n\n", i_aav_number2, i_aav_number3, i_aav_number1, i_aav_number2, i_aav_result);

	
	i_aav_result = !i_aav_number1;
	printf("Number1 :%d Logical Not(!) Operation Result : %d\n\n ",i_aav_number1, i_aav_result);

	i_aav_result = !i_aav_number2;
	printf("Number2 :%d Logical Not(!) Operation Result : %d\n\n ", i_aav_number2, i_aav_result);

	i_aav_result = !i_aav_number3;
	printf("Number3 :%d Logical Not(!) Operation Result : %d\n\n ", i_aav_number3, i_aav_result);
	
	i_aav_result = !((i_aav_number3 == i_aav_number2) && (i_aav_number3 == i_aav_number1));
	printf("Operation is !((number3 == number2) && (number3 == number1) : Result is %d\n", i_aav_result);

	i_aav_result = (!(i_aav_number3 <= i_aav_number2) || !(i_aav_number3 >= i_aav_number1));
	printf("Operation is !(number3 <= number2) || !(number3 >= number1) : Result is %d\n", i_aav_result);

	printf("\n\n");
}