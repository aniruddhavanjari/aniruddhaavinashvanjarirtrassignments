#include <stdio.h>

int main(void)
{
	void PrintBinaryFromOfNumber(unsigned int);

	unsigned int ui_aav_number1;
	unsigned int ui_aav_result;

	printf("Enter The Number1 :");
	scanf("%u", &ui_aav_number1);

	ui_aav_result = ~ui_aav_number1;
	printf("Number1 : %d  Compliment Result : %d \n", ui_aav_number1, ui_aav_result);

	PrintBinaryFromOfNumber(ui_aav_number1);
	PrintBinaryFromOfNumber(ui_aav_result);
	return(0);
}

void PrintBinaryFromOfNumber(unsigned int ui_aav_decimal_number)
{
	unsigned int ui_aav_quotient, ui_aav_remainder;
	unsigned int ui_aav_num;
	unsigned int ui_aav_binary_array[8];
	int i_aav_i;

	for (i_aav_i = 0; i_aav_i < 8; i_aav_i++)
		ui_aav_binary_array[i_aav_i] = 0;

	printf("The Binary Form Of The Decimal Integer %d Is\t=\t", ui_aav_decimal_number);
	ui_aav_num = ui_aav_decimal_number;
	i_aav_i = 7;
	while (ui_aav_num != 0)
	{
		ui_aav_quotient = ui_aav_num / 2;
		ui_aav_remainder = ui_aav_num % 2;
		ui_aav_binary_array[i_aav_i] = ui_aav_remainder;
		ui_aav_num = ui_aav_quotient;
		i_aav_i--;
	}

	for (i_aav_i = 0; i_aav_i < 8; i_aav_i++)
		printf("%u", ui_aav_binary_array[i_aav_i]);

	printf("\n\n");

}