#include <stdio.h>

int main(void)
{
	int i_aav_a;
	int i_aav_b;
	int i_aav_x;

	//Code
	printf("\n\n");
	printf("Enter  A Number : ");
	scanf("%d", &i_aav_a);

	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d", &i_aav_b);

	printf("\n\n");

	i_aav_x = i_aav_a;
	i_aav_a += i_aav_b;
	printf("Addition of Number1 = %d And Number2 = %d Gives %d.\n", i_aav_x, i_aav_b, i_aav_a);

	i_aav_x = i_aav_a;
	i_aav_a -= i_aav_b;
	printf("Subtraction of Number1 = %d And Number2 = %d Gives %d.\n", i_aav_x, i_aav_b, i_aav_a);

	i_aav_x = i_aav_a;
	i_aav_a *= i_aav_b;
	printf("Multiplication of Number1 = %d And Number2 = %d Gives %d.\n", i_aav_x, i_aav_b, i_aav_a);

	i_aav_x = i_aav_a;
	i_aav_a /= i_aav_b;
	printf("Division of Number1 = %d And Number2 = %d Gives Quotient %d. \n", i_aav_x, i_aav_b, i_aav_a);

	i_aav_x = i_aav_a;
	i_aav_a %= i_aav_b;
	printf("Division of Number1 = %d And Number2 = %d Gives Remainder %d. \n", i_aav_x, i_aav_b, i_aav_a);

	printf("\n\n");

	return(0);

}