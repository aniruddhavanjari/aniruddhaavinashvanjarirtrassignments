#include <stdio.h>

int main(void)
{
	//Variable
	int i_aav_number1 = 5;
	int i_aav_number2 = 10;

	//Code
	printf("\n\n");
	printf("Number1 = %d\n", i_aav_number1);
	printf("Number1 = %d\n", i_aav_number1++);
	printf("Number1 = %d\n", i_aav_number1);
	printf("Number1 = %d\n\n", ++i_aav_number1);

	printf("Number2 = %d\n", i_aav_number2);
	printf("Number2 = %d\n", i_aav_number2--);
	printf("Number2 = %d\n", i_aav_number2);
	printf("Number2 = %d\n\n", --i_aav_number2);
	
	return(0);
}

