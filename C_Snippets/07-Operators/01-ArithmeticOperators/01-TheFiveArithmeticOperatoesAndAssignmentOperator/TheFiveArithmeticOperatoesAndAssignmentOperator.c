#include <stdio.h>

int main(void)
{
	//Variable declarations
	int i_aav_a;
	int i_aav_b;
	int i_aav_result;

	//Code
	printf("\n\n");
	printf("Enter A Number :");
	scanf("%d", &i_aav_a);

	printf("\n\n");
	printf("Enter Another Number : ");
	scanf("%d", &i_aav_b);
	
	printf("\n\n");

	i_aav_result = i_aav_a + i_aav_b;
	printf("Addition of A = %d And B = %d Gives %d. \n", i_aav_a, i_aav_b, i_aav_result);

	i_aav_result = i_aav_a - i_aav_b; 
	printf("Subtraction of A = %d And B = %d Given %d.\n", i_aav_a, i_aav_b, i_aav_result);

	i_aav_result = i_aav_a * i_aav_b; 
	printf("Multiplication of A = %d And B = %d Gives %d.\n", i_aav_a, i_aav_b, i_aav_result);

	i_aav_result = i_aav_a / i_aav_b;
	printf("Division of A = %d And B = %d Gives Quotient %d.\n", i_aav_a, i_aav_b, i_aav_result);

	i_aav_result = i_aav_a % i_aav_b;
	printf("Division of A = %d And B = %d Give Remainder %d.\n", i_aav_a, i_aav_b, i_aav_result);

	printf("\n\n");

	return(0);

}