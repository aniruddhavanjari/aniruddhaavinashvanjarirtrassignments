#include<stdio.h>

#define MAX_NUMBER(i_aav_a, i_aav_b) ((i_aav_a>i_aav_b) ? i_aav_a : i_aav_b )

int main(int argc,char* argv[],char* envp[])
{

	int i_aav_number1;
	int i_aav_number2;
	int i_aav_result;
	
	float f_aav_number1;
	float f_aav_number2;
	float f_aav_result;
	
	printf("\n");
	printf("Enter the Number1 : \n");
	scanf("%d",&i_aav_number1);
	
	printf("\n");
	printf("Enter the Number2 : \n");
	scanf("%d",&i_aav_number2);
	
	i_aav_result = MAX_NUMBER(i_aav_number1,i_aav_number2);
	printf("Result of Macro function is %d\n",i_aav_result);
	
	printf("\n");
	
	printf("Enter the Flaoting Number1 : \n");
	scanf("%f",&f_aav_number1);
	
	printf("\n");
	printf("Enter the Flaoting Number2 : \n");
	scanf("%f",&f_aav_number2);
	
	f_aav_result = MAX_NUMBER(f_aav_number1,f_aav_number2);
	printf("Result of Macro function %f\n",f_aav_result);
	
	printf("\n");
	
	return(0);
}