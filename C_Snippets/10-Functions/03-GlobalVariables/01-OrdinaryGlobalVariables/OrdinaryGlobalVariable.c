#include<stdio.h>

int i_aav_global_count = 0;

int main(int argc,char* argv[],char* envp[])
{

	void change_count_one(void);
	void change_count_two(void);
	void change_count_three(void);
	
	printf("\n");
	
	printf("main() : Value of global_count = %d\n",i_aav_global_count);
	
	change_count_one();
	change_count_two();
	change_count_three();
	
	printf("\n");
	
	return 0;
}

void change_count_one(void)
{

	i_aav_global_count = 300;
	printf("change_count_one() Value of global_count = %d\n",i_aav_global_count);

}

void change_count_two(void)
{

	i_aav_global_count += 24;
	printf("change_count_two() Value of global_count = %d\n",i_aav_global_count);

}

void change_count_three(void)
{

	i_aav_global_count += 8;
	printf("change_count_three() Value of global_count = %d\n",i_aav_global_count);

}

