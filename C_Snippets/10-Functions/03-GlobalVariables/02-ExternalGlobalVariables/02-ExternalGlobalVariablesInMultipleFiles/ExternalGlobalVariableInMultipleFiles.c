#include<stdio.h>

int i_aav_global_count = 0;

int main(void)
{

	void change_count(void);
	void change_count_one(void);
	void change_count_two(void);
	
	printf("\n");
	
	change_count();
	change_count_one();
	change_count_two();

	return(0);
}

void change_count(void)
{

	i_aav_global_count = i_aav_global_count + 4;
	printf("Global count = %d\n",i_aav_global_count);

}