#include<stdio.h>

int main(void)
{
	
	void change_count(void);
	
	extern int i_aav_global_count;
	
	printf("\n");
	printf("Value of i_aav_global_count before change_count() %d\n",i_aav_global_count);

	change_count();

	printf("Value of i_aav_global_count after change_count() %d\n",i_aav_global_count);

	return(0);
}

int i_aav_global_count = 1;

void change_count(void)
{
	
		i_aav_global_count = 24;
		
		printf("Value of global_count in change_count() %d\n",i_aav_global_count);
	
}
