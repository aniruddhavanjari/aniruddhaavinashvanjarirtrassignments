#include<stdio.h>

int main(int argc,char* argv[],char* envp[])
{

	void MyAddition(void);
	int MySubtraction(void);
	void MyMultiplication(int,int);
	int MyDivision(int);
	
	int i_aav_subResult,i_aav_DivResult;
	int i_aav_number1,i_aav_number2;
	
	MyAddition();
	
	i_aav_subResult = MySubtraction();
	
	printf("subtraction of Number1 and Number2 : %d", i_aav_subResult);
	
	
	printf("\n");
	printf("Enter number1 : ");
	scanf("%d",&i_aav_number1);
	
	printf("\n");
	printf("Enter number2 : ");
	scanf("%d",&i_aav_number2);
	
	MyMultiplication(i_aav_number1,i_aav_number2);
	
	printf("\n");
	printf("Enter number1 : ");
	scanf("%d",&i_aav_number1);
	
	printf("\n");
	printf("Enter number2 : ");
	scanf("%d",&i_aav_number2);
	
	i_aav_DivResult = MyDivision(i_aav_number1, i_aav_number2);
	
	printf("Division of Number1 and Number2 : %d", i_aav_DivResult);
	
	return 0;

}

void MyAddition(void)
{
	
	int i_aav_number1,i_aav_number2,i_aav_sum;
	
	printf("\n");
	printf("Enter number1 : ");
	scanf("%d",&i_aav_number1);
	
	printf("\n");
	printf("Enter number2 : ");
	scanf("%d",&i_aav_number2);
	
	i_aav_sum = i_aav_number1 + i_aav_number2;
	
	printf("\n");
	
	printf("Sum of Number1 and Number2 is : %d:",i_aav_sum);
}

int MySubtraction(void)
{
	
	int i_aav_number1,i_aav_number2,i_aav_sub;
	
	printf("\n");
	printf("Enter number1 : ");
	scanf("%d",&i_aav_number1);
	
	printf("\n");
	printf("Enter number2 : ");
	scanf("%d",&i_aav_number2);
	
	if (i_aav_number1 >= i_aav_number2)
	{
		i_aav_sub = i_aav_number1 - i_aav_number2;
	}
	else
	{
		i_aav_sub = i_aav_number2 - i_aav_number1;
	}

	return i_aav_sub;
}

void MyMultiplication(int i_aav_number1,int i_aav_number2)
{
	
	int i_aav_mul;
	
	i_aav_mul = i_aav_number1 * i_aav_number2;
	
	printf("\n");
	
	printf("Multiplication of Number1 and Number2 is : %d:",i_aav_mul);
}

int MyDivision(int i_aav_number1, int i_aav_number2)
{
	
	int i_aav_div;
	
	if(i_aav_number1 > i_aav_number2)
	{
		i_aav_div = i_aav_number1 / i_aav_number2;
	}
	else
	{
		i_aav_div = i_aav_number2 / i_aav_number1;
	}

	return i_aav_div;
}


