#include<stdio.h>

int main(int argc, char* argv[], char* envp[])
{
	//Code
	void MyAddition( int, int);
	
	int i_aav_number1, i_aav_number2;
	
	printf("\n");
	printf("Enter number1: ");
	scanf("%d", &i_aav_number1);
	
	printf("\n");
	printf("Enter number2: ");
	scanf("%d",  &i_aav_number2);
	
	MyAddition(i_aav_number1, i_aav_number2);

	return 0;

}

void MyAddition(int i_aav_number1, int i_aav_number2)
{
	
	int i_aav_sum;
	
	i_aav_sum = i_aav_number1 + i_aav_number2;
	
	printf("\n");
	
	printf("Sum of Number1 and Number2 is : %d",i_aav_sum);

}