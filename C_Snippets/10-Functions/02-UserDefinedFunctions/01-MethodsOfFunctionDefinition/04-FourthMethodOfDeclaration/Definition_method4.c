#include<stdio.h>

int main(int argc, char* argv[], char* envp[])
{
	//Code
	int MyAddition(int, int);
	
	int i_aav_number1, i_aav_number2, i_aav_result;
	
	printf("\n");
	printf("Enter number1: ");
	scanf("%d",&i_aav_number1);
	
	printf("\n");
	printf("Enter number2: ");
	scanf("%d",&i_aav_number2);
	
	i_aav_result = MyAddition(i_aav_number1, i_aav_number2);
	
	printf("\nSum of Number1 and Number : %d",i_aav_result);
	
	return 0;

}

int MyAddition(int i_aav_number1, int i_aav_number2)
{
	
	int i_aav_sum;
	
	i_aav_sum = i_aav_number1 + i_aav_number2;
	
	return i_aav_sum;

}