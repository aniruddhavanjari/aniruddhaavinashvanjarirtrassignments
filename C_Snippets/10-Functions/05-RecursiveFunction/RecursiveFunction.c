#include<stdio.h>

int main(int argc,char* argv[],char* envp[])
{
	
	unsigned int ui_aav_number1;
	
	void recursive(unsigned int);
	
	printf("\n");
	printf("Enter a number : \n");
	scanf("%u",&ui_aav_number1);
	
	printf("Output of recursive function : \n");
	
	recursive(ui_aav_number1);
	
	printf("\n\n");

	return 0;
}

void recursive(unsigned int ui_aav_number)
{
	
	printf("ui_aav_number = %d\n",ui_aav_number);
	
	if(ui_aav_number > 0)
	{
		recursive(ui_aav_number-1);
	}
	
}