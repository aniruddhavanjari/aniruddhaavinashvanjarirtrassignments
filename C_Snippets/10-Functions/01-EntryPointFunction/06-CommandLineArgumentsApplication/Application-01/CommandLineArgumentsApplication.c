#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>

int main(int argc,char *argv[],char* envp[])
{
	//Code
	int i_aav_number;
	int i_aav_number1;
	int i_aav_sum = 0;
	
	if(argc == 1)
	{
		printf("\n");
		printf("No Number Given For Addition ! Exiting Now \n");	
		printf("Usage : CommandLineArgumentsApplication <first number> <second number>...\n");
		exit(0);
	}
	
	printf("\n");
	printf("Sum of all Integers :\n");
	for(i_aav_number = 0; i_aav_number < argc; i_aav_number++)
	{
		i_aav_number1 =  atoi(argv[i_aav_number]);
		i_aav_sum = i_aav_sum + i_aav_number1;
	}
	
	printf("Sum = %d\n",i_aav_sum);

	return 0;
}
