#include<stdio.h>

int main(int argc,char* argv[],char* envp[])
{

	int i_aav_number;
	
	printf("\n");
	printf("Hello World \n");
	printf("Number of command line arguments = %d\n",argc);
	
	printf("Command line arguments passed to the program are :\n");

	for(i_aav_number = 0;i_aav_number<argc;  i_aav_number++)
	{
		printf("Command line arguments number %d = %s\n", (i_aav_number+1), argv[i_aav_number]);
	}
	printf("\n");
	
	printf("First 50 Environmental Variable Passed To This Program Are :\n");
	for(i_aav_number = 0; i_aav_number < 50; i_aav_number++)
	{
		printf("Environmental Variable Number %d = %s\n", (i_aav_number+1), envp[i_aav_number]);
	}		

	printf("\n");
	
	return(0);
}