#include<stdio.h>

int main(int argc,char* argv[])
{
	int i_aav_number;
	
	printf("\n");
	printf("Hello World \n");
	printf("Number of command line arguments = %d \n",argc);
	
	printf("Command line arguments passed to the program are : \n");

	for(i_aav_number = 0; i_aav_number < argc; i_aav_number++)
	{
		printf("Command line arguments number %d = %s\n", (i_aav_number+1), argv[i_aav_number]);
	}
	printf("\n\n");
	
	return(0);
}