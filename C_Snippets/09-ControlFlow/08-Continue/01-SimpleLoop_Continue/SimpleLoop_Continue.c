#include <stdio.h>
int main(void)
{
	int i_aav_number;

	//Code 
	printf("Printing Even Number Range  form 0 to 210 \n");

	for (i_aav_number = 0; i_aav_number <= 210; i_aav_number++)
	{
		if (i_aav_number % 2 != 0)
		{
			continue;
		}
		else
		{
			printf("\t%d\n", i_aav_number);
		}
	}
	printf("\n\n");

	return(0);
}