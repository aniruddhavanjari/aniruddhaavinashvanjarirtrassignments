#include <stdio.h>

int main(void)
{
	//Variable Declaration
	char c_aav_option, c_aav_ch = '\0';

	//Code
	printf("To Stop The Infinite Loop , Enter 'Q' or 'q' to Quit\n");
	printf("Enter 'Y' or 'y' To Initiate user Controlled Infinite Loop:");
	printf("\n\n");
	c_aav_option = getch();
	if (c_aav_option == 'Y' || c_aav_option == 'y')
	{
		for (;;)
		{
			printf("In Loop...\n");
			c_aav_ch = getch();
			if (c_aav_ch == 'Q' || c_aav_ch == 'q')
				break;
		}
	}
	printf("\n\n");
	printf("Exiting User Controlled Loop\n");

	return(0);
}