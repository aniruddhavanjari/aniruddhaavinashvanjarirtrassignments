#include <stdio.h>

int main(void)
{
	int i_aav_i, i_aav_j, i_aav_c;

	//Code
	printf("\n\n");
	for (i_aav_i = 0; i_aav_i < 64; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < 64; i_aav_j++)
		{
			i_aav_c = ((i_aav_i & 0x8) == 0) ^ ((i_aav_j & 0x8) == 0);

			if (i_aav_c == 0)
				printf(" ");

			if (i_aav_c == 1)
				printf("* ");
		}
		printf("\n\n");
	}
	return(0);
}