#include <stdio.h>
int main(void)
{
	int i_aav_i, i_aav_j;

	printf("\n\n");

	for (i_aav_i = 10; i_aav_i <= 20; i_aav_i++)
	{
		printf("i = %d\n", i_aav_i);
		printf("------\n\n");

		for (i_aav_j = 1; i_aav_j <= 5; i_aav_j++)
		{
			printf("\tj = %d\n", i_aav_j);
		}

		printf("\n\n");
	}
	return(0);
}