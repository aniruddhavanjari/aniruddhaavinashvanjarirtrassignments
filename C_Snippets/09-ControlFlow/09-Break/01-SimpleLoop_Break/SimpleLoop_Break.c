#include <stdio.h>
#include <conio.h>

int main(void)
{
	int i_aav_number;
	char c_aav_chrater;

	printf("Printing the Number From 1 to 1000 Every User Input .Exitting the Loop when user press 'Q' or 'q' \n");
	printf("Enter 'Q' or 'q' to Break the Loop\n");

	for (i_aav_number = 1; i_aav_number <= 1000; i_aav_number++)
	{

		printf("\t%d\n", i_aav_number);
		c_aav_chrater = getch();
		if (c_aav_chrater == 'Q' || c_aav_chrater == 'q')
		{
			break;
		}
	}

	printf(" EXIT... LOOP\n");
	printf("\n\n");

	return(0);

}