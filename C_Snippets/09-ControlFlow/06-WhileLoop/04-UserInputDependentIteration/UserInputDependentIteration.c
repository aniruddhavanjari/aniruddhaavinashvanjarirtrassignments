#include <stdio.h>

int main(void)
{
	int i_aav_i, i_aav_number, i_aav_number1;

	printf("\n\n");

	printf("Enter the Value For the start :");
	scanf("%d", &i_aav_number);
	printf("Enter the Value For the End :");
	scanf("%d", &i_aav_number1);

	printf("Printing Digits of %d To %d :\n\n", i_aav_number, (i_aav_number + i_aav_number1));

	i_aav_i = i_aav_number;
	while (i_aav_i <= (i_aav_number + i_aav_number1))
	{
		printf("\t %d\n",  i_aav_i);
		i_aav_i++;
	}
	printf("\n\n");

	return(0);
}