#include <stdio.h>
int main(void)
{
	int i_aav_i, i_aav_j, i_aav_k;

	printf("\n\n");
	i_aav_i = 10;
	while (i_aav_i <= 20)
	{
		printf("i = %d\n", i_aav_i);
		printf("------\n\n");
		i_aav_j = 1;
		while ( i_aav_j <= 5)
		{
			printf("\tj = %d\n", i_aav_j);
			printf("------\n\n");
			i_aav_k = 1;
			while (i_aav_k <= 3)
			{
				printf("\t\tk = %d\n", i_aav_k);
				i_aav_k++;
			}
			printf("\n\n");
			i_aav_j++;
		}

		printf("\n\n");
		i_aav_i++;
	}
	return(0);
}