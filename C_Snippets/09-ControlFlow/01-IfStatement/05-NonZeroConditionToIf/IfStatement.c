#include <stdio.h>

int main(void)
{
	 
	int i_aav_number;

	printf("\n\n");

	i_aav_number = 40;
	
	if (i_aav_number)
	{
		printf("Value = %d !!!\n\n", i_aav_number);
	}

	i_aav_number = -24; 
	if (i_aav_number)
	{
		printf("Value = %d !!!\n\n", i_aav_number);
	}

	i_aav_number = 0;
	if (i_aav_number)
	{
		printf("Value of %d!!!\n", i_aav_number);
	}

	printf("All Three if-statemensts Are Done !!!\n\n");

	return(0);
}
