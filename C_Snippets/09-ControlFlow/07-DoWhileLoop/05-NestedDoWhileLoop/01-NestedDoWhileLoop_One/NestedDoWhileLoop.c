#include <stdio.h>
int main(void)
{
	int i_aav_i, i_aav_j;

	printf("\n\n");
	i_aav_i = 10;
	do
	{
		printf("i = %d\n", i_aav_i);
		printf("------\n\n");

		i_aav_j = 1;
		do
		{
			printf("\tj = %d\n", i_aav_j);
			i_aav_j++;
		} while (i_aav_j <= 5);
		i_aav_i++;
		printf("\n\n");
	} while (i_aav_i <= 20);
	return(0);
}