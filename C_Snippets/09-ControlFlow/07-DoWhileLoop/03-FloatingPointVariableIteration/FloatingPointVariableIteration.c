#include <stdio.h>

int main(void)
{
	float f_aav_f;
	float f_aav_number = 1.8f;

	//Code
	printf("Printfing Numbers %f to %f:\n\n", f_aav_number, (f_aav_number * 10.0f));

	f_aav_f = f_aav_number;
	do
	{
		printf("\t %f\n", f_aav_f);
		f_aav_f = f_aav_f + f_aav_number;
	}while (f_aav_f <= (f_aav_number * 10.0f));

	printf("\n\n");

	return(0);
}