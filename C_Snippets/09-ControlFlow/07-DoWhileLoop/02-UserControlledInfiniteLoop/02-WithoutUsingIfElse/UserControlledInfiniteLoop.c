#include <stdio.h>

int main(void)
{
	//Variable Declaration
	char c_aav_option, c_aav_ch = '\0';

	//Code
	printf("To Stop The Infinite Loop , Enter 'Q' or 'q' to Quit\n");
	
	printf("\n\n");
	
	do
	{
		do
		{
			printf("In Loop...\n");
			c_aav_ch = getch();
		} while (c_aav_ch != 'Q' && c_aav_ch != 'q');
		printf("\n\n");
		printf("Exiting User Controlled Loop\n");
		printf("Enter 'Y' or 'y' To Initiate user Controlled Infinite Loop:");
		c_aav_option = getch();
	} while (c_aav_option == 'Y' || c_aav_option == 'y');
	return(0);
}

