#include <stdio.h>

int main(void)
{
	// Variable Declaration 
	int i_aav_i = 24;
	float f_aav_f = 3.9f;
	double d_aav_d = 8.041997;
	char c_aav_c = 'A';

	//Code 
	printf("\n\n");

	printf("i_aav_i = %d\n", i_aav_i);
	printf("f_aav_f = %f\n", f_aav_f);
	printf("d_aav_d = %lf\n", d_aav_d);
	printf("c_aav_c = %c\n", c_aav_c);

	printf("\n\n");

	i_aav_i = 240;
	f_aav_f = 6.54f;
	d_aav_d = 24.2424;
	c_aav_c = 'S';

	printf("i_aav_i = %d\n", i_aav_i);
	printf("f_aav_f = %f\n", f_aav_f);
	printf("d_aav_d = %lf\n", d_aav_d);
	printf("c_aav_c = %c\n", c_aav_c);

	printf("\n\n");

	return(0);
}


