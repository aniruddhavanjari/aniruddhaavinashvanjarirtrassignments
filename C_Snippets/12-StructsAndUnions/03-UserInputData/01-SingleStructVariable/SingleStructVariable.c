#include <stdio.h>
#include <conio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char ch;
};

int main(void)
{
	struct MyData struct_aav_data;

	printf("\n\n");
	printf("Enter Interger Value For Data Menber i : \n");
	scanf("%d", &struct_aav_data.i);

	printf("Enter flaot Value For Data Menber f : \n");
	scanf("%f", &struct_aav_data.f);

	printf("Enter Double Value For Data Menber d : \n");
	scanf("%lf", &struct_aav_data.d);

	printf("Enter Charater Value For Data Menber ch : \n");
	struct_aav_data.ch = getch();

	printf("\n\n");
	printf("Data Member of MyData:\n\n");
	printf("i = %d\n", struct_aav_data.i);
	printf("f = %f\n", struct_aav_data.f);
	printf("d = %lf\n", struct_aav_data.d);
	printf("c = %c\n", struct_aav_data.ch);
	
	printf("\n\n");

	return(0);

}