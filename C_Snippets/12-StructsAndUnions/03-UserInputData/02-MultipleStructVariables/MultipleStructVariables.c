#include <stdio.h>

struct MyPoint
{
	int x;
	int y;
};

int main(void)
{
	struct MyPoint struct_aav_pointA, struct_aav_pointB, struct_aav_pointC, struct_aav_pointD, struct_aav_pointE;

	//Code
	printf("\n\n");
	printf("Enter X-Coordinate For Point 'A' :");
	scanf("%d", &struct_aav_pointA.x);
	printf("Enter Y-Coordinate For Point 'A' :");
	scanf("%d", &struct_aav_pointA.y);

	printf("\n\n");
	printf("Enter X-Coordinate For Point 'B' :");
	scanf("%d", &struct_aav_pointB.x);
	printf("Enter Y-Coordinate For Point 'B' :");
	scanf("%d", &struct_aav_pointB.y);

	printf("\n\n");
	printf("Enter X-Coordinate For Point 'C' :");
	scanf("%d", &struct_aav_pointC.x);
	printf("Enter Y-Coordinate For Point 'C' :");
	scanf("%d", &struct_aav_pointC.y);

	printf("\n\n");
	printf("Enter X-Coordinate For Point 'D' :");
	scanf("%d", &struct_aav_pointD.x);
	printf("Enter Y-Coordinate For Point 'D' :");
	scanf("%d", &struct_aav_pointD.y);

	printf("\n\n");
	printf("Enter X-Coordinate For Point 'E' :");
	scanf("%d", &struct_aav_pointE.x);
	printf("Enter Y-Coordinate For Point 'E' :");
	scanf("%d", &struct_aav_pointE.y);

	printf("Co-ordianate (x,y) of Point 'A' Are :(%d,%d)\n", struct_aav_pointA.x, struct_aav_pointA.y);
	printf("Co-ordianate (x,y) of Point 'B' Are :(%d,%d)\n", struct_aav_pointB.x, struct_aav_pointB.y);
	printf("Co-ordianate (x,y) of Point 'C' Are :(%d,%d)\n", struct_aav_pointC.x, struct_aav_pointC.y);
	printf("Co-ordianate (x,y) of Point 'D' Are :(%d,%d)\n", struct_aav_pointD.x, struct_aav_pointD.y);
	printf("Co-ordianate (x,y) of Point 'E' Are :(%d,%d)\n", struct_aav_pointE.x, struct_aav_pointE.y);

	return(0);
}