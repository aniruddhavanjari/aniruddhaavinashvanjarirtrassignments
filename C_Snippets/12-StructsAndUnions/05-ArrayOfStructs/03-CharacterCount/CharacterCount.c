#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define MAX_STRING_LENGTH 1024

struct ChracterCount
{
	char ch;
	int ch_count;
}character_and_count[] = { {'A',0},
							{'B',0},
							{'C',0},
							{'D',0},
							{'E',0},
							{'F',0},
							{'G',0},
							{'H',0},
							{'I',0},
							{'J',0},
							{'K',0},
							{'L',0},
							{'M',0},
							{'N',0},
							{'O',0},
							{'P',0},
							{'Q',0},
							{'R',0},
							{'S',0},
							{'T',0},
							{'U',0},
							{'V',0},
							{'W',0},
							{'X',0},
							{'Y',0},
							{'Z',0} };

#define SIZE_OF_ENTRIE_ARRAY_OF_STRUCTS sizeof(character_and_count)
#define SIZE_OF_ONE_STRUCT_FROME_THE_ARRAY_OF_STRUCT sizeof(character_and_count[0])

#define NUM_ELEMENTS_IN_ARRAY (SIZE_OF_ENTRIE_ARRAY_OF_STRUCTS/SIZE_OF_ONE_STRUCT_FROME_THE_ARRAY_OF_STRUCT)

int main(void)
{
	char str[MAX_STRING_LENGTH];
	int i_aav_i, i_aav_j, i_aav_actual_string_length = 0;

	//Code
	printf("\n");
	printf("Enter A String :\n");
	gets_s(str, MAX_STRING_LENGTH);

	i_aav_actual_string_length = strlen(str);

	printf("\n");
	printf("The String You Have Entered Is : \n");
	printf("%s\n", str);

	for (i_aav_i = 0; i_aav_i < i_aav_actual_string_length; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < NUM_ELEMENTS_IN_ARRAY; i_aav_j++)
		{
			str[i_aav_i] = toupper(str[i_aav_i]);

			if (str[i_aav_i] == character_and_count[i_aav_j].ch)
				character_and_count[i_aav_j].ch_count++;
		}
	}

	printf("\n");
	printf("The Number of Occuranve of All Charcters From the Alphabet Are As Follows :\n");
	for (i_aav_i = 0; i_aav_i < NUM_ELEMENTS_IN_ARRAY; i_aav_i++)
	{
		printf("Character %c = %d\n", character_and_count[i_aav_i].ch, character_and_count[i_aav_i].ch_count);
	}
	printf("\n\n");

	return(0);
}