#include <stdio.h>
#include <ctype.h>

#define NUM_EMPLOYEES 5

#define NAME_LENGTH 100
#define MARITIAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	char sex;
	float salary;
	char maritail_status;
};

int main(void)
{
	void MyGetString(char[], int);

	struct Employee EmployeeRecord[NUM_EMPLOYEES];

	int i_aav_i;

	for (i_aav_i = 0; i_aav_i < NUM_EMPLOYEES; i_aav_i++)
	{
		printf("\n\n");
		printf("Data Entry For Employee Number %d", (i_aav_i + 1));

		//Name
		printf("\n\n");
		printf("Entry Employee Name:");
		MyGetString(EmployeeRecord[i_aav_i].name, NAME_LENGTH);

		//Age
		printf("\n\n");
		printf("Entry Employee Age:");
		scanf("%d", &EmployeeRecord[i_aav_i].age);

		//Male And Female
		printf("\n\n");
		printf("Enter Employee's Sex for Male And Female");
		EmployeeRecord[i_aav_i].sex = getch();
		printf("%c", EmployeeRecord[i_aav_i].sex);
		EmployeeRecord[i_aav_i].sex = toupper(EmployeeRecord[i_aav_i].sex);

		//Salary
		printf("\n\n");
		printf("Enter Employee's Salary in BHARAT Paise : ");
		scanf("%f", &EmployeeRecord[i_aav_i].salary);

		//Married Status
		printf("\n\n");
		printf("Is the Employee Married  Y/y as Yes ,N/n as No :");
		EmployeeRecord[i_aav_i].maritail_status = getch();
		printf("%c", EmployeeRecord[i_aav_i].maritail_status);
		EmployeeRecord[i_aav_i].maritail_status = toupper(EmployeeRecord[i_aav_i].maritail_status);

	}
		
	printf("\n\n");
	
	printf("Display Employee Record\n");
	for (i_aav_i = 0; i_aav_i < 5; i_aav_i++)
	{
		printf("****Employee Number %d********\n", (i_aav_i + 1));
		printf("Name			:%s\n", EmployeeRecord[i_aav_i].name);
		printf("Age			:%d Years\n", EmployeeRecord[i_aav_i].age);
		if (EmployeeRecord[i_aav_i].sex == 'M' || EmployeeRecord[i_aav_i].sex == 'm')
			printf("Sex			:Male\n");
		else
			printf("Sex			:Female\n");

		printf("Salary			:Rs %f \n", EmployeeRecord[i_aav_i].salary);
		printf("Maritial Status		:%c\n", EmployeeRecord[i_aav_i].maritail_status);

	}
	return(0);
}

void MyGetString(char str[], int str_size)
{
	int i_aav_i;
	char c_aav_ch = '\0';

	//Code
	i_aav_i = 0;
	do
	{
		c_aav_ch = getch();
		str[i_aav_i] = c_aav_ch;
		printf("%c", str[i_aav_i]);
		i_aav_i++;
	} while ((c_aav_ch != '\r') && (i_aav_i < str_size));

	if (i_aav_i == str_size)
		str[i_aav_i - 1] = '\0';
	else
		str[i_aav_i] = '\0';

}