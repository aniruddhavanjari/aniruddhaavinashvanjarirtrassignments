#include <stdio.h>

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	float salary;
	char sex;
	char maritial_status[MARITAL_STATUS];
};

int main(void)
{
	struct Employee EmployeeRecord[5];

	char employee_Rama[] = "Rama";
	char employee_raj[] = "raj";
	char employee_rajeshvari[] = "Rajeshvari";
	char employee_raju[] = "Raju";
	char employee_Ranu[] = "Renu";
	
	int i_aav_i;

	//Code

	//Employee One
	strcpy(EmployeeRecord[0].name, employee_Rama);
	EmployeeRecord[0].age = 35;
	EmployeeRecord[0].sex = 'F';
	EmployeeRecord[0].salary = 60000.0f;
	strcpy(EmployeeRecord[0].maritial_status, "Unmarried");

	//Employee Two
	strcpy(EmployeeRecord[1].name, employee_raj);
	EmployeeRecord[1].age = 25;
	EmployeeRecord[1].sex = 'M';
	EmployeeRecord[1].salary = 90000.0f;
	strcpy(EmployeeRecord[1].maritial_status, "Unmarried");

	//Employee Three
	strcpy(EmployeeRecord[2].name, employee_rajeshvari);
	EmployeeRecord[2].age = 40;
	EmployeeRecord[2].sex = 'F';
	EmployeeRecord[2].salary = 61000.0f;
	strcpy(EmployeeRecord[2].maritial_status, "Married");

	//Employee Four
	strcpy(EmployeeRecord[3].name, employee_raju);
	EmployeeRecord[3].age = 34;
	EmployeeRecord[3].sex = 'M';
	EmployeeRecord[3].salary = 40900.6f;
	strcpy(EmployeeRecord[3].maritial_status, "Unmarried");

	//Employee Five
	strcpy(EmployeeRecord[4].name, employee_Ranu);
	EmployeeRecord[4].age = 35;
	EmployeeRecord[4].sex = 'F';
	EmployeeRecord[4].salary = 59704.7f;
	strcpy(EmployeeRecord[4].maritial_status, "Married");

	printf("\n\n");
	printf("Display Employee Record\n");
	for (i_aav_i = 0; i_aav_i < 5; i_aav_i++)
	{
		printf("****Employee Number %d********\n", (i_aav_i + 1));
		printf("Name			:%s\n", EmployeeRecord[i_aav_i].name);
		printf("Age			:%d Years\n", EmployeeRecord[i_aav_i].age);
		if (EmployeeRecord[i_aav_i].sex == 'M' || EmployeeRecord[i_aav_i].sex == 'm')
			printf("Sex			:Male\n");
		else
			printf("Sex			:Female\n");

		printf("Salary			:Rs %f \n", EmployeeRecord[i_aav_i].salary);
		printf("Maritial Status		:%s\n", EmployeeRecord[i_aav_i].maritial_status);

	}
	return(0);
}