#include <stdio.h>


int main(void)
{
	
	struct MyPoint
	{
		int x;
		int y;
	}struct_aav_point_A, struct_aav_point_B, struct_aav_point_C, struct_aav_point_D, struct_aav_point_E;


	struct_aav_point_A.x = 50;
	struct_aav_point_A.y = 60;

	struct_aav_point_B.x = 100;
	struct_aav_point_B.y = 200;

	struct_aav_point_C.x = 31;
	struct_aav_point_C.y = 42;

	struct_aav_point_D.x = 59;
	struct_aav_point_D.y = 66;

	struct_aav_point_E.x = 76;
	struct_aav_point_E.y = 83;

	printf("\n\n");
	printf("Cordiantes (x,y) Point A :(%d,%d)\n\n", struct_aav_point_A.x, struct_aav_point_A.y);

	printf("Cordiantes (x,y) Point B :(%d,%d)\n\n", struct_aav_point_B.x, struct_aav_point_B.y);

	printf("Cordiantes (x,y) Point C :(%d,%d)\n\n", struct_aav_point_C.x, struct_aav_point_C.y);

	printf("Cordiantes (x,y) Point D :(%d,%d)\n\n", struct_aav_point_D.x, struct_aav_point_D.y);

	printf("Cordiantes (x,y) Point E :(%d,%d)\n\n", struct_aav_point_E.x, struct_aav_point_E.y);

	return(0);

}