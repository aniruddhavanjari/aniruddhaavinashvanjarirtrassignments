#include <stdio.h>

struct MyPoint
{
	int x;
	int y;

};

struct MyPointerProperties
{
	int quadrant;
	char axis_location[10];

};

struct MyPoint struct_aav_point;
struct MyPointerProperties struct_aav_point_properties;

int main(void)
{
	printf("\n\n");
	printf("Enter X- Cordianate For A Point :");
	scanf("%d", &struct_aav_point.x);
	printf("Enter Y- Cordianate For A Point :");
	scanf("%d", &struct_aav_point.y);

	printf("\n\n");
	printf("Point Cordinate (x,y) Are : (%d,%d)\n\n", struct_aav_point.x, struct_aav_point.y);

	if (struct_aav_point.x == 0 && struct_aav_point.y == 0)
		printf("The Pinter Is The Orignal (%d,%d)\n", struct_aav_point.x, struct_aav_point.y);
	else
	{
		if (struct_aav_point.x == 0)
		{
			if (struct_aav_point.y < 0)
				strcpy(struct_aav_point_properties.axis_location, "Negative Y");
			if (struct_aav_point.y > 0)
				strcpy(struct_aav_point_properties.axis_location, "Positive Y");

			struct_aav_point_properties.quadrant = 0;
			printf("The Poinyt Line on the %s Axis \n\n", struct_aav_point_properties.axis_location);
		}
		else if (struct_aav_point.y == 0)
		{
			if (struct_aav_point.x < 0)
				strcpy(struct_aav_point_properties.axis_location, "Negative X");
			if (struct_aav_point.x > 0)
				strcpy(struct_aav_point_properties.axis_location, "Positive X");

			struct_aav_point_properties.quadrant = 0;
			printf("The Poinyt Line on the %s Axis \n\n", struct_aav_point_properties.axis_location);
		}

		else
		{
			struct_aav_point_properties.axis_location[0] = '\0';

			if (struct_aav_point.x > 0 && struct_aav_point.y > 0)
				struct_aav_point_properties.quadrant = 1;

			else if (struct_aav_point.x < 0 && struct_aav_point.y > 0)
				struct_aav_point_properties.quadrant = 2;

			else if (struct_aav_point.x < 0 && struct_aav_point.y < 0)
				struct_aav_point_properties.quadrant = 3;

			else
				struct_aav_point_properties.quadrant = 4;

			printf("The Point Line in Quadrent %d\n\n", struct_aav_point_properties.quadrant);
		}
	}

	return(0);
}