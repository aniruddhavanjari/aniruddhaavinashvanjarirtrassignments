#include <stdio.h>

struct MyPoint
{
	int x;
	int y;
};

struct MyPoint struct_aav_point_A, struct_aav_point_B, struct_aav_point_C, struct_aav_point_D, struct_aav_point_E;

int main(void)
{

	struct_aav_point_A.x = 1;
	struct_aav_point_A.y = 2;

	struct_aav_point_B.x = 50;
	struct_aav_point_B.y = 80;

	struct_aav_point_C.x = 40;
	struct_aav_point_C.y = 40;

	struct_aav_point_D.x = 90;
	struct_aav_point_D.y = 60;

	struct_aav_point_E.x = 40;
	struct_aav_point_E.y = 90;

	printf("\n\n");
	printf("Cordiantes (x,y) Point A :(%d,%d)\n\n", struct_aav_point_A.x, struct_aav_point_A.y);

	printf("Cordiantes (x,y) Point B :(%d,%d)\n\n", struct_aav_point_B.x, struct_aav_point_B.y);

	printf("Cordiantes (x,y) Point C :(%d,%d)\n\n", struct_aav_point_C.x, struct_aav_point_C.y);

	printf("Cordiantes (x,y) Point D :(%d,%d)\n\n", struct_aav_point_D.x, struct_aav_point_D.y);

	printf("Cordiantes (x,y) Point E :(%d,%d)\n\n", struct_aav_point_E.x, struct_aav_point_E.y);
		
	return(0);

}