#include <stdio.h>

//Define Struct 
struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//variable declaration 
	struct MyData struct_aav_data;

	int i_aav_intSize;
	int i_aav_floatSize;
	int i_aav_boubleSize;
	int i_aav_charSize;
	int i_aav_struct_MyData_Size;

	struct_aav_data.i = 30;
	struct_aav_data.f = 11.45f;
	struct_aav_data.d = 1.2234;
	struct_aav_data.c = 'A';

	printf("\n\n");
	printf("Data Member of MyData:\n\n");
	printf("i = %d\n", struct_aav_data.i);
	printf("f = %f\n", struct_aav_data.f);
	printf("d = %lf\n", struct_aav_data.d);
	printf("c = %c\n", struct_aav_data.c);

	i_aav_intSize = sizeof(struct_aav_data.i);
	i_aav_floatSize = sizeof(struct_aav_data.f);
	i_aav_boubleSize = sizeof(struct_aav_data.d);
	i_aav_charSize = sizeof(struct_aav_data.c);

	printf("\n\n");
	printf("Printing Sizeof of Every Member of MyData: \n\n");
	
	printf("Sizeof of 'i' = %d bytes\n", i_aav_intSize);
	
	printf("Sizeof of 'f' = %d bytes\n", i_aav_floatSize);
	
	printf("Sizeof of 'd' = %d bytes\n", i_aav_boubleSize);
	
	printf("Sizeof of 'c' = %d bytes\n", i_aav_charSize);

	i_aav_struct_MyData_Size = sizeof(struct MyData);

	printf("\n\n");
	printf("Size of Struct MyData : %d bytes\n", i_aav_struct_MyData_Size);

	return(0);

}