#include <stdio.h>

//Defining Struct
struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//fuction prototype
	struct MyData AddStructMEmbers(struct MyData, struct MyData, struct MyData);


	//variable declaration
	struct MyData MyData_aav_one, MyData_aav_two, MyData_aav_three, MyData_aav_answer;

	//code
	printf("\n");
	printf("********Data 1***********\n");

	printf("Enter Integer Value MyData_aav_one.i :\n");
	scanf("%d", &MyData_aav_one.i);
	printf("Enter Float Value MyData_aav_one.f :\n");
	scanf("%f", &MyData_aav_one.f);
	printf("Enter Double Value MyData_aav_one.d :\n");
	scanf("%lf",&MyData_aav_one.d);
	printf("Enter Charater Value MyData_aav_one.c :\n");
	MyData_aav_one.c = getch();
	printf("%c\n", MyData_aav_one.c);
	
	printf("********Data 2***********\n");

	printf("Enter Integer Value MyData_aav_two.i :\n");
	scanf("%d", &MyData_aav_two.i);
	printf("Enter Float Value MyData_aav_two.f :\n");
	scanf("%f", &MyData_aav_two.f);
	printf("Enter Double Value MyData_aav_two.d :\n");
	scanf("%lf", &MyData_aav_two.d);
	printf("Enter Charater Value MyData_aav_two.c :\n");
	MyData_aav_two.c = getch();
	printf("%c\n", MyData_aav_two.c);

	printf("********Data 3***********\n");

	printf("Enter Integer Value MyData_aav_three.i :\n");
	scanf("%d", &MyData_aav_three.i);
	printf("Enter Float Value MyData_aav_three.f :\n");
	scanf("%f", &MyData_aav_three.f);
	printf("Enter Double Value MyData_aav_three.d :\n");
	scanf("%lf", &MyData_aav_three.d);
	printf("Enter Charater Value MyData_aav_three.c :\n");
	MyData_aav_one.c = getch();
	printf("%c\n", MyData_aav_three.c);
	
	MyData_aav_answer = AddStructMEmbers(MyData_aav_one, MyData_aav_two, MyData_aav_three);

	printf("\n\n");
	printf("Answer:\n");
	
	printf("Addresses of Member of Union :\n");
	printf("MyData_aav_answer.i = %d\n", MyData_aav_answer.i);
	printf("MyData_aav_answer.f = %f\n", MyData_aav_answer.f);
	printf("MyData_aav_answer.d = %lf\n", MyData_aav_answer.d);
	
	MyData_aav_answer.c = MyData_aav_one.c;
	printf("MyData_aav_answer.c from Data1 %c\n", MyData_aav_answer.c);

	MyData_aav_answer.c = MyData_aav_two.c;
	printf("MyData_aav_answer.c from Data2 %c\n", MyData_aav_answer.c);

	MyData_aav_answer.c = MyData_aav_three.c;
	printf("MyData_aav_answer.c from Data3 = %c\n", MyData_aav_answer.c);
	
	return(0);
}

struct MyData AddStructMEmbers(struct MyData md_one, struct MyData md_two, struct MyData md_three)
{
	//variable declartion
	struct MyData answer;

	//Code
	answer.i = md_one.i + md_two.i + md_three.i;
	answer.f = md_one.f + md_two.f + md_three.f;
	answer.d = md_one.d + md_two.d + md_three.d;

	return(answer);
}

