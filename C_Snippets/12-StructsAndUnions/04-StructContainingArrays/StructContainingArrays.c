#include <stdio.h>

#define INT_ARRAY_SIZE 10
#define FLOAT_ARRAY_SIZE 5
#define CHAR_ARRAY_SIZE 26

#define NUM_STRINGS 10
#define MAX_CHARACTERS_PER_STRING 20

#define ALPHABET_BEFINNING 65 

struct MyDataOne
{
	int iArray[INT_ARRAY_SIZE];
	float fArray[FLOAT_ARRAY_SIZE];
};

struct MyDataTwo
{
	char c_aav_Array[CHAR_ARRAY_SIZE];
	char c_aav_StrArray[NUM_STRINGS][MAX_CHARACTERS_PER_STRING];
};

int main(void)
{
	struct MyDataOne struct_dataOne;
	struct MyDataTwo struct_dataTwo;
	int i_aav_i;

	struct_dataOne.fArray[0] = 0.1f;
	struct_dataOne.fArray[1] = 0.2f;
	struct_dataOne.fArray[2] = 0.3f;
	struct_dataOne.fArray[3] = 0.4f;
	struct_dataOne.fArray[4] = 0.5f;
	
	printf("\n");
	printf("Enter %d Intergers :\n", INT_ARRAY_SIZE);

	for (i_aav_i = 0; i_aav_i < INT_ARRAY_SIZE; i_aav_i++)
	{
		scanf("%d", &struct_dataOne.iArray[i_aav_i]);
	}

	for (i_aav_i = 0; i_aav_i < CHAR_ARRAY_SIZE; i_aav_i++)
	{
		struct_dataTwo.c_aav_Array[i_aav_i] = (char)(i_aav_i + ALPHABET_BEFINNING);
	}

	strcpy(struct_dataTwo.c_aav_StrArray[0], "Welcome !!!");
	strcpy(struct_dataTwo.c_aav_StrArray[1], "This");
	strcpy(struct_dataTwo.c_aav_StrArray[2], "Is");
	strcpy(struct_dataTwo.c_aav_StrArray[3],"ASTROMEDICOMP'S");
	strcpy(struct_dataTwo.c_aav_StrArray[4], "Real");
	strcpy(struct_dataTwo.c_aav_StrArray[5], "Time");
	strcpy(struct_dataTwo.c_aav_StrArray[6], "Rendering");
	strcpy(struct_dataTwo.c_aav_StrArray[7], "Batch");
	strcpy(struct_dataTwo.c_aav_StrArray[8], "Of");
	strcpy(struct_dataTwo.c_aav_StrArray[9], "2020-2021 !!!");

	printf("\n\n");
	printf("Member of Struct DataOne: \n");
	
	printf("Integer Array data: \n");
	for (i_aav_i = 0; i_aav_i < INT_ARRAY_SIZE; i_aav_i++)
		printf("struct_dataOne[%d] = %d\n", i_aav_i, struct_dataOne.iArray[i_aav_i]);

	printf("\n");

	printf("Floating Point Array Data:\n");
	for (i_aav_i = 0; i_aav_i < FLOAT_ARRAY_SIZE; i_aav_i++)
	{
		printf("struct_dataOne[%d] = %f\n", i_aav_i, struct_dataOne.fArray[i_aav_i]);
	}

	printf("\n");
	printf("Member of Struct MyDataTwo:\n");
	printf("Charater Array Data:\n");
	
	for (i_aav_i = 0; i_aav_i < INT_ARRAY_SIZE; i_aav_i++)
		printf("struct_dataTwo[%d] = %c\n", i_aav_i, struct_dataTwo.c_aav_Array[i_aav_i]);

	printf("\n");
	printf("String Array data: \n");
	for (i_aav_i = 0; i_aav_i < NUM_STRINGS; i_aav_i++)
	{
		printf("%s\n", struct_dataTwo.c_aav_StrArray[i_aav_i]);
	}
	printf("\n\n");
	return(0);
}