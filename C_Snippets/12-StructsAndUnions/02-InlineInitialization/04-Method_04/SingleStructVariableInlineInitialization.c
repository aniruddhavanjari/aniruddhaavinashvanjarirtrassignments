#include <stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{

	struct MyData struct_aav_dataOne = { 31,4.5f,101.4525,'A' };
	struct MyData struct_aav_dataTwo = {'P',4.5f,101.4525,68 };
	struct MyData struct_aav_dataThree = { 36,'G' };
	struct MyData struct_aav_dataFour = { 79 };

	printf("\n\n");
	printf("Data Member of Structure MyData  One:\n\n");
	printf(" i = %d\n", struct_aav_dataOne.i);
	printf(" f = %f\n", struct_aav_dataTwo.f);
	printf(" d = %lf\n", struct_aav_dataThree.d);
	printf(" c = %c\n", struct_aav_dataFour.c);

	printf("\n\n");
	printf("Data Member of Structure MyData Two:\n\n");
	printf(" i = %d\n", struct_aav_dataOne.i);
	printf(" f = %f\n", struct_aav_dataTwo.f);
	printf(" d = %lf\n", struct_aav_dataThree.d);
	printf(" c = %c\n", struct_aav_dataFour.c);

	printf("\n\n");
	printf("Data Member of Structure MyData Three:\n\n");
	printf(" i = %d\n", struct_aav_dataOne.i);
	printf(" f = %f\n", struct_aav_dataTwo.f);
	printf(" d = %lf\n", struct_aav_dataThree.d);
	printf(" c = %c\n", struct_aav_dataFour.c);

	printf("\n\n");
	printf("Data Member of Structure MyData Four:\n\n");
	printf(" i = %d\n", struct_aav_dataOne.i);
	printf(" f = %f\n", struct_aav_dataTwo.f);
	printf(" d = %lf\n", struct_aav_dataThree.d);
	printf(" c = %c\n", struct_aav_dataFour.c);

	return(0);
}

