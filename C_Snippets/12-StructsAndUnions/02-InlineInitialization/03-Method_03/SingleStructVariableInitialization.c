#include <stdio.h>


int main(void)
{
	struct MyData
{
	int i;
	float f;
	double d;
	char c;
}struct_aav_data = { 31,4.5f,101.4525,'Z' };


	printf("\n\n");
	printf("Data Member of Structure MyData :\n\n");
	printf(" i = %d\n", struct_aav_data.i);
	printf(" f = %f\n", struct_aav_data.f);
	printf(" d = %lf\n", struct_aav_data.d);
	printf("c = %c\n", struct_aav_data.c);

	return(0);
}

