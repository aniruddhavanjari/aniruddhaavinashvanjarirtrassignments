#include <stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
}

struct MyData struct_aav_data = { 301,41.5f,111.4525,'Z' };

int main(void)
{


	printf("\n\n");
	printf("Data Member of Structure MyData :\n\n");
	printf(" i = %d\n", struct_aav_data.i);
	printf(" f = %f\n", struct_aav_data.f);
	printf(" d = %lf\n", struct_aav_data.d);
	printf("c = %c\n", struct_aav_data.c);

	return(0);
}

