#include <stdio.h>

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//variable declarations
	struct MyData structMyData_aav_data;

	//code
	structMyData_aav_data.i = 50;
	structMyData_aav_data.f = 15.54f;
	structMyData_aav_data.d = 53.4353;
	structMyData_aav_data.c = 'A';

	printf("\n");
	printf("Data Members of MyData Structure :\n");
	printf(" i = %d\n", structMyData_aav_data.i);
	printf(" f = %f\n", structMyData_aav_data.f);
	printf(" d = %lf\n", structMyData_aav_data.d);
	printf(" c = %c\n", structMyData_aav_data.c);

	printf("\n");
	printf("Data Members of MyData Structure :\n");
	printf(" i Occupies Addresses From  %p\n", &structMyData_aav_data.i);
	printf(" f Occupies Addresses From  %p\n", &structMyData_aav_data.f);
	printf(" d Occupies Addresses From  %p\n", &structMyData_aav_data.d);
	printf(" c Occupies Addresses From  %p\n", &structMyData_aav_data.c);

	printf("Strting Address of 'MyData' varaible = %p\n", &structMyData_aav_data);

	return(0);
}