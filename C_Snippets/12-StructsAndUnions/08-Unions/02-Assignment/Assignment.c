#include <stdio.h>

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//variable declaration
	union MyUnion MyUnion_aav_u1, MyUnion_aav_u2;

	//code
	printf("\n");
	printf("Members of Union 1 Are :\n");

	MyUnion_aav_u1.i = 6;
	MyUnion_aav_u1.f = 1.2f;
	MyUnion_aav_u1.d = 9.25226;
	MyUnion_aav_u1.c = 'A';

	printf("MyUnion_aav_u1.i = %d\n", MyUnion_aav_u1.i);
	printf("MyUnion_aav_u1.f = %f\n", MyUnion_aav_u1.f);
	printf("MyUnion_aav_u1.d = %lf\n", MyUnion_aav_u1.d);
	printf("MyUnion_aav_u1.c = %c\n", MyUnion_aav_u1.c);

	printf("Addresses of Member of Union 1 :\n");
	printf("MyUnion_aav_u1.i = %p\n", &MyUnion_aav_u1.i);
	printf("MyUnion_aav_u1.f = %p\n", &MyUnion_aav_u1.f);
	printf("MyUnion_aav_u1.d = %p\n", &MyUnion_aav_u1.d);
	printf("MyUnion_aav_u1.c = %p\n", &MyUnion_aav_u1.c);
	printf("MyUnion_aav_u1 = %p\n", &MyUnion_aav_u1);


	printf("\n");
	printf("Members of Union 2 Are :\n");
	
	MyUnion_aav_u2.i = 58;
	printf("MyUnion_aav_u2.i = %d\n", MyUnion_aav_u2.i);
	
	MyUnion_aav_u2.f = 45.89f;
	printf("MyUnion_aav_u2.f = %f\n", MyUnion_aav_u2.f);
	
	MyUnion_aav_u2.d = 32.25262;
	printf("MyUnion_aav_u2.d = %lf\n", MyUnion_aav_u2.d);
	
	MyUnion_aav_u2.c = 'A';
	printf("MyUnion_aav_u2.c = %c\n", MyUnion_aav_u2.c);

	printf("Addresses of Member of Union 2 :\n");
	printf("MyUnion_aav_u2.i = %p\n", &MyUnion_aav_u2.i);
	printf("MyUnion_aav_u2.f = %p\n", &MyUnion_aav_u2.f);
	printf("MyUnion_aav_u2.d = %p\n", &MyUnion_aav_u2.d);
	printf("MyUnion_aav_u2.c = %p\n", &MyUnion_aav_u2.c);
	printf("MyUnion_aav_u2 = %p\n", &MyUnion_aav_u2);

	return(0);
}