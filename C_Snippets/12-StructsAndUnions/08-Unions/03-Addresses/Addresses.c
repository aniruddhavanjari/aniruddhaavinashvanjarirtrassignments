#include <stdio.h>

struct MyStructure
{
	int i;
	float f;
	double d;
	char c;
};


union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//variable declaration
	struct MyStructure MyStruct_aav_s;
	union MyUnion  MyUnion_aav_u;

	//code
	printf("\n");
	printf("Members of Structure Are :\n");

	MyStruct_aav_s.i = 6;
	MyStruct_aav_s.f = 1.2f;
	MyStruct_aav_s.d = 9.25226;
	MyStruct_aav_s.c = 'A';

	printf("MyStruct_aav_s.i = %d\n", MyStruct_aav_s.i);
	printf("MyStruct_aav_s.f = %f\n", MyStruct_aav_s.f);
	printf("MyStruct_aav_s.d = %lf\n", MyStruct_aav_s.d);
	printf("MyStruct_aav_s.c = %c\n", MyStruct_aav_s.c);

	printf("Addresses of Member of Structure :\n");
	printf("MyStruct_aav_s.i = %p\n", &MyStruct_aav_s.i);
	printf("MyStruct_aav_s.f = %p\n", &MyStruct_aav_s.f);
	printf("MyStruct_aav_s.d = %p\n", &MyStruct_aav_s.d);
	printf("MyStruct_aav_s.c = %p\n", &MyStruct_aav_s.c);
	printf("MyStruct_aav_s = %p\n", &MyStruct_aav_s);


	printf("\n");
	printf("Members of Union  Are :\n");

	MyUnion_aav_u.i = 58;
	printf("MyUnion_aav_u.i = %d\n", MyUnion_aav_u.i);

	MyUnion_aav_u.f = 45.89f;
	printf("MyUnion_aav_u.f = %f\n", MyUnion_aav_u.f);

	MyUnion_aav_u.d = 32.25262;
	printf("MyUnion_aav_u.d = %lf\n", MyUnion_aav_u.d);

	MyUnion_aav_u.c = 'A';
	printf("MyUnion_aav_u.c = %c\n", MyUnion_aav_u.c);

	printf("Addresses of Member of Union :\n");
	printf("MyUnion_aav_u.i = %p\n", &MyUnion_aav_u.i);
	printf("MyUnion_aav_u.f = %p\n", &MyUnion_aav_u.f);
	printf("MyUnion_aav_u.d = %p\n", &MyUnion_aav_u.d);
	printf("MyUnion_aav_u.c = %p\n", &MyUnion_aav_u.c);
	printf("MyUnion_aav_u = %p\n", &MyUnion_aav_u);

	return(0);
}