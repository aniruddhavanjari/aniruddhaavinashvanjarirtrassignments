#include <stdio.h>

struct MyStruct
{
	int i;
	float f;
	double d;
	char c;
};

union MyUnion
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	//variable declaration
	struct MyStruct MyStruct_aav_s;
	union MyUnion MyUnion_aav_u;

	//code
	printf("\n");
	printf("Size of MyStruct = %lu\n", sizeof(MyStruct_aav_s));
	printf("\n");
	printf("Size of MyUnion = %lu\n", sizeof(MyUnion_aav_u));

	return(0);
}