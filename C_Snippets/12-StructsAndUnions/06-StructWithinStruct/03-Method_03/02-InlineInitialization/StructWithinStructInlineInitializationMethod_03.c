#include <stdio.h>

int main(void)
{
	// local Structure visible inside main only
	struct Rectangle
	{
		//Structure Inside Structure
		struct MyPoint
		{
			int x;
			int y;
		}point_01, point_02; // Inner Structure 2 Variable
	}struct_aav_rect = { {4,7,} , {9,10} }; // Outer strcture Variable

	//Variable declaration
	int i_aav_length, i_aav_breadth, i_aav_area;

	//Code 
	i_aav_length = struct_aav_rect.point_02.y - struct_aav_rect.point_01.y;
	if (i_aav_length < 0)
		i_aav_length = i_aav_length * -1;

	i_aav_breadth = struct_aav_rect.point_02.x - struct_aav_rect.point_01.x;
	if (i_aav_breadth < 0)
		i_aav_breadth = i_aav_breadth * -1;

	i_aav_area = i_aav_length * i_aav_breadth;

	printf("\n");
	printf("Length of Rectangle = %d \n", i_aav_length);
	printf("Breadth of Rectangle = %d \n", i_aav_breadth);
	printf("Area of Rectangle = %d \n", i_aav_area);

	return(0);

}