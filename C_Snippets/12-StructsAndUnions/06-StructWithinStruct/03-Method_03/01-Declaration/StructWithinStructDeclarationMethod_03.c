#include <stdio.h>

int main(void)
{
	// local Structure visible inside main only
	struct Rectangle
	{
		//Structure Inside Structure
		struct MyPoint
		{
			int x;
			int y;
		}point_01, point_02; // Inner Structure 2 Variable
	}struct_aav_rect; // Outer strcture Variable

	//Variable declaration
	int i_aav_length, i_aav_breadth, i_aav_area;

	//Code 
	printf("\n");
	printf("enter LeftMost X-Cordianate of Rectangle:");
	scanf("%d", &struct_aav_rect.point_01.x);

	printf("\n");
	printf("Enter Bottommost Y-Cordinate of Rectangle:");
	scanf("%d", &struct_aav_rect.point_01.y);

	printf("\n");
	printf("enter RightMost X-Cordianate of Rectangle:");
	scanf("%d", &struct_aav_rect.point_02.x);

	printf("\n");
	printf("Enter Topmmost Y-Cordinate of Rectangle:");
	scanf("%d", &struct_aav_rect.point_02.y);

	i_aav_length = struct_aav_rect.point_02.y - struct_aav_rect.point_01.y;
	if (i_aav_length < 0)
		i_aav_length = i_aav_length * -1;

	i_aav_breadth = struct_aav_rect.point_02.x - struct_aav_rect.point_01.x;
	if (i_aav_breadth < 0)
		i_aav_breadth = i_aav_breadth * -1;

	i_aav_area = i_aav_length * i_aav_breadth;

	printf("\n");
	printf("Length of Rectangle = %d \n", i_aav_length);
	printf("Breadth of Rectangle = %d \n", i_aav_breadth);
	printf("Area of Rectangle = %d \n", i_aav_area);

	return(0);

}