#include <stdio.h>

struct Rectangle
{
	struct MyPoint
	{
		int x;
		int y;
	}point_01, point_02;

}rect = { {4,5},{8,9} };

int main(void)
{

	//Variable declaration
	int i_aav_length, i_aav_breadth, i_aav_area;

	//Code 
	i_aav_length = rect.point_02.y - rect.point_01.y;
	if (i_aav_length < 0)
		i_aav_length = i_aav_length * -1;

	i_aav_breadth = rect.point_02.x - rect.point_01.x;
	if (i_aav_breadth < 0)
		i_aav_breadth = i_aav_breadth * -1;

	i_aav_area = i_aav_length * i_aav_breadth;

	printf("\n");
	printf("Length of Rectangle = %d \n", i_aav_length);
	printf("Breadth of Rectangle = %d \n", i_aav_breadth);
	printf("Area of Rectangle = %d \n", i_aav_area);

	return(0);
}