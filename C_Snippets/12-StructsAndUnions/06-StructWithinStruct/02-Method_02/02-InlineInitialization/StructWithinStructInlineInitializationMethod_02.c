#include <stdio.h>

struct MyPoint
{
	int x;
	int y;
};

struct Rectangle
{

	struct MyPoint point_01, point_02;

};
struct Rectangle struct_aav_rect = { {5,7},{8,9}};

int main(void)
{
	//Variable declaration
	
	int i_aav_length, i_aav_breadth, i_aav_area;

	//Code 
	i_aav_length = struct_aav_rect.point_02.y - struct_aav_rect.point_01.y;
	if (i_aav_length < 0)
		i_aav_length = i_aav_length * -1;

	i_aav_breadth = struct_aav_rect.point_02.x - struct_aav_rect.point_01.x;
	if (i_aav_breadth < 0)
		i_aav_breadth = i_aav_breadth * -1;

	i_aav_area = i_aav_length * i_aav_breadth;

	printf("\n");
	printf("Length of Rectangle = %d \n", i_aav_length);
	printf("Breadth of Rectangle = %d \n", i_aav_breadth);
	printf("Area of Rectangle = %d \n", i_aav_area);

	return(0);

}