#include <stdio.h>

struct MyNumber
{
	int num;
	int num_table[10];
};

struct NumTables
{
	struct MyNumber n;
};

int main(void)
{
	//variable declrations
	struct NumTables struct_tables[10];
	
	int i_aav_i, i_aav_j;

	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		struct_tables[i_aav_i].n.num = (i_aav_i + 1);
	}

	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("\n\n");
		printf("Table of %d :\n\n", struct_tables[i_aav_i].n.num); 
		for (i_aav_j = 0; i_aav_j < 10; i_aav_j++)
		{
			struct_tables[i_aav_i].n.num_table[i_aav_j] = struct_tables[i_aav_i].n.num * (i_aav_j + 1);
			printf("%d * %d = %d \n", struct_tables[i_aav_i].n.num, (i_aav_j + 1), struct_tables[i_aav_i].n.num_table[i_aav_j]);
		}
	}
	return(0);
}