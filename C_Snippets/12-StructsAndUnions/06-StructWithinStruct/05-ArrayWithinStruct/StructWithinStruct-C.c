#include <stdio.h>

struct MyNumber
{
	int num;
	int num_table[10];
};

struct NumTables
{
	struct MyNumber a;
	struct MyNumber b;
	struct MyNumber c;
};

int main(void)
{
	struct NumTables struct_aav_table;
	int i_aav_i;

	struct_aav_table.a.num = 2;
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
		struct_aav_table.a.num_table[i_aav_i] = struct_aav_table.a.num * (i_aav_i + 1);
	printf("\n");
	printf("Table of %d :\n\n", struct_aav_table.a.num);
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
		printf("%d * %d = %d\n", struct_aav_table.a.num, (i_aav_i + 1), struct_aav_table.a.num_table[i_aav_i]);
	
	struct_aav_table.b.num = 3;
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
		struct_aav_table.b.num_table[i_aav_i] = struct_aav_table.b.num * (i_aav_i + 1);
	printf("\n");
	printf("Table of %d :\n\n", struct_aav_table.b.num);
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
		printf("%d * %d = %d\n", struct_aav_table.b.num, (i_aav_i + 1), struct_aav_table.b.num_table[i_aav_i]);

	struct_aav_table.c.num = 4;
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
		struct_aav_table.c.num_table[i_aav_i] = struct_aav_table.c.num * (i_aav_i + 1);
	printf("\n");
	printf("Table of %d :\n\n", struct_aav_table.c.num);
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
		printf("%d * %d = %d\n", struct_aav_table.c.num, (i_aav_i + 1), struct_aav_table.c.num_table[i_aav_i]);

	return(0);
}