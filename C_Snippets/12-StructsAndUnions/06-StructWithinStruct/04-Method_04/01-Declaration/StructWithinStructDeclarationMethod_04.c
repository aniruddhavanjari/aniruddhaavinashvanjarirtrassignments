#include <stdio.h>

int main(void)
{
	//Variable declaration
	int i_aav_length, i_aav_breadth, i_aav_area;

	// local Structures visible inside main only
	struct MyPoint
	{
		int x;
		int y;
	};
	
	struct Rectangle
	{
		struct MyPoint point_01;
		struct MyPoint point_02;
	};

	struct Rectangle struct_aav_rect;

	//Code 
	printf("\n");
	printf("enter LeftMost X-Cordianate of Rectangle:");
	scanf("%d", &struct_aav_rect.point_01.x);

	printf("\n");
	printf("Enter Bottommost Y-Cordinate of Rectangle:");
	scanf("%d", &struct_aav_rect.point_01.y);

	printf("\n");
	printf("enter RightMost X-Cordianate of Rectangle:");
	scanf("%d", &struct_aav_rect.point_02.x);

	printf("\n");
	printf("Enter Topmmost Y-Cordinate of Rectangle:");
	scanf("%d", &struct_aav_rect.point_02.y);

	i_aav_length = struct_aav_rect.point_02.y - struct_aav_rect.point_01.y;
	if (i_aav_length < 0)
		i_aav_length = i_aav_length * -1;

	i_aav_breadth = struct_aav_rect.point_02.x - struct_aav_rect.point_01.x;
	if (i_aav_breadth < 0)
		i_aav_breadth = i_aav_breadth * -1;

	i_aav_area = i_aav_length * i_aav_breadth;

	printf("\n");
	printf("Length of Rectangle = %d \n", i_aav_length);
	printf("Breadth of Rectangle = %d \n", i_aav_breadth);
	printf("Area of Rectangle = %d \n", i_aav_area);

	return(0);

}