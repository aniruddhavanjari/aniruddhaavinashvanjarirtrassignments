#include <stdio.h>

typedef int MY_INT;

int main(void)
{
	//Fucntion Prototype
	MY_INT Add(MY_INT, MY_INT);

	//Typedefs
	typedef int MY_INT;

	typedef float PVG_FLOAT;

	typedef char CHARAVTER;

	typedef double MY_DOUBLE;

	// Win32SDK
	typedef unsigned int UINT;
	typedef UINT HANDLE;
	typedef HANDLE HWND;
	typedef HANDLE HINDTANCE;

	MY_INT a = 10, i_aav_i;
	MY_INT iArray[] = { 1, 2, 3, 4, 5, 6, 7, 8,9 };

	PVG_FLOAT f_pvg = 30.9f;
	const PVG_FLOAT f_pvg_pi = 3.14f;

	CHARAVTER c_aav_c = '*';
	CHARAVTER c_aav_ArrayOne[] = "Hello";
	CHARAVTER c_aav_ArrayTwo[][10] = {"RTR","Batch","2020-2021"};

	MY_DOUBLE d_aav_d = 8.041997;

	UINT uint = 128;
	HANDLE handle = 56;
	HWND hwnd = 1001;
	HINDTANCE hInstance = 123456;

	//Code
	printf("\n\n");
	printf("Type MY-INT varaible a = %d \n", a);
	printf("\n");

	for (i_aav_i = 0; i_aav_i < (sizeof(c_aav_ArrayOne) / sizeof(int)); i_aav_i++)
	{
		printf("Type MY_INT array variable ArrayOne[%d] = %d\n", i_aav_i, c_aav_ArrayOne[i_aav_i]);

	}
	printf("\n");
	printf("Type PVG_FLOAT variable f_aav = %f\n", f_pvg);
	printf("Type PVG_FLOAT variable f_aav_pi = %f\n", f_pvg_pi);

	printf("\n");
	printf("Type MY_DOUBLE variable d_aav_d = %lf\n", d_aav_d);

	printf("\n");
	printf("Type CHARACTER variable d_aav_d = %c\n", c_aav_c);

	printf("\n");
	printf("Type CHARACTER array variable c_aav_ArrayOne = %s\n", c_aav_ArrayOne);

	printf("\n");
	for (i_aav_i = 0; i_aav_i < (sizeof(c_aav_ArrayTwo) / sizeof(c_aav_ArrayTwo[0])); i_aav_i++)
	{
		printf("%s\t", c_aav_ArrayTwo[i_aav_i]);
	}
	printf("\n");


	printf("\n");
	printf("Type UINT variable d_aav_d = %u\n", uint);
	printf("Type HANDLE variable d_aav_d = %u\n", handle);
	printf("Type MY_DOUBLE variable d_aav_d = %u\n", hwnd);
	printf("Type MY_DOUBLE variable d_aav_d = %u\n", hInstance);
	printf("\n\n");

	MY_INT i_aav_a = 10;
	MY_INT i_aav_b = 30;
	MY_INT i_aav_ret;

	i_aav_ret = Add(i_aav_a, i_aav_b);
	printf("Result = %d \n", i_aav_ret);

	return(0);
}


MY_INT Add(MY_INT i_aav_a, MY_INT i_aav_b)
{
	//Code
	MY_INT i_aav_c;
	i_aav_c = i_aav_a + i_aav_b;
	return(i_aav_c);
}