#include <stdio.h>

#define MAX_NAME_LENGTH 100

struct Employee
{
	char name[MAX_NAME_LENGTH];
	unsigned int age;
	char gender;
	double salary;
};

struct MyData
{
	int i;
	float f;
	double d;
	char c;
};

int main(void)
{
	typedef struct Employee MY_EMPLOYEE_TYPE;
	typedef struct MyData MY_DATA_TYPE;

	struct Employee struct_employee = { "Funny",25,'M',10000.00 };
	MY_EMPLOYEE_TYPE typedef_employee = { "Bunny",50,'F',202020.00 };

	struct MyData struct_mydata = { 3,12.12f,23.123,'X' };
	MY_DATA_TYPE typedef_mydata;

	//Code
	typedef_mydata.i = 1;
	typedef_mydata.f = 1.5f;
	typedef_mydata.d = 9.23325;
	typedef_mydata.c = 'Z';

	printf("\n\n");
	printf("struct employee :\n");
	printf("Name : %s\n", struct_employee.name);
	printf("AGE : %d\n", struct_employee.age);
	printf("Gender : %c\n", struct_employee.gender);
	printf("Salary : %lf\n", struct_employee.salary);

	printf("\n\n");
	printf("MY_EMPLOTEE_TYPE typedef :\n");
	printf("Name : %s\n", typedef_employee.name);
	printf("AGE : %d\n", typedef_employee.age);
	printf("Gender : %c\n", typedef_employee.gender);
	printf("Salary : %lf\n", typedef_employee.salary);

	printf("\n");
	printf("struct MyData:\n");
	printf("i = %d\n", struct_mydata.i);
	printf("f = %f\n", struct_mydata.f);
	printf("d = %lf\n", struct_mydata.d);
	printf("c = %c\n", struct_mydata.c);
	
	printf("\n");
	printf("MY_DATA_TYPE typedef:\n");
	printf("i = %d\n", typedef_mydata.i);
	printf("f = %f\n", typedef_mydata.f);
	printf("d = %lf\n", typedef_mydata.d);
	printf("c = %c\n", typedef_mydata.c);

	printf("\n");
	return(0);
}