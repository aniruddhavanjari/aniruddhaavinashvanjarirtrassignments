#include <stdio.h>
#include <stdarg.h>

#define NUM_TO_BE_FOUND 3
#define NUM_ELEMENTS 10

int main(void)
{
	//Function prototype
	void FindNumber(int, int, ...);

	//variable declarations
	int i_aav_answer;

	//code
	printf("\n\n");

	FindNumber(NUM_TO_BE_FOUND, NUM_ELEMENTS, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1);

	return(0);
}

void FindNumber(int num_to_be_found, int num, ...)// Variabic Function
{
	//varaible declarations
	int count = 0;
	int n;
	va_list number_list;

	//code
	va_start(number_list, num);

	count = va_FindNumber(num_to_be_found, num, number_list);

	if (count == 0)
		printf("Number %d Could Not be Found \n\n", num_to_be_found);
	else
		printf("Number %d Found %d Times \n\n", num_to_be_found, count);

	va_end(number_list);
}

int va_FindNumber(int num_to_be_found, int num, va_list list)
{
	int count = 0, n;


	while (num)
	{
		n = va_arg(list, int);
		if (n == num_to_be_found)
			count++;
		num--;
	}
	return(count);
}