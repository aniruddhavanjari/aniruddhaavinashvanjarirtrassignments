#include <stdio.h>
#include <stdarg.h>

int main(void)
{
	//Function prototype
	int CalculateSum(int, ...);

	//variable declarations
	int i_aav_answer;

	//code
	printf("\n\n");

	i_aav_answer = CalculateSum(5, 20, 40, 40, 50,60);
	printf("Answer = %d\n", i_aav_answer);

	i_aav_answer = CalculateSum(10, 1, 1, 1, 1, 1, 1, 1, 1, 1,1);
	printf("Answer = %d\n", i_aav_answer);

	i_aav_answer = CalculateSum(0);
	printf("Answer = %d\n", i_aav_answer);

	return(0);
}

int CalculateSum(int num, ...) // Variabic Function
{
	//Function prototype
	int va_CalculateSum(int, va_list);

	//varaible declarations
	int sum = 0; 
	va_list number_list;

	//code
	va_start(number_list, num);

	sum = va_CalculateSum(num, number_list);

	va_end(number_list);

	return(sum);
}

int va_CalculateSum(int num, va_list list)
{

	//variale declaration
	int n;
	int sum_total = 0;

	//code
	while (num)
	{
		n = va_arg(list, int);
		sum_total = sum_total + n;
		num--;
	}

	return(sum_total);
}