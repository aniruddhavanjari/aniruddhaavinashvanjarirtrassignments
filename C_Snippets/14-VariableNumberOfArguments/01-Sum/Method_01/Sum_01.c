#include <stdio.h>
#include <stdarg.h>

int main(void)
{
	//Function prototype
	int CalculateSum(int, ...);

	//variable declarations
	int i_aav_answer;

	//code
	printf("\n\n");

	i_aav_answer = CalculateSum(5, 200, 400, 400, 500, 600);
	printf("Answer = %d\n", i_aav_answer);

	i_aav_answer = CalculateSum(10, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1);
	printf("Answer = %d\n", i_aav_answer);

	i_aav_answer = CalculateSum(0);
	printf("Answer = %d\n", i_aav_answer);

	return(0);
}

int CalculateSum(int num, ...) // Variabic Function
{
	//varaible declarations
	int sum_total = 0;
	int n;
	va_list number_list;

	//code
	va_start(number_list, num);

	while (num)
	{
		n = va_arg(number_list, int);
		sum_total = sum_total + n;
		num--;
	}

	va_end(number_list);

	return(sum_total);
}
