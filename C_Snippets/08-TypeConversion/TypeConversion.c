#include <stdio.h>

int main(void)
{
	char c_aav_ch1;
	char c_aav_ch2;

	int i_aav_number;
	int i_aav_number1;
	int i_aav_number2;

	int i_aav_result;
	int i_aav_explicit;

	float f_aav_number1;
	float f_aav_explicit;
	float f_aav_result;
	printf("\n\n");

	i_aav_number = 65;
	c_aav_ch1 = i_aav_number;
	printf(" i_aav_number = %d\n", i_aav_number);
	printf("Implicit Type Casting int to char %c\n\n", c_aav_ch1);

	c_aav_ch2 = 'a';
	i_aav_number1 = c_aav_ch2;
	printf("Implicit Type Casting char to int %c\n\n", c_aav_ch2);

	i_aav_number2 = 24;
	f_aav_number1 = 24.6f;
	f_aav_result = i_aav_number2 + f_aav_number1;
	printf("Integer  :%d float :%f  Restul :%f\n\n",i_aav_number2, f_aav_number1, f_aav_result);
	
	i_aav_result = i_aav_number2 + f_aav_number1;
	printf("Integer  :%d float :%f  Restul :%d\n\n", i_aav_number2, f_aav_number1, i_aav_result);

	f_aav_explicit = 30.1234f;
	i_aav_explicit = (int)f_aav_explicit;
	printf("Floating :%f\n",f_aav_explicit);
	printf("Interger After Explicit Type Casting of %f = %d\n\n", f_aav_explicit,i_aav_explicit);

	return(0);

}




