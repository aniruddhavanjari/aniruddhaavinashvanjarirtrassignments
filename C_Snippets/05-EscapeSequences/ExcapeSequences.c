#include <stdio.h>
int main(void)
{
	//Code 
	printf("\n\n");
	printf("Going On To Next Line...Using \\n Escape Sequence\n\n");
	printf("Demonstrating \t Horizontal \t Tab \t Using \t \\t Excape Sequecw !!!\n\n");
	printf("\"This Is A Double Quoted Outed\" Done Using \\\" \\\" Escape Sequence\n\n");
	printf("\'This Is A Single Quoted Output\' Done Using \\\' \\\' Escape Sequence\n\n");
	printf("BACKSPACE Turned To BACKSPACE\b Using Escape Sequence \\b\n\n");

	printf("\r Demonstrating Carriage Return Using \\r Escape Sequece\n");
	printf("Demonstrating \r Carriage Return Using \\r Escape Sequece\n");
	printf("Demonstrating Carriage \r Return Using \\r Escape Sequence\n\n ");

	printf("Demonstarating \x41 Using \\xhh Escape Sequence\n\n"); //0x41 Is The Hecadecimal Code For Letter 'A'. 'xhh' Is The Place-holder For 'x' Followed By 2 digits (hh), altogether forming a Hexadecimal Number.
	printf("Demonstarating \102 Using \\ooo Escape Sequence\n\n"); //102 Is The Octal Code For Letter 'B'. 'ooo' Is The Place-holder For 3 Digits Forming An Octal Number.
	return(0);
}

