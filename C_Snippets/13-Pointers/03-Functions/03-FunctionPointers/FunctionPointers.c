#include <stdio.h>

int main(void)
{
	//Function Declarations
	int AddIntegers(int, int);
	int SubtractIntegers(int, int);
	float AddFloats(float, float);

	//variable declaration
	typedef int (*AddIntsFnPtr)(int, int);
	AddIntsFnPtr ptrAddTwoIntegers = NULL;
	AddIntsFnPtr ptrFunc = NULL;

	typedef float (*AddFloatsFnPtr)(float, float);
	AddFloatsFnPtr ptrAddTwoFloats = NULL;

	int i_aav_Answer = 0;
	float f_aav_Answer = 0.0f;

	//code
	ptrAddTwoIntegers = AddIntegers;
	i_aav_Answer = ptrAddTwoIntegers(9, 30);
	printf("\n\n");
	printf("Sum of Integers = %d \n", i_aav_Answer);

	ptrFunc = SubtractIntegers;
	i_aav_Answer = ptrFunc(8, 29);
	printf("\n");
	printf("Subtraction of Integers = %d\n", i_aav_Answer);

	ptrAddTwoFloats = AddFloats;
	f_aav_Answer = ptrAddTwoFloats(9.12f, 45.6f);
	printf("\n\n");
	printf("Sum of Floating Point Number = %f\n", f_aav_Answer);

	return(0);
}

int AddIntegers(int a, int b)
{
	//variable declaration
	int sum;
	//code 
	sum = a + b;
	return(sum);
}

int SubtractIntegers(int a, int b)
{
	//variable declaration
	int sum;

	//code
	sum = a + b;
	return(sum);
}

float AddFloats(float a, float b)
{
	//variable declaration
	float sum;

	//code
	sum = a + b;
	return(sum);

}

