#include <stdio.h>
#include <stdlib.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//Fucntion prototype
	char* ReplaceVowesWithHashSymbol(char*);
	
	//variable declaration

	char string[MAX_STRING_LENGTH];
	char* ptr_aav_replaceString = NULL;
	
	//Code
	
	//String Input
	printf("Enter A String :\n");
	gets_s(string, MAX_STRING_LENGTH);

	ptr_aav_replaceString  = ReplaceVowesWithHashSymbol(string);
	if (ptr_aav_replaceString == NULL)
	{
		printf("ReplaceVowesWithHashSymbol() function has Failed. Exitting Now\n");
		exit(0);
	}

	
	printf("\n");
	printf("Replaced String  is :\n");
	printf("%s\n", ptr_aav_replaceString);


	if (ptr_aav_replaceString)
	{
		free(ptr_aav_replaceString);
		ptr_aav_replaceString = NULL;
	}
	return(0);
}

char* ReplaceVowesWithHashSymbol(char *s)
{
	//Function declarations
	void MyStrcpy(char*, char*);
	int MyStrlen(char*);

	//variable declarations
	char* ptr_aav_new_string = NULL;
	int i_aav_i;

	//code
	printf("\n");
	ptr_aav_new_string = (char*)malloc(MAX_STRING_LENGTH * sizeof(char));
	if (ptr_aav_new_string == NULL)
	{
		printf("Memory Allocation of New String Failed. Exitting Now\n");
		return(NULL);
	}

	MyStrcpy(ptr_aav_new_string, s);
	for (i_aav_i = 0; i_aav_i < MyStrlen(ptr_aav_new_string); i_aav_i++)
	{
		switch (ptr_aav_new_string[i_aav_i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			ptr_aav_new_string[i_aav_i] = '#';
			break;
		default:
			break;
		}

			
	}
	return(ptr_aav_new_string);
}



void MyStrcpy(char* str_destination, char* str_source)
{
	//Fuction declartion
	int MyStrlen(char*);

	//varaible declarations
	int i_aav_Stringlength = 0;
	int i_aav_j = 0;

	//code
	i_aav_Stringlength = MyStrlen(str_source);
	for (i_aav_j = 0; i_aav_j < i_aav_Stringlength; i_aav_j++)
		*(str_destination + i_aav_j) = *(str_source + i_aav_j);
	*(str_destination + i_aav_j) = '\0';
}

int MyStrlen(char* str)
{
	//variable declaration
	int i_aav_j;
	int i_aav_stringLen = 0;

	//code
	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (*(str + i_aav_j) == '\0')
			break;
		i_aav_stringLen++;
	}
	return(i_aav_stringLen);
}
