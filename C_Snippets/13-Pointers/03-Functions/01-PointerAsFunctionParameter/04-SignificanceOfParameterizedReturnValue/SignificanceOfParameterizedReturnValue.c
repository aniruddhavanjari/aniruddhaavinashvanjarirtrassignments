#include <stdio.h>

enum
{
	NEGATIVE = -1,
	ZERO,
	POSITIVE
};

int main(void)
{
	//function declaration
	int Diffrence(int, int, int*);

	//variable declaration
	int i_aav_a;
	int i_aav_b;
	int i_aav_answer, i_aav_ret;
	//code 
	printf("\n\n");
	printf("Enter Valaue for 'A' ");
	scanf("%d", &i_aav_a);

	printf("\n\n");
	printf("Enter Valaue for 'B' ");
	scanf("%d", &i_aav_b);


	i_aav_ret = Diffrence(i_aav_a, i_aav_b, &i_aav_answer);

	printf("\n");

	printf("Difference of %d And %d = %d \n ", i_aav_a, i_aav_b, i_aav_answer);

	if (i_aav_ret == POSITIVE)
		printf("The Diefference of %d And %d Is Positive \n", i_aav_a, i_aav_b);

	else if (i_aav_ret == NEGATIVE)
		printf("The Diefference of %d And %d Is Negative \n", i_aav_a, i_aav_b);

	else
		printf("The Diefference of %d And %d Zero \n", i_aav_a, i_aav_b);

	return(0);
}

int Diffrence(int i_aav_x, int i_aav_y, int* pi_aav_diff)
{
	//code
	*pi_aav_diff = i_aav_x - i_aav_y;
	if (*pi_aav_diff > 0)
		return(POSITIVE);
	else if (*pi_aav_diff < 0)
		return(NEGATIVE);
	else
		return(ZERO);
}

