#include <stdio.h>

int main(void)
{
	//function declaration
	void MathematicalOperations(int, int, int*, int*, int*, int*, int*);

	//variable declaration
	int i_aav_a;
	int i_aav_b;
	int *ptri_aav_answer_sum;
	int *ptri_aav_answer_diffrenece;
	int *ptri_aav_answer_product;
	int *ptri_aav_answer_questient;
	int *ptri_aav_answer_remainder;

	//code 
	printf("\n\n");
	printf("Enter Valaue for 'A' ");
	scanf("%d", &i_aav_a);

	printf("\n\n");
	printf("Enter Valaue for 'B' ");
	scanf("%d", &i_aav_b);

	ptri_aav_answer_sum = (int*)malloc(1 * sizeof(int));
	if (ptri_aav_answer_sum == NULL)
	{
		printf("Could not Allocate Memory For 'ptri_aav_answer_sum' .Exiting Now\n");
		exit(0);
	}

	ptri_aav_answer_diffrenece = (int*)malloc(1 * sizeof(int));
	if (ptri_aav_answer_diffrenece == NULL)
	{
		printf("Could not Allocate Memory For 'ptri_aav_answer_diffrenece' .Exiting Now\n");
		exit(0);
	}

	ptri_aav_answer_product = (int*)malloc(1 * sizeof(int));
	if (ptri_aav_answer_product == NULL)
	{
		printf("Could not Allocate Memory For 'ptri_aav_answer_product' .Exiting Now\n");
		exit(0);
	}

	ptri_aav_answer_questient = (int*)malloc(1 * sizeof(int));
	if (ptri_aav_answer_questient == NULL)
	{
		printf("Could not Allocate Memory For 'ptri_aav_answer_questient' .Exiting Now\n");
		exit(0);
	}
	ptri_aav_answer_remainder = (int*)malloc(1 * sizeof(int));
	if (ptri_aav_answer_remainder == NULL)
	{
		printf("Could not Allocate Memory For 'ptri_aav_answer_remainder' .Exiting Now\n");
		exit(0);
	}



	//passing address to a function
	MathematicalOperations(i_aav_a, i_aav_b, ptri_aav_answer_sum, ptri_aav_answer_diffrenece, ptri_aav_answer_product, ptri_aav_answer_questient, ptri_aav_answer_remainder);

	printf("\n\n");
	printf("*****Result****\n");
	printf("Sum = %d\n", *ptri_aav_answer_sum);
	printf("Difference = %d\n", *ptri_aav_answer_diffrenece);
	printf("Product = %d\n", *ptri_aav_answer_product);
	printf("Questient = %d\n", *ptri_aav_answer_questient);
	printf("Remainder =%d\n", *ptri_aav_answer_remainder);


	if (ptri_aav_answer_sum)
	{
		free(ptri_aav_answer_sum);
		ptri_aav_answer_sum = NULL;
		printf("Allocate Memory For 'ptri_aav_answer_sum'Successfully Freed\n");
	}
	if (ptri_aav_answer_diffrenece)
	{
		free(ptri_aav_answer_diffrenece);
		ptri_aav_answer_diffrenece = NULL;
		printf("Allocate Memory For 'ptri_aav_answer_diffrenece'Successfully Freed\n");
	}
	if (ptri_aav_answer_product)
	{
		free(ptri_aav_answer_product);
		ptri_aav_answer_product = NULL;
		printf("Allocate Memory For 'ptri_aav_answer_product'Successfully Freed\n");
	}
	if (ptri_aav_answer_questient)
	{
		free(ptri_aav_answer_questient);
		ptri_aav_answer_questient = NULL;
		printf("Allocate Memory For 'ptri_aav_answer_questient'Successfully Freed\n");
	}
	if (ptri_aav_answer_remainder)
	{
		free(ptri_aav_answer_remainder);
		ptri_aav_answer_remainder = NULL;
		printf("Allocate Memory For 'ptri_aav_answer_remainder'Successfully Freed\n");
	}

	return(0);
}

void MathematicalOperations(int i_aav_x, int i_aav_y, int* pi_aav_sum, int* pi_aav_diffrence, int* pi_aav_product, int* pi_aav_quotient, int* pi_aav_remainder)
{
	//code
	*pi_aav_sum = i_aav_x + i_aav_y;
	*pi_aav_diffrence = i_aav_x - i_aav_y;
	*pi_aav_product = i_aav_x * i_aav_y;
	*pi_aav_quotient = i_aav_x / i_aav_y;
	*pi_aav_remainder = i_aav_x % i_aav_y;

}
