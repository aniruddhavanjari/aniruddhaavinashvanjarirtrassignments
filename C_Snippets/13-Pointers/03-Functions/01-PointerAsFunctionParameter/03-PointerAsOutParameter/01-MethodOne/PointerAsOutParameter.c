#include <stdio.h>

int main(void)
{
	//function declaration
	void MathematicalOperations(int, int ,int *,int *,int *,int *, int *);

	//variable declaration
	int i_aav_a;
	int i_aav_b;
	int i_aav_answer_sum;
	int i_aav_answer_diffrenece;
	int i_aav_answer_product;
	int i_aav_answer_questient;
	int i_aav_answer_remainder;
	
	//code 
	printf("\n\n");
	printf("Enter Valaue for 'A' ");
	scanf("%d", &i_aav_a);

	printf("\n\n");
	printf("Enter Valaue for 'B' ");
	scanf("%d", &i_aav_b);

	MathematicalOperations(i_aav_a, i_aav_b,&i_aav_answer_sum, &i_aav_answer_diffrenece,&i_aav_answer_product,&i_aav_answer_questient,&i_aav_answer_remainder);

	printf("\n\n");
	printf("*****Result****\n");
	printf("Sum = %d\n", i_aav_answer_sum);
	printf("Difference = %d\n", i_aav_answer_diffrenece);
	printf("Product = %d\n", i_aav_answer_product);
	printf("Questient = %d\n", i_aav_answer_questient);
	printf("Remainder =%d\n", i_aav_answer_remainder);

	return(0);
}

void MathematicalOperations(int i_aav_x, int i_aav_y, int *pi_aav_sum, int *pi_aav_diffrence, int *pi_aav_product, int *pi_aav_quotient, int *pi_aav_remainder)
{
	//code
	*pi_aav_sum = i_aav_x + i_aav_y;
	*pi_aav_diffrence = i_aav_x - i_aav_y;
	*pi_aav_product = i_aav_x * i_aav_y;
	*pi_aav_quotient = i_aav_x / i_aav_y;
	*pi_aav_remainder = i_aav_x % i_aav_y;

}
