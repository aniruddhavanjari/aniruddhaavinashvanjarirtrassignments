#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	//Function declaration 
	void MultipyArrayElementsByNumber(int*, int, int);

	//Variable declaration
	int* ptr_aav_Array = NULL;
	int i_aav_NumElements;
	int i_aav_i, i_aav_num;

	//code
	printf("\n\n");
	printf("Enter how Many Elements you want in Interger Array\n");
	scanf("%d", &i_aav_NumElements);

	ptr_aav_Array = (int*)malloc(i_aav_NumElements * sizeof(int));
	if (ptr_aav_Array == NULL)
	{
		printf("Memory Allocation of Integer Array String Failed. Exitting Now\n");
		exit(0);
	}

	printf("\n\n");
	printf("Enter %d Elements for Integer Array :\n", i_aav_NumElements);
	for (i_aav_i = 0; i_aav_i < i_aav_NumElements; i_aav_i++)
		scanf("%d", &ptr_aav_Array[i_aav_i]);

	//One
	printf("\n\n");
	printf("After Before Passing to Function MultiplyArrayElementsByNumber():\n");
	for (i_aav_i = 0; i_aav_i < i_aav_NumElements; i_aav_i++)
		printf("ptr_aav_Array[%d] = %d\n", i_aav_i, ptr_aav_Array[i_aav_i]);

	printf("\n\n");
	printf("Enter the number you want to Multiply Each Elements :\n");
	scanf("%d", &i_aav_num);

	MultipyArrayElementsByNumber(ptr_aav_Array,i_aav_NumElements,i_aav_num);

	printf("\n\n");
	printf("Array Returning By Function MultipyArrayElementsByNumber():\n");

	for (i_aav_i = 0; i_aav_i < i_aav_NumElements; i_aav_i++)
		printf("ptr_aav_Array[%d] = %d \n", i_aav_i, ptr_aav_Array[i_aav_i]);

	if (ptr_aav_Array)
	{
		free(ptr_aav_Array);
		ptr_aav_Array = NULL;
		printf("\n");
		printf("Memory Allocation of ptr_aav_Array has been  Freed\n");
	}

	return(0);
}

void MultipyArrayElementsByNumber(int* arr, int iNumElements, int n)
{
	//variable Declaration
	int i_aav_i;

	//code
	for (i_aav_i = 0; i_aav_i < iNumElements; i_aav_i++)
		arr[i_aav_i] = arr[i_aav_i] * n;

}