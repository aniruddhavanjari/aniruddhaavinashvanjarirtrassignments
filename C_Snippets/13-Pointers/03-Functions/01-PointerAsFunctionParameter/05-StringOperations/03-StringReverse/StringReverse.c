#include <stdio.h>
#include <stdlib.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//Fucntion prototype
	void MyStrrev(char*, char*);
	int MyStrlen(char*);

	//variable declaration
	char* ptr_aav_orignal = NULL, * ptr_aav_copy = NULL;
	int i_aav_OrignalStringLen = 0;

	//Code
	printf("\n");
	ptr_aav_orignal = (char*)malloc(MAX_STRING_LENGTH * sizeof(char));
	if (ptr_aav_orignal == NULL)
	{
		printf("Memory Allocation of Orignal String Failed. Exitting Now\n");
		exit(0);
	}

	//String Input
	printf("Enter A String :\n");
	gets_s(ptr_aav_orignal, MAX_STRING_LENGTH);

	i_aav_OrignalStringLen = MyStrlen(ptr_aav_orignal);

	ptr_aav_copy = (char*)malloc(i_aav_OrignalStringLen * sizeof(char));
	if (ptr_aav_copy == NULL)
	{
		printf("Memory Allocation of Reverse String Failed. Exitting Now\n");
		exit(0);
	}

	//String Copy
	MyStrrev(ptr_aav_copy, ptr_aav_orignal);

	//String outPut
	printf("\n");
	printf("String Entered By You is :\n");
	printf("%s\n", ptr_aav_orignal);

	printf("\n");
	printf("String Reverse is :\n");
	printf("%s\n", ptr_aav_copy);


	if (ptr_aav_orignal)
	{
		free(ptr_aav_orignal);
		ptr_aav_orignal = NULL;
	}
	if (ptr_aav_copy)
	{
		free(ptr_aav_copy);
		ptr_aav_copy = NULL;
	}
	return(0);
}

void MyStrrev(char* str_destination, char* str_source)
{
	//Fuction declartion
	int MyStrlen(char*);

	//varaible declarations
	int i_aav_Stringlength = 0;
	int i_aav_i, i_aav_j, i_aav_len;
	//code
	i_aav_Stringlength = MyStrlen(str_source);
	i_aav_len = i_aav_Stringlength - 1;
	for (i_aav_i = 0, i_aav_j = i_aav_len; i_aav_i < i_aav_Stringlength, i_aav_j >= 0; i_aav_i++, i_aav_j--)
	{
		*(str_destination + i_aav_i) = *(str_source + i_aav_j);
	}
	*(str_destination + i_aav_i) = '\0';
}

int MyStrlen(char* str)
{
	//variable declaration
	int i_aav_j;
	int i_aav_stringLen = 0;

	//code
	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (*(str + i_aav_j) == '\0')
			break;
		i_aav_stringLen++;
	}
	return(i_aav_stringLen);
}
