#include <stdio.h>
#include <stdlib.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//Fucntion prototype
	int MyStrlen(char*);

	//variable declaration
	char* ptr_aav_cArray = NULL;
	int i_aav_StringLen = 0;

	//Code
	printf("\n");
	ptr_aav_cArray = (char*)malloc(MAX_STRING_LENGTH * sizeof(char));
	if (ptr_aav_cArray == NULL)
	{
		printf("Memory Allocation of character Array Failed. Exiting Now\n");
		exit(0);
	}

	//String Input
	printf("Enter A String :\n");
	gets_s(ptr_aav_cArray, MAX_STRING_LENGTH);

	//String outPut
	printf("\n");
	printf("String Entered By You is :\n");
	printf("%s\n", ptr_aav_cArray);

	//String Length
	printf("\n");
	i_aav_StringLen = MyStrlen(ptr_aav_cArray);
	printf("Length of String is : %d \n", i_aav_StringLen);

	if (ptr_aav_cArray)
	{
		free(ptr_aav_cArray);
		ptr_aav_cArray = NULL;
	}

	return(0);
}

int MyStrlen(char* str)
{
	//variable declaration
	int i_aav_j;
	int i_aav_stringLen = 0;

	//code
	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (*(str + i_aav_j) == '\0')
			break;
		i_aav_stringLen++;
	}
	return(i_aav_stringLen);
}
