#include <stdio.h>
#include <stdlib.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//Fucntion prototype
	void MyStrcat(char*, char*);
	int MyStrlen(char*);

	//variable declaration
	char* ptr_aav_one = NULL, * ptr_aav_two = NULL;
	

	//Code
	printf("\n");
	ptr_aav_one = (char*)malloc(MAX_STRING_LENGTH * sizeof(char));
	if (ptr_aav_one == NULL)
	{
		printf("Memory Allocation of First String Failed. Exitting Now\n");
		exit(0);
	}

	//String Input
	printf("Enter A First String :\n");
	gets_s(ptr_aav_one, MAX_STRING_LENGTH);


	ptr_aav_two = (char*)malloc(MAX_STRING_LENGTH * sizeof(char));
	if (ptr_aav_two == NULL)
	{
		printf("Memory Allocation of Second String Failed. Exitting Now\n");
		exit(0);
	}
	printf("\n\n");
	printf("Enter A Second String :\n");
	gets_s(ptr_aav_two, MAX_STRING_LENGTH);

	
	//String outPut

	printf("\n");
	printf("Before Concatenation\n");
	printf("String First Entered By You is :\n");
	printf("%s\n", ptr_aav_one);

	printf("\n");
	printf("String Second Reverse is :\n");
	printf("%s\n", ptr_aav_two);

	//String Concatenation
	MyStrcat(ptr_aav_one, ptr_aav_two);

	printf("\n");
	printf("After Concatenation\n");
	printf("String First is :\n");
	printf("%s\n", ptr_aav_one);

	printf("\n");
	printf("String Second is :\n");
	printf("%s\n", ptr_aav_two);
	printf("\n\n");

	if (ptr_aav_one)
	{
		free(ptr_aav_one);
		ptr_aav_one = NULL;
		printf("Memory Allocated for String One has Successfully Freed\n");
	}
	if (ptr_aav_two)
	{
		free(ptr_aav_two);
		ptr_aav_two = NULL;
		printf("Memory Allocated for String Second has Successfully Freed\n");
	}
	return(0);
}

void MyStrcat(char* str_destination, char* str_source)
{
	//Fuction declartion
	int MyStrlen(char*);

	//varaible declarations
	int i_aav_StringlengthS = 0, i_aav_StringlengthD = 0;
	int i_aav_i, i_aav_j;
	//code
	i_aav_StringlengthS = MyStrlen(str_source);
	i_aav_StringlengthD = MyStrlen(str_destination);
	for (i_aav_i = i_aav_StringlengthD, i_aav_j = 0; i_aav_j < i_aav_StringlengthS ; i_aav_i++, i_aav_j++)
	{
		*(str_destination + i_aav_i) = *(str_source + i_aav_j);
	}
	*(str_destination + i_aav_i) = '\0';
}

int MyStrlen(char* str)
{
	//variable declaration
	int i_aav_j;
	int i_aav_stringLen = 0;

	//code
	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (*(str + i_aav_j) == '\0')
			break;
		i_aav_stringLen++;
	}
	return(i_aav_stringLen);
}
