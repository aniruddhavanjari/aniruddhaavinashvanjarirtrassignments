#include <stdio.h>

int main(void)
{
	//function declaration
	void SwapNumbers(int, int);

	//variable declaration
	int i_aav_a;
	int i_aav_b;

	//code 
	printf("\n\n");
	printf("Enter Valaue for 'A' ");
	scanf("%d", &i_aav_a);

	printf("\n\n");
	printf("Enter Valaue for 'B' ");
	scanf("%d", &i_aav_b);

	printf("\n\n");
	printf("*******Before Swapping*******\n");
	printf("Value of 'A' = %d\n", i_aav_a);
	printf("Value of 'B' = %d\n", i_aav_b);

	SwapNumbers(i_aav_a, i_aav_b);

	printf("\n\n");
	printf("*******After Swapping*******\n");
	printf("Value of 'A' = %d\n", i_aav_a);
	printf("Value of 'B' = %d\n", i_aav_b);

	return(0);
}

void SwapNumbers(int i_aav_x, int i_aav_y) // In this Fuction only value of x and y value are getting swap , not a and b. Pass by Value 
{
	//varaible declaration 
	int temp;

	printf("\n");
	printf("\n\n");
	printf("*******Before Swapping*******\n");
	printf("Value of 'x' = %d\n", i_aav_x);
	printf("Value of 'y' = %d\n", i_aav_y);

	temp = i_aav_x;
	i_aav_x = i_aav_y;
	i_aav_y = temp;

	printf("\n\n");
	printf("*******After Swapping*******\n");
	printf("Value of 'x' = %d\n", i_aav_x);
	printf("Value of 'y' = %d\n", i_aav_y);

}