#include <stdio.h>

int main(void)
{
	//variable declaration
	int i_aav_Array[] = {34,46,47,456,345,3,62,67,12,10};

	float f_aav_Array[] = { 4.6f, 25.09f,6.6f,4.7f,7.3f };

	double d_aav_Array[] = { 10.2262,5.23526,6.6223 };

	char c_aav_Array[] = {'A','S','T','R','O','M','E','D','I','C','O','M','P','\0'};

	//code
	printf("\n");
	printf("Integer Array Elements And The Addresses :\n");

	printf("i_aav_Array[0] = %d \t At Address :%p\n", *(i_aav_Array + 0), (i_aav_Array + 0));
	printf("i_aav_Array[1] = %d \t At Address :%p\n", *(i_aav_Array + 1), (i_aav_Array + 1));
	printf("i_aav_Array[2] = %d \t At Address :%p\n", *(i_aav_Array + 2), (i_aav_Array + 2));
	printf("i_aav_Array[3] = %d \t At Address :%p\n", *(i_aav_Array + 3), (i_aav_Array + 3));
	printf("i_aav_Array[4] = %d \t At Address :%p\n", *(i_aav_Array + 4), (i_aav_Array + 4));
	printf("i_aav_Array[5] = %d \t At Address :%p\n", *(i_aav_Array + 5), (i_aav_Array + 5));
	printf("i_aav_Array[6] = %d \t At Address :%p\n", *(i_aav_Array + 6), (i_aav_Array + 6));
	printf("i_aav_Array[7] = %d \t At Address :%p\n", *(i_aav_Array + 7), (i_aav_Array + 7));
	printf("i_aav_Array[8] = %d \t At Address :%p\n", *(i_aav_Array + 8), (i_aav_Array + 8));
	printf("i_aav_Array[9] = %d \t At Address :%p\n", *(i_aav_Array + 9), (i_aav_Array + 9));
	
	printf("\n");
	printf("Float Array Elements And The Adrresses :\n");
	printf("f_aav_Array[0] = %f \t At Address :%p\n", *(f_aav_Array + 0), (f_aav_Array + 0));
	printf("f_aav_Array[1] = %f \t At Address :%p\n", *(f_aav_Array + 1), (f_aav_Array + 1));
	printf("f_aav_Array[2] = %f \t At Address :%p\n", *(f_aav_Array + 2), (f_aav_Array + 2));
	printf("f_aav_Array[3] = %f \t At Address :%p\n", *(f_aav_Array + 3), (f_aav_Array + 3));
	printf("f_aav_Array[4] = %f \t At Address :%p\n", *(f_aav_Array + 4), (f_aav_Array + 4));

	printf("\n");
	printf("Double Array Elements And The Address :\n");
	printf("d_aav_Array[0] = %lf \t At Address :%p\n", *(d_aav_Array + 0), (d_aav_Array + 0));
	printf("d_aav_Array[0] = %lf \t At Address :%p\n", *(d_aav_Array + 1), (d_aav_Array + 1));
	printf("d_aav_Array[0] = %lf \t At Address :%p\n", *(d_aav_Array + 2), (d_aav_Array + 2));
	
	printf("\n");
	printf("Character Array Elements And The Address :\n");
	printf("c_aav_Array[0] = %c \t At Address :%p\n", *(c_aav_Array + 0), (c_aav_Array + 0));
	printf("c_aav_Array[1] = %c \t At Address :%p\n", *(c_aav_Array + 1), (c_aav_Array + 1));
	printf("c_aav_Array[2] = %c \t At Address :%p\n", *(c_aav_Array + 2), (c_aav_Array + 2));
	printf("c_aav_Array[3] = %c \t At Address :%p\n", *(c_aav_Array + 3), (c_aav_Array + 3));
	printf("c_aav_Array[4] = %c \t At Address :%p\n", *(c_aav_Array + 4), (c_aav_Array + 4));
	printf("c_aav_Array[5] = %c \t At Address :%p\n", *(c_aav_Array + 5), (c_aav_Array + 5));
	printf("c_aav_Array[6] = %c \t At Address :%p\n", *(c_aav_Array + 6), (c_aav_Array + 6));
	printf("c_aav_Array[7] = %c \t At Address :%p\n", *(c_aav_Array + 7), (c_aav_Array + 7));
	printf("c_aav_Array[8] = %c \t At Address :%p\n", *(c_aav_Array + 8), (c_aav_Array + 8));
	printf("c_aav_Array[9] = %c \t At Address :%p\n", *(c_aav_Array + 9), (c_aav_Array + 9));
	printf("c_aav_Array[10] = %c \t At Address :%p\n", *(c_aav_Array + 10), (c_aav_Array + 10));
	printf("c_aav_Array[11] = %c \t At Address :%p\n", *(c_aav_Array + 11), (c_aav_Array + 11));
	printf("c_aav_Array[12] = %c \t At Address :%p\n", *(c_aav_Array + 12), (c_aav_Array + 12));

	return(0);

}