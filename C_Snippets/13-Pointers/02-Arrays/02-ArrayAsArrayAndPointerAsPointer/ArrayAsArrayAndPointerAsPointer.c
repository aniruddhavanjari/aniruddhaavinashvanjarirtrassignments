#include <stdio.h>

int main(void)
{
	//variable declaration
	int i_aav_Array[] = { 111,122,133,144,155,166,177,188,199,200 };
	int* pi_aav_Array = NULL;
	//code
	printf("\n");
	printf("Using Array Name of Array : Printing Value and Address of 10 Elements\n");
	printf("Integer Array Value And Address\n");
	printf("i_aav_Array[0] = %d \t At Address &i_aav_Array[0] :%p \n", i_aav_Array[0], &i_aav_Array[0]);
	printf("i_aav_Array[1] = %d \t At Address &i_aav_Array[1] :%p \n", i_aav_Array[1], &i_aav_Array[1]);
	printf("i_aav_Array[2] = %d \t At Address &i_aav_Array[2] :%p \n", i_aav_Array[2], &i_aav_Array[2]);
	printf("i_aav_Array[3] = %d \t At Address &i_aav_Array[3] :%p \n", i_aav_Array[3], &i_aav_Array[3]);
	printf("i_aav_Array[4] = %d \t At Address &i_aav_Array[4] :%p \n", i_aav_Array[4], &i_aav_Array[4]);
	printf("i_aav_Array[5] = %d \t At Address &i_aav_Array[5] :%p \n", i_aav_Array[5], &i_aav_Array[5]);
	printf("i_aav_Array[6] = %d \t At Address &i_aav_Array[6] :%p \n", i_aav_Array[6], &i_aav_Array[6]);
	printf("i_aav_Array[7] = %d \t At Address &i_aav_Array[7] :%p \n", i_aav_Array[7], &i_aav_Array[7]);
	printf("i_aav_Array[8] = %d \t At Address &i_aav_Array[8] :%p \n", i_aav_Array[8], &i_aav_Array[8]);
	printf("i_aav_Array[9] = %d \t At Address &i_aav_Array[9] :%p \n", i_aav_Array[9], &i_aav_Array[9]);

	
	pi_aav_Array = i_aav_Array;
	printf("\n");
	printf("Using Pointer :Integer Array Elements And The Addresses :\n");

	printf("*(pi_aav_Array + 0) = %d \t At Address (pi_aav_Array + 0) :%p \n", *(pi_aav_Array + 0), (pi_aav_Array + 0));
	printf("*(pi_aav_Array + 1) = %d \t At Address (pi_aav_Array + 1) :%p \n", *(pi_aav_Array + 1), (pi_aav_Array + 1));
	printf("*(pi_aav_Array + 2) = %d \t At Address (pi_aav_Array + 2) :%p \n", *(pi_aav_Array + 2), (pi_aav_Array + 2));
	printf("*(pi_aav_Array + 3) = %d \t At Address (pi_aav_Array + 3) :%p \n", *(pi_aav_Array + 3), (pi_aav_Array + 3));
	printf("*(pi_aav_Array + 4) = %d \t At Address (pi_aav_Array + 4) :%p \n", *(pi_aav_Array + 4), (pi_aav_Array + 4));
	printf("*(pi_aav_Array + 5) = %d \t At Address (pi_aav_Array + 5) :%p \n", *(pi_aav_Array + 5), (pi_aav_Array + 5));
	printf("*(pi_aav_Array + 6) = %d \t At Address (pi_aav_Array + 6) :%p \n", *(pi_aav_Array + 6), (pi_aav_Array + 6));
	printf("*(pi_aav_Array + 7) = %d \t At Address (pi_aav_Array + 7) :%p \n", *(pi_aav_Array + 7), (pi_aav_Array + 7));
	printf("*(pi_aav_Array + 8) = %d \t At Address (pi_aav_Array + 8) :%p \n", *(pi_aav_Array + 8), (pi_aav_Array + 8));
	printf("*(pi_aav_Array + 9) = %d \t At Address (pi_aav_Array + 9) :%p \n", *(pi_aav_Array + 9), (pi_aav_Array + 9));

	return(0);
}
