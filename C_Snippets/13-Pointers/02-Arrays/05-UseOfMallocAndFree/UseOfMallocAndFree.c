#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int *ptr_aav_Array = NULL;

	unsigned int i_aav_ArrayLength = 0;

	int i_aav_i;

	printf("\n");
	printf("Enter the Number of Elements You Want In Your Interger Array :\n");
	scanf("%d", &i_aav_ArrayLength);

	ptr_aav_Array = (int*)malloc(sizeof(int) * i_aav_ArrayLength);

	if (ptr_aav_Array == NULL)
	{
		printf("\n\n");
		printf("Memory Allocation For Integer Array has Failed .Exiting Now..\n");
		exit(0);
	}
	else
	{
		printf("\n");
		printf("Memory Allocation For Integer Array has Success...\n");
		printf("Memory Address From %p to %p have been Allocated to Integer Array \n",
			ptr_aav_Array, (ptr_aav_Array + (i_aav_ArrayLength - 1)));
	}
	printf("\n");
	printf("Enter %d Elements for the Integer Array :\n", i_aav_ArrayLength);
	for (i_aav_i = 0; i_aav_i < i_aav_ArrayLength; i_aav_i++)
		scanf("%d", (ptr_aav_Array + i_aav_i));
	printf("\n");
	printf("The Integer Array Entered By You , Consisting of %d Elements:\n", i_aav_ArrayLength);

	for (i_aav_i = 0; i_aav_i < i_aav_ArrayLength; i_aav_i++)
	{
		printf("ptr_aav_Array[%d] = %d \t At Address &ptr_aav_Array[%d] :%p\n", i_aav_i, ptr_aav_Array[i_aav_i], i_aav_i, &ptr_aav_Array[i_aav_i]);
	}

	if (ptr_aav_Array)
	{
		free(ptr_aav_Array);
		ptr_aav_Array = NULL;
		printf("\n");
		printf("Memory Allocated for Integer Array Has been Successfully freed\n");
	}
	return(0);
}