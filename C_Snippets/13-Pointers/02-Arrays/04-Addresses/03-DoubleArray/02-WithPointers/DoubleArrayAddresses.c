#include <stdio.h>

int main(void)
{
	double d_aav_Array[10];
	double *pd_aav_Array = NULL;
	int i_aav_i;

	//code
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		d_aav_Array[i_aav_i] = (double)(i_aav_i + 1) * 3.83526f;
	}

	//Assigning base Address of Array to pointer
	pd_aav_Array = d_aav_Array; // pf_aav_Array = &f_aav_Array[0];

	printf("\n");
	printf("Elements of the Double Array :\n\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("d_aav_Array[%d] = %lf\n", i_aav_i, *(pd_aav_Array + i_aav_i));
	}

	printf("\n");
	printf("Elements of the Double Array And Address :\n\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("d_aav_Array[%d] = %lf \t Address = %p\n", i_aav_i, *(pd_aav_Array + i_aav_i), (pd_aav_Array + i_aav_i));
	}
	printf("\n\n");
	return(0);
}