#include <stdio.h>

int main(void)
{
	//variable declaration
	double d_aav_Array[10];
	int i_aav_i;

	//code
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		d_aav_Array[i_aav_i] = (double)(i_aav_i + 1) * 2.4363f;
	}

	printf("\n");
	printf("Elements of the double Array :\n\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("d_aav_Array[%d] = %lf\n", i_aav_i, d_aav_Array[i_aav_i]);
	}

	printf("\n");
	printf("Elements of the double Array And Address :\n\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("d_aav_Array[%d] = %lf \t Address = %p\n", i_aav_i, d_aav_Array[i_aav_i], &d_aav_Array[i_aav_i]);
	}
	printf("\n\n");
	return(0);
}