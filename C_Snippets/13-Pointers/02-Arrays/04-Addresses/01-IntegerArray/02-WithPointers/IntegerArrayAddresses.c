#include <stdio.h>

int main(void)
{
	int i_aav_Array[10];
	int* pi_aav_Array = NULL;
	int i_aav_i;

	//code
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		i_aav_Array[i_aav_i] = (i_aav_i + 1) * 3;
	}

	//Assigning base Address of Array to pointer
	pi_aav_Array = i_aav_Array;

	printf("\n");
	printf("Elements of the Interger Array :\n\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("i_aav_Array[%d] = %d\n", i_aav_i, *(pi_aav_Array + i_aav_i));
	}

	printf("\n");
	printf("Elements of the Interger Array And Address :\n\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("i_aav_Array[%d] = %d \t Address = %p\n", i_aav_i, *(pi_aav_Array + i_aav_i), (pi_aav_Array+ i_aav_i));
	}
	printf("\n\n");
	return(0);
}