#include <stdio.h>

int main(void)
{
	float f_aav_Array[10];
	int i_aav_i;

	//code
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		f_aav_Array[i_aav_i] = (float)(i_aav_i + 1) * 1.5f;
	}

	printf("\n");
	printf("Elements of the float Array :\n\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("f_aav_Array[%d] = %f\n", i_aav_i, f_aav_Array[i_aav_i]);
	}

	printf("\n");
	printf("Elements of the float Array And Address :\n\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("f_aav_Array[%d] = %f \t Address = %p\n", i_aav_i, f_aav_Array[i_aav_i], &f_aav_Array[i_aav_i]);
	}
	printf("\n\n");
	return(0);
}