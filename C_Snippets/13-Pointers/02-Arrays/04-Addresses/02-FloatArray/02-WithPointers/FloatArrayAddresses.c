#include <stdio.h>

int main(void)
{
	float f_aav_Array[10];
	float *pf_aav_Array = NULL;
	int i_aav_i;

	//code
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		f_aav_Array[i_aav_i] = (float)(i_aav_i + 1) * 1.5f;
	}

	//Assigning base Address of Array to pointer
	pf_aav_Array = f_aav_Array; // pf_aav_Array = &f_aav_Array[0];

	printf("\n");
	printf("Elements of the float Array :\n\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("f_aav_Array[%d] = %f\n", i_aav_i, *(pf_aav_Array + i_aav_i));
	}

	printf("\n");
	printf("Elements of the flaot Array And Address :\n\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("f_aav_Array[%d] = %f \t Address = %p\n", i_aav_i, *(pf_aav_Array + i_aav_i), (pf_aav_Array + i_aav_i));
	}
	printf("\n\n");
	return(0);
}