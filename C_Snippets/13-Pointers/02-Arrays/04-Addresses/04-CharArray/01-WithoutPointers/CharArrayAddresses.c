#include <stdio.h>

int main(void)
{

	//variable declaration
	char c_aav_Array[10];
	int i_aav_i;

	//Code
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		c_aav_Array[i_aav_i] = (char)(i_aav_i + 65);
	}

	printf("\n\n");
	printf("Elements of Character Array:\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("c_aav_Array[%d] = %c\n", i_aav_i, c_aav_Array[i_aav_i]);
	}

	printf("\n\n");
	printf("Elements of Character Array:\n");
	for (i_aav_i = 0; i_aav_i < 10; i_aav_i++)
	{
		printf("c_aav_Array[%d] = %c \t Address = %p\n", i_aav_i, c_aav_Array[i_aav_i], &c_aav_Array[i_aav_i]);
	}
	printf("\n\n");

	return(0);

}