#include <stdio.h>
#include <stdlib.h> // FOR exit()

//Global Macro
#define NUM_ROWS 5
#define NUM_COLUMNS_ONE 3
#define NUM_COLUMNS_TWO 8


int main(void)
{
	//Variable Declarations
	int* ptr_aav_iArray[NUM_ROWS];
	int i_aav_i,i_aav_j;

	//Code
	printf("\n");
	printf("First Memory Allocation of 2D Integer Array\n");
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		ptr_aav_iArray[i_aav_i] = (int*)malloc(NUM_COLUMNS_ONE * sizeof(int));
		if (ptr_aav_iArray[i_aav_i] == NULL)
		{
			printf("Faileded to Allocated Memoory to Row %d of 2D Integer Array Exiting Now\n ", i_aav_i);
			exit(0);
		}
		else
			printf("Memory Allocation to Row %d of of 2D Integer Array Succeeded \n", i_aav_i);
	}

	//Assigning Value to 2D Array
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS_ONE; i_aav_j++)
		{
			ptr_aav_iArray[i_aav_i][i_aav_j] = (i_aav_i + 1) * (i_aav_j + 1);
		}
	}

	//Displaying 2D Array 
	printf("\n");
	printf("Displaying 2D Array : \n");
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS_ONE; i_aav_j++)
		{
			printf("ptr_aav_iArray[%d][%d] = %d\n", i_aav_i, i_aav_j, ptr_aav_iArray[i_aav_i][i_aav_j]);
		}
		printf("\n");
	}
	printf("\n");

	//Freeing Memeory Assigned to 2D array 
	for (i_aav_i = (NUM_ROWS - 1); i_aav_i >= 0; i_aav_i--)
	{
		free(ptr_aav_iArray[i_aav_i]);
		ptr_aav_iArray[i_aav_i] = NULL;
		printf("Memory Allocated to Row %d of 2D Integer Array has Been Successfully Freed \n\n", i_aav_i);
	}

	printf("\n\n");
	printf("Second Memory Allocatioin to 2D array\n");
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		ptr_aav_iArray[i_aav_i] = (int*)malloc(NUM_COLUMNS_TWO * sizeof(int));
		if (ptr_aav_iArray[i_aav_i] == NULL)
		{
			printf("Faileded to Allocated Memoory to Row %d of 2D Integer Array Exiting Now\n ", i_aav_i);
			exit(0);
		}
		else
			printf("Memory Allocation to Row %d of of 2D Integer Array Succeeded \n", i_aav_i);
	}

	//Assigning Value to 2D Array
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS_TWO; i_aav_j++)
		{
			ptr_aav_iArray[i_aav_i][i_aav_j] = (i_aav_i + 1) * (i_aav_j + 1);
		}
	}

	//Displaying 2D Array 
	printf("\n");
	printf("Displaying 2D Array : \n");
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS_TWO; i_aav_j++)
		{
			printf("ptr_aav_iArray[%d][%d] = %d\n", i_aav_i, i_aav_j, ptr_aav_iArray[i_aav_i][i_aav_j]);
		}
		printf("\n");
	}
	printf("\n");

	//Freeing Memeory Assigned to 2D array 
	for (i_aav_i = (NUM_ROWS - 1); i_aav_i >= 0; i_aav_i--)
	{
		free(ptr_aav_iArray[i_aav_i]);
		ptr_aav_iArray[i_aav_i] = NULL;
		printf("Memory Allocated to Row %d of 2D Integer Array has Been Successfully Freed \n\n", i_aav_i);
	}

	return(0);

}