#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	//variable declaration
	int **pptr_aav_iArray = NULL; //Using Double Pinter 
	int i_aav_i, i_aav_j;
	int i_aav_numrows, i_aav_numcolumns;

	//Code
	printf("\n");
	printf("Enter Number of Rows :");
	scanf("%d", &i_aav_numrows);

	printf("Enter Number of Columns :");
	scanf("%d", &i_aav_numcolumns);

	//Alocting Memory
	pptr_aav_iArray = (int**)malloc(i_aav_numrows * sizeof(int*));

	if (pptr_aav_iArray == NULL)
	{
		printf("Memory Allocation of the 1D Array of base Address of %d Rows Failed. Exiting Now\n", i_aav_numrows);
		exit(0);
	}
	else
		printf("Memory Allocation of the 1D Array of base Address of %d Rows has Succeeded\n",i_aav_numrows);
	//Alocation Ememory to Each Row
	for (i_aav_i = 0; i_aav_i < i_aav_numrows; i_aav_i++)
	{
		pptr_aav_iArray[i_aav_i] = (int*)malloc(i_aav_numcolumns * sizeof(int));
		if (pptr_aav_iArray == NULL)
		{
			printf("Memory Allocation of the 1D Array of base Address of %d  Columsn Failed. Exiting Now\n", i_aav_i);
			exit(0);
		}
		else
		{
			printf("Memory Allocation of the 1D Array of base Address of %d Columsn has Succeeded.\n", i_aav_i);
		}
	}

	//Assignationg Value 
	for (i_aav_i = 0; i_aav_i < i_aav_numrows; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < i_aav_numcolumns; i_aav_j++)
		{
			*(*(pptr_aav_iArray + i_aav_i) + i_aav_j) = (i_aav_i + 1) * (i_aav_j + 1); // Similar As  pptr_aav_iArray[i_aav_i][i_aav_j] = (i_aav_i  + 1)*(i_aav_j + 1)
		}
	}


	//Displaying Value  
	for (i_aav_i = 0; i_aav_i < i_aav_numrows; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < i_aav_numcolumns; i_aav_j++)
		{
			printf("pptr_aav_iArray[%d][%d]  = %d \t At Address &pptr_aav_iArray[%d][%d] :%p\n", i_aav_i, i_aav_j, pptr_aav_iArray[i_aav_i][i_aav_j], i_aav_i, i_aav_j, &pptr_aav_iArray[i_aav_i][i_aav_j]);

		}
		printf("\n\n");
	}

	//Freeing Allocation of Memorry 
	for (i_aav_i = (i_aav_numrows - 1); i_aav_i >= 0; i_aav_i--)
	{
		if (*(pptr_aav_iArray + i_aav_i))
		{
			free(*(pptr_aav_iArray + i_aav_i));
			*(pptr_aav_iArray + i_aav_i) = NULL;
			printf("Memory Alocated to Row %d has been successfully freed\n\n", i_aav_i);
		}
	}

	if (pptr_aav_iArray)
	{
		free(pptr_aav_iArray);
		pptr_aav_iArray = NULL;
		printf("Memory Alocated to pptr_aav_iArary has been successfully freed\n\n");
	}

	return(0);

}