#include <stdio.h>
#include <stdlib.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	//variable declarations
	int i_aav_Array[NUM_ROWS][NUM_COLUMNS];
	int i_aav_i, i_aav_j;

	//code
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS; i_aav_j++)
			*(i_aav_Array[i_aav_i]+i_aav_j) = (i_aav_i + 1) * (i_aav_j + 1);
	}

	printf("\n");
	printf("2D Integer Array Elements Along with Addresses :\n");
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS; i_aav_j++)
		{
			printf("i_aav_Array[%d][%d] = %d \t At Address : %p \n", i_aav_i, i_aav_j, *(i_aav_Array[i_aav_i]+i_aav_j), (i_aav_Array[i_aav_i]+i_aav_j));
		}
		printf("\n\n");
	}
	return(0);
}
