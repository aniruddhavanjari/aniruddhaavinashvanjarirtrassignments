#include <stdio.h>
#include <stdlib.h>

#define INT_SIZE sizeof(int)
#define FLOAT_SIZE sizeof(float)
#define DOUBLE_SIZE sizeof(double)
#define CHAR_SIZE sizeof(char)

int main(void)
{
	//variable declaration
	int *ptr_aav_iArray = NULL;
	unsigned int i_aav_intArrayLength = 0;

	float *ptr_aav_fArray = NULL;
	unsigned int i_aav_floatArrayLength = 0; 

	double *ptr_aav_dArray = NULL;
	unsigned int i_aav_doubleArrayLength = 0;

	char *ptr_aav_cArray = NULL;
	unsigned int i_aav_charArrayLength = 0;

	int i_aav_i;

	//Code
	printf("\n");
	printf("Enter the Nubmer of Elments in Integer Array :\n");
	scanf("%u", &i_aav_intArrayLength);

	ptr_aav_iArray = (int*)malloc(INT_SIZE * i_aav_intArrayLength);
	if (ptr_aav_iArray == NULL)
	{
		printf("\n");
		printf("Memory Allocation For Integer Array Falied Exiting Now \n");
		exit(0);
	}
	else
	{
		printf("\n");
		printf("Memory Allocation for Integer Array Succeded \n");
	}

	printf("\n");
	printf("Enter the %d Integer Elements To Fill Up The Interger Array:\n", i_aav_intArrayLength);
	for (i_aav_i = 0; i_aav_i < i_aav_intArrayLength; i_aav_i++)
		scanf("%d", (ptr_aav_iArray + i_aav_i));

	//******Float Array ************
	printf("\n");
	printf("Enter the Nubmer of Elments in float Array :\n");
	scanf("%u", &i_aav_floatArrayLength);

	ptr_aav_fArray = (float*)malloc(FLOAT_SIZE * i_aav_floatArrayLength);
	if (ptr_aav_fArray == NULL)
	{
		printf("\n");
		printf("Memory Allocation For float Array Falied Exiting Now \n");
		exit(0);
	}
	else
	{
		printf("\n");
		printf("Memory Allocation for float Array Succeded \n");
	}

	printf("\n");
	printf("Enter the %d float Elements To Fill Up The float Array:\n", i_aav_floatArrayLength);
	for (i_aav_i = 0; i_aav_i < i_aav_floatArrayLength; i_aav_i++)
		scanf("%f", (ptr_aav_fArray + i_aav_i));

	//*********Double Array ***************
	printf("\n");
	printf("Enter the Nubmer of Elments in double Array :\n");
	scanf("%u", &i_aav_doubleArrayLength);

	ptr_aav_dArray = (double*)malloc(DOUBLE_SIZE * i_aav_doubleArrayLength);
	if (ptr_aav_dArray == NULL)
	{
		printf("\n");
		printf("Memory Allocation For double Array Falied Exiting Now \n");
		exit(0);
	}
	else
	{
		printf("\n");
		printf("Memory Allocation for double Array Succeded \n");
	}

	printf("\n");
	printf("Enter the %d double Elements To Fill Up The double Array:\n", i_aav_doubleArrayLength);
	for (i_aav_i = 0; i_aav_i < i_aav_doubleArrayLength; i_aav_i++)
		scanf("%lf", (ptr_aav_dArray + i_aav_i));

	//**********Char Array **********
	printf("\n");
	printf("Enter the Nubmer of Elments in charater Array :\n");
	scanf("%u", &i_aav_charArrayLength);

	ptr_aav_cArray = (char*)malloc(CHAR_SIZE * i_aav_charArrayLength);
	if (ptr_aav_cArray == NULL)
	{
		printf("\n");
		printf("Memory Allocation For character Array Falied Exiting Now \n");
		exit(0);
	}
	else
	{
		printf("\n");
		printf("Memory Allocation for character Array Succeded \n");
	}

	printf("\n");
	printf("Enter the %d character Elements To Fill Up The character Array:\n", i_aav_charArrayLength);
	for (i_aav_i = 0; i_aav_i < i_aav_charArrayLength; i_aav_i++)
	{
		*(ptr_aav_cArray + i_aav_i) = getch();
		printf("%c\n", *(ptr_aav_cArray + i_aav_i));
	}

	//**************Display of Arrays

	//************Integer Array***************
	printf("\n");
	printf("The Integer Array Entered By You And Consisting of %u Elements Is As Follows :\n", i_aav_intArrayLength);

	for (i_aav_i = 0; i_aav_i < i_aav_intArrayLength; i_aav_i++)
		printf("%d \t at Address :%p \n", *(ptr_aav_iArray + i_aav_i), (ptr_aav_iArray + i_aav_i));

	//*************Float Array
	printf("\n");
	printf("The float Array Entered By You And Consisting of %u Elements Is As Follows :\n", i_aav_floatArrayLength);

	for (i_aav_i = 0; i_aav_i < i_aav_floatArrayLength; i_aav_i++)
		printf("%f \t at Address :%p \n", *(ptr_aav_fArray + i_aav_i), (ptr_aav_fArray + i_aav_i));

	//*********Double Array
	printf("\n");
	printf("The double Array Entered By You And Consisting of %u Elements Is As Follows :\n", i_aav_doubleArrayLength);

	for (i_aav_i = 0; i_aav_i < i_aav_doubleArrayLength; i_aav_i++)
		printf("%lf \t at Address :%p \n", *(ptr_aav_dArray + i_aav_i), (ptr_aav_dArray + i_aav_i));

	//**********chracter Array
	printf("\n");
	printf("The Character Array Entered By You And Consisting of %u Elements Is As Follows :\n", i_aav_charArrayLength);

	for (i_aav_i = 0; i_aav_i < i_aav_charArrayLength; i_aav_i++)
		printf("%c \t at Address :%p \n", *(ptr_aav_cArray + i_aav_i), (ptr_aav_cArray + i_aav_i));
	return(0);
}

