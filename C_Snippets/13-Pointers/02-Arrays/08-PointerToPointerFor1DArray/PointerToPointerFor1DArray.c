#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	//function declaration
	void MyAlloc(int** ptr, unsigned int numberofElements);

	//variavle declarations
	int* ptr_aav_Array = NULL;
	unsigned int ui_aav_numElements;
	int i_aav_i;

	//code
	printf("\n");
	printf("How Manay Elemtsn you want in Integer Array:\n");
	scanf("%u", &ui_aav_numElements);

	printf("\n");
	MyAlloc(&ptr_aav_Array, ui_aav_numElements);

	printf("Enter %u Elements to Fill up Integer Array:\n", ui_aav_numElements);
	for (i_aav_i = 0; i_aav_i < ui_aav_numElements; i_aav_i++)
		scanf("%d", &ptr_aav_Array[i_aav_i]);

	printf("\n");
	printf("The %u Elements of Integer Array:\n", ui_aav_numElements);
	for (i_aav_i = 0; i_aav_i < ui_aav_numElements; i_aav_i++)
		printf("%d\n", ptr_aav_Array[i_aav_i]);

	printf("\n\n");
	if (ptr_aav_Array)
	{
		free(ptr_aav_Array);
		ptr_aav_Array = NULL;
		printf("Memory Allocated has been free Successfully .\n");
	}

	return(0);
}

void MyAlloc(int** ptr, unsigned int numberofElements)
{
	//code
	*ptr = (int*)malloc(numberofElements * sizeof(int));
	if (*ptr == NULL)
	{
		printf("Could Not Allocate Memory Exiting Now\n\n");
		exit(0);
	}

	printf("MyAlloc() has Successfully Allocated %lu Bytes For Interger Array \n", (numberofElements * sizeof(int)));
}

