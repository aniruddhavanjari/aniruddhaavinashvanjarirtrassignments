#include <stdio.h>

//defining struct 
struct MyData
{
	int i;
	float f;
	double d;
};

int main(void)
{
	int i_aav_size;
	int  f_aav_size;
	int  d_aav_size;
	int struct_MyData_size;
	int pointer_to_struct_MyData_size;

	struct MyData* ptr_aav_Data = NULL;

	//code 
	printf("\n");
	ptr_aav_Data = (struct MyData*)malloc(sizeof(struct MyData));
	if (ptr_aav_Data == NULL)
	{
		printf("Failed to Allocater Memory to struct MyData Exiting Now \n");
		exit(0);
	}
	else
		printf("SuccessFully Allocated Memory to struct MyData \n");

	(*ptr_aav_Data).i = 30;
	(*ptr_aav_Data).f = 11.45f;
	(*ptr_aav_Data).d = 1.2515;

	//Displaying 
	printf("\n\n");
	printf("Data Member of struct MyData Are :\n\n");
	printf("i = %d\n", (*ptr_aav_Data).i);
	printf("f = %f\n", (*ptr_aav_Data).f);
	printf("d = %lf\n", (*ptr_aav_Data).d);
	
	i_aav_size = sizeof((*ptr_aav_Data).i);
	f_aav_size = sizeof((*ptr_aav_Data).f);
	d_aav_size = sizeof((*ptr_aav_Data).d);

	printf("\n\n");
	printf("Size (in Bytes)of Data Members of struct MyData Are :\n");
	printf("Size of 'i' = %d bytes\n",i_aav_size);
	printf("Size of 'f' = %d bytes\n", f_aav_size);
	printf("Size of 'd' = %d bytes\n", d_aav_size);

	struct_MyData_size = sizeof(struct MyData);
	pointer_to_struct_MyData_size = sizeof(struct MyData*);

	printf("\n");
	printf("Size of struct MyData :%d bytes \n\n", struct_MyData_size);
	printf("Size of pointer to struct MyData : %d bytes\n", pointer_to_struct_MyData_size);

	if (ptr_aav_Data)
	{
		free(ptr_aav_Data);
		ptr_aav_Data = NULL;
		printf("Memory Allocated to struct MyData Has Beed Successfully Freed \n");
	}

	return(0);
}


