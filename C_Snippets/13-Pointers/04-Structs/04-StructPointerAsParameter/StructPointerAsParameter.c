#include <stdio.h>

//defining struct 
struct MyData
{
	int i;
	float f;
	double d;
};

int main(void)
{
	//Function prototypes
	void ChangeValue(struct MyData*);

	//varable declaration
	struct MyData* ptr_aav_Data = NULL;

	//code 
	printf("\n");
	ptr_aav_Data = (struct MyData*)malloc(sizeof(struct MyData));
	if (ptr_aav_Data == NULL)
	{
		printf("Failed to Allocater Memory to struct MyData Exiting Now \n");
		exit(0);
	}
	else
		printf("SuccessFully Allocated Memory to struct MyData \n");

	ptr_aav_Data->i = 180;
	ptr_aav_Data->f = 300.8485f;
	ptr_aav_Data->d = 567.82515;


	//Displaying 
	printf("\n\n");
	printf("Data Member of struct MyData :\n\n");
	printf("i = %d\n", ptr_aav_Data->i);
	printf("f = %f\n", ptr_aav_Data->f);
	printf("d = %lf\n", ptr_aav_Data->d);
	
	ChangeValue(ptr_aav_Data);

	printf("\n\n");
	printf("Data Member of struct MyData  :\n\n");
	printf("i = %d\n", ptr_aav_Data->i);
	printf("f = %f\n", ptr_aav_Data->f);
	printf("d = %lf\n", ptr_aav_Data->d);

	if (ptr_aav_Data)
	{
		free(ptr_aav_Data);
		ptr_aav_Data = NULL;
		printf("Memory Allocated to struct MyData Has Beed Successfully Freed \n");
	}

	return(0);
}

void ChangeValue(struct MyData *pParam_Data)
{
	//code
	pParam_Data->i = 9;
	pParam_Data->f = 8.9f;
	pParam_Data->d = 65.3425;
}
