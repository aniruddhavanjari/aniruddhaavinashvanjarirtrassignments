#include <stdio.h>
#include <stdlib.h>

//defining struct 

struct MyData
{
	int* ptr_aav_i;
	int i_aav_i;

	float* ptr_aav_f;
	float f_aav_f;

	double* ptr_aav_d;
	double d_aav_d;
};

int main(void)
{
	//varaibale declaration	
	struct MyData *ptr_struct_data = NULL;

	//code
	printf("\n");
	ptr_struct_data = (struct MyData*)malloc(sizeof(struct MyData));
	if (ptr_struct_data == NULL)
	{
		printf("failed to  Allocated memory to struct MyData Exiting Now\n");
		exit(0);
	}
	else
	{
		printf("Successfully Allocated memory to struct MyData \n");
	}
	(*ptr_struct_data).i_aav_i = 91;
	(*ptr_struct_data).ptr_aav_i = &(*ptr_struct_data).i_aav_i;

	(*ptr_struct_data).f_aav_f = 11.35f;
	(*ptr_struct_data).ptr_aav_f = &(*ptr_struct_data).f_aav_f;

	(*ptr_struct_data).d_aav_d = 45.0909;
	(*ptr_struct_data).ptr_aav_d = &(*ptr_struct_data).d_aav_d;

	printf("\n\n");
	printf("i = %d\n", *((*ptr_struct_data).ptr_aav_i));
	printf("Address of 'i' = %p\n", (*ptr_struct_data).ptr_aav_i);

	printf("\n\n");
	printf("f = %f\n", *((*ptr_struct_data).ptr_aav_f));
	printf("Address of 'f' = %p\n", (*ptr_struct_data).ptr_aav_f);

	printf("\n\n");
	printf("d = %lf\n", *((*ptr_struct_data).ptr_aav_d));
	printf("Address of 'd' = %p\n", (*ptr_struct_data).ptr_aav_d);

	if (ptr_struct_data)
	{
		free(ptr_struct_data);
		ptr_struct_data = NULL;
		printf("Successfully Freed Allocated memory to struct MyData \n");
	}
	return(0);

}