#include <stdio.h>

//defining struct 
struct MyData
{
	int *ptr_i;
	int i;

	float *ptr_f;
	float f;

	double *ptr_d;
	double d;
};

int main(void)
{
	struct MyData* ptr_aav_Data = NULL;

	//code 
	printf("\n");
	ptr_aav_Data = (struct MyData*)malloc(sizeof(struct MyData));
	if (ptr_aav_Data == NULL)
	{
		printf("Failed to Allocater Memory to struct MyData Exiting Now \n");
		exit(0);
	}
	else
		printf("SuccessFully Allocated Memory to struct MyData \n");

	ptr_aav_Data->i = 90;
	ptr_aav_Data->ptr_i = &(ptr_aav_Data->i);

	ptr_aav_Data->f = 181.8485f;
	ptr_aav_Data->ptr_f = &(ptr_aav_Data->f);

	ptr_aav_Data->d = 1.82515;
	ptr_aav_Data->ptr_d = &(ptr_aav_Data->d);

	//Displaying 
	printf("\n\n");
	printf("Data Member of struct MyData And Address :\n\n");
	printf("i = %d\n", *(ptr_aav_Data->ptr_i));
	printf("Address of i = %p\n", ptr_aav_Data->ptr_i);
	printf("f = %f\n", *(ptr_aav_Data->ptr_f));
	printf("Address of f = %p\n", ptr_aav_Data->ptr_f);
	printf("d = %lf\n", *(ptr_aav_Data->ptr_d));
	printf("Address of d = %p\n", ptr_aav_Data->ptr_d);

	if (ptr_aav_Data)
	{
		free(ptr_aav_Data);
		ptr_aav_Data = NULL;
		printf("Memory Allocated to struct MyData Has Beed Successfully Freed \n");
	}

	return(0);
}


