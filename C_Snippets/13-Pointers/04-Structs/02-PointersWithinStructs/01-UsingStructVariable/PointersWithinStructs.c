#include <stdio.h>

//defining struct 

struct MyData
{
	int* ptr_aav_i;
	int i_aav_i;

	float* ptr_aav_f;
	float f_aav_f;

	double* ptr_aav_d;
	double d_aav_d;
};

int main(void)
{
	 //varaibale declaration	
	struct MyData struct_data;

	//code
	struct_data.i_aav_i = 91;
	struct_data.ptr_aav_i = &struct_data.i_aav_i;

	struct_data.f_aav_f = 11.35f;
	struct_data.ptr_aav_f = &struct_data.f_aav_f;

	struct_data.d_aav_d = 45.0909;
	struct_data.ptr_aav_d = &struct_data.d_aav_d;

	printf("\n\n");
	printf("i = %d\n", *(struct_data.ptr_aav_i));
	printf("Address of 'i' = %p\n", struct_data.ptr_aav_i);

	printf("\n\n");
	printf("f = %f\n", *(struct_data.ptr_aav_f));
	printf("Address of 'f' = %p\n", struct_data.ptr_aav_f);

	printf("\n\n");
	printf("d = %lf\n", *(struct_data.ptr_aav_d));
	printf("Address of 'd' = %p\n", struct_data.ptr_aav_d);

	return(0);

}