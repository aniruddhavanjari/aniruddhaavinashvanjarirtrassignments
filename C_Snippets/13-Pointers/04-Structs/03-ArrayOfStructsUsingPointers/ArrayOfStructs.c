#include <stdio.h>
#include <ctype.h>

#define NAME_LENGTH 100
#define MARITAL_STATUS 10

struct Employee
{
	char name[NAME_LENGTH];
	int age;
	char sex;
	float salary;
	char marital_status;
};

int main(void)
{
	//Function Prototype
	void MyGetString(char[], int);

	//variable declarations
	struct Employee* pEmployeeRecord = NULL;
	int i_aav_numEmployee, i_aav_i;

	//code

	printf("\n\n");
	printf("Enter Number of Employee Whose Details You Want to Record :");
	scanf("%d", &i_aav_numEmployee);

	printf("\n\n");
	pEmployeeRecord = (struct Employee*)malloc(i_aav_numEmployee * (sizeof(struct Employee)));
	if (pEmployeeRecord == NULL)
	{
		printf("Failed to Allocated Memory For %d Employee Exiting Now \n", i_aav_numEmployee);
		exit(0);
	}
	else
		printf("Successfullly Allocated Memory For %d Employee \n", i_aav_numEmployee);

	for (i_aav_i = 0; i_aav_i < i_aav_numEmployee; i_aav_i++)
	{
		printf("\n\n");
		printf("Data Entry For Employee Number %d\n", (i_aav_i + 1));
		printf("\n\n");
		printf("Enter Employee Name :");
		MyGetString(pEmployeeRecord[i_aav_i].name, NAME_LENGTH);

		printf("\n\n");
		printf("Enter Employee's Age in Years :");
		scanf("%d", &pEmployeeRecord[i_aav_i].age);

		printf("\n\n");
		printf("Enter Employee's Salary in Annualy :");
		scanf("%f", &pEmployeeRecord[i_aav_i].salary);

		printf("\n\n");
		printf("Enter Employee's Sex (M/m for Male F/f for Female :");
		pEmployeeRecord[i_aav_i].sex = getch();
		printf("%c", pEmployeeRecord[i_aav_i].sex);
		pEmployeeRecord[i_aav_i].sex = toupper(pEmployeeRecord[i_aav_i].sex);

		printf("\n\n");
		printf("Is the Employee MArried ? (Y/y For N/n For No) :");
		pEmployeeRecord[i_aav_i].marital_status = getch();
		printf("%c", pEmployeeRecord[i_aav_i].marital_status);
		pEmployeeRecord[i_aav_i].marital_status = toupper(pEmployeeRecord[i_aav_i].marital_status);
	}

	printf("\n\n");
	printf("Displaying Employee Records\n\n");
	for (i_aav_i = 0; i_aav_i < i_aav_numEmployee; i_aav_i++)
	{
		printf("Employee Number %d \n\n", (i_aav_i + 1));
		printf("Name :%s\n", pEmployeeRecord[i_aav_i].name);
		printf("Age :%d years\n", pEmployeeRecord[i_aav_i].age);
		
		if (pEmployeeRecord[i_aav_i].sex == 'M')
			printf("Sex :Male\n");
		else if (pEmployeeRecord[i_aav_i].sex == 'F')
			printf("Sex :Female\n");
		else
			printf("Sex :Invalid Data Entered\n");

		printf("Salary :Rs.%f\n", pEmployeeRecord[i_aav_i].salary);
		if (pEmployeeRecord[i_aav_i].marital_status == 'Y')
			printf("Maritial Status : Married\n");
		else if (pEmployeeRecord[i_aav_i].marital_status == 'N')
			printf("Maritial Status : UnMarried\n");
		else 
				printf("Maritial Status :Invalid Data Entry\n");
		printf("\n\n");
	}

	if (pEmployeeRecord)
	{
		free(pEmployeeRecord);
		pEmployeeRecord = NULL;
		printf("Memory Allocation to %d Employee has Been Successfully Free\n", i_aav_numEmployee);
	}

	return(0);
}

void MyGetString(char str[], int str_size)
{
	//variable 
	int i_aav_i; 
	char c_aav_ch = '\0';
	
	//code 
	i_aav_i = 0;
	do
	{
		c_aav_ch = getch();
		str[i_aav_i] = c_aav_ch;
		printf("%c", str[i_aav_i]);
		i_aav_i++;
	} while ((c_aav_ch != '\r') && (i_aav_i < str_size));

	if (i_aav_i == str_size)
		str[i_aav_i - 1] = '\0';
	else
		str[i_aav_i] = '\0';

}