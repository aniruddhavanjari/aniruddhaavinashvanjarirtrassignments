#include <stdio.h>

int main(void)
{
	//vsrisble declarations
	char c_aav_ch;
	char *pc_aav_ptr = NULL;

	c_aav_ch = 'A';

	printf("\n");

	printf("**Before c_aav_ptr = &c_aav_*****\n");
	printf("Value of 'c_aav_ch' = %c\n", c_aav_ch);
	printf("Address of 'c_aav_ch = %p\n", &c_aav_ch);
	printf("Value at Address of 'c_aav_ch' =%c", *(&c_aav_ch));

	pc_aav_ptr = &c_aav_ch;

	printf("\n");

	printf("**After c_aav_ptr = &ch*****\n");
	printf("Value of 'c_aav_ch' = %c\n", c_aav_ch);
	printf("Address of 'c_aav_ch = %p\n", pc_aav_ptr);
	printf("Value at Address of 'c_aav_ch' = %c", *pc_aav_ptr);

	return(0);
}