#include <stdio.h>

int main(void)
{
	//variable declarations
	double d_aav_num;
	double *pd_aav_ptr = NULL;

	//code
	d_aav_num = 373.45736;

	printf("\n\n");
	printf("***********Before pd_aav_ptr = &d_aav_num*********\n");
	printf("Value of 'd_aav_num'  = %f\n", d_aav_num);
	printf("Address of 'd_aav_num'  = %p\n", &d_aav_num);
	printf("Value at Address of 'd_aav_num'  = %f\n", *(&d_aav_num));

	pd_aav_ptr = &d_aav_num;

	printf("\n\n");
	printf("***********After pd_aav_ptr = &d_aav_num**********\n");
	printf("Value of 'd_aav_num'  = %lf\n", d_aav_num);
	printf("Address of 'd_aav_num'  = %p\n", pd_aav_ptr);
	printf("Value at Address of 'd_aav_num'  = %lf\n", *pd_aav_ptr);

	return(0);

}