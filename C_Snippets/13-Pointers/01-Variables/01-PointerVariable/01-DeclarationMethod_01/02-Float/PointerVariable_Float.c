#include <stdio.h>

int main(void)
{
	//variable declarations
	float f_aav_num;
	float *pf_aav_ptr = NULL;

	//code
	f_aav_num = 56.06f;

	printf("\n\n");
	printf("***********Before pf_aav_ptr = &f_aav_num*****\n");
	printf("Value of 'f_aav_num'  = %f\n\n", f_aav_num);
	printf("Address of 'f_aav_num'  = %p\n\n", &f_aav_num);
	printf("Value at Address of 'f_aav_num'  = %f\n\n", *(&f_aav_num));

	pf_aav_ptr = &f_aav_num;

	printf("\n\n");
	printf("***********After pf_aav_ptr = &f_aav_num*****\n");
	printf("Value of 'f_aav_num'  = %f\n\n", f_aav_num);
	printf("Address of 'f_aav_num'  = %p\n\n", pf_aav_ptr);
	printf("Value at Address of 'f_aav_num'  = %f\n\n", *pf_aav_ptr);

	return(0);

}