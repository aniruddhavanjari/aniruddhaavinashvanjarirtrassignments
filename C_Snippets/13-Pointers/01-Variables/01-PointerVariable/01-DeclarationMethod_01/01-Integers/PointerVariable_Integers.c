#include <stdio.h>

int main(void)
{
	//variable declarations
	int i_aav_num;
	int *pi_aav_ptr = NULL;//Declaration Method 1 : '*pi_aav_ptr is variable of type 'int'

	//code
	i_aav_num = 45;

	printf("\n\n");
	printf("***********Before pi_aav_ptr = &i_aav_num*****\n");
	printf("Value of 'i_aav_num'  = %d\n\n", i_aav_num);
	printf("Address of 'i_aav_num'  = %p\n\n", &i_aav_num);
	printf("Value at Address of 'i_aav_num'  = %d\n\n", *(&i_aav_num));

	pi_aav_ptr = &i_aav_num;

	printf("\n\n");
	printf("***********After pi_aav_ptr = &i_aav_num*****\n");
	printf("Value of 'i_aav_num'  = %d\n\n", i_aav_num);
	printf("Address of 'i_aav_num'  = %p\n\n", pi_aav_ptr);
	printf("Value at Address of 'i_aav_num'  = %d\n\n", *pi_aav_ptr);

	return(0);
}