#include <stdio.h>

int main(void)
{
	//variable declaration
	int i_aav_num;
	int* pi_aav_ptr = NULL;
	int* pi_aav_copyptr = NULL;

	//code
	i_aav_num =50;
	pi_aav_ptr = &i_aav_num;

	printf("\n\n");
	printf("Before pi_aav_copyptr = pi_aav_ptr \n\n");
	printf("i_aav_num = %d\n", i_aav_num);
	printf("&i_aav_num = %p\n", &i_aav_num);
	printf(" *(&i_aav_num) = %d\n", *(&i_aav_num));
	printf("pi_aav_ptr = %p\n", pi_aav_ptr);
	printf("*pi_aav_ptr = %d\n", *pi_aav_ptr); 
	
	pi_aav_copyptr = pi_aav_ptr;

	printf("\n\n");
	printf("Before pi_aav_copyptr = pi_aav_ptr \n\n");
	printf("i_aav_num = %d\n", i_aav_num);
	printf("&i_aav_num = %p\n", &i_aav_num);
	printf(" *(&i_aav_num) = %d\n", *(&i_aav_num));
	printf("pi_aav_ptr = %p\n", pi_aav_ptr);
	printf("pi_aav_ptr = %d\n", *pi_aav_ptr);
	printf("pi_aav_copyptr = %p\n", pi_aav_copyptr);
	printf("*pi_aav_copyptr = %d\n", *pi_aav_copyptr);


	return(0);
}