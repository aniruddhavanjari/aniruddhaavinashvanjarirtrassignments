#include <stdio.h>

int main(void)
{

	int i_aav_num;
	int *pi_aav_ptr = NULL;
	int** ppi_aav_ptr = NULL;

	i_aav_num = 100;

	printf("\n");

	printf("Before pi_aav_ptr = &i_aav_num\n");
	printf("Value of 'i_aav_num' = %d\n", i_aav_num);
	printf("Address of 'i_aav_num' = %p\n", &i_aav_num);
	printf("Value at Address of 'i_aav_num' = %d\n", *(&i_aav_num));

	pi_aav_ptr = &i_aav_num;

	printf("\n");

	printf("After pi_aav_ptr = &i_aav_num\n");
	printf("Value of 'i_aav_num' = %d\n", i_aav_num);
	printf("Address of 'i_aav_num' = %p\n", pi_aav_ptr);
	printf("Value at Address of 'i_aav_num' = %d\n", *pi_aav_ptr);

	ppi_aav_ptr = &pi_aav_ptr;

	printf("\n");

	printf("After ppi_aav_ptr = &pi_aav_ptr\n");
	printf("Value of 'i_aav_num' = %d\n", i_aav_num);
	printf("Address of 'i_aav_num' = %p\n", pi_aav_ptr);
	printf("Address of 'pi_aav_ptr'(ppi_aav_ptr) = %p\n", ppi_aav_ptr);
	printf("Value at Address of 'ppi_aav_ptr'(*ppi_aav_ptr) = %p\n", *ppi_aav_ptr);
	printf("Value at Address of 'i_aav_num'(*pi_aav_ptr)(**ppi_aav_ptr) = %d\n", **ppi_aav_ptr);

}