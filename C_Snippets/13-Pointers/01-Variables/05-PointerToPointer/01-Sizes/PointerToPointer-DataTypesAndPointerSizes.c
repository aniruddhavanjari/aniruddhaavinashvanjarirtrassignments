#include <stdio.h>

struct Employee
{
	char name[100];
	int age;
	float salary;
	char sex;
	char marital_statuc;
};


int main(void)
{

	//code
	printf("\n");
	printf("Size of Ddata Type And Pointer to Those Respective Data Type Are :\n");
	printf("Size of (int) :%d \t\t Size of Pointer to int (int*) :%d \t\t Size of Pointer to Pinter to int (int**) :%d\n",sizeof(int), sizeof(int*), sizeof(int**));
	printf("Size of (float) :%d \t\t Size of Pointer to float (float*) :%d \t\t Size of Pointer to Pinter to flaot (float**) :%d\n",sizeof(float), sizeof(float*), sizeof(float**));
	printf("Size of (double) :%d \t\t Size of Pointer to double (double*) :%d \t Size of Pointer to Pinter to double (double**) :%d\n",sizeof(double), sizeof(double*), sizeof(double**));
	printf("Size of (char) :%d \t\t Size of Pointer to char (char*) :%d \t\t Size of Pointer to Pinter to char(char**) :%d\n", sizeof(char), sizeof(char*), sizeof(char**));
	printf("Size of (struct Employee) :%d \t Size of Pointer to (struct Employee*) :%d \t Size of Pointer to Pinter to struct Employee (struct Employee**) :%d\n", sizeof(struct Employee), sizeof(struct Employee*), sizeof(struct Employee**));

	return(0);
}