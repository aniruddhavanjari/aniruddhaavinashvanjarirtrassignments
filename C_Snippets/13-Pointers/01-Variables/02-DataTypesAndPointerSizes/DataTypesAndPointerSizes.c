#include <stdio.h>

struct Employee
{
	char name[100];
	int age;
	float salary;
	char sex;
	char marital_status;
};

int main(void)
{
	//code 
	printf("\n");
	printf("Size of Data Type And Pointers to those Respective Data Type Are: \n\n");
	printf("Size of (int) :%d \t Size of pointer to int (int*) :%d\n", sizeof(int), sizeof(int*));
	printf("Size of (float) :%d \t Size of pointer to float (float*) :%d\n", sizeof(float), sizeof(float*));
	printf("Size of (double) :%d \t Size of pointer to double (bouble*) :%d\n", sizeof(double), sizeof(double*));
	printf("Size of (char) :%d \t Size of pointer to char (char*) :%d\n", sizeof(char), sizeof(char*));
	printf("Size of (struct Employee) :%d \t Size of pointer to struct Employee (struct Employee*) :%d\n", sizeof(struct Employee), sizeof(char*));
	
	return(0);
}