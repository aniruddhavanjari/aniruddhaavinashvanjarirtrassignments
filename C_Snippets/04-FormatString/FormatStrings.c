#include <stdio.h>
int main(void)
{
	//Code
	printf("\n\n");
	printf("********************************\n");
	printf("\n\n");

	printf("Hello World !!!\n\n");

	int i_aav_a = 13;
	printf("Integer Decimal Value of 'i_aav_a' = %d\n", i_aav_a);
	printf("Integer Octal Value of 'i_aav_a' = %o\n", i_aav_a);
	printf("Integer Hexadecimal Value of 'i_aav_a' (Hexadecimal Letters In Lower In Lower Case) = %x\n", i_aav_a);
	printf("Integer Hexadecimal Value of 'i_aav_a' (Hexadecimal Letters In Lower In Lower Case) = %X\n\n", i_aav_a);

	char c_aav_ch = 'A';
	printf("Charater c_aav_ch = %c\n", c_aav_ch);
	char ca_aav_str[] = "AstroMediComp's Real Time Rendering Batch 3.0 (2020-2021)";
	printf("String ca_aav_str = %s\n\n", ca_aav_str);

	long l_aav_num = 30121995L;
	printf("Long Integer = %ld\n\n", l_aav_num);

	unsigned int ui_aav_b = 7;
	printf("Unsigned Integer 'ui_aav_b' = %u\n\n", ui_aav_b);

	float f_aav_num = 3012.1995f;
	printf("Floating Point Number With Just %%f 'f_aav_num' = %f\n", f_aav_num);
	printf("Floating Point Number With %%4.2f 'f_aav_num' = %4.2f\n", f_aav_num);
	printf("Floating Point Number With %%6.5f 'f_aav_num' = %6.5f\n\n", f_aav_num);

	double d_aav_pi = 3.14159265358979323846;
	printf("Double Precision Floating Point Number Without Exponential = %g\n", d_aav_pi);
	printf("Double Precision Floating Point Number With Exponential (Lower Case) = %e\n", d_aav_pi);
	printf("Double Precision Floating Point Number With Exponential (Upper Case) = %E\n\n", d_aav_pi);
	printf("Double Hexadecimal Value of 'd_aav_pi' (Hexadecimal Letters In Lower Case) = %a\n", d_aav_pi);
	printf("Double Hexadecimal Value of 'd_aav_pi' (Hexadecimal Letters In Upper Case) = %A\n\n", d_aav_pi);

	printf("**************************************************************");
	printf("\n\n");
	return(0);
}