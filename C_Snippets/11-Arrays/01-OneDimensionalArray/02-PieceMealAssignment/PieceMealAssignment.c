#include <stdio.h>

int main(void)
{
	int i_aav_array1[10];
	int i_aav_array2[10];

	/*****Array One******/

	i_aav_array1[0] = 11;
	i_aav_array1[1] = 12;
	i_aav_array1[2] = 13;
	i_aav_array1[3] = 14;
	i_aav_array1[4] = 15;
	i_aav_array1[5] = 16;
	i_aav_array1[6] = 17;
	i_aav_array1[7] = 18;
	i_aav_array1[8] = 19;
	i_aav_array1[9] = 20;

	printf("\n");
	printf("Piece-Meal (Hard-coded)\n");
	printf("1 Element = %d\n", i_aav_array1[0]);
	printf("2 Element = %d\n", i_aav_array1[1]);
	printf("3 Element = %d\n", i_aav_array1[2]);
	printf("4 Element = %d\n", i_aav_array1[3]);
	printf("5 Element = %d\n", i_aav_array1[4]);
	printf("6 Element = %d\n", i_aav_array1[5]);
	printf("7 Element = %d\n", i_aav_array1[6]);
	printf("8 Element = %d\n", i_aav_array1[7]);
	printf("9 Element = %d\n", i_aav_array1[8]);
	printf("10 Element = %d\n", i_aav_array1[9]);
	
	/******Array Two*****/
	printf("\n");
	printf("Enter 1 Element ");
	scanf("%d", &i_aav_array2[0]);
	printf("Enter 2 Element ");
	scanf("%d", &i_aav_array2[1]);
	printf("Enter 3 Element ");
	scanf("%d", &i_aav_array2[2]);
	printf("Enter 4 Element ");
	scanf("%d", &i_aav_array2[3]);
	printf("Enter 5 Element ");
	scanf("%d", &i_aav_array2[4]);
	printf("Enter 6 Element ");
	scanf("%d", &i_aav_array2[5]);
	printf("Enter 7 Element ");
	scanf("%d", &i_aav_array2[6]);
	printf("Enter 8 Element ");
	scanf("%d", &i_aav_array2[7]);
	printf("Enter 9 Element ");
	scanf("%d", &i_aav_array2[8]);
	printf("Enter 10 Element ");
	scanf("%d", &i_aav_array2[9]);

	printf("\n");
	printf("Display Array2\n");
	printf("1 Element = %d\n", i_aav_array2[0]);
	printf("2 Element = %d\n", i_aav_array2[1]);
	printf("3 Element = %d\n", i_aav_array2[2]);
	printf("4 Element = %d\n", i_aav_array2[3]);
	printf("5 Element = %d\n", i_aav_array2[4]);
	printf("6 Element = %d\n", i_aav_array2[5]);
	printf("7 Element = %d\n", i_aav_array2[6]);
	printf("8 Element = %d\n", i_aav_array2[7]);
	printf("9 Element = %d\n", i_aav_array2[8]);
	printf("10 Element = %d\n", i_aav_array2[9]);

	return(0);
}
