#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declaration
	void Mystrcpy(char[], char[]);

	char c_aav_ArrayOrignal[MAX_STRING_LENGTH], c_aav_ArrayReplaceVowels[MAX_STRING_LENGTH];

	int i_aav_StringLen;
	int i_aav_i;

	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_ArrayOrignal, MAX_STRING_LENGTH);

	Mystrcpy(c_aav_ArrayReplaceVowels, c_aav_ArrayOrignal);

	i_aav_StringLen = MyStrlen(c_aav_ArrayReplaceVowels);

	for (i_aav_i = 0; i_aav_i < i_aav_StringLen; i_aav_i++)
	{
		switch (c_aav_ArrayReplaceVowels[i_aav_i])
		{
		case 'A':
		case 'a':
		case 'E':
		case 'e':
		case 'I':
		case 'i':
		case 'O':
		case 'o':
		case 'U':
		case 'u':
			c_aav_ArrayReplaceVowels[i_aav_i] = '*';
			break;
		default:
			break;
		}
	}

	printf("\n");
	printf(" Orignal String is : \n");
	printf("%s\n", c_aav_ArrayOrignal);

	printf("\n");
	printf("String After Replacement of Vowels By * is : \n");
	printf("%s\n", c_aav_ArrayReplaceVowels);

	return(0);
}

void Mystrcpy(char c_aav_strdestination[], char c_aav_strsource[])
{
	int i_aav_MyStinglen(char[]);

	int i_aav_StringLength = 0;
	int i_aav_j = 0;
	i_aav_StringLength = MyStrlen(c_aav_strsource);
	for (i_aav_j = 0; i_aav_j < i_aav_StringLength; i_aav_j++)
	{
		c_aav_strdestination[i_aav_j] = c_aav_strsource[i_aav_j];
	}
	c_aav_strdestination[i_aav_j] = '\0';
}
int MyStrlen(char c_aav_str[])
{
	int i_aav_j;
	int i_aav_string_length = 0;

	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (c_aav_str[i_aav_j] == '\0')
			break;
		else
			i_aav_string_length++;
	}
	return(i_aav_string_length);
}