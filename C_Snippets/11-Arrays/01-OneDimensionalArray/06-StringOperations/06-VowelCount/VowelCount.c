#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrlen(char c_aav_str[]);

	char c_aav_Array[MAX_STRING_LENGTH];
	int i_aav_StringLength;
	int i_aav_count_A = 0, i_aav_count_E = 0, i_aav_count_I = 0, i_aav_count_O = 0, i_aav_count_U = 0;
	int i_aav_i;

	printf("\n\n");
	printf("Enter A String :\n\n");
	gets_s(c_aav_Array, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("String Entered By You Is : \n\n");
	printf("%s\n", c_aav_Array);

	i_aav_StringLength = MyStrlen(c_aav_Array);

	for (i_aav_i = 0; i_aav_i < i_aav_StringLength; i_aav_i++)
	{
		switch (c_aav_Array[i_aav_i])
		{
		case 'A':
		case 'a':
			i_aav_count_A++;
			break;
		case 'E':
		case 'e':
			i_aav_count_E++;
			break;
		case 'I':
			i_aav_count_I++;
			break;
		case 'O':
		case 'o':
			i_aav_count_O++;
			break;
		case 'U':
		case 'u':
			i_aav_count_U++;
			break;
		default:
			break;
		}
	}
	printf("\n\n");
	printf("In The String Entered By You ,The Vowels And The Number of Their Occurences Are Follows :\n\n");
	printf("'A' Has Occured = %d Time !!!\n\n", i_aav_count_A);
	printf("'E' Has Occured = %d Time !!!\n\n", i_aav_count_E);
	printf("'I' Has Occured = %d Time !!!\n\n", i_aav_count_I);
	printf("'O' Has Occured = %d Time !!!\n\n", i_aav_count_O);
	printf("'U' Has Occured = %d Time !!!\n\n", i_aav_count_U);

	return(0);
}
int MyStrlen(char c_aav_str[])
{
	int i_aav_j;
	int i_aav_string_length = 0;

	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (c_aav_str[i_aav_j] == '\0')
			break;
		else
			i_aav_string_length++;
	}
	return(i_aav_string_length);
}
