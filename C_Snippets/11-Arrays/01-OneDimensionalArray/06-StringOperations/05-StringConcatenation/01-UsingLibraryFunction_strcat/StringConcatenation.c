#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declaration
	char c_aav_ArrayOne[MAX_STRING_LENGTH], c_aav_ArrayTwo[MAX_STRING_LENGTH];


	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_ArrayOne, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_ArrayTwo, MAX_STRING_LENGTH);


	
	printf("\nBefore Concatenation\n");
	printf("\n");
	printf(" One String is : \n");
	printf("%s\n", c_aav_ArrayOne);

	printf("\n");
	printf("Two  String is : \n");
	printf("%s\n", c_aav_ArrayTwo);

	strcat(c_aav_ArrayOne, c_aav_ArrayTwo);

	printf("\n\n");
	printf("After Concatenation");
	printf(" One String is : \n");
	printf("%s\n", c_aav_ArrayOne);

	printf("\n");
	printf("Two  String is : \n");
	printf("%s\n", c_aav_ArrayTwo);


	return(0);
}
