#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declaration
	void MyStrcat(char[], char[]);
	char c_aav_ArrayOne[MAX_STRING_LENGTH], c_aav_ArrayTwo[MAX_STRING_LENGTH];


	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_ArrayOne, MAX_STRING_LENGTH);

	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_ArrayTwo, MAX_STRING_LENGTH);



	printf("\nBefore Concatenation\n");
	printf("\n");
	printf(" One String is : \n");
	printf("%s\n", c_aav_ArrayOne);

	printf("\n");
	printf("Two  String is : \n");
	printf("%s\n", c_aav_ArrayTwo);

	MyStrcat(c_aav_ArrayOne, c_aav_ArrayTwo);

	printf("\n\n");
	printf("After Concatenation");
	printf(" One String is : \n");
	printf("%s\n", c_aav_ArrayOne);

	printf("\n");
	printf("Two  String is : \n");
	printf("%s\n", c_aav_ArrayTwo);


	return(0);
}
void MyStrcat(char c_aav_strdestination[], char c_aav_strsource[])
{
	int i_aav_MyStinglen(char[]);

	int i_aav_StringLengthSource = 0, i_aav_StringLengthDestiantion = 0;
	int i_aav_i, i_aav_j;

	i_aav_StringLengthSource = MyStrlen(c_aav_strsource);
	i_aav_StringLengthDestiantion = MyStrlen(c_aav_strdestination);


	for (i_aav_i = i_aav_StringLengthDestiantion, i_aav_j = 0; i_aav_j < i_aav_StringLengthSource ; i_aav_i++, i_aav_j++)
	{
		c_aav_strdestination[i_aav_i] = c_aav_strsource[i_aav_j];
	}
	c_aav_strdestination[i_aav_i] = '\0';
}

int MyStrlen(char c_aav_str[])
{
	int i_aav_j;
	int i_aav_string_length = 0;

	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (c_aav_str[i_aav_j] == '\0')
			break;
		else
			i_aav_string_length++;
	}
	return(i_aav_string_length);
}
