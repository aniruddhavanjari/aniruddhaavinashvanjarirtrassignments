#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declaration
	char c_aav_ArrayOrignal[MAX_STRING_LENGTH], c_aav_ArrayCopy[MAX_STRING_LENGTH];
	

	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_ArrayOrignal, MAX_STRING_LENGTH);

	strcpy(c_aav_ArrayCopy,c_aav_ArrayOrignal);

	printf("\n");
	printf(" Orignal String is : \n");
	printf("%s\n", c_aav_ArrayOrignal);

	printf("\n");
	printf("Copyed  String is : \n");
	printf("%s\n", c_aav_ArrayCopy);

	return(0);
}