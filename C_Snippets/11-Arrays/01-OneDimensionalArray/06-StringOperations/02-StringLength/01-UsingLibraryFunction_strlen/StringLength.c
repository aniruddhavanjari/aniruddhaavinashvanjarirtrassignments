#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declaration
	char c_aav_Array[MAX_STRING_LENGTH];
	int i_aav_StringLength = 0;

	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_Array, MAX_STRING_LENGTH);

	printf("\n");
	printf("String Entered By You is : \n");
	printf("%s\n", c_aav_Array);

	//String Length
	printf("\n");
	i_aav_StringLength = strlen(c_aav_Array);
	printf("Length of Sting = %d\n", i_aav_StringLength);
	return(0);
}