#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declaration
	char c_aav_Array[MAX_STRING_LENGTH];
	int i_aav_StringLength = 0;

	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_Array, MAX_STRING_LENGTH);

	printf("\n");
	printf("String Entered By You is : \n");
	printf("%s\n", c_aav_Array);

	//String Length
	printf("\n");
	i_aav_StringLength = MyStrlen(c_aav_Array);
	printf("Length of Sting = %d\n", i_aav_StringLength);
	return(0);
}

int MyStrlen(char c_aav_str[])
{
	int i_aav_j;
	int i_aav_string_length = 0;

	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (c_aav_str[i_aav_j] == '\0')
			break;
		else
			i_aav_string_length++;
	}
	return(i_aav_string_length);
}