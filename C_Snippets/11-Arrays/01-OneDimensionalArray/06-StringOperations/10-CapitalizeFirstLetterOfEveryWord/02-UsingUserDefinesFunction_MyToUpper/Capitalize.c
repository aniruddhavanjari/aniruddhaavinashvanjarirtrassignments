#include <stdio.h>

#define MAX_STRING_LENGTH 512

#define SPACE ' '

#define FULLSTOP '.'
#define COMMA ','
#define EXCLAMATION '!'
#define QUESTION_MARK '?'

int main(void)
{
	//variable declaration
	int MyStrlen(char c_aav_str[]);
	char MyToUpper(char);

	char c_aav_Array[MAX_STRING_LENGTH], c_aav_ArrayCapitalizeFirstLetterOfEveryWorld[MAX_STRING_LENGTH];

	int i_aav_StringLen;
	int i_aav_i, i_aav_j;


	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_Array, MAX_STRING_LENGTH);

	i_aav_StringLen = MyStrlen(c_aav_Array);

	i_aav_j = 0;
	for (i_aav_i = 0; i_aav_i < i_aav_StringLen; i_aav_i++)
	{
		if (i_aav_i == 0)
		{
			c_aav_ArrayCapitalizeFirstLetterOfEveryWorld[i_aav_j] = toupper(c_aav_Array[i_aav_i]);
		}
		else if (c_aav_Array[i_aav_i] == SPACE)
		{
			c_aav_ArrayCapitalizeFirstLetterOfEveryWorld[i_aav_j] = c_aav_Array[i_aav_i];
			c_aav_ArrayCapitalizeFirstLetterOfEveryWorld[i_aav_j + 1] = toupper(c_aav_Array[i_aav_i + 1]);
			i_aav_i++;
			i_aav_j++;
		}
		else if((c_aav_Array[i_aav_i] == FULLSTOP || c_aav_Array[i_aav_i] == COMMA || c_aav_Array[i_aav_i] == EXCLAMATION || c_aav_Array[i_aav_i] == QUESTION_MARK) && (c_aav_Array[i_aav_i] != SPACE))
		{
			c_aav_ArrayCapitalizeFirstLetterOfEveryWorld[i_aav_j] = c_aav_Array[i_aav_i];
			c_aav_ArrayCapitalizeFirstLetterOfEveryWorld[i_aav_j+1] = SPACE;
			c_aav_ArrayCapitalizeFirstLetterOfEveryWorld[i_aav_j+2] = MyToUpper(c_aav_Array[i_aav_i+1]);
			i_aav_j = i_aav_j+2;
			i_aav_i++;

		}
		else 
		{
			c_aav_ArrayCapitalizeFirstLetterOfEveryWorld[i_aav_j] = c_aav_Array[i_aav_i];

		}
		i_aav_j++;
	}

	c_aav_ArrayCapitalizeFirstLetterOfEveryWorld[i_aav_j] = '\0';

	printf("\n");
	printf(" String Entered is : \n");
	printf("%s\n", c_aav_Array);

	printf("\n");
	printf("String After Removing Spaces is : \n");
	printf("%s\n", c_aav_ArrayCapitalizeFirstLetterOfEveryWorld);
	return(0);
}

int MyStrlen(char c_aav_str[])
{
	int i_aav_j;
	int i_aav_string_length = 0;

	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (c_aav_str[i_aav_j] == '\0')
			break;
		else
			i_aav_string_length++;
	}
	return(i_aav_string_length);
}

char MyToUpper(char c_aav_ch)
{
	int i_aav_num;
	int i_aav_c;

	i_aav_num = 'a' - 'A';

	if ((int)c_aav_ch >= 97 && (int)c_aav_ch <= 122)
	{
		i_aav_c = (int)c_aav_ch - i_aav_num;
		return((char)i_aav_c);
	}
	else
		return(c_aav_ch);
}