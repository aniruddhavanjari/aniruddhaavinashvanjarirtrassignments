#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declaration
	int MyStrlen(char c_aav_str[]);

	char c_aav_Array[MAX_STRING_LENGTH],c_aav_ArraySpaceRemove[MAX_STRING_LENGTH];

	int i_aav_StringLen;
	int i_aav_i, i_aav_j;
	

	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_Array, MAX_STRING_LENGTH);

	i_aav_j = 0;

	i_aav_StringLen = MyStrlen(c_aav_Array);

	for (i_aav_i = 0; i_aav_i < i_aav_StringLen; i_aav_i++)
	{
		if (c_aav_Array[i_aav_i] == ' ')
			continue;
		else
		{
			c_aav_ArraySpaceRemove[i_aav_j] = c_aav_Array[i_aav_i];
			i_aav_j++;
		}
	}

	c_aav_ArraySpaceRemove[i_aav_j] = '\0';

	printf("\n");
	printf(" String Entered is : \n");
	printf("%s\n", c_aav_Array);

	printf("\n");
	printf("String After Removing Spaces is : \n");
	printf("%s\n", c_aav_ArraySpaceRemove);
	return(0);
}

int MyStrlen(char c_aav_str[])
{
	int i_aav_j;
	int i_aav_string_length = 0;

	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (c_aav_str[i_aav_j] == '\0')
			break;
		else
			i_aav_string_length++;
	}
	return(i_aav_string_length);
}