#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	//variable declaration
	char c_aav_ArrayOrignal[MAX_STRING_LENGTH];


	printf("\n\n");
	printf("Enter A String : \n\n");
	gets_s(c_aav_ArrayOrignal, MAX_STRING_LENGTH);

	printf("\n");
	printf(" Orignal String is : \n");
	printf("%s\n", c_aav_ArrayOrignal);

	printf("\n");
	printf("Reversed  String is : \n");
	printf("%s\n", strrev(c_aav_ArrayOrignal));

	return(0);
}