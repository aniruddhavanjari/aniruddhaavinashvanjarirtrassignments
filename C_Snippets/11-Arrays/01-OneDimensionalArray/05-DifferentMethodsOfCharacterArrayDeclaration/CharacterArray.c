#include <stdio.h>

int main(void)
{
	char c_aav_arr1[] = {'A','S','T','R','O','M','E','D','I','C','O','M','P','\0'};
	char c_aav_arr2[] = { 'W','E','L','C','O','M','S','\0' };
	char c_aav_arr3[] = {'Y','O','U','\0'};
	char c_aav_arr4[] = "to";
	char c_aav_arr5[] = "REAL TIME RENDERING BATCH OF 2020-21";

	char c_aav_arr6[] = {'H','E','L','L','O','\0'};

	printf("\n");
	printf("Size of Array1 :%lu\n", sizeof(c_aav_arr1));
	printf("Size of Array2 :%lu\n", sizeof(c_aav_arr2));
	printf("Size of Array3 :%lu\n", sizeof(c_aav_arr3));
	printf("Size of Array4 :%lu\n", sizeof(c_aav_arr4));
	printf("Size of Array5 :%lu\n", sizeof(c_aav_arr5));

	printf("\n");
	printf("The String Are :\n\n");
	printf("Size of Array1 :%s\n", c_aav_arr1);
	printf("Size of Array2 :%s\n", c_aav_arr2);
	printf("Size of Array3 :%s\n", c_aav_arr3);
	printf("Size of Array4 :%s\n", c_aav_arr4);
	printf("Size of Array5 :%s\n", c_aav_arr5);

	printf("\n");
	printf("Size of c_aav_arr5 : %lu\n\n", sizeof(c_aav_arr6));
	printf("c_aav_arr:%s\n\n", c_aav_arr6);

	return(0);
}