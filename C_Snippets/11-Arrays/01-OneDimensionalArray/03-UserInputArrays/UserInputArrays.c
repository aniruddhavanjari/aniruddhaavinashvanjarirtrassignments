#include <stdio.h>

#define INT_ARRAY_NUM_ELEMENTS 5
#define FLOAT_ARRAY_NUM_ELEMENTS 3
#define CHAR_ARRAY_NUM_ELEMENTS 15

int main(void)
{
	int i_aav_arr[INT_ARRAY_NUM_ELEMENTS];
	float f_aav_arr[FLOAT_ARRAY_NUM_ELEMENTS];
	char c_aav_arr[CHAR_ARRAY_NUM_ELEMENTS];
	int i_aav_i, i_aav_num;

	/*********Array Elemensts INPUT ****************/
	printf("\n");
	printf("Enter Elements For 'Integer' Array:\n");
	for (i_aav_i = 0; i_aav_i < INT_ARRAY_NUM_ELEMENTS; i_aav_i++)
		scanf("%d", &i_aav_arr[i_aav_i]);

	printf("\n");
	printf("Enter Elements For 'Float' Array:\n");
	for (i_aav_i = 0; i_aav_i < FLOAT_ARRAY_NUM_ELEMENTS; i_aav_i++)
		scanf("%f", &f_aav_arr[i_aav_i]);

	printf("\n");
	printf("Enter Elements For 'Character' Array:\n");
	for (i_aav_i = 0; i_aav_i < CHAR_ARRAY_NUM_ELEMENTS; i_aav_i++)
	{
		c_aav_arr[i_aav_i] = getch();
		printf("%c\n",c_aav_arr[i_aav_i]);
	}

	/*************Array Elements OutPut*****************/
	printf("\n");
	printf("Display:\n");
	printf("Elements For 'Integer' Array:\n");
	for (i_aav_i = 0; i_aav_i < INT_ARRAY_NUM_ELEMENTS; i_aav_i++)
		printf("%d\n", i_aav_arr[i_aav_i]);

	printf("\n");
	printf("Elements For 'Float' Array:\n");
	for (i_aav_i = 0; i_aav_i < FLOAT_ARRAY_NUM_ELEMENTS; i_aav_i++)
		printf("%f\n", f_aav_arr[i_aav_i]);

	printf("\n");
	printf("Elements For 'Character' Array:\n");
	for (i_aav_i = 0; i_aav_i < CHAR_ARRAY_NUM_ELEMENTS; i_aav_i++)
		printf("%c\n", c_aav_arr[i_aav_i]);
	
	return(0);
}


