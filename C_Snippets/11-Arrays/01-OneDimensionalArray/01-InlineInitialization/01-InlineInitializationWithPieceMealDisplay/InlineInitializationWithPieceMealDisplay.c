#include <stdio.h>
int main(void)
{
	int i_aav_arr[] = { 30 , 31, 32, 33,  34, 35, 36, 37, 38, 39 };
	int i_aav_intsize;
	int i_aav_ArrSize;
	int i_aav_numbElements;

	float f_aav_arr[] = { 3.1f , 3.2f, 3.2f, 3.3f,  3.4f, 3.5f, 3.6f, 3.7f, 3.8f, 3.9f };
	int i_aav_floatsize;
	int i_aav_fArrSize;
	int i_aav_fnumbElements;

	char c_aav_arr[] = { 'A', 'N', 'I', 'R', 'U', 'D', 'D', 'H', 'A' };
	char i_aav_charsize;
	char i_aav_cArrSize;
	char i_aav_cnumbElements;

	printf("\n\n");
	printf("In-Line  Initialization and Piece-meal Display of Elemnts of Arrary \n\n");

	printf("int Array[0] 1 Elements = %d\n", i_aav_arr[0]);
	printf("int Array[1] 2 Elements = %d\n", i_aav_arr[1]);
	printf("int Array[2] 3 Elements = %d\n", i_aav_arr[2]);
	printf("int Array[3] 4 Elements = %d\n", i_aav_arr[3]);
	printf("int Array[4] 5 Elements = %d\n", i_aav_arr[4]);
	printf("int Array[5] 6 Elements = %d\n", i_aav_arr[5]);
	printf("int Array[6] 7 Elements = %d\n", i_aav_arr[6]);
	printf("int Array[7] 8 Elements = %d\n", i_aav_arr[7]);
	printf("int Array[8] 9 Elements = %d\n", i_aav_arr[8]);
	printf("int Array[9] 10 Elements = %d\n", i_aav_arr[9]);

	i_aav_intsize = sizeof(int);
	i_aav_ArrSize = sizeof(i_aav_arr);
	i_aav_numbElements = i_aav_ArrSize / i_aav_intsize;
	printf("Size of Data type 'int'=  %d bytes\n", i_aav_intsize);

	printf("Number of elements in 'int' Array =  %d Elemensts\n", i_aav_intsize);

	printf("Size of Array (%d Elements * %d Bytes ) = %d Bytes\n", i_aav_numbElements, i_aav_intsize, i_aav_ArrSize);


	//float array
	printf("\n\n");
	printf("In-Line  Initialization and Piece-meal Display of Elemnts of float Arrary \n\n");

	printf("float Array[0] 1  Elements = %f\n", f_aav_arr[0]);
	printf("float Array[1] 2  Elements = %f\n", f_aav_arr[1]);
	printf("float Array[2] 3  Elements = %f\n", f_aav_arr[2]);
	printf("float Array[3] 4  Elements = %f\n", f_aav_arr[3]);
	printf("float Array[4] 5  Elements = %f\n", f_aav_arr[4]);
	printf("float Array[5] 6  Elements = %f\n", f_aav_arr[5]);
	printf("float Array[6] 7  Elements = %f\n", f_aav_arr[6]);
	printf("float Array[7] 8  Elements = %f\n", f_aav_arr[7]);
	printf("float Array[8] 9  Elements = %f\n", f_aav_arr[8]);
	printf("float Array[9] 10 Elements = %f\n", f_aav_arr[9]);

	i_aav_floatsize = sizeof(float);
	i_aav_fArrSize = sizeof(f_aav_arr);
	i_aav_fnumbElements = i_aav_fArrSize / i_aav_floatsize;
	printf("Size of Data type 'float'=  %d bytes\n", i_aav_floatsize);

	printf("Number of elemensts in 'float' Array =  %d Elemensts\n", i_aav_fArrSize);

	printf("Size of Array is %d Elements * %d Bytes  =  %d bytes\n", i_aav_fnumbElements, i_aav_fArrSize, i_aav_fArrSize);


	//Charater array
	printf("\n\n");
	printf("In-Line  Initialization and Piece-meal Display of Elemnts of Charater  Arrary \n\n");

	printf("char Array[0] 1 Elements = %c\n", c_aav_arr[0]);
	printf("char Array[1] 2 Elements = %c\n", c_aav_arr[1]);
	printf("char Array[2] 3 Elements = %c\n", c_aav_arr[2]);
	printf("char Array[3] 4 Elements = %c\n", c_aav_arr[3]);
	printf("char Array[4] 5 Elements = %c\n", c_aav_arr[4]);
	printf("char Array[5] 6 Elements = %c\n", c_aav_arr[5]);
	printf("char Array[6] 7 Elements = %c\n", c_aav_arr[6]);
	printf("char Array[7] 8 Elements = %c\n", c_aav_arr[7]);
	printf("char Array[8] 9 Elements = %c\n", c_aav_arr[8]);
	

	i_aav_charsize = sizeof(char);
	i_aav_cArrSize = sizeof(c_aav_arr);
	i_aav_cnumbElements = i_aav_cArrSize / i_aav_charsize;
	printf("Size of Data type 'char'=  %d Bytes\n", i_aav_charsize);

	printf("Number of elements in 'char' Array =  %d Elemensts\n", i_aav_cnumbElements);

	printf("Size of Array (%d Elements * %d Bytes ) = %d Bytes\n", i_aav_cnumbElements, i_aav_charsize, i_aav_cArrSize);

	return(0);
}




