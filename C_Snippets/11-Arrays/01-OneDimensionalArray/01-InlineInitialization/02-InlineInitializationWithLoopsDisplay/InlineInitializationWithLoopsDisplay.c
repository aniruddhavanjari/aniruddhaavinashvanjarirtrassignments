#include <stdio.h>

int main(void)
{
	int i_aav_arr[] = {11,12,13,14,15,16,17,18,19,20};
	int i_aav_iSize; 
	int i_aav_iArraySize;
	int i_aav_iArrayElements;

	float f_aav_arr[] = { 11.1, 12.2, 13.4, 14.5, 15.6, 16.7,17.8,18.8,19.9,20.4 };
	int i_aav_fSize;
	int i_aav_fArraySize;
	int i_aav_fArrayElements;

	char c_aav_arr[] = {'A','N','I','R','U','D','D','H','A'};
	int i_aav_cSize;
	int i_aav_cArraySize;
	int i_aav_cArrayElements;

	int i_aav_i;

	/*****Integer Array************/
	printf("\n");
	printf("In - Line Initialization \n\n");

	i_aav_iSize = sizeof(int);
	i_aav_iArraySize = sizeof(i_aav_arr);
	i_aav_iArrayElements = i_aav_iArraySize / i_aav_iSize;

	for (i_aav_i = 0; i_aav_i < i_aav_iArrayElements; i_aav_i++)
	{
		printf("Integer Array [%d] = %d\n", i_aav_i, i_aav_arr[i_aav_i]);
	}

	printf("\n");
	printf("Size of Data type 'int' = %d bytes\n",i_aav_iSize);
	printf("Number of Elements in integer Array = %d Elements\n", i_aav_iArrayElements);
	printf("Size of integer Array %d", i_aav_iArraySize);

	/**********Float Arrar*********************/
	printf("\n");
	printf("In - Line Initialization \n\n");

	i_aav_fSize = sizeof(int);
	i_aav_fArraySize = sizeof(f_aav_arr);
	i_aav_fArrayElements = i_aav_fArraySize / i_aav_fSize;

	for (i_aav_i = 0; i_aav_i < i_aav_fArrayElements; i_aav_i++)
	{
		printf("Integer Array [%d] = %f\n", i_aav_i, f_aav_arr[i_aav_i]);
	}

	printf("\n");
	printf("Size of Data type 'float' = %d bytes\n", i_aav_fSize);
	printf("Number of Elements in float Array = %d Elements\n", i_aav_fArrayElements);
	printf("Size of float Array %d", i_aav_fArraySize);


	/************Character Array**************/
	printf("\n");
	printf("In - Line Initialization \n\n");

	i_aav_cSize = sizeof(char);
	i_aav_cArraySize = sizeof(c_aav_arr);
	i_aav_cArrayElements = i_aav_cArraySize / i_aav_cSize;

	for (i_aav_i = 0; i_aav_i < i_aav_cArrayElements; i_aav_i++)
	{
		printf("Integer Array [%d] = %c\n", i_aav_i, c_aav_arr[i_aav_i]);
	}

	printf("\n");
	printf("Size of Data type 'char' = %d bytes\n", i_aav_cSize);
	printf("Number of Elements in char Array = %d Elements\n", i_aav_cArrayElements);
	printf("Size of character Array %d", i_aav_cArraySize);

	return(0);
}