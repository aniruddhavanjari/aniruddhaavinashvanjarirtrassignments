#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	int i_aav_iArray[NUM_ELEMENTS];
	int i_aav_i, i_aav_num, i_aav_j, i_aav_count = 0; 

	printf("\n");

	printf("Enter Integer Elements in Array: \n\n");
	for (i_aav_i = 0; i_aav_i < NUM_ELEMENTS; i_aav_i++)
	{
		scanf("%d", &i_aav_num);

		if (i_aav_num < 0)
		{
			i_aav_num = -1 * i_aav_num;
		}
		i_aav_iArray[i_aav_i] = i_aav_num;
	}

	printf("\n");
	printf("Array Elements :\n\n");
	for (i_aav_i = 0; i_aav_i < NUM_ELEMENTS; i_aav_i++)
	{
		printf("%d\n", i_aav_iArray[i_aav_i]);
	}

	printf("\n");
	printf("Prime Number in Array\n");
	for (i_aav_i = 0; i_aav_i < NUM_ELEMENTS; i_aav_i++)
	{
		for (i_aav_j = 1; i_aav_j <= i_aav_iArray[i_aav_i]; i_aav_j++)
		{
			if ((i_aav_iArray[i_aav_i] % i_aav_j) == 0)
			{
				i_aav_count++;
			}
		}
		if (i_aav_count == 2)
		{
			printf("%d\n", i_aav_iArray[i_aav_i]);
		}
		i_aav_count = 0; 
	}
	return(0);
}




