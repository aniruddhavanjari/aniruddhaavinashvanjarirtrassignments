#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	int i_aav_array[NUM_ELEMENTS];
	int i_aav_i, i_aav_num, i_aav_sum = 0;

	printf("\n");

	printf("Enter Integer Array: \n");
	for (i_aav_i = 0; i_aav_i < NUM_ELEMENTS; i_aav_i++)
	{
		scanf("%d", &i_aav_num);
		i_aav_array[i_aav_i] = i_aav_num;
	}

	printf("\n\n");
	printf("Evene Nubmers in Array\n");
	for (i_aav_i = 0; i_aav_i < NUM_ELEMENTS; i_aav_i++)
	{
		if ((i_aav_array[i_aav_i] % 2) == 0)
		{
			printf("%d\n", i_aav_array[i_aav_i]);
		}
	}

	printf("\n");
	printf("Odd Number in Array:\n");
	for (i_aav_i = 0; i_aav_i < NUM_ELEMENTS; i_aav_i++)
	{
		if ((i_aav_array[i_aav_i] % 2) != 0)
		{
			printf("%d\n", i_aav_array[i_aav_i]);
		}
	}

	return(0);
}