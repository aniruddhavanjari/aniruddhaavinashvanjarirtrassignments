#include <stdio.h>

#define NUM_ELEMENTS 10

int main(void)
{
	int i_aav_array[NUM_ELEMENTS];
	int i_aav_i, i_aav_num, i_aav_sum = 0;

	printf("\n");

	printf("Enter Integer Array: \n");
	for (i_aav_i = 0; i_aav_i < NUM_ELEMENTS; i_aav_i++)
	{
		scanf("%d", &i_aav_num);
		i_aav_array[i_aav_i] = i_aav_num;
	}

	for (i_aav_i = 0; i_aav_i < NUM_ELEMENTS; i_aav_i++)
	{
		i_aav_sum = i_aav_sum + i_aav_array[i_aav_i];
	}

	printf("\n");
	printf("Sum of All Elemets of Array : %d\n",i_aav_sum);

	return(0);
}