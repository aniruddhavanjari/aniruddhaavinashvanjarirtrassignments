#include<stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3

int main(void)
{
	//Variable
	int i_aav_Array2D[NUM_ROWS][NUM_COLUMNS];

	int i_aav_Array1D[NUM_ROWS*NUM_COLUMNS];

	int i_aav_i, i_aav_j;
	int i_aav_num;

	printf("Enter Elelmnts in 2D Array:\n");
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		printf("From Row Number %d :\n", (i_aav_i + 1));
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS; i_aav_j++)
		{
			printf("Enter the Elements %d :\n", (i_aav_j + 1));
			scanf("%d", &i_aav_num);
			i_aav_Array2D[i_aav_i][i_aav_j] = i_aav_num;
		}
	}

	//Display 2D Array
	printf("\n");
	printf("Enter Elelmnts in 2D Array:\n");
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		printf("From Row Number %d :\n", (i_aav_i + 1));
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS; i_aav_j++)
		{
			printf("2D Array[%d][%d] =  %d\n",i_aav_i,i_aav_j,i_aav_Array2D[i_aav_i][i_aav_j]);
		}
		printf("\n");
	}

	//Converting 2D Array to 1D
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS; i_aav_j++)
		{
			i_aav_Array1D[(i_aav_i * NUM_COLUMNS) + i_aav_j] = i_aav_Array2D[i_aav_i][i_aav_j];
		}
	}
	printf("\n");
	printf("One Dimention Array of Integers:\n");
	for (i_aav_i = 0; i_aav_i < (NUM_ROWS*NUM_COLUMNS); i_aav_i++)
	{
		printf("1D Array[%d] = %d\n", i_aav_i, i_aav_Array1D[i_aav_i]);
	}
	printf("\n");
	return(0);
}