#include <stdio.h>

int main(void)
{
	int i_aav_arr[3][5];
	int i_aav_iSize;
	int i_aav_iArraySize;
	int i_aav_iArrayElements, i_aav_iArrayRows, i_aav_iArrayColums;
	int i_aav_i, i_aav_j;
	//Code
	printf("\n\n");
	i_aav_iSize = sizeof(int);

	i_aav_iArraySize = sizeof(i_aav_arr);
	printf("Size of Two Dimention Integer Array = %d\n\n", i_aav_iArraySize);

	i_aav_iArrayRows = i_aav_iArraySize / sizeof(i_aav_arr[0]);
	printf("Number of Row In Two Dimensional Integet Array %d \n\n", i_aav_iArrayRows);

	i_aav_iArrayColums = sizeof(i_aav_arr[0]) / i_aav_iSize;
	printf("Nuber of Columns In Two Dimension Integer Array %d \n\n", i_aav_iArrayColums);

	i_aav_iArrayElements = i_aav_iArrayRows * i_aav_iArrayColums;
	printf("Number of Elements in 2D integer Array is %d \n", i_aav_iArrayElements);

	printf("\n");
	printf("Elements in 2d Array :\n\n");

	i_aav_arr[0][0] = 1;
	i_aav_arr[0][1] = 2;
	i_aav_arr[0][2] = 3;
	i_aav_arr[0][3] = 4;
	i_aav_arr[0][4] = 5;

	i_aav_arr[1][0] = 6;
	i_aav_arr[1][1] = 7;
	i_aav_arr[1][2] = 8;
	i_aav_arr[1][3] = 9;
	i_aav_arr[1][4] = 10;

	i_aav_arr[2][0] = 11;
	i_aav_arr[2][1] = 12;
	i_aav_arr[2][2] = 13;
	i_aav_arr[2][3] = 14;
	i_aav_arr[2][4] = 15;


	for (i_aav_i = 0; i_aav_i < i_aav_iArrayRows; i_aav_i++)
	{
		printf("******ROW %d*******\n",(i_aav_i+1));
			for (i_aav_j = 0; i_aav_j < i_aav_iArrayColums; i_aav_j++)
			{
				printf("Integer Array[%d][%d] = [%d]\n", i_aav_i, i_aav_j, i_aav_arr[i_aav_i][i_aav_j]);
			}
		printf("\n\n");
	}

	return(0);
}
