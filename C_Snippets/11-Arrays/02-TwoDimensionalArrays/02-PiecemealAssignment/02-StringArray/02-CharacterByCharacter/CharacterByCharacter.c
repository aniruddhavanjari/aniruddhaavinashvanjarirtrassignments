#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{

	char i_aav_strArray[6][10];

	int i_aav_charSize;
	int i_aav_strArraySize;
	int i_aav_strArrayNumberElements, i_aav_strArrayNumberRows, i_aav_strArrayNumberColumns;
	int i_aav_strActualNumChar = 0;
	int i_aav_i;

	printf("\n\n");
	i_aav_charSize = sizeof(char);

	i_aav_strArraySize = sizeof(i_aav_strArray);
	printf("Size of 2D Character Array = %d\n\n", i_aav_strArraySize);

	i_aav_strArrayNumberRows = i_aav_strArraySize / sizeof(i_aav_strArray[0]);
	printf("Numberof Rows in 2D Array = %d\n", i_aav_strArrayNumberRows);

	i_aav_strArrayNumberColumns = sizeof(i_aav_strArray[0]) / i_aav_charSize;
	printf("Numberof Columns in 2D Array = %d\n", i_aav_strArrayNumberColumns);

	i_aav_strArrayNumberElements = i_aav_strArrayNumberRows * i_aav_strArrayNumberColumns;
	printf("Maximun Nuber of Elements in 2D Array = %d\n ", i_aav_strArrayNumberElements);

	i_aav_strArray[0][0] = 'M';
	i_aav_strArray[0][1] = 'y';
	i_aav_strArray[0][2] = '\0';

	i_aav_strArray[1][0] = 'N';
	i_aav_strArray[1][1] = 'a';
	i_aav_strArray[1][2] = 'm';
	i_aav_strArray[1][3] = 'e';
	i_aav_strArray[1][4] = '\0';

	i_aav_strArray[2][0] = 'I';
	i_aav_strArray[2][1] = 's';
	i_aav_strArray[2][2] = '\0';

	i_aav_strArray[3][0] = 'A';
	i_aav_strArray[3][1] = 'n';
	i_aav_strArray[3][2] = 'i';
	i_aav_strArray[3][3] = 'r';
	i_aav_strArray[3][4] = 'u';
	i_aav_strArray[3][5] = 'd';
	i_aav_strArray[3][6] = 'd';
	i_aav_strArray[3][7] = 'h';
	i_aav_strArray[3][8] = 'a';
	i_aav_strArray[3][9] = '\0';

	i_aav_strArray[4][0] = 'A';
	i_aav_strArray[4][1] = 'v';
	i_aav_strArray[4][2] = 'i';
	i_aav_strArray[4][3] = 'n';
	i_aav_strArray[4][4] = 'a';
	i_aav_strArray[4][5] = 's';
	i_aav_strArray[4][6] = 'h';
	i_aav_strArray[4][7] = '\0';

	i_aav_strArray[5][0] = 'V';
	i_aav_strArray[5][1] = 'a';
	i_aav_strArray[5][2] = 'n';
	i_aav_strArray[5][3] = 'j';
	i_aav_strArray[5][4] = 'a';
	i_aav_strArray[5][5] = 'r';
	i_aav_strArray[5][6] = 'i';
	i_aav_strArray[5][7] = '\0';
	
	printf("\nStrings in 2D Array:\n");
	for (i_aav_i = 0; i_aav_i < i_aav_strArrayNumberRows; i_aav_i++)
	{
		printf("%s ", i_aav_strArray[i_aav_i]);
	}
	printf("\n");

	return(0);
}

