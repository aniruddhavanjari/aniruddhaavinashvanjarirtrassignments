#include <stdio.h>

int main(void)
{
	int i_aav_arr[5][3] = { {1,2,3},{2,4,6},{3,6,9},{4,8,12},{5,10,15} };
	int i_aav_iSize;
	int i_aav_iArraySize;
	int i_aav_iArrayElements, i_aav_iArrayRows, i_aav_iArrayColums;

	//Code
	printf("\n\n");
	i_aav_iSize = sizeof(int);

	i_aav_iArraySize = sizeof(i_aav_arr);
	printf("Size of Two Dimention Integer Array %d\n\n", i_aav_iArraySize);
	
	i_aav_iArrayRows = i_aav_iArraySize / sizeof(i_aav_arr[0]);
	printf("Number of Row In Two Dimensional Integet Array %d \n\n", i_aav_iArrayRows);

	i_aav_iArrayColums = sizeof(i_aav_arr[0]) / i_aav_iSize;
	printf("Nuber of Columns In Two Dimension Integer Array %d \n\n", i_aav_iArrayColums);

	i_aav_iArrayElements = i_aav_iArrayRows * i_aav_iArrayColums;
	printf("Number of Elements in 2D integer Array is %d \n", i_aav_iArrayElements);

	printf("\n");
	printf("Elements in 2d Array :\n\n");

	printf("******Row 1 ********\n");
	printf("Integer Array[0][0] = %d\n", i_aav_arr[0][0]);

	printf("Integer Array[0][1] = %d\n", i_aav_arr[0][2]);

	printf("Integer Array[0][2] = %d\n", i_aav_arr[0][3]);

	printf("******Row 2 ********\n");
	printf("Integer Array[1][0] = %d\n", i_aav_arr[1][0]);

	printf("Integer Array[1][1] = %d\n", i_aav_arr[1][1]);

	printf("Integer Array[1][2] = %d\n", i_aav_arr[1][2]);

	printf("******Row 3 ********\n");
	printf("Integer Array[2][0] = %d\n", i_aav_arr[2][0]);

	printf("Integer Array[2][1] = %d\n", i_aav_arr[2][1]);

	printf("Integer Array[2][2] = %d\n", i_aav_arr[2][2]);

	printf("******Row 4 ********\n");
	printf("Integer Array[3][0] = %d\n", i_aav_arr[3][0]);

	printf("Integer Array[3][1] = %d\n", i_aav_arr[3][0]);

	printf("Integer Array[3][2] = %d\n", i_aav_arr[3][0]);

	printf("******Row 4 ********\n");
	printf("Integer Array[4][0] = %d\n", i_aav_arr[4][0]);

	printf("Integer Array[4][1] = %d\n", i_aav_arr[4][1]);

	printf("Integer Array[4][2] = %d\n", i_aav_arr[4][2]);

	printf("\n\n");

	return(0);
}