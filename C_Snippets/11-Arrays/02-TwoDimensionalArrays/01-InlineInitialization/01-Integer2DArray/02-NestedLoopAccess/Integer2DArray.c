#include <stdio.h>

int main(void)
{
	int i_aav_arr[5][3] = { {1,2,3},{2,4,6},{3,6,9},{4,8,12},{5,10,15} };
	int i_aav_iSize;
	int i_aav_iArraySize;
	int i_aav_iArrayElements, i_aav_iArrayRows, i_aav_iArrayColums;
	int i_aav_i, i_aav_j;

	printf("\n");

	i_aav_iSize = sizeof(int);

	printf("\n\n");
	i_aav_iSize = sizeof(int);

	i_aav_iArraySize = sizeof(i_aav_arr);
	printf("Size of Two Dimention Integer Array %d\n\n", i_aav_iArraySize);

	i_aav_iArrayRows = i_aav_iArraySize / sizeof(i_aav_arr[0]);
	printf("Number of Row In Two Dimensional Integet Array %d \n\n", i_aav_iArrayRows);

	i_aav_iArrayColums = sizeof(i_aav_arr[0]) / i_aav_iSize;
	printf("Nuber of Columns In Two Dimension Integer Array %d \n\n", i_aav_iArrayColums);

	i_aav_iArrayElements = i_aav_iArrayRows * i_aav_iArrayColums;
	printf("Number of Elements in 2D integer Array is %d \n", i_aav_iArrayElements);

	printf("\n");
	printf("Elements in 2d Array :\n\n");

	for (i_aav_i = 0; i_aav_i < i_aav_iArrayRows; i_aav_i++)
	{
		printf("************ROW %d *************\n", (i_aav_i + 1));
		for (i_aav_j = 0; i_aav_j < i_aav_iArrayColums; i_aav_j++)
		{
			printf("Integer Array[%d][%d] = [%d]\n", i_aav_i, i_aav_j, i_aav_arr[i_aav_i][i_aav_j]);
		}
		printf("\n\n");
	}

	return(0);
}

