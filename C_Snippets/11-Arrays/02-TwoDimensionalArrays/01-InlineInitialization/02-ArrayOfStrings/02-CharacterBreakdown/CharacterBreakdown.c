#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrlen(char[]);

	char i_aav_strArray[10][15] = { "HELLO","WELCOME","TO","REAL","TIME","RENDERING","BATCH","(2020-21)","OF","ASTROMEDICOMP." };

	int i_aav_iStringLength[10];

	int i_aav_strArray_Size;
	int i_aav_strArrayRows;
	int i_aav_i, i_aav_j;

	i_aav_strArray_Size = sizeof(i_aav_strArray);
	i_aav_strArrayRows = i_aav_strArray_Size / sizeof(i_aav_strArray[0]);

	for (i_aav_i = 0; i_aav_i < i_aav_strArrayRows; i_aav_i++)
	{
		i_aav_iStringLength[i_aav_i] = MyStrlen(i_aav_strArray[i_aav_i]);
	}

	printf("\nThe Entire String Array:\n");
	for (i_aav_i = 0; i_aav_i < i_aav_strArrayRows; i_aav_i++)
	{
		printf("%s", i_aav_strArray[i_aav_i]);
	}

	printf("\n\n");
	printf("String In the 2D Array :\n\n");

	for (i_aav_i = 0; i_aav_i < i_aav_strArrayRows; i_aav_i++)
	{
		printf("String Number %d => %s\n\n", (i_aav_i + 1), i_aav_strArray[i_aav_i]);
		for (i_aav_j = 0; i_aav_j < i_aav_iStringLength[i_aav_i]; i_aav_j++)
		{
			printf("Chracter %d = %c\n", (i_aav_j + 1), i_aav_strArray[i_aav_i][i_aav_j]);
		}
		printf("\n\n");
	}
	return(0);
}

int MyStrlen(char i_aav_str[])
{
	int i_aav_j;
	int i_aav_stringLength = 0;

	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (i_aav_str[i_aav_j] == '\0')
			break;
		else
			i_aav_stringLength++;
	}
	return(i_aav_stringLength);
}