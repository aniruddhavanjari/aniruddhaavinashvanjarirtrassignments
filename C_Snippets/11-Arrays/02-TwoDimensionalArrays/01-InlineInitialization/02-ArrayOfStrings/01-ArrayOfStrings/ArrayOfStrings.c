#include <stdio.h>

#define MAX_STRING_LENGTH 512

int main(void)
{
	int MyStrlen(char[]);

	char i_aav_strArray[10][15] = { "HELLO","WELCOME","TO","REAL","TIME","RENDERING","BATCH","(2020-21)","OF","ASTROMEDICOMP." };

	int i_aav_charSize;
	int i_aav_strArraySize;
	int i_aav_strArrayNumberElements, i_aav_strArrayNumberRows, i_aav_strArrayNumberColumns;
	int i_aav_strActualNumChar = 0;
	int i_aav_i;

	printf("\n\n");
	i_aav_charSize = sizeof(char);

	i_aav_strArraySize = sizeof(i_aav_strArray);
	printf("Size of 2D Character Array = %d\n\n", i_aav_strArraySize);

	i_aav_strArrayNumberRows = i_aav_strArraySize / sizeof(i_aav_strArray[0]);
	printf("Numberof Rows in 2D Array = %d\n", i_aav_strArrayNumberRows);

	i_aav_strArrayNumberColumns = sizeof(i_aav_strArray[0]) / i_aav_charSize;
	printf("Numberof Columns in 2D Array = %d\n", i_aav_strArrayNumberColumns);

	i_aav_strArrayNumberElements = i_aav_strArrayNumberRows * i_aav_strArrayNumberColumns;
	printf("Maximun Nuber of Elements in 2D Array = %d\n ", i_aav_strArrayNumberElements);

	for (i_aav_i = 0; i_aav_i < i_aav_strArrayNumberRows; i_aav_i++)
	{
		i_aav_strActualNumChar = i_aav_strActualNumChar + MyStrlen(i_aav_strArray[i_aav_i]);
	}
	printf("Actual Number of Charater in 2D Array = %d\n", i_aav_strActualNumChar);

	printf("\nStrings in 2D Array:\n");

	printf("%s ", i_aav_strArray[0]);
	printf("%s ", i_aav_strArray[1]);
	printf("%s ", i_aav_strArray[2]);
	printf("%s ", i_aav_strArray[3]);
	printf("%s ", i_aav_strArray[4]);
	printf("%s ", i_aav_strArray[5]);
	printf("%s ", i_aav_strArray[6]);
	printf("%s ", i_aav_strArray[7]);
	printf("%s ", i_aav_strArray[8]);
	printf("%s ", i_aav_strArray[9]);

	return(0);
}

int MyStrlen(char i_aav_str[])
{
	int i_aav_j;
	int i_aav_stringLength = 0;

	for (i_aav_j = 0; i_aav_j < MAX_STRING_LENGTH; i_aav_j++)
	{
		if (i_aav_str[i_aav_j] == '\0')
			break;
		else
			i_aav_stringLength++;
	}
	return(i_aav_stringLength);
}