#include <stdio.h>

int main(viod)
{
	//variable declaration
	int i_aav_Array[5];
	int i_aav_ArrayTwo[5][3];
	int i_aav_ArrayThree[100][100][5];

	int i_aav_numRow_2D;
	int i_aav_numCol_2D;


	int i_aav_numRow_3D;
	int i_aav_numCol_3D;
	int i_aav_depth_3D;

	//Code
	printf("\n\n");
	printf("Size of 1D Array = %lu\n", sizeof(i_aav_Array));
	printf("Number of elements in 1D Array = %lu\n", sizeof(i_aav_Array) / sizeof(int));
	
	//2D
	printf("\n\n");
	printf("Size of 2D Array = %lu\n", sizeof(i_aav_ArrayTwo));

	i_aav_numRow_2D = (sizeof(i_aav_ArrayTwo)) / sizeof(i_aav_ArrayTwo[0]);
	printf("Number of Row in Integers 2D Array= %lu\n", i_aav_numRow_2D);

	i_aav_numCol_2D = (sizeof(i_aav_ArrayTwo[0])) / (sizeof(i_aav_ArrayTwo[0][0]));
	printf("Number of Columns in Integers 2D Array= %lu\n", (sizeof(i_aav_ArrayTwo[0])) / (sizeof(i_aav_ArrayTwo[0][0])));

	printf("Number of Elements in Integers 2D Array= %lu\n", (i_aav_numRow_2D*i_aav_numCol_2D));

	//3D
	printf("\n\n");
	printf("Size of 3D Array = %lu\n", sizeof(i_aav_ArrayTwo));

	i_aav_numRow_3D = (sizeof(i_aav_ArrayThree)) / (sizeof(i_aav_ArrayThree[0]));
	printf("Number of Row in Integers 3D Array= %lu\n", i_aav_numRow_3D);

	i_aav_numCol_3D = (sizeof(i_aav_ArrayThree[0])) / (sizeof(i_aav_ArrayThree[0][0]));
	printf("Number of Columns in Integers 3D Array= %lu\n", i_aav_numCol_3D);

	i_aav_depth_3D = (sizeof(i_aav_ArrayThree[0][0])) / (sizeof(i_aav_ArrayThree[0][0][0]));
	printf("Number of Depth in Integers 3D Array= %lu\n", i_aav_depth_3D);

	printf("Number of Elements in Integers 3D Array= %lu\n", (i_aav_numRow_3D * i_aav_numCol_3D *i_aav_depth_3D));

	printf("\n\n");

	return(0);
}