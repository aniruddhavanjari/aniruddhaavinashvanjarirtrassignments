#include <stdio.h>

int main(void)
{
	//vatiable declatation
	int i_aav_Array[5][3][2] = { {{1,2} , {3,4},{5,6}},
								 {{7,8},{9,1},{10,11}},
								 {{12,13},{14,15},{16,17}},
								 {{18,19},{20,21},{22,23}},
								 {{24,25},{26,27},{28,29}} };
	int i_aav_intSize;
	int i_aav_ArraySize;
	int i_aav_ArrayNumElements, i_aav_ArrayWidth, i_aav_ArrayHeight, i_aav_ArrayDepth;

	printf("\n");
	
	i_aav_intSize = sizeof(int);
	i_aav_ArraySize = sizeof(i_aav_Array);

	printf("Size of 3d Integer Array is = %d\n", i_aav_ArraySize);

	i_aav_ArrayWidth = i_aav_ArraySize / sizeof(i_aav_Array[0]);
	printf("Number of Rows In 3d Array = %d\n", i_aav_ArrayWidth);

	i_aav_ArrayHeight = sizeof(i_aav_Array[0]) / sizeof(i_aav_Array[0][0]);
	printf("Number of Columns in 3d Array = %d\n", i_aav_ArrayHeight);

	i_aav_ArrayDepth = sizeof(i_aav_Array[0][0]) / i_aav_intSize;
	printf("Depth of 3d Array = %d\n ", i_aav_ArrayDepth);

	printf("\n\n");

	printf("Elements in Integer 3D Array:\n\n");

	printf("****** Rows 1 ********\n");
	printf("**** COLUMN 1 ******\n");
	printf("Array[0][0][0] = %d\n", i_aav_Array[0][0][0]);
	printf("Array[0][0][1] = %d\n", i_aav_Array[0][0][1]);
	printf("\n");

	
	printf("**** COLUMN 2 ******\n");
	printf("Array[0][1][0] = %d\n", i_aav_Array[0][1][0]);
	printf("Array[0][1][1] = %d\n", i_aav_Array[0][1][1]);
	printf("\n");

	printf("**** COLUMN 3 ******\n");
	printf("Array[0][2][0] = %d\n", i_aav_Array[0][2][0]);
	printf("Array[0][2][1] = %d\n", i_aav_Array[0][2][1]);
	printf("\n");

	printf("****** Rows 2 ********\n");
	printf("**** COLUMN 1 ******\n");
	printf("Array[1][0][0] = %d\n", i_aav_Array[1][0][0]);
	printf("Array[1][0][1] = %d\n", i_aav_Array[1][0][1]);
	printf("\n");

	printf("**** COLUMN 2 ******\n");
	printf("Array[1][1][0] = %d\n", i_aav_Array[1][1][0]);
	printf("Array[1][1][1] = %d\n", i_aav_Array[1][1][1]);
	printf("\n");

	printf("**** COLUMN 3 ******\n");
	printf("Array[1][2][0] = %d\n", i_aav_Array[1][2][0]);
	printf("Array[1][2][1] = %d\n", i_aav_Array[1][2][1]);
	printf("\n");

	////////////////////////////////////
	printf("*****Rows 3******\n");
	printf("**** COLUMN 1 ******\n");
	printf("Array[2][1][0] = %d\n", i_aav_Array[2][0][0]);
	printf("Array[2][1][1] = %d\n", i_aav_Array[2][0][1]);
	printf("\n");

	printf("**** COLUMN 2 ******\n");
	printf("Array[2][1][0] = %d\n", i_aav_Array[2][1][0]);
	printf("Array[2][1][1] = %d\n", i_aav_Array[2][1][1]);
	printf("\n");

	printf("**** COLUMN 3 ******\n");
	printf("Array[2][2][0] = %d\n", i_aav_Array[2][2][0]);
	printf("Array[2][2][1] = %d\n", i_aav_Array[2][2][1]);
	printf("\n");

	///////////////////////////////////////
	printf("*********Rows 4********\n");
	printf("**** COLUMN 1 ******\n");
	printf("Array[3][0][0] = %d\n", i_aav_Array[3][0][0]);
	printf("Array[3][0][1] = %d\n", i_aav_Array[3][0][1]);
	printf("\n");

	printf("**** COLUMN 2 ******\n");
	printf("Array[3][1][0] = %d\n", i_aav_Array[3][1][0]);
	printf("Array[3][1][1] = %d\n", i_aav_Array[3][1][1]);
	printf("\n");

	printf("**** COLUMN 3 ******\n");
	printf("Array[3][2][0] = %d\n", i_aav_Array[3][2][0]);
	printf("Array[3][2][1] = %d\n", i_aav_Array[3][2][1]);
	printf("\n");

	/////////////////////////////////////
	printf("**********Row 5******\n");
	printf("**** COLUMN 1 ******\n");
	printf("Array[4][0][0] = %d\n", i_aav_Array[4][0][0]);
	printf("Array[4][0][1] = %d\n", i_aav_Array[4][0][1]);
	printf("\n");

	printf("**** COLUMN 2 ******\n");
	printf("Array[4][1][0] = %d\n", i_aav_Array[4][1][0]);
	printf("Array[4][1][1] = %d\n", i_aav_Array[4][1][1]);
	printf("\n");


	printf("**** COLUMN 2 ******\n");
	printf("Array[4][2][0] = %d\n", i_aav_Array[4][2][0]);
	printf("Array[4][2][1] = %d\n", i_aav_Array[4][2][1]);
	printf("\n");

	return(0);
}