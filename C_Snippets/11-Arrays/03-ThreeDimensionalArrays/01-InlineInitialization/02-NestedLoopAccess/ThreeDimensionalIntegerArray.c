#include <stdio.h>

int main(void)
{
	//vatiable declatation
	int i_aav_Array[5][3][2] = { {{11,21} , {31,41},{51,61}},
								 {{78,8},{9,01},{10,11}},
								 {{62,13},{74,15},{96,17}},
								 {{18,19},{90,21},{26,23}},
								 {{24,25},{26,27},{28,40}} };
	int i_aav_intSize;
	int i_aav_ArraySize;
	int i_aav_ArrayNumElements, i_aav_ArrayWidth, i_aav_ArrayHeight, i_aav_ArrayDepth;
	int i_aav_i, i_aav_j, i_aav_k;
	printf("\n");

	i_aav_intSize = sizeof(int);
	i_aav_ArraySize = sizeof(i_aav_Array);

	printf("Size of 3d Integer Array is = %d\n", i_aav_ArraySize);

	i_aav_ArrayWidth = i_aav_ArraySize / sizeof(i_aav_Array[0]);
	printf("Number of Rows In 3d Array = %d\n", i_aav_ArrayWidth);

	i_aav_ArrayHeight = sizeof(i_aav_Array[0]) / sizeof(i_aav_Array[0][0]);
	printf("Number of Columns in 3d Array = %d\n", i_aav_ArrayHeight);

	i_aav_ArrayDepth = sizeof(i_aav_Array[0][0]) / i_aav_intSize;
	printf("Depth of 3d Array = %d\n ", i_aav_ArrayDepth);

	i_aav_ArrayNumElements = i_aav_ArrayWidth * i_aav_ArrayHeight * i_aav_ArrayDepth;
	printf("Numbeer of Elements in 3D Array = %d\n", i_aav_ArrayNumElements);
	printf("\n\n");

	printf("Elements in 3D Array : \n\n");

	for (i_aav_i = 0; i_aav_i < i_aav_ArrayWidth; i_aav_i++)
	{
		printf("*****ROW %d *******\n", (i_aav_i + 1));
		for (i_aav_j = 0; i_aav_j < i_aav_ArrayHeight; i_aav_j++)
		{
			printf("******COLUMN %d*******\n", (i_aav_j + 1));
			for (i_aav_k = 0; i_aav_k < i_aav_ArrayDepth; i_aav_k++)
			{
				printf("iArray[%d][%d][%d] = %d\n", i_aav_i, i_aav_j, i_aav_k, i_aav_Array[i_aav_i][i_aav_j][i_aav_k]);
			}
			printf("\n");
		}
		printf("\n\n");
	}
	return(0);
}