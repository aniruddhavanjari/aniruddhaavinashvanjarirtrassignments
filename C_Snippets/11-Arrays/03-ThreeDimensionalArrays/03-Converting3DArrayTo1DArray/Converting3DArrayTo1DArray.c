#include <stdio.h>

#define NUM_ROWS 5
#define NUM_COLUMNS 3
#define DEPTH 2

int main(void)
{
	//vatiable declatation
	int i_aav_Array[NUM_ROWS][NUM_COLUMNS][DEPTH] = { {{11,21} , {31,41},{51,61}},
								 {{78,8},{9,01},{10,11}},
								 {{62,13},{74,15},{96,17}},
								 {{18,19},{90,21},{26,23}},
								 {{24,25},{26,27},{28,40}} };
	
	int i_aav_i, i_aav_j, i_aav_k;

	int i_aav_ArrayOneD[NUM_ROWS*NUM_COLUMNS*DEPTH];

	printf("\n");

	printf("Elements in 3D Array : \n\n");

	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		printf("*****ROW %d *******\n", (i_aav_i + 1));
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS; i_aav_j++)
		{
			printf("******COLUMN %d*******\n", (i_aav_j + 1));
			for (i_aav_k = 0; i_aav_k < DEPTH; i_aav_k++)
			{
				printf("iArray[%d][%d][%d] = %d\n", i_aav_i, i_aav_j, i_aav_k, i_aav_Array[i_aav_i][i_aav_j][i_aav_k]);
			}
			printf("\n");
		}
		printf("\n\n");
	}

	// Converting 3D to 1D
	for (i_aav_i = 0; i_aav_i < NUM_ROWS; i_aav_i++)
	{
		for (i_aav_j = 0; i_aav_j < NUM_COLUMNS; i_aav_j++)
		{
			for (i_aav_k = 0; i_aav_k < DEPTH; i_aav_k++)
			{
				i_aav_ArrayOneD[(i_aav_i * NUM_COLUMNS *DEPTH) + (i_aav_j * DEPTH) + i_aav_k] = i_aav_Array[i_aav_i][i_aav_j][i_aav_k];
			}
		}
	}

	// Display 1D Array

	printf("\n\n");
	printf("Elements In 1D Array :\n\n ");
	for (i_aav_i = 0; i_aav_i < (NUM_ROWS*NUM_COLUMNS*DEPTH); i_aav_i++)
	{
		printf("ArrayOneD[%d] = %d\n", i_aav_i, i_aav_ArrayOneD[i_aav_i]);
	}
	return(0);
}