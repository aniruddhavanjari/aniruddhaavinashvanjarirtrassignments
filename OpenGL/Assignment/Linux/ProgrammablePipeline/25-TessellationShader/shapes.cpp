#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include "vmath.h"

//namespaces
using namespace vmath;
using namespace std;

typedef GLXContext (*glxCreateContextAttribsArbProc)(Display*,GLXFBConfig,GLXContext,Bool,const int *);

glxCreateContextAttribsArbProc glxCreateContextAttribsArb = NULL;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};


GLXFBConfig gGLXFBConfig;
//global variable declarations
FILE* aav_gpFile = NULL;
bool aav_bFullscreen = false;
Display *aav_gpDisplay = NULL;
XVisualInfo *aav_gpXVisualInfo = NULL;

GLXContext aav_gGLXContext;

Colormap aav_gColormap;
Window aav_gWindow;
int aav_giWindowWidth = 800;
int aav_giWindowHeight = 600;

GLfloat aav_angleTriangle = 0.0f;
GLfloat aav_angleRectangle = 0.0f;

GLuint gVertexShaderObject;
GLuint glTeselationControlShaderObject;		//Teselation Control Object
GLuint glTeselationEvalutationShaderObject;	//Teselation Evaluation Shader
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint aav_gVao;
GLuint aav_gVbo_position;
GLuint aav_mvp_MatrixUniform;

//Teselation Shader unifroms
GLuint aav_NumberOfSegmentUniform;
GLuint aav_NumberOfStripsUniform;
GLuint aav_lineColorUniform;

mat4 aav_PerspectiveProjectionMatrix;

//Teselation
unsigned int uiNumberOfSegment;


//entry-point fucntoin 
int main(void)
{
	//function prototypes 
	void initialize(void);
	void resize(int ,int);
	void display(void);

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();

	//varuable declarations 
	int aav_winWidth = aav_giWindowWidth;
	int aav_winHeight = aav_giWindowHeight;
	bool aav_bDone = false;

	// Code
	aav_gpFile =fopen("logfile.txt","w+" );
	if(aav_gpFile == NULL)
	{
		printf("Failed to Open A Log File\n");
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
		exit(0);
	}
	else 
	{
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
		
	}		
	
	CreateWindow();
	initialize();

	// Message Loop
	XEvent aav_event;
	KeySym aav_keysym;

	
	
		
	while(aav_bDone == false)
	{
		while(XPending(aav_gpDisplay))
		{
			XNextEvent(aav_gpDisplay,&aav_event);
			switch(aav_event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					aav_keysym = XkbKeycodeToKeysym(aav_gpDisplay,aav_event.xkey.keycode,0,0);
					switch(aav_keysym)
					{
						case XK_Escape:
							aav_bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(aav_bFullscreen == false)
							{
								ToggleFullscreen();
								aav_bFullscreen = true;	
							}
							else
							{
								ToggleFullscreen();
								aav_bFullscreen = false;
							}
							break;
						case XK_Up:
							uiNumberOfSegment++;
							if (uiNumberOfSegment >= 30)
							{
								uiNumberOfSegment = 30;
							}
							break;
						case XK_Down:
							uiNumberOfSegment--;
							if (uiNumberOfSegment <= 0)
							{
								uiNumberOfSegment = 1;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(aav_event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					aav_winWidth = aav_event.xconfigure.width;
					aav_winHeight = aav_event.xconfigure.height;
					resize(aav_winWidth, aav_winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					aav_bDone = true;
					break;
				default:
					break;
			}
		}
		display();

	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//fucntion prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes aav_winAttribs; 

	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *ptempXVisualInfo = NULL;
	int numFBConfig = 0;	

	int aav_defaultScreen;
	int aav_defaultDepth;
	int aav_styleMask;

	static int aav_frameBufferAttributes[] = {GLX_DOUBLEBUFFER,True,
							GLX_RGBA,
							GLX_X_RENDERABLE,True,
							GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
							GLX_RENDER_TYPE,GLX_RGBA_BIT,
							GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
							GLX_RED_SIZE,8,
							GLX_GREEN_SIZE,8,
							GLX_BLUE_SIZE,8,
							GLX_ALPHA_SIZE,8,
							GLX_DEPTH_SIZE,24,
							GLX_STENCIL_SIZE,8,
							None};

	//code
	aav_gpDisplay = XOpenDisplay(NULL);
	if(aav_gpDisplay == NULL)
	{
		printf("ERROR: Unable to Open X Display .\n Extting Now ...\n");
		uninitialize();
		exit(1);
	}
	aav_defaultScreen = XDefaultScreen(aav_gpDisplay);

	aav_gpXVisualInfo= (XVisualInfo*)malloc(sizeof(XVisualInfo));
	if(aav_gpXVisualInfo == NULL)
	{
		printf("ERROR : Memory Allocation Failed.\n Exitting Now...  \n");
		uninitialize();
		exit(1);
	}
	
	pGLXFBConfig = glXChooseFBConfig(aav_gpDisplay,XDefaultScreen(aav_gpDisplay),aav_frameBufferAttributes,&numFBConfig);
	printf("Found Number of FBConfig : numFBConfig %d",numFBConfig);
	
	int bestFrameBufferConfig = -1,worstFrameBufferConfig = -1, bestNuberOfSamples = -1, worstNumberOfSamples = 99;
	int i;
	
	for(i = 0 ; i < numFBConfig; i ++)
	{
		ptempXVisualInfo = glXGetVisualFromFBConfig(aav_gpDisplay, pGLXFBConfig[i]);
		if(ptempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(aav_gpDisplay, pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);
			glXGetFBConfigAttrib(aav_gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);
			
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNuberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNuberOfSamples = samples;	
			}
			if(worstFrameBufferConfig < 0 || !sampleBuffers ||  samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples= samples;	
			}
			printf("When i %d , When samples %d , when sampleBuffer %d , when ptempXVisualInfo->VisualID : %lu \n",i , samples, sampleBuffers, ptempXVisualInfo->visualid);
		}
		XFree(ptempXVisualInfo);
	} 

	printf(" bestNuberOfSamples %d ,bestFrameBufferConfig %d \n",bestNuberOfSamples, bestFrameBufferConfig);
	
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig 	= bestGLXFBConfig;
	
	XFree(pGLXFBConfig);
	
	aav_gpXVisualInfo = glXGetVisualFromFBConfig(aav_gpDisplay,bestGLXFBConfig);	
	
	aav_winAttribs.border_pixel = 0;
	aav_winAttribs.background_pixel = 0;
	aav_winAttribs.colormap = XCreateColormap(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			aav_gpXVisualInfo->visual,
			AllocNone);

	aav_gColormap = aav_winAttribs.colormap;
	aav_winAttribs.background_pixel = BlackPixel(aav_gpDisplay, aav_defaultScreen);

	aav_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	aav_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap; 

	aav_gWindow = XCreateWindow(aav_gpDisplay,
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			0,
			0,
			aav_giWindowWidth,
			aav_giWindowHeight,
			0,
			aav_gpXVisualInfo->depth,
			InputOutput,
			aav_gpXVisualInfo->visual,
			aav_styleMask,
			&aav_winAttribs);

	if(!aav_gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now.. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(aav_gpDisplay, aav_gWindow,"XWindows P.P. AAV:Tessellation Shader.");

	Atom aav_windowManagerDelete = XInternAtom(aav_gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(aav_gpDisplay, aav_gWindow,&aav_windowManagerDelete,1);
	XMapWindow(aav_gpDisplay, aav_gWindow);
}

void ToggleFullscreen(void)
{
	// variable delarations
	Atom aav_wm_state;
	Atom aav_fullscreen;
	XEvent aav_xev = {0};

	//code
	aav_wm_state = XInternAtom(aav_gpDisplay, "_NET_WM_STATE",False);
	memset(&aav_xev,0,sizeof(aav_xev));

	aav_xev.type = ClientMessage;
	aav_xev.xclient.window = aav_gWindow;
	aav_xev.xclient.message_type = aav_wm_state;
	aav_xev.xclient.format = 32;
	aav_xev.xclient.data.l[0] = aav_bFullscreen ? 0 : 1;

	aav_fullscreen = XInternAtom(aav_gpDisplay, "_NET_WM_STATE_FULLSCREEN",False);
	aav_xev.xclient.data.l[1] = aav_fullscreen;

	XSendEvent(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&aav_xev);
}

void initialize(void)
{
	//fuction declaration
	void resize(int, int);
	void uninitialize(void);
	//code
	glxCreateContextAttribsArb = (glxCreateContextAttribsArbProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");	
	if(glxCreateContextAttribsArb == NULL)
	{
		printf("Failed to Locate ""glXCreateContextAttribsARB""\n");
	}

	const int attribs[]  = {GLX_CONTEXT_MAJOR_VERSION_ARB,4,GLX_CONTEXT_MINOR_VERSION_ARB,5, GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,None};	
	
	aav_gGLXContext = glxCreateContextAttribsArb(aav_gpDisplay, gGLXFBConfig, 0, True, attribs);
	if(!aav_gGLXContext)
	{
		const int attribs1[] = {GLX_CONTEXT_MAJOR_VERSION_ARB,1,GLX_CONTEXT_MINOR_VERSION_ARB,0,None};	
		aav_gGLXContext = glxCreateContextAttribsArb(aav_gpDisplay, gGLXFBConfig, 0, True, attribs1);	
		if(!aav_gGLXContext)
		{
			printf("gGLXContext Failed  \n");
			exit(-1);
		}
	}
	
	Bool isDirectContext = glXIsDirect(aav_gpDisplay, aav_gGLXContext);
	if(isDirectContext == True)
	{
		printf("HardWareRendering Context\n");
	}
	else
	{
		printf("SoftWareRendering Context\n");
	}
		
	glXMakeCurrent(aav_gpDisplay,aav_gWindow,aav_gGLXContext);

	GLenum aav_glew_error = glewInit();
	if(aav_glew_error != GLEW_OK)
	{
		uninitialize();
		exit(1);
	}	
	
	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //gRAPHICS Library shadinng language

	
	//OpenGL Enable Extensions 
	GLint  numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	printf("NumExtension %d\n",numExtension);
	for (unsigned int i = 0; i < numExtension; i++)
	{	
		fprintf(aav_gpFile,"%s\n", glGetStringi(GL_EXTENSIONS,3));
	}
	
	//XDestroyWindow(aav_gpDisplay, aav_gWindow);
	//**VERTEX SHADER***
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec2 vPosition;" \
		"void main(void)" \
		"{" \
		"gl_Position =  vec4(vPosition,0.0,1.0);" \
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gVertexShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}
	/*TeselationControlShader*/
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	glTeselationControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);
	const GLchar* teselationShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"layout(vertices = 4)out;" \
		"uniform int numberOfSegments;"\
		"uniform int numberOfStrips;"\
		"void main(void)" \
		"{" \
		"	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;"\
		"	gl_TessLevelOuter[0] = float(numberOfStrips); " \
		"	gl_TessLevelOuter[1] = float(numberOfSegments); " \
		"}";
	glShaderSource(glTeselationControlShaderObject, 1, (const GLchar**)&teselationShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(glTeselationControlShaderObject);

	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;
	szBuffer = NULL;

	glGetShaderiv(glTeselationControlShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(glTeselationControlShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(glTeselationControlShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Teselation Control Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}


	/******Teselation Evaluation Shader*******/
	glTeselationEvalutationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);
	const GLchar* teselationEvalutaionShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"layout(isolines)in;" \
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"{" \
		"	float tessCoord = gl_TessCoord.x;" \
		"	vec3  p0 = gl_in[0].gl_Position.xyz;"\
		"	vec3  p1 = gl_in[1].gl_Position.xyz;"\
		"	vec3  p2 = gl_in[2].gl_Position.xyz;"\
		"	vec3  p3 = gl_in[3].gl_Position.xyz;"\
		"	vec3 p = p0 * (1.0 - tessCoord) * (1.0 - tessCoord) * (1.0 - tessCoord) + p1 * 3.0 * tessCoord  * (1.0 - tessCoord) * (1.0 - tessCoord)  + p2 * 3.0 * tessCoord * tessCoord *(1.0-tessCoord) + p3 *  tessCoord * tessCoord * tessCoord ;" \
		"	gl_Position = u_mvpMatrix * vec4(p,1.0);"\
		"}";
	glShaderSource(glTeselationEvalutationShaderObject, 1, (const GLchar**)&teselationEvalutaionShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(glTeselationEvalutationShaderObject);

	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;
	szBuffer = NULL;

	glGetShaderiv(glTeselationEvalutationShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(glTeselationEvalutationShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(glTeselationEvalutationShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Teselation Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////////


	//**FRAGMENT SHADER**
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"uniform vec4 LineColor;" \
		"void main(void)" \
		"{" \
		"FragColor = LineColor;" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(gFragmentShaderObject);


	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gFragmentShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, glTeselationControlShaderObject);		//
	glAttachShader(gShaderProgramObject, glTeselationEvalutationShaderObject);	//
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);
	
	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glLinkProgram(gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	//Post Linking Information
	aav_mvp_MatrixUniform =  glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");

	aav_NumberOfSegmentUniform = glGetUniformLocation(gShaderProgramObject, "numberOfSegments");

	aav_NumberOfStripsUniform = glGetUniformLocation(gShaderProgramObject, "numberOfStrips");

	aav_lineColorUniform = glGetUniformLocation(gShaderProgramObject, "LineColor");

	

	//vertices array declation
	const GLfloat vertices[] =
	{
	-1.0f,	-1.0f,
	-0.5f,	 1.0f,
	 0.5f,	-1.0f,
	 1.0f,	 1.0f
	};

	glGenVertexArrays(1, &aav_gVao);
	glBindVertexArray(aav_gVao);
	//Record
	glGenBuffers(1, &aav_gVbo_position); // Named Buffer object
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position); // Postion , TexCord, Normal
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Record Off / Pause 
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();

	uiNumberOfSegment = 1; //

	resize(aav_giWindowWidth,aav_giWindowHeight);
}

void resize(int width , int height )
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing
	mat4 modelViewMateix = mat4::identity();
	mat4 modelViewProjectMatrix = mat4::identity();
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
	modelViewMateix = translateMatrix;
	modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform,1,GL_FALSE, modelViewProjectMatrix);

	glUniform1i(aav_NumberOfSegmentUniform, uiNumberOfSegment);//

	glUniform1i(aav_NumberOfStripsUniform, 1);//

	glUniform4fv(aav_lineColorUniform, 1, vmath::vec4(1.0f,1.0f,0.0f,1.0f));//

	glBindVertexArray(aav_gVao);
	glPatchParameteri(GL_PATCH_VERTICES, 4);
	glDrawArrays(GL_PATCHES, 0, 4);

	glBindVertexArray(0);

	glUseProgram(0);
	
	glXSwapBuffers(aav_gpDisplay,aav_gWindow);
}



void uninitialize(void)
{
	//Code
	if (aav_gVao)
	{
		glDeleteVertexArrays(1, &aav_gVao);
		aav_gVao = 0;
	}

	if (aav_gVbo_position)
	{
		glDeleteBuffers(1, &aav_gVbo_position);
		aav_gVbo_position = 0;
	}


	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	glDetachShader(gShaderProgramObject, glTeselationControlShaderObject); //
	glDetachShader(gShaderProgramObject, glTeselationEvalutationShaderObject); //

	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(glTeselationControlShaderObject);//
	glTeselationControlShaderObject = 0;//

	glDeleteShader(glTeselationEvalutationShaderObject);//
	glTeselationEvalutationShaderObject = 0;//

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	glXMakeCurrent(NULL,0,NULL);	
	
	glXDestroyContext(aav_gpDisplay,aav_gGLXContext);
	aav_gGLXContext= NULL;

	
	if(aav_gWindow)
	{
		XDestroyWindow(aav_gpDisplay, aav_gWindow);
	}

	if(aav_gColormap)
	{
		XFreeColormap(aav_gpDisplay, aav_gColormap);
	}

	if(aav_gpXVisualInfo)
	{
		free(aav_gpXVisualInfo);
		aav_gpXVisualInfo = NULL;
	}
	
	if(aav_gpFile)
	{	
		fprintf(aav_gpFile,"Succefully Completed the Program");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

	if(aav_gpDisplay)
	{
		
		XCloseDisplay(aav_gpDisplay);
		aav_gpDisplay = NULL;
	}
}


