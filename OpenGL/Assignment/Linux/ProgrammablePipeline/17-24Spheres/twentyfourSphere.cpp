#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <SOIL/SOIL.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include "vmath.h"
#include "Sphere.h"

//namespaces
using namespace vmath;
using namespace std;

typedef GLXContext (*glxCreateContextAttribsArbProc)(Display*,GLXFBConfig,GLXContext,Bool,const int *);

glxCreateContextAttribsArbProc glxCreateContextAttribsArb = NULL;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};


GLXFBConfig gGLXFBConfig;
//global variable declarations
bool aav_bFullscreen = false;
Display *aav_gpDisplay = NULL;
XVisualInfo *aav_gpXVisualInfo = NULL;

GLXContext aav_gGLXContext;

Colormap aav_gColormap;
Window aav_gWindow;
int aav_giWindowWidth = 800;
int aav_giWindowHeight = 600;


GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint aav_mvp_MatrixUniform;

mat4 aav_PerspectiveProjectionMatrix;

mat4 aav_orthographicProjectionMatrix;

//Sphere Variable
GLfloat aav_sphere_vertices[1146];
GLfloat aav_sphere_normals[1146];
GLfloat aav_sphere_texture[764];
unsigned short aav_sphere_elements[2280];
GLuint aav_numSphereVertices;
GLuint aav_numSphereElements;

GLuint aav_gVao_sphere;
GLuint aav_Vbo_sphere_position;
GLuint aav_Vbo_sphere_normals;
GLuint aav_Vbo_sphere_elements;

bool aav_bLight = false;
 
struct Light
{
	GLfloat  aav_lightAmbiant[4];
	GLfloat aav_lightDiffuse[4];
	GLfloat aav_lightSpecular[4];
	GLfloat aav_lightPosition[4];

};

struct Light light = { { 0.0f,0.0f,0.0f,1.0f } ,{ 1.0f,1.0f,1.0f,1.0f } , { 1.0f,1.0f,1.0f,1.0f } , { 0.0f,0.0f,0.0f,1.0f } };

struct Material
{
	GLfloat aav_materialAmbiant[4];
	GLfloat aav_materialDiffuse[4];
	GLfloat aav_materialSpecular[4];
	GLfloat aav_materialShininess;
};

struct Material material;

GLuint aav_viewMatrixUniform;
GLuint aav_modelMatrixUniform;
GLuint aav_ProjectionMatrixUniform;

GLuint aav_laUniform;
GLuint aav_ldUniform;
GLuint aav_lsUniform;
GLuint aav_lightPositionUniform;

GLuint aav_kaUniform;
GLuint aav_kdUniform;
GLuint aav_ksUniform;

GLuint aav_kShininessUniform;

//KeyPress
GLuint aav_lKeyPressedUniform;
bool aav_xKeyPress = false;
bool aav_yKeyPress = false;
bool aav_zKeyPress = false;
bool aav_animation = false;
//Light Angles
GLfloat aav_lightangle = 0.0f;

//height And width
GLint aav_gHeight = 0;
GLint aav_gWidth = 0;

GLint aav_divideHeight = 0;
GLint aav_divideWidth = 0;

char keys[26];

FILE *aav_gpFile = NULL;
//entry-point fucntoin 

int main(void)
{
	//function prototypes 
	void initialize(void);
	void resize(int ,int);
	void display(void);

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();

	//varuable declarations 
	int aav_winWidth = aav_giWindowWidth;
	int aav_winHeight = aav_giWindowHeight;
	bool aav_bDone = false;

	// Code
	aav_gpFile =fopen("logfile.txt","w+" );
	if(aav_gpFile == NULL)
	{
		printf("Failed to Open A Log File\n");
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
		exit(0);
	}
	else 
	{
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
		
	}		
	
	CreateWindow();
	initialize();

	// Message Loop
	XEvent aav_event;
	KeySym aav_keysym;

	while(aav_bDone == false)
	{
		while(XPending(aav_gpDisplay))
		{
			XNextEvent(aav_gpDisplay,&aav_event);
			switch(aav_event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					aav_keysym = XkbKeycodeToKeysym(aav_gpDisplay,aav_event.xkey.keycode,0,0);
					XLookupString(&aav_event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'a':
						case 'A':
						if (aav_animation == false)
						{
							aav_animation = true;
						}
						else
						{
							aav_animation = false;
						}
						break;
						case 'l':
						case 'L':
						if (aav_bLight == false)
						{
							aav_bLight = true;
						}
						else
						{
							aav_bLight = false;
						}
						break;
						case 'x':
						case 'X':
							aav_lightangle = 0.0f;
							aav_yKeyPress = false;
							aav_zKeyPress = false;
							aav_xKeyPress = true;
						break;
						case 'y':
						case 'Y':
							aav_lightangle = 0.0f;
							aav_xKeyPress = false;
							aav_zKeyPress = false;
							aav_yKeyPress = true;
						break;
						case 'z':
						case 'Z':
						aav_lightangle = 0.0f;
						aav_xKeyPress = false;
						aav_yKeyPress = false;
						aav_zKeyPress = true;
						break;				
						default:
							break;
					}
					switch(aav_keysym)
					{
						case XK_Escape:
							aav_bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(aav_bFullscreen == false)
							{
								ToggleFullscreen();
								aav_bFullscreen = true;	
							}
							else
							{
								ToggleFullscreen();
								aav_bFullscreen = false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(aav_event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					aav_winWidth = aav_event.xconfigure.width;
					aav_winHeight = aav_event.xconfigure.height;
					resize(aav_winWidth, aav_winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					aav_bDone = true;
					break;
				default:
					break;
			}
		}
		display();

	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//fucntion prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes aav_winAttribs; 

	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *ptempXVisualInfo = NULL;
	int numFBConfig = 0;	

	int aav_defaultScreen;
	int aav_defaultDepth;
	int aav_styleMask;

	static int aav_frameBufferAttributes[] = {GLX_DOUBLEBUFFER,True,
							GLX_X_RENDERABLE,True,
							GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
							GLX_RENDER_TYPE,GLX_RGBA_BIT,
							GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
							GLX_RED_SIZE,8,
							GLX_GREEN_SIZE,8,
							GLX_BLUE_SIZE,8,
							GLX_ALPHA_SIZE,8,
							GLX_DEPTH_SIZE,24,
							GLX_STENCIL_SIZE,8,
							None};

	//code
	aav_gpDisplay = XOpenDisplay(NULL);
	if(aav_gpDisplay == NULL)
	{
		printf("ERROR: Unable to Open X Display .\n Extting Now ...\n");
		uninitialize();
		exit(1);
	}
	aav_defaultScreen = XDefaultScreen(aav_gpDisplay);

	aav_gpXVisualInfo= (XVisualInfo*)malloc(sizeof(XVisualInfo));
	if(aav_gpXVisualInfo == NULL)
	{
		printf("ERROR : Memory Allocation Failed.\n Exitting Now...  \n");
		uninitialize();
		exit(1);
	}
	
	pGLXFBConfig = glXChooseFBConfig(aav_gpDisplay,XDefaultScreen(aav_gpDisplay),aav_frameBufferAttributes,&numFBConfig);
	printf("Found Number of FBConfig : numFBConfig %d",numFBConfig);
	
	int bestFrameBufferConfig = -1,worstFrameBufferConfig = -1, bestNuberOfSamples = -1, worstNumberOfSamples = 99;
	int i;
	
	for(i = 0 ; i < numFBConfig; i ++)
	{
		ptempXVisualInfo = glXGetVisualFromFBConfig(aav_gpDisplay, pGLXFBConfig[i]);
		if(ptempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(aav_gpDisplay, pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);
			glXGetFBConfigAttrib(aav_gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);
			
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNuberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNuberOfSamples = samples;	
			}
			if(worstFrameBufferConfig < 0 || !sampleBuffers ||  samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples= samples;	
			}
			printf("When i %d , When samples %d , when sampleBuffer %d , when ptempXVisualInfo->VisualID : %lu \n",i , samples, sampleBuffers, ptempXVisualInfo->visualid);
		}
		XFree(ptempXVisualInfo);
	} 

	printf(" bestNuberOfSamples %d ,bestFrameBufferConfig %d \n",bestNuberOfSamples, bestFrameBufferConfig);
	
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig 	= bestGLXFBConfig;
	
	XFree(pGLXFBConfig);
	
	aav_gpXVisualInfo = glXGetVisualFromFBConfig(aav_gpDisplay,bestGLXFBConfig);	
	
	aav_winAttribs.border_pixel = 0;
	aav_winAttribs.background_pixel = 0;
	aav_winAttribs.colormap = XCreateColormap(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			aav_gpXVisualInfo->visual,
			AllocNone);

	aav_gColormap = aav_winAttribs.colormap;
	aav_winAttribs.background_pixel = BlackPixel(aav_gpDisplay, aav_defaultScreen);

	aav_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	aav_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap; 

	aav_gWindow = XCreateWindow(aav_gpDisplay,
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			0,
			0,
			aav_giWindowWidth,
			aav_giWindowHeight,
			0,
			aav_gpXVisualInfo->depth,
			InputOutput,
			aav_gpXVisualInfo->visual,
			aav_styleMask,
			&aav_winAttribs);

	if(!aav_gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now.. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(aav_gpDisplay, aav_gWindow,"XWindows P.P. AAV : 24Spheres");

	Atom aav_windowManagerDelete = XInternAtom(aav_gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(aav_gpDisplay, aav_gWindow,&aav_windowManagerDelete,1);
	XMapWindow(aav_gpDisplay, aav_gWindow);
}

void ToggleFullscreen(void)
{	
	// variable delarations
	Atom aav_wm_state;
	Atom aav_fullscreen;
	XEvent aav_xev = {0};

	//code
	aav_wm_state = XInternAtom(aav_gpDisplay, "_NET_WM_STATE",False);
	memset(&aav_xev,0,sizeof(aav_xev));

	aav_xev.type = ClientMessage;
	aav_xev.xclient.window = aav_gWindow;
	aav_xev.xclient.message_type = aav_wm_state;
	aav_xev.xclient.format = 32;
	aav_xev.xclient.data.l[0] = aav_bFullscreen ? 0 : 1;

	aav_fullscreen = XInternAtom(aav_gpDisplay, "_NET_WM_STATE_FULLSCREEN",False);
	aav_xev.xclient.data.l[1] = aav_fullscreen;

	XSendEvent(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&aav_xev);
}

void initialize(void)
{
	//fuction declaration
	void resize(int, int);
	void uninitialize(void);
	
	//code
	glxCreateContextAttribsArb = (glxCreateContextAttribsArbProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");	
	if(glxCreateContextAttribsArb == NULL)
	{
		printf("Failed to Locate ""glXCreateContextAttribsARB""\n");
	}

	const int attribs[]  = {GLX_CONTEXT_MAJOR_VERSION_ARB,4,GLX_CONTEXT_MINOR_VERSION_ARB,5, GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,None};	
	
	aav_gGLXContext = glxCreateContextAttribsArb(aav_gpDisplay, gGLXFBConfig, 0, True, attribs);
	if(!aav_gGLXContext)
	{
		const int attribs1[] = {GLX_CONTEXT_MAJOR_VERSION_ARB,1,GLX_CONTEXT_MINOR_VERSION_ARB,0,None};	
		aav_gGLXContext = glxCreateContextAttribsArb(aav_gpDisplay, gGLXFBConfig, 0, True, attribs1);	
		if(!aav_gGLXContext)
		{
			printf("gGLXContext Failed  \n");
			exit(-1);
		}
	}
	
	Bool isDirectContext = glXIsDirect(aav_gpDisplay, aav_gGLXContext);
	if(isDirectContext == True)
	{
		printf("HardWareRendering Context\n");
	}
	else
	{
		printf("SoftWareRendering Context\n");
	}
		
	glXMakeCurrent(aav_gpDisplay,aav_gWindow,aav_gGLXContext);

	GLenum aav_glew_error = glewInit();
	if(aav_glew_error != GLEW_OK)
	{
		uninitialize();
		exit(1);
	}	
	
	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //gRAPHICS Library shadinng language

	
	//OpenGL Enable Extensions 
	GLint  numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	printf("NumExtension %d\n",numExtension);
	for (unsigned int i = 0; i < numExtension; i++)
	{	
		fprintf(aav_gpFile,"%s\n", glGetStringi(GL_EXTENSIONS,i));
	}
	
	//**VERTEX SHADER***
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec4 u_lightPosistion;" \
		"out vec3 tranformed_normal;"\
		"out vec3 lightDirection;" \
		"out vec3 view_vector;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{"\
		"		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"		tranformed_normal = (mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"		lightDirection = (vec3(u_lightPosistion - eyeCordinate));" \
		"		view_vector = (-eyeCordinate.xyz);" \
		"	}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gVertexShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	//**FRAGMENT SHADER**
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 tranformed_normal;"\
		"in vec3 lightDirection;" \
		"in vec3 view_vector;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \

		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_kShineness;"\

		"out vec4 FragColor;" \
		"vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{"\
		"		vec3 normalize_tranformed_normal = normalize(tranformed_normal);" \
		"		vec3 normalize_lightDirection = normalize(lightDirection);" \
		"		vec3 normalize_view_vector = normalize(view_vector);" \
		"		vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" \
		"		vec3 ambiant = u_la * u_ka;" \
		"		vec3 diffuse = u_ld * u_kd * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" \
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness);" \
		"		fong_ads_light = ambiant + diffuse + specular;" \
		"	}"\
		"	else"\
		"	{"\
		"		fong_ads_light = vec3(1.0f,1.0f,1.0f);"\
		"	}"\
		"	FragColor = vec4(fong_ads_light,1.0f);" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(gFragmentShaderObject);


	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gFragmentShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	//Post Linking Information

	/**************************************/
	aav_modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	aav_viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	aav_ProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	aav_laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	aav_ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	aav_lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	aav_lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosistion");
	
	aav_kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	aav_kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	aav_ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");

	aav_kShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_kShineness");

	aav_lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	/**************************************/

	//vertices array declation
	getSphereVertexData(aav_sphere_vertices, aav_sphere_normals, aav_sphere_texture, aav_sphere_elements);
	
	aav_numSphereVertices = getNumberOfSphereVertices();
	aav_numSphereElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &aav_gVao_sphere);
	glBindVertexArray(aav_gVao_sphere);

	//Record Sphere
	glGenBuffers(1, &aav_Vbo_sphere_position); 
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_vertices), aav_sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//normals
	glGenBuffers(1, &aav_Vbo_sphere_normals);
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_normals), aav_sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE,0,NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//elements
	glGenBuffers(1, &aav_Vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements); // Elemtnes Drawing is Also Called As Index Drawing, Elements Drawing
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(aav_sphere_elements), aav_sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	//glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	

	glClearColor(0.5f,0.5f,0.5f,1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();
	resize(aav_giWindowWidth,aav_giWindowHeight);
}

void resize(int width , int height )
{
	//code 
	aav_gHeight = height;
	aav_gWidth = width;

	aav_divideHeight = height / 6;
	aav_divideWidth = width / 4;

	fprintf(aav_gpFile, "height = %d, width = %d\n", aav_gHeight, aav_gWidth);
	fprintf(aav_gpFile, "aav_divideHeight = %d, aav_divideWidth = %d\n", aav_divideHeight, aav_divideWidth);
	if (height == 0)
		height = 1;

	aav_gHeight = height;
	aav_gWidth = width;

	aav_divideHeight = height / 6;
	aav_divideWidth = width / 4;


	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
		//fuction declartion
	void update();

	mat4 aav_modelMateix;
	mat4 aav_viewMatrix;
	mat4 aav_translateMatrix;
	mat4  aav_scaleMatrix;
	mat4 aav_frustomMatrix;
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	if (aav_xKeyPress == true)
	{
		light.aav_lightPosition[0] = 0.0f;
		light.aav_lightPosition[1] = 100 * sin(aav_lightangle);
		light.aav_lightPosition[2] = 100 * cos(aav_lightangle);
		light.aav_lightPosition[3] = 1.0f;
	}
	else if (aav_yKeyPress == true)
	{
		light.aav_lightPosition[0] = 100 * sin(aav_lightangle);
		light.aav_lightPosition[1] = 0.0f;
		light.aav_lightPosition[2] = 100 * cos(aav_lightangle);
		light.aav_lightPosition[3] = 1.0f;
	}
	else if (aav_zKeyPress == true)
	{
		light.aav_lightPosition[0] = 100 * sin(aav_lightangle);
		light.aav_lightPosition[1] = 100 * cos(aav_lightangle);
		light.aav_lightPosition[2] = 0.0f;
		light.aav_lightPosition[3] = 1.0f;
	}

	//OpenGL Drawing
	//1
	glUseProgram(gShaderProgramObject);

	//1st Sppere in 1st Column , emerald 
	material.aav_materialAmbiant[0] = 0.0215f;
	material.aav_materialAmbiant[1] = 0.1745f;
	material.aav_materialAmbiant[2] = 0.0215f;
	material.aav_materialAmbiant[3] = 1.0f;
	

	material.aav_materialDiffuse[0] = 0.07568f;
	material.aav_materialDiffuse[1] = 0.61424f;
	material.aav_materialDiffuse[2] = 0.07568f;
	material.aav_materialDiffuse[3] = 1.0f;
	

	material.aav_materialSpecular[0] = 0.633f;
	material.aav_materialSpecular[1] = 0.727811f;
	material.aav_materialSpecular[2] = 0.633f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.6f * 128.0f;

	if (aav_bLight == true)
	{ 
		glUniform1i(aav_lKeyPressedUniform,1);
		//Light
		glUniform3fv(aav_laUniform,1,(GLfloat *)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform,1,(GLfloat *)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform,1,(GLfloat *)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform,1,(GLfloat *)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform,1,(GLfloat *)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform,1,(GLfloat *)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform,1, (GLfloat *)material.aav_materialSpecular);			//ks
 		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, aav_divideHeight*5, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();
	
	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix ;
	
	glUniformMatrix4fv(aav_modelMatrixUniform,1,GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	
	//2
	//2nd Sphere in 1st column, jade 
	material.aav_materialAmbiant[0] = 0.135f;
	material.aav_materialAmbiant[1] = 0.2225f;
	material.aav_materialAmbiant[2] = 0.1575f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.54f;
	material.aav_materialDiffuse[1] = 0.89f;
	material.aav_materialDiffuse[2] = 0.63f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.316228f;
	material.aav_materialSpecular[1] = 0.316228f;
	material.aav_materialSpecular[2] = 0.316228f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.1f *128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, aav_divideHeight * 4, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//3nd Sphere in 1st column, obsidian
	material.aav_materialAmbiant[0] = 0.05375f;
	material.aav_materialAmbiant[1] = 0.05f;
	material.aav_materialAmbiant[2] = 0.06625f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.18275f;
	material.aav_materialDiffuse[1] = 0.17f;
	material.aav_materialDiffuse[2] = 0.22525f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.332741f;
	material.aav_materialSpecular[1] = 0.328634f;
	material.aav_materialSpecular[2] = 0.346435f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.3f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, aav_divideHeight * 2, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//4th Sphere in 1st column, pearl
	material.aav_materialAmbiant[0] = 0.25f;
	material.aav_materialAmbiant[1] = 0.20725f;
	material.aav_materialAmbiant[2] = 0.20725f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 1.0f;
	material.aav_materialDiffuse[1] = 0.829f;
	material.aav_materialDiffuse[2] = 0.829f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.296648f;
	material.aav_materialSpecular[1] = 0.296648f;
	material.aav_materialSpecular[2] = 0.296648f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.088f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, aav_divideHeight * 3, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//5th sphere in 1st column ,ruby 
	material.aav_materialAmbiant[0] = 0.1745f;
	material.aav_materialAmbiant[1] = 0.01175f;
	material.aav_materialAmbiant[2] = 0.01175f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.61424f;
	material.aav_materialDiffuse[1] = 0.04136f;
	material.aav_materialDiffuse[2] = 0.04136f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.727811f;
	material.aav_materialSpecular[1] = 0.626959f;
	material.aav_materialSpecular[2] = 0.626959f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.6f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, aav_divideHeight, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	////6th sphere in 1st column , turquoise
	material.aav_materialAmbiant[0] = 0.1f;
	material.aav_materialAmbiant[1] = 0.18725f;
	material.aav_materialAmbiant[2] = 0.1745f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.396f;
	material.aav_materialDiffuse[1] = 0.74151f;
	material.aav_materialDiffuse[2] = 0.69102f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.297254f;
	material.aav_materialSpecular[1] = 0.30829f;
	material.aav_materialSpecular[2] = 0.306678f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.1f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, 0, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//1st sphere on 2nd column, brass
	material.aav_materialAmbiant[0] = 0.329412f;
	material.aav_materialAmbiant[1] = 0.223529f;
	material.aav_materialAmbiant[2] = 0.027451f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.780398f;
	material.aav_materialDiffuse[1] = 0.568627f;
	material.aav_materialDiffuse[2] = 0.113725f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.992157f;
	material.aav_materialSpecular[1] = 0.947776f;
	material.aav_materialSpecular[2] = 0.807843f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.21794872f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth , aav_divideHeight * 5, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//2nd sphere on 2nd column,bronze
	material.aav_materialAmbiant[0] = 0.2425f;
	material.aav_materialAmbiant[1] = 0.1275f;
	material.aav_materialAmbiant[2] = 0.054f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.714f;
	material.aav_materialDiffuse[1] = 0.4284f;
	material.aav_materialDiffuse[2] = 0.18144f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.393548f;
	material.aav_materialSpecular[1] = 0.271906f;
	material.aav_materialSpecular[2] = 0.166721f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.2f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth, aav_divideHeight * 4, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//3rd sphere on 2rd column,chrome
	material.aav_materialAmbiant[0] = 0.25f;
	material.aav_materialAmbiant[1] = 0.25f;
	material.aav_materialAmbiant[2] = 0.25f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.4f;
	material.aav_materialDiffuse[1] = 0.4f;
	material.aav_materialDiffuse[2] = 0.4f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.774597f;
	material.aav_materialSpecular[1] = 0.774597f;
	material.aav_materialSpecular[2] = 0.774597f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.6f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth, aav_divideHeight * 3, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//4th sphere on 2nd column, 
	material.aav_materialAmbiant[0] = 0.19125f;
	material.aav_materialAmbiant[1] = 0.0735f;
	material.aav_materialAmbiant[2] = 0.0225f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.7038f;
	material.aav_materialDiffuse[1] = 0.27048f;
	material.aav_materialDiffuse[2] = 0.0828f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.256777f;
	material.aav_materialSpecular[1] = 0.137622f;
	material.aav_materialSpecular[2] = 0.086014f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.1f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth, aav_divideHeight * 2, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//5th Sphere on 2nd Column, gold
	material.aav_materialAmbiant[0] = 0.24725f;
	material.aav_materialAmbiant[1] = 0.1995f;
	material.aav_materialAmbiant[2] = 0.0745f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.75164f;
	material.aav_materialDiffuse[1] = 0.60648f;
	material.aav_materialDiffuse[2] = 0.22648f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.628281f;
	material.aav_materialSpecular[1] = 0.555802f;
	material.aav_materialSpecular[2] = 0.366065f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.4f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth, aav_divideHeight , (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//6th Sphere on 2nd Column 
	material.aav_materialAmbiant[0] = 0.19225f;
	material.aav_materialAmbiant[1] = 0.19225f;
	material.aav_materialAmbiant[2] = 0.19225f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.50754f;
	material.aav_materialDiffuse[1] = 0.50754f;
	material.aav_materialDiffuse[2] = 0.50754f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.508273f;
	material.aav_materialSpecular[1] = 0.508273f;
	material.aav_materialSpecular[2] = 0.508273f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.4f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth, 0 , (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//1st sphere on 3nd column
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.01f;
	material.aav_materialDiffuse[1] = 0.01f;
	material.aav_materialDiffuse[2] = 0.01f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.50f;
	material.aav_materialSpecular[1] = 0.50f;
	material.aav_materialSpecular[2] = 0.50f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, aav_divideHeight * 5, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//2nd Sphere on 3nd column,cyan
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.1f;
	material.aav_materialAmbiant[2] = 0.06f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.0f;
	material.aav_materialDiffuse[1] = 0.50980392f;
	material.aav_materialDiffuse[2] = 0.50980392f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.50196078f;
	material.aav_materialSpecular[1] = 0.50196078f;
	material.aav_materialSpecular[2] = 0.50196078f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, aav_divideHeight * 4, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//3rd Sphere on 3th column 
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.1f;
	material.aav_materialDiffuse[1] = 0.35f;
	material.aav_materialDiffuse[2] = 0.1f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.45f;
	material.aav_materialSpecular[1] = 0.55f;
	material.aav_materialSpecular[2] = 0.45f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, aav_divideHeight * 3, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//4th Sphere on 3rd ,red
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.5f;
	material.aav_materialDiffuse[1] = 0.0f;
	material.aav_materialDiffuse[2] = 0.0f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.7f;
	material.aav_materialSpecular[1] = 0.6f;
	material.aav_materialSpecular[2] = 0.6f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, aav_divideHeight * 2, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//5th Sphere on 3rd , white
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.55f;
	material.aav_materialDiffuse[1] = 0.55f;
	material.aav_materialDiffuse[2] = 0.55f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.70f;
	material.aav_materialSpecular[1] = 0.70f;
	material.aav_materialSpecular[2] = 0.70f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, aav_divideHeight , (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//6th Sphere on 3rd ,yello plastic
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.5f;
	material.aav_materialDiffuse[1] = 0.5f;
	material.aav_materialDiffuse[2] = 0.0f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.60f;
	material.aav_materialSpecular[1] = 0.60f;
	material.aav_materialSpecular[2] = 0.50f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, 0 , (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//1st sphere on 4th column,black
	material.aav_materialAmbiant[0] = 0.02f;
	material.aav_materialAmbiant[1] = 0.02f;
	material.aav_materialAmbiant[2] = 0.02f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.01f;
	material.aav_materialDiffuse[1] = 0.01f;
	material.aav_materialDiffuse[2] = 0.01f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.4f;
	material.aav_materialSpecular[1] = 0.4f;
	material.aav_materialSpecular[2] = 0.4f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.078125f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth *3 , aav_divideHeight * 5, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//2nd Shpere on 4th column,cyan
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.05f;
	material.aav_materialAmbiant[2] = 0.05f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.4f;
	material.aav_materialDiffuse[1] = 0.5f;
	material.aav_materialDiffuse[2] = 0.5f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.04f;
	material.aav_materialSpecular[1] = 0.7f;
	material.aav_materialSpecular[2] = 0.7f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.078125f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 3, aav_divideHeight * 4, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//3rd Sphere on 4th column , green
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.05f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.4f;
	material.aav_materialDiffuse[1] = 0.5f;
	material.aav_materialDiffuse[2] = 0.04f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.04f;
	material.aav_materialSpecular[1] = 0.7f;
	material.aav_materialSpecular[2] = 0.04f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.6f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 3, aav_divideHeight * 3, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//4th Sphere on 4th column, red
	material.aav_materialAmbiant[0] = 0.05f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.5f;
	material.aav_materialDiffuse[1] = 0.4f;
	material.aav_materialDiffuse[2] = 0.4f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.7f;
	material.aav_materialSpecular[1] = 0.04f;
	material.aav_materialSpecular[2] = 0.04f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.078125f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 3, aav_divideHeight * 2, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//5th Sphere on 4th column, white
	material.aav_materialAmbiant[0] = 0.05f;
	material.aav_materialAmbiant[1] = 0.05f;
	material.aav_materialAmbiant[2] = 0.05f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.5f;
	material.aav_materialDiffuse[1] = 0.5f;
	material.aav_materialDiffuse[2] = 0.5f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.7f;
	material.aav_materialSpecular[1] = 0.7f;
	material.aav_materialSpecular[2] = 0.7f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.6f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 3, aav_divideHeight , (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//6th Sphere on 4th column , rubber
	material.aav_materialAmbiant[0] = 0.05f;
	material.aav_materialAmbiant[1] = 0.05f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.5f;
	material.aav_materialDiffuse[1] = 0.5f;
	material.aav_materialDiffuse[2] = 0.4f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.7f;
	material.aav_materialSpecular[1] = 0.7f;
	material.aav_materialSpecular[2] = 0.04f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.078125f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 3, 0, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	glUseProgram(0);

	if (aav_animation == true)
	{
		update();
	}


	glXSwapBuffers(aav_gpDisplay,aav_gWindow);
}

void update(void)
{
	aav_lightangle = aav_lightangle + 0.05f;
	if (aav_lightangle > 360)
	{
		aav_lightangle = 0.0f;
	}
}

void uninitialize(void)
{
	if (aav_gVao_sphere)
	{
		glDeleteVertexArrays(1, &aav_gVao_sphere);
		aav_gVao_sphere = 0;
	}

	if (aav_Vbo_sphere_position)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_position);
		aav_Vbo_sphere_position = 0;
	}

	if (aav_Vbo_sphere_normals)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_normals);
		aav_Vbo_sphere_normals = 0;
	}

	if (aav_Vbo_sphere_elements)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_elements);
		aav_Vbo_sphere_elements = 0;
	}



	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);

	glXMakeCurrent(NULL,0,NULL);	
	
	glXDestroyContext(aav_gpDisplay,aav_gGLXContext);
	aav_gGLXContext= NULL;

	
	if(aav_gWindow)
	{
		XDestroyWindow(aav_gpDisplay, aav_gWindow);
	}

	if(aav_gColormap)
	{
		XFreeColormap(aav_gpDisplay, aav_gColormap);
	}

	if(aav_gpXVisualInfo)
	{
		free(aav_gpXVisualInfo);
		aav_gpXVisualInfo = NULL;
	}
	
	if(aav_gpFile)
	{	
		fprintf(aav_gpFile,"Succefully Completed the Program");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

	if(aav_gpDisplay)
	{
		
		XCloseDisplay(aav_gpDisplay);
		aav_gpDisplay = NULL;
	}
}


