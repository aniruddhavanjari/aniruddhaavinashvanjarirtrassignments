#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <SOIL/SOIL.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include "vmath.h"
#include "Sphere.h"

#define PI  3.14

//namespaces
using namespace vmath;
using namespace std;

typedef GLXContext (*glxCreateContextAttribsArbProc)(Display*,GLXFBConfig,GLXContext,Bool,const int *);

glxCreateContextAttribsArbProc glxCreateContextAttribsArb = NULL;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};


GLXFBConfig gGLXFBConfig;
//global variable declarations
bool aav_bFullscreen = false;
Display *aav_gpDisplay = NULL;
XVisualInfo *aav_gpXVisualInfo = NULL;

GLXContext aav_gGLXContext;

Colormap aav_gColormap;
Window aav_gWindow;
int aav_giWindowWidth = 800;
int aav_giWindowHeight = 600;

GLuint aav_gVertexShaderObject;
GLuint aav_gFragmentShaderObject;
GLuint aav_gShaderProgramObject;

GLuint aav_gVao_triangle;
GLuint aav_gVbo_position_triangle;
GLuint aav_gVbo_color_triangle;

GLuint aav_gVao_square;
GLuint aav_gVbo_position_square;
GLuint aav_gVbo_color_square;

GLuint aav_gVao_linex;
GLuint aav_gVao_liney;
GLuint aav_gVao_line;

GLuint aav_gVbo_linex_position;
GLuint aav_gVbo_liney_position;
GLuint aav_gVbo_line_position;


GLuint aav_gVao_circle;
GLuint aav_gVbo_circle_position;
GLuint aav_aVbo_circle_color;

GLuint aav_gVao_circleInner;
GLuint aav_gVbo_circleInner_position;
GLuint aav_aVbo_circleInner_color;

GLuint aav_gVao_point;
GLuint aav_gVbo_point_position;
GLuint aav_gVbo_point_color;

GLuint aav_mvp_MatrixUniform;
GLuint aav_colorUniform;

GLfloat aav_anglePyramid = 0.0f;
GLfloat aav_angleCube = 0.0f;

mat4 aav_PerspectiveProjectionMatrix;

GLfloat* aav_circleVertices;
GLfloat* aav_circleColor;

GLfloat* aav_circleVerticesInner;
GLfloat* aav_circleColorInner; 

GLfloat r;	   // radius of innder circle 
int count = 0; // Number of interation in Circle Loop

char keys[26];

FILE *aav_gpFile = NULL;
//entry-point fucntoin 

int main(void)
{
	//function prototypes 
	void initialize(void);
	void resize(int ,int);
	void display(void);

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();

	//varuable declarations 
	int aav_winWidth = aav_giWindowWidth;
	int aav_winHeight = aav_giWindowHeight;
	bool aav_bDone = false;

	// Code
	aav_gpFile =fopen("logfile.txt","w+" );
	if(aav_gpFile == NULL)
	{
		printf("Failed to Open A Log File\n");
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
		exit(0);
	}
	else 
	{
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
		
	}		
	
	CreateWindow();
	initialize();

	// Message Loop
	XEvent aav_event;
	KeySym aav_keysym;

	while(aav_bDone == false)
	{
		while(XPending(aav_gpDisplay))
		{
			XNextEvent(aav_gpDisplay,&aav_event);
			switch(aav_event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					aav_keysym = XkbKeycodeToKeysym(aav_gpDisplay,aav_event.xkey.keycode,0,0);
					XLookupString(&aav_event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						default:
							break;
					}
					switch(aav_keysym)
					{
						case XK_Escape:
							aav_bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(aav_bFullscreen == false)
							{
								ToggleFullscreen();
								aav_bFullscreen = true;	
							}
							else
							{
								ToggleFullscreen();
								aav_bFullscreen = false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(aav_event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					aav_winWidth = aav_event.xconfigure.width;
					aav_winHeight = aav_event.xconfigure.height;
					resize(aav_winWidth, aav_winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					aav_bDone = true;
					break;
				default:
					break;
			}
		}
		display();

	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//fucntion prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes aav_winAttribs; 

	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *ptempXVisualInfo = NULL;
	int numFBConfig = 0;	

	int aav_defaultScreen;
	int aav_defaultDepth;
	int aav_styleMask;

	static int aav_frameBufferAttributes[] = {GLX_DOUBLEBUFFER,True,
							GLX_X_RENDERABLE,True,
							GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
							GLX_RENDER_TYPE,GLX_RGBA_BIT,
							GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
							GLX_RED_SIZE,8,
							GLX_GREEN_SIZE,8,
							GLX_BLUE_SIZE,8,
							GLX_ALPHA_SIZE,8,
							GLX_DEPTH_SIZE,24,
							GLX_STENCIL_SIZE,8,
							None};

	//code
	aav_gpDisplay = XOpenDisplay(NULL);
	if(aav_gpDisplay == NULL)
	{
		printf("ERROR: Unable to Open X Display .\n Extting Now ...\n");
		uninitialize();
		exit(1);
	}
	aav_defaultScreen = XDefaultScreen(aav_gpDisplay);

	aav_gpXVisualInfo= (XVisualInfo*)malloc(sizeof(XVisualInfo));
	if(aav_gpXVisualInfo == NULL)
	{
		printf("ERROR : Memory Allocation Failed.\n Exitting Now...  \n");
		uninitialize();
		exit(1);
	}
	
	pGLXFBConfig = glXChooseFBConfig(aav_gpDisplay,XDefaultScreen(aav_gpDisplay),aav_frameBufferAttributes,&numFBConfig);
	printf("Found Number of FBConfig : numFBConfig %d",numFBConfig);
	
	int bestFrameBufferConfig = -1,worstFrameBufferConfig = -1, bestNuberOfSamples = -1, worstNumberOfSamples = 99;
	int i;
	
	for(i = 0 ; i < numFBConfig; i ++)
	{
		ptempXVisualInfo = glXGetVisualFromFBConfig(aav_gpDisplay, pGLXFBConfig[i]);
		if(ptempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(aav_gpDisplay, pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);
			glXGetFBConfigAttrib(aav_gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);
			
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNuberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNuberOfSamples = samples;	
			}
			if(worstFrameBufferConfig < 0 || !sampleBuffers ||  samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples= samples;	
			}
			printf("When i %d , When samples %d , when sampleBuffer %d , when ptempXVisualInfo->VisualID : %lu \n",i , samples, sampleBuffers, ptempXVisualInfo->visualid);
		}
		XFree(ptempXVisualInfo);
	} 

	printf(" bestNuberOfSamples %d ,bestFrameBufferConfig %d \n",bestNuberOfSamples, bestFrameBufferConfig);
	
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig 	= bestGLXFBConfig;
	
	XFree(pGLXFBConfig);
	
	aav_gpXVisualInfo = glXGetVisualFromFBConfig(aav_gpDisplay,bestGLXFBConfig);	
	
	aav_winAttribs.border_pixel = 0;
	aav_winAttribs.background_pixel = 0;
	aav_winAttribs.colormap = XCreateColormap(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			aav_gpXVisualInfo->visual,
			AllocNone);

	aav_gColormap = aav_winAttribs.colormap;
	aav_winAttribs.background_pixel = BlackPixel(aav_gpDisplay, aav_defaultScreen);

	aav_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	aav_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap; 

	aav_gWindow = XCreateWindow(aav_gpDisplay,
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			0,
			0,
			aav_giWindowWidth,
			aav_giWindowHeight,
			0,
			aav_gpXVisualInfo->depth,
			InputOutput,
			aav_gpXVisualInfo->visual,
			aav_styleMask,
			&aav_winAttribs);

	if(!aav_gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now.. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(aav_gpDisplay, aav_gWindow,"XWindows P.P. AAV : GraphPaperWithSpahes");

	Atom aav_windowManagerDelete = XInternAtom(aav_gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(aav_gpDisplay, aav_gWindow,&aav_windowManagerDelete,1);
	XMapWindow(aav_gpDisplay, aav_gWindow);
}

void ToggleFullscreen(void)
{	
	// variable delarations
	Atom aav_wm_state;
	Atom aav_fullscreen;
	XEvent aav_xev = {0};

	//code
	aav_wm_state = XInternAtom(aav_gpDisplay, "_NET_WM_STATE",False);
	memset(&aav_xev,0,sizeof(aav_xev));

	aav_xev.type = ClientMessage;
	aav_xev.xclient.window = aav_gWindow;
	aav_xev.xclient.message_type = aav_wm_state;
	aav_xev.xclient.format = 32;
	aav_xev.xclient.data.l[0] = aav_bFullscreen ? 0 : 1;

	aav_fullscreen = XInternAtom(aav_gpDisplay, "_NET_WM_STATE_FULLSCREEN",False);
	aav_xev.xclient.data.l[1] = aav_fullscreen;

	XSendEvent(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&aav_xev);
}

void initialize(void)
{
	//fuction declaration
	void resize(int, int);
	void uninitialize(void);
	void Calculation(void);

	//code
	glxCreateContextAttribsArb = (glxCreateContextAttribsArbProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");	
	if(glxCreateContextAttribsArb == NULL)
	{
		printf("Failed to Locate ""glXCreateContextAttribsARB""\n");
	}

	const int attribs[]  = {GLX_CONTEXT_MAJOR_VERSION_ARB,4,GLX_CONTEXT_MINOR_VERSION_ARB,5, GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,None};	
	
	aav_gGLXContext = glxCreateContextAttribsArb(aav_gpDisplay, gGLXFBConfig, 0, True, attribs);
	if(!aav_gGLXContext)
	{
		const int attribs1[] = {GLX_CONTEXT_MAJOR_VERSION_ARB,1,GLX_CONTEXT_MINOR_VERSION_ARB,0,None};	
		aav_gGLXContext = glxCreateContextAttribsArb(aav_gpDisplay, gGLXFBConfig, 0, True, attribs1);	
		if(!aav_gGLXContext)
		{
			printf("gGLXContext Failed  \n");
			exit(-1);
		}
	}
	
	Bool isDirectContext = glXIsDirect(aav_gpDisplay, aav_gGLXContext);
	if(isDirectContext == True)
	{
		printf("HardWareRendering Context\n");
	}
	else
	{
		printf("SoftWareRendering Context\n");
	}
		
	glXMakeCurrent(aav_gpDisplay,aav_gWindow,aav_gGLXContext);

	GLenum aav_glew_error = glewInit();
	if(aav_glew_error != GLEW_OK)
	{
		uninitialize();
		exit(1);
	}	
	
	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //gRAPHICS Library shadinng language
	
	//**VERTEX SHADER***
	aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 aav_u_mvpMatrix;" \
		"out vec4 aav_out_color;" \
		"uniform vec4 aav_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = aav_u_mvpMatrix * vPosition;" \
		"aav_out_color =aav_color;" \
		"}";
	glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(aav_gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* aav_szBuffer = NULL;

	glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
					&aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);	
			}
		}
	}

	//**FRAGMENT SHADER**
	aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"in vec4 aav_out_color;" \
		"out vec4 aav_FragColor;" \
		
		"void main(void)" \
		"{" \
		"aav_FragColor = aav_out_color;" \
		"}";
	glShaderSource(aav_gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(aav_gFragmentShaderObject);


	aav_szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
					&aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);	
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	aav_gShaderProgramObject = glCreateProgram();

	glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glLinkProgram(aav_gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	aav_szBuffer = NULL;
	
	glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);	
			}
		}
	}

	//Post Linking Information
	aav_mvp_MatrixUniform =  glGetUniformLocation(aav_gShaderProgramObject, "aav_u_mvpMatrix");
	aav_colorUniform = glGetUniformLocation(aav_gShaderProgramObject, "aav_color");
	//vertices array declation
	const GLfloat aav_triangleVertices[] =
	{
	0.0f,0.5f,0.0f,
	-0.5f,-0.5f,0.0f,

	-0.5f,-0.5f,0.0f,
	0.5f,-0.5f,0.0f,

	0.5f,-0.5f,0.0f,
	0.0f,0.5f,0.0f,
	};

	const GLfloat aav_cubeVertex[] =
	{
	0.5f,0.5f,0.0f,
	-0.5f,0.5f,0.0f,

	-0.5f,0.5f,0.0f,
	-0.5f,-0.5f,0.0f,

	-0.5f,-0.5f,0.0f,
	0.5f,-0.5f,0.0f,

	0.5f,-0.5f,0.0f,
	0.5f,0.5f,0.0f
	};
	
	const GLfloat aav_lineVertexYRed[]=
	{
		0.0f,-1.3f,0.0f,
		0.0f, 1.3f,0.0f
	};
	
	const GLfloat aav_lineVertexXBlue[]=
	{
		-2.0f,0.0f,0.0f,
		2.0f,0.0f,0.0f
	};


	const GLfloat aav_lineVertex[] =
	{
		0.2f,-2.0f,0.0f,
		0.2f, 2.0f,0.0f,

		
		0.4f,-2.0f,0.0f,
		0.4f, 2.0f,0.0f,

		
		0.6f,-2.0f,0.0f,
		0.6f, 2.0f,0.0f,

		0.8f,-2.0f,0.0f,
		0.8f, 2.0f,0.0f,

		
		1.0f,-2.0f,0.0f,
		1.0f, 2.0f,0.0f,

		1.2f,-2.0f,0.0f,
		1.2f, 2.0f,0.0f,

		
		1.4f,-2.0f,0.0f,
		1.4f, 2.0f,0.0f,

		
		1.6f,-2.0f,0.0f,
		1.6f, 2.0f,0.0f,

		
		1.8f,-2.0f,0.0f,
		1.8f, 2.0f,0.0f,

		2.0f,-2.0f,0.0f,
		2.0f, 2.0f,0.0f,


		//x negative
		-0.2f,-2.0f,0.0f,
		-0.2f, 2.0f,0.0f,


		-0.4f,-2.0f,0.0f,
		-0.4f, 2.0f,0.0f,


		-0.6f,-2.0f,0.0f,
		-0.6f, 2.0f,0.0f,

		-0.8f,-2.0f,0.0f,
		-0.8f, 2.0f,0.0f,


		-1.0f,-2.0f,0.0f,
		-1.0f, 2.0f,0.0f,

		-1.2f,-2.0f,0.0f,
		-1.2f, 2.0f,0.0f,


		-1.4f,-2.0f,0.0f,
		-1.4f, 2.0f,0.0f,


		-1.6f,-2.0f,0.0f,
		-1.6f, 2.0f,0.0f,


		-1.8f,-2.0f,0.0f,
		-1.8f, 2.0f,0.0f,

		-2.0f,-2.0f,0.0f,
		-2.0f, 2.0f,0.0f,

		//y axies
		-2.0f,0.13f,0.0f,
		2.0f,0.13f,0.0f,


		-2.0f,0.26f,0.0f,
		2.0f,0.26f,0.0f,


		-2.0f,0.39f,0.0f,
		2.0f,0.39f,0.0f,

		
		-2.0f,0.52f,0.0f,
		2.0f,0.52f,0.0f,

		-2.0f, 0.65f, 0.0f,
		2.0f, 0.65f, 0.0f,


		-2.0f,0.78f,0.0f,
		2.0f,0.78f,0.0f,


		-2.0f, 0.91f, 0.0f,
		2.0f, 0.91f, 0.0f,


		-2.0f,1.04f,0.0f,
		2.0f,1.04f,0.0f,

		-2.0f,1.17f,0.0f,
		2.0f, 1.17f,0.0f,

		-2.0f, 1.3f, 0.0f,
		2.0f, 1.3f, 0.0f,

		//
		-2.0f, -0.13f, 0.0f,
		2.0f, -0.13f, 0.0f,


		-2.0f, -0.26f, 0.0f,
		2.0f, -0.26f, 0.0f,


		-2.0f, -0.39f, 0.0f,
		2.0f, -0.39f, 0.0f,


		-2.0f, -0.52f, 0.0f,
		2.0f, -0.52f, 0.0f,

		-2.0f, -0.65f, 0.0f,
		2.0f, -0.65f, 0.0f,


		-2.0f, -0.78f, 0.0f,
		2.0f, -0.78f, 0.0f,


		-2.0f, -0.91f, 0.0f,
		2.0f, -0.91f, 0.0f,


		-2.0f, -1.04f, 0.0f,
		2.0f, -1.04f, 0.0f,

		-2.0f, -1.17f, 0.0f,
		2.0f, -1.17f, 0.0f,

		-2.0f, -1.3f, 0.0f,
		2.0f, -1.3f, 0.0f
	};

	
	const GLfloat aav_pointVertex[] =
	{
		0.0f,0.0f,0.0f
	};

	Calculation();

	glGenVertexArrays(1, &aav_gVao_triangle);
	glBindVertexArray(aav_gVao_triangle);

	//Record Triangle
	glGenBuffers(1, &aav_gVbo_position_triangle);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position_triangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_triangleVertices), aav_triangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Record Square
	glGenVertexArrays(1,&aav_gVao_square);
	glBindVertexArray(aav_gVao_square);

	glGenBuffers(1,&aav_gVbo_position_square);
	glBindBuffer(GL_ARRAY_BUFFER,aav_gVbo_position_square);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeVertex), aav_cubeVertex,GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	
	//Line midle Line X and Y 
	glGenVertexArrays(1, &aav_gVao_line);
	glBindVertexArray(aav_gVao_line);

	glGenBuffers(1, &aav_gVbo_line_position);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_line_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_lineVertex), aav_lineVertex, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Red Y
	glGenVertexArrays(1, &aav_gVao_liney);
	glBindVertexArray(aav_gVao_liney);

	glGenBuffers(1, &aav_gVbo_liney_position);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_liney_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_lineVertexYRed), aav_lineVertexYRed, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Gree X
	glGenVertexArrays(1, &aav_gVao_linex);
	glBindVertexArray(aav_gVao_linex);

	glGenBuffers(1, &aav_gVbo_linex_position);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_linex_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_lineVertexXBlue), aav_lineVertexXBlue, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Outer circle 
	glGenVertexArrays(1, &aav_gVao_circle);
	glBindVertexArray(aav_gVao_circle);

	glGenBuffers(1, &aav_gVbo_circle_position);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_circle_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* count * 3, aav_circleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Inner Circle
	glGenVertexArrays(1, &aav_gVao_circleInner);
	glBindVertexArray(aav_gVao_circleInner);

	glGenBuffers(1, &aav_gVbo_circleInner_position);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_circleInner_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* count * 3, aav_circleVerticesInner, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Center Point
	glGenVertexArrays(1, &aav_gVao_point);
	glBindVertexArray(aav_gVao_point);

	glGenBuffers(1, &aav_gVbo_point_position);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_point_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pointVertex), aav_pointVertex, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Record Off / Pause 
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	

	glClearColor(0.0f,0.0f,0.0f,1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();
	resize(aav_giWindowWidth,aav_giWindowHeight);
}

void Calculation(void)
{
	GLint Line = 0.0f;
	GLfloat fLine = 0.5f;
	GLfloat side;
	GLfloat Squareside;
	GLfloat OutSideCircleR;

	GLdouble a; //Consider side 1
	GLdouble b; //Consider side 2
	GLdouble c; //Consider side 3

	GLdouble xcenterCor;
	GLdouble ycenterCor;

	//Code
	a = sqrt(((0.5 - (-0.5)) * (0.5 - (-0.5))) + (((-0.5) - (-0.5)) * ((-0.5) - (-0.5)))); //1
	b = sqrt(((0.0 - 0.5) * (0.0 - 0.5)) + ((0.5 - (-0.5)) * (0.5 - (-0.5)))); //1.118033989
	c = sqrt((((-0.5) - 0.0) * ((-0.5) - 0.0)) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); //1.118033989
	
	//Formula For x,y Cordinate for Center Point of Triangle To Draw the Circle.
	xcenterCor = ((a * (0)) + (b * (-0.5)) + (c * (0.5))) / (a + b + c);
	ycenterCor = ((a * (0.5f)) + (b * (-0.5f)) + (c * (-0.5f))) / (a + b + c);

	side = (a + b + c) / 2;
	r = sqrt(side * (side - a) * (side - b) * (side - c)) / side;  //Formmula For inCircle or inscribed Circle Radius. 

	//Circle OutSide Rectangle
	Squareside = sqrt((((-0.5) - (-0.5)) * ((-0.5) - (-0.5))) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); // We Get The Length of One Size of Square
	OutSideCircleR = (Squareside * sqrt(2)) / 2; // We Get Radius of Circle 

	
	for (GLfloat angle = 0; angle < 2 * PI; angle = angle + 0.001)
	{
		count = count + 1;
	}

	aav_circleVertices = (GLfloat*)malloc(sizeof(GLfloat)*count * 3);
	aav_circleColor = (GLfloat*)malloc(sizeof(GLfloat) * count * 3);

	int i = 0;
	for (GLfloat angle = 0; angle < 2 * PI; angle = angle + 0.001)
	{
		aav_circleVertices[i] = OutSideCircleR * sin(angle);
		aav_circleColor[i] = 1.0f; 
		i = i + 1;
		aav_circleVertices[i] = OutSideCircleR * cos(angle);
		aav_circleColor[i] = 1.0f;
		i = i + 1;
		aav_circleVertices[i] = 0.0f;
		aav_circleColor[i] = 1.0f;
		i = i + 1;
	}

	i = 0; 
	aav_circleVerticesInner = (GLfloat*)malloc(sizeof(GLfloat) * count * 3);
	aav_circleColorInner = (GLfloat*)malloc(sizeof(GLfloat) * count * 3);
	for (GLfloat angle = 0; angle < 2 * PI; angle = angle + 0.001)
	{
		aav_circleVerticesInner[i] = r * sin(angle)  + +xcenterCor;
		aav_circleColorInner[i] = 1.0f;
		i = i + 1;
		aav_circleVerticesInner[i] = r * cos(angle)+ +ycenterCor;
		aav_circleColorInner[i] = 1.0f;
		i = i + 1;
		aav_circleVerticesInner[i] = 0.0f;
		aav_circleColorInner[i] = 1.0f;
		i = i + 1;
	}

}

void resize(int width , int height )
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	//fuction declartion
	void update();

	mat4 aav_scaMatrix;
	mat4 aav_translateMatrix;
	mat4 aav_modelViewMateix;
	mat4 aav_modelViewProjectMatrix;
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 
	//triangle
	glUseProgram(aav_gShaderProgramObject);

	//OpenGL Drawing
	glPointSize(1.0f);

	glUniform4f(aav_colorUniform,0.0f,0.0f,1.0f,1.0f);	
	//Lines 
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);

	glBindVertexArray(aav_gVao_line);
	glDrawArrays(GL_LINES, 0, 80);
	glBindVertexArray(0);

	glUniform4f(aav_colorUniform,1.0f,0.0f,0.0f,1.0f);	
	glBindVertexArray(aav_gVao_liney);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
	
	glUniform4f(aav_colorUniform,0.0f,1.0f,0.0f,1.0f);	
	glBindVertexArray(aav_gVao_linex);
	glDrawArrays(GL_LINES, 0, 2);
	glBindVertexArray(0);
	
	glUniform4f(aav_colorUniform,1.0f,1.0f,1.0f,1.0f);	

	//rectangle
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform,1,GL_FALSE, aav_modelViewProjectMatrix);

	glBindVertexArray(aav_gVao_triangle);
	glDrawArrays(GL_LINES, 0, 8);
	glBindVertexArray(0);

	//square
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();
	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);

	glBindVertexArray(aav_gVao_square);
	glDrawArrays(GL_LINES, 0, 8);
	glBindVertexArray(0);
	
	//Outer circle
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
	glBindVertexArray(aav_gVao_circle);
	glDrawArrays(GL_POINTS, 0,count);
	glBindVertexArray(0);

	//Inner
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);

	glBindVertexArray(aav_gVao_circleInner);
	glDrawArrays(GL_POINTS, 0, count);
	glBindVertexArray(0);

	//Center Point 
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
	glPointSize(5.0f);

	
	glBindVertexArray(aav_gVao_point);
	glDrawArrays(GL_POINTS, 0, 1);
	glBindVertexArray(0);


	glUseProgram(0);
	glXSwapBuffers(aav_gpDisplay,aav_gWindow);
}

void uninitialize(void)
{

	if(aav_gVao_point)
	{
		glDeleteVertexArrays(1, &aav_gVao_point);
		aav_gVao_point = 0;
	}
	
	if (aav_gVbo_point_position)
	{
		glDeleteBuffers(1, &aav_gVbo_point_position);
		aav_gVbo_point_position = 0;
	}	

	if(aav_gVao_line)
	{
		glDeleteVertexArrays(1, &aav_gVao_line);
		aav_gVao_line = 0;
	}
	
	if (aav_gVbo_line_position)
	{
		glDeleteBuffers(1, &aav_gVbo_line_position);
		aav_gVbo_line_position = 0;
	}		

	if(aav_gVao_linex)
	{
		glDeleteVertexArrays(1, &aav_gVao_linex);
		aav_gVao_linex = 0;
	}
	
	if (aav_gVbo_linex_position)
	{
		glDeleteBuffers(1, &aav_gVbo_linex_position);
		aav_gVbo_linex_position = 0;
	}
	
	if(aav_gVao_liney)
	{
		glDeleteVertexArrays(1, &aav_gVao_liney);
		aav_gVao_liney = 0;
	}
	
	if (aav_gVbo_liney_position)
	{
		glDeleteBuffers(1, &aav_gVbo_liney_position);
		aav_gVbo_liney_position = 0;
	}
	
	
	if (aav_gVao_triangle)
	{
		glDeleteVertexArrays(1, &aav_gVao_triangle);
		aav_gVao_triangle = 0;
	}

	if (aav_gVbo_position_triangle)
	{
		glDeleteBuffers(1, &aav_gVbo_position_triangle);
		aav_gVbo_position_triangle = 0;
	}

	if (aav_gVbo_color_triangle)
	{
		glDeleteBuffers(1, &aav_gVbo_color_triangle);
		aav_gVbo_color_triangle = 0;
	}

	if (aav_gVao_square)
	{
		glDeleteVertexArrays(1, &aav_gVao_square);
		aav_gVao_square = 0;
	}

	if (aav_gVbo_position_square)
	{
		glDeleteBuffers(1, &aav_gVbo_position_square);
		aav_gVbo_position_square = 0;
	}

	if (aav_gVbo_color_square)
	{
		glDeleteBuffers(1, &aav_gVbo_color_square);
		aav_gVbo_color_square = 0;
	}


	if (aav_gVao_circle)
	{
		glDeleteVertexArrays(1, &aav_gVao_circle);
		aav_gVao_circle = 0;
	}

	if (aav_gVbo_circle_position)
	{
		glDeleteBuffers(1, &aav_gVbo_circle_position);
		aav_gVbo_circle_position = 0;
	}

	if (aav_aVbo_circle_color)
	{
		glDeleteBuffers(1, &aav_aVbo_circle_color);
		aav_aVbo_circle_color = 0;
	}

	if (aav_gVao_circle)
	{
		glDeleteVertexArrays(1, &aav_gVao_circle);
		aav_gVao_circle = 0;
	}

	if (aav_gVbo_circleInner_position)
	{
		glDeleteBuffers(1, &aav_gVbo_circleInner_position);
		aav_gVbo_circleInner_position = 0;
	}

	if (aav_aVbo_circleInner_color)
	{
		glDeleteBuffers(1, &aav_aVbo_circleInner_color);
		aav_aVbo_circleInner_color = 0;
	}

	glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glDeleteShader(aav_gVertexShaderObject);
	aav_gVertexShaderObject = 0;
	glDeleteShader(aav_gFragmentShaderObject);
	aav_gFragmentShaderObject = 0;

	glDeleteProgram(aav_gShaderProgramObject);
	aav_gShaderProgramObject = 0;

	glUseProgram(0);

	glXMakeCurrent(NULL,0,NULL);	
	
	glXDestroyContext(aav_gpDisplay,aav_gGLXContext);
	aav_gGLXContext= NULL;

	
	if(aav_gWindow)
	{
		XDestroyWindow(aav_gpDisplay, aav_gWindow);
	}

	if(aav_gColormap)
	{
		XFreeColormap(aav_gpDisplay, aav_gColormap);
	}

	if(aav_gpXVisualInfo)
	{
		free(aav_gpXVisualInfo);
		aav_gpXVisualInfo = NULL;
	}
	
	if(aav_gpFile)
	{	
		fprintf(aav_gpFile,"Succefully Completed the Program");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

	if (aav_circleVertices)
	{
		free(aav_circleVertices);
	}

	if (aav_circleColor)
	{
		free(aav_circleColor);
	}

	if (aav_circleVerticesInner)
	{
		free(aav_circleVerticesInner);
	}

	if (aav_circleColorInner)
	{
		free(aav_circleColorInner);
	}


	
	if(aav_gpDisplay)
	{
		
		XCloseDisplay(aav_gpDisplay);
		aav_gpDisplay = NULL;
	}
}


