#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <SOIL/SOIL.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include "vmath.h"
#include "tepod.h"

//namespaces
using namespace vmath;
using namespace std;

typedef GLXContext (*glxCreateContextAttribsArbProc)(Display*,GLXFBConfig,GLXContext,Bool,const int *);

glxCreateContextAttribsArbProc glxCreateContextAttribsArb = NULL;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};

GLXFBConfig gGLXFBConfig;
//global variable declarations

FILE* aav_gpFile = NULL;

bool aav_bFullscreen = false;
Display *aav_gpDisplay = NULL;
XVisualInfo *aav_gpXVisualInfo = NULL;

GLXContext aav_gGLXContext;

Colormap aav_gColormap;
Window aav_gWindow;
int aav_giWindowWidth = 800;
int aav_giWindowHeight = 600;

GLuint aav_gVertexShaderObject;
GLuint aav_gFragmentShaderObject;
GLuint aav_gShaderProgramObject;


GLuint aav_mvp_MatrixUniform;

mat4 aav_PerspectiveProjectionMatrix;

//Sphere Variable
GLfloat aav_sphere_vertices[1146];
GLfloat aav_sphere_normals[1146];
GLfloat aav_sphere_texture[764];
unsigned short aav_sphere_elements[2280];
GLuint aav_numSphereVertices;
GLuint aav_numSphereElements;

GLuint aav_gVao_cube;
GLuint aav_gVbo_cubeAll;


bool aav_bLight = false;
 
//Light Array
GLfloat aav_lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };	//la
GLfloat aav_lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	// ld
GLfloat aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };//ls

GLfloat aav_lightPosition[] = { 0.0f,0.0f,2.0f,1.0f };

//material Array 
GLfloat aav_materialAmbiant[]	= { 0.0f, 0.0f, 0.0f ,0.0f}; // ka 
GLfloat aav_materialDiffuse[]	= {1.0f,1.0f,1.0f,1.0f};	// kd
GLfloat aav_materialSpecular[]	= {1.0f,1.0f,1.0f,1.0f};	// ks


GLuint aav_viewMatrixUniform;
GLuint aav_modelMatrixUniform;
GLuint aav_ProjectionMatrixUniform;

GLuint aav_laUniform;
GLuint aav_ldUniform;
GLuint aav_lsUniform;
GLuint aav_lightPositionUniform;

GLuint aav_kShininessUniform;

GLuint aav_lKeyPressedUniform;

GLuint aav_textureSamplerUniform;

bool aav_AnimationFlag = false;

GLfloat aav_rotation = 0.0f;

GLuint aav_texture;
//////////////////////////////////////////////////////////////////////////////////

float* vertexArray = NULL;

int main(void)
{
	//function prototypes 
	void initialize(void);
	void resize(int ,int);
	void display(void);

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();

	//varuable declarations 
	int aav_winWidth = aav_giWindowWidth;
	int aav_winHeight = aav_giWindowHeight;
	bool aav_bDone = false;

	// Code
	aav_gpFile =fopen("logfile.txt","w+" );
	if(aav_gpFile == NULL)
	{
		printf("Failed to Open A Log File\n");
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
		exit(0);
	}
	else 
	{
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
		
	}		
	
	CreateWindow();
	initialize();

	// Message Loop
	XEvent aav_event;
	KeySym aav_keysym;

	
	
		
	while(aav_bDone == false)
	{
		while(XPending(aav_gpDisplay))
		{
			XNextEvent(aav_gpDisplay,&aav_event);
			switch(aav_event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					aav_keysym = XkbKeycodeToKeysym(aav_gpDisplay,aav_event.xkey.keycode,0,0);
					switch(aav_keysym)
					{
						case XK_Escape:
							aav_bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(aav_bFullscreen == false)
							{
								ToggleFullscreen();
								aav_bFullscreen = true;	
							}
							else
							{
								ToggleFullscreen();
								aav_bFullscreen = false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(aav_event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					aav_winWidth = aav_event.xconfigure.width;
					aav_winHeight = aav_event.xconfigure.height;
					resize(aav_winWidth, aav_winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					aav_bDone = true;
					break;
				default:
					break;
			}
		}
		display();

	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//fucntion prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes aav_winAttribs; 

	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *ptempXVisualInfo = NULL;
	int numFBConfig = 0;	

	int aav_defaultScreen;
	int aav_defaultDepth;
	int aav_styleMask;

	static int aav_frameBufferAttributes[] = {GLX_DOUBLEBUFFER,True,
							GLX_X_RENDERABLE,True,
							GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
							GLX_RENDER_TYPE,GLX_RGBA_BIT,
							GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
							GLX_RED_SIZE,8,
							GLX_GREEN_SIZE,8,
							GLX_BLUE_SIZE,8,
							GLX_ALPHA_SIZE,8,
							GLX_DEPTH_SIZE,24,
							GLX_STENCIL_SIZE,8,
							None};

	//code
	aav_gpDisplay = XOpenDisplay(NULL);
	if(aav_gpDisplay == NULL)
	{
		printf("ERROR: Unable to Open X Display .\n Extting Now ...\n");
		uninitialize();
		exit(1);
	}
	aav_defaultScreen = XDefaultScreen(aav_gpDisplay);

	aav_gpXVisualInfo= (XVisualInfo*)malloc(sizeof(XVisualInfo));
	if(aav_gpXVisualInfo == NULL)
	{
		printf("ERROR : Memory Allocation Failed.\n Exitting Now...  \n");
		uninitialize();
		exit(1);
	}
	
	pGLXFBConfig = glXChooseFBConfig(aav_gpDisplay,XDefaultScreen(aav_gpDisplay),aav_frameBufferAttributes,&numFBConfig);
	printf("Found Number of FBConfig : numFBConfig %d",numFBConfig);
	
	int bestFrameBufferConfig = -1,worstFrameBufferConfig = -1, bestNuberOfSamples = -1, worstNumberOfSamples = 99;
	int i;
	
	for(i = 0 ; i < numFBConfig; i ++)
	{
		ptempXVisualInfo = glXGetVisualFromFBConfig(aav_gpDisplay, pGLXFBConfig[i]);
		if(ptempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(aav_gpDisplay, pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);
			glXGetFBConfigAttrib(aav_gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);
			
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNuberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNuberOfSamples = samples;	
			}
			if(worstFrameBufferConfig < 0 || !sampleBuffers ||  samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples= samples;	
			}
			printf("When i %d , When samples %d , when sampleBuffer %d , when ptempXVisualInfo->VisualID : %lu \n",i , samples, sampleBuffers, ptempXVisualInfo->visualid);
		}
		XFree(ptempXVisualInfo);
	} 

	printf(" bestNuberOfSamples %d ,bestFrameBufferConfig %d \n",bestNuberOfSamples, bestFrameBufferConfig);
	
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig 	= bestGLXFBConfig;
	
	XFree(pGLXFBConfig);
	
	aav_gpXVisualInfo = glXGetVisualFromFBConfig(aav_gpDisplay,bestGLXFBConfig);	
	
	aav_winAttribs.border_pixel = 0;
	aav_winAttribs.background_pixel = 0;
	aav_winAttribs.colormap = XCreateColormap(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			aav_gpXVisualInfo->visual,
			AllocNone);

	aav_gColormap = aav_winAttribs.colormap;
	aav_winAttribs.background_pixel = BlackPixel(aav_gpDisplay, aav_defaultScreen);

	aav_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	aav_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap; 

	aav_gWindow = XCreateWindow(aav_gpDisplay,
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			0,
			0,
			aav_giWindowWidth,
			aav_giWindowHeight,
			0,
			aav_gpXVisualInfo->depth,
			InputOutput,
			aav_gpXVisualInfo->visual,
			aav_styleMask,
			&aav_winAttribs);

	if(!aav_gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now.. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(aav_gpDisplay, aav_gWindow,"XWindows P.P. AAV:Interlaved");

	Atom aav_windowManagerDelete = XInternAtom(aav_gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(aav_gpDisplay, aav_gWindow,&aav_windowManagerDelete,1);
	XMapWindow(aav_gpDisplay, aav_gWindow);
}

void ToggleFullscreen(void)
{	
	//function 


	// variable delarations
	Atom aav_wm_state;
	Atom aav_fullscreen;
	XEvent aav_xev = {0};

	//code
	aav_wm_state = XInternAtom(aav_gpDisplay, "_NET_WM_STATE",False);
	memset(&aav_xev,0,sizeof(aav_xev));

	aav_xev.type = ClientMessage;
	aav_xev.xclient.window = aav_gWindow;
	aav_xev.xclient.message_type = aav_wm_state;
	aav_xev.xclient.format = 32;
	aav_xev.xclient.data.l[0] = aav_bFullscreen ? 0 : 1;

	aav_fullscreen = XInternAtom(aav_gpDisplay, "_NET_WM_STATE_FULLSCREEN",False);
	aav_xev.xclient.data.l[1] = aav_fullscreen;

	XSendEvent(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&aav_xev);
}
//XDestroyWindow(aav_gpDisplay, aav_gWindow);
void initialize(void)
{
	//fuction declaration
	void resize(int, int);
	void uninitialize(void);
	GLuint loadBitMapAsTexture(const char *);
	void teaPotMemoryArray(void);

	//code
	glxCreateContextAttribsArb = (glxCreateContextAttribsArbProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");	
	if(glxCreateContextAttribsArb == NULL)
	{
		printf("Failed to Locate ""glXCreateContextAttribsARB""\n");
	}

	const int attribs[]  = {GLX_CONTEXT_MAJOR_VERSION_ARB,4,GLX_CONTEXT_MINOR_VERSION_ARB,5, GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,None};	
	
	aav_gGLXContext = glxCreateContextAttribsArb(aav_gpDisplay, gGLXFBConfig, 0, True, attribs);
	if(!aav_gGLXContext)
	{
		const int attribs1[] = {GLX_CONTEXT_MAJOR_VERSION_ARB,1,GLX_CONTEXT_MINOR_VERSION_ARB,0,None};	
		aav_gGLXContext = glxCreateContextAttribsArb(aav_gpDisplay, gGLXFBConfig, 0, True, attribs1);	
		if(!aav_gGLXContext)
		{
			printf("gGLXContext Failed  \n");
			exit(-1);
		}
	}
	
	Bool isDirectContext = glXIsDirect(aav_gpDisplay, aav_gGLXContext);
	if(isDirectContext == True)
	{
		printf("HardWareRendering Context\n");
	}
	else
	{
		printf("SoftWareRendering Context\n");
	}
		
	glXMakeCurrent(aav_gpDisplay,aav_gWindow,aav_gGLXContext);

	GLenum aav_glew_error = glewInit();
	if(aav_glew_error != GLEW_OK)
	{
		uninitialize();
		exit(1);
	}	
	
	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //gRAPHICS Library shadinng language

	
	//OpenGL Enable Extensions 
	GLint  numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	printf("NumExtension %d\n",numExtension);
	for (unsigned int i = 0; i < numExtension; i++)
	{	
		fprintf(aav_gpFile,"%s\n", glGetStringi(GL_EXTENSIONS,i));
	}
	//XDestroyWindow(aav_gpDisplay, aav_gWindow);
	//**VERTEX SHADER***
	aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;"	\
		"in vec3 vNormal;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_lightPosistion;" \
		"out vec3 tranformed_normal;"\
		"out vec3 lightDirection;" \
		"out vec3 view_vector;" \
		"out vec2 aav_out_TexCoord;" \
		"out vec4 aav_out_color;" \
		"void main(void)" \
		"{" \
		"	vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"	tranformed_normal = (mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"	lightDirection = (vec3(u_lightPosistion - eyeCordinate));" \
		"	view_vector = (-eyeCordinate.xyz);" \
		"	aav_out_TexCoord = vTexCoord; " \
		"	aav_out_color = vColor;" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(aav_gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	//**FRAGMENT SHADER**
	aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 aav_out_color;" \
		"in vec3 tranformed_normal;"\
		"in vec3 lightDirection;" \
		"in vec3 view_vector;" \
		"in vec2 aav_out_TexCoord;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \

		"uniform float u_kShineness;"\

		"out vec4 FragColor;" \
		"vec3 fong_ads_light;" \
		"uniform sampler2D u_texture_sampler;" \
		"void main(void)" \
		"{" \
		"		vec3 normalize_tranformed_normal = normalize(tranformed_normal);" \
		"		vec3 normalize_lightDirection = normalize(lightDirection);" \
		"		vec3 normalize_view_vector = normalize(view_vector);" \
		"		vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" \
		"		vec3 ambiant = u_la ;" \
		"		vec3 diffuse = u_ld  * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" \
		"		vec3 specular = u_ls  * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness);" \
		"		fong_ads_light = ambiant + diffuse + specular;" \
		"		fong_ads_light = fong_ads_light * vec3(texture(u_texture_sampler,aav_out_TexCoord));" \
		"		FragColor = vec4(fong_ads_light,1.0f)  *  aav_out_color; " \
		"}";
	glShaderSource(aav_gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(aav_gFragmentShaderObject);


	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	aav_gShaderProgramObject = glCreateProgram();

	glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_TEXCORD, "vTexCoord");

	glLinkProgram(aav_gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	
	glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	//Post Linking Information
	/**************************************/
	aav_textureSamplerUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_texture_sampler");

	aav_modelMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_model_matrix");
	aav_viewMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_view_matrix");
	aav_ProjectionMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_projection_matrix");

	aav_laUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_la");
	aav_ldUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ld");
	aav_lsUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ls");
	aav_lightPositionUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_lightPosistion");

	aav_kShininessUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kShineness");

	/**************************************/
	
	const GLfloat aav_cubeVertex[] =
	{
	0.5f,0.5f,0.5f,		1.0f,0.0f,0.0f,		0.0f, 0.0f, 1.0f,	0.0f,0.0f,
	-0.5f,0.5f,0.5f,	1.0f,0.0f,0.0f,		0.0f, 0.0f, 1.0f,	1.0f,0.0f,
	-0.5f,-0.5f,0.5f,	1.0f,0.0f,0.0f,		0.0f, 0.0f, 1.0f,	1.0f,1.0f,
	0.5f,-0.5f,0.5f,	1.0f,0.0f,0.0f,		0.0f, 0.0f, 1.0f,	0.0f,1.0f,

	0.5f, 0.5f, -0.5f,	0.0f,1.0f,0.0f,		1.0, 0.0f, 0.0f,	0.0f, 0.0f,
	0.5f, 0.5f, 0.5f,	0.0f,1.0f,0.0f,		1.0, 0.0f, 0.0f,	1.0f, 0.0f,
	0.5f, -0.5f, 0.5f,	0.0f,1.0f,0.0f,		1.0, 0.0f, 0.0f,	1.0f, 1.0f,
	0.5f, -0.5f, -0.5f,	0.0f,1.0f,0.0f,		1.0, 0.0f, 0.0f,	0.0f, 1.0f,

	0.5f, 0.5f, -0.5f,		0.0f,0.0f,1.0f,		0.0f, 0.0, -1.0f,	0.0f, 0.0f,
	-0.5f, 0.5f, -0.5f,		0.0f,0.0f,1.0f,		0.0f, 0.0, -1.0f,	1.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,	0.0f,0.0f,1.0f,		0.0f, 0.0, -1.0f,	1.0f, 1.0f,
	0.5f, -0.5f, -0.5f,		0.0f,0.0f,1.0f,		0.0f, 0.0, -1.0f,	0.0f, 1.0f,
	 
	-0.5f, 0.5f, -0.5f,		0.0f, 1.0f, 1.0f,	 -1.0f, 0.0f, 0.0f,		0.0f, 0.0f,
	-0.5f, 0.5f, 0.5f,		0.0f, 1.0f, 1.0f,	 -1.0f, 0.0f, 0.0f,		1.0f, 0.0f,
	-0.5f, -0.5f, 0.5f,		0.0f, 1.0f, 1.0f,	 -1.0f, 0.0f, 0.0f,		1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,	0.0f, 1.0f, 1.0f,	 -1.0f, 0.0f, 0.0f,		0.0f, 1.0f,

	0.5f, 0.5f, -0.5f,	1.0f, 0.0f, 1.0f,	0.0f, 1.0f, 0.0f,	0.0f, 0.0f,
	-0.5f, 0.5f, -0.5f,	1.0f, 0.0f, 1.0f,	0.0f, 1.0f, 0.0f,	1.0f, 0.0f,
	-0.5f, 0.5f, 0.5f,	1.0f, 0.0f, 1.0f,	 0.0f, 1.0f, 0.0f,	1.0f, 1.0f,
	0.5f, 0.5f, 0.5f,	1.0f, 0.0f, 1.0f,	0.0f, 1.0f, 0.0f,	0.0f, 1.0f,

	0.5f, -0.5f, -0.5f,		1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,	0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,	1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,	1.0f, 0.0f,
	-0.5f, -0.5f, 0.5f,		1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,	1.0f, 1.0f,
	0.5f, -0.5f, 0.5f,		1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,	0.0f, 1.0f
	};



	glGenVertexArrays(1, &aav_gVao_cube);
	glBindVertexArray(aav_gVao_cube);

	//Record Sphere
	glGenBuffers(1, &aav_gVbo_cubeAll); 
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_cubeAll);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeVertex), aav_cubeVertex, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	
	//color
	glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);

	//normals
	glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);

	//texture
	glVertexAttribPointer(AAV_ATTRIBUTE_TEXCORD, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(9 * sizeof(float)));
	glEnableVertexAttribArray(AAV_ATTRIBUTE_TEXCORD);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	
	glEnable(GL_TEXTURE_2D);

	//aav_kundali_texture = loadBitMapAsTexture("Kundali.bmp");
	aav_texture = loadBitMapAsTexture("marble.bmp");

	glClearColor(0.0f,0.0f,0.0f,1.0f);
	
	aav_PerspectiveProjectionMatrix = mat4::identity();
	resize(aav_giWindowWidth,aav_giWindowHeight);
}


GLuint loadBitMapAsTexture(const char *path)
{
	//variable declaration
	int aav_width = 0;
	int aav_height = 0 ;
	unsigned char *aav_imageData = NULL;
	GLuint aav_textureID;

	//code
	aav_imageData = SOIL_load_image(path,&aav_width,&aav_height,NULL,SOIL_LOAD_RGB);
	
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
	glGenTextures(1, &aav_textureID);
	glBindTexture(GL_TEXTURE_2D, aav_textureID);	
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,0,
			GL_RGB,aav_width,aav_height,
			0,GL_RGB,GL_UNSIGNED_BYTE,aav_imageData);
	glGenerateMipmap(GL_TEXTURE_2D);	

	SOIL_free_image_data(aav_imageData);
	return aav_textureID;
}

void resize(int width , int height )
{

	//code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void display(void)
{
	//variable Declaration
	mat4 aav_rotationMatrixA;
	mat4 aav_rotationMatrixX;
	mat4 aav_rotationMatrixY;
	mat4 aav_rotationMatrixZ;

	//fuction declartion
	void update();

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 
	//triangle
	glUseProgram(aav_gShaderProgramObject);

	//Light
	glUniform3fv(aav_laUniform,1,(GLfloat *)aav_lightAmbiant);				// la 
	glUniform3fv(aav_ldUniform,1,(GLfloat *)aav_lightDiffuse);				// ld
	glUniform3fv(aav_lsUniform,1,(GLfloat *)aav_lightSpecular);				// ls
	glUniform4fv(aav_lightPositionUniform,1,(GLfloat *)aav_lightPosition);	//lightPosition

 	glUniform1f(aav_kShininessUniform,128.0f);
	
	//OpenGL Drawing
	mat4 aav_modelMateix = mat4::identity();
	mat4 aav_viewMatrix = mat4::identity();
	mat4 aav_rotationMatrix = mat4::identity();
	mat4 aav_scaleMatrix = mat4::identity();

	mat4 aav_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	aav_rotationMatrixX = vmath::rotate(aav_rotation, 1.0f, 0.0f, 0.0f);
	aav_rotationMatrixY = vmath::rotate(aav_rotation, 0.0f, 1.0f, 0.0f);
	aav_rotationMatrixZ = vmath::rotate(aav_rotation, 0.0f, 0.0f, 1.0f);

	aav_rotationMatrixA = aav_rotationMatrixX * aav_rotationMatrixY * aav_rotationMatrixZ;

	aav_modelMateix = aav_translateMatrix *aav_scaleMatrix * aav_rotationMatrixA;

	glUniformMatrix4fv(aav_modelMatrixUniform,1,GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, aav_texture);
	glUniform1i(aav_textureSamplerUniform, 0);

	glBindVertexArray(aav_gVao_cube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
	glBindVertexArray(0);

	update();

	glXSwapBuffers(aav_gpDisplay,aav_gWindow);
}

void update()
{	
	aav_rotation = aav_rotation + 1.0f;
	if (aav_rotation > 360)
	{
		aav_rotation = 0.0f;
	}
}


void uninitialize(void)
{
	//Code
	if (aav_gVao_cube)
	{
		glDeleteVertexArrays(1, &aav_gVao_cube);
		aav_gVao_cube = 0;
	}

	if (aav_gVbo_cubeAll)
	{
		glDeleteBuffers(1, &aav_gVbo_cubeAll);
		aav_gVbo_cubeAll = 0;
	}

	glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glDeleteShader(aav_gVertexShaderObject);
	aav_gVertexShaderObject = 0;
	glDeleteShader(aav_gFragmentShaderObject);
	aav_gFragmentShaderObject = 0;

	glDeleteProgram(aav_gShaderProgramObject);
	aav_gShaderProgramObject = 0;

	glUseProgram(0);

	glXMakeCurrent(NULL,0,NULL);	
	
	glXDestroyContext(aav_gpDisplay,aav_gGLXContext);
	aav_gGLXContext= NULL;

	
	if(aav_gWindow)
	{
		XDestroyWindow(aav_gpDisplay, aav_gWindow);
	}

	if(aav_gColormap)
	{
		XFreeColormap(aav_gpDisplay, aav_gColormap);
	}

	if(aav_gpXVisualInfo)
	{
		free(aav_gpXVisualInfo);
		aav_gpXVisualInfo = NULL;
	}
	
	if(aav_gpFile)
	{	
		fprintf(aav_gpFile,"Succefully Completed the Program");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

	if(aav_gpDisplay)
	{
		
		XCloseDisplay(aav_gpDisplay);
		aav_gpDisplay = NULL;
	}
}


