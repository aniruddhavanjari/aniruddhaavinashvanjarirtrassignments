#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <GL/glew.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <SOIL/SOIL.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include "vmath.h"
#include "Sphere.h"

//namespaces
using namespace vmath;
using namespace std;

typedef GLXContext (*glxCreateContextAttribsArbProc)(Display*,GLXFBConfig,GLXContext,Bool,const int *);

glxCreateContextAttribsArbProc glxCreateContextAttribsArb = NULL;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};


GLXFBConfig gGLXFBConfig;
//global variable declarations
bool aav_bFullscreen = false;
Display *aav_gpDisplay = NULL;
XVisualInfo *aav_gpXVisualInfo = NULL;

GLXContext aav_gGLXContext;

Colormap aav_gColormap;
Window aav_gWindow;
int aav_giWindowWidth = 800;
int aav_giWindowHeight = 600;


GLuint gVertexShaderObject_pv;
GLuint gFragmentShaderObject_pv;
GLuint gShaderProgramObject_pv;

GLuint gVertexShaderObject_pf;
GLuint gFragmentShaderObject_pf;
GLuint gShaderProgramObject_pf;

GLuint aav_mvp_MatrixUniform;

mat4 aav_PerspectiveProjectionMatrix;

//Sphere Variable
GLfloat aav_sphere_vertices[1146];
GLfloat aav_sphere_normals[1146];
GLfloat aav_sphere_texture[764];
unsigned short aav_sphere_elements[2280];
GLuint aav_numSphereVertices;
GLuint aav_numSphereElements;

GLuint aav_gVao_sphere;
GLuint aav_Vbo_sphere_position;
GLuint aav_Vbo_sphere_normals;
GLuint aav_Vbo_sphere_elements;

bool aav_bLight = false;
bool aav_bPerFragment = false;
bool aav_bPerVertex = true;

//Light Array
GLfloat aav_lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };	//la
GLfloat aav_lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	// ld
GLfloat aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };	//ls

GLfloat aav_lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

//material Array 
GLfloat aav_materialAmbiant[]	= { 0.0f, 0.0f, 0.0f ,0.0f}; // ka 
GLfloat aav_materialDiffuse[]	= {1.0f,1.0f,1.0f,1.0f};	// kd
GLfloat aav_materialSpecular[]	= {1.0f,1.0f,1.0f,1.0f};	// ks

GLfloat aav_materialShininess = 128.0f; // material shininess

GLuint aav_viewMatrixUniform;
GLuint aav_modelMatrixUniform;
GLuint aav_ProjectionMatrixUniform;

GLuint aav_laUniform;
GLuint aav_ldUniform;
GLuint aav_lsUniform;
GLuint aav_lightPositionUniform;

GLuint aav_kaUniform;
GLuint aav_kdUniform;
GLuint aav_ksUniform;

GLuint aav_kShininessUniform;

GLuint aav_lKeyPressedUniform;


//vertex shader
GLuint aav_viewMatrixUniform_v;
GLuint aav_modelMatrixUniform_v;
GLuint aav_ProjectionMatrixUniform_v;

GLuint aav_laUniform_v;
GLuint aav_ldUniform_v;
GLuint aav_lsUniform_v;
GLuint aav_lightPositionUniform_v;

GLuint aav_kaUniform_v;
GLuint aav_kdUniform_v;
GLuint aav_ksUniform_v;

GLuint aav_kShininessUniform_v;

GLuint aav_lKeyPressedUniform_v;

char keys[26];

FILE *aav_gpFile = NULL;
//entry-point fucntoin 

int main(void)
{
	//function prototypes 
	void initialize(void);
	void resize(int ,int);
	void display(void);

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();

	//varuable declarations 
	int aav_winWidth = aav_giWindowWidth;
	int aav_winHeight = aav_giWindowHeight;
	bool aav_bDone = false;

	// Code
	aav_gpFile =fopen("logfile.txt","w+" );
	if(aav_gpFile == NULL)
	{
		printf("Failed to Open A Log File\n");
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
		exit(0);
	}
	else 
	{
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
		
	}		
	
	CreateWindow();
	initialize();

	// Message Loop
	XEvent aav_event;
	KeySym aav_keysym;

	
	
		
	while(aav_bDone == false)
	{
		while(XPending(aav_gpDisplay))
		{
			XNextEvent(aav_gpDisplay,&aav_event);
			switch(aav_event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					aav_keysym = XkbKeycodeToKeysym(aav_gpDisplay,aav_event.xkey.keycode,0,0);
					XLookupString(&aav_event.xkey,keys,sizeof(keys),NULL,NULL);
					switch(keys[0])
					{
						case 'Q':
						case 'q':
							aav_bDone = true;
						break;
						case 'l':
						case 'L':
						if (aav_bLight == false)
						{
							aav_bLight = true;
						}
						else
						{
							aav_bLight = false;
						}
						break;
						case 'f':
						case 'F':
						if (aav_bPerFragment == false)
						{
						aav_bPerVertex = false;
						aav_bPerFragment = true;
						}
			
						break;
						case 'v':
						case 'V':
						if (aav_bPerVertex == false)
						{
							aav_bPerFragment = false;
							aav_bPerVertex = true;
						}
			
						break;			
						default:
							break;
					}
					switch(aav_keysym)
					{
						case XK_Escape:
							if(aav_bFullscreen == false)
							{
								ToggleFullscreen();
								aav_bFullscreen = true;	
							}
							else
							{
								ToggleFullscreen();
								aav_bFullscreen = false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(aav_event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					aav_winWidth = aav_event.xconfigure.width;
					aav_winHeight = aav_event.xconfigure.height;
					resize(aav_winWidth, aav_winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					aav_bDone = true;
					break;
				default:
					break;
			}
		}
		display();

	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//fucntion prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes aav_winAttribs; 

	GLXFBConfig *pGLXFBConfig = NULL;
	GLXFBConfig bestGLXFBConfig;
	XVisualInfo *ptempXVisualInfo = NULL;
	int numFBConfig = 0;	

	int aav_defaultScreen;
	int aav_defaultDepth;
	int aav_styleMask;

	static int aav_frameBufferAttributes[] = {GLX_DOUBLEBUFFER,True,
							GLX_X_RENDERABLE,True,
							GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
							GLX_RENDER_TYPE,GLX_RGBA_BIT,
							GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
							GLX_RED_SIZE,8,
							GLX_GREEN_SIZE,8,
							GLX_BLUE_SIZE,8,
							GLX_ALPHA_SIZE,8,
							GLX_DEPTH_SIZE,24,
							GLX_STENCIL_SIZE,8,
							None};

	//code
	aav_gpDisplay = XOpenDisplay(NULL);
	if(aav_gpDisplay == NULL)
	{
		printf("ERROR: Unable to Open X Display .\n Extting Now ...\n");
		uninitialize();
		exit(1);
	}
	aav_defaultScreen = XDefaultScreen(aav_gpDisplay);

	pGLXFBConfig = glXChooseFBConfig(aav_gpDisplay,XDefaultScreen(aav_gpDisplay),aav_frameBufferAttributes,&numFBConfig);
	printf("Found Number of FBConfig : numFBConfig %d",numFBConfig);
	
	int bestFrameBufferConfig = -1,worstFrameBufferConfig = -1, bestNuberOfSamples = -1, worstNumberOfSamples = 99;
	int i;
	
	for(i = 0 ; i < numFBConfig; i ++)
	{
		ptempXVisualInfo = glXGetVisualFromFBConfig(aav_gpDisplay, pGLXFBConfig[i]);
		if(ptempXVisualInfo != NULL)
		{
			int sampleBuffers, samples;
			glXGetFBConfigAttrib(aav_gpDisplay, pGLXFBConfig[i],GLX_SAMPLE_BUFFERS,&sampleBuffers);
			glXGetFBConfigAttrib(aav_gpDisplay,pGLXFBConfig[i],GLX_SAMPLES,&samples);
			
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNuberOfSamples )
			{
				bestFrameBufferConfig = i;
				bestNuberOfSamples = samples;	
			}
			if(worstFrameBufferConfig < 0 || !sampleBuffers ||  samples < worstNumberOfSamples)
			{
				worstFrameBufferConfig = i;
				worstNumberOfSamples= samples;	
			}
			printf("When i %d , When samples %d , when sampleBuffer %d , when ptempXVisualInfo->VisualID : %lu \n",i , samples, sampleBuffers, ptempXVisualInfo->visualid);
		}
		XFree(ptempXVisualInfo);
	} 

	printf(" bestNuberOfSamples %d ,bestFrameBufferConfig %d \n",bestNuberOfSamples, bestFrameBufferConfig);
	
	bestGLXFBConfig = pGLXFBConfig[bestFrameBufferConfig];
	gGLXFBConfig 	= bestGLXFBConfig;
	
	XFree(pGLXFBConfig);
	
	aav_gpXVisualInfo = glXGetVisualFromFBConfig(aav_gpDisplay,bestGLXFBConfig);	
	
	aav_winAttribs.border_pixel = 0;
	aav_winAttribs.background_pixel = 0;
	aav_winAttribs.colormap = XCreateColormap(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			aav_gpXVisualInfo->visual,
			AllocNone);

	aav_gColormap = aav_winAttribs.colormap;
	aav_winAttribs.background_pixel = BlackPixel(aav_gpDisplay, aav_defaultScreen);

	aav_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	aav_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap; 

	aav_gWindow = XCreateWindow(aav_gpDisplay,
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			0,
			0,
			aav_giWindowWidth,
			aav_giWindowHeight,
			0,
			aav_gpXVisualInfo->depth,
			InputOutput,
			aav_gpXVisualInfo->visual,
			aav_styleMask,
			&aav_winAttribs);

	if(!aav_gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now.. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(aav_gpDisplay, aav_gWindow,"XWindows P.P. AAV : TogglePervertexPerfragmentLighting");

	Atom aav_windowManagerDelete = XInternAtom(aav_gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(aav_gpDisplay, aav_gWindow,&aav_windowManagerDelete,1);
	XMapWindow(aav_gpDisplay, aav_gWindow);
}

void ToggleFullscreen(void)
{	
	
	// variable delarations
	Atom aav_wm_state;
	Atom aav_fullscreen;
	XEvent aav_xev = {0};

	//code
	aav_wm_state = XInternAtom(aav_gpDisplay, "_NET_WM_STATE",False);
	memset(&aav_xev,0,sizeof(aav_xev));

	aav_xev.type = ClientMessage;
	aav_xev.xclient.window = aav_gWindow;
	aav_xev.xclient.message_type = aav_wm_state;
	aav_xev.xclient.format = 32;
	aav_xev.xclient.data.l[0] = aav_bFullscreen ? 0 : 1;

	aav_fullscreen = XInternAtom(aav_gpDisplay, "_NET_WM_STATE_FULLSCREEN",False);
	aav_xev.xclient.data.l[1] = aav_fullscreen;

	XSendEvent(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&aav_xev);
}

void initialize(void)
{
	//fuction declaration
	void resize(int, int);
	void uninitialize(void);
	
	//code
	glxCreateContextAttribsArb = (glxCreateContextAttribsArbProc)glXGetProcAddressARB((GLubyte *)"glXCreateContextAttribsARB");	
	if(glxCreateContextAttribsArb == NULL)
	{
		printf("Failed to Locate ""glXCreateContextAttribsARB""\n");
	}

	const int attribs[]  = {GLX_CONTEXT_MAJOR_VERSION_ARB,4,GLX_CONTEXT_MINOR_VERSION_ARB,5, GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,None};	
	
	aav_gGLXContext = glxCreateContextAttribsArb(aav_gpDisplay, gGLXFBConfig, 0, True, attribs);
	if(!aav_gGLXContext)
	{
		const int attribs1[] = {GLX_CONTEXT_MAJOR_VERSION_ARB,1,GLX_CONTEXT_MINOR_VERSION_ARB,0,None};	
		aav_gGLXContext = glxCreateContextAttribsArb(aav_gpDisplay, gGLXFBConfig, 0, True, attribs1);	
		if(!aav_gGLXContext)
		{
			printf("gGLXContext Failed  \n");
			exit(-1);
		}
	}
	
	Bool isDirectContext = glXIsDirect(aav_gpDisplay, aav_gGLXContext);
	if(isDirectContext == True)
	{
		printf("HardWareRendering Context\n");
	}
	else
	{
		printf("SoftWareRendering Context\n");
	}
		
	glXMakeCurrent(aav_gpDisplay,aav_gWindow,aav_gGLXContext);

	GLenum aav_glew_error = glewInit();
	if(aav_glew_error != GLEW_OK)
	{
		uninitialize();
		exit(1);
	}	
	
	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //gRAPHICS Library shadinng language

	
	//OpenGL Enable Extensions 
	GLint  numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	printf("NumExtension %d\n",numExtension);
	for (unsigned int i = 0; i < numExtension; i++)
	{	
		fprintf(aav_gpFile,"%s\n", glGetStringi(GL_EXTENSIONS,i));
	}
	

	//PerVertex
	//**VERTEX SHADER***
	gVertexShaderObject_pv = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode_pv =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la_pv;" \
		"uniform vec3 u_ld_pv;" \
		"uniform vec3 u_ls_pv;" \
		"uniform vec4 u_lightPosistion_pv;" \
		"uniform vec3 u_ka_pv;" \
		"uniform vec3 u_kd_pv;" \
		"uniform vec3 u_ks_pv;" \
		"uniform float u_kShineness_pv;"\
		"out vec3 fong_ads_light_pv;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{"\
		"		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"		vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"		vec3 lightDirection = normalize(vec3(u_lightPosistion_pv - eyeCordinate));" \
		"		vec3 reflection_vector = reflect(-lightDirection,tranformed_normal);" \
		"		vec3 view_vector = normalize(-eyeCordinate.xyz);" \
		"		vec3 ambiant = u_la_pv * u_ka_pv;" \
		"		vec3 diffuse = u_ld_pv * u_kd_pv * max(dot(lightDirection,tranformed_normal),0.0f);" \
		"		vec3 specular = u_ls_pv * u_ks_pv * pow(max(dot(reflection_vector,view_vector),0.0f),u_kShineness_pv);" \
		"		fong_ads_light_pv = ambiant + diffuse + specular;" \
		"	}" \
		"	else" \
		"	{" \
		"		fong_ads_light_pv = vec3(1.0f,1.0f,1.0f);" \
		"	}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gVertexShaderObject_pv, 1, (const GLchar**)&aav_vertexShaserSourceCode_pv, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(gVertexShaderObject_pv);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_pv, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gVertexShaderObject_pv, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader PerVertex Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	//PerVertex
	//**FRAGMENT SHADER**
	gFragmentShaderObject_pv = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode_pv = 
		"#version 450 core" \
		"\n" \
		"in vec3 fong_ads_light_pv;" \
		"out vec4 FragColor_pv;" \
		"void main(void)" \
		"{" \
		"	FragColor_pv = vec4(fong_ads_light_pv,1.0f);" \
		"}";
	glShaderSource(gFragmentShaderObject_pv, 1, 
		(const GLchar**)&fragmentShaderSourceCode_pv,NULL);
	//compile shader
	glCompileShader(gFragmentShaderObject_pv);

	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(gFragmentShaderObject_pv, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gFragmentShaderObject_pv, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader PerVertex Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	/****************************************************************/
	//PerFragment
	//**VERTEX SHADER***
	gVertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec4 u_lightPosistion_pf;" \
		"out vec3 tranformed_normal_pf;"\
		"out vec3 lightDirection_pf;" \
		"out vec3 view_vector_pf;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{"\
		"		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"		tranformed_normal_pf = (mat3(u_view_matrix* u_model_matrix) * vNormal);" \
		"		lightDirection_pf = (vec3(u_lightPosistion_pf - eyeCordinate));" \
		"		view_vector_pf = (-eyeCordinate.xyz);" \
		"	}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gVertexShaderObject_pf, 1, (const GLchar**)&aav_vertexShaserSourceCode_pf, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(gVertexShaderObject_pf);
	
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;
	szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_pf, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gVertexShaderObject_pf, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader PerFramgment Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	
	//PerFragment
	//**FRAGMENT SHADER**
	gFragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fragmentShaderSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"in vec3 tranformed_normal_pf;"\
		"in vec3 lightDirection_pf;" \
		"in vec3 view_vector_pf;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la_pf;" \
		"uniform vec3 u_ld_pf;" \
		"uniform vec3 u_ls_pf;" \

		"uniform vec3 u_ka_pf;" \
		"uniform vec3 u_kd_pf;" \
		"uniform vec3 u_ks_pf;" \
		"uniform float u_kShineness_pf;"\

		"out vec4 FragColor_pf;" \
		"vec3 fong_ads_light_pf;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{"\
		"		vec3 normalize_tranformed_normal = normalize(tranformed_normal_pf);" \
		"		vec3 normalize_lightDirection = normalize(lightDirection_pf);" \
		"		vec3 normalize_view_vector = normalize(view_vector_pf);" \
		"		vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" \
		"		vec3 ambiant = u_la_pf * u_ka_pf;" \
		"		vec3 diffuse = u_ld_pf * u_kd_pf * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" \
		"		vec3 specular = u_ls_pf * u_ks_pf * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness_pf);" \
		"		fong_ads_light_pf = ambiant + diffuse + specular;" \
		"	}"\
		"	else"\
		"	{"\
		"		fong_ads_light_pf = vec3(1.0f,1.0f,1.0f);"\
		"	}"\
		"	FragColor_pf = vec4(fong_ads_light_pf,1.0f);" \
		"}";
	glShaderSource(gFragmentShaderObject_pf, 1, 
		(const GLchar**)&fragmentShaderSourceCode_pf,NULL);
	//compile shader
	glCompileShader(gFragmentShaderObject_pf);

	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(gFragmentShaderObject_pf, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gFragmentShaderObject_pf, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader of PerFragment Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}

	/****************************************************************/

	//**SHADER PROGRAM For PerVertex**
	//Create 
	gShaderProgramObject_pv = glCreateProgram();

	glAttachShader(gShaderProgramObject_pv, gVertexShaderObject_pv);
	glAttachShader(gShaderProgramObject_pv, gFragmentShaderObject_pv);
		
	glBindAttribLocation(gShaderProgramObject_pv, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject_pv, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject_pv);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	
	glGetProgramiv(gShaderProgramObject_pv, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(gShaderProgramObject_pv, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program PerVertex Link Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}


	/*************************Shader Program For PerFrament***********************/
	//Create 
	gShaderProgramObject_pf = glCreateProgram();

	glAttachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);
	glAttachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);

	glBindAttribLocation(gShaderProgramObject_pf, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject_pf, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject_pf);

	aav_infoLogLength = 0;
	aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_pf, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(gShaderProgramObject_pf, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program PerFragment Link Log: %s\n", szBuffer);
				free(szBuffer);
				XDestroyWindow(aav_gpDisplay, aav_gWindow);
			}
		}
	}
	
	/*****************************************************************************/
	//Post Linking Information
	//PerVertex
	aav_modelMatrixUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_model_matrix");
	aav_viewMatrixUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_view_matrix");
	aav_ProjectionMatrixUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_projection_matrix");

	aav_laUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_la_pv");
	aav_ldUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_pv");
	aav_lsUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_pv");
	aav_lightPositionUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_lightPosistion_pv");
	
	aav_kaUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_ka_pv");
	aav_kdUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_kd_pv");
	aav_ksUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_ks_pv");

	aav_kShininessUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_kShineness_pv");

	aav_lKeyPressedUniform_v = glGetUniformLocation(gShaderProgramObject_pv, "u_lKeyPressed");

	/*************************************************************************************/
	//PerFragment
	aav_modelMatrixUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_model_matrix");
	aav_viewMatrixUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_view_matrix");
	aav_ProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_projection_matrix");
	
	aav_laUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_la_pf");
	aav_ldUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_ld_pf");
	aav_lsUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_ls_pf");
	aav_lightPositionUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_lightPosistion_pf");

	aav_kaUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_ka_pf");
	aav_kdUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_kd_pf");
	aav_ksUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_ks_pf");

	aav_kShininessUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_kShineness_pf");

	aav_lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_lKeyPressed");

	/*****************************************************************************************/

	//vertices array declation
	getSphereVertexData(aav_sphere_vertices, aav_sphere_normals, aav_sphere_texture, aav_sphere_elements);
	
	aav_numSphereVertices = getNumberOfSphereVertices();
	aav_numSphereElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &aav_gVao_sphere);
	glBindVertexArray(aav_gVao_sphere);

	//Record Sphere
	glGenBuffers(1, &aav_Vbo_sphere_position); 
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_vertices), aav_sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//normals
	glGenBuffers(1, &aav_Vbo_sphere_normals);
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_normals), aav_sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE,0,NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//elements
	glGenBuffers(1, &aav_Vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements); // Elemtnes Drawing is Also Called As Index Drawing, Elements Drawing
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(aav_sphere_elements), aav_sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//Record Off / Pause 
	glBindVertexArray(0);

	//glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);
	

	glClearColor(0.0f,0.0f,0.0f,1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();
	resize(aav_giWindowWidth,aav_giWindowHeight);
}

void resize(int width , int height )
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void display(void)
{
	void update();

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 aav_modelMateix;
	mat4 aav_viewMatrix;
	mat4 aav_translateMatrix;
	 
	if (aav_bPerVertex == true)
	{
		glUseProgram(gShaderProgramObject_pv);

		if (aav_bLight == true)
		{
			glUniform1i(aav_lKeyPressedUniform_v, 1);
			//Light
			glUniform3fv(aav_laUniform_v, 1, (GLfloat*)aav_lightAmbiant);				
			glUniform3fv(aav_ldUniform_v, 1, (GLfloat*)aav_lightDiffuse);				
			glUniform3fv(aav_lsUniform_v, 1, (GLfloat*)aav_lightSpecular);			
			glUniform4fv(aav_lightPositionUniform_v, 1, (GLfloat*)aav_lightPosition);	

			//material
			glUniform3fv(aav_kaUniform_v, 1, (GLfloat*)aav_materialAmbiant);	
			glUniform3fv(aav_kdUniform_v, 1, (GLfloat*)aav_materialDiffuse);	
			glUniform3fv(aav_ksUniform_v, 1, (GLfloat*)aav_materialSpecular);	
			glUniform1f(aav_kShininessUniform_v, 128.0f);
		}
		else
		{
			glUniform1i(aav_lKeyPressedUniform, 0);
		}

		//OpenGL Drawing
		aav_modelMateix = mat4::identity();

		aav_viewMatrix = mat4::identity();

		aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

		aav_modelMateix = aav_translateMatrix;

		glUniformMatrix4fv(aav_modelMatrixUniform_v, 1, GL_FALSE, aav_modelMateix);
		glUniformMatrix4fv(aav_viewMatrixUniform_v, 1, GL_FALSE, aav_viewMatrix);
		glUniformMatrix4fv(aav_ProjectionMatrixUniform_v, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

		glBindVertexArray(aav_gVao_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
		glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);

		glUseProgram(0);
	}
	if(aav_bPerFragment == true)
	{
		glUseProgram(gShaderProgramObject_pf);

		if (aav_bLight == true)
		{
			glUniform1i(aav_lKeyPressedUniform, 1);
			//Light
			glUniform3fv(aav_laUniform, 1, (GLfloat*)aav_lightAmbiant);				
			glUniform3fv(aav_ldUniform, 1, (GLfloat*)aav_lightDiffuse);				
			glUniform3fv(aav_lsUniform, 1, (GLfloat*)aav_lightSpecular);				
			glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)aav_lightPosition);	

			//material
			glUniform3fv(aav_kaUniform, 1, (GLfloat*)aav_materialAmbiant);
			glUniform3fv(aav_kdUniform, 1, (GLfloat*)aav_materialDiffuse);	
			glUniform3fv(aav_ksUniform, 1, (GLfloat*)aav_materialSpecular);	
			glUniform1f(aav_kShininessUniform, 128.0f);
		}
		else
		{
			glUniform1i(aav_lKeyPressedUniform, 0);
		}

		//OpenGL Drawing
		aav_modelMateix = mat4::identity();

		aav_viewMatrix = mat4::identity();

		aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

		aav_modelMateix = aav_translateMatrix;

		glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
		glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
		glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

		glBindVertexArray(aav_gVao_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
		glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);

		glUseProgram(0);
	}
	glXSwapBuffers(aav_gpDisplay,aav_gWindow);
}

void uninitialize(void)
{
	//Code
		
	if (aav_gVao_sphere)
	{
		glDeleteVertexArrays(1, &aav_gVao_sphere);
		aav_gVao_sphere = 0;
	}

	if (aav_Vbo_sphere_position)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_position);
		aav_Vbo_sphere_position = 0;
	}

	if (aav_Vbo_sphere_normals)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_normals);
		aav_Vbo_sphere_normals = 0;
	}

	if (aav_Vbo_sphere_elements)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_elements);
		aav_Vbo_sphere_elements = 0;
	}


	glDetachShader(gShaderProgramObject_pv, gVertexShaderObject_pv);
	glDetachShader(gShaderProgramObject_pv, gFragmentShaderObject_pv);

	glDeleteShader(gVertexShaderObject_pv);
	gVertexShaderObject_pv = 0;
	glDeleteShader(gFragmentShaderObject_pv);
	gFragmentShaderObject_pv = 0;

	glDeleteProgram(gShaderProgramObject_pv);
	gShaderProgramObject_pv = 0;


	glDetachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);
	glDetachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);

	glDeleteShader(gVertexShaderObject_pf);
	gVertexShaderObject_pv = 0;
	glDeleteShader(gFragmentShaderObject_pf);
	gFragmentShaderObject_pf = 0;

	glDeleteProgram(gShaderProgramObject_pf);
	gShaderProgramObject_pf = 0;

	glUseProgram(0);


	glXMakeCurrent(NULL,0,NULL);	
	
	glXDestroyContext(aav_gpDisplay,aav_gGLXContext);
	aav_gGLXContext= NULL;

	
	if(aav_gWindow)
	{
		XDestroyWindow(aav_gpDisplay, aav_gWindow);
	}

	if(aav_gColormap)
	{
		XFreeColormap(aav_gpDisplay, aav_gColormap);
	}

	if(aav_gpXVisualInfo)
	{
		free(aav_gpXVisualInfo);
		aav_gpXVisualInfo = NULL;
	}
	
	if(aav_gpFile)
	{	
		fprintf(aav_gpFile,"Succefully Completed the Program");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

	if(aav_gpDisplay)
	{
		
		XCloseDisplay(aav_gpDisplay);
		aav_gpDisplay = NULL;
	}
}


