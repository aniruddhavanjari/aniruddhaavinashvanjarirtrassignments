#include <iostream>

#include <fstream>
#include <cstring>

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <AL/al.h>	// OpenAL
#include <AL/alc.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

#include <math.h>

//namespaces
using namespace std;

//global variable declarations
bool aav_bFullscreen = false;
Display *aav_gpDisplay = NULL;
XVisualInfo *aav_gpXVisualInfo = NULL;

GLXContext aav_gGLXContext;

Colormap aav_gColormap;
Window aav_gWindow;
int aav_giWindowWidth = 800;
int aav_giWindowHeight = 600;

FILE *aav_gpFile = NULL;

unsigned int  aav_bufferid;
unsigned int aav_sourceid;

ALCdevice *aav_device = NULL;
ALCcontext * aav_context = NULL;

char *aav_data = NULL;

//entry-point fucntoin 

int main(void)
{
	//function prototypes 
	void initialize(void);
	void resize(int ,int);
	void display(void);

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();

	//varuable declarations 
	int aav_winWidth = aav_giWindowWidth;
	int aav_winHeight = aav_giWindowHeight;
	bool aav_bDone = false;

	// Code
	CreateWindow();
	initialize();

	XEvent aav_event;
	KeySym aav_keysym;

	aav_gpFile =fopen("logfile.txt","w" );
	if(aav_gpFile == NULL)
	{
		printf("Failed to Open A Log File\n");
		exit(0);
	}
	else 
	{
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
	}

	while(aav_bDone == false)
	{
		while(XPending(aav_gpDisplay))
		{
			XNextEvent(aav_gpDisplay,&aav_event);
			switch(aav_event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					aav_keysym = XkbKeycodeToKeysym(aav_gpDisplay,aav_event.xkey.keycode,0,0);
					switch(aav_keysym)
					{
						case XK_Escape:
							aav_bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(aav_bFullscreen == false)
							{
								ToggleFullscreen();
								aav_bFullscreen = true;	
							}
							else
							{
								ToggleFullscreen();
								aav_bFullscreen = false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(aav_event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					aav_winWidth = aav_event.xconfigure.width;
					aav_winHeight = aav_event.xconfigure.height;
					resize(aav_winWidth, aav_winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					aav_bDone = true;
					break;
				default:
					break;
			}
		}
		display();

	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//fucntion prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes aav_winAttribs; 
	int aav_defaultScreen;
	int aav_defaultDepth;
	int aav_styleMask;
	static int aav_frameBufferAttributes[] = {GLX_DOUBLEBUFFER,True,
							GLX_RGBA,
							GLX_RED_SIZE,8,
							GLX_GREEN_SIZE,8,
							GLX_BLUE_SIZE,8,
							GLX_ALPHA_SIZE,8,
							None};

	//code
	aav_gpDisplay = XOpenDisplay(NULL);
	if(aav_gpDisplay == NULL)
	{
		printf("ERROR: Unable to Open X Display .\n Extting Now ...\n");
		uninitialize();
		exit(1);
	}
	aav_defaultScreen = XDefaultScreen(aav_gpDisplay);


	aav_gpXVisualInfo= (XVisualInfo*)malloc(sizeof(XVisualInfo));
	if(aav_gpXVisualInfo == NULL)
	{
		printf("Error : Unable To Open X Display.\n Exitting Now...  \n");
		uninitialize();
		exit(1);
	}
	
	aav_gpXVisualInfo = glXChooseVisual(aav_gpDisplay,aav_defaultScreen,aav_frameBufferAttributes);
	if(aav_gpXVisualInfo == NULL)
	{
		printf("ERROR :Unable to Get aav_gpXVisualInfo\n");
		uninitialize();
		exit(1);
	}

	aav_winAttribs.border_pixel = 0;
	aav_winAttribs.background_pixel = 0;
	aav_winAttribs.colormap = XCreateColormap(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			aav_gpXVisualInfo->visual,
			AllocNone);

	aav_gColormap = aav_winAttribs.colormap;
	aav_winAttribs.background_pixel = BlackPixel(aav_gpDisplay, aav_defaultScreen);

	aav_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	aav_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap; 

	aav_gWindow = XCreateWindow(aav_gpDisplay,
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			0,
			0,
			aav_giWindowWidth,
			aav_giWindowHeight,
			0,
			aav_gpXVisualInfo->depth,
			InputOutput,
			aav_gpXVisualInfo->visual,
			aav_styleMask,
			&aav_winAttribs);

	if(!aav_gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now.. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(aav_gpDisplay, aav_gWindow,"Aniruddha Avinash Vanjari :Dynamic India");

	Atom aav_windowManagerDelete = XInternAtom(aav_gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(aav_gpDisplay, aav_gWindow,&aav_windowManagerDelete,1);
	XMapWindow(aav_gpDisplay, aav_gWindow);
}

void ToggleFullscreen(void)
{
	// variable delarations
	Atom aav_wm_state;
	Atom aav_fullscreen;
	XEvent aav_xev = {0};

	//code
	aav_wm_state = XInternAtom(aav_gpDisplay, "_NET_WM_STATE",False);
	memset(&aav_xev,0,sizeof(aav_xev));

	aav_xev.type = ClientMessage;
	aav_xev.xclient.window = aav_gWindow;
	aav_xev.xclient.message_type = aav_wm_state;
	aav_xev.xclient.format = 32;
	aav_xev.xclient.data.l[0] = aav_bFullscreen ? 0 : 1;

	aav_fullscreen = XInternAtom(aav_gpDisplay, "_NET_WM_STATE_FULLSCREEN",False);
	aav_xev.xclient.data.l[1] = aav_fullscreen;

	XSendEvent(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&aav_xev);
}

void initialize(void)
{
	//fuction declaration
	void uninitialize(void);
	void resize(int, int);
	bool isBigIndian(void);
	int convertToInteger(char*, int);
	char* loadWavFile(const char* , int&, int&, int&, int&);
	
	//variable declaration
	int aav_channel;
	int aav_sampleRate;
	int aav_bps;
	int aav_size;
	unsigned int  aav_format;

	//code
	aav_gGLXContext = glXCreateContext(aav_gpDisplay, aav_gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(aav_gpDisplay,aav_gWindow,aav_gGLXContext);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	
	//OpenAL
	aav_data = loadWavFile("test.wav",aav_channel, aav_sampleRate, aav_bps, aav_size);
	aav_device = alcOpenDevice(NULL);
	if(aav_device == NULL)
	{
		printf("cannot open Sound Card\n");
		uninitialize();
		exit(-1);
	}

	aav_context = alcCreateContext(aav_device, NULL);
	if(aav_device == NULL)
	{
		printf("cannot open Context\n");
		uninitialize();
		exit(-1);
	}

	alcMakeContextCurrent(aav_context);


	alGenBuffers(1,&aav_bufferid);

	if(aav_channel == 1)
	{
		if(aav_bps == 8)
		{
			aav_format = AL_FORMAT_MONO8; 
		}
		else
		{
			aav_format = AL_FORMAT_MONO16;
		}
	}
	else
	{
		if(aav_bps == 8)
		{
			aav_format = AL_FORMAT_STEREO8; 
			
		}
		else
		{
			aav_format = AL_FORMAT_STEREO16;
		}
	}
	alBufferData(aav_bufferid, aav_format ,aav_data, aav_size,aav_sampleRate);

	alGenSources(1,&aav_sourceid);
	alSourcei(aav_sourceid,AL_BUFFER,aav_bufferid);
	alSourcePlay(aav_sourceid);

	resize(aav_giWindowWidth,aav_giWindowHeight);
}

bool isBigIndian(void)
{
	int aav = 1; 
	return !((char*)&aav)[0];
}

int convertToInteger(char* buffer, int len)
{
	int aav = 0; 
	if (!isBigIndian())
	{
		for (int i = 0; i < len; i++)
		{
			((char*)&aav)[i] = buffer[i];
		}
	}
	else
	{
		for (int i = 0; i < len; i++)
		{
			((char*)&aav)[3 - i] = buffer[i];
		}
	}
	return(aav);
}


char * loadWavFile(const char* fn, int& chan, int& samplerate, int& bps, int& size)
{
	//fucntion declaration
	int convertToInteger(char*, int);
	
	//vraible declaration
	char aav_buffer[4];
	char *aav_dataSize = NULL;

	//code
	std::ifstream in(fn, std::ios::binary); // we are reading a binary file
	in.read(aav_buffer, 4);

	if (strncmp(aav_buffer, "RIFF", 4) != 0)
	{
		printf("This is not a Valid WAVE file \n");
		return NULL;
	}

	in.read(aav_buffer, 4);
	in.read(aav_buffer, 4); // WAVE format check
	in.read(aav_buffer, 4); // fmt
	in.read(aav_buffer, 4); // 16
	in.read(aav_buffer, 2); // 1
	in.read(aav_buffer, 2); 

	chan = convertToInteger(aav_buffer, 2);
	in.read(aav_buffer, 4);

	samplerate = convertToInteger(aav_buffer, 4);
	in.read(aav_buffer, 4);
	in.read(aav_buffer, 2);
	in.read(aav_buffer, 2);

	bps = convertToInteger(aav_buffer, 2);
	in.read(aav_buffer, 4); // data
	in.read(aav_buffer, 4);

	size = convertToInteger(aav_buffer, 4);
	aav_dataSize = new char[size];
	in.read(aav_dataSize,size);
	return aav_dataSize;
	
}
void resize(int width , int height )
{
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

}

void display(void)
{
	void BharatViman(void);

	//Variable 

	static GLfloat I = -2.5f;
	static GLfloat N = 2.5f;
	static GLfloat D = 0.0f;
	static GLfloat ISend = -2.5f;
	static GLfloat A = 1.5f;
	static GLfloat G = 0.0f;
	static GLfloat B = 0.0f;

	/*static GLfloat I = 0.0f;
	static GLfloat N = 0.0f;
	static GLfloat D = 1.0f;
	static GLfloat ISend = 0.0f;
	static GLfloat A = 0.0f;*/
	static GLfloat PlaneTranslate = -2.0f;

	static GLfloat T1 = 0.0f;

	GLfloat size = 0.3f;
	static GLfloat  angle = 0.0f;
	static GLfloat  angle1 = 360.0f;
	static GLfloat angle3 = 90.0f;
	static GLfloat angle4 = -90.0f;
	GLint flag = 0;
	//Code
	glClear((GL_COLOR_BUFFER_BIT));
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(I, 0.0f, -3.0f);
	//I
	glBegin(GL_QUADS);
	//Top 
	glColor3f(1.0f, 0.3921f, 0.0f);
	glVertex3f(-1.1f, 0.8f, 0.0f);
	glVertex3f(-1.4f, 0.8f, 0.0f);
	glVertex3f(-1.4f, 0.7f, 0.0f);
	glVertex3f(-1.1f, 0.7f, 0.0f);

	//Mid
	glColor3f(1.0f, 0.3921f, 0.0f);
	glVertex3f(-1.2f, 0.7f, 0.0f);
	glVertex3f(-1.3f, 0.7f, 0.0f);
	glColor3f(0.0f, 0.3921f, 0.2352f);
	glVertex3f(-1.3f, -0.8f, 0.0f);
	glVertex3f(-1.2f, -0.8f, 0.0f);

	//Bottom
	glColor3f(0.0f, 0.3921f, 0.2352f);
	glVertex3f(-1.1f, -0.8f, 0.0f);
	glVertex3f(-1.4f, -0.8f, 0.0f);
	glVertex3f(-1.4f, -0.9f, 0.0f);
	glVertex3f(-1.1f, -0.9f, 0.0f);
	glEnd();
	I = I + 0.01f;
	if (I >= 0.0f)
	{
		I = 0.0f;
	}

	if ( I >= 0.0f)
	{
		glLoadIdentity();
		glTranslatef(0.0f, N, -3.0f);
		//N
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(-0.8f, 0.8f, 0.0f);
		glVertex3f(-0.9f, 0.8f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(-0.9f, -0.9f, 0.0f);
		glVertex3f(-0.8f, -0.9f, 0.0f);

		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(-0.8f, 0.8f, 0.0f);
		glVertex3f(-0.9f, 0.8f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(-0.6f, -0.9f, 0.0f);
		glVertex3f(-0.5f, -0.9f, 0.0f);

		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(-0.5f, 0.8f, 0.0f);
		glVertex3f(-0.6f, 0.8f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(-0.6f, -0.9f, 0.0f);
		glVertex3f(-0.5f, -0.9f, 0.0f);
		glEnd();
		N = N - 0.01f;
		if (N <= 0.0f)
		{
			N = 0.0f;
		}
	}

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	//D
	//TOP
	//glColor3f(1.0f, 0.3921f, 0.0f); Kesri
	//glColor3f(0.0f, 0.3921f, 0.2352f);
	glBegin(GL_QUADS);
	glColor3f(D, G, 0.0f);
	glVertex3f(0.3f, 0.8f, 0.0f);
	glVertex3f(-0.3f, 0.8f, 0.0f);
	glVertex3f(-0.3f, 0.7f, 0.0f);
	glVertex3f(0.3f, 0.7f, 0.0f);

	//First Vertical
	glVertex3f(-0.08f, 0.7f, 0.0f);
	glVertex3f(-0.18f, 0.7f, 0.0f);
	glColor3f(0.0f, G, B);
	glVertex3f(-0.18f, -0.8f, 0.0f);
	glVertex3f(-0.08f, -0.8f, 0.0f);


	//Second
	glColor3f(D, G, 0.0f);
	glVertex3f(0.3f, 0.7f, 0.0f);
	glVertex3f(0.2f, 0.7f, 0.0f);
	glColor3f(0.0f, G, B);
	glVertex3f(0.2f, -0.8f, 0.0f);
	glVertex3f(0.3f, -0.8f, 0.0f);


	//Bottom
	glColor3f(0.0f, G, B);
	glVertex3f(0.3f, -0.8f, 0.0f);
	glVertex3f(-0.3f, -0.8f, 0.0f);
	glVertex3f(-0.3f, -0.9f, 0.0f);
	glVertex3f(0.3f, -0.9f, 0.0f);
	glEnd();
	D = D + 0.00075f;
	if (D >= 1.0f)
	{
		D = 1.0f;
	}
	G = G + 0.00045f;
	if (G >= 0.3921f)
	{
		G = 0.3921f;
	}
	B = B + 0.0005f;
	if (B >= 0.2352f)
	{
		B = 0.2352f;
	}

	if (N <= 0.0f)
	{
		glTranslatef(0.0f, ISend, 0.0f);
		//Second I
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(0.8f, 0.8f, 0.0f);
		glVertex3f(0.5f, 0.8f, 0.0f);
		glVertex3f(0.5f, 0.7f, 0.0f);
		glVertex3f(0.8f, 0.7f, 0.0f);

		//Mid
		glVertex3f(0.7f, 0.7f, 0.0f);
		glVertex3f(0.6f, 0.7f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(0.6f, -0.8f, 0.0f);
		glVertex3f(0.7f, -0.8f, 0.0f);

		//Bottom
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(0.8f, -0.8f, 0.0f);
		glVertex3f(0.5f, -0.8f, 0.0f);
		glVertex3f(0.5f, -0.9f, 0.0f);
		glVertex3f(0.8f, -0.9f, 0.0f);
		glEnd();
		ISend = ISend + 0.01f;
		if (ISend >= 0)
		{
			ISend = 0.0f;
		}
	}

	if (ISend >= 0.0f)
	{
		glLoadIdentity();
		glTranslatef(A, 0.0f, -3.0f);
		//A
		//Left.
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(1.3f, 0.8f, 0.0f);
		glVertex3f(1.2f, 0.8f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(1.0f, -0.9f, 0.0f);
		glVertex3f(1.1f, -0.9f, 0.0f);

		//Mid
		//0.0333
		if (PlaneTranslate > 1.2f)
		{
			glColor3f(1.0f, 0.3921f, 0.0f);
			glVertex3f(1.295f, 0.0f, 0.0f);
			glVertex3f(1.205f, 0.0f, 0.0f);
			glVertex3f(1.203f, -0.033f, 0.0f);
			glVertex3f(1.30f, -0.033f, 0.0f);

			glColor3f(1.0f, 01.0f, 1.0f);
			glVertex3f(1.30f, -0.033f, 0.0f);
			glVertex3f(1.202f, -0.033f, 0.0f);
			glVertex3f(1.198f, -0.066f, 0.0f);
			glVertex3f(1.305f, -0.066f, 0.0f);

			glColor3f(0.0f, 0.3921f, 0.2352f);
			glVertex3f(1.305f, -0.066f, 0.0f);
			glVertex3f(1.198f, -0.066f, 0.0f);
			glVertex3f(1.194f, -0.099f, 0.0f);
			glVertex3f(1.35f, -0.099f, 0.0f);
		}

		//Right
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(1.2f, 0.8f, 0.0f);
		glVertex3f(1.3f, 0.8f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(1.5f, -0.9f, 0.0f);
		glVertex3f(1.4f, -0.9f, 0.0f);
		A = A - 0.01f;
		if (A <= 0)
		{
			A = 0.0f;
		}
		glEnd();
	}
	if (A == 0 && angle != 90.0f)
	{
		glLoadIdentity();
		glTranslatef(-1.0f, 1.0f - 0.0495f, -3.0f);
		glRotatef(angle, 0.0f, 0.0f, 1.0f);
		glTranslatef(-1.0f, 0.0f, 0.0f);

		glScalef(-0.5, -0.5f, -0.5f);

		BharatViman();
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(-0.033, -0.6f, 0.0f);
		glVertex3f(-0.099f, -0.6f, 0.0f);
		glVertex3f(-0.099f, -0.9f, 0.0f);
		glVertex3f(-0.033f, -0.9f, 0.0f);
		glEnd();

		angle = angle + 0.1f;
		if (angle > 90)
		{
			angle = 90.0f;
		}
	}
	/////////////////////////////////////////////////////////////////////////////
		//Stage = 2;
	if (A == 0 && angle1 != -90.0f)
	{
		glLoadIdentity();
		glTranslatef(-1.0f, -1.0495f, -3.0f);
		glRotatef(angle1, 0.0f, 0.0f, 1.0f);
		glTranslatef(-1.0f, 0.0f, 0.0f);
		glRotatef(180, 0.0f, 0.0f, 1.0f);
		glScalef(-0.5, -0.5f, -0.5f);

		BharatViman();
		glBegin(GL_QUADS);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(0.033f, -0.6f, 0.0f);
		glVertex3f(0.099f, -0.6f, 0.0f);
		glVertex3f(0.099f, -0.9f, 0.0f);
		glVertex3f(0.033f, -0.9f, 0.0f);
		glEnd();


		angle1 = angle1 - 0.1f;
		if (angle1 < 360 - 90)
		{
			angle1 = -90.0f;
		}
	}
	////////////////////////////////////////////////////////////////////////////////
		//Stage = 3;//0.0165
	if (A == 0)
	{
		glLoadIdentity();
		glTranslatef(PlaneTranslate, -0.0495f, -3.0f);
		glRotatef(90, 0.0f, 0.0f, 1.0f);
		glScalef(-0.5, -0.5f, -0.5f);
		BharatViman();
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.3921f, 1.2352f);
		glVertex3f(0.033f, -0.6f, 0.0f);
		glVertex3f(-0.033f, -0.6f, 0.0f);
		glVertex3f(-0.033f, -0.9f, 0.0f);
		glVertex3f(0.033f, -0.9f, 0.0f);
		glEnd();

		if (angle == 90 && PlaneTranslate < 1.6f)
		{
			glBegin(GL_QUADS);
			glColor3f(0.0f, 0.3921f, 0.2352f);
			glVertex3f(0.033f, -0.6f, 0.0f);
			glVertex3f(0.099f, -0.6f, 0.0f);
			glVertex3f(0.099f, -0.9f, 0.0f);
			glVertex3f(0.033f, -0.9f, 0.0f);
			glEnd();

			glBegin(GL_QUADS);
			glColor3f(1.0f, 0.3921f, 0.0f);
			glVertex3f(-0.033, -0.6f, 0.0f);
			glVertex3f(-0.099f, -0.6f, 0.0f);
			glVertex3f(-0.099f, -0.9f, 0.0f);
			glVertex3f(-0.033f, -0.9f, 0.0f);
			glEnd();
		}

		PlaneTranslate = PlaneTranslate + 0.00111;
		if (PlaneTranslate > 2.8f)
		{
			PlaneTranslate = 2.8f;
		}
	}

	//PlaneTranslate = 2.8f;
	//////////////////////////////////////////////////////////////////////////
	//Vimanas
	//Stage = 1.1;
	if (PlaneTranslate > 1.6f)
	{
		glLoadIdentity();
		glTranslatef(1.6f, 1.0f - 0.0495f, -3.0f);
		glRotatef(angle3, 0.0f, 0.0f, 1.0f);
		glTranslatef(-1.0f, 0.0f, 0.0f);

		glScalef(-0.5, -0.5f, -0.5f);

		BharatViman();
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(-0.033, -0.6f, 0.0f);
		glVertex3f(-0.099f, -0.6f, 0.0f);
		glVertex3f(-0.099f, -0.9f, 0.0f);
		glVertex3f(-0.033f, -0.9f, 0.0f);
		glEnd();

		angle3 = angle3 + 0.1f;
		if (angle3 >= 180)
		{
			angle3 = 180.0f;
		}
	}
	/////////////////////////////////////////////////////////////////////////////
		//Stage = 2.1;
	if (PlaneTranslate > 1.6f)
	{
		glLoadIdentity();
		glTranslatef(1.6f, -1.0495f, -3.0f);
		glRotatef(angle4, 0.0f, 0.0f, 1.0f);
		glTranslatef(-1.0f, 0.0f, 0.0f);
		glRotatef(180, 0.0f, 0.0f, 1.0f);
		glScalef(-0.5, -0.5f, -0.5f);

		BharatViman();
		glBegin(GL_QUADS);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(0.033f, -0.6f, 0.0f);
		glVertex3f(0.099f, -0.6f, 0.0f);
		glVertex3f(0.099f, -0.9f, 0.0f);
		glVertex3f(0.033f, -0.9f, 0.0f);
		glEnd();


		angle4 = angle4 - 0.1f;
		if (angle4 <= -180)
		{
			angle4 = -180.0f;
		}
	}
	glXSwapBuffers(aav_gpDisplay,aav_gWindow);
}


void BharatViman()
{
	void Circle(GLfloat r, GLfloat x, GLfloat y);

	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	glVertex3f(-0.4, -0.3f, 0.0f);
	glVertex3f(-0.45f, -0.3f, 0.0f);
	glVertex3f(-0.45f, -0.5f, 0.0f);
	glVertex3f(-0.4f, -0.5f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.45, -0.3f, 0.0f);
	glVertex3f(0.4f, -0.3f, 0.0f);
	glVertex3f(0.4f, -0.5f, 0.0f);
	glVertex3f(0.45f, -0.5f, 0.0f);
	glEnd();

	//Upper Triangle
	glColor3f(0.3921f, 0.7058f, 0.9019f);
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.5f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glColor3f(0.33f, 0.8f, 1.0f);
	glVertex3f(0.5f, -0.5, 0.0f);
	glVertex3f(-0.5f, -0.5, 0.0f);
	glEnd();

	//Top Triangle
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.5f, 1.0f);
	glVertex3f(0.08f, 0.2f, 0.0f);
	glVertex3f(-0.08f, 0.2f, 0.0f);
	glEnd();

	//Mid Left Triangle 
	glBegin(GL_TRIANGLES);
	glColor3f(0.3921f, 0.7058f, 0.9019f);
	glVertex3f(-0.1f, 0.1f, 0.0f);
	glColor3f(0.0f, 0.5f, 1.0f);
	glVertex3f(-0.2f, -0.2f, 0.0f);
	glVertex3f(-0.1f, -0.2f, 0.0f);
	glEnd();

	//Mid Right Triangle 
	glBegin(GL_TRIANGLES);
	glColor3f(0.33f, 0.8f, 1.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glColor3f(0.0f, 0.5f, 1.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);
	glVertex3f(0.1f, -0.2f, 0.0f);
	glEnd();

	//Upper Quad
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(-0.1, 0.2f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(0.1f, -0.2f, 0.0f);
	glVertex3f(-0.1f, -0.2f, 0.0f);
	glEnd();

	//Bottom Quad
	glColor3f(0.0f, 0.5f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.2f, -0.5f, 0.0f);
	glVertex3f(-0.2f, -0.5f, 0.0f);
	glColor3f(0.3921f, 0.7058f, 0.9019f);
	glVertex3f(-0.3f, -0.6f, 0.0f);
	glVertex3f(0.3f, -0.6f, 0.0f);
	glEnd();

	//Innder Cabinate
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	glVertex3f(-0.05, 0.1f, 0.0f);
	glVertex3f(0.051f, 0.1f, 0.0f);
	glVertex3f(0.05f, -0.1f, 0.0f);
	glVertex3f(-0.05f, -0.1f, 0.0f);
	glEnd();


	glLineWidth(1.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, -0.25f, 0.0f);
	glVertex3f(0.0f, -0.45f, 0.0f);
	glVertex3f(0.0f, -0.6f, 0.0f);
	glEnd();


	glPointSize(5.0f);
	glBegin(GL_POINTS);
	glVertex3f(0.0f, 0.0F, 0.0f);
	glEnd();

	glColor3f(1.0f, 0.3f, 0.0f);
	Circle(0.03f, -0.135f, -0.1f);
	glColor3f(1.0f, 1.0f, 1.0f);
	Circle(0.02f, -0.135f, -0.1f);
	glColor3f(0.0f, 0.4f, 0.3f);
	Circle(0.01f, -0.135f, -0.1f);


	glColor3f(1.0f, 0.3f, 0.0f);
	Circle(0.03f, 0.135f, -0.1f);
	glColor3f(1.0f, 1.0f, 1.0f);
	Circle(0.02f, 0.135f, -0.1f);
	glColor3f(0.0f, 0.4f, 0.3f);
	Circle(0.01f, 0.135f, -0.1f);

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	//I
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.03f, -0.4f, 0.0f);
	glVertex3f(-0.03f, -0.4f, 0.0f);
	glVertex3f(0.03f, -0.38f, 0.0f);
	glVertex3f(0.03f, -0.42f, 0.0f);
	glVertex3f(-0.03f, -0.38f, 0.0f);
	glVertex3f(-0.03f, -0.42f, 0.0f);

	////A
	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.03f, -0.360f, 0.0f);
	glVertex3f(-0.03f, -0.346f, 0.0f);
	glVertex3f(0.03f, -0.330f, 0.0f);
	glVertex3f(-0.03f, -0.346f, 0.0f);
	glVertex3f(0.0f, -0.335f, 0.0f);
	glVertex3f(0.0f, -0.355f, 0.0f);

	////F
	//glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.03f, -0.31f, 0.0f);
	glVertex3f(-0.03f, -0.31f, 0.0f);
	glVertex3f(-0.03f, -0.275f, 0.0f);
	glVertex3f(-0.03f, -0.31f, 0.0f);
	glVertex3f(0.0f, -0.285f, 0.0f);
	glVertex3f(0.0f, -0.31f, 0.0f);
	glEnd();

}

void Circle(GLfloat r, GLfloat x, GLfloat y)
{
	glBegin(GL_LINES);
	GLfloat angle = 0.0f;
	for (angle = 0.0f; angle < 2 * 3.14; angle = angle + 0.01f)
	{
		glVertex3f(x, y, 0.0f);
		glVertex3f(r * sin(angle) + x, r * cos(angle) + y, 0.0f);
	}
	glEnd();
}


void uninitialize(void)
{
	
	if(aav_gWindow)
	{
		XDestroyWindow(aav_gpDisplay, aav_gWindow);
	}

	if(aav_gColormap)
	{
		XFreeColormap(aav_gpDisplay, aav_gColormap);
	}

	if(aav_gpXVisualInfo)
	{
		free(aav_gpXVisualInfo);
		aav_gpXVisualInfo = NULL;
	}
	
	if(aav_gpDisplay)
	{
		XCloseDisplay(aav_gpDisplay);
		aav_gpDisplay = NULL;
	}
	
	
	
	if(aav_sourceid)
	{
		alDeleteSources(1,&aav_sourceid);
		aav_sourceid = 0;
	}
	
	if(aav_bufferid)
	{
		alDeleteSources(1,&aav_bufferid);
		aav_bufferid = 0;
	}
	
	if(aav_context)
	{	
		alcDestroyContext(aav_context);	
		aav_context = NULL;
	}
	
	if(aav_device)
	{
		alcCloseDevice(aav_device);
		aav_device = NULL;
	}

	delete[] aav_data;
	
	
	if(aav_gpFile)
	{
		fprintf(aav_gpFile,"Succesfully Closed File End of Program.\n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

}


