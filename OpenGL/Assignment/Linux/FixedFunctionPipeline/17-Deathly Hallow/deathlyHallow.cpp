#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <memory.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include <math.h>
//namespaces
using namespace std;

#define PI  3.14

//global variable declarations
bool aav_bFullscreen = false;
Display *aav_gpDisplay = NULL;
XVisualInfo *aav_gpXVisualInfo = NULL;

GLXContext aav_gGLXContext;

Colormap aav_gColormap;
Window aav_gWindow;
int aav_giWindowWidth = 800;
int aav_giWindowHeight = 600;

// For Calculating the x,y Corcidinate and Center of Triangle To Plot the InCircle
GLdouble xcenterCor;
GLdouble ycenterCor;

FILE *aav_gpFile = NULL;

//entry-point fucntoin 

int main(void)
{
	//function prototypes 
	void initialize(void);
	void resize(int ,int);
	void display(void);

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();

	//varuable declarations 
	int aav_winWidth = aav_giWindowWidth;
	int aav_winHeight = aav_giWindowHeight;
	bool aav_bDone = false;

	// Code
	CreateWindow();
	initialize();

	// Message Loop
	XEvent aav_event;
	KeySym aav_keysym;

	aav_gpFile =fopen("logfile.txt","w" );
	if(aav_gpFile == NULL)
	{
		printf("Failed to Open A Log File\n");
		exit(0);
	}
	else 
	{
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
	}

	while(aav_bDone == false)
	{
		while(XPending(aav_gpDisplay))
		{
			XNextEvent(aav_gpDisplay,&aav_event);
			switch(aav_event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					aav_keysym = XkbKeycodeToKeysym(aav_gpDisplay,aav_event.xkey.keycode,0,0);
					switch(aav_keysym)
					{
						case XK_Escape:
							aav_bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(aav_bFullscreen == false)
							{
								ToggleFullscreen();
								aav_bFullscreen = true;	
							}
							else
							{
								ToggleFullscreen();
								aav_bFullscreen = false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(aav_event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					aav_winWidth = aav_event.xconfigure.width;
					aav_winHeight = aav_event.xconfigure.height;
					resize(aav_winWidth, aav_winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					aav_bDone = true;
					break;
				default:
					break;
			}
		}
		display();

	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//fucntion prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes aav_winAttribs; 
	int aav_defaultScreen;
	int aav_defaultDepth;
	int aav_styleMask;
	static int aav_frameBufferAttributes[] = {GLX_DOUBLEBUFFER,True,
							GLX_RGBA,
							GLX_RED_SIZE,8,
							GLX_GREEN_SIZE,8,
							GLX_BLUE_SIZE,8,
							GLX_ALPHA_SIZE,8,
							None};

	//code
	aav_gpDisplay = XOpenDisplay(NULL);
	if(aav_gpDisplay == NULL)
	{
		printf("ERROR: Unable to Open X Display .\n Extting Now ...\n");
		uninitialize();
		exit(1);
	}
	aav_defaultScreen = XDefaultScreen(aav_gpDisplay);


	aav_gpXVisualInfo= (XVisualInfo*)malloc(sizeof(XVisualInfo));
	if(aav_gpXVisualInfo == NULL)
	{
		printf("Error : Unable To Open X Display.\n Exitting Now...  \n");
		uninitialize();
		exit(1);
	}
	
	aav_gpXVisualInfo = glXChooseVisual(aav_gpDisplay,aav_defaultScreen,aav_frameBufferAttributes);
	if(aav_gpXVisualInfo == NULL)
	{
		printf("ERROR :Unable to Get aav_gpXVisualInfo\n");
		uninitialize();
		exit(1);
	}

	aav_winAttribs.border_pixel = 0;
	aav_winAttribs.background_pixel = 0;
	aav_winAttribs.colormap = XCreateColormap(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			aav_gpXVisualInfo->visual,
			AllocNone);

	aav_gColormap = aav_winAttribs.colormap;
	aav_winAttribs.background_pixel = BlackPixel(aav_gpDisplay, aav_defaultScreen);

	aav_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	aav_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap; 

	aav_gWindow = XCreateWindow(aav_gpDisplay,
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			0,
			0,
			aav_giWindowWidth,
			aav_giWindowHeight,
			0,
			aav_gpXVisualInfo->depth,
			InputOutput,
			aav_gpXVisualInfo->visual,
			aav_styleMask,
			&aav_winAttribs);

	if(!aav_gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now.. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(aav_gpDisplay, aav_gWindow,"Aniruddha Avinash Vanjari :DeathlyHallow");

	Atom aav_windowManagerDelete = XInternAtom(aav_gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(aav_gpDisplay, aav_gWindow,&aav_windowManagerDelete,1);
	XMapWindow(aav_gpDisplay, aav_gWindow);
}

void ToggleFullscreen(void)
{
	// variable delarations
	Atom aav_wm_state;
	Atom aav_fullscreen;
	XEvent aav_xev = {0};

	//code
	aav_wm_state = XInternAtom(aav_gpDisplay, "_NET_WM_STATE",False);
	memset(&aav_xev,0,sizeof(aav_xev));

	aav_xev.type = ClientMessage;
	aav_xev.xclient.window = aav_gWindow;
	aav_xev.xclient.message_type = aav_wm_state;
	aav_xev.xclient.format = 32;
	aav_xev.xclient.data.l[0] = aav_bFullscreen ? 0 : 1;

	aav_fullscreen = XInternAtom(aav_gpDisplay, "_NET_WM_STATE_FULLSCREEN",False);
	aav_xev.xclient.data.l[1] = aav_fullscreen;

	XSendEvent(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&aav_xev);
}

void initialize(void)
{
	//fuction declaration
	void resize(int, int);

	//code
	aav_gGLXContext = glXCreateContext(aav_gpDisplay, aav_gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(aav_gpDisplay,aav_gWindow,aav_gGLXContext);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	resize(aav_giWindowWidth,aav_giWindowHeight);
}

void resize(int width , int height )
{
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

}

void display(void)
{
	//Function Declaration
	void Triangle(GLfloat);
	void Circle(GLfloat);

	//varaible 
	GLint Line = 0;
	GLfloat fLine = 0.5f;
	static GLfloat angle = 0.0f;
	static GLfloat translateCircle = -2.0f;
	static GLfloat translateTriangle = -2.0f;
	static GLfloat translateLine = 2.0f;


	GLdouble a; //Consider side 1
	GLdouble b; //Consider side 2
	GLdouble c; //Consider side 3


	GLfloat side;

	GLfloat r; //  Radius of inCircle 

	//Code
	a = sqrt(((0.5 - (-0.5)) * (0.5 - (-0.5))) + (((-0.5) - (-0.5)) * ((-0.5) - (-0.5)))); //1
	b = sqrt(((0.0 - 0.5) * (0.0 - 0.5)) + ((0.5 - (-0.5)) * (0.5 - (-0.5)))); //1.118033989
	c = sqrt((((-0.5) - 0.0) * ((-0.5) - 0.0)) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); //1.118033989
	//fprintf(pgFile, "a = %lf\n", a);
	//fprintf(pgFile, "b = %lf\n", b);
	//fprintf(pgFile, "c = %lf\n", c);

	//Formula For x,y Cordinate for Center Point of Triangle To Draw the Circle.
	xcenterCor = ((a * (0)) + (b * (-0.5)) + (c * (0.5))) / (a + b + c);
	ycenterCor = ((a * (0.5f)) + (b * (-0.5f)) + (c * (-0.5f))) / (a + b + c);
	//fprintf(pgFile, "xcenterCor = %lf\n", xcenterCor);
	//fprintf(pgFile, "ycenterCor = %lf\n\n", ycenterCor);

	//Formmula For inCircle or inscribed Circle Radius. 
	side = (a + b + c) / 2;
	r = sqrt(side * (side - a) * (side - b) * (side - c)) / side;  


	glClear((GL_COLOR_BUFFER_BIT));
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(0.0f, translateLine , -3.0f);
	glLineWidth(5.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f, fLine, 0.0f);
	glVertex3f(0.0f, -fLine, 0.0f);
	glEnd();
	translateLine = translateLine - 0.0010f;
	if (translateLine < 0)
	{
		translateLine = 0.0f;
	}

	glLoadIdentity();
	glTranslatef(translateTriangle, translateTriangle, -3.0f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	Triangle(fLine);
	if (translateTriangle > 0)
	{
		translateTriangle = 0.0f;
	}
	translateTriangle = translateTriangle + 0.0010f;//0.0001111f;

	glLoadIdentity();
	glPointSize(5.0f);
	glTranslatef(-(translateCircle), translateCircle, -3.0f);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	Circle(r);
	if( translateCircle > 0.0f)
	{
	translateCircle = 0.0f;
	}
	translateCircle = translateCircle + 0.001f;
	angle = angle + 0.09;
	if (angle > 360)
	{
		angle = 0.0f;
	}
	glXSwapBuffers(aav_gpDisplay,aav_gWindow);
}

void Triangle(GLfloat Line)
{
	glBegin(GL_LINES);
	glVertex3f(0.0f, Line, 0.0f);
	glVertex3f(-Line, -Line, 0.0f);

	glVertex3f(-Line, -Line, 0.0f);
	glVertex3f(Line, -Line, 0.0f);

	glVertex3f(Line, -Line, 0.0f);
	glVertex3f(0.0f, Line, 0.0f);
	glEnd();
}

void Circle(GLfloat fRadius)
{
	GLfloat angle = 0; 
	glBegin(GL_POINTS);
	for (angle = 0; angle < 2 * PI; angle = angle + 0.001)
	{
		glVertex3f(fRadius*sin(angle)+ xcenterCor, fRadius*cos(angle)+ ycenterCor, 0.0f);
	}
	glEnd();
}

void uninitialize(void)
{
	if(aav_gWindow)
	{
		XDestroyWindow(aav_gpDisplay, aav_gWindow);
	}

	if(aav_gColormap)
	{
		XFreeColormap(aav_gpDisplay, aav_gColormap);
	}

	if(aav_gpXVisualInfo)
	{
		free(aav_gpXVisualInfo);
		aav_gpXVisualInfo = NULL;
	}
	
	if(aav_gpDisplay)
	{
		XCloseDisplay(aav_gpDisplay);
		aav_gpDisplay = NULL;
	}
	
	if(aav_gpFile)
	{
		fprintf(aav_gpFile,"Succesfully Closed File End of Program.\n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}
}


