#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

//namespaces
using namespace std;

//global variable declarations
bool aav_bFullscreen = false;
Display *aav_gpDisplay = NULL;
XVisualInfo *aav_gpXVisualInfo = NULL;

GLXContext aav_gGLXContext;

Colormap aav_gColormap;
Window aav_gWindow;
int aav_giWindowWidth = 800;
int aav_giWindowHeight = 600;

GLfloat aav_anglepyramid = 0.0f;

bool bLight1 = false; 

GLUquadric* aav_gpquadric = NULL;

FILE *aav_gpFile = NULL;

GLfloat aav_lightAmbiantZero[] =  { 0.0f,0.0f,0.0f,1.0f };
GLfloat aav_lightDefuseZero[] =   { 1.0f,0.0f,0.0f,1.0f };
GLfloat aav_lightSpecularZero[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat aav_lightPositionZero[] = { 0.0f,0.0f,0.0f,1.0f };

GLfloat aav_lightAmbiantOne[] =  { 0.0f,0.0f,0.0f,1.0f };
GLfloat aav_lightDefuseOne[] =   { 0.0f,1.0f,0.0f,1.0f };
GLfloat aav_lightSpecularOne[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat aav_lightPositionOne[] = { 0.0f,0.0f,0.0f,1.0f };


GLfloat aav_lightAmbiantTwo[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat aav_lightDefuseTwo[] =  { 0.0f,0.0f,1.0f,1.0f };
GLfloat aav_lightSpecularTwo[] ={ 0.0f,0.0f,1.0f,1.0f };
GLfloat aav_lightPositionTwo[] ={ 0.0f,0.0f,0.0f,1.0f };

GLfloat aav_MaterialAmbiant[] =  { 0.0f,0.0f,0.0f,1.0f };
GLfloat aav_MaterialDefuse[]=    { 1.0f,1.0f,1.0f,1.0f };
GLfloat aav_MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat aav_MaterialShinesness = 50.0f;

GLfloat aav_lightAngle0 = 0.0f;
GLfloat aav_lightAngle1 = 0.0f;
GLfloat aav_lightAngle2 = 0.0f;

//entry-point fucntoin
int main(void)
{
	//function prototypes 
	void initialize(void);
	void resize(int ,int);
	void display(void);

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();

	//varuable declarations 
	int aav_winWidth = aav_giWindowWidth;
	int aav_winHeight = aav_giWindowHeight;
	bool aav_bDone = false;

	// Code
	CreateWindow();
	initialize();

	// Message Loop
	XEvent aav_event;
	KeySym aav_keysym;
	
	aav_gpFile =fopen("logfile.txt","w" );
	if(aav_gpFile == NULL)
	{
		printf("Failed to Open A Log File\n");
		exit(0);
	}
	else 
	{
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
	}	
		
	while(aav_bDone == false)
	{
		while(XPending(aav_gpDisplay))
		{
			XNextEvent(aav_gpDisplay,&aav_event);
			switch(aav_event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					aav_keysym = XkbKeycodeToKeysym(aav_gpDisplay,aav_event.xkey.keycode,0,0);
					switch(aav_keysym)
					{
						case XK_Escape:
							aav_bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(aav_bFullscreen == false)
							{
								ToggleFullscreen();
								aav_bFullscreen = true;	
							}
							else
							{
								ToggleFullscreen();
								aav_bFullscreen = false;
							}
							break;
						case XK_L:
						case XK_l:
						{
							if (bLight1 == false)
							{
								bLight1 = true;
								glEnable(GL_LIGHTING);
							}
							else
							{
								bLight1 = false;
								glDisable(GL_LIGHTING);
							}
						}
						break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(aav_event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					aav_winWidth = aav_event.xconfigure.width;
					aav_winHeight = aav_event.xconfigure.height;
					resize(aav_winWidth, aav_winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					aav_bDone = true;
					break;
				default:
					break;
			}
		}
		display();

	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//fucntion prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes aav_winAttribs; 
	int aav_defaultScreen;
	int aav_defaultDepth;
	int aav_styleMask;
	static int aav_frameBufferAttributes[] = {GLX_DOUBLEBUFFER,True,
							GLX_RGBA,
							GLX_RED_SIZE,8,
							GLX_GREEN_SIZE,8,
							GLX_BLUE_SIZE,8,
							GLX_ALPHA_SIZE,8,
							GLX_DEPTH_SIZE,24,
							None};

	//code
	aav_gpDisplay = XOpenDisplay(NULL);
	if(aav_gpDisplay == NULL)
	{
		printf("ERROR: Unable to Open X Display .\n Extting Now ...\n");
		uninitialize();
		exit(1);
	}
	aav_defaultScreen = XDefaultScreen(aav_gpDisplay);


	aav_gpXVisualInfo= (XVisualInfo*)malloc(sizeof(XVisualInfo));
	if(aav_gpXVisualInfo == NULL)
	{
		printf("Error : Unable To Open X Display.\n Exitting Now...  \n");
		uninitialize();
		exit(1);
	}
	
	aav_gpXVisualInfo = glXChooseVisual(aav_gpDisplay,aav_defaultScreen,aav_frameBufferAttributes);
	if(aav_gpXVisualInfo == NULL)
	{
		printf("ERROR :Unable to Get aav_gpXVisualInfo\n");
		uninitialize();
		exit(1);
	}

	aav_winAttribs.border_pixel = 0;
	aav_winAttribs.background_pixel = 0;
	aav_winAttribs.colormap = XCreateColormap(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			aav_gpXVisualInfo->visual,
			AllocNone);

	aav_gColormap = aav_winAttribs.colormap;
	aav_winAttribs.background_pixel = BlackPixel(aav_gpDisplay, aav_defaultScreen);

	aav_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	aav_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap; 

	aav_gWindow = XCreateWindow(aav_gpDisplay,
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			0,
			0,
			aav_giWindowWidth,
			aav_giWindowHeight,
			0,
			aav_gpXVisualInfo->depth,
			InputOutput,
			aav_gpXVisualInfo->visual,
			aav_styleMask,
			&aav_winAttribs);

	if(!aav_gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now.. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(aav_gpDisplay, aav_gWindow,"Aniruddha Avinash Vanjari :Three Rotation Lights");

	Atom aav_windowManagerDelete = XInternAtom(aav_gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(aav_gpDisplay, aav_gWindow,&aav_windowManagerDelete,1);
	XMapWindow(aav_gpDisplay, aav_gWindow);
}

void ToggleFullscreen(void)
{
	// variable delarations
	Atom aav_wm_state;
	Atom aav_fullscreen;
	XEvent aav_xev = {0};

	//code
	aav_wm_state = XInternAtom(aav_gpDisplay, "_NET_WM_STATE",False);
	memset(&aav_xev,0,sizeof(aav_xev));

	aav_xev.type = ClientMessage;
	aav_xev.xclient.window = aav_gWindow;
	aav_xev.xclient.message_type = aav_wm_state;
	aav_xev.xclient.format = 32;
	aav_xev.xclient.data.l[0] = aav_bFullscreen ? 0 : 1;

	aav_fullscreen = XInternAtom(aav_gpDisplay, "_NET_WM_STATE_FULLSCREEN",False);
	aav_xev.xclient.data.l[1] = aav_fullscreen;

	XSendEvent(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&aav_xev);
}

void initialize(void)
{
	//fuction declaration
	void resize(int, int);

	//code
	aav_gGLXContext = glXCreateContext(aav_gpDisplay, aav_gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(aav_gpDisplay,aav_gWindow,aav_gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glClearColor(0.0f,0.0f,0.0f,1.0f);

	//Light 1
	glLightfv(GL_LIGHT0, GL_AMBIENT, aav_lightAmbiantZero);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, aav_lightDefuseZero);
	glLightfv(GL_LIGHT0, GL_SPECULAR, aav_lightSpecularZero);
	glEnable(GL_LIGHT0);
	

	//light 2
	glLightfv(GL_LIGHT1, GL_AMBIENT, aav_lightAmbiantOne);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, aav_lightDefuseOne);
	glLightfv(GL_LIGHT1, GL_SPECULAR, aav_lightSpecularOne);
	glEnable(GL_LIGHT1);
	

	//light 3
	glLightfv(GL_LIGHT2, GL_AMBIENT, aav_lightAmbiantTwo);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, aav_lightDefuseTwo);
	glLightfv(GL_LIGHT2, GL_SPECULAR, aav_lightSpecularTwo);
	glEnable(GL_LIGHT2);
	

	//Matrial White
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_MaterialAmbiant);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_MaterialDefuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_MaterialSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, aav_MaterialShinesness);

	resize(aav_giWindowWidth,aav_giWindowHeight);
}

void resize(int width , int height )
{
	//code
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);

}

void display(void)
{
	//Function Declaration
	void Pyramid(void);
	void update(void);

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	
	glLoadIdentity();
	glPushMatrix();

	gluLookAt(0.0f, 0.0f, 2.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
	
	glPushMatrix();
	glRotatef(aav_lightAngle0,1.0f,0.0f,0.0f);
	aav_lightPositionZero[1] = aav_lightAngle0;
	glLightfv(GL_LIGHT0, GL_POSITION, aav_lightPositionZero);
	glPopMatrix();

	glPushMatrix();
	glRotatef(aav_lightAngle1,0.0f,1.0f,0.0f);
	aav_lightPositionOne[0] = aav_lightAngle1;
	glLightfv(GL_LIGHT1, GL_POSITION, aav_lightPositionOne);
	glPopMatrix();

	glPushMatrix();
	glRotatef(aav_lightAngle2, 0.0f, 0.0f, 1.0f);
	aav_lightPositionTwo[0] = aav_lightAngle2;
	glLightfv(GL_LIGHT2, GL_POSITION, aav_lightPositionTwo);
	glPopMatrix();

	glTranslatef(0.0f,0.0f,1.0f);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.2f, 50, 50);
	glPopMatrix();
	
	update();

	glXSwapBuffers(aav_gpDisplay,aav_gWindow);

}

void update(void)
{
	aav_lightAngle0 = aav_lightAngle0 + 0.5f;
	if (aav_lightAngle0 >= 360)
	{
		aav_lightAngle0 = 0.0f;
	}
	aav_lightAngle1 = aav_lightAngle1 + 0.5f;
	if (aav_lightAngle1 >= 360)
	{
		aav_lightAngle1 = 0.0f;
	}
	aav_lightAngle2 = aav_lightAngle2 + 0.5f;
	if (aav_lightAngle2 >= 360)
	{
		aav_lightAngle2 = 0.0f;
	}
}

void uninitialize(void)
{
	if(aav_gWindow)
	{
		XDestroyWindow(aav_gpDisplay, aav_gWindow);
	}

	if(aav_gColormap)
	{
		XFreeColormap(aav_gpDisplay, aav_gColormap);
	}

	if(aav_gpXVisualInfo)
	{
		free(aav_gpXVisualInfo);
		aav_gpXVisualInfo = NULL;
	}
	
	if(aav_gpDisplay)
	{
		XCloseDisplay(aav_gpDisplay);
		aav_gpDisplay = NULL;
	}
	
	if(aav_gpFile)
	{
		fprintf(aav_gpFile,"Succesfully Closed File End of Program.\n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}
}


