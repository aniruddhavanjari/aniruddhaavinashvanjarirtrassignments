#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>

//namespaces
using namespace std;

//global variable declarations
bool aav_bFullscreen = false;
Display *aav_gpDisplay = NULL;
XVisualInfo *aav_gpXVisualInfo = NULL;

GLXContext aav_gGLXContext;

Colormap aav_gColormap;
Window aav_gWindow;
int aav_giWindowWidth = 800;
int aav_giWindowHeight = 600;

GLfloat aav_angleCube = 0.0f;

bool bLight1 = false; 
//Rotation axis
GLfloat aav_angleForXRotation = 0.0f;
GLfloat aav_angleForYRotation = 0.0f;
GLfloat aav_angleForZRotation = 0.0f;

//Light
GLfloat aav_lightAmbiant[] =  { 0.0f,0.0f,0.0f,1.0f };
GLfloat aav_lightDefuse[] =   { 1.0f,1.0f,1.0f,1.0f };
GLfloat aav_lightPosition[] = { 0.0f,3.0f,3.0f,0.0f }; // Directional Light

//Light Model
GLfloat  aav_light_Model_ambiant[] = { 0.2f,0.2f,0.2f,1.0f };
GLfloat  aav_light_Model_local_viewer[] = { 0.0f };

//For Sphere
GLUquadric *aav_gpquadric[24];
//Key Press 
GLint aav_keyPressed;

//Use for Translate when fullScreen
GLfloat aav_move = 0.0f;

//fflag for Trnaslate For FullScreen ON/OFF
bool aav_fflag = false;

FILE *aav_gpFile = NULL;

//entry-point fucntoin 
int main(void)
{
	//function prototypes 
	void initialize(void);
	void resize(int ,int);
	void display(void);

	void CreateWindow(void);
	void ToggleFullscreen(void);
	void uninitialize();

	//varuable declarations 
	int aav_winWidth = aav_giWindowWidth;
	int aav_winHeight = aav_giWindowHeight;
	bool aav_bDone = false;

	// Code
	CreateWindow();
	initialize();

	// Message Loop
	XEvent aav_event;
	KeySym aav_keysym;

	aav_gpFile =fopen("logfile.txt","w" );
	if(aav_gpFile == NULL)
	{
		printf("Failed to Open A Log File\n");
		exit(0);
	}
	else 
	{
		fprintf(aav_gpFile,"Succesfully Create Strat of Program\n");
	}	

	while(aav_bDone == false)
	{
		while(XPending(aav_gpDisplay))
		{
			XNextEvent(aav_gpDisplay,&aav_event);
			switch(aav_event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					aav_keysym = XkbKeycodeToKeysym(aav_gpDisplay,aav_event.xkey.keycode,0,0);
					switch(aav_keysym)
					{
						case XK_Escape:
							aav_bDone = true;
							break;
						case XK_F:
						case XK_f:
							if(aav_bFullscreen == false)
							{
								ToggleFullscreen();
								aav_bFullscreen = true;	
							}
							else
							{
								ToggleFullscreen();
								aav_bFullscreen = false;
							}
							if (aav_fflag == false)
							{
								aav_move = 3.5f;
								aav_fflag = true;
							}
							else
							{
								aav_move = 0.0f;
								aav_fflag = false;
							}
							break;
						case XK_L:
						case XK_l:
						{
							if (bLight1 == false)
							{
								bLight1 = true;
								glEnable(GL_LIGHTING);
							}
							else
							{
								bLight1 = false;
								glDisable(GL_LIGHTING);
							}
						}
						break;
						case XK_X:
						case XK_x:
							aav_keyPressed = 1;
							aav_angleForXRotation = 0.0f;
							break;
						case XK_Y:
						case XK_y:
							aav_keyPressed = 2;
							aav_angleForYRotation = 0.0f;
							break;
						case XK_Z:
						case XK_z:
							aav_keyPressed = 3;
							aav_angleForZRotation = 0.0f;
							break;
						default:
							break;
					}
					break;
				case ButtonPress:
					switch(aav_event.xbutton.button)
					{
						case 1:
							break;
						case 2:
							break;
						case 3:
							break;
					}
					break;
				case MotionNotify:
					break;
				case ConfigureNotify:
					aav_winWidth = aav_event.xconfigure.width;
					aav_winHeight = aav_event.xconfigure.height;
					resize(aav_winWidth, aav_winHeight);
					break;
				case Expose:
					break;
				case DestroyNotify:
					break;
				case 33:
					aav_bDone = true;
					break;
				default:
					break;
			}
		}
		display();

	}

	uninitialize();
	return(0);
}

void CreateWindow(void)
{
	//fucntion prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes aav_winAttribs; 
	int aav_defaultScreen;
	int aav_defaultDepth;
	int aav_styleMask;
	static int aav_frameBufferAttributes[] = {GLX_DOUBLEBUFFER,True,
							GLX_RGBA,
							GLX_RED_SIZE,8,
							GLX_GREEN_SIZE,8,
							GLX_BLUE_SIZE,8,
							GLX_ALPHA_SIZE,8,
							GLX_DEPTH_SIZE,24,
							None};

	//code
	aav_gpDisplay = XOpenDisplay(NULL);
	if(aav_gpDisplay == NULL)
	{
		printf("ERROR: Unable to Open X Display .\n Extting Now ...\n");
		uninitialize();
		exit(1);
	}
	aav_defaultScreen = XDefaultScreen(aav_gpDisplay);


	aav_gpXVisualInfo= (XVisualInfo*)malloc(sizeof(XVisualInfo));
	if(aav_gpXVisualInfo == NULL)
	{
		printf("Error : Unable To Open X Display.\n Exitting Now...  \n");
		uninitialize();
		exit(1);
	}
	
	aav_gpXVisualInfo = glXChooseVisual(aav_gpDisplay,aav_defaultScreen,aav_frameBufferAttributes);
	if(aav_gpXVisualInfo == NULL)
	{
		printf("ERROR :Unable to Get aav_gpXVisualInfo\n");
		uninitialize();
		exit(1);
	}

	aav_winAttribs.border_pixel = 0;
	aav_winAttribs.background_pixel = 0;
	aav_winAttribs.colormap = XCreateColormap(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			aav_gpXVisualInfo->visual,
			AllocNone);

	aav_gColormap = aav_winAttribs.colormap;
	aav_winAttribs.background_pixel = BlackPixel(aav_gpDisplay, aav_defaultScreen);

	aav_winAttribs.event_mask = ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask | StructureNotifyMask;

	aav_styleMask = CWBorderPixel | CWBackPixel | CWEventMask | CWColormap; 

	aav_gWindow = XCreateWindow(aav_gpDisplay,
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			0,
			0,
			aav_giWindowWidth,
			aav_giWindowHeight,
			0,
			aav_gpXVisualInfo->depth,
			InputOutput,
			aav_gpXVisualInfo->visual,
			aav_styleMask,
			&aav_winAttribs);

	if(!aav_gWindow)
	{
		printf("ERROR : Failed To Create Main Window. \n Exitting Now.. \n");
		uninitialize();
		exit(1);
	}

	XStoreName(aav_gpDisplay, aav_gWindow,"Aniruddha Avinash Vanjari : 24 Sphere");

	Atom aav_windowManagerDelete = XInternAtom(aav_gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(aav_gpDisplay, aav_gWindow,&aav_windowManagerDelete,1);
	XMapWindow(aav_gpDisplay, aav_gWindow);
}

void ToggleFullscreen(void)
{
	// variable delarations
	Atom aav_wm_state;
	Atom aav_fullscreen;
	XEvent aav_xev = {0};

	//code
	aav_wm_state = XInternAtom(aav_gpDisplay, "_NET_WM_STATE",False);
	memset(&aav_xev,0,sizeof(aav_xev));

	aav_xev.type = ClientMessage;
	aav_xev.xclient.window = aav_gWindow;
	aav_xev.xclient.message_type = aav_wm_state;
	aav_xev.xclient.format = 32;
	aav_xev.xclient.data.l[0] = aav_bFullscreen ? 0 : 1;

	aav_fullscreen = XInternAtom(aav_gpDisplay, "_NET_WM_STATE_FULLSCREEN",False);
	aav_xev.xclient.data.l[1] = aav_fullscreen;

	XSendEvent(aav_gpDisplay, 
			RootWindow(aav_gpDisplay, aav_gpXVisualInfo->screen),
			False,
			StructureNotifyMask,
			&aav_xev);
}

void initialize(void)
{
	//fuction declaration
	void resize(int, int);

	//code
	aav_gGLXContext = glXCreateContext(aav_gpDisplay, aav_gpXVisualInfo,NULL,GL_TRUE);
	glXMakeCurrent(aav_gpDisplay,aav_gWindow,aav_gGLXContext);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glClearColor(0.5f,0.5f,0.5f,1.0f);

	glEnable(GL_AUTO_NORMAL);// For Given Geomatry Give the Normal Automatic
	glEnable(GL_NORMALIZE); //  if Normal are out of range ,MAke the Normal in range of 1 and -1
	
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, aav_light_Model_ambiant);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER,aav_light_Model_local_viewer);
	//Light 1
	glLightfv(GL_LIGHT0, GL_AMBIENT, aav_lightAmbiant);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, aav_lightDefuse);
	glLightfv(GL_LIGHT0, GL_POSITION, aav_lightPosition);
	glEnable(GL_LIGHT0);
	

	for (int i = 0 ; i < 24 ; i++)
	{
		aav_gpquadric[i] = gluNewQuadric();
	}
	
	resize(aav_giWindowWidth,aav_giWindowHeight);
}

void resize(int width , int height )
{
	//code
	if(height == 0)
		height = 1;
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	if (width <= height)
	{
		glOrtho(0.0f, 15.5f, 0.0f, 15.5f * (GLfloat)height / (GLfloat)width, -10.0f, 10.0f);
	}
	else
	{
		glOrtho(0.0f, 15.5f* (GLfloat)width / (GLfloat)height, 0.0f, 15.5f, -10.0f, 10.0f);
	}
}

void display(void)
{
	//Fucntion declaration
	void DrawTwentyFourSphere(void);
	void Update(void);

	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	aav_lightPosition[0] = 0.0f;
	aav_lightPosition[1] = 3.0f;
	aav_lightPosition[2] = 3.0f;
	aav_lightPosition[3] = 0.0f;
	if (aav_keyPressed == 1)
	{
		glRotatef(aav_angleForXRotation, 1.0f,0.0f,0.0f);
		aav_lightPosition[1] = aav_angleForXRotation;
	}
	else if (aav_keyPressed == 2)
	{
		glRotatef(aav_angleForYRotation, 0.0f, 1.0f, 0.0f);
		aav_lightPosition[2] = aav_angleForYRotation;
	}
	else if(aav_keyPressed == 3)
	{
		glRotatef(aav_angleForZRotation, 0.0f, 0.0f, 1.0f);
		aav_lightPosition[0] = aav_angleForZRotation;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, aav_lightPosition);

	DrawTwentyFourSphere();
	
	Update();
	//Setting the move variable 
	if (aav_fflag == false)
		aav_move = 3.5f;
	else
		aav_move = 0.0f;

	glXSwapBuffers(aav_gpDisplay,aav_gWindow);
}

void DrawTwentyFourSphere(void)
{

	//local variable declaration
	GLfloat aav_materialAmbiant[4];
	GLfloat aav_materialDefuse[4];
	GLfloat aav_materialSpecular[4];
	GLfloat aav_materialShineness;

	//code
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//1st Sppere in 1st Column , emerald 
	aav_materialAmbiant[0] = 0.0215f;
	aav_materialAmbiant[1] = 0.1745f;
	aav_materialAmbiant[2] = 0.0215f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,aav_materialAmbiant);

	aav_materialDefuse[0] = 0.07568f;
	aav_materialDefuse[1] = 0.61424f;
	aav_materialDefuse[2] = 0.07568f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.633f;
	aav_materialSpecular[1] = 0.727811f;
	aav_materialSpecular[2] = 0.633f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.6f * 128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,aav_materialShineness);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 14.0f, 0.0f);
	gluSphere(aav_gpquadric[0], 1.0f, 30, 30);

	//2nd Sphere in 1st column, jade 
	aav_materialAmbiant[0] = 0.135f;
	aav_materialAmbiant[1] = 0.2225f;
	aav_materialAmbiant[2] = 0.1575f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.54f;
	aav_materialDefuse[1] = 0.89f;
	aav_materialDefuse[2] = 0.63f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.316228f;
	aav_materialSpecular[1] = 0.316228f;
	aav_materialSpecular[2] = 0.316228f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 11.5f, 0.0f);
	gluSphere(aav_gpquadric[1], 1.0f, 30, 30);
	
	//3nd Sphere in 1st column, obsidian
	aav_materialAmbiant[0] = 0.05375f;
	aav_materialAmbiant[1] = 0.05f;
	aav_materialAmbiant[2] = 0.06625f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.18275f;
	aav_materialDefuse[1] = 0.17f;
	aav_materialDefuse[2] = 0.22525f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.332741f;
	aav_materialSpecular[1] = 0.328634f;
	aav_materialSpecular[2] = 0.346435f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.3f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 9.0f, 0.0f);
	gluSphere(aav_gpquadric[2], 1.0f, 30, 30);

	//4th Sphere in 1st column, pearl
	aav_materialAmbiant[0] = 0.25f;
	aav_materialAmbiant[1] = 0.20725f;
	aav_materialAmbiant[2] = 0.20725f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 1.0f;
	aav_materialDefuse[1] = 0.829f;
	aav_materialDefuse[2] = 0.829f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.296648f;
	aav_materialSpecular[1] = 0.296648f;
	aav_materialSpecular[2] = 0.296648f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.088f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 6.5f, 0.0f);
	gluSphere(aav_gpquadric[3], 1.0f, 30, 30);

	//5th sphere in 1st column ,ruby 
	aav_materialAmbiant[0] = 0.1745f;
	aav_materialAmbiant[1] = 0.01175f;
	aav_materialAmbiant[2] = 0.01175f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.61424f;
	aav_materialDefuse[1] = 0.04136f;
	aav_materialDefuse[2] = 0.04136f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.727811f;
	aav_materialSpecular[1] = 0.626959f;
	aav_materialSpecular[2] = 0.626959f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 4.0f, 0.0f);
	gluSphere(aav_gpquadric[4], 1.0f, 30, 30);

	////6th sphere in 1st column , turquoise
	aav_materialAmbiant[0] = 0.1f;
	aav_materialAmbiant[1] = 0.18725f;
	aav_materialAmbiant[2] = 0.1745f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.396f;
	aav_materialDefuse[1] = 0.74151f;
	aav_materialDefuse[2] = 0.69102f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.297254f;
	aav_materialSpecular[1] = 0.30829f;
	aav_materialSpecular[2] = 0.306678f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 1.5f, 0.0f);
	gluSphere(aav_gpquadric[5], 1.0f, 30, 30);

	//1st sphere on 2nd column, brass////////////////////////////////////////////////////////////	
	aav_materialAmbiant[0] = 0.329412f;
	aav_materialAmbiant[1] = 0.223529f;
	aav_materialAmbiant[2] = 0.027451f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.780398f;
	aav_materialDefuse[1] = 0.568627f;
	aav_materialDefuse[2] = 0.113725f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.992157f;
	aav_materialSpecular[1] = 0.947776f;
	aav_materialSpecular[2] = 0.807843f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.21794872f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 14.0f, 0.0f);
	gluSphere(aav_gpquadric[6], 1.0f, 30, 30);


	//2nd sphere on 2nd column,bronze
	aav_materialAmbiant[0] = 0.2425f;
	aav_materialAmbiant[1] = 0.1275f;
	aav_materialAmbiant[2] = 0.054f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.714f;
	aav_materialDefuse[1] = 0.4284f;
	aav_materialDefuse[2] = 0.18144f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.393548f;
	aav_materialSpecular[1] = 0.271906f;
	aav_materialSpecular[2] = 0.166721f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.2f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 11.5f, 0.0f);
	gluSphere(aav_gpquadric[7], 1.0f, 30, 30);

	//3rd sphere on 2rd column,chrome
	aav_materialAmbiant[0] = 0.25f;
	aav_materialAmbiant[1] = 0.25f;
	aav_materialAmbiant[2] = 0.25f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.4f;
	aav_materialDefuse[1] = 0.4f;
	aav_materialDefuse[2] = 0.4f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.774597f;
	aav_materialSpecular[1] = 0.774597f;
	aav_materialSpecular[2] = 0.774597f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 9.0f, 0.0f);
	gluSphere(aav_gpquadric[8], 1.0f, 30, 30); 

	//4th sphere on 2nd column, 
	aav_materialAmbiant[0] = 0.19125f;
	aav_materialAmbiant[1] = 0.0735f;
	aav_materialAmbiant[2] = 0.0225f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.7038f;
	aav_materialDefuse[1] = 0.27048f;
	aav_materialDefuse[2] = 0.0828f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.256777f;
	aav_materialSpecular[1] = 0.137622f;
	aav_materialSpecular[2] = 0.086014f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 6.5f, 0.0f);
	gluSphere(aav_gpquadric[9], 1.0f, 30, 30); 

	//5th Sphere on 2nd Column, gold
	aav_materialAmbiant[0] = 0.24725f;
	aav_materialAmbiant[1] = 0.1995f;
	aav_materialAmbiant[2] = 0.0745f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.75164f;
	aav_materialDefuse[1] = 0.60648f;
	aav_materialDefuse[2] = 0.22648f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.628281f;
	aav_materialSpecular[1] = 0.555802f;
	aav_materialSpecular[2] = 0.366065f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.4f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 4.0f, 0.0f);
	gluSphere(aav_gpquadric[10], 1.0f, 30, 30);

	//6th Sphere on 2nd Column ,
	aav_materialAmbiant[0] = 0.19225f;
	aav_materialAmbiant[1] = 0.19225f;
	aav_materialAmbiant[2] = 0.19225f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.50754f;
	aav_materialDefuse[1] = 0.50754f;
	aav_materialDefuse[2] = 0.50754f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.508273f;
	aav_materialSpecular[1] = 0.508273f;
	aav_materialSpecular[2] = 0.508273f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.4f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 1.5f, 0.0f);
	gluSphere(aav_gpquadric[11], 1.0f, 30, 30);


	//1st sphere on 3nd column//////////////////////////////////////////////////////////////////////////////////////////
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.01f;
	aav_materialDefuse[1] = 0.01f;
	aav_materialDefuse[2] = 0.01f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.50f;
	aav_materialSpecular[1] = 0.50f;
	aav_materialSpecular[2] = 0.50f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 14.0f, 0.0f);
	gluSphere(aav_gpquadric[12], 1.0f, 30, 30);

	//2nd Sphere on 3nd column,cyan
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.1f;
	aav_materialAmbiant[2] = 0.06f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.0f;
	aav_materialDefuse[1] = 0.50980392f;
	aav_materialDefuse[2] = 0.50980392f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.50196078f;
	aav_materialSpecular[1] = 0.50196078f;
	aav_materialSpecular[2] = 0.50196078f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 11.5f, 0.0f);
	gluSphere(aav_gpquadric[13], 1.0f, 30, 30);

	
	//3rd Sphere on 3th column 
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.1f;
	aav_materialDefuse[1] = 0.35f;
	aav_materialDefuse[2] = 0.1f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.45f;
	aav_materialSpecular[1] = 0.55f;
	aav_materialSpecular[2] = 0.45f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 9.0f, 0.0f);
	gluSphere(aav_gpquadric[14], 1.0f, 30, 30);

	//4th Sphere on 3rd ,red
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.5f;
	aav_materialDefuse[1] = 0.0f;
	aav_materialDefuse[2] = 0.0f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.7f;
	aav_materialSpecular[1] = 0.6f;
	aav_materialSpecular[2] = 0.6f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 6.5f, 0.0f);
	gluSphere(aav_gpquadric[15], 1.0f, 30, 30);

	//5th Sphere on 3rd , white
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.55f;
	aav_materialDefuse[1] = 0.55f;
	aav_materialDefuse[2] = 0.55f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.70f;
	aav_materialSpecular[1] = 0.70f;
	aav_materialSpecular[2] = 0.70f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 4.0f, 0.0f);
	gluSphere(aav_gpquadric[16], 1.0f, 30, 30);

	//6th Sphere on 3rd ,yello plastic 
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.5f;
	aav_materialDefuse[1] = 0.5f;
	aav_materialDefuse[2] = 0.0f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.60f;
	aav_materialSpecular[1] = 0.60f;
	aav_materialSpecular[2] = 0.50f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 1.5f, 0.0f);
	gluSphere(aav_gpquadric[17], 1.0f, 30, 30);

	//Translste Setting for last Column 
	if (aav_fflag == false)
		aav_move = aav_move + 3.5f;
	else
		aav_move = 0.0f;
	//1st sphere on 4th column,black////////////////////////////////////////////////////////////////////////////////////////////
	aav_materialAmbiant[0] = 0.02f;
	aav_materialAmbiant[1] = 0.02f;
	aav_materialAmbiant[2] = 0.02f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.01f;
	aav_materialDefuse[1] = 0.01f;
	aav_materialDefuse[2] = 0.01f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.4f;
	aav_materialSpecular[1] = 0.4f;
	aav_materialSpecular[2] = 0.4f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 14.0f, 0.0f);
	gluSphere(aav_gpquadric[18], 1.0f, 30, 30);


	//2nd Shpere on 4th column,cyan
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.05f;
	aav_materialAmbiant[2] = 0.05f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.4f;
	aav_materialDefuse[1] = 0.5f;
	aav_materialDefuse[2] = 0.5f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.04f;
	aav_materialSpecular[1] = 0.7f;
	aav_materialSpecular[2] = 0.7f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 11.5f, 0.0f);
	gluSphere(aav_gpquadric[19], 1.0f, 30, 30);

	//3rd Sphere on 4th column , green
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.05f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.4f;
	aav_materialDefuse[1] = 0.5f;
	aav_materialDefuse[2] = 0.4f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.04f;
	aav_materialSpecular[1] = 0.7f;
	aav_materialSpecular[2] = 0.04f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 9.0f, 0.0f);
	gluSphere(aav_gpquadric[20], 1.0f, 30, 30);

	//4th Sphere on 4th column, red
	aav_materialAmbiant[0] = 0.05f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.5f;
	aav_materialDefuse[1] = 0.4f;
	aav_materialDefuse[2] = 0.4f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.7f;
	aav_materialSpecular[1] = 0.04f;
	aav_materialSpecular[2] = 0.04f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 6.5f, 0.0f);
	gluSphere(aav_gpquadric[21], 1.0f, 30, 30);

	//5th Sphere on 4th column, white
	aav_materialAmbiant[0] = 0.05f;
	aav_materialAmbiant[1] = 0.05f;
	aav_materialAmbiant[2] = 0.05f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.5f;
	aav_materialDefuse[1] = 0.5f;
	aav_materialDefuse[2] = 0.5f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.7f;
	aav_materialSpecular[1] = 0.7f;
	aav_materialSpecular[2] = 0.7f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 4.0f, 0.0f);
	gluSphere(aav_gpquadric[22], 1.0f, 30, 30);

	//6th Sphere on 4th column , rubber
	aav_materialAmbiant[0] = 0.05f;
	aav_materialAmbiant[1] = 0.05f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.5f;
	aav_materialDefuse[1] = 0.5f;
	aav_materialDefuse[2] = 0.4f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.7f;
	aav_materialSpecular[1] = 0.7f;
	aav_materialSpecular[2] = 0.04f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 1.5f, 0.0f);
	gluSphere(aav_gpquadric[23], 1.0f, 30, 30);

}


void Update(void)
{
	if (aav_keyPressed == 1)
	{
		aav_angleForXRotation = aav_angleForXRotation + 1.0f;
	}
	else if (aav_keyPressed == 2)
	{
		aav_angleForYRotation = aav_angleForYRotation + 1.0f;
	}
	else if (aav_keyPressed == 3)
	{
		aav_angleForZRotation = aav_angleForZRotation + 1.0f;
	}
}


void uninitialize(void)
{
	if(aav_gWindow)
	{
		XDestroyWindow(aav_gpDisplay, aav_gWindow);
	}

	if(aav_gColormap)
	{
		XFreeColormap(aav_gpDisplay, aav_gColormap);
	}

	if(aav_gpXVisualInfo)
	{
		free(aav_gpXVisualInfo);
		aav_gpXVisualInfo = NULL;
	}
	
	if(aav_gpDisplay)
	{
		XCloseDisplay(aav_gpDisplay);
		aav_gpDisplay = NULL;
	}

	for (int i = 0; i < 24; i++)
	{
		gluDeleteQuadric(aav_gpquadric[i]);
	}

	if(aav_gpFile)
	{
		fprintf(aav_gpFile,"Succesfully Closed File End of Program.\n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

}


