//
//  MyView.m
//  Window
//
//  Created by OM-SAI on 03/07/21.
//  Copyright © 2021 OM-SAI. All rights reserved.
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
#import "sphere.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};

bool aav_bLight = false;
bool aav_bPerFragment = false;
bool aav_bPerVertex = true;
bool aav_bAnimation = false;



GLfloat aav_lightAngle1 = 0.0f;
GLfloat aav_lightAngle2 = 0.0f;
GLfloat aav_lightAngle3 = 0.0f;

GLuint tap = 0;
@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimationg;
    
    GLuint gVertexShaderObject_pv;
    GLuint gFragmentShaderObject_pv;
    GLuint gShaderProgramObject_pv;
    
    GLuint gVertexShaderObject_pf;
    GLuint gFragmentShaderObject_pf;
    GLuint gShaderProgramObject_pf;
    
    GLuint aav_mvp_MatrixUniform;
    
    mat4 aav_PerspectiveProjectionMatrix;
    
    //Sphere Variable
    GLfloat aav_sphere_vertices[1146];
    GLfloat aav_sphere_normals[1146];
    GLfloat aav_sphere_texture[764];
    short aav_sphere_elements[2280];
    GLuint aav_numSphereVertices;
    GLuint aav_numSphereElements;
    
    GLuint aav_gVao_sphere;
    GLuint aav_Vbo_sphere_position;
    GLuint aav_Vbo_sphere_normals;
    GLuint aav_Vbo_sphere_elements;
    
    struct Light
    {
        GLfloat aav_lightAmbiant[4];
        GLfloat aav_lightDiffuse[4];
        GLfloat aav_lightSpecular[4];
        GLfloat aav_lightPosition[4];
    };
    
    struct Light light[3];
    
    struct Material
    {
        GLfloat aav_materialAmbiant[4];
        GLfloat aav_materialDiffuse[4];
        GLfloat aav_materialSpecular[4];
        GLfloat aav_materialShininess;
    };
    
    struct Material material;
    
    GLuint aav_viewMatrixUniform_pf;
    GLuint aav_modelMatrixUniform_pf;
    GLuint aav_ProjectionMatrixUniform_pf;
    
    GLuint aav_laUniform_pf[3];
    GLuint aav_ldUniform_pf[3];
    GLuint aav_lsUniform_pf[3];
    GLuint aav_lightPositionUniform_pf[3];
    
    GLuint aav_kaUniform_pf;
    GLuint aav_kdUniform_pf;
    GLuint aav_ksUniform_pf;
    
    GLuint aav_kShininessUniform_pf;
    
    
    //VertexShader Uniforms
    
    GLuint aav_viewMatrixUniform_pv;
    GLuint aav_modelMatrixUniform_pv;
    GLuint aav_ProjectionMatrixUniform_pv;
    
    GLuint aav_laUniform_pv[3];
    GLuint aav_ldUniform_pv[3];
    GLuint aav_lsUniform_pv[3];
    GLuint aav_lightPositionUniform_pv[3];
    
    GLuint aav_kaUniform_pv;
    GLuint aav_kdUniform_pv;
    GLuint aav_ksUniform_pv;
    
    GLuint aav_kShininessUniform_pv;
    
    GLuint aav_lKeyPressedUniform;
    GLuint aav_lKeyPressedUniform_pv;
    
    
}

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        //OpenGL code
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil]];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context Creation Failed.\n");
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        //render to texture
        glGenFramebuffers(1,&defaultFramebuffer );
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        //storage dena
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        //vatanya chi sange
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        //12 step : buffer width , height set
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //16 depth buffer sathi
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("FrameBuffer is Not Complete.\n");
            [self uninitialize];
            return(nil);
        }
        
        printf("%s\n",glGetString(GL_RENDERER));
        printf("%s\n",glGetString(GL_VERSION));
        printf("%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        animationFrameInterval = 60; //iOS 8.2 take bufeault
        isAnimationg = NO;
        
        
        
        //vertex shader source code start from here
        //**VERTEX SHADER***
        gVertexShaderObject_pv = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* aav_vertexShaserSourceCode_pv =
        "#version 300 es" \
        "\n" \
        "precision mediump int;"\
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_lKeyPressed_pv;" \
        "uniform vec3 u_la_pv[3];" \
        "uniform vec3 u_ld_pv[3];" \
        "uniform vec3 u_ls_pv[3];" \
        "uniform vec4 u_lightPosistion_pv[3];" \
        "uniform vec3 u_ka_pv;" \
        "uniform vec3 u_kd_pv;" \
        "uniform vec3 u_ks_pv;" \
        "uniform float u_kShineness_pv;"\
        "vec3 lightDirection[3];"\
        "vec3 reflection_vector[3];"\
        "vec3 ambiant[3];"\
        "vec3 diffuse[3];"\
        "vec3 specular[3];"\
        "vec3 tempCalculation = vec3(0.0f,0.0f,0.0f);"\
        "out vec3 fong_ads_light_pv;" \
        "void main(void)" \
        "{" \
        "    if(u_lKeyPressed_pv ==1)" \
        "    {" \
        "        vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
        "        vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
        "        vec3 view_vector = normalize(-eyeCordinate.xyz);" \
        "        for(int i = 0 ; i < 3; i++)" \
        "        {" \
        "            lightDirection[i] = normalize(vec3(u_lightPosistion_pv[i] - eyeCordinate));" \
        "            reflection_vector[i] = reflect(-lightDirection[i],tranformed_normal);" \
        "            ambiant[i] = u_la_pv[i] * u_ka_pv;" \
        "            diffuse[i] = u_ld_pv[i] * u_kd_pv * max(dot(lightDirection[i],tranformed_normal),0.0f);" \
        "            specular[i] = u_ls_pv[i] * u_ks_pv * pow(max(dot(reflection_vector[i],view_vector),0.0f),u_kShineness_pv);" \
        "            tempCalculation = tempCalculation + ambiant[i] + diffuse[i] + specular[i];" \
        "            fong_ads_light_pv = tempCalculation;"\
        "        }" \
        "    }" \
        "    else" \
        "    {" \
        "        fong_ads_light_pv = vec3(1.0f,1.0f,1.0f);" \
        "    }" \
        "   gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        glShaderSource(gVertexShaderObject_pv, 1, (const GLchar**)&aav_vertexShaserSourceCode_pv, NULL);
        
        //compile shader , Error checking of Compilation
        glCompileShader(gVertexShaderObject_pv);
        
        GLint aav_infoLogLength = 0;
        GLint aav_shaderCompiledStatus = 0;
        char* szBuffer = NULL;
        
        glGetShaderiv(gVertexShaderObject_pv, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
        
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(gVertexShaderObject_pv, aav_infoLogLength,
                                       &aav_written, szBuffer);
                    printf( "Vertex Shader PerVertex Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                    
                }
            }
        }
        
        //PerVertex
        //**FRAGMENT SHADER**
        gFragmentShaderObject_pv = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentShaderSourceCode_pv =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "in vec3 fong_ads_light_pv;" \
        "out vec4 FragColor_pv;" \
        "void main(void)" \
        "{" \
        "    FragColor_pv = vec4(fong_ads_light_pv,1.0f);" \
        "}";
        glShaderSource(gFragmentShaderObject_pv, 1,
                       (const GLchar**)&fragmentShaderSourceCode_pv,NULL);
        //compile shader
        glCompileShader(gFragmentShaderObject_pv);
        
        szBuffer = NULL;
        aav_infoLogLength = 0;
        aav_shaderCompiledStatus = 0;
        
        glGetShaderiv(gFragmentShaderObject_pv, GL_COMPILE_STATUS,
                      &aav_shaderCompiledStatus);
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(gFragmentShaderObject_pv, aav_infoLogLength,
                                       &aav_written, szBuffer);
                    printf( "Fragment Shader PerVertex Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                    
                }
            }
        }
        
        /****************************************************************/
        //PerFragment
        //**VERTEX SHADER***
        gVertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* aav_vertexShaserSourceCode_pf =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "precision mediump int;"\
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_lKeyPressed;" \
        "uniform vec4 u_lightPosistion_pf[3];" \
        "out vec3 tranformed_normal_pf;"\
        "out vec3 lightDirection_pf[3];" \
        "out vec3 view_vector_pf;" \
        "void main(void)" \
        "{" \
        "    if(u_lKeyPressed == 1)" \
        "    {"\
        "        vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
        "        tranformed_normal_pf = (mat3(u_view_matrix* u_model_matrix) * vNormal);" \
        "        view_vector_pf = (-eyeCordinate.xyz);" \
        "        for(int i = 0 ; i < 3 ; i++)"\
        "        {"\
        "            lightDirection_pf[i] = (vec3(u_lightPosistion_pf[i] - eyeCordinate));" \
        "        }"\
        "    }" \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        glShaderSource(gVertexShaderObject_pf, 1, (const GLchar**)&aav_vertexShaserSourceCode_pf, NULL);
        
        //compile shader , Error checking of Compilation
        glCompileShader(gVertexShaderObject_pf);
        
        aav_infoLogLength = 0;
        aav_shaderCompiledStatus = 0;
        szBuffer = NULL;
        
        glGetShaderiv(gVertexShaderObject_pf, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
        
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(gVertexShaderObject_pf, aav_infoLogLength,
                                       &aav_written, szBuffer);
                    printf("Vertex Shader PerFramgment Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                }
            }
        }
        
        
        //PerFragment
        //**FRAGMENT SHADER**
        gFragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* fragmentShaderSourceCode_pf =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "precision mediump int;" \
        "in vec3 tranformed_normal_pf;"\
        "in vec3 lightDirection_pf[3];" \
        "in vec3 view_vector_pf;" \
        "uniform int u_lKeyPressed;" \
        "uniform vec3 u_la_pf[3];" \
        "uniform vec3 u_ld_pf[3];" \
        "uniform vec3 u_ls_pf[3];" \
        "uniform vec3 u_ka_pf;" \
        "uniform vec3 u_kd_pf;" \
        "uniform vec3 u_ks_pf;" \
        "uniform float u_kShineness_pf;"\
        
        "vec3 normalize_tranformed_normal;"\
        "vec3 normalize_lightDirection[3];"\
        "vec3 normalize_view_vector;"\
        "vec3 reflection_vector[3];"\
        "vec3 ambiant[3];"\
        "vec3 diffuse[3];"\
        "vec3 specular[3];"\
        "vec3 tempCalculation = vec3(0.0f,0.0f,0.0f);"\
        "out vec4 FragColor_pf;" \
        "vec3 fong_ads_light_pf;" \
        "void main(void)" \
        "{" \
        "    if(u_lKeyPressed == 1)" \
        "    {"\
        "        normalize_tranformed_normal = normalize(tranformed_normal_pf);" \
        "        normalize_view_vector = normalize(view_vector_pf);" \
        "        for(int i = 0; i < 3 ; i++)"\
        "        {" \
        "            normalize_lightDirection[i] = normalize(lightDirection_pf[i]);" \
        "            reflection_vector[i] = reflect(-normalize_lightDirection[i],normalize_tranformed_normal);" \
        "            ambiant[i] = u_la_pf[i] * u_ka_pf;" \
        "            diffuse[i] = u_ld_pf[i] * u_kd_pf * max(dot(normalize_lightDirection[i],normalize_tranformed_normal),0.0f);" \
        "            specular[i] = u_ls_pf[i] * u_ks_pf * pow(max(dot(reflection_vector[i],normalize_view_vector),0.0f),u_kShineness_pf);" \
        "   tempCalculation = tempCalculation + ambiant[i] + diffuse[i] + specular[i];"\
        "   fong_ads_light_pf = tempCalculation;" \
        "        }" \
        "    }" \
        "    else"\
        "    {"\
        "        fong_ads_light_pf = vec3(1.0f,1.0f,1.0f);"\
        "    }"\
        "    FragColor_pf = vec4(fong_ads_light_pf,1.0f);" \
        "}";
        glShaderSource(gFragmentShaderObject_pf, 1,
                       (const GLchar**)&fragmentShaderSourceCode_pf,NULL);
        //compile shader
        glCompileShader(gFragmentShaderObject_pf);
        
        szBuffer = NULL;
        aav_infoLogLength = 0;
        aav_shaderCompiledStatus = 0;
        
        glGetShaderiv(gFragmentShaderObject_pf, GL_COMPILE_STATUS,
                      &aav_shaderCompiledStatus);
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(gFragmentShaderObject_pf, aav_infoLogLength,
                                       &aav_written, szBuffer);
                    printf( "Fragment Shader of PerFragment Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                }
            }
        }
        
        /****************************************************************/
        
        //**SHADER PROGRAM For PerVertex**
        //Create
        gShaderProgramObject_pv = glCreateProgram();
        
        glAttachShader(gShaderProgramObject_pv, gVertexShaderObject_pv);
        glAttachShader(gShaderProgramObject_pv, gFragmentShaderObject_pv);
        
        glBindAttribLocation(gShaderProgramObject_pv, AAV_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(gShaderProgramObject_pv, AAV_ATTRIBUTE_NORMAL, "vNormal");
        
        glLinkProgram(gShaderProgramObject_pv);
        
        aav_infoLogLength = 0;
        GLint aav_shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        
        glGetProgramiv(gShaderProgramObject_pv, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
        if (aav_shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_infoLogLength > 0)
                {
                    GLsizei aav_aav_written;
                    glGetProgramInfoLog(gShaderProgramObject_pv, aav_infoLogLength,
                                        &aav_aav_written, szBuffer);
                    printf("Shader Program PerVertex Link Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                    
                }
            }
        }
        
        
        /*************************Shader Program For PerFrament***********************/
        //Create
        gShaderProgramObject_pf = glCreateProgram();
        
        glAttachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);
        glAttachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);
        
        glBindAttribLocation(gShaderProgramObject_pf, AAV_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(gShaderProgramObject_pf, AAV_ATTRIBUTE_NORMAL, "vNormal");
        
        glLinkProgram(gShaderProgramObject_pf);
        
        aav_infoLogLength = 0;
        aav_shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        
        glGetProgramiv(gShaderProgramObject_pf, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
        if (aav_shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_infoLogLength > 0)
                {
                    GLsizei aav_aav_written;
                    glGetProgramInfoLog(gShaderProgramObject_pf, aav_infoLogLength,
                                        &aav_aav_written, szBuffer);
                    printf("Shader Program PerFragment Link Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                    
                }
            }
        }
        
        /*****************************************************************************/
        //Post Linking Information
        //PerVertex
        aav_modelMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_model_matrix");
        aav_viewMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_view_matrix");
        aav_ProjectionMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_projection_matrix");
        
        //Red Light
        aav_laUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_la_pv[0]");
        aav_ldUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_pv[0]");
        aav_lsUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_pv[0]");
        aav_lightPositionUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_lightPosistion_pv[0]");
        
        //Green Light
        aav_laUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_la_pv[1]");
        aav_ldUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_pv[1]");
        aav_lsUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_pv[1]");
        aav_lightPositionUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_lightPosistion_pv[1]");
        
        //Blue Light
        aav_laUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_la_pv[2]");
        aav_ldUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_pv[2]");
        aav_lsUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_pv[2]");
        aav_lightPositionUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_lightPosistion_pv[2]");
        
        aav_kaUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_ka_pv");
        aav_kdUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_kd_pv");
        aav_ksUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_ks_pv");
        
        aav_kShininessUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_kShineness_pv");
        
        aav_lKeyPressedUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_lKeyPressed_pv");
        
        /*************************************************************************************/
        //PerFragment
        aav_modelMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_model_matrix");
        aav_viewMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_view_matrix");
        aav_ProjectionMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_projection_matrix");
        
        //Red Light
        aav_laUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_la_pf[0]");
        aav_ldUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_ld_pf[0]");
        aav_lsUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_ls_pf[0]");
        aav_lightPositionUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_lightPosistion_pf[0]");
        
        //Green Light
        aav_laUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_la_pf[1]");
        aav_ldUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_ld_pf[1]");
        aav_lsUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_ls_pf[1]");
        aav_lightPositionUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_lightPosistion_pf[1]");
        
        //Blue light
        aav_laUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_la_pf[2]");
        aav_ldUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_ld_pf[2]");
        aav_lsUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_ls_pf[2]");
        aav_lightPositionUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_lightPosistion_pf[2]");
        
        aav_kaUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ka_pf");
        aav_kdUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_kd_pf");
        aav_ksUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ks_pf");
        
        aav_kShininessUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_kShineness_pf");
        
        aav_lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_lKeyPressed");
        
        /*****************************************************************************************/
        
        //vertices array declation
        Sphere *sphere = [[Sphere alloc]init];
        
        [sphere getSphereVertexData:aav_sphere_vertices :aav_sphere_normals :aav_sphere_texture :aav_sphere_elements];
        
        aav_numSphereVertices = [sphere getNumberOfSphereVertices];
        aav_numSphereElements = [sphere getNumberOfSphereElements];
        
        glGenVertexArrays(1, &aav_gVao_sphere);
        glBindVertexArray(aav_gVao_sphere);
        
        //Record Sphere
        glGenBuffers(1, &aav_Vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_vertices), aav_sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //normals
        glGenBuffers(1, &aav_Vbo_sphere_normals);
        glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_normals);
        glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_normals), aav_sphere_normals, GL_STATIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE,0,NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //elements
        glGenBuffers(1, &aav_Vbo_sphere_elements);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements); // Elemtnes Drawing is Also Called As Index Drawing, Elements Drawing
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(aav_sphere_elements), aav_sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        
        //Record Off / Pause
        glBindVertexArray(0);
        
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
    
        //
        glClearColor(0.0f,0.0f,0.0f,1.0f);
        
        aav_PerspectiveProjectionMatrix = mat4::identity();
        
        //material
        material.aav_materialAmbiant[0] = 0.0f;
        material.aav_materialAmbiant[1] = 0.0f;
        material.aav_materialAmbiant[2] = 0.0f;
        material.aav_materialAmbiant[3] = 1.0f;
        
        material.aav_materialDiffuse[0] = 1.0f;
        material.aav_materialDiffuse[1] = 1.0f;
        material.aav_materialDiffuse[2] = 1.0f;
        material.aav_materialDiffuse[3] = 1.0f;
        
        material.aav_materialSpecular[0] = 1.0f;
        material.aav_materialSpecular[1] = 1.0f;
        material.aav_materialSpecular[2] = 1.0f;
        material.aav_materialSpecular[3] = 1.0f;
        
        material.aav_materialShininess = 128.0f;
        
        
        //Light
        //Red
        light[0].aav_lightAmbiant[0] = 0.0f;
        light[0].aav_lightAmbiant[1] = 0.0f;
        light[0].aav_lightAmbiant[2] = 0.0f;
        light[0].aav_lightAmbiant[3] = 1.0f;
        
        light[0].aav_lightDiffuse[0] = 1.0f;
        light[0].aav_lightDiffuse[1] = 0.0f;
        light[0].aav_lightDiffuse[2] = 0.0f;
        light[0].aav_lightDiffuse[3] = 1.0f;
        
        light[0].aav_lightSpecular[0] = 1.0f;
        light[0].aav_lightSpecular[1] = 0.0f;
        light[0].aav_lightSpecular[2] = 0.0f;
        light[0].aav_lightSpecular[3] = 1.0f;
        
        light[0].aav_lightPosition[0] = 0.0f;
        light[0].aav_lightPosition[1] = 0.0f;
        light[0].aav_lightPosition[2] = 0.0f;
        light[0].aav_lightPosition[3] = 1.0f;
        
        //Green
        light[1].aav_lightAmbiant[0] = 0.0f;
        light[1].aav_lightAmbiant[1] = 0.0f;
        light[1].aav_lightAmbiant[2] = 0.0f;
        light[1].aav_lightAmbiant[3] = 1.0f;
        
        light[1].aav_lightDiffuse[0] = 0.0f;
        light[1].aav_lightDiffuse[1] = 1.0f;
        light[1].aav_lightDiffuse[2] = 0.0f;
        light[1].aav_lightDiffuse[3] = 1.0f;
        
        light[1].aav_lightSpecular[0] = 0.0f;
        light[1].aav_lightSpecular[1] = 1.0f;
        light[1].aav_lightSpecular[2] = 0.0f;
        light[1].aav_lightSpecular[3] = 1.0f;
        
        light[1].aav_lightPosition[0] = 0.0f;
        light[1].aav_lightPosition[1] = 0.0f;
        light[1].aav_lightPosition[2] = 0.0f;
        light[1].aav_lightPosition[3] = 1.0f;
        
        //Blue
        light[2].aav_lightAmbiant[0] = 0.0f;
        light[2].aav_lightAmbiant[1] = 0.0f;
        light[2].aav_lightAmbiant[2] = 0.0f;
        light[2].aav_lightAmbiant[3] = 1.0f;
        
        light[2].aav_lightDiffuse[0] = 0.0f;
        light[2].aav_lightDiffuse[1] = 0.0f;
        light[2].aav_lightDiffuse[2] = 1.0f;
        light[2].aav_lightDiffuse[3] = 1.0f;
        
        light[2].aav_lightSpecular[0] = 0.0f;
        light[2].aav_lightSpecular[1] = 0.0f;
        light[2].aav_lightSpecular[2] = 1.0f;
        light[2].aav_lightSpecular[3] = 1.0f;
        
        light[2].aav_lightPosition[0] = 0.0f;
        light[2].aav_lightPosition[1] = 0.0f;
        light[2].aav_lightPosition[2] = 0.0f;
        light[2].aav_lightPosition[3] = 1.0f;

        
        // Gestures
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action: @selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; //ake double tap la to single tap manat nahe
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecongnizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self  action:@selector(onLongPress:)];
        [longPressGestureRecongnizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecongnizer];
    }
    
    return(self);
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}
/*
-(void)drawRect:(CGRect)rect
{
    // Drawing code
   
}
*/

-(void)layoutSubviews
{
    //code
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    //storage dena
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
    GLint width;
    GLint height;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is Not Complete.\n");
        [self uninitialize];
    }
    
    if(height < 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
    
    
    [self drawView];
}

-(void)drawView
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer); //patak program
    //glUseProgram code start from here
    
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    mat4 aav_modelMateix;
    mat4 aav_viewMatrix;
    mat4 aav_translateMatrix;
    
    light[0].aav_lightPosition[0] = 0.0f;
    light[0].aav_lightPosition[1] = 5 * sin(aav_lightAngle1);
    light[0].aav_lightPosition[2] = 5 * cos(aav_lightAngle1);
    light[0].aav_lightPosition[3] = 1.0f;
    
    light[1].aav_lightPosition[0] = 5 * sin(aav_lightAngle1);
    light[1].aav_lightPosition[1] = 0.0f;
    light[1].aav_lightPosition[2] = 5 * cos(aav_lightAngle1);
    light[1].aav_lightPosition[3] = 1.0f;
    
    light[2].aav_lightPosition[0] = 5 * sin(aav_lightAngle1);
    light[2].aav_lightPosition[1] = 5 * cos(aav_lightAngle1);
    light[2].aav_lightPosition[2] = 0.0f;
    light[2].aav_lightPosition[3] = 1.0f;
    
    if (aav_bPerVertex == true)
    {
        glUseProgram(gShaderProgramObject_pv);
        
        if (aav_bLight == true)
        {
            glUniform1i(aav_lKeyPressedUniform_pv, 1);
            //Light
            //Red
            glUniform3fv(aav_laUniform_pv[0], 1, (GLfloat*)light[0].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pv[0], 1, (GLfloat*)light[0].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pv[0], 1, (GLfloat*)light[0].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pv[0], 1, (GLfloat*)light[0].aav_lightPosition);
            //Green
            glUniform3fv(aav_laUniform_pv[1], 1, (GLfloat*)light[1].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pv[1], 1, (GLfloat*)light[1].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pv[1], 1, (GLfloat*)light[1].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pv[1], 1, (GLfloat*)light[1].aav_lightPosition);
            //Blue
            glUniform3fv(aav_laUniform_pv[2], 1, (GLfloat*)light[2].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pv[2], 1, (GLfloat*)light[2].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pv[2], 1, (GLfloat*)light[2].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pv[2], 1, (GLfloat*)light[2].aav_lightPosition);
            
            //material
            glUniform3fv(aav_kaUniform_pv, 1, (GLfloat*)material.aav_materialAmbiant);
            glUniform3fv(aav_kdUniform_pv, 1, (GLfloat*)material.aav_materialDiffuse);
            glUniform3fv(aav_ksUniform_pv, 1, (GLfloat*)material.aav_materialSpecular);
            glUniform1f(aav_kShininessUniform_pv, material.aav_materialShininess);
        }
        else
        {
            glUniform1i(aav_lKeyPressedUniform_pv, 0);
        }
        
        //OpenGL Drawing
        aav_modelMateix = mat4::identity();
        
        aav_viewMatrix = mat4::identity();
        
        aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        
        aav_modelMateix = aav_translateMatrix;
        
        glUniformMatrix4fv(aav_modelMatrixUniform_pv, 1, GL_FALSE, aav_modelMateix);
        glUniformMatrix4fv(aav_viewMatrixUniform_pv, 1, GL_FALSE, aav_viewMatrix);
        glUniformMatrix4fv(aav_ProjectionMatrixUniform_pv, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
        
        glBindVertexArray(aav_gVao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
        
        glUseProgram(0);
    }
    
    if(aav_bPerFragment == true)
    {
        glUseProgram(gShaderProgramObject_pf);
        
        if (aav_bLight == true)
        {
            glUniform1i(aav_lKeyPressedUniform, 1);
            //Light
            //Red
            glUniform3fv(aav_laUniform_pf[0], 1, (GLfloat*)light[0].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pf[0], 1, (GLfloat*)light[0].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pf[0], 1, (GLfloat*)light[0].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pf[0], 1, (GLfloat*)light[0].aav_lightPosition);
            //Green
            glUniform3fv(aav_laUniform_pf[1], 1, (GLfloat*)light[1].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pf[1], 1, (GLfloat*)light[1].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pf[1], 1, (GLfloat*)light[1].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pf[1], 1, (GLfloat*)light[1].aav_lightPosition);
            //Blue
            glUniform3fv(aav_laUniform_pf[2], 1, (GLfloat*)light[2].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pf[2], 1, (GLfloat*)light[2].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pf[2], 1, (GLfloat*)light[2].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pf[2], 1, (GLfloat*)light[2].aav_lightPosition);
            
            //material
            glUniform3fv(aav_kaUniform_pf, 1, (GLfloat*)material.aav_materialAmbiant);
            glUniform3fv(aav_kdUniform_pf, 1, (GLfloat*)material.aav_materialDiffuse);
            glUniform3fv(aav_ksUniform_pf, 1, (GLfloat*)material.aav_materialSpecular);
            glUniform1f(aav_kShininessUniform_pf, material.aav_materialShininess);
        }
        else
        {
            glUniform1i(aav_lKeyPressedUniform, 0);
        }
        
        //OpenGL Drawing
        aav_modelMateix = mat4::identity();
        
        aav_viewMatrix = mat4::identity();
        
        aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        
        aav_modelMateix = aav_translateMatrix;
        
        glUniformMatrix4fv(aav_modelMatrixUniform_pf, 1, GL_FALSE, aav_modelMateix);
        glUniformMatrix4fv(aav_viewMatrixUniform_pf, 1, GL_FALSE, aav_viewMatrix);
        glUniformMatrix4fv(aav_ProjectionMatrixUniform_pf, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
        
        glBindVertexArray(aav_gVao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
        
        glUseProgram(0);
    }
    
   
        aav_lightAngle1 = aav_lightAngle1 + 0.01f;
        if (aav_lightAngle1 > 360)
        {
            aav_lightAngle1 = 0.0f;
        }
        
        aav_lightAngle2 = aav_lightAngle2 + 0.01f;
        if (aav_lightAngle2 > 360)
        {
            aav_lightAngle2 = 0.0f;
        }
        
        aav_lightAngle3 = aav_lightAngle3  + 0.01f;
        if (aav_lightAngle3 > 360)
        {
            aav_lightAngle3 = 0.0f;
        }
   
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}


-(void)startAnimation
{
    //code
    if(isAnimationg == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimationg = YES;
    }
    
}

-(void)stopAnimation
{
    //code
    if(isAnimationg == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimationg = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    if (aav_bLight == false)
    {
        aav_bLight = true;
    }
    else
    {
        aav_bLight = false;
    }
   
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    //code
    tap = tap + 1;
    if(tap == 1)
    {
        if (aav_bPerFragment == false)
        {
            aav_bPerVertex = false;
            aav_bPerFragment = true;
        }
    }
    else if(tap == 2)
    {
        if (aav_bPerVertex == false)
        {
            aav_bPerFragment = false;
            aav_bPerVertex = true;
        }
    }
    
    if(tap >=3)
    {
        tap = 0;
    }
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self uninitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
}

-(void)uninitialize
{
    //code
    if (aav_gVao_sphere)
    {
        glDeleteVertexArrays(1, &aav_gVao_sphere);
        aav_gVao_sphere = 0;
    }
    
    if (aav_Vbo_sphere_position)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_position);
        aav_Vbo_sphere_position = 0;
    }
    
    if (aav_Vbo_sphere_normals)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_normals);
        aav_Vbo_sphere_normals = 0;
    }
    
    if (aav_Vbo_sphere_elements)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_elements);
        aav_Vbo_sphere_elements = 0;
    }
    
    
    glDetachShader(gShaderProgramObject_pv, gVertexShaderObject_pv);
    glDetachShader(gShaderProgramObject_pv, gFragmentShaderObject_pv);
    
    glDeleteShader(gVertexShaderObject_pv);
    gVertexShaderObject_pv = 0;
    glDeleteShader(gFragmentShaderObject_pv);
    gFragmentShaderObject_pv = 0;
    
    glDeleteProgram(gShaderProgramObject_pv);
    gShaderProgramObject_pv = 0;
    
    
    glDetachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);
    glDetachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);
    
    glDeleteShader(gVertexShaderObject_pf);
    gVertexShaderObject_pv = 0;
    glDeleteShader(gFragmentShaderObject_pf);
    gFragmentShaderObject_pf = 0;
    
    glDeleteProgram(gShaderProgramObject_pf);
    gShaderProgramObject_pf = 0;
    
    glUseProgram(0);
    
    
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}

-(void)dealloc
{
    //code
    [self uninitialize];
    [super dealloc];
}

@end
