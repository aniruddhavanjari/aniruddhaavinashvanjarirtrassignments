//
//  MyView.m
//  Window
//
//  Created by OM-SAI on 03/07/21.
//  Copyright © 2021 OM-SAI. All rights reserved.
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
#import "sphere.h"


using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};


GLfloat aav_anglePyramid = 0.0f;

bool aav_bAnimationflag = false;
bool aav_bLight = false;

struct Light
{
    vec4 lightAmbiant;
    vec4 lightDefuse;
    vec4 lightSpecular;
    vec4 lightPosition;
};



struct Matrial
{
    vec4 materialAmbiant;
    vec4 materialDiffuse;
    vec4 materialSpecular;
    float materialShininess;
};

struct Light light[2] = {{ vec4(1.0f,0.0f,0.0f,1.0f),vec4(1.0f,0.0f,0.0f,1.0f),vec4(1.0f,0.0f,0.0f,1.0f),vec4(-2.0f,0.0f,-0.0f,1.0f)},
    {vec4(0.0f,0.0f,1.0f,1.0f),vec4(0.0f,0.0f,1.0f,1.0f),vec4(0.0f,0.0f,1.0f,1.0f),vec4(2.0f,0.0f,-0.0f,1.0f)}};


struct Matrial material = { vec4(0.0f,0.0f,0.0f,1.0f),vec4(1.0f,1.0f,1.0f,1.0f),vec4(1.0f,1.0f,1.0f,1.0f),128.0f};


//shader
GLuint aav_gVertexShaderObject;
GLuint aav_gFragmentShaderObject;
GLuint aav_gShaderProgramObject;

GLuint aav_Vao_pyramid;
GLuint aav_Vbo_pyramid_position;
GLuint aav_Vbo_pyramid_normals;

//matrix
mat4 aav_PerspectiveProjectionMatrix;


//uniforms
GLuint aav_viewMatrixUniform;
GLuint aav_modelMatrixUniform;
GLuint aav_ProjectionMatrixUniform;

GLuint aav_laUniform[2];
GLuint aav_ldUniform[2];
GLuint aav_lsUniform[2];
GLuint aav_lightPositionUniform[2];

GLuint aav_kaUniform;
GLuint aav_kdUniform;
GLuint aav_ksUniform;

GLuint aav_kShininessUniform;

GLuint aav_lKeyPressedUniform;

@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimationg;
    

}

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        //OpenGL code
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil]];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context Creation Failed.\n");
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        //render to texture
        glGenFramebuffers(1,&defaultFramebuffer );
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        //storage dena
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        //vatanya chi sange
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        //12 step : buffer width , height set
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //16 depth buffer sathi
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("FrameBuffer is Not Complete.\n");
            [self uninitialize];
            return(nil);
        }
        
        printf("%s\n",glGetString(GL_RENDERER));
        printf("%s\n",glGetString(GL_VERSION));
        printf("%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        animationFrameInterval = 60; //iOS 8.2 take bufeault
        isAnimationg = NO;
        
    
        //vertex shader source code start from here
        //**VERTEX SHADER***
        aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* aav_vertexShaserSourceCode =
        "#version 300 es" \
        "\n" \
        "precision mediump int;"\
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_lKeyPressed;" \
        "uniform vec3 u_la[2];" \
        "uniform vec3 u_ld[2];" \
        "uniform vec3 u_ls[2];" \
        "uniform vec4 u_lightPosistion[2];" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_kd;" \
        "uniform vec3 u_ks;" \
        "uniform float u_kShineness;"\
        "vec3 tempLight = vec3(0.0f,0.0f,0.0f);"\
        "vec3 lightDirection[2];" \
        "vec3 reflection_vector[2];" \
        "vec3 ambiant[2];" \
        "vec3 diffuse[2];" \
        "vec3 specular[2];" \
        
        "out vec3 fong_ads_light;" \
        "void main(void)" \
        "{" \
        "    if(u_lKeyPressed == 1)" \
        "    {"\
        "        vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition; " \
        "        vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal); " \
        "        vec3 view_vector = normalize(-eyeCordinate.xyz);" \
        "        for(int i = 0 ; i < 2 ; i++)"\
        "        {"\
        "            lightDirection[i] = normalize(vec3(u_lightPosistion[i] - eyeCordinate));" \
        "            reflection_vector[i] = reflect(-lightDirection[i],tranformed_normal);" \
        "            ambiant[i] = u_la[i] * u_ka;" \
        "            diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i],tranformed_normal),0.0f);" \
        "            specular[i] = u_ls[i] * u_ks * pow(max(dot(reflection_vector[i],view_vector),0.0f),u_kShineness);" \
        "            tempLight = tempLight + ambiant[i] + diffuse[i] + specular[i];" \
        "            fong_ads_light = tempLight;" \
        "        }"\
        "    }" \
        "    else" \
        "    {" \
        "        fong_ads_light = vec3(1.0f,1.0f,1.0f);" \
        "    }" \
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
        
        //compile shader , Error checking of Compilation
        glCompileShader(aav_gVertexShaderObject);
        
        GLint aav_infoLogLength = 0;
        GLint aav_shaderCompiledStatus = 0;
        char* aav_szBuffer = NULL;
        
        glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
        
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
                                       &aav_written, aav_szBuffer);
                    printf("Vertex Shader Compilation Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                  
                }
            }
        }
        
        //**FRAGMENT SHADER**
        aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "in vec3 fong_ads_light;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "    FragColor = vec4(fong_ads_light,1.0f);" \
        "}";
        glShaderSource(aav_gFragmentShaderObject, 1,
                       (const GLchar**)&fragmentShaderSourceCode,NULL);
        //compile shader
        glCompileShader(aav_gFragmentShaderObject);
        
        
        aav_szBuffer = NULL;
        aav_infoLogLength = 0;
        aav_shaderCompiledStatus = 0;
        
        glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS,
                      &aav_shaderCompiledStatus);
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
                                       &aav_written, aav_szBuffer);
                    printf( "Fragment Shader Compilation Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                    
                }
            }
        }
        
        //**SHADER PROGRAM**
        //Create
        aav_gShaderProgramObject = glCreateProgram();
        
        glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
        glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");
        
        glLinkProgram(aav_gShaderProgramObject);
        
        aav_infoLogLength = 0;
        GLint aav_shaderProgramLinkStatus = 0;
        aav_szBuffer = NULL;
        
        glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
        if (aav_shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_infoLogLength > 0)
                {
                    GLsizei aav_aav_written;
                    glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
                                        &aav_aav_written, aav_szBuffer);
                    printf("Shader Program Link Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                    
                }
            }
        }
        
        //Post Linking Information
        aav_modelMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_model_matrix");
        aav_viewMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_view_matrix");
        aav_ProjectionMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_projection_matrix");
        
        aav_laUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_la[0]");
        aav_ldUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_ld[0]");
        aav_lsUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_ls[0]");
        aav_lightPositionUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_lightPosistion[0]");
        
        aav_laUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_la[1]");
        aav_ldUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_ld[1]");
        aav_lsUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_ls[1]");
        aav_lightPositionUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_lightPosistion[1]");
        
        aav_kaUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ka");
        aav_kdUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kd");
        aav_ksUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ks");
        
        aav_kShininessUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kShineness");
        
        aav_lKeyPressedUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_lKeyPressed");
        
        //vertices array declation
        const GLfloat aav_pyramidVertex[] =
        {
            0.0f,1.0f,0.0f,
            -1.0f,-1.0f,1.0f,
            1.0f,-1.0f,1.0f,
            
            0.0f,1.0f,0.0f,
            1.0f,-1.0,1.0f,
            1.0f,-1.0f,-1.0f,
            
            0.0f,1.0f,0.0f,
            -1.0f,-1.0f,-1.0f,
            1.0f,-1.0f,-1.0f,
            
            0.0f,1.0f,0.0f,
            -1.0f,-1.0f,1.0f,
            -1.0f,-1.0f,-1.0f
        };
        
        const GLfloat aav_pyramidNormals[] =
        {
            0.0f,0.447214f,0.894427f,
            0.0f, 0.447214f, 0.894427f,
            0.0f, 0.447214f, 0.894427f,
            
            0.894427f,0.447214f,0.0f,
            0.894427f, 0.447214f, 0.0f,
            0.894427f, 0.447214f, 0.0f,
            
            0.0f,0.447214f, -0.894427f,
            0.0f, 0.447214f, -0.894427f,
            0.0f, 0.447214f, -0.894427f,
            
            -0.894427f, 0.447214f, 0.0f,
            -0.894427f, 0.447214f, 0.0f,
            -0.894427f,0.447214f,0.0f
        };
        
        
        //Record Square
        glGenVertexArrays(1,&aav_Vao_pyramid);
        glBindVertexArray(aav_Vao_pyramid);
        
        glGenBuffers(1,&aav_Vbo_pyramid_position);
        glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_pyramid_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pyramidVertex), aav_pyramidVertex,GL_STATIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glGenBuffers(1, &aav_Vbo_pyramid_normals);
        glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_pyramid_normals);
        glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pyramidNormals), aav_pyramidNormals, GL_STATIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Record Off / Pause
        glBindVertexArray(0);
        
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        glClearDepthf(1.0f);
        //
        glClearColor(0.0f,0.0f,0.0f,1.0f);
        
        aav_PerspectiveProjectionMatrix = mat4::identity();
        

        
        // Gestures
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action: @selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; //ake double tap la to single tap manat nahe
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecongnizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self  action:@selector(onLongPress:)];
        [longPressGestureRecongnizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecongnizer];
    }
    
    return(self);
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}
/*
-(void)drawRect:(CGRect)rect
{
    // Drawing code
   
}
*/

-(void)layoutSubviews
{
    //code
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    //storage dena
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
    GLint width;
    GLint height;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is Not Complete.\n");
        [self uninitialize];
    }
    
    if(height < 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
    
    
    [self drawView];
}

-(void)drawView
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer); //patak program
    //glUseProgram code start from here
   
    mat4 aav_scaMatrix;
    mat4 aav_translateMatrix;
    mat4 aav_rotationMatrixA;
    mat4 aav_rotationMatrixY;
    
    vec4 aav_lightPosition;
    aav_lightPosition = vec4( 0.0f,0.0f,2.0f,1.0f );
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //triangle
    glUseProgram(aav_gShaderProgramObject);
    
    if (aav_bLight == true)
    {
        //Light Enable
        glUniform1i(aav_lKeyPressedUniform, 1);
        //Light-1
        glUniform3fv(aav_laUniform[0], 1, light[0].lightAmbiant);                // la
        glUniform3fv(aav_ldUniform[0], 1, light[0].lightDefuse);                // ld
        glUniform3fv(aav_lsUniform[0], 1, light[0].lightSpecular);                // ls
        glUniform4fv(aav_lightPositionUniform[0], 1,light[0].lightPosition);    //lightPosition
        
        //Light-2
        glUniform3fv(aav_laUniform[1], 1, light[1].lightAmbiant);                // la
        glUniform3fv(aav_ldUniform[1], 1, light[1].lightDefuse);                // ld
        glUniform3fv(aav_lsUniform[1], 1, light[1].lightSpecular);                // ls
        glUniform4fv(aav_lightPositionUniform[1], 1, light[1].lightPosition);    //lightPosition
        
        //material
        glUniform3fv(aav_kaUniform, 1, material.materialAmbiant);    // ka
        glUniform3fv(aav_kdUniform, 1, material.materialDiffuse);    // kd
        glUniform3fv(aav_ksUniform, 1, material.materialSpecular);    //ks
        glUniform1f(aav_kShininessUniform,material.materialShininess);
    }
    else
    {
        glUniform1i(aav_lKeyPressedUniform, 0);
    }
    
    
    //OpenGL Drawing
    mat4 aav_modelMateix = mat4::identity();
    mat4 aav_viewMatrix = mat4::identity();
    
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    
    aav_rotationMatrixY = vmath::rotate(aav_anglePyramid, 0.0f, 1.0f, 0.0f);
    
    aav_rotationMatrixA =  aav_rotationMatrixY ;
    
    aav_modelMateix = aav_translateMatrix *aav_rotationMatrixA;
    
    glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
    glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
    glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
    
    
    glBindVertexArray(aav_Vao_pyramid);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    if (aav_bAnimationflag == TRUE)
    {
        aav_anglePyramid = aav_anglePyramid + 1.00f;
        if(aav_anglePyramid >= 360.0f)
            aav_anglePyramid = 0.0f;
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}


-(void)startAnimation
{
    //code
    if(isAnimationg == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimationg = YES;
    }
    
}

-(void)stopAnimation
{
    //code
    if(isAnimationg == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimationg = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    if (aav_bLight == false)
    {
        aav_bLight = true;
    }
    else
    {
        aav_bLight = false;
    }
    //printf("OnSingleTap\n");

}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    if (aav_bAnimationflag == false)
    {
        aav_bAnimationflag = true;
    }
    else
    {
        aav_bAnimationflag = false;
    }
    //printf("OnDoubleTap\n");
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self uninitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
    //printf("LongPress\n");

}

-(void)uninitialize
{
    //code
    if (aav_Vao_pyramid)
    {
        glDeleteVertexArrays(1, &aav_Vao_pyramid);
        aav_Vao_pyramid = 0;
    }
    
    if (aav_Vbo_pyramid_position)
    {
        glDeleteBuffers(1, &aav_Vbo_pyramid_position);
        aav_Vbo_pyramid_position = 0;
    }
    
    if (aav_Vbo_pyramid_normals)
    {
        glDeleteBuffers(1, &aav_Vbo_pyramid_normals);
        aav_Vbo_pyramid_normals = 0;
    }
    
    
    glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glDeleteShader(aav_gVertexShaderObject);
    aav_gVertexShaderObject = 0;
    glDeleteShader(aav_gFragmentShaderObject);
    aav_gFragmentShaderObject = 0;
    
    glDeleteProgram(aav_gShaderProgramObject);
    aav_gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}

-(void)dealloc
{
    //code
    [self uninitialize];
    [super dealloc];
}

@end
