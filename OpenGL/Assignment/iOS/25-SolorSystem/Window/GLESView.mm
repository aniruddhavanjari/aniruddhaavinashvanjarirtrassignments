//
//  MyView.m
//  Window
//
//  Created by OM-SAI on 03/07/21.
//  Copyright © 2021 OM-SAI. All rights reserved.
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"
#import "sphere.h"


using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};


bool aav_bLight = false;

//statck
mat4 matrixStack[10];
int stacktop = -1;
int stackSize = 10;
int top = -1;



GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

mat4 aav_PerspectiveProjectionMatrix;

//Sphere Variable
GLfloat aav_sphere_vertices[1146];
GLfloat aav_sphere_normals[1146];
GLfloat aav_sphere_texture[764];
short aav_sphere_elements[2280];
GLuint aav_numSphereVertices;
GLuint aav_numSphereElements;

GLuint aav_gVao_sphere;
GLuint aav_Vbo_sphere_position;
GLuint aav_Vbo_sphere_color;
GLuint aav_Vbo_sphere_elements;


GLfloat aav_color[3];


GLuint aav_viewMatrixUniform;
GLuint aav_modelMatrixUniform;
GLuint aav_ProjectionMatrixUniform;
GLuint aav_colorUniform;
float rotationAngle             = 0.0f;
float  rotationAngle1            = 0.0f;

GLuint aav_tap = 0;
GLuint aav_tap_double = 0;
@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimationg;
    
}

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        //OpenGL code
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil]];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context Creation Failed.\n");
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        //render to texture
        glGenFramebuffers(1,&defaultFramebuffer );
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        //storage dena
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        //vatanya chi sange
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        //12 step : buffer width , height set
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //16 depth buffer sathi
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("FrameBuffer is Not Complete.\n");
            [self uninitialize];
            return(nil);
        }
        
        printf("%s\n",glGetString(GL_RENDERER));
        printf("%s\n",glGetString(GL_VERSION));
        printf("%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        animationFrameInterval = 60; //iOS 8.2 take bufeault
        isAnimationg = NO;
        
       
       
        
        //vertex shader source code start from here
        //**VERTEX SHADER***
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* aav_vertexShaserSourceCode =
        "#version 300 es" \
        "\n" \
        "precision mediump int;"\
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform vec3 u_color;"\
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "out vec3 fong_ads_light;" \
        "void main(void)" \
        "{" \
        "    fong_ads_light = u_color;" \
        "    gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        glShaderSource(gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
        
        //compile shader , Error checking of Compilation
        glCompileShader(gVertexShaderObject);
        
        GLint aav_infoLogLength = 0;
        GLint aav_shaderCompiledStatus = 0;
        char* szBuffer = NULL;
        
        glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
        
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(gVertexShaderObject, aav_infoLogLength,
                                       &aav_written, szBuffer);
                    printf("Vertex Shader Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                  
                }
            }
        }
        
        //**FRAGMENT SHADER**
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "in vec3 fong_ads_light;" \
        "out vec4 FragColor;" \
        "void main(void)" \
        "{" \
        "    FragColor = vec4(fong_ads_light,1.0f);" \
        "}";
        glShaderSource(gFragmentShaderObject, 1,
                       (const GLchar**)&fragmentShaderSourceCode,NULL);
        //compile shader
        glCompileShader(gFragmentShaderObject);
        
        
        szBuffer = NULL;
        aav_infoLogLength = 0;
        aav_shaderCompiledStatus = 0;
        
        glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS,
                      &aav_shaderCompiledStatus);
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(gFragmentShaderObject, aav_infoLogLength,
                                       &aav_written, szBuffer);
                    printf("Fragment Shader Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                   
                }
            }
        }
        
        //**SHADER PROGRAM**
        //Create
        gShaderProgramObject = glCreateProgram();
        
        glAttachShader(gShaderProgramObject, gVertexShaderObject);
        glAttachShader(gShaderProgramObject, gFragmentShaderObject);
        
        glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");
        
        glLinkProgram(gShaderProgramObject);
        
        aav_infoLogLength = 0;
        GLint aav_shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        
        glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
        if (aav_shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_infoLogLength > 0)
                {
                    GLsizei aav_aav_written;
                    glGetProgramInfoLog(gShaderProgramObject, aav_infoLogLength,
                                        &aav_aav_written, szBuffer);
                    printf("Shader Program Link Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                   
                }
            }
        }
        
        //Post Linking Information
        /**************************************/
        aav_modelMatrixUniform        = glGetUniformLocation(gShaderProgramObject, "u_model_matrix"        );
        aav_viewMatrixUniform        = glGetUniformLocation(gShaderProgramObject, "u_view_matrix"        );
        aav_ProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix"    );
        aav_colorUniform            = glGetUniformLocation(gShaderProgramObject, "u_color"                );
        
        /**************************************/
        
        Sphere *sphere = [[Sphere alloc]init];
        
        [sphere getSphereVertexData:aav_sphere_vertices :aav_sphere_normals :aav_sphere_texture :aav_sphere_elements];
        
        aav_numSphereVertices = [sphere getNumberOfSphereVertices];
        aav_numSphereElements = [sphere getNumberOfSphereElements];
        
        glGenVertexArrays(1, &aav_gVao_sphere);
        glBindVertexArray(aav_gVao_sphere);
        
        //Record Sphere
        glGenBuffers(1, &aav_Vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_vertices), aav_sphere_vertices, GL_STATIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        
        //elements
        glGenBuffers(1, &aav_Vbo_sphere_elements);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements); // Elemtnes Drawing is Also Called As Index Drawing, Elements Drawing
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(aav_sphere_elements), aav_sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        
        //Record Off / Pause
        glBindVertexArray(0);
        
        glClearDepthf(1.0f);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
    
        //
        glClearColor(0.0f,0.0f,0.0f,1.0f);
        
        aav_PerspectiveProjectionMatrix = mat4::identity();
        

        
        // Gestures
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action: @selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; //ake double tap la to single tap manat nahe
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecongnizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self  action:@selector(onLongPress:)];
        [longPressGestureRecongnizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecongnizer];
    }
    
    return(self);
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}
/*
-(void)drawRect:(CGRect)rect
{
    // Drawing code
   
}
*/

-(void)layoutSubviews
{
    //code
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    //storage dena
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
    GLint width;
    GLint height;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is Not Complete.\n");
        [self uninitialize];
    }
    
    if(height < 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
    
    
    [self drawView];
}

-(void)drawView
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer); //patak program
    //glUseProgram code start from here
    
    //variable declaration
    mat4 aav_modelMatrix;
    mat4 aav_viewMatrix;
    mat4 aav_translateMatrix;
    mat4 aav_rotationMatrix;
    mat4 aav_scaleMatrix;
    mat4 aav_popMatrix;
    
    aav_popMatrix = mat4::identity();
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //triangle
    glUseProgram(gShaderProgramObject);
    
    //OpenGL Drawing
    aav_color[0] = 1.0f;
    aav_color[1] = 1.0f;
    aav_color[2] = 0.0f;
    glUniform3fv(aav_colorUniform,1, (GLfloat *)aav_color);
    
    aav_modelMatrix = mat4::identity();
    aav_viewMatrix = mat4::identity();
    
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    aav_modelMatrix = aav_translateMatrix ;
    
    glUniformMatrix4fv(aav_modelMatrixUniform,1,GL_FALSE, aav_modelMatrix);
    glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
    glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
    
    //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glBindVertexArray(aav_gVao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES,aav_numSphereElements,GL_UNSIGNED_SHORT,0);
    glBindVertexArray(0);
    
    //Earth
    
    aav_color[0] = 0.0f;
    aav_color[1] = 1.0f;
    aav_color[2] = 1.0f;
    glUniform3fv(aav_colorUniform, 1, (GLfloat*)aav_color);
    
    aav_rotationMatrix = mat4::identity();
    aav_translateMatrix = vmath::translate(-1.3f, 0.0f, 0.0f);
    aav_rotationMatrix = vmath::rotate(rotationAngle1,0.0f,1.0f,0.0f);
    aav_modelMatrix = aav_modelMatrix *  aav_rotationMatrix * aav_translateMatrix;
    [self push:aav_modelMatrix];
    
    aav_scaleMatrix = vmath::scale(0.5f, 0.5f, 0.5f);
    aav_modelMatrix = aav_modelMatrix * aav_scaleMatrix;
    aav_rotationMatrix = vmath::rotate(rotationAngle1, 0.0f, 1.0f, 0.0f);
    aav_modelMatrix = aav_modelMatrix * aav_rotationMatrix;
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMatrix);
    glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
    glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
    
    glBindVertexArray(aav_gVao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
    glDrawElements(GL_LINES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    //moon
    [self pop:&aav_modelMatrix];
    aav_color[0] = 1.0f;
    aav_color[1] = 1.0f;
    aav_color[2] = 1.0f;
    glUniform3fv(aav_colorUniform, 1, (GLfloat*)aav_color);
    
    aav_rotationMatrix = mat4::identity();
    aav_translateMatrix = vmath::translate(-0.5f, 0.0f, 0.0f);
    aav_rotationMatrix = vmath::rotate(rotationAngle1, 0.0f, 1.0f, 0.0f);
    aav_modelMatrix = aav_modelMatrix * aav_rotationMatrix * aav_translateMatrix;
    
    aav_scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
    aav_modelMatrix = aav_modelMatrix * aav_scaleMatrix;
    aav_rotationMatrix = vmath::rotate(rotationAngle1, 0.0f, 1.0f, 0.0f);
    
    aav_modelMatrix = aav_modelMatrix * aav_rotationMatrix;
    glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMatrix);
    glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
    glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
    
    glBindVertexArray(aav_gVao_sphere);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
    glDrawElements(GL_LINES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    
    if(aav_tap == 1)
    {
        rotationAngle = rotationAngle + 1.0f;
        if(rotationAngle > 360.0f)
        {
            rotationAngle = 0.0f;
        }
    }
    
    if(aav_tap == 2)
    {
        rotationAngle = rotationAngle - 1.0f;
        if(rotationAngle < -360.0f)
        {
            rotationAngle = 0.0f;
        }
    }
    
    if(aav_tap_double == 1)
    {
        rotationAngle1 = rotationAngle1 + 1.0f;
        if(rotationAngle1 > 360.0f)
        {
            rotationAngle1 = 0.0f;
        }
    }
    
    if(aav_tap_double == 2)
    {
        rotationAngle1 = rotationAngle1 - 1.0f;
        if(rotationAngle1 < -360.0f)
        {
            rotationAngle1 = 0.0f;
        }
    }
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}

-(void)push:(mat4)pushMatrix
{
    void uninitialize(void);
    
    if (top == stackSize)
    {
        printf("Stack Full");
        [self release];
       
    }
    top = top + 1;
    matrixStack[top] = pushMatrix;
}

-(void)pop:(mat4*) popMatrix
{
    void uninitialize(void);
    
    if (top == -1)
    {
        printf( "Stack Empty");
        [self release];
        
    }
    *popMatrix = matrixStack[top];
    top = top - 1;
}



-(void)startAnimation
{
    //code
    if(isAnimationg == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimationg = YES;
    }
    
}

-(void)stopAnimation
{
    //code
    if(isAnimationg == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimationg = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    aav_tap = aav_tap + 1;
    
    if(aav_tap > 3)
    {
        aav_tap = 0;
    }
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
  
    aav_tap_double = aav_tap_double + 1;
    
    if(aav_tap_double > 3)
    {
        aav_tap_double = 0;
    }
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self uninitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
}

-(void)uninitialize
{
    //code
    if (aav_gVao_sphere)
    {
        glDeleteVertexArrays(1, &aav_gVao_sphere);
        aav_gVao_sphere = 0;
    }
    
    if (aav_Vbo_sphere_position)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_position);
        aav_Vbo_sphere_position = 0;
    }
    
    if (aav_Vbo_sphere_elements)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_elements);
        aav_Vbo_sphere_elements = 0;
    }
    
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;
    
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    
    
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}

-(void)dealloc
{
    //code
    [self uninitialize];
    [super dealloc];
}

@end
