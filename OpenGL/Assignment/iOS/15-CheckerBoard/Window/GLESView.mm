//
//  MyView.m
//  Window
//
//  Created by OM-SAI on 03/07/21.
//  Copyright © 2021 OM-SAI. All rights reserved.
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};

#define CHECKER_IMAGE_WIDTH 64
#define CHECKER_IMAGE_HEIGHT 64

@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimationg;
    
    //shader object//shader object
    GLuint aav_gVertexShaderObject;
    GLuint aav_gFragmentShaderObject;
    GLuint aav_gShaderProgramObject;
    
    GLuint aav_gVao_cube;
    GLuint aav_gVbo_position_cube;
    GLuint aav_gVbo_texture_cube;
    
    GLuint aav_mvp_MatrixUniform;
    GLuint aav_PostionUniform;
    GLuint aav_textureSamplerUniform;
    
    mat4 aav_PerspectiveProjectionMatrix;
    
    GLubyte aav_checkImage[CHECKER_IMAGE_WIDTH][CHECKER_IMAGE_HEIGHT][4];
    
    GLuint aav_uiTextImage;
    
    
}

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        //OpenGL code
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil]];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context Creation Failed.\n");
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        //render to texture
        glGenFramebuffers(1,&defaultFramebuffer );
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        //storage dena
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        //vatanya chi sange
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        //12 step : buffer width , height set
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //16 depth buffer sathi
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("FrameBuffer is Not Complete.\n");
            [self uninitialize];
            return(nil);
        }
        
        printf("%s\n",glGetString(GL_RENDERER));
        printf("%s\n",glGetString(GL_VERSION));
        printf("%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        animationFrameInterval = 60; //iOS 8.2 take bufeault
        isAnimationg = NO;
        
        
        //vertex shader source code start from here
        //**VERTEX SHADER***
        aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* aav_vertexShaserSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec2 vTexCoord;" \
        "uniform mat4 aav_u_mvpMatrix;" \
        "out vec2 aav_out_TexCoord;" \
        "void main(void)" \
        "{" \
        "gl_Position = aav_u_mvpMatrix * vPosition;" \
        "aav_out_TexCoord = vTexCoord;" \
        "}";
        glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
        
        //compile shader , Error checking of Compilation
        glCompileShader(aav_gVertexShaderObject);
        
        GLint aav_infoLogLength = 0;
        GLint aav_shaderCompiledStatus = 0;
        char* aav_szBuffer = NULL;
        
        glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
        
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
                                       &aav_written, aav_szBuffer);
                    printf("Vertex Shader Compilation Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                 
                }
            }
        }
        
        //**FRAGMENT SHADER**
        aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "in vec2 aav_out_TexCoord;" \
        "uniform sampler2D u_texture_sampler;" \
        "out vec4 aav_FragColor;" \
        "void main(void)" \
        "{" \
        "    aav_FragColor = texture(u_texture_sampler,aav_out_TexCoord);" \
        "}";
        glShaderSource(aav_gFragmentShaderObject, 1,
                       (const GLchar**)&fragmentShaderSourceCode,NULL);
        //compile shader
        glCompileShader(aav_gFragmentShaderObject);
        
        aav_szBuffer = NULL;
        aav_infoLogLength = 0;
        aav_shaderCompiledStatus = 0;
        
        glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS,
                      &aav_shaderCompiledStatus);
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
                                       &aav_written, aav_szBuffer);
                    printf("Fragment Shader Compilation Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                    
                }
            }
        }
        
        //**SHADER PROGRAM**
        //Create
        aav_gShaderProgramObject = glCreateProgram();
        
        glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
        glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_TEXCORD, "vTexCoord");
        
        glLinkProgram(aav_gShaderProgramObject);
        
        aav_infoLogLength = 0;
        GLint aav_shaderProgramLinkStatus = 0;
        aav_szBuffer = NULL;
        
        glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
        if (aav_shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_infoLogLength > 0)
                {
                    GLsizei aav_aav_written;
                    glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
                                        &aav_aav_written, aav_szBuffer);
                    printf("Shader Program Link Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                    
                }
            }
        }
        
        //Post Linking Information
        aav_mvp_MatrixUniform         = glGetUniformLocation(aav_gShaderProgramObject, "aav_u_mvpMatrix");
        
        
        const GLfloat aav_cubeTexCoord[] =
        {
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
            
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f
        };
        
        //Record Square
        glGenVertexArrays(1,&aav_gVao_cube);
        glBindVertexArray(aav_gVao_cube);
        
        glGenBuffers(1,&aav_gVbo_position_cube);
        glBindBuffer(GL_ARRAY_BUFFER,aav_gVbo_position_cube);
        glBufferData(GL_ARRAY_BUFFER, 4*3*sizeof(GLfloat), NULL,GL_DYNAMIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glGenBuffers(1, &aav_gVbo_texture_cube);
        glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_texture_cube);
        glBufferData(GL_ARRAY_BUFFER,sizeof(aav_cubeTexCoord), aav_cubeTexCoord, GL_STATIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_TEXCORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_TEXCORD);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Record Off / Pause
        glBindVertexArray(0);
        
       
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        //
        glClearColor(0.0f,0.0f,0.0f,1.0f);
        
        [self loadTexture];
        
        aav_PerspectiveProjectionMatrix = mat4::identity();
        
        // Gestures
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action: @selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:2];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; //ake double tap la to single tap manat nahe
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecongnizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self  action:@selector(onLongPress:)];
        [longPressGestureRecongnizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecongnizer];
    }
    
    return(self);
}

-(void)loadTexture
{
    //code
    [self MakeCheckImage];
    glGenTextures(1,&aav_uiTextImage);
    glBindTexture(GL_TEXTURE_2D, aav_uiTextImage);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    
    
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECKER_IMAGE_WIDTH, CHECKER_IMAGE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, aav_checkImage);
    
}

-(void)MakeCheckImage
{
    //variable Declaration
    int aav_i, aav_j, aav_c;
    //Code
    for (aav_i = 0; aav_i < CHECKER_IMAGE_HEIGHT; aav_i++)
    {
        for (aav_j = 0; aav_j < CHECKER_IMAGE_WIDTH; aav_j++)
        {
            aav_c = (((aav_i & 0x8) == 0) ^ ((aav_j & 0x8) == 0)) * 255;
            aav_checkImage[aav_i][aav_j][0] = (GLubyte)aav_c;
            aav_checkImage[aav_i][aav_j][1] = (GLubyte)aav_c;
            aav_checkImage[aav_i][aav_j][2] = (GLubyte)aav_c;
            aav_checkImage[aav_i][aav_j][3] = 255;
        }
    }
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}
/*
-(void)drawRect:(CGRect)rect
{
    // Drawing code
   
}
*/

-(void)layoutSubviews
{
    //code
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    //storage dena
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
    GLint width;
    GLint height;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is Not Complete.\n");
        [self uninitialize];
    }
    
    if(height < 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
    
    
    [self drawView];
}

-(void)drawView
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer); //patak program
    //glUseProgram code start from here
    
    mat4 aav_scaMatrix;
    mat4 aav_translateMatrix;
    
    
    const GLfloat aav_cubeVertex[] =
    {
        -2.0f, -1.0f, 0.0f,
        -2.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, -1.0f, 0.0f,
    };
    
    const GLfloat aav_cubeVertexAngle[] =
    {
        1.0f, -1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        2.41421f, 1.0f, -1.41421f,
        2.41421f, -1.0f, -1.41421f
    };
    
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(aav_gShaderProgramObject);
    
    mat4 aav_modelViewMateix = mat4::identity();
    mat4 aav_modelViewProjectMatrix = mat4::identity();
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.6f);
    
    aav_modelViewMateix = aav_translateMatrix;
    aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
    
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, aav_uiTextImage);
    glUniform1i(aav_textureSamplerUniform, 0);
    
    //Plane Quad
    glBindVertexArray(aav_gVao_cube);
    
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position_cube);
    glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), aav_cubeVertex, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glBindVertexArray(0);
    
    //Second Quad
    glBindVertexArray(aav_gVao_cube);
    
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position_cube);
    glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), aav_cubeVertexAngle, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}


-(void)startAnimation
{
    //code
    if(isAnimationg == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimationg = YES;
    }
    
}

-(void)stopAnimation
{
    //code
    if(isAnimationg == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimationg = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
   
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    printf("onSwipe\n");
    [self uninitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
}

-(void)uninitialize
{
    //code
    if (aav_gVao_cube)
    {
        glDeleteVertexArrays(1, &aav_gVao_cube);
        aav_gVao_cube = 0;
    }
    
    if (aav_gVbo_position_cube)
    {
        glDeleteBuffers(1, &aav_gVbo_position_cube);
        aav_gVbo_position_cube = 0;
    }
    
    if (aav_gVbo_texture_cube)
    {
        glDeleteBuffers(1, &aav_gVbo_texture_cube);
        aav_gVbo_texture_cube = 0;
    }
    
    glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glDeleteShader(aav_gVertexShaderObject);
    aav_gVertexShaderObject = 0;
    glDeleteShader(aav_gFragmentShaderObject);
    aav_gFragmentShaderObject = 0;
    
    glDeleteProgram(aav_gShaderProgramObject);
    aav_gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}

-(void)dealloc
{
    //code
    [self uninitialize];
    [super dealloc];
}

@end
