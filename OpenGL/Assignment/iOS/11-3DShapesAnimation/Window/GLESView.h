//
//  MyView.h
//  Window
//
//  Created by OM-SAI on 03/07/21.
//  Copyright © 2021 OM-SAI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GLESView : UIView <UIGestureRecognizerDelegate>

-(void)startAnimation;
-(void)stopAnimation;

@end

