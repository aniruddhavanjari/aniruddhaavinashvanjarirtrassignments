//
//  MyView.m
//  Window
//
//  Created by OM-SAI on 03/07/21.
//  Copyright © 2021 OM-SAI. All rights reserved.
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};

bool aav_bLight = false;
BOOL aav_AnimationFlag = FALSE;

GLfloat aav_rotation = 0.0f;

//Light Array
GLfloat aav_lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };    //la
GLfloat aav_lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };    // ld
GLfloat aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };//ls

GLfloat aav_lightPosition[] = { 0.0f,0.0f,2.0f,1.0f };

//material Array
GLfloat aav_materialAmbiant[]    = { 0.0f, 0.0f, 0.0f ,0.0f}; // ka
GLfloat aav_materialDiffuse[]    = {1.0f,1.0f,1.0f,1.0f};    // kd
GLfloat aav_materialSpecular[]    = {1.0f,1.0f,1.0f,1.0f};    // ks


float* vertexArray = NULL;

@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimationg;
    
    GLuint aav_gVertexShaderObject;
    GLuint aav_gFragmentShaderObject;
    GLuint aav_gShaderProgramObject;
    
    
    GLuint aav_mvp_MatrixUniform;
    
    mat4 aav_PerspectiveProjectionMatrix;
    
    GLuint aav_gVao_cube;
    GLuint aav_gVbo_cubeAll;
    
    GLuint aav_viewMatrixUniform;
    GLuint aav_modelMatrixUniform;
    GLuint aav_ProjectionMatrixUniform;
    
    GLuint aav_laUniform;
    GLuint aav_ldUniform;
    GLuint aav_lsUniform;
    GLuint aav_lightPositionUniform;
    
    GLuint aav_kShininessUniform;
    
    GLuint aav_lKeyPressedUniform;
    
    GLuint aav_textureSamplerUniform;
    
    GLuint aav_textureNew;
    
}

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        //OpenGL code
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil]];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context Creation Failed.\n");
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        //render to texture
        glGenFramebuffers(1,&defaultFramebuffer );
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        //storage dena
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        //vatanya chi sange
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        //12 step : buffer width , height set
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //16 depth buffer sathi
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("FrameBuffer is Not Complete.\n");
            [self uninitialize];
            return(nil);
        }
        
        printf("%s\n",glGetString(GL_RENDERER));
        printf("%s\n",glGetString(GL_VERSION));
        printf("%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        animationFrameInterval = 60; //iOS 8.2 take bufeault
        isAnimationg = NO;
        
       
        
       
        //vertex shader source code start from here
        //**VERTEX SHADER***
        aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* aav_vertexShaserSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;"    \
        "in vec3 vNormal;" \
        "in vec2 vTexCoord;" \
        "uniform mat4 u_view_matrix;" \
        "uniform mat4 u_model_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform vec4 u_lightPosistion;" \
        "out vec3 tranformed_normal;"\
        "out vec3 lightDirection;" \
        "out vec3 view_vector;" \
        "out vec2 aav_out_TexCoord;" \
        "out vec4 aav_out_color;" \
        "void main(void)" \
        "{" \
        "    vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
        "    tranformed_normal = (mat3(u_view_matrix * u_model_matrix) * vNormal);" \
        "    lightDirection = (vec3(u_lightPosistion - eyeCordinate));" \
        "    view_vector = (-eyeCordinate.xyz);" \
        "    aav_out_TexCoord = vTexCoord; " \
        "    aav_out_color = vColor;" \
        "    gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
        "}";
        glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
        
        //compile shader , Error checking of Compilation
        glCompileShader(aav_gVertexShaderObject);
        
        GLint aav_infoLogLength = 0;
        GLint aav_shaderCompiledStatus = 0;
        char* szBuffer = NULL;
        
        glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
        
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
                                       &aav_written, szBuffer);
                    printf("Vertex Shader Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
               
                }
            }
        }
        
        //**FRAGMENT SHADER**
        aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar* fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "in vec4 aav_out_color;" \
        "in vec3 tranformed_normal;"\
        "in vec3 lightDirection;" \
        "in vec3 view_vector;" \
        "in vec2 aav_out_TexCoord;" \
        "uniform int u_lKeyPressed;" \
        "uniform vec3 u_la;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_ls;" \
        
        "uniform float u_kShineness;"\
        
        "out vec4 FragColor;" \
        "vec3 fong_ads_light;" \
        "uniform sampler2D u_texture_sampler;" \
        "void main(void)" \
        "{" \
        "        vec3 normalize_tranformed_normal = normalize(tranformed_normal);" \
        "        vec3 normalize_lightDirection = normalize(lightDirection);" \
        "        vec3 normalize_view_vector = normalize(view_vector);" \
        "        vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" \
        "        vec3 ambiant = u_la ;" \
        "        vec3 diffuse = u_ld  * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" \
        "        vec3 specular = u_ls  * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness);" \
        "        fong_ads_light = ambiant + diffuse + specular;" \
        "        fong_ads_light = fong_ads_light * vec3(texture(u_texture_sampler,aav_out_TexCoord));" \
        "        FragColor = vec4(fong_ads_light,1.0f)  *  aav_out_color; " \
        "}";
        glShaderSource(aav_gFragmentShaderObject, 1,
                       (const GLchar**)&fragmentShaderSourceCode,NULL);
        //compile shader
        glCompileShader(aav_gFragmentShaderObject);
        
        
        szBuffer = NULL;
        aav_infoLogLength = 0;
        aav_shaderCompiledStatus = 0;
        
        glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS,
                      &aav_shaderCompiledStatus);
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
                                       &aav_written, szBuffer);
                    printf("Fragment Shader Compilation Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                  
                }
            }
        }
        
        //**SHADER PROGRAM**
        //Create
        aav_gShaderProgramObject = glCreateProgram();
        
        glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
        glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_TEXCORD, "vTexCoord");
        
        glLinkProgram(aav_gShaderProgramObject);
        
        aav_infoLogLength = 0;
        GLint aav_shaderProgramLinkStatus = 0;
        szBuffer = NULL;
        
        glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
        if (aav_shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_infoLogLength > 0)
                {
                    GLsizei aav_aav_written;
                    glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
                                        &aav_aav_written, szBuffer);
                    printf("Shader Program Link Log: %s\n", szBuffer);
                    free(szBuffer);
                    [self release];
                   
                }
            }
        }
        
        //Post Linking Information
        /**************************************/
        aav_textureSamplerUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_texture_sampler");
        
        aav_modelMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_model_matrix");
        aav_viewMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_view_matrix");
        aav_ProjectionMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_projection_matrix");
        
        aav_laUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_la");
        aav_ldUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ld");
        aav_lsUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ls");
        aav_lightPositionUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_lightPosistion");
        
        aav_kShininessUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kShineness");
        
        /**************************************/
        
        const GLfloat aav_cubeVertex[] =
        {
            0.5f,0.5f,0.5f,        1.0f,0.0f,0.0f,        0.0f, 0.0f, 1.0f,    0.0f,0.0f,
            -0.5f,0.5f,0.5f,    1.0f,0.0f,0.0f,        0.0f, 0.0f, 1.0f,    1.0f,0.0f,
            -0.5f,-0.5f,0.5f,    1.0f,0.0f,0.0f,        0.0f, 0.0f, 1.0f,    1.0f,1.0f,
            0.5f,-0.5f,0.5f,    1.0f,0.0f,0.0f,        0.0f, 0.0f, 1.0f,    0.0f,1.0f,
            
            0.5f, 0.5f, -0.5f,    0.0f,1.0f,0.0f,        1.0, 0.0f, 0.0f,    0.0f, 0.0f,
            0.5f, 0.5f, 0.5f,    0.0f,1.0f,0.0f,        1.0, 0.0f, 0.0f,    1.0f, 0.0f,
            0.5f, -0.5f, 0.5f,    0.0f,1.0f,0.0f,        1.0, 0.0f, 0.0f,    1.0f, 1.0f,
            0.5f, -0.5f, -0.5f,    0.0f,1.0f,0.0f,        1.0, 0.0f, 0.0f,    0.0f, 1.0f,
            
            0.5f, 0.5f, -0.5f,        0.0f,0.0f,1.0f,        0.0f, 0.0, -1.0f,    0.0f, 0.0f,
            -0.5f, 0.5f, -0.5f,        0.0f,0.0f,1.0f,        0.0f, 0.0, -1.0f,    1.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,    0.0f,0.0f,1.0f,        0.0f, 0.0, -1.0f,    1.0f, 1.0f,
            0.5f, -0.5f, -0.5f,        0.0f,0.0f,1.0f,        0.0f, 0.0, -1.0f,    0.0f, 1.0f,
            
            -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 1.0f,     -1.0f, 0.0f, 0.0f,        0.0f, 0.0f,
            -0.5f, 0.5f, 0.5f,        0.0f, 1.0f, 1.0f,     -1.0f, 0.0f, 0.0f,        1.0f, 0.0f,
            -0.5f, -0.5f, 0.5f,        0.0f, 1.0f, 1.0f,     -1.0f, 0.0f, 0.0f,        1.0f, 1.0f,
            -0.5f, -0.5f, -0.5f,    0.0f, 1.0f, 1.0f,     -1.0f, 0.0f, 0.0f,        0.0f, 1.0f,
            
            0.5f, 0.5f, -0.5f,    1.0f, 0.0f, 1.0f,    0.0f, 1.0f, 0.0f,    0.0f, 0.0f,
            -0.5f, 0.5f, -0.5f,    1.0f, 0.0f, 1.0f,    0.0f, 1.0f, 0.0f,    1.0f, 0.0f,
            -0.5f, 0.5f, 0.5f,    1.0f, 0.0f, 1.0f,     0.0f, 1.0f, 0.0f,    1.0f, 1.0f,
            0.5f, 0.5f, 0.5f,    1.0f, 0.0f, 1.0f,    0.0f, 1.0f, 0.0f,    0.0f, 1.0f,
            
            0.5f, -0.5f, -0.5f,        1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,    0.0f, 0.0f,
            -0.5f, -0.5f, -0.5f,    1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,    1.0f, 0.0f,
            -0.5f, -0.5f, 0.5f,        1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,    1.0f, 1.0f,
            0.5f, -0.5f, 0.5f,        1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,    0.0f, 1.0f
        };
        
        
        
        glGenVertexArrays(1, &aav_gVao_cube);
        glBindVertexArray(aav_gVao_cube);
        
        //Record Sphere
        glGenBuffers(1, &aav_gVbo_cubeAll);
        glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_cubeAll);
        glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeVertex), aav_cubeVertex, GL_STATIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
        
        //color
        glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
        glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
        
        //normals
        glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
        glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
        
        //texture
        glVertexAttribPointer(AAV_ATTRIBUTE_TEXCORD, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(9 * sizeof(float)));
        glEnableVertexAttribArray(AAV_ATTRIBUTE_TEXCORD);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        
       
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        //
        glClearColor(0.0f,0.0f,0.0f,1.0f);
        
        aav_textureNew = [self loadTextureFromBmpFile:@"marble" :@"bmp"];
        if(aav_textureNew == 0)
        {
            [self release];
        }
       
        aav_PerspectiveProjectionMatrix = mat4::identity();
        
        // Gestures
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action: @selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; //ake double tap la to single tap manat nahe
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecongnizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self  action:@selector(onLongPress:)];
        [longPressGestureRecongnizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecongnizer];
    }
    
    return(self);
}

-(GLuint)loadTextureFromBmpFile:(NSString *)imageFileName :(NSString *)extension
{
    //code
    
    GLuint aav_texture;
    
    NSString *imageFileNameWithPath = [[NSBundle mainBundle]pathForResource:imageFileName ofType:extension];
    
    UIImage *bmpImage = [[UIImage alloc]initWithContentsOfFile:imageFileNameWithPath];//Get Image Representation of our image file.
    
    CGImageRef cgImage = [bmpImage CGImage];
    
    int imageWidth = (int)CGImageGetWidth(cgImage);
    int imageHeight = (int)CGImageGetHeight(cgImage);
    
    //Get CoreFoundation Representation of Image
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));

    //Convert CFDataRef to void * generic Data
    void *pixels = (void *)CFDataGetBytePtr(imageData); //pixel CFData cha pointer miltoi.
    
    //
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glGenTextures(1, &aav_texture); // Gatu
    glBindTexture(GL_TEXTURE_2D, aav_texture);
    
    //Setting Texture Paramter
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    
    glTexImage2D(GL_TEXTURE_2D,0,
                 GL_RGBA,imageWidth,imageHeight,
                 0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
    glGenerateMipmap(GL_TEXTURE_2D);
    
   // CFRelease(imageData);
    
    return aav_texture;
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}
/*
-(void)drawRect:(CGRect)rect
{
    // Drawing code
   
}
*/

-(void)layoutSubviews
{
    //code
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    //storage dena
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
    GLint width;
    GLint height;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is Not Complete.\n");
        [self uninitialize];
    }
    
    if(height < 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
    
    
    [self drawView];
}

-(void)drawView
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer); //patak program
    //glUseProgram code start from here
    
    mat4 aav_rotationMatrixA;
    mat4 aav_rotationMatrixX;
    mat4 aav_rotationMatrixY;
    mat4 aav_rotationMatrixZ;
    
    //fuction declartion
    void update();
    
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //triangle
    glUseProgram(aav_gShaderProgramObject);
    
    //Light
    glUniform3fv(aav_laUniform,1,(GLfloat *)aav_lightAmbiant);                // la
    glUniform3fv(aav_ldUniform,1,(GLfloat *)aav_lightDiffuse);                // ld
    glUniform3fv(aav_lsUniform,1,(GLfloat *)aav_lightSpecular);                // ls
    glUniform4fv(aav_lightPositionUniform,1,(GLfloat *)aav_lightPosition);    //lightPosition
    
    glUniform1f(aav_kShininessUniform,128.0f);
    
    
    //OpenGL Drawing
    mat4 aav_modelMateix = mat4::identity();
    mat4 aav_viewMatrix = mat4::identity();
    //mat4 aav_rotationMatrix = mat4::identity();
    mat4 aav_scaleMatrix = mat4::identity();
    
    mat4 aav_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    aav_rotationMatrixX = vmath::rotate(aav_rotation, 1.0f, 0.0f, 0.0f);
    aav_rotationMatrixY = vmath::rotate(aav_rotation, 0.0f, 1.0f, 0.0f);
    aav_rotationMatrixZ = vmath::rotate(aav_rotation, 0.0f, 0.0f, 1.0f);
    
    aav_rotationMatrixA = aav_rotationMatrixX * aav_rotationMatrixY * aav_rotationMatrixZ;
    
    aav_modelMateix = aav_translateMatrix *aav_scaleMatrix * aav_rotationMatrixA;
    
    glUniformMatrix4fv(aav_modelMatrixUniform,1,GL_FALSE, aav_modelMateix);
    glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
    glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, aav_textureNew);
    glUniform1i(aav_textureSamplerUniform, 0);
    
    
    glBindVertexArray(aav_gVao_cube);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
    glBindVertexArray(0);
    
    aav_rotation = aav_rotation + 1.0f;
    if (aav_rotation > 360)
    {
        aav_rotation = 0.0f;
    }
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}


-(void)startAnimation
{
    //code
    if(isAnimationg == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimationg = YES;
    }
    
}

-(void)stopAnimation
{
    //code
    if(isAnimationg == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimationg = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self uninitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
}

-(void)uninitialize
{
    //code
    if (aav_gVao_cube)
    {
        glDeleteVertexArrays(1, &aav_gVao_cube);
        aav_gVao_cube = 0;
    }
    
    if (aav_gVbo_cubeAll)
    {
        glDeleteBuffers(1, &aav_gVbo_cubeAll);
        aav_gVbo_cubeAll = 0;
    }
    
    glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glDeleteShader(aav_gVertexShaderObject);
    aav_gVertexShaderObject = 0;
    glDeleteShader(aav_gFragmentShaderObject);
    aav_gFragmentShaderObject = 0;
    
    glDeleteProgram(aav_gShaderProgramObject);
    aav_gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}

-(void)dealloc
{
    //code
    [self uninitialize];
    [super dealloc];
}

@end
