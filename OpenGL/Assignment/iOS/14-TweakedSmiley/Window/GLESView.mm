//
//  MyView.m
//  Window
//
//  Created by OM-SAI on 03/07/21.
//  Copyright © 2021 OM-SAI. All rights reserved.
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"


using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};


GLuint aav_gVertexShaderObject;
GLuint aav_gFragmentShaderObject;
GLuint aav_gShaderProgramObject;

GLuint aav_gVao_pyramid;
GLuint aav_gVbo_position_pyramid;
GLuint aav_gVbo_color_pyramid;

GLuint aav_gVao_cube;
GLuint aav_gVbo_position_cube;
GLuint aav_gVbo_color_cube;

GLuint aav_mvp_MatrixUniform;


mat4 aav_PerspectiveProjectionMatrix;

//Rotation Angle
GLfloat aav_anglePyramid = 0.0f;
GLfloat aav_angleCube = 0.0f;

@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimationg;
    
    //shader object
    GLuint aav_gVertexShaderObject;
    GLuint aav_gFragmentShaderObject;
    GLuint aav_gShaderProgramObject;
    
    
    GLuint aav_gVao_cube;
    GLuint aav_gVbo_position_cube;
    GLuint aav_gVbo_texture_cube;
    
    GLuint aav_mvp_MatrixUniform;
    GLuint aav_textureSamplerUniform;
    
    //Texture
    GLuint aav_stone_texture;
    GLuint aav_kundali_texture;
    
    GLuint aav_textureKeyPress;
    
    GLuint aav_keySmilePress;
    
    GLuint count ;
    mat4 aav_PerspectiveProjectionMatrix;
    
}

-(id)initWithFrame:(CGRect)frame
{
    //code
    count = 0;
    self = [super initWithFrame:frame];
    
    if(self)
    {
        //OpenGL code
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil]];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context Creation Failed.\n");
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        //render to texture
        glGenFramebuffers(1,&defaultFramebuffer );
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        //storage dena
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        //vatanya chi sange
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        //12 step : buffer width , height set
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //16 depth buffer sathi
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("FrameBuffer is Not Complete.\n");
            [self uninitialize];
            return(nil);
        }
        
        printf("%s\n",glGetString(GL_RENDERER));
        printf("%s\n",glGetString(GL_VERSION));
        printf("%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        animationFrameInterval = 60; //iOS 8.2 take bufeault
        isAnimationg = NO;
        
        //vertex shader source code start from here
        //**VERTEX SHADER***
        aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* aav_vertexShaserSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec2 vTexCoord;" \
        "uniform mat4 aav_u_mvpMatrix;" \
        "out vec2 aav_out_TexCoord;" \
        "void main(void)" \
        "{" \
        "gl_Position = aav_u_mvpMatrix * vPosition;" \
        "aav_out_TexCoord = vTexCoord;" \
        "}";
        glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
        
        //compile shader , Error checking of Compilation
        glCompileShader(aav_gVertexShaderObject);
        
        GLint aav_infoLogLength = 0;
        GLint aav_shaderCompiledStatus = 0;
        char* aav_szBuffer = NULL;
        
        glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
        
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
                                       &aav_written, aav_szBuffer);
                    printf( "Vertex Shader Compilation Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                    
                }
            }
        }
        
        //**FRAGMENT SHADER**
        aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "in vec2 aav_out_TexCoord;" \
        "uniform sampler2D u_texture_sampler;" \
        "uniform int u_texCoredFlag;" \
        "out vec4 aav_FragColor;" \
        "void main(void)" \
        "{" \
        "    if(u_texCoredFlag == 0)" \
        "    {"\
        "        aav_FragColor = vec4(1.0f,1.0f,1.0f,1.0f);" \
        "    }"\
        "    else " \
        "    {"\
        "        aav_FragColor = texture(u_texture_sampler,aav_out_TexCoord);" \
        "    }" \
        "}";
        glShaderSource(aav_gFragmentShaderObject, 1,
                       (const GLchar**)&fragmentShaderSourceCode,NULL);
        //compile shader
        glCompileShader(aav_gFragmentShaderObject);
        
        aav_szBuffer = NULL;
        aav_infoLogLength = 0;
        aav_shaderCompiledStatus = 0;
        
        glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS,
                      &aav_shaderCompiledStatus);
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
                                       &aav_written, aav_szBuffer);
                    printf("Fragment Shader Compilation Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                    
                }
            }
        }
        
        //**SHADER PROGRAM**
        //Create
        aav_gShaderProgramObject = glCreateProgram();
        
        glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
        glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_TEXCORD, "vTexCoord");
        
        glLinkProgram(aav_gShaderProgramObject);
        
        aav_infoLogLength = 0;
        GLint aav_shaderProgramLinkStatus = 0;
        aav_szBuffer = NULL;
        
        glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
        if (aav_shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_infoLogLength > 0)
                {
                    GLsizei aav_aav_written;
                    glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
                                        &aav_aav_written, aav_szBuffer);
                    printf( "Shader Program Link Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                   
                }
            }
        }
        
        //Post Linking Information
        aav_mvp_MatrixUniform         = glGetUniformLocation(aav_gShaderProgramObject, "aav_u_mvpMatrix");
        
        aav_textureSamplerUniform     = glGetUniformLocation(aav_gShaderProgramObject, "u_texture_sampler");
        
        aav_textureKeyPress             = glGetUniformLocation(aav_gShaderProgramObject, "u_texCoredFlag");
        
        const GLfloat aav_cubeVertex[] =
        {
            1.0f,1.0f,0.0f,
            -1.0f,1.0f,0.0f,
            -1.0f,-1.0f,0.0f,
            1.0f,-1.0f,0.0f,
        };
        
        const GLfloat aav_cubeTexCoord[] =
        {
            1.0f,1.0f,
            0.0f,1.0f,
            0.0f,0.0f,
            1.0f,0.0f,
        };
        
        //Record Square
        glGenVertexArrays(1,&aav_gVao_cube);
        glBindVertexArray(aav_gVao_cube);
        
        glGenBuffers(1,&aav_gVbo_position_cube);
        glBindBuffer(GL_ARRAY_BUFFER,aav_gVbo_position_cube);
        glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeVertex), aav_cubeVertex,GL_STATIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glGenBuffers(1, &aav_gVbo_texture_cube);
        glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_texture_cube);
        glBufferData(GL_ARRAY_BUFFER, 4*2*sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_TEXCORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_TEXCORD);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Record Off / Pause
        glBindVertexArray(0);
        
       
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        //
        glClearColor(0.0f,0.0f,0.0f,1.0f);
        
        aav_kundali_texture = [self loadTextureFromBmpFile:@"Smiley" :@"bmp"];
        if(aav_kundali_texture == 0)
        {
            [self release];
        }
        aav_PerspectiveProjectionMatrix = mat4::identity();
        
        // Gestures
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action: @selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:2];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; //ake double tap la to single tap manat nahe
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecongnizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self  action:@selector(onLongPress:)];
        [longPressGestureRecongnizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecongnizer];
    }
    
    return(self);
}

-(GLuint)loadTextureFromBmpFile:(NSString *)imageFileName :(NSString *)extension
{
    //code
    
    GLuint aav_texture;
    
    NSString *imageFileNameWithPath = [[NSBundle mainBundle]pathForResource:imageFileName ofType:extension];
    
    UIImage *bmpImage = [[UIImage alloc]initWithContentsOfFile:imageFileNameWithPath];//Get Image Representation of our image file.
    
    CGImageRef cgImage = [bmpImage CGImage];
    
    int imageWidth = (int)CGImageGetWidth(cgImage);
    int imageHeight = (int)CGImageGetHeight(cgImage);
    
    //Get CoreFoundation Representation of Image
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));

    //Convert CFDataRef to void * generic Data
    void *pixels = (void *)CFDataGetBytePtr(imageData); //pixel CFData cha pointer miltoi.
    
    //
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glGenTextures(1, &aav_texture); // Gatu
    glBindTexture(GL_TEXTURE_2D, aav_texture);
    
    //Setting Texture Paramter
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    
    glTexImage2D(GL_TEXTURE_2D,0,
                 GL_RGBA,imageWidth,imageHeight,
                 0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
    glGenerateMipmap(GL_TEXTURE_2D);
    
   // CFRelease(imageData);
    
    return aav_texture;
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}
/*
-(void)drawRect:(CGRect)rect
{
    // Drawing code
   
}
*/

-(void)layoutSubviews
{
    //code
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    //storage dena
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
    GLint width;
    GLint height;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is Not Complete.\n");
        [self uninitialize];
    }
    
    if(height < 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
    
    
    [self drawView];
}

-(void)drawView
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer); //patak program
    //glUseProgram code start from here
    
    GLfloat aav_texCord[8] = {0.0f};
    mat4 aav_scaMatrix;
    mat4 aav_translateMatrix;
    
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //triangle
    glUseProgram(aav_gShaderProgramObject);
    
    //square
    mat4 aav_modelViewMateix = mat4::identity();
    mat4 aav_modelViewProjectMatrix = mat4::identity();
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    
    aav_modelViewMateix = aav_translateMatrix;
    aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
    
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, aav_kundali_texture);
    glUniform1i(aav_textureSamplerUniform, 0);
    glBindVertexArray(aav_gVao_cube);
    
    if (aav_keySmilePress == 0)
    {
        glUniform1i(aav_textureKeyPress, 0);
    }
    
    if (aav_keySmilePress == 1)
    {
        aav_texCord[0] = 1.0f;
        aav_texCord[1] = 1.0f;
        aav_texCord[2] = 0.0f;
        aav_texCord[3] = 1.0f;
        aav_texCord[4] = 0.0f;
        aav_texCord[5] = 0.0f;
        aav_texCord[6] = 1.0f;
        aav_texCord[7] = 0.0f;
        glUniform1i(aav_textureKeyPress, 1);
    }
    else if (aav_keySmilePress == 2 )
    {
        aav_texCord[0] = 0.5f;
        aav_texCord[1] = 0.5f;
        aav_texCord[2] = 0.0f;
        aav_texCord[3] = 0.5f;
        aav_texCord[4] = 0.0f;
        aav_texCord[5] = 0.0f;
        aav_texCord[6] = 0.5f;
        aav_texCord[7] = 0.0f;
        glUniform1i(aav_textureKeyPress, 1);
    }
    else if (aav_keySmilePress == 3)
    {
        aav_texCord[0] = 2.0f;
        aav_texCord[1] = 2.0f;
        aav_texCord[2] = 0.0f;
        aav_texCord[3] = 2.0f;
        aav_texCord[4] = 0.0f;
        aav_texCord[5] = 0.0f;
        aav_texCord[6] = 2.0f;
        aav_texCord[7] = 0.0f;
        glUniform1i(aav_textureKeyPress, 1);
    }
    else if (aav_keySmilePress == 4)
    {
        aav_texCord[0] = 0.5f;
        aav_texCord[1] = 0.5f;
        aav_texCord[2] = 0.5f;
        aav_texCord[3] = 0.5f;
        aav_texCord[4] = 0.5f;
        aav_texCord[5] = 0.5f;
        aav_texCord[6] = 0.5f;
        aav_texCord[7] = 0.5f;
        glUniform1i(aav_textureKeyPress, 1);
    }
    
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_texture_cube);
    glBufferData(GL_ARRAY_BUFFER, 4 * 2 * sizeof(GLfloat), aav_texCord, GL_DYNAMIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_TEXCORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_TEXCORD);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}


-(void)startAnimation
{
    //code
    if(isAnimationg == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimationg = YES;
    }
    
}

-(void)stopAnimation
{
    //code
    if(isAnimationg == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimationg = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
    aav_keySmilePress = aav_keySmilePress + 1;
    if(aav_keySmilePress >= 5)
    {
        aav_keySmilePress = 0;
    }
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
   
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    printf("onSwipe\n");
    [self uninitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
}

-(void)uninitialize
{
    //code
    if (aav_gVao_cube)
    {
        glDeleteVertexArrays(1, &aav_gVao_cube);
        aav_gVao_cube = 0;
    }
    
    if (aav_gVbo_position_cube)
    {
        glDeleteBuffers(1, &aav_gVbo_position_cube);
        aav_gVbo_position_cube = 0;
    }
    
    if (aav_gVbo_texture_cube)
    {
        glDeleteBuffers(1, &aav_gVbo_texture_cube);
        aav_gVbo_texture_cube = 0;
    }
    
    
    glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glDeleteShader(aav_gVertexShaderObject);
    aav_gVertexShaderObject = 0;
    glDeleteShader(aav_gFragmentShaderObject);
    aav_gFragmentShaderObject = 0;
    
    glDeleteProgram(aav_gShaderProgramObject);
    aav_gShaderProgramObject = 0;
    
    if (aav_stone_texture)
    {
        glDeleteTextures(1, &aav_stone_texture);
    }
    
    if (aav_kundali_texture)
    {
        glDeleteTextures(1, &aav_kundali_texture);
    }
    
    glUseProgram(0);
    
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}

-(void)dealloc
{
    //code
    [self uninitialize];
    [super dealloc];
}

@end
