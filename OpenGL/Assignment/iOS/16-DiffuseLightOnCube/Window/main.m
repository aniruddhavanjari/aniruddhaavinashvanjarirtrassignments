//
//  main.m
//  Window
//
//  Created by OM-SAI on 03/07/21.
//  Copyright © 2021 OM-SAI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    //code
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    int ret = UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    
    [pool release];
    
    return(ret);
}
