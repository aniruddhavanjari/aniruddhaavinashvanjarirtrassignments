//
//  MyView.m
//  Window
//
//  Created by OM-SAI on 03/07/21.
//  Copyright © 2021 OM-SAI. All rights reserved.
//
#import <OpenGLES/ES3/gl.h>
#import <OpenGLES/ES3/glext.h>

#import "GLESView.h"
#import "vmath.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};
GLfloat aav_anglePyramid = 0.0f;
GLfloat aav_angleCube = 0.0f;

bool aav_bAnimationflag = false;
bool aav_bLight = false;

@implementation GLESView
{
    @private
    EAGLContext *eaglContext;
    
    GLuint defaultFramebuffer;
    GLuint colorRenderbuffer;
    GLuint depthRenderbuffer;
    
    id displayLink;
    NSInteger animationFrameInterval;
    BOOL isAnimationg;
    
    GLuint aav_gVertexShaderObject;
    GLuint aav_gFragmentShaderObject;
    GLuint aav_gShaderProgramObject;
    
    GLuint aav_Vao_cube;
    GLuint aav_Vbo_cube_position;
    GLuint aav_Vbo_cube_normals;
    
    mat4 aav_PerspectiveProjectionMatrix;
    
    GLuint aav_modelViewMatrixUniform;
    GLuint aav_modelViewPerspectiveProjectionUniform;
    
    GLuint aav_LkeyPressedUniform;
    
    GLuint aav_ldUniform;
    GLuint aav_kdUniform;
    GLuint aav_lightPositionUniform;
    
}

-(id)initWithFrame:(CGRect)frame
{
    //code
    self = [super initWithFrame:frame];
    
    if(self)
    {
        //OpenGL code
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)[super layer];
        [eaglLayer setOpaque:YES];
        [eaglLayer setDrawableProperties:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:NO],kEAGLDrawablePropertyRetainedBacking,
                                          kEAGLColorFormatRGBA8,kEAGLDrawablePropertyColorFormat, nil]];
        
        eaglContext = [[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];
        if(eaglContext == nil)
        {
            printf("OpenGL-ES Context Creation Failed.\n");
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];
        
        //render to texture
        glGenFramebuffers(1,&defaultFramebuffer );
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);
        glGenRenderbuffers(1,&colorRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
        //storage dena
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];
        
        //vatanya chi sange
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderbuffer);
        
        //12 step : buffer width , height set
        GLint backingWidth;
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &backingWidth);
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        //16 depth buffer sathi
        glGenRenderbuffers(1, &depthRenderbuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, backingWidth, backingHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
        
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            printf("FrameBuffer is Not Complete.\n");
            [self uninitialize];
            return(nil);
        }
        
        printf("%s\n",glGetString(GL_RENDERER));
        printf("%s\n",glGetString(GL_VERSION));
        printf("%s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        animationFrameInterval = 60; //iOS 8.2 take bufeault
        isAnimationg = NO;
        
        

        //vertex shader source code start from here
        //**VERTEX SHADER***
        aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
        const GLchar* aav_vertexShaserSourceCode =
        "#version 300 es" \
        "\n" \
        "precision mediump int;"\
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_modelView_matrix;" \
        "uniform mat4 u_projection_matrix;" \
        "uniform int u_lKeyPressed;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_kd;" \
        "uniform vec4 u_lightPosistion;" \
        "out vec3 defuse_light;"
        "void main(void)" \
        "{" \
        "if(u_lKeyPressed == 1)" \
        "{" \
        "    vec4 eyeCordinate = u_modelView_matrix * vPosition;" \
        "    mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));" \
        "    vec3 tenorm = normalize(normal_matrix * vNormal); " \
        "    vec3 s = normalize(vec3(u_lightPosistion - eyeCordinate));" \
        "    defuse_light = u_ld * u_kd * (max(dot(s,tenorm),0.0));" \
        "}" \
        "gl_Position = u_projection_matrix *u_modelView_matrix* vPosition;" \
        "}";
        glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
        
        //compile shader , Error checking of Compilation
        glCompileShader(aav_gVertexShaderObject);
        
        GLint aav_infoLogLength = 0;
        GLint aav_shaderCompiledStatus = 0;
        char* aav_szBuffer = NULL;
        
        glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
        
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
                                       &aav_written, aav_szBuffer);
                    printf("Vertex Shader Compilation Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                   
                }
            }
        }
        
        //**FRAGMENT SHADER**
        aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
        const GLchar *fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;"\
        "vec4 color;"
        "in vec3 defuse_light;" \
        "uniform int u_lKeyPressed;" \
        "out vec4 aav_FragColor;" \
        "void main(void)" \
        "{" \
        "    if(u_lKeyPressed == 1)" \
        "    {" \
        "        color = vec4(defuse_light,1.0);"\
        "    }" \
        "    else " \
        "    {" \
        "        color = vec4(1.0f,1.0f,1.0f,1.0f);" \
        "    }" \
        " aav_FragColor = color; " \
        "}";
        glShaderSource(aav_gFragmentShaderObject, 1,
                       (const GLchar**)&fragmentShaderSourceCode,NULL);
        //compile shader
        glCompileShader(aav_gFragmentShaderObject);
        
        
        aav_szBuffer = NULL;
        aav_infoLogLength = 0;
        aav_shaderCompiledStatus = 0;
        
        glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS,
                      &aav_shaderCompiledStatus);
        if (aav_shaderCompiledStatus == GL_FALSE)
        {
            glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_szBuffer != NULL)
                {
                    GLsizei aav_written;
                    glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
                                       &aav_written, aav_szBuffer);
                    printf("Fragment Shader Compilation Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                }
            }
        }
        
        //**SHADER PROGRAM**
        //Create
        aav_gShaderProgramObject = glCreateProgram();
        
        glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
        glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
        
        glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");
        
        glLinkProgram(aav_gShaderProgramObject);
        
        aav_infoLogLength = 0;
        GLint aav_shaderProgramLinkStatus = 0;
        aav_szBuffer = NULL;
        
        glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
        if (aav_shaderProgramLinkStatus == GL_FALSE)
        {
            glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                aav_szBuffer = (char*)malloc(aav_infoLogLength);
                if (aav_infoLogLength > 0)
                {
                    GLsizei aav_aav_written;
                    glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
                                        &aav_aav_written, aav_szBuffer);
                    printf( "Shader Program Link Log: %s\n", aav_szBuffer);
                    free(aav_szBuffer);
                    [self release];
                  
                }
            }
        }
        
        //Post Linking Information
        aav_modelViewMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_modelView_matrix");
        aav_modelViewPerspectiveProjectionUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_projection_matrix");
        
        aav_LkeyPressedUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_lKeyPressed");
        
        aav_ldUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ld");
        aav_kdUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kd");
        aav_lightPositionUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_light_position");;
        
        //vertices array declation
        const GLfloat aav_cubeVertex[] =
        {
            1.0f,1.0f,1.0f,
            -1.0f,1.0f,1.0f,
            -1.0f,-1.0f,1.0f,
            1.0f,-1.0f,1.0f,
            
            1.0f, 1.0f, -1.0f,
            1.0f, 1.0f, 1.0f,
            1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, -1.0f,
            
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            1.0f, -1.0f, -1.0f,
            
            -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, 1.0f,
            -1.0f, -1.0f, 1.0f,
            -1.0f, -1.0f, -1.0f,
            
            1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, -1.0f,
            -1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f,
            
            1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f, 1.0f,
            1.0f, -1.0f, 1.0f
        };
        
        const GLfloat aav_cubeNormals[] =
        {
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            
            1.0, 0.0f, 0.0f,
            1.0, 0.0f, 0.0f,
            1.0, 0.0f, 0.0f,
            1.0, 0.0f, 0.0f,
            
            0.0f, 0.0, -1.0f,
            0.0f, 0.0, -1.0f,
            0.0f, 0.0, -1.0f,
            0.0f, 0.0, -1.0f,
            
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            -1.0f, 0.0f, 0.0f,
            
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,
            
            0.0, -1.0, 0.0f,
            0.0, -1.0, 0.0f,
            0.0, -1.0, 0.0f,
            0.0, -1.0, 0.0f
        };
        
        
        //Record Square
        glGenVertexArrays(1,&aav_Vao_cube);
        glBindVertexArray(aav_Vao_cube);
        
        glGenBuffers(1,&aav_Vbo_cube_position);
        glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_cube_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeVertex), aav_cubeVertex,GL_STATIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        glGenBuffers(1, &aav_Vbo_cube_normals);
        glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_cube_normals);
        glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeNormals), aav_cubeNormals, GL_STATIC_DRAW);
        glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        //Record Off / Pause
        glBindVertexArray(0);;
        
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);
        
        //
        glClearColor(0.0f,0.0f,0.0f,1.0f);
        
        aav_PerspectiveProjectionMatrix = mat4::identity();
        

        
        // Gestures
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action: @selector(onSingleTap:)];
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];
        
        UITapGestureRecognizer *doubleTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];
        [doubleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];
        
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; //ake double tap la to single tap manat nahe
        
        UISwipeGestureRecognizer *swipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onSwipe:)];
        [swipeGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        UILongPressGestureRecognizer *longPressGestureRecongnizer = [[UILongPressGestureRecognizer alloc]initWithTarget:self  action:@selector(onLongPress:)];
        [longPressGestureRecongnizer setDelegate:self];
        [self addGestureRecognizer:longPressGestureRecongnizer];
    }
    
    return(self);
}

+(Class)layerClass
{
    //code
    return([CAEAGLLayer class]);
}
/*
-(void)drawRect:(CGRect)rect
{
    // Drawing code
   
}
*/

-(void)layoutSubviews
{
    //code
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    //storage dena
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer *)[self layer]];
    
    GLint width;
    GLint height;
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderbuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderbuffer);
    
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        printf("FrameBuffer is Not Complete.\n");
        [self uninitialize];
    }
    
    if(height < 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
    
    
    
    [self drawView];
}

-(void)drawView
{
    //code
    [EAGLContext setCurrentContext:eaglContext];
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer); //patak program
    //glUseProgram code start from here
    
    GLfloat aav_lightPosition[] = {0.0f,0.0f,2.0f,1.0f};
    
    mat4 aav_scaMatrix;
    mat4 aav_translateMatrix;
    mat4 aav_rotationMatrixA;
    mat4 aav_rotationMatrixX;
    mat4 aav_rotationMatrixY;
    mat4 aav_rotationMatrixZ;
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //triangle
    glUseProgram(aav_gShaderProgramObject);
    
    if (aav_bLight == true)
    {
        //Light Enable
        glUniform1i(aav_LkeyPressedUniform,1);
        glUniform3f(aav_ldUniform,1.0f,1.0f,1.0f); // color light
        glUniform3f(aav_kdUniform,0.5f,0.5f,0.5f); // Gray Material
        glUniform4fv(aav_lightPositionUniform, 1,(GLfloat *)aav_lightPosition); // Light Position
    }
    else
    {
        glUniform1i(aav_LkeyPressedUniform, 0);
    }
    
    //OpenGL Drawing
    mat4 aav_modelViewMateix = mat4::identity();
    
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    aav_rotationMatrixX = vmath::rotate(aav_angleCube, 1.0f, 0.0f, 0.0f);
    aav_rotationMatrixY = vmath::rotate(aav_angleCube, 0.0f, 1.0f, 0.0f);
    aav_rotationMatrixZ = vmath::rotate(aav_angleCube, 0.0f, 0.0f, 1.0f);
    aav_rotationMatrixA = aav_rotationMatrixX * aav_rotationMatrixY * aav_rotationMatrixZ;
    
    aav_modelViewMateix = aav_translateMatrix *aav_rotationMatrixA;
    
    glUniformMatrix4fv(aav_modelViewMatrixUniform, 1, GL_FALSE, aav_modelViewMateix);
    glUniformMatrix4fv(aav_modelViewPerspectiveProjectionUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
    
    glBindVertexArray(aav_Vao_cube);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
  
    aav_angleCube = aav_angleCube + 1.00f;
    if(aav_angleCube >= 360.0f)
        aav_angleCube = 0.0f;
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderbuffer);
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];
}


-(void)startAnimation
{
    //code
    if(isAnimationg == NO)
    {
        displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView)];
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        [displayLink addToRunLoop: [NSRunLoop currentRunLoop] forMode: NSDefaultRunLoopMode];
        
        isAnimationg = YES;
    }
    
}

-(void)stopAnimation
{
    //code
    if(isAnimationg == YES)
    {
        [displayLink invalidate];
        displayLink = nil;
        
        isAnimationg = NO;
    }
}

-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    //code
   
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    //code
    if (aav_bLight == false)
    {
        aav_bLight = true;
    }
    else
    {
        aav_bLight = false;
    }
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    //code
    [self uninitialize];
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    //code
}

-(void)uninitialize
{
    //code
    if (aav_Vao_cube)
    {
        glDeleteVertexArrays(1, &aav_Vao_cube);
        aav_Vao_cube = 0;
    }
    
    if (aav_Vbo_cube_position)
    {
        glDeleteBuffers(1, &aav_Vbo_cube_position);
        aav_Vbo_cube_position = 0;
    }
    
    if (aav_Vbo_cube_normals)
    {
        glDeleteBuffers(1, &aav_Vbo_cube_normals);
        aav_Vbo_cube_normals = 0;
    }
    
    
    glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glDeleteShader(aav_gVertexShaderObject);
    aav_gVertexShaderObject = 0;
    glDeleteShader(aav_gFragmentShaderObject);
    aav_gFragmentShaderObject = 0;
    
    glDeleteProgram(aav_gShaderProgramObject);
    aav_gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    
    if(depthRenderbuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderbuffer);
        depthRenderbuffer = 0;
    }
    
    if(colorRenderbuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderbuffer);
        colorRenderbuffer = 0;
    }
    
    if(defaultFramebuffer)
    {
        glDeleteRenderbuffers(1, &defaultFramebuffer);
        defaultFramebuffer = 0;
    }
    
    if(eaglContext)
    {
        if([EAGLContext currentContext] == eaglContext)
        {
            [EAGLContext setCurrentContext:nil];
            [eaglContext release];
            eaglContext = nil;
        }
    }
}

-(void)dealloc
{
    //code
    [self uninitialize];
    [super dealloc];
}

@end
