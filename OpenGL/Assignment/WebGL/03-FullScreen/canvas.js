var context = null;
var canvas = null;
function main()
{
	//Step1 
	//get Canvas from DOM
	canvas = document.getElementById("AAV"); //tpe Inferance 
	
	if(!canvas)
	{
		console.log("Optaining canvas Failed \n");
	}
	else
	{
		console.log("Optaining Canvas Sucess\n");
	}

	//retrive the width and height of canvas 
								//position argument
	console.log("canvas width  = "+canvas.width+" canvas height = "+canvas.height+"\n");
	//console.log(" canvas height = "+canvas.height);

	//get Drawing context from the canvas 
	context = canvas.getContext("2d");

	if(!context)
	{
		console.log("Optaing  context Failed \n");
	}
	else
	{
		console.log("Optaining context Sucess\n");
	}

	//paint BackGround by black color
	context.fillStyle = "Black"; // set Function : properties are internal function
	
	context.fillRect(0,0,canvas.width,canvas.height);

	drawText("Hello World!!!");

	window.addEventListener("keydown",keyDown,false);

	window.addEventListener("click",mouseDown,false);
}

function toggleFullScreen()
{
	//Multi Browser complient
	//varaible declaration
	var fullScreen_element = document.fullscreenElement ||
							document.webkitFullscreenElement || 
							document.mozFullScreenElement || 
							document.msFullScreenElement || 
							null;
	
	if(fullScreen_element == null)
	{
		//Full Screen Set Karnara code
		if(canvas.requestFullscreen)
		{
			canvas.requestFullscreen();
		}
		else if(canvas.webkitRequestFullscreen)
		{
			canvas.webkitRequestFullscreen();
		}
		else if(canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		}
		else if(canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.mozcancalFullSreen)
		{
			document.mozcancalFullSreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}
	}
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			drawText("Hello World!!!");
		break;
		default:
			break;
	}
}

function mouseDown()
{
	alert("Mouse Button is Clicked");
}


function drawText(text)
{
		//Center the text
		context.textAlign = "center"; // horizontal Center
		context.textBaseline = "middle"; // vertical center
	
		context.font = "48px san-serif"; //netscapnavigater 
	
		
		//Color
		context.fillStyle = "white";
	
		//display the text
		context.fillText(text,canvas.width/2,canvas.height/2);
}

