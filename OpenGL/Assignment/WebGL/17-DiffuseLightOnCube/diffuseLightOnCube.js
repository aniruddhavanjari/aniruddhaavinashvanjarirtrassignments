//global variable
var aav_gl = null;  // webgl context
var aav_bFullscreen = false;
var aav_canvas_orignal_width;
var aav_canvas_orignal_height;

const WebGLMacros = 
{
    AAV_ATTRIBUTE_VERTEX:0,
    AAV_ATTRIBUTE_COLOR:1,
    AAV_ATTRIBUTE_NORMAL:2,
    AAV_ATTRIBUTE_TEXTURE:3
};


var aav_vertexShaderObject;
var aav_fragmentShaderObject;
var aav_shaderProgramObject;

var aav_vao_cube;
var aav_vbo_position_cube;
var aav_vbo_color_cube;
var aav_vbo_normal_cube;

var aav_mvpUniform;

var perspectiveProjectionMatrix;

var aav_angleCube = 0;

var aav_modelViewMatrixUniform; 
var aav_modelViewPerspectiveProjectionUniform;

var aav_LkeyPressedUniform;

var aav_ldUniform;
var aav_kdUniform;
var aav_lightPositionUniform;

var aav_lightPosition = [0.0,0.0,2.0,1.0];
var aav_lightDiffuse = [1.0,1.0,1.0];
var aav_materialDifuse = [0.5,0.5,0.5];

var aav_bLight = false;
var aav_bAnimationflag = false;

//To start animation: To have requestAnimation() to be called "cross-browser" compatible
var aav_requestAnimationFrame = 
window.requestAnimationFrame || 
window.webkitRequestAnimationFrame || 
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame;

//To stop animation : to have aav_cancelAnimationFrame() to be called "cross-browser" compatible
var aav_cancelAnimationFrame = 
window.aav_cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame || 
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || 
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AAV");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    aav_canvas_orignal_width = canvas.width;
    aav_canvas_orignal_height = canvas.height;

    //register keyboard's keydown event handler
    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",resize,false);

    //initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    //code 
    var aav_fullscreen_element = 
    document.fullscreenElement || 
    document.webkitFullscreenElement || 
    document.mozFullscreenElement || 
    document.msFullscreenElement || 
    null;

    //if not fullscreen
    if(aav_fullscreen_element == null)
    {
        if(canvas.requestFullScree)
            canvas.requestFullScree();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();        
        aav_bFullscreen = true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        aav_bFullscreen =false;
    }
}

function init()
{
    // code 
    // get WebGL 2.0 context 
    aav_gl = canvas.getContext("webgl2");
    if(aav_gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    aav_gl.viewportWidth = canvas.width;
    aav_gl.viewportHeight = canvas.height;

    //vertex shader 
    var aav_vertexShaderSourcedCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "in vec4 vPosition;"+
    "in vec3 vNormal;" +
    "uniform mat4 u_modelView_matrix;"+
    "uniform mat4 u_projection_matrix;"+
	"uniform vec3 u_ld;" +
	"uniform vec3 u_kd;" +
	"uniform vec4 u_lightPosistion;" +
	"out vec3 defuse_light;"+
    "void main(void)"+ 
    "{"+
	"	vec4 eyeCordinate = u_modelView_matrix * vPosition;" +
	"	mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));" +
	"	vec3 tenorm = normalize(normal_matrix * vNormal); " +
	"	vec3 s = normalize(vec3(u_lightPosistion - eyeCordinate));" +
	"	defuse_light = u_ld * u_kd * (max(dot(s,tenorm),0.0));" +
    "   gl_Position = u_projection_matrix *u_modelView_matrix* vPosition;"+
    "}";
    aav_vertexShaderObject = aav_gl.createShader(aav_gl.VERTEX_SHADER);
    aav_gl.shaderSource(aav_vertexShaderObject,aav_vertexShaderSourcedCode);
    aav_gl.compileShader(aav_vertexShaderObject);
    if(aav_gl.getShaderParameter(aav_vertexShaderObject,aav_gl.COMPILE_STATUS)== false)
    {
        var error = aav_gl.getShaderInfoLog(aav_vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragemnt shader
    var aav_fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+ 
    "precision highp float;"+
    "vec4 color;"+
    "in vec3 defuse_light;" +
	"uniform int u_lKeyPressed;" +
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "	if(u_lKeyPressed == 1)" +
    "	{" +
    "		color = vec4(defuse_light,1.0);"+
    "	}" +
    "	else " +
    "	{" +
    "		color = vec4(1.0f,1.0f,1.0f,1.0f);" +
    "	}" +
    "FragColor =color;"+
    "}";
    aav_fragmentShaderObject = aav_gl.createShader(aav_gl.FRAGMENT_SHADER);
    aav_gl.shaderSource(aav_fragmentShaderObject,aav_fragmentShaderSourceCode);
    aav_gl.compileShader(aav_fragmentShaderObject);
    if(aav_gl.getShaderParameter(aav_fragmentShaderObject,aav_gl.COMPILE_STATUS) == false)
    {
        var error = aav_gl.getShaderInfoLog(aav_fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program 
    aav_shaderProgramObject = aav_gl.createProgram();
    aav_gl.attachShader(aav_shaderProgramObject,aav_vertexShaderObject);
    aav_gl.attachShader(aav_shaderProgramObject,aav_fragmentShaderObject);

    //pre-link binding of shader program object with vertex shader attributes
    aav_gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");
    aav_gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_NORMAL,"vNormal");

    //linking 
    aav_gl.linkProgram(aav_shaderProgramObject);
    if(!aav_gl.getProgramParameter(aav_shaderProgramObject,aav_gl.LINK_STATUS))
    {
        var error = aav_gl.getProgramInfoLog(aav_shaderProgramObject);
        if(error.length >  0)
        {
            alert(error);
            uninitialize();
        }
    }

    //get uniform location

    aav_modelViewMatrixUniform =aav_gl.getUniformLocation(aav_shaderProgramObject, "u_modelView_matrix");
	aav_modelViewPerspectiveProjectionUniform = aav_gl.getUniformLocation(aav_shaderProgramObject, "u_projection_matrix");

	aav_LkeyPressedUniform = aav_gl.getUniformLocation(aav_shaderProgramObject, "u_lKeyPressed");
	
	aav_ldUniform = aav_gl.getUniformLocation(aav_shaderProgramObject, "u_ld");
	aav_kdUniform = aav_gl.getUniformLocation(aav_shaderProgramObject, "u_kd");
	aav_lightPositionUniform = aav_gl.getUniformLocation(aav_shaderProgramObject, "u_light_position");;


    // ** vertices , color , shader attribs, vbo initialization***
  
    var aav_cubeVerteis = new Float32Array([
    1.0,1.0,1.0,
	-1.0,1.0,1.0,
	-1.0,-1.0,1.0,
	1.0,-1.0,1.0,

	1.0, 1.0, -1.0,
	1.0, 1.0, 1.0,
	1.0, -1.0, 1.0,
	1.0, -1.0, -1.0,

	1.0, 1.0, -1.0,
	-1.0, 1.0, -1.0,
	-1.0, -1.0, -1.0,
	1.0, -1.0, -1.0,

	-1.0, 1.0, -1.0,
	-1.0, 1.0, 1.0,
	-1.0, -1.0, 1.0,
	-1.0, -1.0, -1.0,

	1.0, 1.0, -1.0,
	-1.0, 1.0, -1.0,
	-1.0, 1.0, 1.0,
	1.0, 1.0, 1.0,

	1.0, -1.0, -1.0,
	-1.0, -1.0, -1.0,
	-1.0, -1.0, 1.0,
	1.0, -1.0, 1.0
    ]);
    
    var aav_cubeNormals = new Float32Array([
        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,
    
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
    
        0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,
        0.0, 0.0, -1.0,
    
        -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,
        -1.0, 0.0, 0.0,
    
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0,
    
        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0,
        0.0, -1.0, 0.0
        ]);
        
    //cube vao
    aav_vao_cube = aav_gl.createVertexArray();
    aav_gl.bindVertexArray(aav_vao_cube);

    //cube vbo vertice
    aav_vbo_position_cube = aav_gl.createBuffer();
    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER, aav_vbo_position_cube);
    aav_gl.bufferData(aav_gl.ARRAY_BUFFER, aav_cubeVerteis, aav_gl.STATIC_DRAW);
    aav_gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
                                            3, // 3 is for x,y,z co-cordinates is our triangle Verteices array
                                            aav_gl.FLOAT,
                                            false,
                                            0,0);

    aav_gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER,null);

    //cube vbo normals
  
    aav_vbo_normal_cube = aav_gl.createBuffer();
    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER, aav_vbo_normal_cube);
    aav_gl.bufferData(aav_gl.ARRAY_BUFFER, aav_cubeNormals, aav_gl.STATIC_DRAW);
    aav_gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_NORMAL,
                                            3, 
                                            aav_gl.FLOAT,
                                            false,
                                            0,0);

    aav_gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_NORMAL);
    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER,null);

    aav_gl.bindVertexArray(null);

    //set clear color 
    aav_gl.clearColor(0.0,0.0,0.0,1.0); //blue 

    aav_gl.enable(aav_gl.DEPTH_TEST);
    aav_gl.depthFunc(aav_gl.LEQUAL);
   
    //inidialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    //code 
    if(aav_bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = aav_canvas_orignal_width;
        canvas.height = aav_canvas_orignal_height;
    }

    //set the viewport to match
    aav_gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);

}

function draw()
{
    //variable declarations
    var aav_modelViewMatrix = mat4.create();
    var aav_transformMatrix = mat4.create();
    var aav_rotationMatrix  = mat4.create(); 

    //var aav_modelViewProjectionMatrix = mat4.create();
    //code 
    aav_gl.clear(aav_gl.COLOR_BUFFER_BIT|  aav_gl.DEPTH_BUFFER_BIT);
    
    aav_gl.useProgram(aav_shaderProgramObject);

    if (aav_bLight == true)
	{
		//Light Enable
        aav_gl.uniform1i(aav_LkeyPressedUniform,1);
        aav_gl.uniform3fv(aav_ldUniform,aav_lightDiffuse); // color light
        aav_gl.uniform3fv(aav_kdUniform,aav_materialDifuse); // Gray Material
        aav_gl.uniform4fv(aav_lightPositionUniform,aav_lightPosition); // Light Position
	}
	else
	{
		aav_gl.uniform1i(aav_LkeyPressedUniform, 0);
	}

    /*********Cube************/
    aav_modelViewMatrix = mat4.create();
    aav_transformMatrix = mat4.create();
    aav_rotationMatrix  = mat4.create(); 
    aav_modelViewProjectionMatrix = mat4.create(); // tayar hi honar and indentity matrix la inidilization pan karnar  

    mat4.translate(aav_transformMatrix,aav_transformMatrix,[0.0,0.0,-6.0]);

    mat4.rotateX(aav_rotationMatrix,aav_rotationMatrix,degToRad(aav_angleCube));
    mat4.rotateY(aav_rotationMatrix,aav_rotationMatrix,degToRad(aav_angleCube));
    mat4.rotateZ(aav_rotationMatrix,aav_rotationMatrix,degToRad(aav_angleCube));
   
    mat4.multiply(aav_modelViewMatrix,aav_transformMatrix,aav_rotationMatrix);

    aav_gl.uniformMatrix4fv(aav_modelViewMatrixUniform, false, aav_modelViewMatrix);

    aav_gl.uniformMatrix4fv(aav_modelViewPerspectiveProjectionUniform, false, perspectiveProjectionMatrix);

    aav_gl.bindVertexArray(aav_vao_cube);

    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,0,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,4,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,8,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,12,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,16,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,20,4);
    aav_gl.drawArrays(aav_gl.TRIANGLE_FAN,24,4);

    aav_gl.bindVertexArray(null);


    aav_gl.useProgram(null);
    
    //animation loop
    if (aav_bAnimationflag == true)
	{
        aav_angleCube = aav_angleCube + 1;
        if(aav_angleCube > 360)
        {
            aav_angleCube = 0;
        } 
    }
    requestAnimationFrame(draw, canvas);
}


function degToRad(degree)
{
    return(degree * Math.PI/180.0);
}


function uninitialize()
{
    //code
       if(aav_vao_cube)
    {
        aav_gl.deleteVertexArary(aav_vao_cube);
        aav_vao_cube = null;
    }

    if(aav_vbo_position_cube)
    {
        aav_gl.deleteBuffer(aav_vbo_position_cube);
        aav_vbo_position_cube = null;
    }

    if(aav_vbo_normal_cube)
    {
        aav_gl.deleteBuffer(aav_vbo_normal_cube);
        aav_vbo_normal_cube = null;
    }

    if(aav_shaderProgramObject)
    {
        if(aav_fragmentShaderObject)
        {
            aav_gl.detachShader(aav_shaderProgramObject,aav_fragmentShaderObject);
            aav_gl.deleteShader(aav_fragmentShaderObject);
            aav_fragmentShaderObject = null;
        }

        if(aav_vertexShaderObject)
        {
            aav_gl.detachShader(aav_shaderProgramObject,aav_vertexShaderObject);
            aav_gl.deleteShader(aav_vertexShaderObject);
            aav_vertexShaderObject = null;
        }

        aav_gl.deleteProgram(aav_shaderProgramObject);
        aav_shaderProgramObject = null;
    }
}

function keyDown(event)
{
    //code
    switch(event.keyCode)
    {
        case 27: // escape 
            //unitialize
            uninitialize();
            //close our application's tab
            window.close(); // may not work in firefox but work in safari and chrome 
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
            case 97:
            case 65:
                    if (aav_bAnimationflag == false)
                    {
                        aav_bAnimationflag = true;
                    }
                    else
                    {
                        aav_bAnimationflag = false;
                    }
            break;
            case 108:
            case 76:
                    if (aav_bLight == false)
                    {
                        aav_bLight = true;
                    }
                    else
                    {
                        aav_bLight = false;
                    }
            break;
            default:
			break;
    }
}

function mouseDown()
{
    //code
}