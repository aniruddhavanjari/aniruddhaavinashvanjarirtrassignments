//global variable
var gl = null;  // webgl context
var aav_bFullscreen = false;
var aav_canvas_orignal_width;
var aav_canvas_orignal_height;

//Matrix Array Creation
var matrix = new Array(10); 
var stacktop = -1;
for (var i = 0; i < 10; i++)
{
    matrix[i] = new Array(16); // for mat4 is Linear Array of 16
}


const WebGLMacros = 
{
    AAV_ATTRIBUTE_VERTEX:0,
    AAV_ATTRIBUTE_COLOR:1,
    AAV_ATTRIBUTE_NORMAL:2,
    AAV_ATTRIBUTE_TEXTURE:3
};


var aav_vertexShaderObject;
var aav_fragmentShaderObject;
var aav_shaderProgramObject;

var aav_vao_cube_square;
var aav_vbo_position_cube_square;
var aav_vbo_color_pyramid_square;

var aav_vao_cube;
var aav_vbo_position_cube;
var aav_vbo_color_pyramid;

var aav_mvpUniform;
var aav_colorUniform;

var perspectiveProjectionMatrix;

var aav_angleCube = 0;

var sphereMesh = null;


var aav_sholderRotate = 0.0;
var aav_elboRotate = 0.0;

//To start animation: To have requestAnimation() to be called "cross-browser" compatible
var aav_requestAnimationFrame = 
window.requestAnimationFrame || 
window.webkitRequestAnimationFrame || 
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame;

//To stop animation : to have aav_cancelAnimationFrame() to be called "cross-browser" compatible
var aav_cancelAnimationFrame = 
window.aav_cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame || 
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || 
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

var aav_color = new Array(3);

var aav_earthRevolution = 0.0;
var aav_giday = 0.0;
var aav_giMoonday = 0.0;
var aav_giMoonyear = 0.0;

// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AAV");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    aav_canvas_orignal_width = canvas.width;
    aav_canvas_orignal_height = canvas.height;

    //register keyboard's keydown event handler
    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",resize,false);

    //initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    //code 
    var aav_fullscreen_element = 
    document.fullscreenElement || 
    document.webkitFullscreenElement || 
    document.mozFullscreenElement || 
    document.msFullscreenElement || 
    null;

    //if not fullscreen
    if(aav_fullscreen_element == null)
    {
        if(canvas.requestFullScree)
            canvas.requestFullScree();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();        
        aav_bFullscreen = true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        aav_bFullscreen =false;
    }
}

function init()
{
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");
    if(gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shader 
    var aav_vertexShaderSourcedCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "uniform mat4 u_mvp_matrix;" +
    "uniform vec3 u_color;" +
     "out vec3 fong_ads_light;" +
    "void main(void)"+ 
    "{" +
     "fong_ads_light = u_color;" +
    "gl_Position = u_mvp_matrix * vPosition;"+
    "}";
    aav_vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(aav_vertexShaderObject,aav_vertexShaderSourcedCode);
    gl.compileShader(aav_vertexShaderObject);
    if(gl.getShaderParameter(aav_vertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(aav_vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragemnt shader
    var aav_fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+ 
    "precision highp float;" +
    "in vec3 fong_ads_light;" +
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "FragColor = vec4(fong_ads_light,1.0f);"+
    "}";
    aav_fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(aav_fragmentShaderObject,aav_fragmentShaderSourceCode);
    gl.compileShader(aav_fragmentShaderObject);
    if(gl.getShaderParameter(aav_fragmentShaderObject,gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(aav_fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program 
    aav_shaderProgramObject = gl.createProgram();
    gl.attachShader(aav_shaderProgramObject,aav_vertexShaderObject);
    gl.attachShader(aav_shaderProgramObject,aav_fragmentShaderObject);

    //pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");

    //linking 
    gl.linkProgram(aav_shaderProgramObject);
    if(!gl.getProgramParameter(aav_shaderProgramObject,gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(aav_shaderProgramObject);
        if(error.length >  0)
        {
            alert(error);
            uninitialize();
        }
    }

    //get MVP uniform location
    aav_mvpUniform = gl.getUniformLocation(aav_shaderProgramObject,"u_mvp_matrix");
    aav_colorUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_color");	
    // ** vertices , color , shader attribs, vbo initialization***
 
    sphereMesh = new Mesh();
    makeSphere(sphereMesh, 1.0, 30,30);
    //makeSphere(sphereMesh, 0.5, 30, 30);
    //makeSphere(sphereMesh, 0.3, 30, 30);
    //set clear color 
    gl.clearColor(0.0,0.0,0.0,1.0); //blue 

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
   
    //inidialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    //code 
    if(aav_bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = aav_canvas_orignal_width;
        canvas.height = aav_canvas_orignal_height;
    }

    //set the viewport to match
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);

}

function draw()
{
    //variable declarations
    var aav_modelViewMatrix = mat4.create();
    var aav_transformMatrix = mat4.create();
    var aav_tempStoreMatrix = mat4.create(); 
    var aav_scaleMatrix = mat4.create();
    var aav_modelViewProjectionMatrix = mat4.create();
    //code 
    gl.clear(gl.COLOR_BUFFER_BIT|  gl.DEPTH_BUFFER_BIT);
    
    gl.useProgram(aav_shaderProgramObject);

    aav_color[0] = 1.0;
    aav_color[1] = 1.0;
    aav_color[2] = 0.0;
    gl.uniform3fv(aav_colorUniform,aav_color);	


    aav_modelViewMatrix = mat4.create();
    aav_transformMatrix = mat4.create();
    aav_rotationMatrix = mat4.create(); 
    aav_scaleMatrix = mat4.create();
    aav_modelViewProjectionMatrix = mat4.create(); // tayar hi honar and indentity matrix la inidilization pan karnar  

    mat4.translate(aav_modelViewMatrix,aav_transformMatrix,[0.0,0.0,-4.0]);
    
    push(aav_modelViewMatrix);

 
    mat4.rotateX(aav_rotationMatrix, aav_rotationMatrix, degToRad(90.0));
    mat4.multiply(aav_modelViewMatrix, aav_modelViewMatrix, aav_rotationMatrix);


    mat4.multiply(aav_modelViewMatrix, aav_modelViewMatrix, aav_scaleMatrix);

    mat4.multiply(aav_modelViewProjectionMatrix, perspectiveProjectionMatrix, aav_modelViewMatrix);

    gl.uniformMatrix4fv(aav_mvpUniform, false, aav_modelViewProjectionMatrix);

    sphereMesh.draw(0);


    //Earth
   // aav_modelViewMatrix = mat4.create();
    aav_color[0] = 0.0;
    aav_color[1] = 1.0;
    aav_color[2] = 1.0;
    gl.uniform3fv(aav_colorUniform, aav_color);	

    aav_rotationMatrix = mat4.create(); 
    pop(aav_modelViewMatrix);
    mat4.translate(aav_transformMatrix, aav_transformMatrix, [-1.7, 0.0, 0.0]);

    mat4.rotateX(aav_rotationMatrix, aav_rotationMatrix, degToRad(-90));
    mat4.multiply(aav_modelViewMatrix, aav_modelViewMatrix, aav_rotationMatrix);

    aav_rotationMatrix = mat4.create(); 
    mat4.rotateZ(aav_rotationMatrix, aav_rotationMatrix, degToRad(aav_earthRevolution));

    mat4.multiply(aav_modelViewMatrix,aav_modelViewMatrix,aav_rotationMatrix);
    mat4.multiply(aav_modelViewMatrix, aav_modelViewMatrix, aav_transformMatrix);
  
   
    push(aav_modelViewMatrix);

    mat4.scale(aav_scaleMatrix, aav_scaleMatrix, [-0.5, -0.5, -0.5]);
    mat4.multiply(aav_modelViewMatrix, aav_modelViewMatrix, aav_scaleMatrix);

    aav_rotationMatrix = mat4.create(); 
    mat4.rotateZ(aav_rotationMatrix, aav_rotationMatrix, degToRad(aav_giday));
    mat4.multiply(aav_modelViewMatrix, aav_modelViewMatrix, aav_rotationMatrix);


     mat4.multiply(aav_modelViewProjectionMatrix, perspectiveProjectionMatrix, aav_modelViewMatrix);

    gl.uniformMatrix4fv(aav_mvpUniform, false, aav_modelViewProjectionMatrix);

    sphereMesh.draw(1);


    //Moon
    aav_color[0] = 1.0;
    aav_color[1] = 1.0;
    aav_color[2] = 1.0;
    gl.uniform3fv(aav_colorUniform, aav_color);	

    aav_rotationMatrix = mat4.create();
    aav_scaleMatrix = mat4.create();
    aav_transformMatrix = mat4.create();
    pop(aav_modelViewMatrix);
    mat4.translate(aav_transformMatrix, aav_transformMatrix, [-1.0, 0.0, 0.0]);

  
    aav_rotationMatrix = mat4.create();
    mat4.rotateZ(aav_rotationMatrix, aav_rotationMatrix, degToRad(aav_giMoonyear));

    mat4.multiply(aav_modelViewMatrix, aav_modelViewMatrix, aav_rotationMatrix);
    mat4.multiply(aav_modelViewMatrix, aav_modelViewMatrix, aav_transformMatrix);
    

    push(aav_modelViewMatrix);

    mat4.scale(aav_scaleMatrix, aav_scaleMatrix, [-0.3, -0.3, -0.3]);
    mat4.multiply(aav_modelViewMatrix, aav_modelViewMatrix, aav_scaleMatrix);

    aav_rotationMatrix = mat4.create();
    mat4.rotateZ(aav_rotationMatrix, aav_rotationMatrix, degToRad(aav_giMoonday));
    mat4.multiply(aav_modelViewMatrix, aav_modelViewMatrix, aav_rotationMatrix);


    mat4.multiply(aav_modelViewProjectionMatrix, perspectiveProjectionMatrix, aav_modelViewMatrix);

    gl.uniformMatrix4fv(aav_mvpUniform, false, aav_modelViewProjectionMatrix);

    sphereMesh.draw(1);

    pop(aav_modelViewMatrix);
    gl.useProgram(null);
    
    //animation loop
    aav_angleCube = aav_angleCube + 1;
    if(aav_angleCube > 360)
    {
       aav_angleCube = 0;
    } 
  
    requestAnimationFrame(draw, canvas);
}

function push(pushMatrix)
{
    if (stacktop >= 10) {
        return (-1);
    }
    stacktop = stacktop + 1;
    for (var i = 0; i < 16; i++)
    {
        matrix[stacktop][i] = pushMatrix[i];
    }
}

function pop(popMatrix)
{
    if (stacktop <= -1)
    {
        return (-1);
    }
    for (var i = 0; i < 16; i++) {
        popMatrix[i] = matrix[stacktop][i];
    }
    stacktop = stacktop - 1;
}

function degToRad(degree)
{
    return(degree * Math.PI/180.0);
}


function uninitialize()
{
    //code
    if(sphere)
    {
        sphere.deallocate();
    }

    if(aav_shaderProgramObject)
    {
        if(aav_fragmentShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject,aav_fragmentShaderObject);
            gl.deleteShader(aav_fragmentShaderObject);
            aav_fragmentShaderObject = null;
        }

        if(aav_vertexShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject,aav_vertexShaderObject);
            gl.deleteShader(aav_vertexShaderObject);
            aav_vertexShaderObject = null;
        }

        gl.deleteProgram(aav_shaderProgramObject);
        aav_shaderProgramObject = null;
    }
}

function keyDown(event)
{
    //code
    switch(event.keyCode)
    {
        case 27: // escape 
            uninitialize();
            window.close(); // may not work in firefox but work in safari and chrome 
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
        default:
            break;
    }

    switch (event.key)
    {
        case 'e':
            aav_earthRevolution = aav_earthRevolution + 1.0;
            break;
        case 'E':
            aav_earthRevolution = aav_earthRevolution - 1.0;
        case 'm':
            aav_giMoonyear = aav_giMoonyear + 1.0;
            break;
        case 'M':
            aav_giMoonyear = aav_giMoonyear - 1.0;
            break;
        case 'n':
            aav_giMoonday = aav_giMoonday + 1.0;
            break;
        case 'N':
            aav_giMoonday = aav_giMoonday - 1.0;
            break;
        case 'd':
            aav_giday = aav_giday + 1.0;
            break;
        case 'D':
            aav_giday = aav_giday - 1.0;
            break;
        default:
            break;
    }
   
}

function mouseDown()
{
    //code
}

//Note : KeyBoardEvent
//https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent#obsolete_properties