//global variable
var gl = null;  // webgl context
var aav_bFullscreen = false;
var aav_canvas_orignal_width;
var aav_canvas_orignal_height;

const WebGLMacros =
{
    AAV_ATTRIBUTE_VERTEX: 0,
    AAV_ATTRIBUTE_COLOR: 1,
    AAV_ATTRIBUTE_NORMAL: 2,
    AAV_ATTRIBUTE_TEXTURE: 3
};


var aav_vertexShaderObject_pf;
var aav_fragmentShaderObject_pf;
var aav_shaderProgramObject_pf;


var aav_vertexShaderObject_pv;
var aav_fragmentShaderObject_pv;
var aav_shaderProgramObject_pv;

var perspectiveProjectionMatrix;

var aav_angleCube = 0;


var aav_bPerFragment = false;
var aav_bPerVertex = true;

var aav_bLight = false;
var aav_bAnimationflag = false;

var sphereMesh = null;

var aav_bLight = false;

//Light Array
var aav_lightAmbiant = [0.0, 0.0, 0.0];	//la
var aav_lightDiffuse = [1.0, 1.0, 1.0];	// ld
var aav_lightSpecular = [1.0, 1.0, 1.0];//ls

var aav_lightPosition = [100.0, 100.0, 100.0, 1.0];

//aav_material Array 
var aav_materialAmbiant = [0.0, 0.0, 0.0]; // ka 
var aav_materialDiffuse = [1.0, 1.0, 1.0];	// kd
var aav_materialSpecular = [1.0, 1.0, 1.0];	// ks


var aav_viewMatrixUniform_pf;
var aav_modelMatrixUniform_pf;
var aav_ProjectionMatrixUniform_pf;

var aav_laUniform_pf = new Array(3);
var aav_ldUniform_pf = new Array(3);
var aav_lsUniform_pf = new Array(3);
var aav_lightPositionUniform_pf = new Array(3);

var aav_kaUniform_pf;
var aav_kdUniform_pf;
var aav_ksUniform_pf;

var aav_kShininessUniform_pf;

var aav_lKeyPressedUniform_pf;

//vertex shader
var aav_viewMatrixUniform_pv;
var aav_modelMatrixUniform_pv;
var aav_ProjectionMatrixUniform_pv;

var aav_laUniform_pv = new Array(3);
var aav_ldUniform_pv = new Array(3);
var aav_lsUniform_pv = new Array(3);
var aav_lightPositionUniform_pv = new Array(3);

var aav_kaUniform_pv;
var aav_kdUniform_pv;
var aav_ksUniform_pv;

var aav_kShininessUniform_pv;

var aav_lKeyPressedUniform_pv;


function aav_Light()
{
    this.aav_lightAmbiant = new Float32Array(3);
    this.aav_lightDiffuse = new Float32Array(3);
    this.aav_lightSpecular = new Float32Array(3);
    this.aav_lightPosition = new Float32Array(4);
};

var aav_light = [];

function aav_Material()
{
    this.aav_materialAmbiant = new Float32Array(3);  // ka 
    this.aav_materialDiffuse = new Float32Array(3);// kd
    this.aav_materialSpecular = new Float32Array(3);// ks
    this.aav_materialShininess;
}

var aav_material;


var aav_lightAngle1 = 0.0;
var aav_lightAngle2 = 0.0;
var aav_lightAngle3 = 0.0;

//To start animation: To have requestAnimation() to be called "cross-browser" compatible
var aav_requestAnimationFrame =
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

//To stop animation : to have aav_cancelAnimationFrame() to be called "cross-browser" compatible
var aav_cancelAnimationFrame =
    window.aav_cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AAV");
    if (!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    aav_canvas_orignal_width = canvas.width;
    aav_canvas_orignal_height = canvas.height;

    //register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    //initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    //code 
    var aav_fullscreen_element =
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullscreenElement ||
        document.msFullscreenElement ||
        null;

    //if not fullscreen
    if (aav_fullscreen_element == null)
    {
        if (canvas.requestFullScree)
            canvas.requestFullScree();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        aav_bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
        aav_bFullscreen = false;
    }
}

function init()
{
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");
    if (gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;


    //PerVertex
    //Vertex Shader
    var aav_vertexShaderSourcedCode_pv =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_projection_matrix;" +

        "uniform int u_lKeyPressed;" +
        "uniform vec3 u_la_pv[3];" +
        "uniform vec3 u_ld_pv[3];" +
        "uniform vec3 u_ls_pv[3];" +
        "uniform vec4 u_lightPosistion_pv[3];" +
        "uniform vec3 u_ka_pv;" +
        "uniform vec3 u_kd_pv;" +
        "uniform vec3 u_ks_pv;" +
        "uniform float u_kShineness_pv;" +
        "vec3 lightDirection[3];" +
        "vec3 reflection_vector[3];" +
        "vec3 ambiant[3];" +
        "vec3 diffuse[3];" +
        "vec3 specular[3];" +
        "out vec3 fong_ads_light_pv;" +
        "void main(void)" +
        "{" +
        "	if(u_lKeyPressed == 1)" +
        "	{" +
        "		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" +
        "		vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
        "		vec3 view_vector = normalize(-eyeCordinate.xyz);" +

        "		for(int i = 0 ; i < 3; i++)" +
        "		{" +
        "			lightDirection[i] = normalize(vec3(u_lightPosistion_pv[i] - eyeCordinate));" +
        "			reflection_vector[i] = reflect(-lightDirection[i],tranformed_normal);" +
        "			ambiant[i] = u_la_pv[i] * u_ka_pv;" +
        "			diffuse[i] = u_ld_pv[i] * u_kd_pv * max(dot(lightDirection[i],tranformed_normal),0.0f);" +
        "			specular[i] = u_ls_pv[i] * u_ks_pv * pow(max(dot(reflection_vector[i],view_vector),0.0f),u_kShineness_pv);" +
        "			fong_ads_light_pv = fong_ads_light_pv + ambiant[i] + diffuse[i] + specular[i];" +
        "		}" +
        "	}" +
        "	else" +
        "	{" +
        "		fong_ads_light_pv = vec3(1.0f,1.0f,1.0f);" +
        "	}" +
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";
    aav_vertexShaderObject_pv = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(aav_vertexShaderObject_pv, aav_vertexShaderSourcedCode_pv);
    gl.compileShader(aav_vertexShaderObject_pv);
    if (gl.getShaderParameter(aav_vertexShaderObject_pv, gl.COMPILE_STATUS) == false) {
        var error = gl.getShaderInfoLog(aav_vertexShaderObject_pv);
        if (error.length > 0) {
            alert(error);
            uninitialize();
        }
    }

    //Pervertex
    //fragemnt shader
    var aav_fragmentShaderSourceCode_pv =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 fong_ads_light_pv;" +
        "out vec4 FragColor_pv;" +
        "void main(void)" +
        "{" +
        "	FragColor_pv = vec4(fong_ads_light_pv,1.0f);" +
        "}";
    aav_fragmentShaderObject_pv = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(aav_fragmentShaderObject_pv, aav_fragmentShaderSourceCode_pv);
    gl.compileShader(aav_fragmentShaderObject_pv);
    if (gl.getShaderParameter(aav_fragmentShaderObject_pv, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(aav_fragmentShaderObject_pv);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }


    //PerFragment
    //vertex shader 
    var aav_vertexShaderSourcedCode_pf =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform int u_lKeyPressed;" +
        "uniform vec4 u_lightPosistion_pf[3];" +
        "out vec3 tranformed_normal_pf;" +
        "out vec3 lightDirection_pf[3];" +
        "out vec3 view_vector_pf;" +
        "void main(void)" +
        "{" +
        "	if(u_lKeyPressed == 1)" +
        "	{" +
        "		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" +
        "		tranformed_normal_pf = (mat3(u_view_matrix* u_model_matrix) * vNormal);" +
        "		view_vector_pf = (-eyeCordinate.xyz);" +
        "		for(int i = 0 ; i < 3 ; i++)" +
        "		{" +
        "			lightDirection_pf[i] = (vec3(u_lightPosistion_pf[i] - eyeCordinate));" +
        "		}" +
        "	}" +
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";
    aav_vertexShaderObject_pf = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(aav_vertexShaderObject_pf, aav_vertexShaderSourcedCode_pf);
    gl.compileShader(aav_vertexShaderObject_pf);
    if (gl.getShaderParameter(aav_vertexShaderObject_pf, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(aav_vertexShaderObject_pf);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //PerFragment
    //fragemnt shader
    var aav_fragmentShaderSourceCode_pf =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "precision mediump int;" +
        "in vec3 tranformed_normal_pf;" +
        "in vec3 lightDirection_pf[3];" +
        "in vec3 view_vector_pf;" +
        "uniform int u_lKeyPressed;" +
        "uniform vec3 u_la_pf[3];" +
        "uniform vec3 u_ld_pf[3];" +
        "uniform vec3 u_ls_pf[3];" +
        "uniform vec3 u_ka_pf;" +
        "uniform vec3 u_kd_pf;" +
        "uniform vec3 u_ks_pf;" +
        "uniform float u_kShineness_pf;" +

        "vec3 normalize_tranformed_normal;" +
        "vec3 normalize_lightDirection[3];" +
        "vec3 normalize_view_vector;" +
        "vec3 reflection_vector[3];" +
        "vec3 ambiant[3];" +
        "vec3 diffuse[3];" +
        "vec3 specular[3];" +

        "out vec4 FragColor_pf;" +
        "vec3 fong_ads_light_pf;" +
        "void main(void)" +
        "{" +
        "	if(u_lKeyPressed == 1)" +
        "	{" +
        "		normalize_tranformed_normal = normalize(tranformed_normal_pf);" +
        "		normalize_view_vector = normalize(view_vector_pf);" +
        "		for(int i = 0; i < 3 ; i++)" +
        "		{" +
        "			normalize_lightDirection[i] = normalize(lightDirection_pf[i]);" +
        "			reflection_vector[i] = reflect(-normalize_lightDirection[i],normalize_tranformed_normal);" +
        "			ambiant[i] = u_la_pf[i] * u_ka_pf;" +
        "			diffuse[i] = u_ld_pf[i] * u_kd_pf * max(dot(normalize_lightDirection[i],normalize_tranformed_normal),0.0f);" +
        "			specular[i] = u_ls_pf[i] * u_ks_pf * pow(max(dot(reflection_vector[i],normalize_view_vector),0.0f),u_kShineness_pf);" +
        "			fong_ads_light_pf = fong_ads_light_pf + ambiant[i] + diffuse[i] + specular[i];" +
        "		}" +
        "	}" +
        "	else" +
        "	{" +
        "		fong_ads_light_pf = vec3(1.0f,1.0f,1.0f);" +
        "	}" +
        "	FragColor_pf = vec4(fong_ads_light_pf,1.0f);" +
        "}";
    aav_fragmentShaderObject_pf = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(aav_fragmentShaderObject_pf, aav_fragmentShaderSourceCode_pf);
    gl.compileShader(aav_fragmentShaderObject_pf);
    if (gl.getShaderParameter(aav_fragmentShaderObject_pf, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(aav_fragmentShaderObject_pf);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //PerVertrex Shader  Program
    //shader program 
    aav_shaderProgramObject_pv = gl.createProgram();
    gl.attachShader(aav_shaderProgramObject_pv, aav_vertexShaderObject_pv);
    gl.attachShader(aav_shaderProgramObject_pv, aav_fragmentShaderObject_pv);

    //pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(aav_shaderProgramObject_pv, WebGLMacros.AAV_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(aav_shaderProgramObject_pv, WebGLMacros.AAV_ATTRIBUTE_NORMAL, "vNormal");

    //linking 
    gl.linkProgram(aav_shaderProgramObject_pv);
    if (!gl.getProgramParameter(aav_shaderProgramObject_pv, gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(aav_shaderProgramObject_pv);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //PerFragment Shader Program
    //shader program 
    aav_shaderProgramObject_pf = gl.createProgram();
    gl.attachShader(aav_shaderProgramObject_pf, aav_vertexShaderObject_pf);
    gl.attachShader(aav_shaderProgramObject_pf, aav_fragmentShaderObject_pf);

    //pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(aav_shaderProgramObject_pf, WebGLMacros.AAV_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(aav_shaderProgramObject_pf, WebGLMacros.AAV_ATTRIBUTE_NORMAL, "vNormal");

    //linking 
    gl.linkProgram(aav_shaderProgramObject_pf);
    if (!gl.getProgramParameter(aav_shaderProgramObject_pf, gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(aav_shaderProgramObject_pf);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //get uniform location


    /*****************************************************************************/
    //Post Linking Information
    //PerVertex
    aav_modelMatrixUniform_pv = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_model_matrix");
    aav_viewMatrixUniform_pv = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_view_matrix");
    aav_ProjectionMatrixUniform_pv = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_projection_matrix");

    //Red Light
    aav_laUniform_pv[0] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_la_pv[0]");
    aav_ldUniform_pv[0] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_ld_pv[0]");
    aav_lsUniform_pv[0] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_ls_pv[0]");
    aav_lightPositionUniform_pv[0] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_lightPosistion_pv[0]");

    //Green Light
    aav_laUniform_pv[1] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_la_pv[1]");
    aav_ldUniform_pv[1] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_ld_pv[1]");
    aav_lsUniform_pv[1] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_ls_pv[1]");
    aav_lightPositionUniform_pv[1] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_lightPosistion_pv[1]");

    //Blue Light
    aav_laUniform_pv[2] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_la_pv[2]");
    aav_ldUniform_pv[2] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_ld_pv[2]");
    aav_lsUniform_pv[2] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_ls_pv[2]");
    aav_lightPositionUniform_pv[2] = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_lightPosistion_pv[2]");

    aav_kaUniform_pv = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_ka_pv");
    aav_kdUniform_pv = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_kd_pv");
    aav_ksUniform_pv = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_ks_pv");

    aav_kShininessUniform_pv = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_kShineness_pv");

    aav_lKeyPressedUniform_pv = gl.getUniformLocation(aav_shaderProgramObject_pv, "u_lKeyPressed");

    /*************************************************************************************/
    //PerFragment
    aav_modelMatrixUniform_pf = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_model_matrix");
    aav_viewMatrixUniform_pf = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_view_matrix");
    aav_ProjectionMatrixUniform_pf = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_projection_matrix");

    //Red Light
    aav_laUniform_pf[0] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_la_pf[0]");
    aav_ldUniform_pf[0] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_ld_pf[0]");
    aav_lsUniform_pf[0] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_ls_pf[0]");
    aav_lightPositionUniform_pf[0] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_lightPosistion_pf[0]");

    //Green Light
    aav_laUniform_pf[1] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_la_pf[1]");
    aav_ldUniform_pf[1] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_ld_pf[1]");
    aav_lsUniform_pf[1] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_ls_pf[1]");
    aav_lightPositionUniform_pf[1] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_lightPosistion_pf[1]");

    //Blue aav_light
    aav_laUniform_pf[2] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_la_pf[2]");
    aav_ldUniform_pf[2] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_ld_pf[2]");
    aav_lsUniform_pf[2] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_ls_pf[2]");
    aav_lightPositionUniform_pf[2] = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_lightPosistion_pf[2]");

    aav_kaUniform_pf = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_ka_pf");
    aav_kdUniform_pf = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_kd_pf");
    aav_ksUniform_pf = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_ks_pf");

    aav_kShininessUniform_pf = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_kShineness_pf");

    aav_lKeyPressedUniform_pf = gl.getUniformLocation(aav_shaderProgramObject_pf, "u_lKeyPressed");

    /*****************************************************************************************/


    // ** vertices , color , shader attribs, vbo initialization***

    sphereMesh = new Mesh();
    makeSphere(sphereMesh, 2.0, 30, 30);

    //cube vao
    aav_vao_cube = gl.createVertexArray();
    gl.bindVertexArray(aav_vao_cube);

    //aav_material
    aav_material = new aav_Material();

    aav_material.aav_materialAmbiant[0] = 0.0;
    aav_material.aav_materialAmbiant[1] = 0.0;
    aav_material.aav_materialAmbiant[2] = 0.0;

    aav_material.aav_materialDiffuse[0] = 1.0;
    aav_material.aav_materialDiffuse[1] = 1.0;
    aav_material.aav_materialDiffuse[2] = 1.0;

    aav_material.aav_materialSpecular[0] = 1.0;
    aav_material.aav_materialSpecular[1] = 1.0;
    aav_material.aav_materialSpecular[2] = 1.0;

    aav_material.aav_materialShininess = 128.0;


    //lights
    aav_light[0] = new aav_Light();
    aav_light[1] = new aav_Light();
    aav_light[2] = new aav_Light();

    aav_light[0].aav_lightAmbiant[0] = 0.0;
    aav_light[0].aav_lightAmbiant[1] = 0.0;
    aav_light[0].aav_lightAmbiant[2] = 0.0;


    aav_light[0].aav_lightDiffuse[0] = 1.0;
    aav_light[0].aav_lightDiffuse[1] = 0.0;
    aav_light[0].aav_lightDiffuse[2] = 0.0;


    aav_light[0].aav_lightSpecular[0] = 1.0;
    aav_light[0].aav_lightSpecular[1] = 0.0;
    aav_light[0].aav_lightSpecular[2] = 0.0;


    aav_light[0].aav_lightPosition[0] = 0.0;
    aav_light[0].aav_lightPosition[1] = 0.0;
    aav_light[0].aav_lightPosition[2] = 0.0;
    aav_light[0].aav_lightPosition[3] = 1.0;

    //Green
    aav_light[1].aav_lightAmbiant[0] = 0.0;
    aav_light[1].aav_lightAmbiant[1] = 0.0;
    aav_light[1].aav_lightAmbiant[2] = 0.0;

    aav_light[1].aav_lightDiffuse[0] = 0.0;
    aav_light[1].aav_lightDiffuse[1] = 1.0;
    aav_light[1].aav_lightDiffuse[2] = 0.0;

    aav_light[1].aav_lightSpecular[0] = 0.0;
    aav_light[1].aav_lightSpecular[1] = 1.0;
    aav_light[1].aav_lightSpecular[2] = 0.0;

    aav_light[1].aav_lightPosition[0] = 0.0;
    aav_light[1].aav_lightPosition[1] = 0.0;
    aav_light[1].aav_lightPosition[2] = 0.0;
    aav_light[1].aav_lightPosition[3] = 1.0;

    //Blue
    aav_light[2].aav_lightAmbiant[0] = 0.0;
    aav_light[2].aav_lightAmbiant[1] = 0.0;
    aav_light[2].aav_lightAmbiant[2] = 0.0;

    aav_light[2].aav_lightDiffuse[0] = 0.0;
    aav_light[2].aav_lightDiffuse[1] = 0.0;
    aav_light[2].aav_lightDiffuse[2] = 1.0;

    aav_light[2].aav_lightSpecular[0] = 0.0;
    aav_light[2].aav_lightSpecular[1] = 0.0;
    aav_light[2].aav_lightSpecular[2] = 1.0;

    aav_light[2].aav_lightPosition[0] = 0.0;
    aav_light[2].aav_lightPosition[1] = 0.0;
    aav_light[2].aav_lightPosition[2] = 0.0;
    aav_light[2].aav_lightPosition[3] = 1.0;

    //set clear color 
    gl.clearColor(0.0, 0.0, 0.0, 1.0); //blue 

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    //inidialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    //code 
    if (aav_bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else {
        canvas.width = aav_canvas_orignal_width;
        canvas.height = aav_canvas_orignal_height;
    }

    //set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);

    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);

}

function draw()
{
    //variable declarations
    var aav_modelMateix = mat4.create();
    var aav_transformMatrix = mat4.create();
    var aav_viewMatrix = mat4.create();

    //var aav_modelViewProjectionMatrix = mat4.create();
    //code 

    aav_light[0].aav_lightPosition[0] = 0.0;
    aav_light[0].aav_lightPosition[1] = 5 * Math.sin(aav_lightAngle1);
    aav_light[0].aav_lightPosition[2] = 5 * Math.cos(aav_lightAngle1);
    aav_light[0].aav_lightPosition[3] = 1.0;

    aav_light[1].aav_lightPosition[0] = 5 * Math.sin(aav_lightAngle1);
    aav_light[1].aav_lightPosition[1] = 0.0;
    aav_light[1].aav_lightPosition[2] = 5 * Math.cos(aav_lightAngle1);
    aav_light[1].aav_lightPosition[3] = 1.0;

    aav_light[2].aav_lightPosition[0] = 5 * Math.sin(aav_lightAngle1);
    aav_light[2].aav_lightPosition[1] = 5 * Math.cos(aav_lightAngle1);
    aav_light[2].aav_lightPosition[2] = 0.0;
    aav_light[2].aav_lightPosition[3] = 1.0;

    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if (aav_bPerVertex == true)
    {
        gl.useProgram(aav_shaderProgramObject_pv);
        if (aav_bLight == true)
        {
            gl.uniform1i(aav_lKeyPressedUniform_pv, 1);
            //Light
            //Red
            gl.uniform3fv(aav_laUniform_pv[0], aav_light[0].aav_lightAmbiant);
            gl.uniform3fv(aav_ldUniform_pv[0], aav_light[0].aav_lightDiffuse);
            gl.uniform3fv(aav_lsUniform_pv[0], aav_light[0].aav_lightSpecular);
            gl.uniform4fv(aav_lightPositionUniform_pv[0], aav_light[0].aav_lightPosition);
            //Green
            gl.uniform3fv(aav_laUniform_pv[1], aav_light[1].aav_lightAmbiant);
            gl.uniform3fv(aav_ldUniform_pv[1], aav_light[1].aav_lightDiffuse);
            gl.uniform3fv(aav_lsUniform_pv[1], aav_light[1].aav_lightSpecular);
            gl.uniform4fv(aav_lightPositionUniform_pv[1], aav_light[1].aav_lightPosition);
            //Blue
            gl.uniform3fv(aav_laUniform_pv[2], aav_light[2].aav_lightAmbiant);
            gl.uniform3fv(aav_ldUniform_pv[2], aav_light[2].aav_lightDiffuse);
            gl.uniform3fv(aav_lsUniform_pv[2], aav_light[2].aav_lightSpecular);
            gl.uniform4fv(aav_lightPositionUniform_pv[2], aav_light[2].aav_lightPosition);

            //aav_material
            gl.uniform3fv(aav_kaUniform_pv, aav_material.aav_materialAmbiant);
            gl.uniform3fv(aav_kdUniform_pv, aav_material.aav_materialDiffuse);
            gl.uniform3fv(aav_ksUniform_pv, aav_material.aav_materialSpecular);
            gl.uniform1f(aav_kShininessUniform_pv, aav_material.aav_materialShininess);

        }
        else
        {

            gl.uniform1i(aav_lKeyPressedUniform_pv, 0);
        }

        aav_transformMatrix = mat4.create();
        aav_viewMatrix = mat4.create();
        aav_modelMateix = mat4.create();

        mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

        //mat4.multiply(aav_modelMateix,aav_modelMateix,aav_rotationMatrix);

        gl.uniformMatrix4fv(aav_modelMatrixUniform_pv, false, aav_modelMateix);

        gl.uniformMatrix4fv(aav_viewMatrixUniform_pv, false, aav_viewMatrix);

        gl.uniformMatrix4fv(aav_ProjectionMatrixUniform_pv, false, perspectiveProjectionMatrix);

        sphereMesh.draw();

        gl.useProgram(null);

    }

    else if (aav_bPerFragment == true)
    {
        gl.useProgram(aav_shaderProgramObject_pf);
        if (aav_bLight == true)
        {
            gl.uniform1i(aav_lKeyPressedUniform_pf, 1);
            //Light
            //Red
            gl.uniform3fv(aav_laUniform_pf[0], aav_light[0].aav_lightAmbiant);
            gl.uniform3fv(aav_ldUniform_pf[0], aav_light[0].aav_lightDiffuse);
            gl.uniform3fv(aav_lsUniform_pf[0], aav_light[0].aav_lightSpecular);
            gl.uniform4fv(aav_lightPositionUniform_pf[0], aav_light[0].aav_lightPosition);
            //Green
            gl.uniform3fv(aav_laUniform_pf[1], aav_light[1].aav_lightAmbiant);
            gl.uniform3fv(aav_ldUniform_pf[1], aav_light[1].aav_lightDiffuse);
            gl.uniform3fv(aav_lsUniform_pf[1], aav_light[1].aav_lightSpecular);
            gl.uniform4fv(aav_lightPositionUniform_pf[1], aav_light[1].aav_lightPosition);
            //Blue
            gl.uniform3fv(aav_laUniform_pf[2], aav_light[2].aav_lightAmbiant);
            gl.uniform3fv(aav_ldUniform_pf[2], aav_light[2].aav_lightDiffuse);
            gl.uniform3fv(aav_lsUniform_pf[2], aav_light[2].aav_lightSpecular);
            gl.uniform4fv(aav_lightPositionUniform_pf[2], aav_light[2].aav_lightPosition);

            //aav_material
            gl.uniform3fv(aav_kaUniform_pf, aav_material.aav_materialAmbiant);
            gl.uniform3fv(aav_kdUniform_pf, aav_material.aav_materialDiffuse);
            gl.uniform3fv(aav_ksUniform_pf, aav_material.aav_materialSpecular);
            gl.uniform1f(aav_kShininessUniform_pf, aav_material.aav_materialShininess);

        }
        else
        {

            gl.uniform1i(aav_lKeyPressedUniform_pf, 0);
        }

        aav_transformMatrix = mat4.create();
        aav_viewMatrix = mat4.create();
        aav_modelMateix = mat4.create();

        mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

        //mat4.multiply(aav_modelMateix,aav_modelMateix,aav_rotationMatrix);

        gl.uniformMatrix4fv(aav_modelMatrixUniform_pf, false, aav_modelMateix);

        gl.uniformMatrix4fv(aav_viewMatrixUniform_pf, false, aav_viewMatrix);

        gl.uniformMatrix4fv(aav_ProjectionMatrixUniform_pf, false, perspectiveProjectionMatrix);

        sphereMesh.draw();

        gl.useProgram(null);
    }


    if (aav_bAnimationflag == true)
    {
        update();
    }

    requestAnimationFrame(draw, canvas);
}


function update()
{

    aav_lightAngle1 = aav_lightAngle1 + 0.01;
    if (aav_lightAngle1 > 360)
    {
        aav_lightAngle1 = 0.0;
    }

    aav_lightAngle2 = aav_lightAngle2 + 0.01;
    if (aav_lightAngle2 > 360)
    {
        aav_lightAngle2 = 0.0;
    }

    aav_lightAngle3 = aav_lightAngle3 + 0.01;
    if (aav_lightAngle3 > 360)
    {
        aav_lightAngle3 = 0.0;
    }
}


function degToRad(degree)
{
    return (degree * Math.PI / 180.0);
}

function uninitialize()
{
    //code
    if (sphereMesh)
    {
        sphereMesh.deallocate();
    }

    if (aav_shaderProgramObject_pf)
    {
        if (aav_fragmentShaderObject_pf)
        {
            gl.detachShader(aav_shaderProgramObject_pf, aav_fragmentShaderObject_pf);
            gl.deleteShader(aav_fragmentShaderObject_pf);
            aav_fragmentShaderObject_pf = null;
        }

        if (aav_vertexShaderObject_pf)
        {
            gl.detachShader(aav_shaderProgramObject_pf, aav_vertexShaderObject_pf);
            gl.deleteShader(aav_vertexShaderObject_pf);
            aav_vertexShaderObject_pf = null;
        }

        gl.deleteProgram(aav_shaderProgramObject_pf);
        aav_shaderProgramObject_pf = null;
    }
}

function keyDown(event)
{
    //code
    switch (event.keyCode)
    {
        case 81:
        case 113:
            //unitialize
            uninitialize();
            //close our application's tab
            window.close(); // may not work in firefox but work in safari and chrome 
            break;
        case 97:
        case 65:
            if (aav_bAnimationflag == false)
            {
                aav_bAnimationflag = true;
            }
            else {
                aav_bAnimationflag = false;
            }
            break; case 27: // for 'F' or 'f'
            toggleFullScreen();
            break;
        case 70:
        case 102:
            if (aav_bPerFragment == false)
            {
                aav_bPerVertex = false;
                aav_bPerFragment = true;
            }

            break;
        case 86:
        case 118:
            if (aav_bPerVertex == false)
            {
                aav_bPerFragment = false;
                aav_bPerVertex = true;
            }

            break;
        case 108:
        case 76:
            if (aav_bLight == false)
            {
                aav_bLight = true;
            }
            else
            {
                aav_bLight = false;
            }
            break;
        default:
            break;
    }
}

function mouseDown()
{
    //code
}