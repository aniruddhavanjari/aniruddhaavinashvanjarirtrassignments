//global variable
var gl = null;  // webgl context
var aav_bFullscreen = false;
var aav_canvas_orignal_width;
var aav_canvas_orignal_height;

//Matrix Array Creation
var matrix = new Array(10); 
var stacktop = -1;
for (var i = 0; i < 10; i++)
{
    matrix[i] = new Array(16); // for mat4 is Linear Array of 16
}


const WebGLMacros = 
{
    AAV_ATTRIBUTE_VERTEX:0,
    AAV_ATTRIBUTE_COLOR:1,
    AAV_ATTRIBUTE_NORMAL:2,
    AAV_ATTRIBUTE_TEXTURE:3
};


var aav_vertexShaderObject;
var aav_fragmentShaderObject;
var aav_shaderProgramObject;

var aav_vao_cube_square;
var aav_vbo_position_cube_square;
var aav_vbo_color_pyramid_square;

var aav_vao_cube;
var aav_vbo_position_cube;
var aav_vbo_color_pyramid;

var aav_mvpUniform;
var aav_colorUniform;

var perspectiveProjectionMatrix;

var aav_angleCube = 0;

var sphereMesh = null;


var aav_sholderRotate = 0.0;
var aav_elboRotate = 0.0;

var aav_gVertexShaderObject;
var aav_gFragmentShaderObject;
var aav_gShaderProgramObject;

var aav_gVao_triangle;
var aav_gVbo_position_triangle;
var aav_gVbo_color_triangle;

var aav_gVao_square;
var aav_gVbo_position_square;
var aav_gVbo_color_square;

var aav_gVao_line;
var aav_gVbo_line_position;
var aav_aVbo_line_color;

var aav_gVao_circleOuter;
var aav_gVbo_circleOuter_position;
var aav_aVbo_circleOuter_color;

var aav_gVao_circleInner;
var aav_gVbo_circleInner_position;
var aav_aVbo_circleInner_color;

var aav_gVao_point;
var aav_gVbo_point_position;
var aav_gVbo_point_color;


var  aav_circleVertices = [];
var aav_circleColor = [];

var aav_circleVerticesInner = [];
var aav_circleColorInner = [];

var r;	   // radius of innder circle 
var count = 0; // Number of interation in Circle Loop



//To start animation: To have requestAnimation() to be called "cross-browser" compatible
var aav_requestAnimationFrame = 
window.requestAnimationFrame || 
window.webkitRequestAnimationFrame || 
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame;

//To stop animation : to have aav_cancelAnimationFrame() to be called "cross-browser" compatible
var aav_cancelAnimationFrame = 
window.aav_cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame || 
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || 
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

var aav_color = new Array(3);

var aav_earthRevolution = 0.0;
var aav_giday = 0.0;
var aav_giMoonday = 0.0;
var aav_giMoonyear = 0.0;


var aav_vao_pyramid;
var aav_vbo_position_pyramid;
var aav_vbo_color_pyramid;

// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AAV");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    aav_canvas_orignal_width = canvas.width;
    aav_canvas_orignal_height = canvas.height;

    //register keyboard's keydown event handler
    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",resize,false);

    //initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    //code 
    var aav_fullscreen_element = 
    document.fullscreenElement || 
    document.webkitFullscreenElement || 
    document.mozFullscreenElement || 
    document.msFullscreenElement || 
    null;

    //if not fullscreen
    if(aav_fullscreen_element == null)
    {
        if(canvas.requestFullScree)
            canvas.requestFullScree();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();        
        aav_bFullscreen = true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        aav_bFullscreen =false;
    }
}

function init()
{
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");
    if(gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shader 
    var aav_vertexShaderSourcedCode = 
    "#version 300 es"+
    "\n" +
   
    "in vec4 vPosition;" +
    "in vec3 vColor;"+
    "uniform mat4 u_mvp_matrix;" +
    "uniform vec3 u_color;" +
    "out vec3 fong_ads_light;" +
    "void main(void)"+ 
    "{" +
     "fong_ads_light = vColor;" +
        "gl_Position = u_mvp_matrix * vPosition;" +
        "gl_PointSize=1.5;" +
    "}";
    aav_vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(aav_vertexShaderObject,aav_vertexShaderSourcedCode);
    gl.compileShader(aav_vertexShaderObject);
    if(gl.getShaderParameter(aav_vertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(aav_vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragemnt shader
    var aav_fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+ 
    "precision highp float;" +
    "in vec3 fong_ads_light;" +
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "FragColor = vec4(fong_ads_light,1.0f);"+
    "}";
    aav_fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(aav_fragmentShaderObject,aav_fragmentShaderSourceCode);
    gl.compileShader(aav_fragmentShaderObject);
    if(gl.getShaderParameter(aav_fragmentShaderObject,gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(aav_fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program 
    aav_shaderProgramObject = gl.createProgram();
    gl.attachShader(aav_shaderProgramObject,aav_vertexShaderObject);
    gl.attachShader(aav_shaderProgramObject,aav_fragmentShaderObject);

    //pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(aav_shaderProgramObject, WebGLMacros.AAV_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(aav_shaderProgramObject, WebGLMacros.AAV_ATTRIBUTE_COLOR, "vColor");

    //linking 
    gl.linkProgram(aav_shaderProgramObject);
    if(!gl.getProgramParameter(aav_shaderProgramObject,gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(aav_shaderProgramObject);
        if(error.length >  0)
        {
            alert(error);
            uninitialize();
        }
    }

    //get MVP uniform location
    aav_mvpUniform = gl.getUniformLocation(aav_shaderProgramObject,"u_mvp_matrix");
    aav_colorUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_color");	
    // ** vertices , color , shader attribs, vbo initialization***
 
    var aav_pyramidVerteis = new Float32Array([
        0.0, 0.5, 0.0,
        -0.5, -0.5, 0.5,
        0.5, -0.5, 0.5,

        0.0, 0.5, 0.0,
        0.5, -0.5, 0.5,
        0.5, -0.5, -0.5,

        0.0, 0.5, 0.0,
        -0.5, -0.5, -0.5,
        0.5, -0.5, -0.5,

        0.0, 0.5, 0.0,
        -0.5, -0.5, 0.5,
        -0.5, -0.5, -0.5
    ]);

    var aav_triangleVertices = new Float32Array([
        0.0, 0.5, 0.0,
        -0.5, -0.5, 0.0,

        -0.5, -0.5, 0.0,
        0.5, -0.5, 0.0,

        0.5, -0.5, 0.0,
        0.0, 0.5, 0.0,
    ]);

    var aav_trianglecolors = new Float32Array([
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0
    ]);


    var aav_cubeVertex = new Float32Array([
        0.5, 0.5, 0.0,
        -0.5, 0.5, 0.0,

        -0.5, 0.5, 0.0,
        -0.5, -0.5, 0.0,

        -0.5, -0.5, 0.0,
        0.5, -0.5, 0.0,

        0.5, -0.5, 0.0,
        0.5, 0.5, 0.0
    ]);

    var aav_cubecolor = new Float32Array([
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0,
        1.0, 1.0, 1.0
    ]);



    var aav_lineVertex = new Float32Array([
      


        0.2, -1.3, 0.0,
        0.2, 1.3, 0.0,


        0.4, -1.3, 0.0,
        0.4, 1.3, 0.0,


        0.6, -1.3, 0.0,
        0.6, 1.3, 0.0,

        0.8, -1.3, 0.0,
        0.8, 1.3, 0.0,


        1.0, -1.3, 0.0,
        1.0, 1.3, 0.0,

        1.2, -1.3, 0.0,
        1.2, 1.3, 0.0,


        1.4, -1.3, 0.0,
        1.4, 1.3, 0.0,


        1.6, -1.3, 0.0,
        1.6, 1.3, 0.0,


        1.8, -1.3, 0.0,
        1.8, 1.3, 0.0,

        2.0, -1.3, 0.0,
        2.0, 1.3, 0.0,


        //x negative
        -0.2, -1.3, 0.0,
        -0.2, 1.3, 0.0,


        -0.4, -1.3, 0.0,
        -0.4, 1.3, 0.0,


        -0.6, -1.3, 0.0,
        -0.6, 1.3, 0.0,

        -0.8, -1.3, 0.0,
        -0.8, 1.3, 0.0,


        -1.0, -1.3, 0.0,
        -1.0, 1.3, 0.0,

        -1.2, -1.3, 0.0,
        -1.2, 1.3, 0.0,


        -1.4, -1.3, 0.0,
        -1.4, 1.3, 0.0,


        -1.6, -1.3, 0.0,
        -1.6, 1.3, 0.0,


        -1.8, -1.3, 0.0,
        -1.8, 1.3, 0.0,

        -2.0, -1.3, 0.0,
        -2.0, 1.3, 0.0,

        //y axies
        -2.0, 0.13, 0.0,
        2.0, 0.13, 0.0,


        -2.0, 0.26, 0.0,
        2.0, 0.26, 0.0,


        -2.0, 0.39, 0.0,
        2.0, 0.39, 0.0,


        -2.0, 0.52, 0.0,
        2.0, 0.52, 0.0,

        -2.0, 0.65, 0.0,
        2.0, 0.65, 0.0,


        -2.0, 0.78, 0.0,
        2.0, 0.78, 0.0,


        -2.0, 0.91, 0.0,
        2.0, 0.91, 0.0,


        -2.0, 1.04, 0.0,
        2.0, 1.04, 0.0,

        -2.0, 1.17, 0.0,
        2.0, 1.17, 0.0,

        -2.0, 1.3, 0.0,
        2.0, 1.3, 0.0,

        //
        -2.0, -0.13, 0.0,
        2.0, -0.13, 0.0,


        -2.0, -0.26, 0.0,
        2.0, -0.26, 0.0,


        -2.0, -0.39, 0.0,
        2.0, -0.39, 0.0,


        -2.0, -0.52, 0.0,
        2.0, -0.52, 0.0,

        -2.0, -0.65, 0.0,
        2.0, -0.65, 0.0,


        -2.0, -0.78, 0.0,
        2.0, -0.78, 0.0,


        -2.0, -0.91, 0.0,
        2.0, -0.91, 0.0,


        -2.0, -1.04, 0.0,
        2.0, -1.04, 0.0,

        -2.0, -1.17, 0.0,
        2.0, -1.17, 0.0,

        -2.0, -1.3, 0.0,
        2.0, -1.3, 0.0,

        0.0, -1.3, 0.0,
        0.0, 1.3, 0.0,
        -2.0, 0.0, 0.0,
        2.0, 0.0, 0.0
    ]);

    var aav_lineColor = new Float32Array([
       

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        //
        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        0.0, 0.0, 1.0,
        0.0, 0.0, 1.0,

        //0.0, 0.0, 1.0,
        //0.0, 0.0, 1.0,

        //0.0, 0.0, 1.0,
        //0.0, 0.0, 1.0,

        //0.0, 0.0, 1.0,
        //0.0, 0.0, 1.0,

        //0.0, 0.0, 1.0,
        //0.0, 0.0, 1.0,


        1.0, 0.0, 0.0,
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 1.0, 0.0
    ]);

    var aav_pointVertex = new Float32Array
    ([
            0.0, 0.0, 0.0
    ]);

    var aav_pointColor = new Float32Array
    ([
            1.0, 1.0, 1.0
    ]);

    //Point
    aav_gVao_point = gl.createVertexArray();
    gl.bindVertexArray(aav_gVao_point);

    aav_gVbo_point_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_gVbo_point_position);
    gl.bufferData(gl.ARRAY_BUFFER, aav_pointVertex, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    aav_gVbo_point_color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_gVbo_point_color);
    gl.bufferData(gl.ARRAY_BUFFER, aav_pointColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_COLOR,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);



    //Line
    //Lines
    aav_gVao_line = gl.createVertexArray();
    gl.bindVertexArray(aav_gVao_line);

    aav_gVbo_line_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_gVbo_line_position);
    gl.bufferData(gl.ARRAY_BUFFER, aav_lineVertex, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    aav_aVbo_line_color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_aVbo_line_color);
    gl.bufferData(gl.ARRAY_BUFFER, aav_lineColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_COLOR,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    //triangle
    aav_gVao_triangle = gl.createVertexArray();
    gl.bindVertexArray(aav_gVao_triangle);

    aav_gVbo_position_triangle = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_gVbo_position_triangle);
    gl.bufferData(gl.ARRAY_BUFFER, aav_triangleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
        3, 
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    aav_gVbo_color_triangle = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_gVbo_color_triangle);
    gl.bufferData(gl.ARRAY_BUFFER, aav_trianglecolors, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_COLOR,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);

    //Square
    aav_gVao_square = gl.createVertexArray();
    gl.bindVertexArray(aav_gVao_square);

    aav_gVbo_position_square = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_gVbo_position_square);
    gl.bufferData(gl.ARRAY_BUFFER, aav_cubeVertex, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
        3, 
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    aav_gVbo_color_square = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_gVbo_color_square);
    gl.bufferData(gl.ARRAY_BUFFER, aav_cubecolor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_COLOR,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);


    Calculation();
    //Inner Circle
    aav_gVao_circleInner = gl.createVertexArray();
    gl.bindVertexArray(aav_gVao_circleInner);

    aav_gVbo_circleInner_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_gVbo_circleInner_position);
    gl.bufferData(gl.ARRAY_BUFFER, aav_circleVerticesInner, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    aav_aVbo_circleInner_color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_aVbo_circleInner_color);
    gl.bufferData(gl.ARRAY_BUFFER, aav_circleColorInner, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_COLOR,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

    //OutCircle
    aav_gVao_circleOuter = gl.createVertexArray();
    gl.bindVertexArray(aav_gVao_circleOuter);

    aav_gVbo_circleOuter_position = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_gVbo_circleOuter_position);
    gl.bufferData(gl.ARRAY_BUFFER, aav_circleVertices, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    aav_aVbo_circleOuter_color = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_aVbo_circleOuter_color);
    gl.bufferData(gl.ARRAY_BUFFER, aav_circleColor, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_COLOR,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_COLOR);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    gl.bindVertexArray(null);

   
    //set clear color 
    gl.clearColor(0.0,0.0,0.0,1.0); //blue 

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.lineWidth(5.0);
    //inidialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    //code 
    if(aav_bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = aav_canvas_orignal_width;
        canvas.height = aav_canvas_orignal_height;
    }

    //set the viewport to match
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);

}

function Calculation()
{
    var Line = 0.0;
    var fLine = 0.5;
    var side;
    var Squareside;
    var OutSideCircleR;

    var a; //Consider side 1
    var b; //Consider side 2
    var c; //Consider side 3

    var xcenterCor;
    var ycenterCor;

    var PI = 3.14;
    //Code
    a = Math.sqrt(((0.5 - (-0.5)) * (0.5 - (-0.5))) + (((-0.5) - (-0.5)) * ((-0.5) - (-0.5)))); //1
    b = Math.sqrt(((0.0 - 0.5) * (0.0 - 0.5)) + ((0.5 - (-0.5)) * (0.5 - (-0.5)))); //1.118033989
    c = Math.sqrt((((-0.5) - 0.0) * ((-0.5) - 0.0)) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); //1.118033989

    //Formula For x,y Cordinate for Center Point of Triangle To Draw the Circle.
    xcenterCor = ((a * (0)) + (b * (-0.5)) + (c * (0.5))) / (a + b + c);
    ycenterCor = ((a * (0.5)) + (b * (-0.5)) + (c * (-0.5))) / (a + b + c);

    side = (a + b + c) / 2;
    r = Math.sqrt(side * (side - a) * (side - b) * (side - c)) / side;  //Formmula For inCircle or inscribed Circle Radius. 

    //Circle OutSide Rectangle
    Squareside = Math.sqrt((((-0.5) - (-0.5)) * ((-0.5) - (-0.5))) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); // We Get The Length of One Size of Square
    OutSideCircleR = (Squareside * Math.sqrt(2)) / 2; // We Get Radius of Circle 

  
    for (var angle = 0; angle < 2 * PI; angle = angle + 0.001)
    {
        count = count + 1;
    }

    aav_circleVertices = new Float32Array(4 * count * 3);
    aav_circleColor = new Float32Array(4* count * 3);

    var i = 0;
    for (var angle = 0; angle < 2 * PI; angle = angle + 0.001)
    {
        aav_circleVertices[i] = OutSideCircleR * Math.sin(angle);
        aav_circleColor[i] = 1.0;
        i = i + 1;
        aav_circleVertices[i] = OutSideCircleR * Math.cos(angle);
        aav_circleColor[i] = 1.0;
        i = i + 1;
        aav_circleVertices[i] = 0.0;
        aav_circleColor[i] = 1.0;
        i = i + 1;
    }

    i = 0;
    aav_circleVerticesInner = new Float32Array(4 * count * 3);
    aav_circleColorInner = new Float32Array(4 * count * 3);
    for (var angle = 0; angle < 2 * PI; angle = angle + 0.001)
    {
        aav_circleVerticesInner[i] = r * Math.sin(angle) + +xcenterCor;
        aav_circleColorInner[i] = 1.0;
        i = i + 1;
        aav_circleVerticesInner[i] = r * Math.cos(angle) + +ycenterCor;
        aav_circleColorInner[i] = 1.0;
        i = i + 1;
        aav_circleVerticesInner[i] = 0.0;
        aav_circleColorInner[i] = 1.0;
        i = i + 1;
    }

}


function draw()
{
    //variable declarations
    var aav_modelViewMatrix = mat4.create();
    var aav_transformMatrix = mat4.create();
    var aav_tempStoreMatrix = mat4.create(); 
    var aav_scaleMatrix = mat4.create();
    var aav_modelViewProjectionMatrix = mat4.create();
    //code 
    gl.clear(gl.COLOR_BUFFER_BIT|  gl.DEPTH_BUFFER_BIT);
    
    gl.useProgram(aav_shaderProgramObject);

    aav_color[0] = 1.0;
    aav_color[1] = 1.0;
    aav_color[2] = 1.0;
    gl.uniform3fv(aav_colorUniform,aav_color);	

    mat4.translate(aav_modelViewMatrix,aav_transformMatrix,[0.0,0.0,-3.0]);

    mat4.multiply(aav_modelViewProjectionMatrix, perspectiveProjectionMatrix, aav_modelViewMatrix);

    gl.uniformMatrix4fv(aav_mvpUniform, false, aav_modelViewProjectionMatrix);

    gl.lineWidth(5.0);
    //Line
    gl.bindVertexArray(aav_gVao_line);
    gl.drawArrays(gl.LINES, 0, 84);
    gl.bindVertexArray(null);

    //Triangle
    gl.bindVertexArray(aav_gVao_triangle);

    gl.drawArrays(gl.LINE_LOOP, 0, 6);

    gl.bindVertexArray(null);

    //Square
    gl.bindVertexArray(aav_gVao_square);

    gl.drawArrays(gl.LINES, 0, 8);

    gl.bindVertexArray(null);


    ////Center Point
    
    gl.bindVertexArray(aav_gVao_point);
    
    gl.drawArrays(gl.POINTS, 0, 1);

    gl.bindVertexArray(null);


    gl.bindVertexArray(aav_gVao_circleInner);
    gl.drawArrays(gl.POINTS, 0, count);
    gl.bindVertexArray(null);

    
    gl.bindVertexArray(aav_gVao_circleOuter);
    gl.drawArrays(gl.POINTS, 0, count);
    gl.bindVertexArray(null);
    gl.useProgram(null);
    
    //animation loop
    aav_angleCube = aav_angleCube + 1;
    if(aav_angleCube > 360)
    {
       aav_angleCube = 0;
    } 
  
    requestAnimationFrame(draw, canvas);
}

function push(pushMatrix)
{
    if (stacktop >= 10) {
        return (-1);
    }
    stacktop = stacktop + 1;
    for (var i = 0; i < 16; i++)
    {
        matrix[stacktop][i] = pushMatrix[i];
    }
}

function pop(popMatrix)
{
    if (stacktop <= -1)
    {
        return (-1);
    }
    for (var i = 0; i < 16; i++) {
        popMatrix[i] = matrix[stacktop][i];
    }
    stacktop = stacktop - 1;
}

function degToRad(degree)
{
    return(degree * Math.PI/180.0);
}


function uninitialize()
{
    //code
    //if(sphere)
    //{
    //    sphere.deallocate();
    //}

    if(aav_shaderProgramObject)
    {
        if(aav_fragmentShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject,aav_fragmentShaderObject);
            gl.deleteShader(aav_fragmentShaderObject);
            aav_fragmentShaderObject = null;
        }

        if(aav_vertexShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject,aav_vertexShaderObject);
            gl.deleteShader(aav_vertexShaderObject);
            aav_vertexShaderObject = null;
        }

        gl.deleteProgram(aav_shaderProgramObject);
        aav_shaderProgramObject = null;
    }
}

function keyDown(event)
{
    //code
    switch(event.keyCode)
    {
        case 27: // escape 
            uninitialize();
            window.close(); // may not work in firefox but work in safari and chrome 
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
        default:
            break;
    }

   
   
}

function mouseDown()
{
    //code
}

//Note : KeyBoardEvent
//https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent#obsolete_properties