//global variable
var gl = null;  // webgl context
var aav_bFullscreen = false;
var aav_canvas_orignal_width;
var aav_canvas_orignal_height;

const WebGLMacros = 
{
    AAV_ATTRIBUTE_VERTEX:0,
    AAV_ATTRIBUTE_COLOR:1,
    AAV_ATTRIBUTE_NORMAL:2,
    AAV_ATTRIBUTE_TEXTURE:3
};


var aav_vertexShaderObject;
var aav_fragmentShaderObject;
var aav_shaderProgramObject;

var aav_vao_cube_square;
var aav_vbo_position_cube_square;
var aav_vbo_color_pyramid_square;

var aav_vao_cube;
var aav_vbo_position_cube;
var aav_vbo_color_pyramid;

var aav_mvpUniform;

var aav_perspectiveProjectionMatrix;

var aav_angleCube = 0;

var sphereMesh = null;

//Light Array
var aav_lightAmbiant = [0.0,0.0,0.0 ];	//la
var aav_lightDiffuse = [ 1.0,1.0,1.0];	// ld
var aav_lightSpecular = [ 1.0,1.0,1.0];	//ls

var aav_lightPosition = [100.0,100.0,100.0,1.0];

//material Array 
var aav_materialAmbiant	= [ 0.0, 0.0, 0.0 ]; // ka 
var aav_materialDiffuse	= [1.0,1.0,1.0];	// kd
var aav_materialSpecular	= [1.0,1.0,1.0];	// ks

var aav_materialShininess = 128.0; // material shininess

var aav_viewMatrixUniform;
var aav_modelMatrixUniform;
var aav_ProjectionMatrixUniform;

var aav_laUniform;
var aav_ldUniform;
var aav_lsUniform;
var aav_lightPositionUniform;

var aav_kaUniform;
var aav_kdUniform;
var aav_ksUniform;

var aav_kShininessUniform;

var aav_lKeyPressedUniform;

var aav_bLight = false;

//To start animation: To have requestAnimation() to be called "cross-browser" compatible
var aav_requestAnimationFrame = 
window.requestAnimationFrame || 
window.webkitRequestAnimationFrame || 
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame;

//To stop animation : to have aav_cancelAnimationFrame() to be called "cross-browser" compatible
var aav_cancelAnimationFrame = 
window.aav_cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame || 
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || 
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AAV");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    aav_canvas_orignal_width = canvas.width;
    aav_canvas_orignal_height = canvas.height;

    //register keyboard's keydown event handler
    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",resize,false);

    //initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    //code 
    var aav_fullscreen_element = 
    document.fullscreenElement || 
    document.webkitFullscreenElement || 
    document.mozFullscreenElement || 
    document.msFullscreenElement || 
    null;

    //if not fullscreen
    if(aav_fullscreen_element == null)
    {
        if(canvas.requestFullScree)
            canvas.requestFullScree();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();        
        aav_bFullscreen = true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        aav_bFullscreen =false;
    }
}

function init()
{
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");
    if(gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shader 
    var aav_vertexShaderSourcedCode = 
    "#version 300 es"+
    "\n"+
    "precision highp float;"+
    "precision mediump int;"+
    "in vec4 vPosition;" +
	"in vec3 vNormal;" +
	"uniform mat4 u_view_matrix;" +
	"uniform mat4 u_model_matrix;" +
	"uniform mat4 u_projection_matrix;" +
	"uniform int u_lKeyPressed;" +
	"uniform vec3 u_la;" +
	"uniform vec3 u_ld;" +
	"uniform vec3 u_ls;" +
	"uniform vec4 u_lightPosistion;" +
	"uniform vec3 u_ka;" +
	"uniform vec3 u_kd;" +
	"uniform vec3 u_ks;" +
	"uniform float u_kShineness;"+
	"out vec3 fong_ads_light;" +
	"void main(void)" +
	"{" +
	"	if(u_lKeyPressed == 1)" +
	"	{"+
	"		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" +
	"		vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
	"		vec3 lightDirection = normalize(vec3(u_lightPosistion - eyeCordinate));" +
	"		vec3 reflection_vector = reflect(-lightDirection,tranformed_normal);" +
	"		vec3 view_vector = normalize(-eyeCordinate.xyz);" +
	"		vec3 ambiant = u_la * u_ka;" +
	"		vec3 diffuse = u_ld * u_kd * max(dot(lightDirection,tranformed_normal),0.0f);" +
	"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector,view_vector),0.0f),u_kShineness);" +
	"		fong_ads_light = ambiant + diffuse + specular;"+
	"	}" +
	"	else" +
	"	{" +
	"		fong_ads_light = vec3(1.0f,1.0f,1.0f);"+
	"	}"+
	"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
    "}";
    aav_vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(aav_vertexShaderObject,aav_vertexShaderSourcedCode);
    gl.compileShader(aav_vertexShaderObject);
    if(gl.getShaderParameter(aav_vertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(aav_vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragemnt shader
    var aav_fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+ 
    "precision highp float;"+
    "in vec3 fong_ads_light;"+
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "FragColor = vec4(fong_ads_light,1.0);"+
    "}";
    aav_fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(aav_fragmentShaderObject,aav_fragmentShaderSourceCode);
    gl.compileShader(aav_fragmentShaderObject);
    if(gl.getShaderParameter(aav_fragmentShaderObject,gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(aav_fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program 
    aav_shaderProgramObject = gl.createProgram();
    gl.attachShader(aav_shaderProgramObject,aav_vertexShaderObject);
    gl.attachShader(aav_shaderProgramObject,aav_fragmentShaderObject);

    //pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(aav_shaderProgramObject, WebGLMacros.AAV_ATTRIBUTE_NORMAL, "vNormal");

    //linking 
    gl.linkProgram(aav_shaderProgramObject);
    if(!gl.getProgramParameter(aav_shaderProgramObject,gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(aav_shaderProgramObject);
        if(error.length >  0)
        {
            alert(error);
            uninitialize();
        }
    }

    //get MVP uniform location

    aav_modelMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_model_matrix");
	aav_viewMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_view_matrix");
	aav_ProjectionMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_projection_matrix");

	aav_laUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_la");
	aav_ldUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ld");
	aav_lsUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ls");
	aav_lightPositionUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_lightPosistion");
	
	aav_kaUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ka");
	aav_kdUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_kd");
	aav_ksUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ks");

	aav_kShininessUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_kShineness");

	aav_lKeyPressedUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_lKeyPressed");

    // ** vertices , color , shader attribs, vbo initialization***
    sphereMesh = new Mesh();
    makeSphere(sphereMesh, 2.0, 30,30);

    //set clear color 
    gl.clearColor(0.0,0.0,0.0,1.0); //blue 

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
   
    //inidialize projection matrix
    aav_perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    //code 
    if(aav_bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = aav_canvas_orignal_width;
        canvas.height = aav_canvas_orignal_height;
    }

    //set the viewport to match
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(aav_perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);

}

function draw()
{
    //variable declarations
    var aav_modelMateix;
    var aav_viewMatrix ;
    var aav_transformMatrix = mat4.create();

   
    //code 
    gl.clear(gl.COLOR_BUFFER_BIT|  gl.DEPTH_BUFFER_BIT);
    
    gl.useProgram(aav_shaderProgramObject);


    if (aav_bLight == true)
	{ 
		gl.uniform1i(aav_lKeyPressedUniform,1);
		//Light
		gl.uniform3fv(aav_laUniform,aav_lightAmbiant);				// la 
		gl.uniform3fv(aav_ldUniform,aav_lightDiffuse);				// ld
		gl.uniform3fv(aav_lsUniform,aav_lightSpecular);				// ls
		gl.uniform4fv(aav_lightPositionUniform,aav_lightPosition);	//lightPosition

		//material
		gl.uniform3fv(aav_kaUniform,aav_materialAmbiant);	// ka
		gl.uniform3fv(aav_kdUniform,aav_materialDiffuse);	// kd
		gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform,128.0);
	}
	else
	{
		gl.uniform1i(aav_lKeyPressedUniform, 0);
	}

    aav_modelMateix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_transformMatrix  = mat4.create(); 
    

    mat4.translate(aav_modelMateix,aav_transformMatrix,[0.0,0.0,-6.0]);

    //mat4.multiply(aav_modelMateix,aav_modelMateix,aav_transformMatrix);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);
    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);
    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, aav_perspectiveProjectionMatrix);

    sphereMesh.draw();

    gl.useProgram(null);
    
    //animation loop
    aav_angleCube = aav_angleCube + 1;
    if(aav_angleCube > 360)
    {
       aav_angleCube = 0;
    } 
  
    requestAnimationFrame(draw, canvas);
}


function degToRad(degree)
{
    return(degree * Math.PI/180.0);
}


function uninitialize()
{
    //code
    if(sphere)
    {
        sphere.deallocate();
    }

    if(aav_shaderProgramObject)
    {
        if(aav_fragmentShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject,aav_fragmentShaderObject);
            gl.deleteShader(aav_fragmentShaderObject);
            aav_fragmentShaderObject = null;
        }

        if(aav_vertexShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject,aav_vertexShaderObject);
            gl.deleteShader(aav_vertexShaderObject);
            aav_vertexShaderObject = null;
        }

        gl.deleteProgram(aav_shaderProgramObject);
        aav_shaderProgramObject = null;
    }
}

function keyDown(event)
{
    //code
    switch(event.keyCode)
    {
        case 27: // escape 
            //unitialize
            uninitialize();
            //close our application's tab
            window.close(); // may not work in firefox but work in safari and chrome 
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
        case 76:
        case 108:
        if (aav_bLight == false)
        {
            aav_bLight = true;
        }
        else
        {
             aav_bLight = false;
        }
        break;
        default:
        break;
    }
}

function mouseDown()
{
    //code
}