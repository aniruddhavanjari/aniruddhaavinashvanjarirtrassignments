//global variable
var gl = null;  // webgl context
var aav_bFullscreen = false;
var aav_canvas_orignal_width;
var aav_canvas_orignal_height;

const WebGLMacros = 
{
    AAV_ATTRIBUTE_VERTEX:0,
    AAV_ATTRIBUTE_COLOR:1,
    AAV_ATTRIBUTE_NORMAL:2,
    AAV_ATTRIBUTE_TEXTURE:3
};

var aav_vertexShaderObject;
var aav_fragmentShaderObject;
var aav_shaderProgramObject;

var aav_vao_pyramid;
var aav_vbo_position_pyramid;
var aav_vbo_color_pyramid;
var aav_vbo_texture_pyramid;

var aav_vao_cube;
var aav_vbo_position_cube;
var aav_vbo_color_cube;
var aav_vbo_texture_cube;

var aav_mvpUniform;

var perspectiveProjectionMatrix;

var aav_anglePyramid = 0;
var aav_angleCube = 0;

//To start animation: To have requestAnimation() to be called "cross-browser" compatible
var aav_requestAnimationFrame = 
window.requestAnimationFrame || 
window.webkitRequestAnimationFrame || 
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame;

//To stop animation : to have aav_cancelAnimationFrame() to be called "cross-browser" compatible
var aav_cancelAnimationFrame = 
window.aav_cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame || 
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || 
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

//texture
var aav_stoneTexture ;
var aav_marbleTexture ;
var aav_textureSamplerUniform;

var aav_modelMatrixUniform; 
var aav_viewMatrixUniform;
var aav_ProjectionMatrixUniform;

//Light Array
var aav_lightAmbiant = [0.0, 0.0, 0.0];	//la
var aav_lightDiffuse = [1.0, 1.0, 1.0];	// ld
var aav_lightSpecular = [1.0, 1.0, 1.0];//ls

var aav_lightPosition = [0.0, 0.0, -2.0, 1.0];

//material Array 
var aav_materialAmbiant = [0.0, 0.0, 0.0]; // ka 
var aav_materialDiffuse = [1.0, 1.0, 1.0];	// kd
var aav_materialSpecular = [1.0, 1.0, 1.0];	// ks


// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AAV");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    aav_canvas_orignal_width = canvas.width;
    aav_canvas_orignal_height = canvas.height;

    //register keyboard's keydown event handler
    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",resize,false);

    //initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    //code 
    var aav_fullscreen_element = 
    document.fullscreenElement || 
    document.webkitFullscreenElement || 
    document.mozFullscreenElement || 
    document.msFullscreenElement || 
    null;

    //if not fullscreen
    if(aav_fullscreen_element == null)
    {
        if(canvas.requestFullScree)
            canvas.requestFullScree();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();        
        aav_bFullscreen = true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        aav_bFullscreen =false;
    }
}

function init()
{
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");
    if(gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shader 
    var aav_vertexShaderSourcedCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "in vec4 vColor;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_view_matrix;" +
    "uniform mat4 u_model_matrix;" +
    "uniform mat4 u_projection_matrix;" +
    "in vec2 vTexCoord;" +
    "out vec4 aav_out_color;"+
        "out vec2 aav_out_TexCoord;" +
        "uniform vec4 u_lightPosistion;" +
        "out vec3 tranformed_normal;" +
        "out vec3 lightDirection;" +
        "out vec3 view_vector;" +
    "void main(void)"+ 
    "{"+
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;"+
    "aav_out_color = vColor;"+
        "aav_out_TexCoord = vTexCoord; " +
        "		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" +
        "		tranformed_normal = (mat3(u_view_matrix * u_model_matrix) * vNormal);" +
        "		lightDirection = (vec3(u_lightPosistion - eyeCordinate));" +
        "		view_vector = (-eyeCordinate.xyz);" +

    "}";
    aav_vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(aav_vertexShaderObject,aav_vertexShaderSourcedCode);
    gl.compileShader(aav_vertexShaderObject);
    if(gl.getShaderParameter(aav_vertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(aav_vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragemnt shader
    var aav_fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+ 
    "precision highp float;"+
    "in vec4 aav_out_color;"+
    "in vec2 aav_out_TexCoord;"+
        "uniform highp sampler2D u_texture_sampler;" +

        "in vec3 tranformed_normal;" +
        "in vec3 lightDirection;" +
        "in vec3 view_vector;" +

        "uniform vec3 u_la;" +
        "uniform vec3 u_ld;" +
        "uniform vec3 u_ls;" +

        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_kShineness;" +

        "out vec4 FragColor;" +
        "vec3 fong_ads_light;" +

    "void main(void)"+
        "{" +
        "		vec3 normalize_tranformed_normal = normalize(tranformed_normal);" +
        "		vec3 normalize_lightDirection = normalize(lightDirection);" +
        "		vec3 normalize_view_vector = normalize(view_vector);" +
        "		vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" +
        "		vec3 ambiant = u_la * u_ka;" +
        "		vec3 diffuse = u_ld * u_kd * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" +
        "		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness);" +
        "		fong_ads_light = ambiant + diffuse + specular;" +
        "       fong_ads_light = fong_ads_light * vec3(texture(u_texture_sampler,aav_out_TexCoord));"+
    "FragColor = vec4(fong_ads_light,1.0f)  *  aav_out_color;"+
    "}";
    aav_fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(aav_fragmentShaderObject,aav_fragmentShaderSourceCode);
    gl.compileShader(aav_fragmentShaderObject);
    if(gl.getShaderParameter(aav_fragmentShaderObject,gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(aav_fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program 
    aav_shaderProgramObject = gl.createProgram();
    gl.attachShader(aav_shaderProgramObject,aav_vertexShaderObject);
    gl.attachShader(aav_shaderProgramObject,aav_fragmentShaderObject);

    //pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_COLOR,"vColor");
    gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_TEXTURE,"vTexCoord");
    //linking 
    gl.linkProgram(aav_shaderProgramObject);
    if(!gl.getProgramParameter(aav_shaderProgramObject,gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(aav_shaderProgramObject);
        if(error.length >  0)
        {
            alert(error);
            uninitialize();
        }
    }

    //get MVP uniform location
    aav_modelMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_model_matrix");
    aav_viewMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_view_matrix");
    aav_ProjectionMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_projection_matrix");

    aav_laUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_la");
    aav_ldUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ld");
    aav_lsUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ls");
    aav_lightPositionUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_lightPosistion");

    aav_kaUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ka");
    aav_kdUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_kd");
    aav_ksUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ks");

    aav_kShininessUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_kShineness");


    aav_textureSamplerUniform	 = gl.getUniformLocation(aav_shaderProgramObject, "u_texture_sampler");

    // ** vertices , color , shader attribs, vbo initialization***
  
    var aav_cubaAll = new Float32Array([
        0.5, 0.5, 0.5, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
        -0.5, 0.5, 0.5, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0,
        -0.5, -0.5, 0.5, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0,
        0.5, -0.5, 0.5, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0,

        0.5, 0.5, -0.5, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0,
        0.5, 0.5, 0.5, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 0.0,
        0.5, -0.5, 0.5, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0,
        0.5, -0.5, -0.5, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0,

        0.5, 0.5, -0.5, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0,
        -0.5, 0.5, -0.5, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 1.0, 0.0,
        -0.5, -0.5, -0.5, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 1.0, 1.0,
        0.5, -0.5, -0.5, 0.0, 0.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0,

        -0.5, 0.5, -0.5, 0.0, 1.0, 1.0, -1.0, 0.0, 0.0, 0.0, 0.0,
        -0.5, 0.5, 0.5, 0.0, 1.0, 1.0, -1.0, 0.0, 0.0, 1.0, 0.0,
        -0.5, -0.5, 0.5, 0.0, 1.0, 1.0, -1.0, 0.0, 0.0, 1.0, 1.0,
        -0.5, -0.5, -0.5, 0.0, 1.0, 1.0, -1.0, 0.0, 0.0, 0.0, 1.0,

        0.5, 0.5, -0.5, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0,
        -0.5, 0.5, -0.5, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0,
        -0.5, 0.5, 0.5, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0,
        0.5, 0.5, 0.5, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0,

        0.5, -0.5, -0.5, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0,
        -0.5, -0.5, -0.5, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0,
        -0.5, -0.5, 0.5, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 1.0,
        0.5, -0.5, 0.5, 1.0, 1.0, 0.0, 0.0, -1.0, 0.0, 0.0, 1.0
    ]);

    //Cube vao
    aav_vao_cube = gl.createVertexArray();
    gl.bindVertexArray(aav_vao_cube);

    //cube position
    aav_vbo_position_cube = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_vbo_position_cube);
    gl.bufferData(gl.ARRAY_BUFFER, aav_cubaAll, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
        3,
        gl.FLOAT,
        false,
        11 * 4, 0);
    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);

    //color
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_COLOR,
        3,
        gl.FLOAT,
        false,
        11 * 4, 3 * 4);
    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_COLOR);

    //normal
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_NORMAL,
        3,
        gl.FLOAT,
        false,
        11 * 4, 6 * 4);
    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_NORMAL);

    //texture
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_TEXTURE,
        2,
        gl.FLOAT,
        false,
        11 * 4, 9 * 4);
    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_TEXTURE);
   
    gl.bindBuffer(gl.ARRAY_BUFFER,null);

    gl.bindVertexArray(null);

    aav_marbleTexture = LoadGLTexture("marble.bmp");
    //set clear color 
    gl.clearColor(0.0,0.0,0.0,1.0); //blue 

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
   
    //inidialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

//Loading texture Function
function LoadGLTexture(textureFileName)
{
    var aav_texture= gl.createTexture();
    aav_texture.image = new Image();
    aav_texture.image.src = textureFileName;
    //lamda , blog , closer
    aav_texture.image.onload = function()
    {
        gl.bindTexture(gl.TEXTURE_2D,aav_texture);
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL,1);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        
        gl.texImage2D(gl.TEXTURE_2D,0,gl.RGBA,gl.RGBA,gl.UNSIGNED_BYTE,
            aav_texture.image);
            gl.bindTexture(gl.TEXTURE_2D,null);
    };
   return aav_texture;
}


function resize()
{
    //code 
    if(aav_bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = aav_canvas_orignal_width;
        canvas.height = aav_canvas_orignal_height;
    }

    //set the viewport to match
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);

}

function draw()
{
    //variable declarations
    var aav_modelMateix = mat4.create();
    var aav_transformMatrix = mat4.create();
    var aav_viewMatrix = mat4.create();

    var aav_transformMatrix = mat4.create();
    var aav_rotationMatrix  = mat4.create(); 
   
    //code 
    gl.clear(gl.COLOR_BUFFER_BIT|  gl.DEPTH_BUFFER_BIT);
    
    gl.useProgram(aav_shaderProgramObject);

    //Light
    gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
    gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
    gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
    gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

    //material
    gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
    gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
    gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
    gl.uniform1f(aav_kShininessUniform, 128.0);

    /*********Cube*************/
    //aav_vao_cube
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    aav_transformMatrix = mat4.create();
    aav_scaleMatrix = mat4.create();
    aav_rotationMatrix  = mat4.create(); 
   
    mat4.translate(aav_transformMatrix,aav_transformMatrix,[0.0,0.0,-6.0]);
    mat4.rotateX(aav_rotationMatrix,aav_rotationMatrix,degToRad(aav_angleCube));
    mat4.rotateY(aav_rotationMatrix,aav_rotationMatrix,degToRad(aav_angleCube));
    mat4.rotateZ(aav_rotationMatrix,aav_rotationMatrix,degToRad(aav_angleCube));
    mat4.multiply(aav_modelMateix,aav_scaleMatrix,aav_rotationMatrix);
    mat4.multiply(aav_modelMateix, aav_transformMatrix, aav_modelMateix);
    

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D,aav_marbleTexture);
    gl.uniform1i(aav_textureSamplerUniform, 0);

    gl.bindVertexArray(aav_vao_cube);
    gl.drawArrays(gl.TRIANGLE_FAN,0,4);
    gl.drawArrays(gl.TRIANGLE_FAN,4,4);
    gl.drawArrays(gl.TRIANGLE_FAN,8,4);
    gl.drawArrays(gl.TRIANGLE_FAN,12,4);
    gl.drawArrays(gl.TRIANGLE_FAN,16,4);
    gl.drawArrays(gl.TRIANGLE_FAN,20,4);
    gl.drawArrays(gl.TRIANGLE_FAN,24,4);
    gl.bindVertexArray(null);

    gl.useProgram(null);
    
    //animation loop
    aav_anglePyramid = aav_anglePyramid + 1;
    if(aav_anglePyramid > 360)
    {
       aav_anglePyramid = 0;
    } 
    aav_angleCube = aav_angleCube + 1;
    if(aav_angleCube > 360)
    {
        aav_angleCube = 0;
    } 
  
    requestAnimationFrame(draw, canvas);
}

function degToRad(degree)
{
    return(degree * Math.PI/180.0);
}

function uninitialize()
{
    //code
    if(aav_vao_pyramid)
    {
        gl.deleteVertexArary(aav_vao_pyramid);
        aav_vao_pyramid = null;
    }

    if(aav_vbo_position_pyramid)
    {
        gl.deleteBuffer(aav_vbo_position_pyramid);
        aav_vbo_position_pyramid = null;
    }

    if(aav_vbo_color_pyramid)
    {
        gl.deleteBuffer(aav_vbo_color_pyramid);
        aav_vbo_color_pyramid = null;
    }
    
    if(aav_vbo_texture_pyramid)
    {
        gl.deleteBuffer(aav_vbo_texture_pyramid);
        aav_vbo_texture_pyramid = null;
    }

    if(aav_vao_cube)
    {
        gl.deleteVertexArary(aav_vao_cube);
        aav_vao_cube = null;
    }

    if(aav_vbo_position_cube)
    {
        gl.deleteBuffer(aav_vbo_position_cube);
        aav_vbo_position_cube = null;
    }

    if(aav_vbo_color_cube)
    {
        gl.deleteBuffer(aav_vbo_color_cube);
        aav_vbo_color_cube = null;
    }

    if(aav_vbo_texture_cube)
    {
        gl.deleteBuffer(aav_vbo_texture_cube);
        aav_vbo_texture_cube = null;
    }

	if (aav_stoneTexture)
	{
        gl.deleteTextures(1, aav_stoneTexture);
	}

	if (aav_marbleTexture)
	{
        gl.deleteTextures(1, aav_marbleTexture);
	}


    if(aav_shaderProgramObject)
    {
        if(aav_fragmentShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject,aav_fragmentShaderObject);
            gl.deleteShader(aav_fragmentShaderObject);
            aav_fragmentShaderObject = null;
        }

        if(aav_vertexShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject,aav_vertexShaderObject);
            gl.deleteShader(aav_vertexShaderObject);
            aav_vertexShaderObject = null;
        }

        gl.deleteProgram(aav_shaderProgramObject);
        aav_shaderProgramObject = null;
    }
}

function keyDown(event)
{
    //code
    switch(event.keyCode)
    {
        case 27: // escape 
            //unitialize
            uninitialize();
            //close our application's tab
            window.close(); // may not work in firefox but work in safari and chrome 
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}

function mouseDown()
{
    //code
}