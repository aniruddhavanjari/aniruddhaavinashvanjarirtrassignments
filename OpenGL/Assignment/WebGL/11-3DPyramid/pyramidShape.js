//global variable
var aav_gl = null;  // webgl context
var aav_bFullscreen = false;
var aav_canvas_orignal_width;
var aav_canvas_orignal_height;

const WebGLMacros = 
{
    AAV_ATTRIBUTE_VERTEX:0,
    AAV_ATTRIBUTE_COLOR:1,
    AAV_ATTRIBUTE_NORMAL:2,
    AAV_ATTRIBUTE_TEXTURE:3
};


var aav_vertexShaderObject;
var aav_fragmentShaderObject;
var aav_shaderProgramObject;

var aav_vao_pyramid_square;
var aav_vbo_position_pyramid_square;
var aav_vbo_color_pyramid_square;

var aav_vao_pyramid;
var aav_vbo_position_pyramid;
var aav_vbo_color_pyramid;

var aav_mvpUniform;

var perspectiveProjectionMatrix;

var aav_anglePyramid = 0;
var aav_angleSquare = 0;

//To start animation: To have requestAnimation() to be called "cross-browser" compatible
var aav_requestAnimationFrame = 
window.requestAnimationFrame || 
window.webkitRequestAnimationFrame || 
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame;

//To stop animation : to have aav_cancelAnimationFrame() to be called "cross-browser" compatible
var aav_cancelAnimationFrame = 
window.aav_cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame || 
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || 
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AAV");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    aav_canvas_orignal_width = canvas.width;
    aav_canvas_orignal_height = canvas.height;

    //register keyboard's keydown event handler
    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",resize,false);

    //initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    //code 
    var aav_fullscreen_element = 
    document.fullscreenElement || 
    document.webkitFullscreenElement || 
    document.mozFullscreenElement || 
    document.msFullscreenElement || 
    null;

    //if not fullscreen
    if(aav_fullscreen_element == null)
    {
        if(canvas.requestFullScree)
            canvas.requestFullScree();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();        
        aav_bFullscreen = true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        aav_bFullscreen =false;
    }
}

function init()
{
    // code 
    // get WebGL 2.0 context 
    aav_gl = canvas.getContext("webgl2");
    if(aav_gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    aav_gl.viewportWidth = canvas.width;
    aav_gl.viewportHeight = canvas.height;

    //vertex shader 
    var aav_vertexShaderSourcedCode = 
    "#version 300 es"+
    "\n"+
    "in vec4 vPosition;"+
    "uniform mat4 u_mvp_matrix;"+
    "void main(void)"+ 
    "{"+
    "gl_Position = u_mvp_matrix * vPosition;"+
    "}";
    aav_vertexShaderObject = aav_gl.createShader(aav_gl.VERTEX_SHADER);
    aav_gl.shaderSource(aav_vertexShaderObject,aav_vertexShaderSourcedCode);
    aav_gl.compileShader(aav_vertexShaderObject);
    if(aav_gl.getShaderParameter(aav_vertexShaderObject,aav_gl.COMPILE_STATUS)== false)
    {
        var error = aav_gl.getShaderInfoLog(aav_vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragemnt shader
    var aav_fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+ 
    "precision highp float;"+
    "out vec4 FragColor;"+
    "void main(void)"+
    "{"+
    "FragColor = vec4(1.0,1.0,1.0,1.0);"+
    "}";
    aav_fragmentShaderObject = aav_gl.createShader(aav_gl.FRAGMENT_SHADER);
    aav_gl.shaderSource(aav_fragmentShaderObject,aav_fragmentShaderSourceCode);
    aav_gl.compileShader(aav_fragmentShaderObject);
    if(aav_gl.getShaderParameter(aav_fragmentShaderObject,aav_gl.COMPILE_STATUS) == false)
    {
        var error = aav_gl.getShaderInfoLog(aav_fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program 
    aav_shaderProgramObject = aav_gl.createProgram();
    aav_gl.attachShader(aav_shaderProgramObject,aav_vertexShaderObject);
    aav_gl.attachShader(aav_shaderProgramObject,aav_fragmentShaderObject);

    //pre-link binding of shader program object with vertex shader attributes
    aav_gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");

    //linking 
    aav_gl.linkProgram(aav_shaderProgramObject);
    if(!aav_gl.getProgramParameter(aav_shaderProgramObject,aav_gl.LINK_STATUS))
    {
        var error = aav_gl.getProgramInfoLog(aav_shaderProgramObject);
        if(error.length >  0)
        {
            alert(error);
            uninitialize();
        }
    }

    //get MVP uniform location
    aav_mvpUniform = aav_gl.getUniformLocation(aav_shaderProgramObject,"u_mvp_matrix");

    // ** vertices , color , shader attribs, vbo initialization***
  
    var aav_pyramidVerteis = new Float32Array([
        0.0,0.5,0.0,
        -0.5,-0.5,0.5,
        0.5,-0.5,0.5,
        
        0.0,0.5,0.0,
        0.5,-0.5,0.5,
        0.5,-0.5,-0.5,
    
        0.0,0.5,0.0,
        -0.5,-0.5,-0.5,
        0.5,-0.5,-0.5,
    
        0.0,0.5,0.0,
        -0.5,-0.5,0.5,
        -0.5,-0.5,-0.5
                                                ]);
    

    aav_vao_pyramid = aav_gl.createVertexArray();
    aav_gl.bindVertexArray(aav_vao_pyramid);

    aav_vbo_position_pyramid = aav_gl.createBuffer();
    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER, aav_vbo_position_pyramid);
    aav_gl.bufferData(aav_gl.ARRAY_BUFFER, aav_pyramidVerteis, aav_gl.STATIC_DRAW);
    aav_gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
                                            3, // 3 is for x,y,z co-cordinates is our triangle Verteices array
                                            aav_gl.FLOAT,
                                            false,
                                            0,0);

    aav_gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    aav_gl.bindBuffer(aav_gl.ARRAY_BUFFER,null);

    aav_gl.bindVertexArray(null);

    //set clear color 
    aav_gl.clearColor(0.0,0.0,0.0,1.0); //blue 

    aav_gl.enable(aav_gl.DEPTH_TEST);
    aav_gl.depthFunc(aav_gl.LEQUAL);
   
    //inidialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    //code 
    if(aav_bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = aav_canvas_orignal_width;
        canvas.height = aav_canvas_orignal_height;
    }

    //set the viewport to match
    aav_gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);

}

function draw()
{
    //variable declarations
    var aav_modelViewMatrix = mat4.create();
    var aav_transformMatrix = mat4.create();
    var aav_rotationMatrix  = mat4.create(); 
    var aav_modelViewProjectionMatrix = mat4.create();
    //code 
    aav_gl.clear(aav_gl.COLOR_BUFFER_BIT|  aav_gl.DEPTH_BUFFER_BIT);
    
    aav_gl.useProgram(aav_shaderProgramObject);

    /*********pyramid************/
    aav_modelViewMatrix = mat4.create();
    aav_transformMatrix = mat4.create();
    aav_rotationMatrix  = mat4.create(); 
    aav_modelViewProjectionMatrix = mat4.create(); // tayar hi honar and indentity matrix la inidilization pan karnar  

    mat4.translate(aav_transformMatrix,aav_transformMatrix,[0.0,0.0,-3.0]);

    mat4.rotateY(aav_rotationMatrix,aav_rotationMatrix,degToRad(aav_anglePyramid));
   
    mat4.multiply(aav_modelViewMatrix,aav_transformMatrix,aav_rotationMatrix);

    mat4.multiply(aav_modelViewProjectionMatrix, perspectiveProjectionMatrix, aav_modelViewMatrix);

    aav_gl.uniformMatrix4fv(aav_mvpUniform, false, aav_modelViewProjectionMatrix);

    aav_gl.bindVertexArray(aav_vao_pyramid);

    aav_gl.drawArrays(aav_gl.TRIANGLES,0,12);

    aav_gl.bindVertexArray(null);


    aav_gl.useProgram(null);
    
    //animation loop
    aav_anglePyramid = aav_anglePyramid + 1;
    if(aav_anglePyramid > 360)
    {
       aav_anglePyramid = 0;
    } 
  
    requestAnimationFrame(draw, canvas);
}


function degToRad(degree)
{
    return(degree * Math.PI/180.0);
}


function uninitialize()
{
    //code
       if(aav_vao_pyramid)
    {
        aav_gl.deleteVertexArary(aav_vao_pyramid);
        aav_vao_pyramid = null;
    }

    if(aav_vbo_position_pyramid)
    {
        aav_gl.deleteBuffer(aav_vbo_position_pyramid);
        aav_vbo_position_pyramid = null;
    }

    if(aav_vbo_color_pyramid)
    {
        aav_gl.deleteBuffer(aav_vbo_color_pyramid);
        aav_vbo_color_pyramid = null;
    }

    if(aav_shaderProgramObject)
    {
        if(aav_fragmentShaderObject)
        {
            aav_gl.detachShader(aav_shaderProgramObject,aav_fragmentShaderObject);
            aav_gl.deleteShader(aav_fragmentShaderObject);
            aav_fragmentShaderObject = null;
        }

        if(aav_vertexShaderObject)
        {
            aav_gl.detachShader(aav_shaderProgramObject,aav_vertexShaderObject);
            aav_gl.deleteShader(aav_vertexShaderObject);
            aav_vertexShaderObject = null;
        }

        aav_gl.deleteProgram(aav_shaderProgramObject);
        aav_shaderProgramObject = null;
    }
}

function keyDown(event)
{
    //code
    switch(event.keyCode)
    {
        case 27: // escape 
            //unitialize
            uninitialize();
            //close our application's tab
            window.close(); // may not work in firefox but work in safari and chrome 
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
    }
}

function mouseDown()
{
    //code
}