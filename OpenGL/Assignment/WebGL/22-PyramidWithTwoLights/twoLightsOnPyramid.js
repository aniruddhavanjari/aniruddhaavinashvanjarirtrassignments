//global variable
var gl = null;  // webgl context
var aav_bFullscreen = false;
var aav_canvas_orignal_width;
var aav_canvas_orignal_height;

const WebGLMacros = 
{
    AAV_ATTRIBUTE_VERTEX:0,
    AAV_ATTRIBUTE_COLOR:1,
    AAV_ATTRIBUTE_NORMAL:2,
    AAV_ATTRIBUTE_TEXTURE:3
};

var aav_vertexShaderObject;
var aav_fragmentShaderObject;
var aav_shaderProgramObject;

var aav_vao_pyramid;
var aav_vbo_position_pyramid;
var aav_vbo_normal_pyramid;

var aav_perspectiveProjectionMatrix;

var aav_angleCube = 0;

var aav_viewMatrixUniform;
var aav_modelMatrixUniform;
var aav_ProjectionMatrixUniform;

var aav_laUniform = new Array(2);
var aav_ldUniform = new Array(2);
var aav_lsUniform = new Array(2);
var aav_lightPositionUniform = new Array(2);

var aav_kaUniform;
var aav_kdUniform;
var aav_ksUniform;

var aav_kShininessUniform;

var aav_lKeyPressedUniform;

var aav_bLight = false;

function aav_Light()
{
    this.aav_lightAmbiant = new Float32Array(3);
    this.aav_lightDiffuse = new Float32Array(3);
    this.aav_lightSpecular = new Float32Array(3);
    this.aav_lightPosition = new Float32Array(4);
};

var aav_light = [];

function aav_Material()
{
    this.aav_materialAmbiant = new Float32Array(3);  // ka 
    this.aav_materialDiffuse = new Float32Array(3);// kd
    this.aav_materialSpecular = new Float32Array(3);// ks
    this.aav_materialShininess;
}

var aav_material;

//To start animation: To have requestAnimation() to be called "cross-browser" compatible
var aav_requestAnimationFrame = 
window.requestAnimationFrame || 
window.webkitRequestAnimationFrame || 
window.mozRequestAnimationFrame || 
window.oRequestAnimationFrame || 
window.msRequestAnimationFrame;

//To stop animation : to have aav_cancelAnimationFrame() to be called "cross-browser" compatible
var aav_cancelAnimationFrame = 
window.aav_cancelAnimationFrame||
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame || 
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame || 
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AAV");
    if(!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    aav_canvas_orignal_width = canvas.width;
    aav_canvas_orignal_height = canvas.height;

    //register keyboard's keydown event handler
    window.addEventListener("keydown",keyDown, false);
    window.addEventListener("click",mouseDown,false);
    window.addEventListener("resize",resize,false);

    //initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    //code 
    var aav_fullscreen_element = 
    document.fullscreenElement || 
    document.webkitFullscreenElement || 
    document.mozFullscreenElement || 
    document.msFullscreenElement || 
    null;

    //if not fullscreen
    if(aav_fullscreen_element == null)
    {
        if(canvas.requestFullScree)
            canvas.requestFullScree();
        else if(canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if(canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if(canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();        
        aav_bFullscreen = true;
    }
    else // if already fullscreen
    {
        if(document.exitFullscreen)
            document.exitFullscreen();
        else if(document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if(document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if(document.msExitFullscreen)
            document.msExitFullscreen();
        aav_bFullscreen =false;
    }
}

function init()
{
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");
    if(gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shader 
    var aav_vertexShaderSourcedCode = 
        "#version 300 es"+
        "\n"+
        "precision highp float;"+
        "precision mediump int;"+
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform int u_lKeyPressed;" +
        "uniform vec3 u_la[2];" +
        "uniform vec3 u_ld[2];" +
        "uniform vec3 u_ls[2];" +
        "uniform vec4 u_lightPosistion[2];" +
        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_kShineness;" +

        "vec3 lightDirection[2];" +
        "vec3 reflection_vector[2];" +
        "vec3 ambiant[2];" +
        "vec3 diffuse[2];" +
        "vec3 specular[2];" +

        "out vec3 fong_ads_light;" +
        "void main(void)" +
        "{" +
        "	if(u_lKeyPressed == 1)" +
        "	{" +
        "		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition; " +
        "		vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal); " +
        "		vec3 view_vector = normalize(-eyeCordinate.xyz);" +
        "		for(int i = 0 ; i < 2 ; i++)" +
        "		{" +
        "			lightDirection[i] = normalize(vec3(u_lightPosistion[i] - eyeCordinate));" +
        "			reflection_vector[i] = reflect(-lightDirection[i],tranformed_normal);" +
        "			ambiant[i] = u_la[i] * u_ka;" +
        "			diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i],tranformed_normal),0.0f);" +
        "			specular[i] = u_ls[i] * u_ks * pow(max(dot(reflection_vector[i],view_vector),0.0f),u_kShineness);" +
        "			fong_ads_light = fong_ads_light + ambiant[i] + diffuse[i] + specular[i];" +
        "		}" +
        "	}" +
        "	else" +
        "	{" +
        "		fong_ads_light = vec3(1.0f,1.0f,1.0f);" +
        "	}" +
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";
    aav_vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(aav_vertexShaderObject,aav_vertexShaderSourcedCode);
    gl.compileShader(aav_vertexShaderObject);
    if(gl.getShaderParameter(aav_vertexShaderObject,gl.COMPILE_STATUS)== false)
    {
        var error = gl.getShaderInfoLog(aav_vertexShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragemnt shader
    var aav_fragmentShaderSourceCode = 
    "#version 300 es"+
    "\n"+ 
    "precision highp float;"+
    "in vec3 fong_ads_light;" +
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "	FragColor = vec4(fong_ads_light,1.0f);" +
    "}";
    aav_fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(aav_fragmentShaderObject,aav_fragmentShaderSourceCode);
    gl.compileShader(aav_fragmentShaderObject);
    if(gl.getShaderParameter(aav_fragmentShaderObject,gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(aav_fragmentShaderObject);
        if(error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program 
    aav_shaderProgramObject = gl.createProgram();
    gl.attachShader(aav_shaderProgramObject,aav_vertexShaderObject);
    gl.attachShader(aav_shaderProgramObject,aav_fragmentShaderObject);

    //pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(aav_shaderProgramObject,WebGLMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");
    gl.bindAttribLocation(aav_shaderProgramObject, WebGLMacros.AAV_ATTRIBUTE_NORMAL, "vNormal");

    //linking 
    gl.linkProgram(aav_shaderProgramObject);
    if(!gl.getProgramParameter(aav_shaderProgramObject,gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(aav_shaderProgramObject);
        if(error.length >  0)
        {
            alert(error);
            uninitialize();
        }
    }

    //get MVP uniform location

    aav_modelMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_model_matrix");
	aav_viewMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_view_matrix");
	aav_ProjectionMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_projection_matrix");

	aav_laUniform[0] = gl.getUniformLocation(aav_shaderProgramObject, "u_la[0]");
	aav_ldUniform[0] = gl.getUniformLocation(aav_shaderProgramObject, "u_ld[0]");
	aav_lsUniform[0] = gl.getUniformLocation(aav_shaderProgramObject, "u_ls[0]");
	aav_lightPositionUniform[0] = gl.getUniformLocation(aav_shaderProgramObject, "u_lightPosistion[0]");


    aav_laUniform[1] = gl.getUniformLocation(aav_shaderProgramObject, "u_la[1]");
    aav_ldUniform[1] = gl.getUniformLocation(aav_shaderProgramObject, "u_ld[1]");
    aav_lsUniform[1] = gl.getUniformLocation(aav_shaderProgramObject, "u_ls[1]");
    aav_lightPositionUniform[1] = gl.getUniformLocation(aav_shaderProgramObject, "u_lightPosistion[1]");


	aav_kaUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ka");
	aav_kdUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_kd");
	aav_ksUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ks");

	aav_kShininessUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_kShineness");

	aav_lKeyPressedUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_lKeyPressed");

    // ** vertices , color , shader attribs, vbo initialization***

    var aav_cubeVerteis = new Float32Array([
        0.0, 1.0, 0.0,
        -1.0, -1.0, 1.0,
        1.0, -1.0, 1.0,

        0.0, 1.0, 0.0,
        1.0, -1.0, 1.0,
        1.0, -1.0, -1.0,

        0.0, 1.0, 0.0,
        -1.0, -1.0, -1.0,
        1.0, -1.0, -1.0,

        0.0, 1.0, 0.0,
        -1.0, -1.0, 1.0,
        -1.0, -1.0, -1.0
    ]);

    var aav_cubeNormals = new Float32Array([
        0.0, 0.447214, 0.894427,
        0.0, 0.447214, 0.894427,
        0.0, 0.447214, 0.894427,

        0.894427, 0.447214, 0.0,
        0.894427, 0.447214, 0.0,
        0.894427, 0.447214, 0.0,

        0.0, 0.447214, -0.894427,
        0.0, 0.447214, -0.894427,
        0.0, 0.447214, -0.894427,

        -0.894427, 0.447214, 0.0,
        -0.894427, 0.447214, 0.0,
        -0.894427, 0.447214, 0.0
    ]);

    //cube vao
    aav_vao_pyramid = gl.createVertexArray();
    gl.bindVertexArray(aav_vao_pyramid);

    //cube vbo vertice
    aav_vbo_position_pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_vbo_position_pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, aav_cubeVerteis, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_VERTEX,
        3, // 3 is for x,y,z co-cordinates is our triangle Verteices array
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_VERTEX);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    //cube vbo normals

    aav_vbo_normal_pyramid = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, aav_vbo_normal_pyramid);
    gl.bufferData(gl.ARRAY_BUFFER, aav_cubeNormals, gl.STATIC_DRAW);
    gl.vertexAttribPointer(WebGLMacros.AAV_ATTRIBUTE_NORMAL,
        3,
        gl.FLOAT,
        false,
        0, 0);

    gl.enableVertexAttribArray(WebGLMacros.AAV_ATTRIBUTE_NORMAL);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    gl.bindVertexArray(null);


   
    aav_light[0] = new aav_Light();
    aav_light[1] = new aav_Light();
    aav_light[0].aav_lightAmbiant[0] = 0.0;
    aav_light[0].aav_lightAmbiant[1] = 0.0;
    aav_light[0].aav_lightAmbiant[2] = 0.0;

    aav_light[0].aav_lightDiffuse[0] = 1.0;
    aav_light[0].aav_lightDiffuse[1] = 0.0;
    aav_light[0].aav_lightDiffuse[2] = 0.0;

    aav_light[0].aav_lightSpecular[0] = 1.0;
    aav_light[0].aav_lightSpecular[1] = 0.0;
    aav_light[0].aav_lightSpecular[2] = 0.0;

    aav_light[0].aav_lightPosition[0] = -2.0;
    aav_light[0].aav_lightPosition[1] = 0.0;
    aav_light[0].aav_lightPosition[2] = 0.0;
    aav_light[0].aav_lightPosition[3] = 1.0;

    aav_light[1].aav_lightAmbiant[0] = 0.0;
    aav_light[1].aav_lightAmbiant[1] = 0.0;
    aav_light[1].aav_lightAmbiant[2] = 0.0;

    aav_light[1].aav_lightDiffuse[0] = 0.0;
    aav_light[1].aav_lightDiffuse[1] = 0.0;
    aav_light[1].aav_lightDiffuse[2] = 1.0;

    aav_light[1].aav_lightSpecular[0] = 0.0;
    aav_light[1].aav_lightSpecular[1] = 0.0;
    aav_light[1].aav_lightSpecular[2] = 1.0;

    aav_light[1].aav_lightPosition[0] = 2.0;
    aav_light[1].aav_lightPosition[1] = 0.0;
    aav_light[1].aav_lightPosition[2] = 0.0;
    aav_light[1].aav_lightPosition[3] = 1.0;

    //material

    aav_material = new aav_Material();
    aav_material.aav_materialAmbiant[0] = 0.0;
    aav_material.aav_materialAmbiant[1] = 0.0;
    aav_material.aav_materialAmbiant[2] = 0.0;

    aav_material.aav_materialDiffuse[0] = 1.0;
    aav_material.aav_materialDiffuse[1] = 1.0;
    aav_material.aav_materialDiffuse[2] = 1.0;

    aav_material.aav_materialSpecular[0] = 1.0;
    aav_material.aav_materialSpecular[1] = 1.0;
    aav_material.aav_materialSpecular[2] = 1.0;

    aav_material.aav_materialShininess = 128.0;
    //set clear color 
    gl.clearColor(0.0,0.0,0.0,1.0); //blue 

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
   
    //inidialize projection matrix
    aav_perspectiveProjectionMatrix = mat4.create();
}

function resize()
{
    //code 
    if(aav_bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = aav_canvas_orignal_width;
        canvas.height = aav_canvas_orignal_height;
    }

    //set the viewport to match
    gl.viewport(0,0,canvas.width,canvas.height);

    mat4.perspective(aav_perspectiveProjectionMatrix,45.0,parseFloat(canvas.width)/parseFloat(canvas.height),0.1,100.0);

}

function draw()
{
    //variable declarations
    var aav_modelMateix;
    var aav_viewMatrix;
    var aav_rotationMatrix;
    var aav_transformMatrix ;

   
    //code 
    gl.clear(gl.COLOR_BUFFER_BIT|  gl.DEPTH_BUFFER_BIT);
    
    gl.useProgram(aav_shaderProgramObject);


    if (aav_bLight == true)
	{ 
		gl.uniform1i(aav_lKeyPressedUniform,1);
		//aav_Light
        gl.uniform3fv(aav_laUniform[0], aav_light[0].aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform[0], aav_light[0].aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform[0], aav_light[0].aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform[0], aav_light[0].aav_lightPosition);	//lightPosition

        gl.uniform3fv(aav_laUniform[1], aav_light[1].aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform[1], aav_light[1].aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform[1], aav_light[1].aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform[1], aav_light[1].aav_lightPosition);	//lightPosition

		//material
        gl.uniform3fv(aav_kaUniform, aav_material.aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_material.aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_material.aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_material.aav_materialShininess);
	}
	else
	{
		gl.uniform1i(aav_lKeyPressedUniform, 0);
	}

    aav_modelMateix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_transformMatrix  = mat4.create(); 
    aav_rotationMatrix = mat4.create();

    mat4.translate(aav_modelMateix,aav_transformMatrix,[0.0,0.0,-6.0]);

    mat4.rotateY(aav_rotationMatrix, aav_rotationMatrix, degToRad(aav_angleCube));

    mat4.multiply(aav_modelMateix, aav_modelMateix, aav_rotationMatrix);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);
    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);
    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, aav_perspectiveProjectionMatrix);

    gl.bindVertexArray(aav_vao_pyramid);

    gl.drawArrays(gl.TRIANGLES, 0, 12);
   

    gl.bindVertexArray(null);

    gl.useProgram(null);
    
    //animation loop
    aav_angleCube = aav_angleCube + 1;
    if(aav_angleCube > 360)
    {
       aav_angleCube = 0;
    } 
  
    requestAnimationFrame(draw, canvas);
}


function degToRad(degree)
{
    return(degree * Math.PI/180.0);
}


function uninitialize()
{
    //code
    if (aav_vao_pyramid) {
        aav_gl.deleteVertexArary(aav_vao_pyramid);
        aav_vao_pyramid = null;
    }

    if (aav_vbo_position_pyramid) {
        aav_gl.deleteBuffer(aav_vbo_position_pyramid);
        aav_vbo_position_pyramid = null;
    }

    if (aav_vbo_normal_pyramid) {
        aav_gl.deleteBuffer(aav_vbo_normal_pyramid);
        aav_vbo_normal_pyramid = null;
    }

    if(aav_shaderProgramObject)
    {
        if(aav_fragmentShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject,aav_fragmentShaderObject);
            gl.deleteShader(aav_fragmentShaderObject);
            aav_fragmentShaderObject = null;
        }

        if(aav_vertexShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject,aav_vertexShaderObject);
            gl.deleteShader(aav_vertexShaderObject);
            aav_vertexShaderObject = null;
        }

        gl.deleteProgram(aav_shaderProgramObject);
        aav_shaderProgramObject = null;
    }
}

function keyDown(event)
{
    //code
    switch(event.keyCode)
    {
        case 27: // escape 
            //unitialize
            uninitialize();
            //close our application's tab
            window.close(); // may not work in firefox but work in safari and chrome 
            break;
        case 70: // for 'F' or 'f'
            toggleFullScreen();
            break;
        case 76:
        case 108:
        if (aav_bLight == false)
        {
            aav_bLight = true;
        }
        else
        {
             aav_bLight = false;
        }
        break;
        default:
        break;
    }
}

function mouseDown()
{
    //code
}