var gl = null;
var aav_canvas = null;

var aav_canvas_orignal_width;
var aav_canvas_orignal_height;
var aav_bFullScreen = false;
var requestAnimationFrame;

function main()
{
	//Step1 
	//get Canvas from DOM
	aav_canvas = document.getElementById("AAV"); //tpe Inferance 
	
	if(aav_canvas == null)
	{
		console.log("Optaining aav_canvas Failed \n");
	}
	else
	{
		console.log("Optaining Canvas Sucess\n");
	}

	//retrive the width and height of canvas 
								//position argument
	console.log("canvas width  = "+aav_canvas.width+" canvas height = "+aav_canvas.height+"\n");
	//console.log(" canvas height = "+canvas.height);

	
	aav_canvas_orignal_width = aav_canvas.width;
	aav_canvas_orignal_height = aav_canvas.height;

	window.addEventListener("keydown",keyDown,false);

	window.addEventListener("click",mouseDown,false);

	window.addEventListener("resize",resize,false);

	init();

}

function toggleFullScreen()
{
	//Multi Browser complient
	//varaible declaration
	var fullScreen_element = document.fullscreenElement			||
							document.webkitFullscreenElement	|| 
							document.mozFullScreenElement		|| 
							document.msFullScreenElement		|| 
							null;
	
	if(fullScreen_element == null)
	{
		//Full Screen Set Karnara code
		if(aav_canvas.requestFullscreen)
		{
			aav_canvas.requestFullscreen();
		}
		else if(aav_canvas.webkitRequestFullscreen)
		{
			aav_canvas.webkitRequestFullscreen();
		}
		else if(aav_canvas.mozRequestFullScreen)
		{
			aav_canvas.mozRequestFullScreen();
		}
		else if(aav_canvas.msRequestFullscreen)
		{
			aav_canvas.msRequestFullscreen();
		}

		aav_bFullScreen = true;
	}
	else
	{
		if(document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if(document.webkitExitFullscreen)
		{
			document.webkitExitFullscreen();
		}
		else if(document.mozcancalFullSreen)
		{
			document.mozcancalFullSreen();
		}
		else if(document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}

		aav_bFullScreen = false;
	}
}

function keyDown(event)
{
	switch(event.keyCode)
	{
		case 70:
			toggleFullScreen();
			break;
		default:
			break;
	}
}

function mouseDown()
{
	alert("Mouse Button is Clicked");
}

function init()
{
	//To maintain the consistance 

	//we are getting WebGL Context. 
	gl	=	aav_canvas.getContext("webgl2");

	if(gl	==	null)
	{
		console.log("Optaing  WebGL2 Contextext Failed \n");
	}
	else
	{
		console.log("Optaining WebGL2 Contextext Sucess\n");
	}

	gl.viewPortWidth	=	aav_canvas.width;   //-------\
									   //		 } WebGL specific two member 
	gl.viewPortHeight	=	aav_canvas.height; //-------/	

	//First OpenGL(WebGL) Fcuntion Call.	
	gl.clearColor(0.0,0.0,1.0,1.0); // 

	resize();	// warup

	draw();		// here explicit wramup re-paint call
}

function resize()
{
	if(aav_bFullScreen == true)
	{
		aav_canvas.width	=	window.innerWidth; // window ha inbuilt variable  : client area chee inner width.
		aav_canvas.height	=	window.innerHeight;
	}
	else
	{
		aav_canvas.width	=	aav_canvas_orignal_width;
		aav_canvas.height	=	aav_canvas_orignal_height;
	}
	gl.viewport(0,0,aav_canvas.width,aav_canvas.height);
}

function draw()
{
	gl.clear(gl.COLOR_BUFFER_BIT);

	requestAnimationFrame = window.requestAnimationFrame 		||
							window.webkitRquestAnimationFrame 	|| 
							window.mozRequestAnimationFrame 	||
							window.oRequestAnimationFrame 		|| 
							window.msRequestAnimationFrame;
	
	requestAnimationFrame(draw,aav_canvas);
}