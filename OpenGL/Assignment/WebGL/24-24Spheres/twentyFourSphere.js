//global variable
var gl = null;  // webgl context
var aav_bFullscreen = false;
var aav_canvas_orignal_width;
var aav_canvas_orignal_height;

const WebGLMacros =
{
    AAV_ATTRIBUTE_VERTEX: 0,
    AAV_ATTRIBUTE_COLOR: 1,
    AAV_ATTRIBUTE_NORMAL: 2,
    AAV_ATTRIBUTE_TEXTURE: 3
};


var aav_vertexShaderObject;
var aav_fragmentShaderObject;
var aav_shaderProgramObject;

var aav_vao_cube;
var aav_vbo_position_cube;
var aav_vbo_color_cube;
var aav_vbo_normal_cube;

var aav_mvpUniform;

var perspectiveProjectionMatrix;

var aav_lightAngle = 0;

var aav_bLight = false;
var aav_bAnimationflag = false;

var sphereMesh = null;

var aav_bLight = false;

var aav_xKeyPress = false;
var aav_yKeyPress = false;
var aav_zKeyPress = false;

//Light Array
var aav_lightAmbiant = [0.0, 0.0, 0.0];	//la
var aav_lightDiffuse = [1.0, 1.0, 1.0];	// ld
var aav_lightSpecular = [1.0, 1.0, 1.0];//ls

var aav_lightPosition = [100.0, 100.0, 100.0, 1.0];

//material Array 
var aav_materialAmbiant = new Array(3); // ka 
var aav_materialDiffuse = new Array(3);	// kd
var aav_materialSpecular = new Array(3);	// ks
var aav_materialShininess = 128.0;

var aav_viewMatrixUniform;
var aav_modelMatrixUniform;
var aav_ProjectionMatrixUniform;

var aav_laUniform;
var aav_ldUniform;
var aav_lsUniform;
var aav_lightPositionUniform;

var aav_kaUniform;
var aav_kdUniform;
var aav_ksUniform;

var aav_kShininessUniform;

var aav_lKeyPressedUniform;

var aav_divideHeight;
var aav_divideWidth;

var aav_gHeight = 0;
var aav_gWidth = 0;

//To start animation: To have requestAnimation() to be called "cross-browser" compatible
var aav_requestAnimationFrame =
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame;

//To stop animation : to have aav_cancelAnimationFrame() to be called "cross-browser" compatible
var aav_cancelAnimationFrame =
    window.aav_cancelAnimationFrame ||
    window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||
    window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame ||
    window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||
    window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;

// onload function
function main()
{
    // get canvas element
    canvas = document.getElementById("AAV");
    if (!canvas)
        console.log("Obtaining Canvas Failed\n");
    else
        console.log("Obtaining Canvas Succeeded\n");
    aav_canvas_orignal_width = canvas.width;
    aav_canvas_orignal_height = canvas.height;

    //register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", resize, false);

    //initialize WebGL
    init();

    // start drawing here as warming-up
    resize();
    draw();
}

function toggleFullScreen()
{
    //code 
    var aav_fullscreen_element =
        document.fullscreenElement ||
        document.webkitFullscreenElement ||
        document.mozFullscreenElement ||
        document.msFullscreenElement ||
        null;

    //if not fullscreen
    if (aav_fullscreen_element == null)
    {
        if (canvas.requestFullScree)
            canvas.requestFullScree();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();
        aav_bFullscreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
        aav_bFullscreen = false;
    }
}

function init() {
    // code 
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");
    if (gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    //vertex shader 
    var aav_vertexShaderSourcedCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 vPosition;" +
        "in vec3 vNormal;" +
        "uniform mat4 u_view_matrix;" +
        "uniform mat4 u_model_matrix;" +
        "uniform mat4 u_projection_matrix;" +
        "uniform mediump int u_lKeyPressed;" +
        "uniform vec4 u_lightPosistion;" +
        "out vec3 tranformed_normal;" +
        "out vec3 lightDirection;" +
        "out vec3 view_vector;" +
        "void main(void)" +
        "{" +
        "	if(u_lKeyPressed == 1)" +
        "	{" +
        "		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" +
        "		tranformed_normal = (mat3(u_view_matrix * u_model_matrix) * vNormal);" +
        "		lightDirection = (vec3(u_lightPosistion - eyeCordinate));" +
        "		view_vector = (-eyeCordinate.xyz);" +
        "	}" +
        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
        "}";
    aav_vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(aav_vertexShaderObject, aav_vertexShaderSourcedCode);
    gl.compileShader(aav_vertexShaderObject);
    if (gl.getShaderParameter(aav_vertexShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(aav_vertexShaderObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //fragemnt shader
    var aav_fragmentShaderSourceCode =
        "#version 300 es" +
        "\n" +
        "precision highp float;" +
        "in vec3 tranformed_normal;" +
        "in vec3 lightDirection;" +
        "in vec3 view_vector;" +
        "uniform mediump int u_lKeyPressed;" +
        "uniform vec3 u_la;" +
        "uniform vec3 u_ld;" +
        "uniform vec3 u_ls;" +

        "uniform vec3 u_ka;" +
        "uniform vec3 u_kd;" +
        "uniform vec3 u_ks;" +
        "uniform float u_kShineness;" +

        "out vec4 FragColor;" +
        "vec3 fong_ads_light;" +
        "void main(void)" +
        "{" +
        "	if(u_lKeyPressed == 1)" +
        "	{" +
        "		vec3 normalize_tranformed_normal = normalize(tranformed_normal);" +
        "		vec3 normalize_lightDirection = normalize(lightDirection);" +
        "		vec3 normalize_view_vector = normalize(view_vector);" +
        "		vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" +
        "		vec3 ambiant = u_la * u_ka;" +
        "		vec3 diffuse = u_ld * u_kd * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" +
        "		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness);" +
        "		fong_ads_light = ambiant + diffuse + specular;" +
        "	}" +
        "	else" +
        "	{" +
        "		fong_ads_light = vec3(1.0f,1.0f,1.0f);" +
        "	}" +
        "	FragColor = vec4(fong_ads_light,1.0f);" +
        "}";
    aav_fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(aav_fragmentShaderObject, aav_fragmentShaderSourceCode);
    gl.compileShader(aav_fragmentShaderObject);
    if (gl.getShaderParameter(aav_fragmentShaderObject, gl.COMPILE_STATUS) == false)
    {
        var error = gl.getShaderInfoLog(aav_fragmentShaderObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //shader program 
    aav_shaderProgramObject = gl.createProgram();
    gl.attachShader(aav_shaderProgramObject, aav_vertexShaderObject);
    gl.attachShader(aav_shaderProgramObject, aav_fragmentShaderObject);

    //pre-link binding of shader program object with vertex shader attributes
    gl.bindAttribLocation(aav_shaderProgramObject, WebGLMacros.AAV_ATTRIBUTE_VERTEX, "vPosition");
    gl.bindAttribLocation(aav_shaderProgramObject, WebGLMacros.AAV_ATTRIBUTE_NORMAL, "vNormal");

    //linking 
    gl.linkProgram(aav_shaderProgramObject);
    if (!gl.getProgramParameter(aav_shaderProgramObject, gl.LINK_STATUS))
    {
        var error = gl.getProgramInfoLog(aav_shaderProgramObject);
        if (error.length > 0)
        {
            alert(error);
            uninitialize();
        }
    }

    //get uniform location
    aav_modelMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_model_matrix");
    aav_viewMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_view_matrix");
    aav_ProjectionMatrixUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_projection_matrix");

    aav_laUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_la");
    aav_ldUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ld");
    aav_lsUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ls");
    aav_lightPositionUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_lightPosistion");

    aav_kaUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ka");
    aav_kdUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_kd");
    aav_ksUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_ks");

    aav_kShininessUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_kShineness");

    aav_lKeyPressedUniform = gl.getUniformLocation(aav_shaderProgramObject, "u_lKeyPressed");

    // ** vertices , color , shader attribs, vbo initialization***

    sphereMesh = new Mesh();
    makeSphere(sphereMesh, 2.0, 30, 30);

    //cube vao
    aav_vao_cube = gl.createVertexArray();
    gl.bindVertexArray(aav_vao_cube);


    //set clear color 
    gl.clearColor(0.5, 0.5, 0.5, 1.0); //blue 

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    //inidialize projection matrix
    perspectiveProjectionMatrix = mat4.create();
}

function resize() {
    //code 

    if (aav_bFullscreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = aav_canvas_orignal_width;
        canvas.height = aav_canvas_orignal_height;
    }

    aav_gHeight = parseInt(canvas.height);
    aav_gWidth = parseInt(canvas.width);

    aav_divideHeight = parseInt(canvas.height) / parseInt(6);
    aav_divideWidth = parseInt(canvas.width) / 4;

    //set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);
   
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);

}

function draw()
{
    //variable declarations
    var aav_modelMateix = mat4.create();
    var aav_transformMatrix = mat4.create();
    var aav_viewMatrix = mat4.create();

    //var aav_modelViewProjectionMatrix = mat4.create();
    //code 
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    if (aav_xKeyPress == true)
    {
       aav_lightPosition[0] = 0.0;
       aav_lightPosition[1] = 100 * Math.sin(aav_lightAngle);
       aav_lightPosition[2] = 100 * Math.cos(aav_lightAngle);
       aav_lightPosition[3] = 1.0;
    }
    else if (aav_yKeyPress == true)
    {
       aav_lightPosition[0] = 100 * Math.sin(aav_lightAngle);
       aav_lightPosition[1] = 0.0;
       aav_lightPosition[2] = 100 * Math.cos(aav_lightAngle);
       aav_lightPosition[3] = 1.0;
    }
    else if (aav_zKeyPress == true)
    {
        aav_lightPosition[0] = 100 * Math.sin(aav_lightAngle);
        aav_lightPosition[1] = 100 * Math.cos(aav_lightAngle);
        aav_lightPosition[2] = 0.0;
        aav_lightPosition[3] = 1.0;
    }

    //1st Sppere in 1st Column , emerald 
    aav_materialAmbiant[0] = 0.0215;
    aav_materialAmbiant[1] = 0.1745;
    aav_materialAmbiant[2] = 0.0215;

    aav_materialDiffuse[0] = 0.07568;
    aav_materialDiffuse[1] = 0.61424;
    aav_materialDiffuse[2] = 0.07568;

    aav_materialSpecular[0] = 0.633;
    aav_materialSpecular[1] = 0.727811;
    aav_materialSpecular[2] = 0.633;


    aav_materialShininess = 0.6 * 128.0;

    gl.useProgram(aav_shaderProgramObject);

    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }

    gl.viewport(0, aav_divideHeight * 5, parseInt(aav_gWidth) / parseInt(4), parseInt(aav_gHeight) / parseInt(6));
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //
    //2nd Sphere in 1st column, jade 
    aav_materialAmbiant[0] = 0.135;
    aav_materialAmbiant[1] = 0.2225;
    aav_materialAmbiant[2] = 0.1575;

    aav_materialDiffuse[0] = 0.54;
    aav_materialDiffuse[1] = 0.89;
    aav_materialDiffuse[2] = 0.63;

    aav_materialSpecular[0] = 0.316228;
    aav_materialSpecular[1] = 0.316228;
    aav_materialSpecular[2] = 0.316228;

    aav_materialShininess = 0.1 * 128.0;

    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }

    gl.viewport(0, aav_divideHeight * 4, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();



    //3nd Sphere in 1st column, obsidian
    aav_materialAmbiant[0] = 0.05375;
    aav_materialAmbiant[1] = 0.05;
    aav_materialAmbiant[2] = 0.06625;

    aav_materialDiffuse[0] = 0.18275;
    aav_materialDiffuse[1] = 0.17;
    aav_materialDiffuse[2] = 0.22525;

    aav_materialSpecular[0] = 0.332741;
    aav_materialSpecular[1] = 0.328634;
    aav_materialSpecular[2] = 0.346435;


    aav_materialShininess = 0.3 * 128.0;
    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }

    gl.viewport(0, aav_divideHeight * 2, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();


    //4th Sphere in 1st column, pearl
    aav_materialAmbiant[0] = 0.25;
    aav_materialAmbiant[1] = 0.20725;
    aav_materialAmbiant[2] = 0.20725;



    aav_materialDiffuse[0] = 1.0;
    aav_materialDiffuse[1] = 0.829;
    aav_materialDiffuse[2] = 0.829;



    aav_materialSpecular[0] = 0.296648;
    aav_materialSpecular[1] = 0.296648;
    aav_materialSpecular[2] = 0.296648;


    aav_materialShininess = 0.088 * 128.0;
    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(0, aav_divideHeight * 3, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //5th sphere in 1st column ,ruby 
    aav_materialAmbiant[0] = 0.1745;
    aav_materialAmbiant[1] = 0.01175;
    aav_materialAmbiant[2] = 0.01175;



    aav_materialDiffuse[0] = 0.61424;
    aav_materialDiffuse[1] = 0.04136;
    aav_materialDiffuse[2] = 0.04136;



    aav_materialSpecular[0] = 0.727811;
    aav_materialSpecular[1] = 0.626959;
    aav_materialSpecular[2] = 0.626959;


    aav_materialShininess = 0.6 * 128.0;

    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(0, aav_divideHeight, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    ////6th sphere in 1st column , turquoise
    aav_materialAmbiant[0] = 0.1;
    aav_materialAmbiant[1] = 0.18725;
    aav_materialAmbiant[2] = 0.1745;



    aav_materialDiffuse[0] = 0.396;
    aav_materialDiffuse[1] = 0.74151;
    aav_materialDiffuse[2] = 0.69102;



    aav_materialSpecular[0] = 0.297254;
    aav_materialSpecular[1] = 0.30829;
    aav_materialSpecular[2] = 0.306678;


    aav_materialShininess = 0.1 * 128.0;

    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(0, 0, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //1st sphere on 2nd column, brass
    aav_materialAmbiant[0] = 0.329412;
    aav_materialAmbiant[1] = 0.223529;
    aav_materialAmbiant[2] = 0.027451;



    aav_materialDiffuse[0] = 0.780398;
    aav_materialDiffuse[1] = 0.568627;
    aav_materialDiffuse[2] = 0.113725;



    aav_materialSpecular[0] = 0.992157;
    aav_materialSpecular[1] = 0.947776;
    aav_materialSpecular[2] = 0.807843;


    aav_materialShininess = 0.21794872 * 128.0;
    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth, aav_divideHeight * 5, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //2nd sphere on 2nd column,bronze
    aav_materialAmbiant[0] = 0.2425;
    aav_materialAmbiant[1] = 0.1275;
    aav_materialAmbiant[2] = 0.054;



    aav_materialDiffuse[0] = 0.714;
    aav_materialDiffuse[1] = 0.4284;
    aav_materialDiffuse[2] = 0.18144;



    aav_materialSpecular[0] = 0.393548;
    aav_materialSpecular[1] = 0.271906;
    aav_materialSpecular[2] = 0.166721;


    aav_materialShininess = 0.2 * 128.0;
    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth, aav_divideHeight * 4, aav_gWidth / 4, aav_gHeight / 6);
        aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();


    //3rd sphere on 2rd column,chrome
    aav_materialAmbiant[0] = 0.25;
    aav_materialAmbiant[1] = 0.25;
    aav_materialAmbiant[2] = 0.25;



    aav_materialDiffuse[0] = 0.4;
    aav_materialDiffuse[1] = 0.4;
    aav_materialDiffuse[2] = 0.4;



    aav_materialSpecular[0] = 0.774597;
    aav_materialSpecular[1] = 0.774597;
    aav_materialSpecular[2] = 0.774597;


    aav_materialShininess = 0.6 * 128.0;
    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth, aav_divideHeight * 3, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //4th sphere on 2nd column, 
    aav_materialAmbiant[0] = 0.19125;
    aav_materialAmbiant[1] = 0.0735;
    aav_materialAmbiant[2] = 0.0225;



    aav_materialDiffuse[0] = 0.7038;
    aav_materialDiffuse[1] = 0.27048;
    aav_materialDiffuse[2] = 0.0828;



    aav_materialSpecular[0] = 0.256777;
    aav_materialSpecular[1] = 0.137622;
    aav_materialSpecular[2] = 0.086014;


    aav_materialShininess = 0.1 * 128.0;
    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth, aav_divideHeight * 2, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();


    //5th Sphere on 2nd Column, gold
    aav_materialAmbiant[0] = 0.24725;
    aav_materialAmbiant[1] = 0.1995;
    aav_materialAmbiant[2] = 0.0745;



    aav_materialDiffuse[0] = 0.75164;
    aav_materialDiffuse[1] = 0.60648;
    aav_materialDiffuse[2] = 0.22648;



    aav_materialSpecular[0] = 0.628281;
    aav_materialSpecular[1] = 0.555802;
    aav_materialSpecular[2] = 0.366065;


    aav_materialShininess = 0.4 * 128.0;
    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth, aav_divideHeight , aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();


    //6th Sphere on 2nd Column 
    aav_materialAmbiant[0] = 0.19225;
    aav_materialAmbiant[1] = 0.19225;
    aav_materialAmbiant[2] = 0.19225;



    aav_materialDiffuse[0] = 0.50754;
    aav_materialDiffuse[1] = 0.50754;
    aav_materialDiffuse[2] = 0.50754;



    aav_materialSpecular[0] = 0.508273;
    aav_materialSpecular[1] = 0.508273;
    aav_materialSpecular[2] = 0.508273;


    aav_materialShininess = 0.4 * 128.0;
    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth,0, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();


    //1st sphere on 3nd column
    aav_materialAmbiant[0] = 0.0;
    aav_materialAmbiant[1] = 0.0;
    aav_materialAmbiant[2] = 0.0;


    aav_materialDiffuse[0] = 0.01;
    aav_materialDiffuse[1] = 0.01;
    aav_materialDiffuse[2] = 0.01;


    aav_materialSpecular[0] = 0.50;
    aav_materialSpecular[1] = 0.50;
    aav_materialSpecular[2] = 0.50;


    aav_materialShininess = 0.25 * 128.0;

    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth *2, aav_divideHeight * 5, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //2nd Sphere on 3nd column,cyan
    aav_materialAmbiant[0] = 0.0;
    aav_materialAmbiant[1] = 0.1;
    aav_materialAmbiant[2] = 0.06;


    aav_materialDiffuse[0] = 0.0;
    aav_materialDiffuse[1] = 0.50980392;
    aav_materialDiffuse[2] = 0.50980392;


    aav_materialSpecular[0] = 0.50196078;
    aav_materialSpecular[1] = 0.50196078;
    aav_materialSpecular[2] = 0.50196078;


    aav_materialShininess = 0.25 * 128.0;

    if (aav_bLight == true)
    {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else
    {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 2, aav_divideHeight * 4, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //3rd Sphere on 3th column 
    aav_materialAmbiant[0] = 0.0;
    aav_materialAmbiant[1] = 0.0;
    aav_materialAmbiant[2] = 0.0;


    aav_materialDiffuse[0] = 0.1;
    aav_materialDiffuse[1] = 0.35;
    aav_materialDiffuse[2] = 0.1;


    aav_materialSpecular[0] = 0.45;
    aav_materialSpecular[1] = 0.55;
    aav_materialSpecular[2] = 0.45;


    aav_materialShininess = 0.25 * 128.0;

    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 2, aav_divideHeight * 3, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //4th Sphere on 3rd ,red
    aav_materialAmbiant[0] = 0.0;
    aav_materialAmbiant[1] = 0.0;
    aav_materialAmbiant[2] = 0.0;


    aav_materialDiffuse[0] = 0.5;
    aav_materialDiffuse[1] = 0.0;
    aav_materialDiffuse[2] = 0.0;


    aav_materialSpecular[0] = 0.7;
    aav_materialSpecular[1] = 0.6;
    aav_materialSpecular[2] = 0.6;


    aav_materialShininess = 0.25 * 128.0;
    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 2, aav_divideHeight * 2, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //5th Sphere on 3rd , white
    aav_materialAmbiant[0] = 0.0;
    aav_materialAmbiant[1] = 0.0;
    aav_materialAmbiant[2] = 0.0;


    aav_materialDiffuse[0] = 0.55;
    aav_materialDiffuse[1] = 0.55;
    aav_materialDiffuse[2] = 0.55;


    aav_materialSpecular[0] = 0.70;
    aav_materialSpecular[1] = 0.70;
    aav_materialSpecular[2] = 0.70;


    aav_materialShininess = 0.25 * 128.0;

    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 2, aav_divideHeight, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //6th Sphere on 3rd ,yello plastic
    aav_materialAmbiant[0] = 0.0;
    aav_materialAmbiant[1] = 0.0;
    aav_materialAmbiant[2] = 0.0;


    aav_materialDiffuse[0] = 0.5;
    aav_materialDiffuse[1] = 0.5;
    aav_materialDiffuse[2] = 0.0;


    aav_materialSpecular[0] = 0.60;
    aav_materialSpecular[1] = 0.60;
    aav_materialSpecular[2] = 0.50;


    aav_materialShininess = 0.25 * 128.0;

    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 2,0, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();


    //1st sphere on 4th column,black
    aav_materialAmbiant[0] = 0.02;
    aav_materialAmbiant[1] = 0.02;
    aav_materialAmbiant[2] = 0.02;


    aav_materialDiffuse[0] = 0.01;
    aav_materialDiffuse[1] = 0.01;
    aav_materialDiffuse[2] = 0.01;


    aav_materialSpecular[0] = 0.4;
    aav_materialSpecular[1] = 0.4;
    aav_materialSpecular[2] = 0.4;


    aav_materialShininess = 0.078125 * 128.0;

    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 3, aav_divideHeight * 5, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //2nd Shpere on 4th column,cyan
    aav_materialAmbiant[0] = 0.0;
    aav_materialAmbiant[1] = 0.05;
    aav_materialAmbiant[2] = 0.05;


    aav_materialDiffuse[0] = 0.4;
    aav_materialDiffuse[1] = 0.5;
    aav_materialDiffuse[2] = 0.5;


    aav_materialSpecular[0] = 0.04;
    aav_materialSpecular[1] = 0.7;
    aav_materialSpecular[2] = 0.7;


    aav_materialShininess = 0.078125 * 128.0;
    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 3, aav_divideHeight * 4, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //3rd Sphere on 4th column , green
    aav_materialAmbiant[0] = 0.0;
    aav_materialAmbiant[1] = 0.05;
    aav_materialAmbiant[2] = 0.0;


    aav_materialDiffuse[0] = 0.4;
    aav_materialDiffuse[1] = 0.5;
    aav_materialDiffuse[2] = 0.04;


    aav_materialSpecular[0] = 0.04;
    aav_materialSpecular[1] = 0.7;
    aav_materialSpecular[2] = 0.04;


    aav_materialShininess = 0.6 * 128.0;
    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 3, aav_divideHeight * 3, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //4th Sphere on 4th column, red
    aav_materialAmbiant[0] = 0.05;
    aav_materialAmbiant[1] = 0.0;
    aav_materialAmbiant[2] = 0.0;


    aav_materialDiffuse[0] = 0.5;
    aav_materialDiffuse[1] = 0.4;
    aav_materialDiffuse[2] = 0.4;


    aav_materialSpecular[0] = 0.7;
    aav_materialSpecular[1] = 0.04;
    aav_materialSpecular[2] = 0.04;


    aav_materialShininess = 0.078125 * 128.0;
    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 3, aav_divideHeight * 2, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //5th Sphere on 4th column, white
    aav_materialAmbiant[0] = 0.05;
    aav_materialAmbiant[1] = 0.05;
    aav_materialAmbiant[2] = 0.05;


    aav_materialDiffuse[0] = 0.5;
    aav_materialDiffuse[1] = 0.5;
    aav_materialDiffuse[2] = 0.5;


    aav_materialSpecular[0] = 0.7;
    aav_materialSpecular[1] = 0.7;
    aav_materialSpecular[2] = 0.7;


    aav_materialShininess = 0.6 * 128.0;
    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 3, aav_divideHeight, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();

    //6th Sphere on 4th column , rubber
    aav_materialAmbiant[0] = 0.05;
    aav_materialAmbiant[1] = 0.05;
    aav_materialAmbiant[2] = 0.0;


    aav_materialDiffuse[0] = 0.5;
    aav_materialDiffuse[1] = 0.5;
    aav_materialDiffuse[2] = 0.4;

    aav_materialSpecular[0] = 0.7;
    aav_materialSpecular[1] = 0.7;
    aav_materialSpecular[2] = 0.04;

    aav_materialShininess = 0.078125 * 128.0;
    if (aav_bLight == true) {
        gl.uniform1i(aav_lKeyPressedUniform, 1);
        //Light
        gl.uniform3fv(aav_laUniform, aav_lightAmbiant);				// la 
        gl.uniform3fv(aav_ldUniform, aav_lightDiffuse);				// ld
        gl.uniform3fv(aav_lsUniform, aav_lightSpecular);				// ls
        gl.uniform4fv(aav_lightPositionUniform, aav_lightPosition);	//lightPosition

        //material
        gl.uniform3fv(aav_kaUniform, aav_materialAmbiant);	// ka
        gl.uniform3fv(aav_kdUniform, aav_materialDiffuse);	// kd
        gl.uniform3fv(aav_ksUniform, aav_materialSpecular);	//ks
        gl.uniform1f(aav_kShininessUniform, aav_materialShininess);

    }
    else {

        gl.uniform1i(aav_lKeyPressedUniform, 0);
    }
    gl.viewport(aav_divideWidth * 3, 0, aav_gWidth / 4, aav_gHeight / 6);
    aav_transformMatrix = mat4.create();
    aav_viewMatrix = mat4.create();
    aav_modelMateix = mat4.create();

    mat4.translate(aav_modelMateix, aav_transformMatrix, [0.0, 0.0, -6.0]);

    gl.uniformMatrix4fv(aav_modelMatrixUniform, false, aav_modelMateix);

    gl.uniformMatrix4fv(aav_viewMatrixUniform, false, aav_viewMatrix);

    gl.uniformMatrix4fv(aav_ProjectionMatrixUniform, false, perspectiveProjectionMatrix);

    sphereMesh.draw();
   
    gl.useProgram(null);

    if (aav_bAnimationflag == true)
    {
        update();
    }

    //animation loop
    requestAnimationFrame(draw, canvas);
}


function degToRad(degree)
{
    return (degree * Math.PI / 180.0);
}

function update()
{

    aav_lightAngle = aav_lightAngle + 0.01;
    if (aav_lightAngle > 360)
    {
        aav_lightAngle = 0.0;
    }
}


function uninitialize()
{
    //code
    if (sphereMesh) {
        sphereMesh.deallocate();
    }

    if (aav_shaderProgramObject)
    {
        if (aav_fragmentShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject, aav_fragmentShaderObject);
            gl.deleteShader(aav_fragmentShaderObject);
            aav_fragmentShaderObject = null;
        }

        if (aav_vertexShaderObject)
        {
            gl.detachShader(aav_shaderProgramObject, aav_vertexShaderObject);
            gl.deleteShader(aav_vertexShaderObject);
            aav_vertexShaderObject = null;
        }

        gl.deleteProgram(aav_shaderProgramObject);
        aav_shaderProgramObject = null;
    }
}

function keyDown(event)
{
    //code
    switch (event.keyCode) {
        case 27: 
            uninitialize();
            window.close();
            break;
        case 70: 
            toggleFullScreen();
            break;
        case 97:
        case 65:
            if (aav_bAnimationflag == false) {
                aav_bAnimationflag = true;
            }
            else {
                aav_bAnimationflag = false;
            }
            break;
        case 108:
        case 76:
            if (aav_bLight == false) {
                aav_bLight = true;
            }
            else {
                aav_bLight = false;
            }
            break;

        case 88:
        case 120:
            aav_lightAngle = 0.0;
            aav_yKeyPress = false;
            aav_zKeyPress = false;
            aav_xKeyPress = true;
            break;
        case 89:
        case 121:
            aav_lightAngle = 0.0;
            aav_xKeyPress = false;
            aav_zKeyPress = false;
            aav_yKeyPress = true;
            break;
        case 90:
        case 122:
            aav_lightAngle = 0.0;
            aav_xKeyPress = false;
            aav_yKeyPress = false;
            aav_zKeyPress = true;
            break;
        default:
            break;
    }
}

function mouseDown()
{
    //code
}