#import <Foundation/Foundation.h>
#import <cocoa/cocoa.h>             //analogous to windows.h xlib.h

#import <QuartzCore/CVDisplayLink.h> // Audio and Display , Core Vidio
#import <OpenGL/gl3.h>             //gl.h
#import"vmath.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};


//ProtoType
//CallBackFunction
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp * , CVOptionFlags, CVOptionFlags *,void *);

FILE *aav_gpFile = NULL;

GLfloat aav_anglePyramid = 0.0f;
GLfloat aav_angleCube = 0.0f;

bool aav_bAnimationflag = false;
bool aav_bLight = false;

@interface appDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>    //NeStep
//forwardDeclaration  extens  and implement
 @end


//main function
int main(int argc, char *argv[])
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    //Baap Object , magaicha , maza hinstace de
    //InBuilt Global object , jo NSApplication cha variable ahe
    NSApp = [NSApplication sharedApplication]; //asnara re NSapplication la
    
    [NSApp setDelegate:[[appDelegate alloc]init]];                          //^
    
    //Event loop , Message Loop
    [NSApp run]; // This is Called as Run Loop
    
    [pool release]; //1. program samtoi, all the memeory is reclamed
    
    return 0;
}

@interface MyOpenGLView: NSOpenGLView

@end

//implementation
@implementation appDelegate
{
    //class variables
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView;
}

//instance method
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; //ha path return hoto /User/UserName/Desktop/RTR/log/Window.app
    
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt",parentDirPath];
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    
    aav_gpFile = fopen(pszLogFileNameWithPath,"w");
    if(aav_gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
    }
    
    fprintf(aav_gpFile,"Program Started SuccessFully \n");
    
    
    //NSPoints Struct(x,y), NSSize Structure(width, height)  CG Point , CG Size
    NSRect win_rect = NSMakeRect(0.0,0.0,800.0,600.0); //internal CG Rect :Code Graphics as this "c" library so this is calling is C style
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                    NSWindowStyleMaskClosable |
                                                    NSWindowStyleMaskMiniaturizable |
                                                    NSWindowStyleMaskResizable
                                          backing: NSBackingStoreBuffered
                                          defer:NO];
    //styleMask is OVERLAPPEDWINDOW
    //NSWindowStyleMaskTitled is caption
    //NSWindowStyleMaskMiniaturizable is minimize
    //NSWindowStyleMaskResizable is resize
    //backing : retain karu nako , buffer madhe thav
    //Defer : NO , maje lagech dakhav , NO ha Objective C madhla Bool
    
    [window setTitle: @"AAV:OpenGL Diffused Light On Cube "]; //create window cha Second Parameter
    [window center]; //centering the window
    
    //view Creation
    myOpenGLView = [[MyOpenGLView alloc] initWithFrame:win_rect]; //NSView madhle methos
    
    [window setContentView: myOpenGLView]; // maza contect view set kar
    [window setDelegate: self]; // objective C mathla self ha c++ this
    [window makeKeyAndOrderFront:self]; //mazaya varti focuse kar , set focuse , get foucuse , set Forground window, z order la sartavar pudhe an
    
 }

//Apan Shevti Ithe Yato
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code
    
    
    if(aav_gpFile)
    {
        fprintf(aav_gpFile,"Program Ended SuccessFully \n");
        fclose(aav_gpFile);
        aav_gpFile = NULL;
    }
    
}

//NSWindow deligate  che window
-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
    
}

-(void)dealloc
{
    //code
    [myOpenGLView release];
    [window release];
    [super dealloc]; //mazya super la dealloc kar
}
@end


//MyView Implementation
@implementation MyOpenGLView
{
    //code
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint aav_gVertexShaderObject;
    GLuint aav_gFragmentShaderObject;
    GLuint aav_gShaderProgramObject;
    
    GLuint aav_Vao_cube;
    GLuint aav_Vbo_cube_position;
    GLuint aav_Vbo_cube_normals;
    
    mat4 aav_PerspectiveProjectionMatrix;
    
    GLuint aav_modelViewMatrixUniform;
    GLuint aav_modelViewPerspectiveProjectionUniform;
    
    GLuint aav_LkeyPressedUniform;
    
    GLuint aav_ldUniform;
    GLuint aav_kdUniform;
    GLuint aav_lightPositionUniform;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        //Pixel Format Attribute
        NSOpenGLPixelFormatAttribute attributes[] =
        {
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };
        
        NSOpenGLPixelFormat * pixelFormal = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
        if(pixelFormal == nil)
        {
            fprintf(aav_gpFile,"Can Not Get pixelFormat Attribute");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormal shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormal];
        [self setOpenGLContext:glContext];
        
    }
    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)outputType
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init]; //New Thread is Created.
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

//initialize prepare OpenGL
-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];
    
    [[self openGLContext]makeCurrentContext];
    
    //swap Interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
   
    //**VERTEX SHADER***
    aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* aav_vertexShaserSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_modelView_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_lKeyPressed;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_kd;" \
    "uniform vec4 u_lightPosistion;" \
    "out vec3 defuse_light;"
    "void main(void)" \
    "{" \
    "if(u_lKeyPressed == 1)" \
    "{" \
    "    vec4 eyeCordinate = u_modelView_matrix * vPosition;" \
    "    mat3 normal_matrix = mat3(transpose(inverse(u_modelView_matrix)));" \
    "    vec3 tenorm = normalize(normal_matrix * vNormal); " \
    "    vec3 s = normalize(vec3(u_lightPosistion - eyeCordinate));" \
    "    defuse_light = u_ld * u_kd * (max(dot(s,tenorm),0.0));" \
    "}" \
    "gl_Position = u_projection_matrix *u_modelView_matrix* vPosition;" \
    "}";
    glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(aav_gVertexShaderObject);
    
    GLint aav_infoLogLength = 0;
    GLint aav_shaderCompiledStatus = 0;
    char* aav_szBuffer = NULL;
    
    glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
                                   &aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**FRAGMENT SHADER**
    aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "vec4 color;"
    "in vec3 defuse_light;" \
    "uniform int u_lKeyPressed;" \
    "out vec4 aav_FragColor;" \
    "void main(void)" \
    "{" \
    "    if(u_lKeyPressed == 1)" \
    "    {" \
    "        color = vec4(defuse_light,1.0);"\
    "    }" \
    "    else " \
    "    {" \
    "        color = vec4(1.0f,1.0f,1.0f,1.0f);" \
    "    }" \
    " aav_FragColor = color; " \
    "}";
    glShaderSource(aav_gFragmentShaderObject, 1,
                   (const GLchar**)&fragmentShaderSourceCode,NULL);
    //compile shader
    glCompileShader(aav_gFragmentShaderObject);
    
    
    aav_szBuffer = NULL;
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    
    glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS,
                  &aav_shaderCompiledStatus);
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
                                   &aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**SHADER PROGRAM**
    //Create
    aav_gShaderProgramObject = glCreateProgram();
    
    glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");
    
    glLinkProgram(aav_gShaderProgramObject);
    
    aav_infoLogLength = 0;
    GLint aav_shaderProgramLinkStatus = 0;
    aav_szBuffer = NULL;
    
    glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
    if (aav_shaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                GLsizei aav_aav_written;
                glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
                                    &aav_aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Shader Program Link Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //Post Linking Information
    aav_modelViewMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_modelView_matrix");
    aav_modelViewPerspectiveProjectionUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_projection_matrix");
    
    aav_LkeyPressedUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_lKeyPressed");
    
    aav_ldUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ld");
    aav_kdUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kd");
    aav_lightPositionUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_light_position");;
    
    //vertices array declation
    const GLfloat aav_cubeVertex[] =
    {
        1.0f,1.0f,1.0f,
        -1.0f,1.0f,1.0f,
        -1.0f,-1.0f,1.0f,
        1.0f,-1.0f,1.0f,
        
        1.0f, 1.0f, -1.0f,
        1.0f, 1.0f, 1.0f,
        1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, -1.0f,
        
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        1.0f, -1.0f, -1.0f,
        
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        -1.0f, -1.0f, 1.0f,
        -1.0f, -1.0f, -1.0f,
        
        1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, -1.0f,
        -1.0f, 1.0f, 1.0f,
        1.0f, 1.0f, 1.0f,
        
        1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, -1.0f,
        -1.0f, -1.0f, 1.0f,
        1.0f, -1.0f, 1.0f
    };
    
    const GLfloat aav_cubeNormals[] =
    {
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        0.0f, 0.0f, 1.0f,
        
        1.0, 0.0f, 0.0f,
        1.0, 0.0f, 0.0f,
        1.0, 0.0f, 0.0f,
        1.0, 0.0f, 0.0f,
        
        0.0f, 0.0, -1.0f,
        0.0f, 0.0, -1.0f,
        0.0f, 0.0, -1.0f,
        0.0f, 0.0, -1.0f,
        
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        
        0.0, -1.0, 0.0f,
        0.0, -1.0, 0.0f,
        0.0, -1.0, 0.0f,
        0.0, -1.0, 0.0f
    };
    
    
    //Record Square
    glGenVertexArrays(1,&aav_Vao_cube);
    glBindVertexArray(aav_Vao_cube);
    
    glGenBuffers(1,&aav_Vbo_cube_position);
    glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_cube_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeVertex), aav_cubeVertex,GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &aav_Vbo_cube_normals);
    glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_cube_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeNormals), aav_cubeNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Record Off / Pause
    glBindVertexArray(0);;
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
   
    //
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    
    aav_PerspectiveProjectionMatrix = mat4::identity();
    
    //create displayLink
    //CallBackSet karne
    //NSPixelFormat to Core
    //CGL
    //DisplayLinkStart
    
    //CV and CGL related Code
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDispalyLinkCallBack,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPiexelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPiexelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    //code
    [super reshape];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    if(rect.size.height < 0)
        rect.size.height = 1;
    glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);
    
   aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)rect.size.width / (GLfloat)rect.size.height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//invalidateRect
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    GLfloat aav_lightPosition[] = {0.0f,0.0f,2.0f,1.0f};
    
    mat4 aav_scaMatrix;
    mat4 aav_translateMatrix;
    mat4 aav_rotationMatrixA;
    mat4 aav_rotationMatrixX;
    mat4 aav_rotationMatrixY;
    mat4 aav_rotationMatrixZ;
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //triangle
    glUseProgram(aav_gShaderProgramObject);
    
    if (aav_bLight == true)
    {
        //Light Enable
        glUniform1i(aav_LkeyPressedUniform,1);
        glUniform3f(aav_ldUniform,1.0f,1.0f,1.0f); // color light
        glUniform3f(aav_kdUniform,0.5f,0.5f,0.5f); // Gray Material
        glUniform4fv(aav_lightPositionUniform, 1,(GLfloat *)aav_lightPosition); // Light Position
    }
    else
    {
        glUniform1i(aav_LkeyPressedUniform, 0);
    }
    
    //OpenGL Drawing
    mat4 aav_modelViewMateix = mat4::identity();
    mat4 aav_modelViewProjectMatrix = mat4::identity();
    
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    aav_rotationMatrixX = vmath::rotate(aav_angleCube, 1.0f, 0.0f, 0.0f);
    aav_rotationMatrixY = vmath::rotate(aav_angleCube, 0.0f, 1.0f, 0.0f);
    aav_rotationMatrixZ = vmath::rotate(aav_angleCube, 0.0f, 0.0f, 1.0f);
    aav_rotationMatrixA = aav_rotationMatrixX * aav_rotationMatrixY * aav_rotationMatrixZ;
    
    aav_modelViewMateix = aav_translateMatrix *aav_rotationMatrixA;
    
    glUniformMatrix4fv(aav_modelViewMatrixUniform, 1, GL_FALSE, aav_modelViewMateix);
    glUniformMatrix4fv(aav_modelViewPerspectiveProjectionUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
    
    glBindVertexArray(aav_Vao_cube);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    if (aav_bAnimationflag == TRUE)
    {
        aav_angleCube = aav_angleCube + 1.00f;
        if(aav_angleCube >= 360.0f)
            aav_angleCube = 0.0f;
    }
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//NSResponder methos
-(BOOL)acceptsFirstResponder
{
    //code
    //[[self window] makeFirstResponder ];
    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
             
            break;
        case 'F':
        case 'f':
        [[self window] toggleFullScreen:self]; //Full Screen
            break;
        case 'a':
        case 'A':
            if (aav_bAnimationflag == false)
            {
                aav_bAnimationflag = true;
            }
            else
            {
                aav_bAnimationflag = false;
            }
            break;
        case 'l':
        case 'L':
            if (aav_bLight == false)
            {
                aav_bLight = true;
            }
            else
            {
                aav_bLight = false;
            }
            break;
        default:
            break;
        
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
 
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)dealloc
{
    //code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    if (aav_Vao_cube)
    {
        glDeleteVertexArrays(1, &aav_Vao_cube);
        aav_Vao_cube = 0;
    }
    
    if (aav_Vbo_cube_position)
    {
        glDeleteBuffers(1, &aav_Vbo_cube_position);
        aav_Vbo_cube_position = 0;
    }
    
    if (aav_Vbo_cube_normals)
    {
        glDeleteBuffers(1, &aav_Vbo_cube_normals);
        aav_Vbo_cube_normals = 0;
    }
    
    
    glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glDeleteShader(aav_gVertexShaderObject);
    aav_gVertexShaderObject = 0;
    glDeleteShader(aav_gFragmentShaderObject);
    aav_gFragmentShaderObject = 0;
    
    glDeleteProgram(aav_gShaderProgramObject);
    aav_gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    [super dealloc];
}
@end


//Global Function
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp * outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut,void *displayLinkContext)
{
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return result;
}
