#import <Foundation/Foundation.h>
#import <cocoa/cocoa.h>             //analogous to windows.h xlib.h

#import <QuartzCore/CVDisplayLink.h> // Audio and Display , Core Vidio
#import <OpenGL/gl3.h>             //gl.h
#import"vmath.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};

//ProtoType
//CallBackFunction
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp * , CVOptionFlags, CVOptionFlags *,void *);

FILE *gpFile = NULL;

bool aav_bLight = false;
BOOL aav_AnimationFlag = FALSE;

GLfloat aav_rotation = 0.0f;

//Light Array
GLfloat aav_lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };    //la
GLfloat aav_lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };    // ld
GLfloat aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };//ls

GLfloat aav_lightPosition[] = { 0.0f,0.0f,2.0f,1.0f };

//material Array
GLfloat aav_materialAmbiant[]    = { 0.0f, 0.0f, 0.0f ,0.0f}; // ka
GLfloat aav_materialDiffuse[]    = {1.0f,1.0f,1.0f,1.0f};    // kd
GLfloat aav_materialSpecular[]    = {1.0f,1.0f,1.0f,1.0f};    // ks


float* vertexArray = NULL;

@interface appDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>    //NeStep
//forwardDeclaration  extens  and implement
 @end


//main function
int main(int argc, char *argv[])
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    //Baap Object , magaicha , maza hinstace de
    //InBuilt Global object , jo NSApplication cha variable ahe
    NSApp = [NSApplication sharedApplication]; //asnara re NSapplication la
    
    [NSApp setDelegate:[[appDelegate alloc]init]];                          //^
    
    //Event loop , Message Loop
    [NSApp run]; // This is Called as Run Loop
    
    [pool release]; //1. program samtoi, all the memeory is reclamed
    
    return 0;
}

@interface MyOpenGLView: NSOpenGLView

@end

//implementation
@implementation appDelegate
{
    //class variables
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView;
}

//instance method
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; //ha path return hoto /User/UserName/Desktop/RTR/log/Window.app
    
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt",parentDirPath];
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    
    gpFile = fopen(pszLogFileNameWithPath,"w");
    if(gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
    }
    
    fprintf(gpFile,"Program Started SuccessFully \n");
    
    
    //NSPoints Struct(x,y), NSSize Structure(width, height)  CG Point , CG Size
    NSRect win_rect = NSMakeRect(0.0,0.0,800.0,600.0); //internal CG Rect :Code Graphics as this "c" library so this is calling is C style
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                    NSWindowStyleMaskClosable |
                                                    NSWindowStyleMaskMiniaturizable |
                                                    NSWindowStyleMaskResizable
                                          backing: NSBackingStoreBuffered
                                          defer:NO];
    //styleMask is OVERLAPPEDWINDOW
    //NSWindowStyleMaskTitled is caption
    //NSWindowStyleMaskMiniaturizable is minimize
    //NSWindowStyleMaskResizable is resize
    //backing : retain karu nako , buffer madhe thav
    //Defer : NO , maje lagech dakhav , NO ha Objective C madhla Bool
    
    [window setTitle: @"AAV:OpenGL Interleaved"]; //create window cha Second Parameter
    [window center]; //centering the window
    
    //view Creation
    myOpenGLView = [[MyOpenGLView alloc] initWithFrame:win_rect]; //NSView madhle methos
    
    [window setContentView: myOpenGLView]; // maza contect view set kar
    [window setDelegate: self]; // objective C mathla self ha c++ this
    [window makeKeyAndOrderFront:self]; //mazaya varti focuse kar , set focuse , get foucuse , set Forground window, z order la sartavar pudhe an
    
 }

//Apan Shevti Ithe Yato
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code
    if(gpFile)
    {
        fprintf(gpFile,"Program Ended SuccessFully \n");
        fclose(gpFile);
        gpFile = NULL;
    }
    
}

//NSWindow deligate  che window
-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
    
}

-(void)dealloc
{
    //code
    [myOpenGLView release];
    [window release];
    [super dealloc]; //mazya super la dealloc kar
}
@end


//MyView Implementation
@implementation MyOpenGLView
{
    //code
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint aav_gVertexShaderObject;
    GLuint aav_gFragmentShaderObject;
    GLuint aav_gShaderProgramObject;
    
    
    GLuint aav_mvp_MatrixUniform;
    
    mat4 aav_PerspectiveProjectionMatrix;
    
    GLuint aav_gVao_cube;
    GLuint aav_gVbo_cubeAll;
    
    GLuint aav_viewMatrixUniform;
    GLuint aav_modelMatrixUniform;
    GLuint aav_ProjectionMatrixUniform;
    
    GLuint aav_laUniform;
    GLuint aav_ldUniform;
    GLuint aav_lsUniform;
    GLuint aav_lightPositionUniform;
    
    GLuint aav_kShininessUniform;
    
    GLuint aav_lKeyPressedUniform;
    
    GLuint aav_textureSamplerUniform;
    
    GLuint aav_textureNew;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        //Pixel Format Attribute
        NSOpenGLPixelFormatAttribute attributes[] =
        {
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };
        
        NSOpenGLPixelFormat * pixelFormal = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
        if(pixelFormal == nil)
        {
            fprintf(gpFile,"Can Not Get pixelFormat Attribute");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormal shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormal];
        [self setOpenGLContext:glContext];
        
    }
    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)outputType
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init]; //New Thread is Created.
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

//initialize prepare OpenGL
-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];
    
    [[self openGLContext]makeCurrentContext];
    
    //swap Interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    
    //**VERTEX SHADER***
    aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* aav_vertexShaserSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec4 vColor;"    \
    "in vec3 vNormal;" \
    "in vec2 vTexCoord;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform vec4 u_lightPosistion;" \
    "out vec3 tranformed_normal;"\
    "out vec3 lightDirection;" \
    "out vec3 view_vector;" \
    "out vec2 aav_out_TexCoord;" \
    "out vec4 aav_out_color;" \
    "void main(void)" \
    "{" \
    "    vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
    "    tranformed_normal = (mat3(u_view_matrix * u_model_matrix) * vNormal);" \
    "    lightDirection = (vec3(u_lightPosistion - eyeCordinate));" \
    "    view_vector = (-eyeCordinate.xyz);" \
    "    aav_out_TexCoord = vTexCoord; " \
    "    aav_out_color = vColor;" \
    "    gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";
    glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(aav_gVertexShaderObject);
    
    GLint aav_infoLogLength = 0;
    GLint aav_shaderCompiledStatus = 0;
    char* szBuffer = NULL;
    
    glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**FRAGMENT SHADER**
    aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 aav_out_color;" \
    "in vec3 tranformed_normal;"\
    "in vec3 lightDirection;" \
    "in vec3 view_vector;" \
    "in vec2 aav_out_TexCoord;" \
    "uniform int u_lKeyPressed;" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_ls;" \
    
    "uniform float u_kShineness;"\
    
    "out vec4 FragColor;" \
    "vec3 fong_ads_light;" \
    "uniform sampler2D u_texture_sampler;" \
    "void main(void)" \
    "{" \
    "        vec3 normalize_tranformed_normal = normalize(tranformed_normal);" \
    "        vec3 normalize_lightDirection = normalize(lightDirection);" \
    "        vec3 normalize_view_vector = normalize(view_vector);" \
    "        vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" \
    "        vec3 ambiant = u_la ;" \
    "        vec3 diffuse = u_ld  * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" \
    "        vec3 specular = u_ls  * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness);" \
    "        fong_ads_light = ambiant + diffuse + specular;" \
    "        fong_ads_light = fong_ads_light * vec3(texture(u_texture_sampler,aav_out_TexCoord));" \
    "        FragColor = vec4(fong_ads_light,1.0f)  *  aav_out_color; " \
    "}";
    glShaderSource(aav_gFragmentShaderObject, 1,
                   (const GLchar**)&fragmentShaderSourceCode,NULL);
    //compile shader
    glCompileShader(aav_gFragmentShaderObject);
    
    
    szBuffer = NULL;
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    
    glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS,
                  &aav_shaderCompiledStatus);
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**SHADER PROGRAM**
    //Create
    aav_gShaderProgramObject = glCreateProgram();
    
    glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_TEXCORD, "vTexCoord");
    
    glLinkProgram(aav_gShaderProgramObject);
    
    aav_infoLogLength = 0;
    GLint aav_shaderProgramLinkStatus = 0;
    szBuffer = NULL;
    
    glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
    if (aav_shaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                GLsizei aav_aav_written;
                glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
                                    &aav_aav_written, szBuffer);
                fprintf(gpFile, "Shader Program Link Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //Post Linking Information
    /**************************************/
    aav_textureSamplerUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_texture_sampler");
    
    aav_modelMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_model_matrix");
    aav_viewMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_view_matrix");
    aav_ProjectionMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_projection_matrix");
    
    aav_laUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_la");
    aav_ldUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ld");
    aav_lsUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ls");
    aav_lightPositionUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_lightPosistion");
    
    aav_kShininessUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kShineness");
    
    /**************************************/
    
    const GLfloat aav_cubeVertex[] =
    {
        0.5f,0.5f,0.5f,        1.0f,0.0f,0.0f,        0.0f, 0.0f, 1.0f,    0.0f,0.0f,
        -0.5f,0.5f,0.5f,    1.0f,0.0f,0.0f,        0.0f, 0.0f, 1.0f,    1.0f,0.0f,
        -0.5f,-0.5f,0.5f,    1.0f,0.0f,0.0f,        0.0f, 0.0f, 1.0f,    1.0f,1.0f,
        0.5f,-0.5f,0.5f,    1.0f,0.0f,0.0f,        0.0f, 0.0f, 1.0f,    0.0f,1.0f,
        
        0.5f, 0.5f, -0.5f,    0.0f,1.0f,0.0f,        1.0, 0.0f, 0.0f,    0.0f, 0.0f,
        0.5f, 0.5f, 0.5f,    0.0f,1.0f,0.0f,        1.0, 0.0f, 0.0f,    1.0f, 0.0f,
        0.5f, -0.5f, 0.5f,    0.0f,1.0f,0.0f,        1.0, 0.0f, 0.0f,    1.0f, 1.0f,
        0.5f, -0.5f, -0.5f,    0.0f,1.0f,0.0f,        1.0, 0.0f, 0.0f,    0.0f, 1.0f,
        
        0.5f, 0.5f, -0.5f,        0.0f,0.0f,1.0f,        0.0f, 0.0, -1.0f,    0.0f, 0.0f,
        -0.5f, 0.5f, -0.5f,        0.0f,0.0f,1.0f,        0.0f, 0.0, -1.0f,    1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,    0.0f,0.0f,1.0f,        0.0f, 0.0, -1.0f,    1.0f, 1.0f,
        0.5f, -0.5f, -0.5f,        0.0f,0.0f,1.0f,        0.0f, 0.0, -1.0f,    0.0f, 1.0f,
        
        -0.5f, 0.5f, -0.5f,        0.0f, 1.0f, 1.0f,     -1.0f, 0.0f, 0.0f,        0.0f, 0.0f,
        -0.5f, 0.5f, 0.5f,        0.0f, 1.0f, 1.0f,     -1.0f, 0.0f, 0.0f,        1.0f, 0.0f,
        -0.5f, -0.5f, 0.5f,        0.0f, 1.0f, 1.0f,     -1.0f, 0.0f, 0.0f,        1.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,    0.0f, 1.0f, 1.0f,     -1.0f, 0.0f, 0.0f,        0.0f, 1.0f,
        
        0.5f, 0.5f, -0.5f,    1.0f, 0.0f, 1.0f,    0.0f, 1.0f, 0.0f,    0.0f, 0.0f,
        -0.5f, 0.5f, -0.5f,    1.0f, 0.0f, 1.0f,    0.0f, 1.0f, 0.0f,    1.0f, 0.0f,
        -0.5f, 0.5f, 0.5f,    1.0f, 0.0f, 1.0f,     0.0f, 1.0f, 0.0f,    1.0f, 1.0f,
        0.5f, 0.5f, 0.5f,    1.0f, 0.0f, 1.0f,    0.0f, 1.0f, 0.0f,    0.0f, 1.0f,
        
        0.5f, -0.5f, -0.5f,        1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,    0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,    1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,    1.0f, 0.0f,
        -0.5f, -0.5f, 0.5f,        1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,    1.0f, 1.0f,
        0.5f, -0.5f, 0.5f,        1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,    0.0f, 1.0f
    };
    
    
    
    glGenVertexArrays(1, &aav_gVao_cube);
    glBindVertexArray(aav_gVao_cube);
    
    //Record Sphere
    glGenBuffers(1, &aav_gVbo_cubeAll);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_cubeAll);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeVertex), aav_cubeVertex, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    
    //color
    glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
    
    //normals
    glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
    
    //texture
    glVertexAttribPointer(AAV_ATTRIBUTE_TEXCORD, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(9 * sizeof(float)));
    glEnableVertexAttribArray(AAV_ATTRIBUTE_TEXCORD);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
   
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    
    glEnable(GL_TEXTURE_2D);
    aav_textureNew = [self loadTextureFromBmpFile:"marble.bmp"];
    if(aav_textureNew == 0)
    {
        [self release];
        [NSApp terminate:self];
    }
    
    //
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    
    aav_PerspectiveProjectionMatrix = mat4::identity();
    
    //create displayLink
    //CallBackSet karne
    //NSPixelFormat to Core
    //CGL
    //DisplayLinkStart
    
    //CV and CGL related Code
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDispalyLinkCallBack,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPiexelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPiexelFormat);
    CVDisplayLinkStart(displayLink);
}

-(GLuint)loadTextureFromBmpFile:(const char *)imageFileName
{
    //code
    
    GLuint aav_texture;
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; //ha path return hoto /User/UserName/Desktop/RTR/log/Window.app
    
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];
    
    NSString *imageFileNameWithPath = [NSString stringWithFormat:@"%@/%s",parentDirPath,imageFileName];
    
    NSImage *bmpImage = [[NSImage alloc]initWithContentsOfFile:imageFileNameWithPath];//Get Image Representation of our image file.
    
    if(!bmpImage)
    {
        fprintf(gpFile, "NSImage File Conversion Fail\n");
        return 0;
    }
    // Now get CGImageRepresentation Of NSImage
    CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];
    //get Width and height of CGImage
    int imageWidth = (int)CGImageGetWidth(cgImage);
    int imageHeight = (int)CGImageGetHeight(cgImage);
    
    //Get CoreFoundation Representation of Image
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    //Convert CFDataRef to void * generic Data
    void *pixels = (void *)CFDataGetBytePtr(imageData); //pixel CFData cha pointer miltoi.
    
    //
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glGenTextures(1, &aav_texture); // Gatu
    glBindTexture(GL_TEXTURE_2D, aav_texture);
    
    //Setting Texture Paramter
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    
    glTexImage2D(GL_TEXTURE_2D,0,
                 GL_RGBA,imageWidth,imageHeight,
                 0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    
    return aav_texture;
}


-(void)reshape
{
    //code
    [super reshape];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    if(rect.size.height < 0)
        rect.size.height = 1;
    glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);
    
   aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)rect.size.width / (GLfloat)rect.size.height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//invalidateRect
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    mat4 aav_rotationMatrixA;
    mat4 aav_rotationMatrixX;
    mat4 aav_rotationMatrixY;
    mat4 aav_rotationMatrixZ;
    
    //fuction declartion
    void update();
    
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //triangle
    glUseProgram(aav_gShaderProgramObject);
    
    //Light
    glUniform3fv(aav_laUniform,1,(GLfloat *)aav_lightAmbiant);                // la
    glUniform3fv(aav_ldUniform,1,(GLfloat *)aav_lightDiffuse);                // ld
    glUniform3fv(aav_lsUniform,1,(GLfloat *)aav_lightSpecular);                // ls
    glUniform4fv(aav_lightPositionUniform,1,(GLfloat *)aav_lightPosition);    //lightPosition
    
    glUniform1f(aav_kShininessUniform,128.0f);
    
    
    //OpenGL Drawing
    mat4 aav_modelMateix = mat4::identity();
    mat4 aav_viewMatrix = mat4::identity();
    mat4 aav_rotationMatrix = mat4::identity();
    mat4 aav_scaleMatrix = mat4::identity();
    
    mat4 aav_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    aav_rotationMatrixX = vmath::rotate(aav_rotation, 1.0f, 0.0f, 0.0f);
    aav_rotationMatrixY = vmath::rotate(aav_rotation, 0.0f, 1.0f, 0.0f);
    aav_rotationMatrixZ = vmath::rotate(aav_rotation, 0.0f, 0.0f, 1.0f);
    
    aav_rotationMatrixA = aav_rotationMatrixX * aav_rotationMatrixY * aav_rotationMatrixZ;
    
    aav_modelMateix = aav_translateMatrix *aav_scaleMatrix * aav_rotationMatrixA;
    
    glUniformMatrix4fv(aav_modelMatrixUniform,1,GL_FALSE, aav_modelMateix);
    glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
    glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, aav_textureNew);
    glUniform1i(aav_textureSamplerUniform, 0);
    
    
    glBindVertexArray(aav_gVao_cube);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
    glBindVertexArray(0);
    
    aav_rotation = aav_rotation + 1.0f;
    if (aav_rotation > 360)
    {
        aav_rotation = 0.0f;
    }
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//NSResponder methos
-(BOOL)acceptsFirstResponder
{
    //code
    //[[self window] makeFirstResponder ];
    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
             
            break;
        case 'F':
        case 'f':
        [[self window] toggleFullScreen:self]; //Full Screen
            break;
        case 'l':
        case 'L':
            if (aav_bLight == false)
            {
                aav_bLight = true;
            }
            else
            {
                aav_bLight = false;
            }
            break;
        case 'A':
        case 'a':
            if (aav_AnimationFlag == FALSE)
            {
                aav_AnimationFlag = TRUE;
            }
            else
            {
                aav_AnimationFlag = FALSE;
            }
            break;
        default:
            break;
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
 
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)dealloc
{
    //code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    if (aav_gVao_cube)
    {
        glDeleteVertexArrays(1, &aav_gVao_cube);
        aav_gVao_cube = 0;
    }
    
    if (aav_gVbo_cubeAll)
    {
        glDeleteBuffers(1, &aav_gVbo_cubeAll);
        aav_gVbo_cubeAll = 0;
    }
    
    glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glDeleteShader(aav_gVertexShaderObject);
    aav_gVertexShaderObject = 0;
    glDeleteShader(aav_gFragmentShaderObject);
    aav_gFragmentShaderObject = 0;
    
    glDeleteProgram(aav_gShaderProgramObject);
    aav_gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    [super dealloc];
}
@end


//Global Function
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp * outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut,void *displayLinkContext)
{
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return result;
}
