#import <Foundation/Foundation.h>
#import <cocoa/cocoa.h>             //analogous to windows.h xlib.h

#import <QuartzCore/CVDisplayLink.h> // Audio and Display , Core Vidio
#import <OpenGL/gl3.h>             //gl.h
#import"vmath.h"
#import"sphere.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};

//ProtoType
//CallBackFunction
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp * , CVOptionFlags, CVOptionFlags *,void *);

FILE *aav_gpFile = NULL;

GLfloat aav_anglePyramid = 0.0f;
GLfloat aav_angleCube = 0.0f;

bool aav_bLight = false;
bool aav_bPerFragment = false;
bool aav_bPerVertex = true;
bool aav_bAnimation = false;

//Light Array
GLfloat aav_lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };    //la
GLfloat aav_lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };    // ld
GLfloat aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };    //ls

GLfloat aav_lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat aav_lightAngle1 = 0.0f;
GLfloat aav_lightAngle2 = 0.0f;
GLfloat aav_lightAngle3 = 0.0f;

@interface appDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>    //NeStep
//forwardDeclaration  extens  and implement
 @end


//main function
int main(int argc, char *argv[])
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    //Baap Object , magaicha , maza hinstace de
    //InBuilt Global object , jo NSApplication cha variable ahe
    NSApp = [NSApplication sharedApplication]; //asnara re NSapplication la
    
    [NSApp setDelegate:[[appDelegate alloc]init]];                          //^
    
    //Event loop , Message Loop
    [NSApp run]; // This is Called as Run Loop
    
    [pool release]; //1. program samtoi, all the memeory is reclamed
    
    return 0;
}

@interface MyOpenGLView: NSOpenGLView

@end

//implementation
@implementation appDelegate
{
    //class variables
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView;
}

//instance method
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; //ha path return hoto /User/UserName/Desktop/RTR/log/Window.app
    
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt",parentDirPath];
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    
    aav_gpFile = fopen(pszLogFileNameWithPath,"w");
    if(aav_gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
    }
    
    fprintf(aav_gpFile,"Program Started SuccessFully \n");
    
    
    //NSPoints Struct(x,y), NSSize Structure(width, height)  CG Point , CG Size
    NSRect win_rect = NSMakeRect(0.0,0.0,800.0,600.0); //internal CG Rect :Code Graphics as this "c" library so this is calling is C style
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                    NSWindowStyleMaskClosable |
                                                    NSWindowStyleMaskMiniaturizable |
                                                    NSWindowStyleMaskResizable
                                          backing: NSBackingStoreBuffered
                                          defer:NO];
    //styleMask is OVERLAPPEDWINDOW
    //NSWindowStyleMaskTitled is caption
    //NSWindowStyleMaskMiniaturizable is minimize
    //NSWindowStyleMaskResizable is resize
    //backing : retain karu nako , buffer madhe thav
    //Defer : NO , maje lagech dakhav , NO ha Objective C madhla Bool
    
    [window setTitle: @"AAV:OpenGL Three Rotating Lights On Sphere "]; //create window cha Second Parameter
    [window center]; //centering the window
    
    //view Creation
    myOpenGLView = [[MyOpenGLView alloc] initWithFrame:win_rect]; //NSView madhle methos
    
    [window setContentView: myOpenGLView]; // maza contect view set kar
    [window setDelegate: self]; // objective C mathla self ha c++ this
    [window makeKeyAndOrderFront:self]; //mazaya varti focuse kar , set focuse , get foucuse , set Forground window, z order la sartavar pudhe an
    
 }

//Apan Shevti Ithe Yato
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code
    
    
    if(aav_gpFile)
    {
        fprintf(aav_gpFile,"Program Ended SuccessFully \n");
        fclose(aav_gpFile);
        aav_gpFile = NULL;
    }
    
}

//NSWindow deligate  che window
-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
    
}

-(void)dealloc
{
    //code
    [myOpenGLView release];
    [window release];
    [super dealloc]; //mazya super la dealloc kar
}
@end


//MyView Implementation
@implementation MyOpenGLView
{
    //code
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint gVertexShaderObject_pv;
    GLuint gFragmentShaderObject_pv;
    GLuint gShaderProgramObject_pv;
    
    GLuint gVertexShaderObject_pf;
    GLuint gFragmentShaderObject_pf;
    GLuint gShaderProgramObject_pf;
    
    GLuint aav_mvp_MatrixUniform;
    
    mat4 aav_PerspectiveProjectionMatrix;
    
    //Sphere Variable
    GLfloat aav_sphere_vertices[1146];
    GLfloat aav_sphere_normals[1146];
    GLfloat aav_sphere_texture[764];
    short aav_sphere_elements[2280];
    GLuint aav_numSphereVertices;
    GLuint aav_numSphereElements;
    
    GLuint aav_gVao_sphere;
    GLuint aav_Vbo_sphere_position;
    GLuint aav_Vbo_sphere_normals;
    GLuint aav_Vbo_sphere_elements;
    
    struct Light
    {
        GLfloat aav_lightAmbiant[4];
        GLfloat aav_lightDiffuse[4];
        GLfloat aav_lightSpecular[4];
        GLfloat aav_lightPosition[4];
    };
    
    struct Light light[3];
    
    struct Material
    {
        GLfloat aav_materialAmbiant[4];
        GLfloat aav_materialDiffuse[4];
        GLfloat aav_materialSpecular[4];
        GLfloat aav_materialShininess;
    };
    
    struct Material material;
    
    GLuint aav_viewMatrixUniform_pf;
    GLuint aav_modelMatrixUniform_pf;
    GLuint aav_ProjectionMatrixUniform_pf;
    
    GLuint aav_laUniform_pf[3];
    GLuint aav_ldUniform_pf[3];
    GLuint aav_lsUniform_pf[3];
    GLuint aav_lightPositionUniform_pf[3];
    
    GLuint aav_kaUniform_pf;
    GLuint aav_kdUniform_pf;
    GLuint aav_ksUniform_pf;
    
    GLuint aav_kShininessUniform_pf;
    
    
    //VertexShader Uniforms
    
    GLuint aav_viewMatrixUniform_pv;
    GLuint aav_modelMatrixUniform_pv;
    GLuint aav_ProjectionMatrixUniform_pv;
    
    GLuint aav_laUniform_pv[3];
    GLuint aav_ldUniform_pv[3];
    GLuint aav_lsUniform_pv[3];
    GLuint aav_lightPositionUniform_pv[3];
    
    GLuint aav_kaUniform_pv;
    GLuint aav_kdUniform_pv;
    GLuint aav_ksUniform_pv;
    
    GLuint aav_kShininessUniform_pv;
    
    GLuint aav_lKeyPressedUniform;
    GLuint aav_lKeyPressedUniform_pv;
    
  
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        //Pixel Format Attribute
        NSOpenGLPixelFormatAttribute attributes[] =
        {
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };
        
        NSOpenGLPixelFormat * pixelFormal = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
        if(pixelFormal == nil)
        {
            fprintf(aav_gpFile,"Can Not Get pixelFormat Attribute");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormal shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormal];
        [self setOpenGLContext:glContext];
        
    }
    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)outputType
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init]; //New Thread is Created.
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

//initialize prepare OpenGL
-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];
    
    [[self openGLContext]makeCurrentContext];
    
    //swap Interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    

    //PerVertex
    //**VERTEX SHADER***
    gVertexShaderObject_pv = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* aav_vertexShaserSourceCode_pv =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_lKeyPressed_pv;" \
    "uniform vec3 u_la_pv[3];" \
    "uniform vec3 u_ld_pv[3];" \
    "uniform vec3 u_ls_pv[3];" \
    "uniform vec4 u_lightPosistion_pv[3];" \
    "uniform vec3 u_ka_pv;" \
    "uniform vec3 u_kd_pv;" \
    "uniform vec3 u_ks_pv;" \
    "uniform float u_kShineness_pv;"\
    "vec3 lightDirection[3];"\
    "vec3 reflection_vector[3];"\
    "vec3 ambiant[3];"\
    "vec3 diffuse[3];"\
    "vec3 specular[3];"\
    "vec3 tempCalculation = vec3(0.0f,0.0f,0.0f);"\
    "out vec3 fong_ads_light_pv;" \
    "void main(void)" \
    "{" \
    "    if(u_lKeyPressed_pv ==1)" \
    "    {" \
    "        vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
    "        vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
    "        vec3 view_vector = normalize(-eyeCordinate.xyz);" \
    "        for(int i = 0 ; i < 3; i++)" \
    "        {" \
    "            lightDirection[i] = normalize(vec3(u_lightPosistion_pv[i] - eyeCordinate));" \
    "            reflection_vector[i] = reflect(-lightDirection[i],tranformed_normal);" \
    "            ambiant[i] = u_la_pv[i] * u_ka_pv;" \
    "            diffuse[i] = u_ld_pv[i] * u_kd_pv * max(dot(lightDirection[i],tranformed_normal),0.0f);" \
    "            specular[i] = u_ls_pv[i] * u_ks_pv * pow(max(dot(reflection_vector[i],view_vector),0.0f),u_kShineness_pv);" \
    "            tempCalculation = tempCalculation + ambiant[i] + diffuse[i] + specular[i];" \
    "            fong_ads_light_pv = tempCalculation;"\
    "        }" \
    "    }" \
    "    else" \
    "    {" \
    "        fong_ads_light_pv = vec3(1.0f,1.0f,1.0f);" \
    "    }" \
    "   gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";
    glShaderSource(gVertexShaderObject_pv, 1, (const GLchar**)&aav_vertexShaserSourceCode_pv, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(gVertexShaderObject_pv);
    
    GLint aav_infoLogLength = 0;
    GLint aav_shaderCompiledStatus = 0;
    char* szBuffer = NULL;
    
    glGetShaderiv(gVertexShaderObject_pv, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(gVertexShaderObject_pv, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(aav_gpFile, "Vertex Shader PerVertex Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //PerVertex
    //**FRAGMENT SHADER**
    gFragmentShaderObject_pv = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *fragmentShaderSourceCode_pv =
    "#version 410 core" \
    "\n" \
    "in vec3 fong_ads_light_pv;" \
    "out vec4 FragColor_pv;" \
    "void main(void)" \
    "{" \
    "    FragColor_pv = vec4(fong_ads_light_pv,1.0f);" \
    "}";
    glShaderSource(gFragmentShaderObject_pv, 1,
                   (const GLchar**)&fragmentShaderSourceCode_pv,NULL);
    //compile shader
    glCompileShader(gFragmentShaderObject_pv);
    
    szBuffer = NULL;
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    
    glGetShaderiv(gFragmentShaderObject_pv, GL_COMPILE_STATUS,
                  &aav_shaderCompiledStatus);
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(gFragmentShaderObject_pv, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(aav_gpFile, "Fragment Shader PerVertex Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /****************************************************************/
    //PerFragment
    //**VERTEX SHADER***
    gVertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* aav_vertexShaserSourceCode_pf =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_lKeyPressed;" \
    "uniform vec4 u_lightPosistion_pf[3];" \
    "out vec3 tranformed_normal_pf;"\
    "out vec3 lightDirection_pf[3];" \
    "out vec3 view_vector_pf;" \
    "void main(void)" \
    "{" \
    "    if(u_lKeyPressed == 1)" \
    "    {"\
    "        vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
    "        tranformed_normal_pf = (mat3(u_view_matrix* u_model_matrix) * vNormal);" \
    "        view_vector_pf = (-eyeCordinate.xyz);" \
    "        for(int i = 0 ; i < 3 ; i++)"\
    "        {"\
    "            lightDirection_pf[i] = (vec3(u_lightPosistion_pf[i] - eyeCordinate));" \
    "        }"\
    "    }" \
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";
    glShaderSource(gVertexShaderObject_pf, 1, (const GLchar**)&aav_vertexShaserSourceCode_pf, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(gVertexShaderObject_pf);
    
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    szBuffer = NULL;
    
    glGetShaderiv(gVertexShaderObject_pf, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(gVertexShaderObject_pf, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(aav_gpFile, "Vertex Shader PerFramgment Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    
    //PerFragment
    //**FRAGMENT SHADER**
    gFragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar* fragmentShaderSourceCode_pf =
    "#version 410 core" \
    "\n" \
    "in vec3 tranformed_normal_pf;"\
    "in vec3 lightDirection_pf[3];" \
    "in vec3 view_vector_pf;" \
    "uniform int u_lKeyPressed;" \
    "uniform vec3 u_la_pf[3];" \
    "uniform vec3 u_ld_pf[3];" \
    "uniform vec3 u_ls_pf[3];" \
    "uniform vec3 u_ka_pf;" \
    "uniform vec3 u_kd_pf;" \
    "uniform vec3 u_ks_pf;" \
    "uniform float u_kShineness_pf;"\
    
    "vec3 normalize_tranformed_normal;"\
    "vec3 normalize_lightDirection[3];"\
    "vec3 normalize_view_vector;"\
    "vec3 reflection_vector[3];"\
    "vec3 ambiant[3];"\
    "vec3 diffuse[3];"\
    "vec3 specular[3];"\
    
    "out vec4 FragColor_pf;" \
    "vec3 fong_ads_light_pf;" \
    "void main(void)" \
    "{" \
    "    if(u_lKeyPressed == 1)" \
    "    {"\
    "        normalize_tranformed_normal = normalize(tranformed_normal_pf);" \
    "        normalize_view_vector = normalize(view_vector_pf);" \
    "        for(int i = 0; i < 3 ; i++)"\
    "        {" \
    "            normalize_lightDirection[i] = normalize(lightDirection_pf[i]);" \
    "            reflection_vector[i] = reflect(-normalize_lightDirection[i],normalize_tranformed_normal);" \
    "            ambiant[i] = u_la_pf[i] * u_ka_pf;" \
    "            diffuse[i] = u_ld_pf[i] * u_kd_pf * max(dot(normalize_lightDirection[i],normalize_tranformed_normal),0.0f);" \
    "            specular[i] = u_ls_pf[i] * u_ks_pf * pow(max(dot(reflection_vector[i],normalize_view_vector),0.0f),u_kShineness_pf);" \
    "            fong_ads_light_pf = fong_ads_light_pf + ambiant[i] + diffuse[i] + specular[i];" \
    "        }" \
    "    }" \
    "    else"\
    "    {"\
    "        fong_ads_light_pf = vec3(1.0f,1.0f,1.0f);"\
    "    }"\
    "    FragColor_pf = vec4(fong_ads_light_pf,1.0f);" \
    "}";
    glShaderSource(gFragmentShaderObject_pf, 1,
                   (const GLchar**)&fragmentShaderSourceCode_pf,NULL);
    //compile shader
    glCompileShader(gFragmentShaderObject_pf);
    
    szBuffer = NULL;
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    
    glGetShaderiv(gFragmentShaderObject_pf, GL_COMPILE_STATUS,
                  &aav_shaderCompiledStatus);
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(gFragmentShaderObject_pf, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(aav_gpFile, "Fragment Shader of PerFragment Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /****************************************************************/
    
    //**SHADER PROGRAM For PerVertex**
    //Create
    gShaderProgramObject_pv = glCreateProgram();
    
    glAttachShader(gShaderProgramObject_pv, gVertexShaderObject_pv);
    glAttachShader(gShaderProgramObject_pv, gFragmentShaderObject_pv);
    
    glBindAttribLocation(gShaderProgramObject_pv, AAV_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject_pv, AAV_ATTRIBUTE_NORMAL, "vNormal");
    
    glLinkProgram(gShaderProgramObject_pv);
    
    aav_infoLogLength = 0;
    GLint aav_shaderProgramLinkStatus = 0;
    szBuffer = NULL;
    
    glGetProgramiv(gShaderProgramObject_pv, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
    if (aav_shaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                GLsizei aav_aav_written;
                glGetProgramInfoLog(gShaderProgramObject_pv, aav_infoLogLength,
                                    &aav_aav_written, szBuffer);
                fprintf(aav_gpFile, "Shader Program PerVertex Link Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    
    /*************************Shader Program For PerFrament***********************/
    //Create
    gShaderProgramObject_pf = glCreateProgram();
    
    glAttachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);
    glAttachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);
    
    glBindAttribLocation(gShaderProgramObject_pf, AAV_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject_pf, AAV_ATTRIBUTE_NORMAL, "vNormal");
    
    glLinkProgram(gShaderProgramObject_pf);
    
    aav_infoLogLength = 0;
    aav_shaderProgramLinkStatus = 0;
    szBuffer = NULL;
    
    glGetProgramiv(gShaderProgramObject_pf, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
    if (aav_shaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                GLsizei aav_aav_written;
                glGetProgramInfoLog(gShaderProgramObject_pf, aav_infoLogLength,
                                    &aav_aav_written, szBuffer);
                fprintf(aav_gpFile, "Shader Program PerFragment Link Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    /*****************************************************************************/
    //Post Linking Information
    //PerVertex
    aav_modelMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_model_matrix");
    aav_viewMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_view_matrix");
    aav_ProjectionMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_projection_matrix");
    
    //Red Light
    aav_laUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_la_pv[0]");
    aav_ldUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_pv[0]");
    aav_lsUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_pv[0]");
    aav_lightPositionUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_lightPosistion_pv[0]");
    
    //Green Light
    aav_laUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_la_pv[1]");
    aav_ldUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_pv[1]");
    aav_lsUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_pv[1]");
    aav_lightPositionUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_lightPosistion_pv[1]");
    
    //Blue Light
    aav_laUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_la_pv[2]");
    aav_ldUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_pv[2]");
    aav_lsUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_pv[2]");
    aav_lightPositionUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_lightPosistion_pv[2]");
    
    aav_kaUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_ka_pv");
    aav_kdUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_kd_pv");
    aav_ksUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_ks_pv");
    
    aav_kShininessUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_kShineness_pv");
    
    aav_lKeyPressedUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_lKeyPressed_pv");
    
    /*************************************************************************************/
    //PerFragment
    aav_modelMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_model_matrix");
    aav_viewMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_view_matrix");
    aav_ProjectionMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_projection_matrix");
    
    //Red Light
    aav_laUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_la_pf[0]");
    aav_ldUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_ld_pf[0]");
    aav_lsUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_ls_pf[0]");
    aav_lightPositionUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_lightPosistion_pf[0]");
    
    //Green Light
    aav_laUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_la_pf[1]");
    aav_ldUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_ld_pf[1]");
    aav_lsUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_ls_pf[1]");
    aav_lightPositionUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_lightPosistion_pf[1]");
    
    //Blue light
    aav_laUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_la_pf[2]");
    aav_ldUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_ld_pf[2]");
    aav_lsUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_ls_pf[2]");
    aav_lightPositionUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_lightPosistion_pf[2]");
    
    aav_kaUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ka_pf");
    aav_kdUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_kd_pf");
    aav_ksUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ks_pf");
    
    aav_kShininessUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_kShineness_pf");
    
    aav_lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_lKeyPressed");
    
    /*****************************************************************************************/
    
    //vertices array declation
    Sphere *sphere = [[Sphere alloc]init];
    
    [sphere getSphereVertexData:aav_sphere_vertices :aav_sphere_normals :aav_sphere_texture :aav_sphere_elements];
    
    aav_numSphereVertices = [sphere getNumberOfSphereVertices];
    aav_numSphereElements = [sphere getNumberOfSphereElements];
    
    glGenVertexArrays(1, &aav_gVao_sphere);
    glBindVertexArray(aav_gVao_sphere);
    
    //Record Sphere
    glGenBuffers(1, &aav_Vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_vertices), aav_sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //normals
    glGenBuffers(1, &aav_Vbo_sphere_normals);
    glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_normals), aav_sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //elements
    glGenBuffers(1, &aav_Vbo_sphere_elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements); // Elemtnes Drawing is Also Called As Index Drawing, Elements Drawing
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(aav_sphere_elements), aav_sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    //Record Off / Pause
    glBindVertexArray(0);
    
   
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
   
    //
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    
    aav_PerspectiveProjectionMatrix = mat4::identity();
    
    //material
    material.aav_materialAmbiant[0] = 0.0f;
    material.aav_materialAmbiant[1] = 0.0f;
    material.aav_materialAmbiant[2] = 0.0f;
    material.aav_materialAmbiant[3] = 1.0f;
    
    material.aav_materialDiffuse[0] = 1.0f;
    material.aav_materialDiffuse[1] = 1.0f;
    material.aav_materialDiffuse[2] = 1.0f;
    material.aav_materialDiffuse[3] = 1.0f;
    
    material.aav_materialSpecular[0] = 1.0f;
    material.aav_materialSpecular[1] = 1.0f;
    material.aav_materialSpecular[2] = 1.0f;
    material.aav_materialSpecular[3] = 1.0f;
    
    material.aav_materialShininess = 128.0f;
    
    
    //Light
    //Red
    light[0].aav_lightAmbiant[0] = 0.0f;
    light[0].aav_lightAmbiant[1] = 0.0f;
    light[0].aav_lightAmbiant[2] = 0.0f;
    light[0].aav_lightAmbiant[3] = 1.0f;
    
    light[0].aav_lightDiffuse[0] = 1.0f;
    light[0].aav_lightDiffuse[1] = 0.0f;
    light[0].aav_lightDiffuse[2] = 0.0f;
    light[0].aav_lightDiffuse[3] = 1.0f;
    
    light[0].aav_lightSpecular[0] = 1.0f;
    light[0].aav_lightSpecular[1] = 0.0f;
    light[0].aav_lightSpecular[2] = 0.0f;
    light[0].aav_lightSpecular[3] = 1.0f;
    
    light[0].aav_lightPosition[0] = 0.0f;
    light[0].aav_lightPosition[1] = 0.0f;
    light[0].aav_lightPosition[2] = 0.0f;
    light[0].aav_lightPosition[3] = 1.0f;
    
    //Green
    light[1].aav_lightAmbiant[0] = 0.0f;
    light[1].aav_lightAmbiant[1] = 0.0f;
    light[1].aav_lightAmbiant[2] = 0.0f;
    light[1].aav_lightAmbiant[3] = 1.0f;
    
    light[1].aav_lightDiffuse[0] = 0.0f;
    light[1].aav_lightDiffuse[1] = 1.0f;
    light[1].aav_lightDiffuse[2] = 0.0f;
    light[1].aav_lightDiffuse[3] = 1.0f;
    
    light[1].aav_lightSpecular[0] = 0.0f;
    light[1].aav_lightSpecular[1] = 1.0f;
    light[1].aav_lightSpecular[2] = 0.0f;
    light[1].aav_lightSpecular[3] = 1.0f;
    
    light[1].aav_lightPosition[0] = 0.0f;
    light[1].aav_lightPosition[1] = 0.0f;
    light[1].aav_lightPosition[2] = 0.0f;
    light[1].aav_lightPosition[3] = 1.0f;
    
    //Blue
    light[2].aav_lightAmbiant[0] = 0.0f;
    light[2].aav_lightAmbiant[1] = 0.0f;
    light[2].aav_lightAmbiant[2] = 0.0f;
    light[2].aav_lightAmbiant[3] = 1.0f;
    
    light[2].aav_lightDiffuse[0] = 0.0f;
    light[2].aav_lightDiffuse[1] = 0.0f;
    light[2].aav_lightDiffuse[2] = 1.0f;
    light[2].aav_lightDiffuse[3] = 1.0f;
    
    light[2].aav_lightSpecular[0] = 0.0f;
    light[2].aav_lightSpecular[1] = 0.0f;
    light[2].aav_lightSpecular[2] = 1.0f;
    light[2].aav_lightSpecular[3] = 1.0f;
    
    light[2].aav_lightPosition[0] = 0.0f;
    light[2].aav_lightPosition[1] = 0.0f;
    light[2].aav_lightPosition[2] = 0.0f;
    light[2].aav_lightPosition[3] = 1.0f;
    
    
    //create displayLink
    //CallBackSet karne
    //NSPixelFormat to Core
    //CGL
    //DisplayLinkStart
    
    //CV and CGL related Code
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDispalyLinkCallBack,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPiexelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPiexelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    //code
    [super reshape];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    if(rect.size.height < 0)
        rect.size.height = 1;
    glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);
    
   aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)rect.size.width / (GLfloat)rect.size.height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//invalidateRect
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    mat4 aav_modelMateix;
    mat4 aav_viewMatrix;
    mat4 aav_translateMatrix;
    
    light[0].aav_lightPosition[0] = 0.0f;
    light[0].aav_lightPosition[1] = 5 * sin(aav_lightAngle1);
    light[0].aav_lightPosition[2] = 5 * cos(aav_lightAngle1);
    light[0].aav_lightPosition[3] = 1.0f;
    
    light[1].aav_lightPosition[0] = 5 * sin(aav_lightAngle1);
    light[1].aav_lightPosition[1] = 0.0f;
    light[1].aav_lightPosition[2] = 5 * cos(aav_lightAngle1);
    light[1].aav_lightPosition[3] = 1.0f;
    
    light[2].aav_lightPosition[0] = 5 * sin(aav_lightAngle1);
    light[2].aav_lightPosition[1] = 5 * cos(aav_lightAngle1);
    light[2].aav_lightPosition[2] = 0.0f;
    light[2].aav_lightPosition[3] = 1.0f;
    
    if (aav_bPerVertex == true)
    {
        glUseProgram(gShaderProgramObject_pv);
        
        if (aav_bLight == true)
        {
            glUniform1i(aav_lKeyPressedUniform_pv, 1);
            //Light
            //Red
            glUniform3fv(aav_laUniform_pv[0], 1, (GLfloat*)light[0].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pv[0], 1, (GLfloat*)light[0].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pv[0], 1, (GLfloat*)light[0].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pv[0], 1, (GLfloat*)light[0].aav_lightPosition);
            //Green
            glUniform3fv(aav_laUniform_pv[1], 1, (GLfloat*)light[1].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pv[1], 1, (GLfloat*)light[1].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pv[1], 1, (GLfloat*)light[1].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pv[1], 1, (GLfloat*)light[1].aav_lightPosition);
            //Blue
            glUniform3fv(aav_laUniform_pv[2], 1, (GLfloat*)light[2].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pv[2], 1, (GLfloat*)light[2].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pv[2], 1, (GLfloat*)light[2].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pv[2], 1, (GLfloat*)light[2].aav_lightPosition);
            
            //material
            glUniform3fv(aav_kaUniform_pv, 1, (GLfloat*)material.aav_materialAmbiant);
            glUniform3fv(aav_kdUniform_pv, 1, (GLfloat*)material.aav_materialDiffuse);
            glUniform3fv(aav_ksUniform_pv, 1, (GLfloat*)material.aav_materialSpecular);
            glUniform1f(aav_kShininessUniform_pv, material.aav_materialShininess);
        }
        else
        {
            glUniform1i(aav_lKeyPressedUniform_pv, 0);
        }
        
        //OpenGL Drawing
        aav_modelMateix = mat4::identity();
        
        aav_viewMatrix = mat4::identity();
        
        aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        
        aav_modelMateix = aav_translateMatrix;
        
        glUniformMatrix4fv(aav_modelMatrixUniform_pv, 1, GL_FALSE, aav_modelMateix);
        glUniformMatrix4fv(aav_viewMatrixUniform_pv, 1, GL_FALSE, aav_viewMatrix);
        glUniformMatrix4fv(aav_ProjectionMatrixUniform_pv, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
        
        glBindVertexArray(aav_gVao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
        
        glUseProgram(0);
    }
    if(aav_bPerFragment == true)
    {
        glUseProgram(gShaderProgramObject_pf);
        
        if (aav_bLight == true)
        {
            glUniform1i(aav_lKeyPressedUniform, 1);
            //Light
            //Red
            glUniform3fv(aav_laUniform_pf[0], 1, (GLfloat*)light[0].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pf[0], 1, (GLfloat*)light[0].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pf[0], 1, (GLfloat*)light[0].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pf[0], 1, (GLfloat*)light[0].aav_lightPosition);
            //Green
            glUniform3fv(aav_laUniform_pf[1], 1, (GLfloat*)light[1].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pf[1], 1, (GLfloat*)light[1].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pf[1], 1, (GLfloat*)light[1].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pf[1], 1, (GLfloat*)light[1].aav_lightPosition);
            //Blue
            glUniform3fv(aav_laUniform_pf[2], 1, (GLfloat*)light[2].aav_lightAmbiant);
            glUniform3fv(aav_ldUniform_pf[2], 1, (GLfloat*)light[2].aav_lightDiffuse);
            glUniform3fv(aav_lsUniform_pf[2], 1, (GLfloat*)light[2].aav_lightSpecular);
            glUniform4fv(aav_lightPositionUniform_pf[2], 1, (GLfloat*)light[2].aav_lightPosition);
            
            //material
            glUniform3fv(aav_kaUniform_pf, 1, (GLfloat*)material.aav_materialAmbiant);
            glUniform3fv(aav_kdUniform_pf, 1, (GLfloat*)material.aav_materialDiffuse);
            glUniform3fv(aav_ksUniform_pf, 1, (GLfloat*)material.aav_materialSpecular);
            glUniform1f(aav_kShininessUniform_pf, material.aav_materialShininess);
        }
        else
        {
            glUniform1i(aav_lKeyPressedUniform, 0);
        }
        
        //OpenGL Drawing
        aav_modelMateix = mat4::identity();
        
        aav_viewMatrix = mat4::identity();
        
        aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
        
        aav_modelMateix = aav_translateMatrix;
        
        glUniformMatrix4fv(aav_modelMatrixUniform_pf, 1, GL_FALSE, aav_modelMateix);
        glUniformMatrix4fv(aav_viewMatrixUniform_pf, 1, GL_FALSE, aav_viewMatrix);
        glUniformMatrix4fv(aav_ProjectionMatrixUniform_pf, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
        
        glBindVertexArray(aav_gVao_sphere);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
        glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
        glBindVertexArray(0);
        
        glUseProgram(0);
    }
    
    if (aav_bAnimation == true)
    {
        aav_lightAngle1 = aav_lightAngle1 + 0.01f;
        if (aav_lightAngle1 > 360)
        {
            aav_lightAngle1 = 0.0f;
        }
        
        aav_lightAngle2 = aav_lightAngle2 + 0.01f;
        if (aav_lightAngle2 > 360)
        {
            aav_lightAngle2 = 0.0f;
        }
        
        aav_lightAngle3 = aav_lightAngle3  + 0.01f;
        if (aav_lightAngle3 > 360)
        {
            aav_lightAngle3 = 0.0f;
        }
    }
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//NSResponder methos
-(BOOL)acceptsFirstResponder
{
    //code
    //[[self window] makeFirstResponder ];
    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27:
             [[self window] toggleFullScreen:self]; //Full Screen
             
            break;
        case 'Q':
        case 'q':
            [self release];
            [NSApp terminate:self];
            break;
        case 'l':
        case 'L':
            if (aav_bLight == false)
            {
                aav_bLight = true;
            }
            else
            {
                aav_bLight = false;
            }
            break;
        case 'f':
        case 'F':
            if (aav_bPerFragment == false)
            {
                aav_bPerVertex = false;
                aav_bPerFragment = true;
            }
            
            break;
        case 'v':
        case 'V':
            if (aav_bPerVertex == false)
            {
                aav_bPerFragment = false;
                aav_bPerVertex = true;
            }
            break;
        case 'a':
        case 'A':
            if (aav_bAnimation == false)
            {
                aav_bAnimation = true;
            }
            else
            {
                aav_bAnimation = false;
            }
            break;
        default:
            break;
        
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
 
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)dealloc
{
    //code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    if (aav_gVao_sphere)
    {
        glDeleteVertexArrays(1, &aav_gVao_sphere);
        aav_gVao_sphere = 0;
    }
    
    if (aav_Vbo_sphere_position)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_position);
        aav_Vbo_sphere_position = 0;
    }
    
    if (aav_Vbo_sphere_normals)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_normals);
        aav_Vbo_sphere_normals = 0;
    }
    
    if (aav_Vbo_sphere_elements)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_elements);
        aav_Vbo_sphere_elements = 0;
    }
    
    
    glDetachShader(gShaderProgramObject_pv, gVertexShaderObject_pv);
    glDetachShader(gShaderProgramObject_pv, gFragmentShaderObject_pv);
    
    glDeleteShader(gVertexShaderObject_pv);
    gVertexShaderObject_pv = 0;
    glDeleteShader(gFragmentShaderObject_pv);
    gFragmentShaderObject_pv = 0;
    
    glDeleteProgram(gShaderProgramObject_pv);
    gShaderProgramObject_pv = 0;
    
    
    glDetachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);
    glDetachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);
    
    glDeleteShader(gVertexShaderObject_pf);
    gVertexShaderObject_pv = 0;
    glDeleteShader(gFragmentShaderObject_pf);
    gFragmentShaderObject_pf = 0;
    
    glDeleteProgram(gShaderProgramObject_pf);
    gShaderProgramObject_pf = 0;
    
    glUseProgram(0);
    
    [super dealloc];
}
@end


//Global Function
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp * outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut,void *displayLinkContext)
{
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return result;
}
