#import <Foundation/Foundation.h>
#import <cocoa/cocoa.h>             //analogous to windows.h xlib.h

#import <QuartzCore/CVDisplayLink.h> // Audio and Display , Core Vidio
#import <OpenGL/gl3.h>             //gl.h
#import"vmath.h"

#define PI  3.14

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};

//ProtoType
//CallBackFunction
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp * , CVOptionFlags, CVOptionFlags *,void *);

FILE *aav_gpFile = NULL;
int count = 0; // Number of interation in Circle Loop
@interface appDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>    //NeStep
//forwardDeclaration  extens  and implement
 @end


//main function
int main(int argc, char *argv[])
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    //Baap Object , magaicha , maza hinstace de
    //InBuilt Global object , jo NSApplication cha variable ahe
    NSApp = [NSApplication sharedApplication]; //asnara re NSapplication la
    
    [NSApp setDelegate:[[appDelegate alloc]init]];                          //^
    
    //Event loop , Message Loop
    [NSApp run]; // This is Called as Run Loop
    
    [pool release]; //1. program samtoi, all the memeory is reclamed
    
    return 0;
}

@interface MyOpenGLView: NSOpenGLView

@end

//implementation
@implementation appDelegate
{
    //class variables
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView;
}

//instance method
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; //ha path return hoto /User/UserName/Desktop/RTR/log/Window.app
    
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt",parentDirPath];
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    
    aav_gpFile = fopen(pszLogFileNameWithPath,"w");
    if(aav_gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
    }
    
    fprintf(aav_gpFile,"Program Started SuccessFully \n");
    
    
    //NSPoints Struct(x,y), NSSize Structure(width, height)  CG Point , CG Size
    NSRect win_rect = NSMakeRect(0.0,0.0,800.0,600.0); //internal CG Rect :Code Graphics as this "c" library so this is calling is C style
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                    NSWindowStyleMaskClosable |
                                                    NSWindowStyleMaskMiniaturizable |
                                                    NSWindowStyleMaskResizable
                                          backing: NSBackingStoreBuffered
                                          defer:NO];
    //styleMask is OVERLAPPEDWINDOW
    //NSWindowStyleMaskTitled is caption
    //NSWindowStyleMaskMiniaturizable is minimize
    //NSWindowStyleMaskResizable is resize
    //backing : retain karu nako , buffer madhe thav
    //Defer : NO , maje lagech dakhav , NO ha Objective C madhla Bool
    
    [window setTitle: @"AAV:OpenGL Graph Paper With Shapes"]; //create window cha Second Parameter
    [window center]; //centering the window
    
    //view Creation
    myOpenGLView = [[MyOpenGLView alloc] initWithFrame:win_rect]; //NSView madhle methos
    
    [window setContentView: myOpenGLView]; // maza contect view set kar
    [window setDelegate: self]; // objective C mathla self ha c++ this
    [window makeKeyAndOrderFront:self]; //mazaya varti focuse kar , set focuse , get foucuse , set Forground window, z order la sartavar pudhe an
    
 }

//Apan Shevti Ithe Yato
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code
    if(aav_gpFile)
    {
        fprintf(aav_gpFile,"Program Ended SuccessFully \n");
        fclose(aav_gpFile);
        aav_gpFile = NULL;
    }
    
}

//NSWindow deligate  che window
-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
    
}

-(void)dealloc
{
    //code
    [myOpenGLView release];
    [window release];
    [super dealloc]; //mazya super la dealloc kar
}
@end


//MyView Implementation
@implementation MyOpenGLView
{
    //code
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint aav_gVertexShaderObject;
    GLuint aav_gFragmentShaderObject;
    GLuint aav_gShaderProgramObject;
    
    GLuint aav_gVao_triangle;
    GLuint aav_gVbo_position_triangle;
    GLuint aav_gVbo_color_triangle;
    
    GLuint aav_gVao_square;
    GLuint aav_gVbo_position_square;
    GLuint aav_gVbo_color_square;
    
    GLuint aav_gVao_line;
    GLuint aav_gVbo_line_position;
    GLuint aav_aVbo_line_color;
    
    GLuint aav_gVao_circle;
    GLuint aav_gVbo_circle_position;
    GLuint aav_aVbo_circle_color;
    
    GLuint aav_gVao_circleInner;
    GLuint aav_gVbo_circleInner_position;
    GLuint aav_aVbo_circleInner_color;
    
    GLuint aav_gVao_point;
    GLuint aav_gVbo_point_position;
    GLuint aav_gVbo_point_color;
    
    GLuint aav_mvp_MatrixUniform;
    
   
    mat4 aav_PerspectiveProjectionMatrix;
    
    GLfloat* aav_circleVertices;
    GLfloat* aav_circleColor;
    
    GLfloat* aav_circleVerticesInner;
    GLfloat* aav_circleColorInner;
    
    GLfloat r;       // radius of innder circle
    
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        //Pixel Format Attribute
        NSOpenGLPixelFormatAttribute attributes[] =
        {
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };
        
        NSOpenGLPixelFormat * pixelFormal = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
        if(pixelFormal == nil)
        {
            fprintf(aav_gpFile,"Can Not Get pixelFormat Attribute");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormal shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormal];
        [self setOpenGLContext:glContext];
        
    }
    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)outputType
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init]; //New Thread is Created.
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

//initialize prepare OpenGL
-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];
    
    [[self openGLContext]makeCurrentContext];
    
    //swap Interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    
    //**VERTEX SHADER***
    aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* aav_vertexShaserSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec4 vColor;" \
    "uniform mat4 aav_u_mvpMatrix;" \
    "out vec4 aav_out_color;" \
    "void main(void)" \
    "{" \
    "gl_Position = aav_u_mvpMatrix * vPosition;" \
    "aav_out_color = vColor;" \
    "}";
    glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(aav_gVertexShaderObject);
    
    GLint aav_infoLogLength = 0;
    GLint aav_shaderCompiledStatus = 0;
    char* aav_szBuffer = NULL;
    
    glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
                                   &aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**FRAGMENT SHADER**
    aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 aav_out_color;" \
    "out vec4 aav_FragColor;" \
    "void main(void)" \
    "{" \
    "aav_FragColor = aav_out_color;" \
    "}";
    glShaderSource(aav_gFragmentShaderObject, 1,
                   (const GLchar**)&fragmentShaderSourceCode,NULL);
    //compile shader
    glCompileShader(aav_gFragmentShaderObject);
    
    
    aav_szBuffer = NULL;
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    
    glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS,
                  &aav_shaderCompiledStatus);
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
                                   &aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**SHADER PROGRAM**
    //Create
    aav_gShaderProgramObject = glCreateProgram();
    
    glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_COLOR, "vColor");
    
    glLinkProgram(aav_gShaderProgramObject);
    
    aav_infoLogLength = 0;
    GLint aav_shaderProgramLinkStatus = 0;
    aav_szBuffer = NULL;
    
    glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
    if (aav_shaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                GLsizei aav_aav_written;
                glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
                                    &aav_aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Shader Program Link Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //Post Linking Information
    aav_mvp_MatrixUniform =  glGetUniformLocation(aav_gShaderProgramObject, "aav_u_mvpMatrix");
    
    //vertices array declation
    const GLfloat aav_triangleVertices[] =
    {
        0.0f,0.5f,0.0f,
        -0.5f,-0.5f,0.0f,
        
        -0.5f,-0.5f,0.0f,
        0.5f,-0.5f,0.0f,
        
        0.5f,-0.5f,0.0f,
        0.0f,0.5f,0.0f,
    };
    
    const GLfloat aav_trianglecolors[] =
    {
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f
    };
    
    const GLfloat aav_cubeVertex[] =
    {
        0.5f,0.5f,0.0f,
        -0.5f,0.5f,0.0f,
        
        -0.5f,0.5f,0.0f,
        -0.5f,-0.5f,0.0f,
        
        -0.5f,-0.5f,0.0f,
        0.5f,-0.5f,0.0f,
        
        0.5f,-0.5f,0.0f,
        0.5f,0.5f,0.0f
    };
    
    const GLfloat aav_cubecolor[] =
    {
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f,
        1.0f,1.0f,1.0f
    };
    
    const GLfloat aav_lineVertex[] =
    {
        0.0f,-1.3f,0.0f,
        0.0f, 1.3f,0.0f,
        -2.0f,0.0f,0.0f,
        2.0f,0.0f,0.0f,
        
        
        0.2f,-2.0f,0.0f,
        0.2f, 2.0f,0.0f,
        
        
        0.4f,-2.0f,0.0f,
        0.4f, 2.0f,0.0f,
        
        
        0.6f,-2.0f,0.0f,
        0.6f, 2.0f,0.0f,
        
        0.8f,-2.0f,0.0f,
        0.8f, 2.0f,0.0f,
        
        
        1.0f,-2.0f,0.0f,
        1.0f, 2.0f,0.0f,
        
        1.2f,-2.0f,0.0f,
        1.2f, 2.0f,0.0f,
        
        
        1.4f,-2.0f,0.0f,
        1.4f, 2.0f,0.0f,
        
        
        1.6f,-2.0f,0.0f,
        1.6f, 2.0f,0.0f,
        
        
        1.8f,-2.0f,0.0f,
        1.8f, 2.0f,0.0f,
        
        2.0f,-2.0f,0.0f,
        2.0f, 2.0f,0.0f,
        
        
        //x negative
        -0.2f,-2.0f,0.0f,
        -0.2f, 2.0f,0.0f,
        
        
        -0.4f,-2.0f,0.0f,
        -0.4f, 2.0f,0.0f,
        
        
        -0.6f,-2.0f,0.0f,
        -0.6f, 2.0f,0.0f,
        
        -0.8f,-2.0f,0.0f,
        -0.8f, 2.0f,0.0f,
        
        
        -1.0f,-2.0f,0.0f,
        -1.0f, 2.0f,0.0f,
        
        -1.2f,-2.0f,0.0f,
        -1.2f, 2.0f,0.0f,
        
        
        -1.4f,-2.0f,0.0f,
        -1.4f, 2.0f,0.0f,
        
        
        -1.6f,-2.0f,0.0f,
        -1.6f, 2.0f,0.0f,
        
        
        -1.8f,-2.0f,0.0f,
        -1.8f, 2.0f,0.0f,
        
        -2.0f,-2.0f,0.0f,
        -2.0f, 2.0f,0.0f,
        
        //y axies
        -2.0f,0.13f,0.0f,
        2.0f,0.13f,0.0f,
        
        
        -2.0f,0.26f,0.0f,
        2.0f,0.26f,0.0f,
        
        
        -2.0f,0.39f,0.0f,
        2.0f,0.39f,0.0f,
        
        
        -2.0f,0.52f,0.0f,
        2.0f,0.52f,0.0f,
        
        -2.0f, 0.65f, 0.0f,
        2.0f, 0.65f, 0.0f,
        
        
        -2.0f,0.78f,0.0f,
        2.0f,0.78f,0.0f,
        
        
        -2.0f, 0.91f, 0.0f,
        2.0f, 0.91f, 0.0f,
        
        
        -2.0f,1.04f,0.0f,
        2.0f,1.04f,0.0f,
        
        -2.0f,1.17f,0.0f,
        2.0f, 1.17f,0.0f,
        
        -2.0f, 1.3f, 0.0f,
        2.0f, 1.3f, 0.0f,
        
        //
        -2.0f, -0.13f, 0.0f,
        2.0f, -0.13f, 0.0f,
        
        
        -2.0f, -0.26f, 0.0f,
        2.0f, -0.26f, 0.0f,
        
        
        -2.0f, -0.39f, 0.0f,
        2.0f, -0.39f, 0.0f,
        
        
        -2.0f, -0.52f, 0.0f,
        2.0f, -0.52f, 0.0f,
        
        -2.0f, -0.65f, 0.0f,
        2.0f, -0.65f, 0.0f,
        
        
        -2.0f, -0.78f, 0.0f,
        2.0f, -0.78f, 0.0f,
        
        
        -2.0f, -0.91f, 0.0f,
        2.0f, -0.91f, 0.0f,
        
        
        -2.0f, -1.04f, 0.0f,
        2.0f, -1.04f, 0.0f,
        
        -2.0f, -1.17f, 0.0f,
        2.0f, -1.17f, 0.0f,
        
        -2.0f, -1.3f, 0.0f,
        2.0f, -1.3f, 0.0f
    };
    
    const GLfloat aav_lineColor[] =
    {
        1.0f,0.0f,0.0f,
        1.0f,0.0f,0.0f,
        0.0f,1.0f,0.0f,
        0.0f,1.0f,0.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        //
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f,
        
        0.0f,0.0f,1.0f,
        0.0f,0.0f,1.0f
        
    };
    
    const GLfloat aav_pointVertex[] =
    {
        0.0f,0.0f,0.0f
    };
    
    const GLfloat aav_pointColor[] =
    {
        1.0f,1.0f,1.0f
    };
    
    [self Calculation];
    
    glGenVertexArrays(1, &aav_gVao_triangle);
    glBindVertexArray(aav_gVao_triangle);
    
    //Record Triangle
    glGenBuffers(1, &aav_gVbo_position_triangle);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position_triangle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_triangleVertices), aav_triangleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &aav_gVbo_color_triangle);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_color_triangle);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_trianglecolors), aav_trianglecolors, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Record Square
    glGenVertexArrays(1,&aav_gVao_square);
    glBindVertexArray(aav_gVao_square);
    
    glGenBuffers(1,&aav_gVbo_position_square);
    glBindBuffer(GL_ARRAY_BUFFER,aav_gVbo_position_square);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeVertex), aav_cubeVertex,GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &aav_gVbo_color_square);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_color_square);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubecolor), aav_cubecolor, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Line midle Line X and Y
    glGenVertexArrays(1, &aav_gVao_line);
    glBindVertexArray(aav_gVao_line);
    
    glGenBuffers(1, &aav_gVbo_line_position);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_line_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_lineVertex), aav_lineVertex, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &aav_aVbo_line_color);
    glBindBuffer(GL_ARRAY_BUFFER, aav_aVbo_line_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_lineColor), aav_lineColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // Outer circle
    glGenVertexArrays(1, &aav_gVao_circle);
    glBindVertexArray(aav_gVao_circle);
    
    glGenBuffers(1, &aav_gVbo_circle_position);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_circle_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* count * 3, aav_circleVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &aav_aVbo_circle_color);
    glBindBuffer(GL_ARRAY_BUFFER, aav_aVbo_circle_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)*count * 3, aav_circleColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Inner Circle
    glGenVertexArrays(1, &aav_gVao_circleInner);
    glBindVertexArray(aav_gVao_circleInner);
    
    glGenBuffers(1, &aav_gVbo_circleInner_position);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_circleInner_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* count * 3, aav_circleVerticesInner, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &aav_aVbo_circleInner_color);
    glBindBuffer(GL_ARRAY_BUFFER, aav_aVbo_circleInner_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* count * 3, aav_circleColorInner, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Center Point
    //aav_gVao_point;
    glGenVertexArrays(1, &aav_gVao_point);
    glBindVertexArray(aav_gVao_point);
    
    glGenBuffers(1, &aav_gVbo_point_position);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_point_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pointVertex), aav_pointVertex, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &aav_aVbo_circleInner_color);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_point_color);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pointColor) , aav_pointColor, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Record Off / Pause
    glBindVertexArray(0);
    
   
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
   
    //glEnable(GL_CULL_FACE);
    
    //
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    
    aav_PerspectiveProjectionMatrix = mat4::identity();
    
    //create displayLink
    //CallBackSet karne
    //NSPixelFormat to Core
    //CGL
    //DisplayLinkStart
    
    //CV and CGL related Code
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDispalyLinkCallBack,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPiexelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPiexelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)Calculation
{
    GLint Line = 0.0f;
    GLfloat fLine = 0.5f;
    GLfloat side;
    GLfloat Squareside;
    GLfloat OutSideCircleR;
    
    GLdouble a; //Consider side 1
    GLdouble b; //Consider side 2
    GLdouble c; //Consider side 3
    
    GLdouble xcenterCor;
    GLdouble ycenterCor;
    
    //Code
    a = sqrt(((0.5 - (-0.5)) * (0.5 - (-0.5))) + (((-0.5) - (-0.5)) * ((-0.5) - (-0.5)))); //1
    b = sqrt(((0.0 - 0.5) * (0.0 - 0.5)) + ((0.5 - (-0.5)) * (0.5 - (-0.5)))); //1.118033989
    c = sqrt((((-0.5) - 0.0) * ((-0.5) - 0.0)) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); //1.118033989
    
    //Formula For x,y Cordinate for Center Point of Triangle To Draw the Circle.
    xcenterCor = ((a * (0)) + (b * (-0.5)) + (c * (0.5))) / (a + b + c);
    ycenterCor = ((a * (0.5f)) + (b * (-0.5f)) + (c * (-0.5f))) / (a + b + c);
    
    side = (a + b + c) / 2;
    r = sqrt(side * (side - a) * (side - b) * (side - c)) / side;  //Formmula For inCircle or inscribed Circle Radius.
    
    //Circle OutSide Rectangle
    Squareside = sqrt((((-0.5) - (-0.5)) * ((-0.5) - (-0.5))) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); // We Get The Length of One Size of Square
    OutSideCircleR = (Squareside * sqrt(2)) / 2; // We Get Radius of Circle
    
    
    for (GLfloat angle = 0; angle < 2 * PI; angle = angle + 0.001)
    {
        count = count + 1;
    }
    
    aav_circleVertices = (GLfloat*)malloc(sizeof(GLfloat)*count * 3);
    aav_circleColor = (GLfloat*)malloc(sizeof(GLfloat) * count * 3);
    
    int i = 0;
    for (GLfloat angle = 0; angle < 2 * PI; angle = angle + 0.001)
    {
        aav_circleVertices[i] = OutSideCircleR * sin(angle);
        aav_circleColor[i] = 1.0f;
        i = i + 1;
        aav_circleVertices[i] = OutSideCircleR * cos(angle);
        aav_circleColor[i] = 1.0f;
        i = i + 1;
        aav_circleVertices[i] = 0.0f;
        aav_circleColor[i] = 1.0f;
        i = i + 1;
    }
    
    i = 0;
    aav_circleVerticesInner = (GLfloat*)malloc(sizeof(GLfloat) * count * 3);
    aav_circleColorInner = (GLfloat*)malloc(sizeof(GLfloat) * count * 3);
    for (GLfloat angle = 0; angle < 2 * PI; angle = angle + 0.001)
    {
        aav_circleVerticesInner[i] = r * sin(angle)  + +xcenterCor;
        aav_circleColorInner[i] = 1.0f;
        i = i + 1;
        aav_circleVerticesInner[i] = r * cos(angle)+ +ycenterCor;
        aav_circleColorInner[i] = 1.0f;
        i = i + 1;
        aav_circleVerticesInner[i] = 0.0f;
        aav_circleColorInner[i] = 1.0f;
        i = i + 1;
    }
}

-(void)reshape
{
    //code
    [super reshape];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    if(rect.size.height < 0)
        rect.size.height = 1;
    glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);
    
   aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)rect.size.width / (GLfloat)rect.size.height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//invalidateRect
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    mat4 aav_scaMatrix;
    mat4 aav_translateMatrix;
    mat4 aav_modelViewMateix;
    mat4 aav_modelViewProjectMatrix;
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //triangle
    glUseProgram(aav_gShaderProgramObject);
    
    //OpenGL Drawing
    glPointSize(1.0f);
    //Lines
    aav_modelViewMateix = mat4::identity();
    aav_modelViewProjectMatrix = mat4::identity();
    
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    aav_modelViewMateix = aav_translateMatrix;
    aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
    
    glBindVertexArray(aav_gVao_line);
    glDrawArrays(GL_LINES, 0, 84);
    glBindVertexArray(0);
    
    //rectangle
    aav_modelViewMateix = mat4::identity();
    aav_modelViewProjectMatrix = mat4::identity();
    
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    aav_modelViewMateix = aav_translateMatrix;
    aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform,1,GL_FALSE, aav_modelViewProjectMatrix);
    
    glBindVertexArray(aav_gVao_triangle);
    glDrawArrays(GL_LINES, 0, 8);
    glBindVertexArray(0);
    
    //square
    aav_modelViewMateix = mat4::identity();
    aav_modelViewProjectMatrix = mat4::identity();
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    aav_modelViewMateix = aav_translateMatrix;
    aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
    
    glBindVertexArray(aav_gVao_square);
    glDrawArrays(GL_LINES, 0, 8);
    glBindVertexArray(0);
    
    //Outer circle
    aav_modelViewMateix = mat4::identity();
    aav_modelViewProjectMatrix = mat4::identity();
    
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    aav_modelViewMateix = aav_translateMatrix;
    aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
    glBindVertexArray(aav_gVao_circle);
    glDrawArrays(GL_POINTS, 0,count);
    glBindVertexArray(0);
    
    //Inner
    aav_modelViewMateix = mat4::identity();
    aav_modelViewProjectMatrix = mat4::identity();
    
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    aav_modelViewMateix = aav_translateMatrix;
    aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
    
    glBindVertexArray(aav_gVao_circleInner);
    glDrawArrays(GL_POINTS, 0, count);
    glBindVertexArray(0);
    
    //Center Point
    aav_modelViewMateix = mat4::identity();
    aav_modelViewProjectMatrix = mat4::identity();
    
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    aav_modelViewMateix = aav_translateMatrix;
    aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
    glPointSize(5.0f);
    glBindVertexArray(aav_gVao_point);
    glDrawArrays(GL_POINTS, 0, 1);
    glBindVertexArray(0);
    
    
    glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//NSResponder methos
-(BOOL)acceptsFirstResponder
{
    //code
    //[[self window] makeFirstResponder ];
    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
             
            break;
        case 'F':
        case 'f':
        [[self window] toggleFullScreen:self]; //Full Screen
            break;
        
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
 
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)dealloc
{
    //code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    if (aav_gVao_triangle)
    {
        glDeleteVertexArrays(1, &aav_gVao_triangle);
        aav_gVao_triangle = 0;
    }
    
    if (aav_gVbo_position_triangle)
    {
        glDeleteBuffers(1, &aav_gVbo_position_triangle);
        aav_gVbo_position_triangle = 0;
    }
    
    if (aav_gVbo_color_triangle)
    {
        glDeleteBuffers(1, &aav_gVbo_color_triangle);
        aav_gVbo_color_triangle = 0;
    }
    
    if (aav_gVao_square)
    {
        glDeleteVertexArrays(1, &aav_gVao_square);
        aav_gVao_square = 0;
    }
    
    if (aav_gVbo_position_square)
    {
        glDeleteBuffers(1, &aav_gVbo_position_square);
        aav_gVbo_position_square = 0;
    }
    
    if (aav_gVbo_color_square)
    {
        glDeleteBuffers(1, &aav_gVbo_color_square);
        aav_gVbo_color_square = 0;
    }
    
    
    if (aav_gVao_circle)
    {
        glDeleteVertexArrays(1, &aav_gVao_circle);
        aav_gVao_circle = 0;
    }
    
    if (aav_gVbo_circle_position)
    {
        glDeleteBuffers(1, &aav_gVbo_circle_position);
        aav_gVbo_circle_position = 0;
    }
    
    if (aav_aVbo_circle_color)
    {
        glDeleteBuffers(1, &aav_aVbo_circle_color);
        aav_aVbo_circle_color = 0;
    }
    
    if (aav_gVao_circle)
    {
        glDeleteVertexArrays(1, &aav_gVao_circle);
        aav_gVao_circle = 0;
    }
    
    if (aav_gVbo_circleInner_position)
    {
        glDeleteBuffers(1, &aav_gVbo_circleInner_position);
        aav_gVbo_circleInner_position = 0;
    }
    
    if (aav_aVbo_circleInner_color)
    {
        glDeleteBuffers(1, &aav_aVbo_circleInner_color);
        aav_aVbo_circleInner_color = 0;
    }
    
    
    
    
    glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glDeleteShader(aav_gVertexShaderObject);
    aav_gVertexShaderObject = 0;
    glDeleteShader(aav_gFragmentShaderObject);
    aav_gFragmentShaderObject = 0;
    
    glDeleteProgram(aav_gShaderProgramObject);
    aav_gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    
    [super dealloc];
}
@end


//Global Function
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp * outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut,void *displayLinkContext)
{
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return result;
}
