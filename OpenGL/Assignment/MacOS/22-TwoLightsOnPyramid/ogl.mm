#import <Foundation/Foundation.h>
#import <cocoa/cocoa.h>             //analogous to windows.h xlib.h

#import <QuartzCore/CVDisplayLink.h> // Audio and Display , Core Vidio
#import <OpenGL/gl3.h>             //gl.h
#import"vmath.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};

//ProtoType
//CallBackFunction
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp * , CVOptionFlags, CVOptionFlags *,void *);

FILE *aav_gpFile = NULL;

GLfloat aav_anglePyramid = 0.0f;

bool aav_bAnimationflag = false;
bool aav_bLight = false;

struct Light
{
    vec4 lightAmbiant;
    vec4 lightDefuse;
    vec4 lightSpecular;
    vec4 lightPosition;
};



struct Matrial
{
    vec4 materialAmbiant;
    vec4 materialDiffuse;
    vec4 materialSpecular;
    float materialShininess;
};

struct Light light[2] = {{ vec4(1.0f,0.0f,0.0f,1.0f),vec4(1.0f,0.0f,0.0f,1.0f),vec4(1.0f,0.0f,0.0f,1.0f),vec4(-2.0f,0.0f,-0.0f,1.0f)},
    {vec4(0.0f,0.0f,1.0f,1.0f),vec4(0.0f,0.0f,1.0f,1.0f),vec4(0.0f,0.0f,1.0f,1.0f),vec4(2.0f,0.0f,-0.0f,1.0f)}};


struct Matrial material = { vec4(0.0f,0.0f,0.0f,1.0f),vec4(1.0f,1.0f,1.0f,1.0f),vec4(1.0f,1.0f,1.0f,1.0f),128.0f};

@interface appDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>    //NeStep
//forwardDeclaration  extens  and implement
 @end


//main function
int main(int argc, char *argv[])
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    //Baap Object , magaicha , maza hinstace de
    //InBuilt Global object , jo NSApplication cha variable ahe
    NSApp = [NSApplication sharedApplication]; //asnara re NSapplication la
    
    [NSApp setDelegate:[[appDelegate alloc]init]];                          //^
    
    //Event loop , Message Loop
    [NSApp run]; // This is Called as Run Loop
    
    [pool release]; //1. program samtoi, all the memeory is reclamed
    
    return 0;
}

@interface MyOpenGLView: NSOpenGLView

@end

//implementation
@implementation appDelegate
{
    //class variables
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView;
}

//instance method
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; //ha path return hoto /User/UserName/Desktop/RTR/log/Window.app
    
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt",parentDirPath];
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    
    aav_gpFile = fopen(pszLogFileNameWithPath,"w");
    if(aav_gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
    }
    
    fprintf(aav_gpFile,"Program Started SuccessFully \n");
    
    
    //NSPoints Struct(x,y), NSSize Structure(width, height)  CG Point , CG Size
    NSRect win_rect = NSMakeRect(0.0,0.0,800.0,600.0); //internal CG Rect :Code Graphics as this "c" library so this is calling is C style
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                    NSWindowStyleMaskClosable |
                                                    NSWindowStyleMaskMiniaturizable |
                                                    NSWindowStyleMaskResizable
                                          backing: NSBackingStoreBuffered
                                          defer:NO];
    //styleMask is OVERLAPPEDWINDOW
    //NSWindowStyleMaskTitled is caption
    //NSWindowStyleMaskMiniaturizable is minimize
    //NSWindowStyleMaskResizable is resize
    //backing : retain karu nako , buffer madhe thav
    //Defer : NO , maje lagech dakhav , NO ha Objective C madhla Bool
    
    [window setTitle: @"AAV:OpenGL Two Lights On Pyramid "]; //create window cha Second Parameter
    [window center]; //centering the window
    
    //view Creation
    myOpenGLView = [[MyOpenGLView alloc] initWithFrame:win_rect]; //NSView madhle methos
    
    [window setContentView: myOpenGLView]; // maza contect view set kar
    [window setDelegate: self]; // objective C mathla self ha c++ this
    [window makeKeyAndOrderFront:self]; //mazaya varti focuse kar , set focuse , get foucuse , set Forground window, z order la sartavar pudhe an
    
 }

//Apan Shevti Ithe Yato
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code
    
    
    if(aav_gpFile)
    {
        fprintf(aav_gpFile,"Program Ended SuccessFully \n");
        fclose(aav_gpFile);
        aav_gpFile = NULL;
    }
    
}

//NSWindow deligate  che window
-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
    
}

-(void)dealloc
{
    //code
    [myOpenGLView release];
    [window release];
    [super dealloc]; //mazya super la dealloc kar
}
@end


//MyView Implementation
@implementation MyOpenGLView
{
    //code
    @private
    CVDisplayLinkRef displayLink;
    
    //shader
    GLuint aav_gVertexShaderObject;
    GLuint aav_gFragmentShaderObject;
    GLuint aav_gShaderProgramObject;
    
    GLuint aav_Vao_pyramid;
    GLuint aav_Vbo_pyramid_position;
    GLuint aav_Vbo_pyramid_normals;
    
    //matrix
    mat4 aav_PerspectiveProjectionMatrix;
    
    
    //uniforms
    GLuint aav_viewMatrixUniform;
    GLuint aav_modelMatrixUniform;
    GLuint aav_ProjectionMatrixUniform;
    
    GLuint aav_laUniform[2];
    GLuint aav_ldUniform[2];
    GLuint aav_lsUniform[2];
    GLuint aav_lightPositionUniform[2];
    
    GLuint aav_kaUniform;
    GLuint aav_kdUniform;
    GLuint aav_ksUniform;
    
    GLuint aav_kShininessUniform;
    
    GLuint aav_lKeyPressedUniform;
    
  
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        //Pixel Format Attribute
        NSOpenGLPixelFormatAttribute attributes[] =
        {
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };
        
        NSOpenGLPixelFormat * pixelFormal = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
        if(pixelFormal == nil)
        {
            fprintf(aav_gpFile,"Can Not Get pixelFormat Attribute");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormal shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormal];
        [self setOpenGLContext:glContext];
        
    }
    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)outputType
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init]; //New Thread is Created.
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

//initialize prepare OpenGL
-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];
    
    [[self openGLContext]makeCurrentContext];
    
    //swap Interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    
    //**VERTEX SHADER***
    aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* aav_vertexShaserSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_lKeyPressed;" \
    "uniform vec3 u_la[2];" \
    "uniform vec3 u_ld[2];" \
    "uniform vec3 u_ls[2];" \
    "uniform vec4 u_lightPosistion[2];" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_kShineness;"\
    
    "vec3 lightDirection[2];" \
    "vec3 reflection_vector[2];" \
    "vec3 ambiant[2];" \
    "vec3 diffuse[2];" \
    "vec3 specular[2];" \
    
    "out vec3 fong_ads_light;" \
    "void main(void)" \
    "{" \
    "    if(u_lKeyPressed == 1)" \
    "    {"\
    "        vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition; " \
    "        vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal); " \
    "        vec3 view_vector = normalize(-eyeCordinate.xyz);" \
    "        for(int i = 0 ; i < 2 ; i++)"\
    "        {"\
    "            lightDirection[i] = normalize(vec3(u_lightPosistion[i] - eyeCordinate));" \
    "            reflection_vector[i] = reflect(-lightDirection[i],tranformed_normal);" \
    "            ambiant[i] = u_la[i] * u_ka;" \
    "            diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i],tranformed_normal),0.0f);" \
    "            specular[i] = u_ls[i] * u_ks * pow(max(dot(reflection_vector[i],view_vector),0.0f),u_kShineness);" \
    "            fong_ads_light = fong_ads_light + ambiant[i] + diffuse[i] + specular[i];" \
    "        }"\
    "    }" \
    "    else" \
    "    {" \
    "        fong_ads_light = vec3(1.0f,1.0f,1.0f);" \
    "    }" \
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";
    glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(aav_gVertexShaderObject);
    
    GLint aav_infoLogLength = 0;
    GLint aav_shaderCompiledStatus = 0;
    char* aav_szBuffer = NULL;
    
    glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
                                   &aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**FRAGMENT SHADER**
    aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec3 fong_ads_light;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "    FragColor = vec4(fong_ads_light,1.0f);" \
    "}";
    glShaderSource(aav_gFragmentShaderObject, 1,
                   (const GLchar**)&fragmentShaderSourceCode,NULL);
    //compile shader
    glCompileShader(aav_gFragmentShaderObject);
    
    
    aav_szBuffer = NULL;
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    
    glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS,
                  &aav_shaderCompiledStatus);
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
                                   &aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**SHADER PROGRAM**
    //Create
    aav_gShaderProgramObject = glCreateProgram();
    
    glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");
    
    glLinkProgram(aav_gShaderProgramObject);
    
    aav_infoLogLength = 0;
    GLint aav_shaderProgramLinkStatus = 0;
    aav_szBuffer = NULL;
    
    glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
    if (aav_shaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                GLsizei aav_aav_written;
                glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
                                    &aav_aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Shader Program Link Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //Post Linking Information
    aav_modelMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_model_matrix");
    aav_viewMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_view_matrix");
    aav_ProjectionMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_projection_matrix");
    
    aav_laUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_la[0]");
    aav_ldUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_ld[0]");
    aav_lsUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_ls[0]");
    aav_lightPositionUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_lightPosistion[0]");
    
    aav_laUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_la[1]");
    aav_ldUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_ld[1]");
    aav_lsUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_ls[1]");
    aav_lightPositionUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_lightPosistion[1]");
    
    aav_kaUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ka");
    aav_kdUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kd");
    aav_ksUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ks");
    
    aav_kShininessUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kShineness");
    
    aav_lKeyPressedUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_lKeyPressed");
    
    //vertices array declation
    const GLfloat aav_pyramidVertex[] =
    {
        0.0f,1.0f,0.0f,
        -1.0f,-1.0f,1.0f,
        1.0f,-1.0f,1.0f,
        
        0.0f,1.0f,0.0f,
        1.0f,-1.0,1.0f,
        1.0f,-1.0f,-1.0f,
        
        0.0f,1.0f,0.0f,
        -1.0f,-1.0f,-1.0f,
        1.0f,-1.0f,-1.0f,
        
        0.0f,1.0f,0.0f,
        -1.0f,-1.0f,1.0f,
        -1.0f,-1.0f,-1.0f
    };
    
    const GLfloat aav_pyramidNormals[] =
    {
        0.0f,0.447214f,0.894427f,
        0.0f, 0.447214f, 0.894427f,
        0.0f, 0.447214f, 0.894427f,
        
        0.894427f,0.447214f,0.0f,
        0.894427f, 0.447214f, 0.0f,
        0.894427f, 0.447214f, 0.0f,
        
        0.0f,0.447214f, -0.894427f,
        0.0f, 0.447214f, -0.894427f,
        0.0f, 0.447214f, -0.894427f,
        
        -0.894427f, 0.447214f, 0.0f,
        -0.894427f, 0.447214f, 0.0f,
        -0.894427f,0.447214f,0.0f
    };
    
    
    //Record Square
    glGenVertexArrays(1,&aav_Vao_pyramid);
    glBindVertexArray(aav_Vao_pyramid);
    
    glGenBuffers(1,&aav_Vbo_pyramid_position);
    glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_pyramid_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pyramidVertex), aav_pyramidVertex,GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &aav_Vbo_pyramid_normals);
    glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_pyramid_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pyramidNormals), aav_pyramidNormals, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Record Off / Pause
    glBindVertexArray(0);
   
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
   
    //
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    
    aav_PerspectiveProjectionMatrix = mat4::identity();
    
    //create displayLink
    //CallBackSet karne
    //NSPixelFormat to Core
    //CGL
    //DisplayLinkStart
    
    //CV and CGL related Code
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDispalyLinkCallBack,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPiexelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPiexelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    //code
    [super reshape];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    if(rect.size.height < 0)
        rect.size.height = 1;
    glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);
    
   aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)rect.size.width / (GLfloat)rect.size.height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//invalidateRect
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    mat4 aav_scaMatrix;
    mat4 aav_translateMatrix;
    mat4 aav_rotationMatrixA;
    mat4 aav_rotationMatrixY;
    
    vec4 aav_lightPosition;
    aav_lightPosition = vec4( 0.0f,0.0f,2.0f,1.0f );
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //triangle
    glUseProgram(aav_gShaderProgramObject);
    
    if (aav_bLight == true)
    {
        //Light Enable
        glUniform1i(aav_lKeyPressedUniform, 1);
        //Light-1
        glUniform3fv(aav_laUniform[0], 1, light[0].lightAmbiant);                // la
        glUniform3fv(aav_ldUniform[0], 1, light[0].lightDefuse);                // ld
        glUniform3fv(aav_lsUniform[0], 1, light[0].lightSpecular);                // ls
        glUniform4fv(aav_lightPositionUniform[0], 1,light[0].lightPosition);    //lightPosition
        
        //Light-2
        glUniform3fv(aav_laUniform[1], 1, light[1].lightAmbiant);                // la
        glUniform3fv(aav_ldUniform[1], 1, light[1].lightDefuse);                // ld
        glUniform3fv(aav_lsUniform[1], 1, light[1].lightSpecular);                // ls
        glUniform4fv(aav_lightPositionUniform[1], 1, light[1].lightPosition);    //lightPosition
        
        //material
        glUniform3fv(aav_kaUniform, 1, material.materialAmbiant);    // ka
        glUniform3fv(aav_kdUniform, 1, material.materialDiffuse);    // kd
        glUniform3fv(aav_ksUniform, 1, material.materialSpecular);    //ks
        glUniform1f(aav_kShininessUniform,material.materialShininess);
    }
    else
    {
        glUniform1i(aav_lKeyPressedUniform, 0);
    }
    
    
    //OpenGL Drawing
    mat4 aav_modelMateix = mat4::identity();
    mat4 aav_viewMatrix = mat4::identity();
    
    aav_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
    
    aav_rotationMatrixY = vmath::rotate(aav_anglePyramid, 0.0f, 1.0f, 0.0f);
    
    aav_rotationMatrixA =  aav_rotationMatrixY ;
    
    aav_modelMateix = aav_translateMatrix *aav_rotationMatrixA;
    
    glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
    glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
    glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
    
    
    glBindVertexArray(aav_Vao_pyramid);
    glDrawArrays(GL_TRIANGLES, 0, 12);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    if (aav_bAnimationflag == TRUE)
    {
        aav_anglePyramid = aav_anglePyramid + 1.00f;
        if(aav_anglePyramid >= 360.0f)
            aav_anglePyramid = 0.0f;
    }
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//NSResponder methos
-(BOOL)acceptsFirstResponder
{
    //code
    //[[self window] makeFirstResponder ];
    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
             
            break;
        case 'F':
        case 'f':
        [[self window] toggleFullScreen:self]; //Full Screen
            break;
        case 'a':
        case 'A':
            if (aav_bAnimationflag == false)
            {
                aav_bAnimationflag = true;
            }
            else
            {
                aav_bAnimationflag = false;
            }
            break;
        case 'l':
        case 'L':
            if (aav_bLight == false)
            {
                aav_bLight = true;
            }
            else
            {
                aav_bLight = false;
            }
            break;
        default:
            break;
        
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
 
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)dealloc
{
    //code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    if (aav_Vao_pyramid)
    {
        glDeleteVertexArrays(1, &aav_Vao_pyramid);
        aav_Vao_pyramid = 0;
    }
    
    if (aav_Vbo_pyramid_position)
    {
        glDeleteBuffers(1, &aav_Vbo_pyramid_position);
        aav_Vbo_pyramid_position = 0;
    }
    
    if (aav_Vbo_pyramid_normals)
    {
        glDeleteBuffers(1, &aav_Vbo_pyramid_normals);
        aav_Vbo_pyramid_normals = 0;
    }
    
    
    glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glDeleteShader(aav_gVertexShaderObject);
    aav_gVertexShaderObject = 0;
    glDeleteShader(aav_gFragmentShaderObject);
    aav_gFragmentShaderObject = 0;
    
    glDeleteProgram(aav_gShaderProgramObject);
    aav_gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    [super dealloc];
}
@end


//Global Function
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp * outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut,void *displayLinkContext)
{
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return result;
}
