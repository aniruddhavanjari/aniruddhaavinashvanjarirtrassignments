#import <Foundation/Foundation.h>
#import <cocoa/cocoa.h>             //analogous to windows.h xlib.h

#import <QuartzCore/CVDisplayLink.h> // Audio and Display , Core Vidio
#import <OpenGL/gl3.h>             //gl.h
#import"vmath.h"
#import"sphere.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};
//ProtoType
//CallBackFunction
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp * , CVOptionFlags, CVOptionFlags *,void *);

FILE *aav_gpFile = NULL;

GLfloat aav_anglePyramid = 0.0f;
GLfloat aav_angleCube = 0.0f;

bool aav_bLight = false;

//Light Array
GLfloat aav_lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };    //la
GLfloat aav_lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };    // ld
GLfloat aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };    //ls

GLfloat aav_lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

//material Array
GLfloat aav_materialAmbiant[]    = { 0.0f, 0.0f, 0.0f ,0.0f}; // ka
GLfloat aav_materialDiffuse[]    = {1.0f,1.0f,1.0f,1.0f};    // kd
GLfloat aav_materialSpecular[]    = {1.0f,1.0f,1.0f,1.0f};    // ks

GLfloat aav_materialShininess = 128.0f; // material shininess

@interface appDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>    //NeStep
//forwardDeclaration  extens  and implement
 @end


//main function
int main(int argc, char *argv[])
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    //Baap Object , magaicha , maza hinstace de
    //InBuilt Global object , jo NSApplication cha variable ahe
    NSApp = [NSApplication sharedApplication]; //asnara re NSapplication la
    
    [NSApp setDelegate:[[appDelegate alloc]init]];                          //^
    
    //Event loop , Message Loop
    [NSApp run]; // This is Called as Run Loop
    
    [pool release]; //1. program samtoi, all the memeory is reclamed
    
    return 0;
}

@interface MyOpenGLView: NSOpenGLView

@end

//implementation
@implementation appDelegate
{
    //class variables
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView;
}

//instance method
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; //ha path return hoto /User/UserName/Desktop/RTR/log/Window.app
    
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt",parentDirPath];
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    
    aav_gpFile = fopen(pszLogFileNameWithPath,"w");
    if(aav_gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
    }
    
    fprintf(aav_gpFile,"Program Started SuccessFully \n");
    
    
    //NSPoints Struct(x,y), NSSize Structure(width, height)  CG Point , CG Size
    NSRect win_rect = NSMakeRect(0.0,0.0,800.0,600.0); //internal CG Rect :Code Graphics as this "c" library so this is calling is C style
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                    NSWindowStyleMaskClosable |
                                                    NSWindowStyleMaskMiniaturizable |
                                                    NSWindowStyleMaskResizable
                                          backing: NSBackingStoreBuffered
                                          defer:NO];
    //styleMask is OVERLAPPEDWINDOW
    //NSWindowStyleMaskTitled is caption
    //NSWindowStyleMaskMiniaturizable is minimize
    //NSWindowStyleMaskResizable is resize
    //backing : retain karu nako , buffer madhe thav
    //Defer : NO , maje lagech dakhav , NO ha Objective C madhla Bool
    
    [window setTitle: @"AAV:OpenGL PerVertexLighting "]; //create window cha Second Parameter
    [window center]; //centering the window
    
    //view Creation
    myOpenGLView = [[MyOpenGLView alloc] initWithFrame:win_rect]; //NSView madhle methos
    
    [window setContentView: myOpenGLView]; // maza contect view set kar
    [window setDelegate: self]; // objective C mathla self ha c++ this
    [window makeKeyAndOrderFront:self]; //mazaya varti focuse kar , set focuse , get foucuse , set Forground window, z order la sartavar pudhe an
    
 }

//Apan Shevti Ithe Yato
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code
    
    
    if(aav_gpFile)
    {
        fprintf(aav_gpFile,"Program Ended SuccessFully \n");
        fclose(aav_gpFile);
        aav_gpFile = NULL;
    }
    
}

//NSWindow deligate  che window
-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
    
}

-(void)dealloc
{
    //code
    [myOpenGLView release];
    [window release];
    [super dealloc]; //mazya super la dealloc kar
}
@end


//MyView Implementation
@implementation MyOpenGLView
{
    //code
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    GLuint aav_mvp_MatrixUniform;
    
    mat4 aav_PerspectiveProjectionMatrix;
    
    //Sphere Variable
    GLfloat aav_sphere_vertices[1146];
    GLfloat aav_sphere_normals[1146];
    GLfloat aav_sphere_texture[764];
    short aav_sphere_elements[2280];
    GLuint aav_numSphereVertices;
    GLuint aav_numSphereElements;
    
    GLuint aav_gVao_sphere;
    GLuint aav_Vbo_sphere_position;
    GLuint aav_Vbo_sphere_normals;
    GLuint aav_Vbo_sphere_elements;
    
   
    
    
    GLuint aav_viewMatrixUniform;
    GLuint aav_modelMatrixUniform;
    GLuint aav_ProjectionMatrixUniform;
    
    GLuint aav_laUniform;
    GLuint aav_ldUniform;
    GLuint aav_lsUniform;
    GLuint aav_lightPositionUniform;
    
    GLuint aav_kaUniform;
    GLuint aav_kdUniform;
    GLuint aav_ksUniform;
    
    GLuint aav_kShininessUniform;
    
    GLuint aav_lKeyPressedUniform;
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        //Pixel Format Attribute
        NSOpenGLPixelFormatAttribute attributes[] =
        {
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };
        
        NSOpenGLPixelFormat * pixelFormal = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
        if(pixelFormal == nil)
        {
            fprintf(aav_gpFile,"Can Not Get pixelFormat Attribute");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormal shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormal];
        [self setOpenGLContext:glContext];
        
    }
    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)outputType
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init]; //New Thread is Created.
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

//initialize prepare OpenGL
-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];
    
    [[self openGLContext]makeCurrentContext];
    
    //swap Interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
   
    //**VERTEX SHADER***
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* aav_vertexShaserSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec3 vNormal;" \
    "uniform mat4 u_view_matrix;" \
    "uniform mat4 u_model_matrix;" \
    "uniform mat4 u_projection_matrix;" \
    "uniform int u_lKeyPressed;" \
    "uniform vec3 u_la;" \
    "uniform vec3 u_ld;" \
    "uniform vec3 u_ls;" \
    "uniform vec4 u_lightPosistion;" \
    "uniform vec3 u_ka;" \
    "uniform vec3 u_kd;" \
    "uniform vec3 u_ks;" \
    "uniform float u_kShineness;"\
    "out vec3 fong_ads_light;" \
    "void main(void)" \
    "{" \
    "    if(u_lKeyPressed == 1)" \
    "    {"\
    "        vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
    "        vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
    "        vec3 lightDirection = normalize(vec3(u_lightPosistion - eyeCordinate));" \
    "        vec3 reflection_vector = reflect(-lightDirection,tranformed_normal);" \
    "        vec3 view_vector = normalize(-eyeCordinate.xyz);" \
    "        vec3 ambiant = u_la * u_ka;" \
    "        vec3 diffuse = u_ld * u_kd * max(dot(lightDirection,tranformed_normal),0.0f);" \
    "        vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector,view_vector),0.0f),u_kShineness);" \
    "        fong_ads_light = ambiant + diffuse + specular;" \
    "    }" \
    "    else" \
    "    {" \
    "        fong_ads_light = vec3(1.0f,1.0f,1.0f);" \
    "    }" \
    "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
    "}";
    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(gVertexShaderObject);
    
    GLint aav_infoLogLength = 0;
    GLint aav_shaderCompiledStatus = 0;
    char* szBuffer = NULL;
    
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(gVertexShaderObject, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**FRAGMENT SHADER**
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec3 fong_ads_light;" \
    "out vec4 FragColor;" \
    "void main(void)" \
    "{" \
    "    FragColor = vec4(fong_ads_light,1.0f);" \
    "}";
    glShaderSource(gFragmentShaderObject, 1,
                   (const GLchar**)&fragmentShaderSourceCode,NULL);
    //compile shader
    glCompileShader(gFragmentShaderObject);
    
    
    szBuffer = NULL;
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS,
                  &aav_shaderCompiledStatus);
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(gFragmentShaderObject, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**SHADER PROGRAM**
    //Create
    gShaderProgramObject = glCreateProgram();
    
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");
    
    glLinkProgram(gShaderProgramObject);
    
    aav_infoLogLength = 0;
    GLint aav_shaderProgramLinkStatus = 0;
    szBuffer = NULL;
    
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
    if (aav_shaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                GLsizei aav_aav_written;
                glGetProgramInfoLog(gShaderProgramObject, aav_infoLogLength,
                                    &aav_aav_written, szBuffer);
                fprintf(aav_gpFile, "Shader Program Link Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //Post Linking Information
    
    //aav_mvp_MatrixUniform =  glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");
    
    /**************************************/
    aav_modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
    aav_viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
    aav_ProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");
    
    aav_laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
    aav_ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
    aav_lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
    aav_lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosistion");
    
    aav_kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
    aav_kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
    aav_ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
    
    aav_kShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_kShineness");
    
    aav_lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
    /**************************************/
    
    //vertices array declation
    
    Sphere *sphere = [[Sphere alloc]init];
    
    [sphere getSphereVertexData:aav_sphere_vertices :aav_sphere_normals :aav_sphere_texture :aav_sphere_elements];
    
    aav_numSphereVertices = [sphere getNumberOfSphereVertices];
    aav_numSphereElements = [sphere getNumberOfSphereElements];
    
    glGenVertexArrays(1, &aav_gVao_sphere);
    glBindVertexArray(aav_gVao_sphere);
    
    //Record Sphere
    glGenBuffers(1, &aav_Vbo_sphere_position);
    glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_position);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_vertices), aav_sphere_vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //normals
    glGenBuffers(1, &aav_Vbo_sphere_normals);
    glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_normals);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_normals), aav_sphere_normals, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //elements
    glGenBuffers(1, &aav_Vbo_sphere_elements);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements); // Elemtnes Drawing is Also Called As Index Drawing, Elements Drawing
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(aav_sphere_elements), aav_sphere_elements, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    //Record Off / Pause
    glBindVertexArray(0);
    
   
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
   
    //
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    
    aav_PerspectiveProjectionMatrix = mat4::identity();
    
    //create displayLink
    //CallBackSet karne
    //NSPixelFormat to Core
    //CGL
    //DisplayLinkStart
    
    //CV and CGL related Code
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDispalyLinkCallBack,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPiexelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPiexelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    //code
    [super reshape];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    if(rect.size.height < 0)
        rect.size.height = 1;
    glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);
    
   aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)rect.size.width / (GLfloat)rect.size.height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//invalidateRect
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(gShaderProgramObject);
    
    
    if (aav_bLight == true)
    {
        glUniform1i(aav_lKeyPressedUniform,1);
        //Light
        glUniform3fv(aav_laUniform,1,(GLfloat *)aav_lightAmbiant);                // la
        glUniform3fv(aav_ldUniform,1,(GLfloat *)aav_lightDiffuse);                // ld
        glUniform3fv(aav_lsUniform,1,(GLfloat *)aav_lightSpecular);                // ls
        glUniform4fv(aav_lightPositionUniform,1,(GLfloat *)aav_lightPosition);    //lightPosition
        
        //material
        glUniform3fv(aav_kaUniform,1,(GLfloat *)aav_materialAmbiant);    // ka
        glUniform3fv(aav_kdUniform,1,(GLfloat *)aav_materialDiffuse);    // kd
        glUniform3fv(aav_ksUniform,1, (GLfloat *)aav_materialSpecular);    //ks
        glUniform1f(aav_kShininessUniform,128.0f);
    }
    else
    {
        glUniform1i(aav_lKeyPressedUniform, 0);
    }
    
    //OpenGL Drawing
    mat4 aav_modelMateix = mat4::identity();
    mat4 aav_viewMatrix = mat4::identity();
    
    mat4 aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
    aav_modelMateix = aav_translateMatrix ;
    
    glUniformMatrix4fv(aav_modelMatrixUniform,1,GL_FALSE, aav_modelMateix);
    glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
    glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
    
    glBindVertexArray(aav_gVao_sphere);
    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
    glDrawElements(GL_TRIANGLES,aav_numSphereElements,GL_UNSIGNED_SHORT,0);
    
    glBindVertexArray(0);
    
    
    glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//NSResponder methos
-(BOOL)acceptsFirstResponder
{
    //code
    //[[self window] makeFirstResponder ];
    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
             
            break;
        case 'F':
        case 'f':
        [[self window] toggleFullScreen:self]; //Full Screen
            break;
        case 'l':
        case 'L':
            if (aav_bLight == false)
            {
                aav_bLight = true;
            }
            else
            {
                aav_bLight = false;
            }
            break;
        default:
            break;
        
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
 
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)dealloc
{
    //code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    if (aav_gVao_sphere)
    {
        glDeleteVertexArrays(1, &aav_gVao_sphere);
        aav_gVao_sphere = 0;
    }
    
    if (aav_Vbo_sphere_position)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_position);
        aav_Vbo_sphere_position = 0;
    }
    
    if (aav_Vbo_sphere_normals)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_normals);
        aav_Vbo_sphere_normals = 0;
    }
    
    if (aav_Vbo_sphere_elements)
    {
        glDeleteBuffers(1, &aav_Vbo_sphere_elements);
        aav_Vbo_sphere_elements = 0;
    }
    
    
    
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;
    
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    [super dealloc];
}
@end


//Global Function
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp * outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut,void *displayLinkContext)
{
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return result;
}
