#import <Foundation/Foundation.h>
#import <cocoa/cocoa.h>             //analogous to windows.h xlib.h

#import <QuartzCore/CVDisplayLink.h> // Audio and Display , Core Vidio
#import <OpenGL/gl3.h>             //gl.h
#import"vmath.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_TEXCOORD,
    AAV_ATTRIBUTE_NORMAL,
};

//ProtoType
//CallBackFunction
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp * , CVOptionFlags, CVOptionFlags *,void *);

FILE *aav_gpFile = NULL;

//Rotation Angle
GLfloat aav_anglePyramid = 0.0f;
GLfloat aav_angleCube = 0.0f;


@interface appDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>    //NeStep
//forwardDeclaration  extens  and implement
 @end


//main function
int main(int argc, char *argv[])
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    //Baap Object , magaicha , maza hinstace de
    //InBuilt Global object , jo NSApplication cha variable ahe
    NSApp = [NSApplication sharedApplication]; //asnara re NSapplication la
    
    [NSApp setDelegate:[[appDelegate alloc]init]];                          //^
    
    //Event loop , Message Loop
    [NSApp run]; // This is Called as Run Loop
    
    [pool release]; //1. program samtoi, all the memeory is reclamed
    
    return 0;
}

@interface MyOpenGLView: NSOpenGLView

@end

//implementation
@implementation appDelegate
{
    //class variables
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView;
}

//instance method
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; //ha path return hoto /User/UserName/Desktop/RTR/log/Window.app
    
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt",parentDirPath];
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    
    aav_gpFile = fopen(pszLogFileNameWithPath,"w");
    if(aav_gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
    }
    
    fprintf(aav_gpFile,"Program Started SuccessFully \n");
    
    
    //NSPoints Struct(x,y), NSSize Structure(width, height)  CG Point , CG Size
    NSRect win_rect = NSMakeRect(0.0,0.0,800.0,600.0); //internal CG Rect :Code Graphics as this "c" library so this is calling is C style
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                    NSWindowStyleMaskClosable |
                                                    NSWindowStyleMaskMiniaturizable |
                                                    NSWindowStyleMaskResizable
                                          backing: NSBackingStoreBuffered
                                          defer:NO];
    //styleMask is OVERLAPPEDWINDOW
    //NSWindowStyleMaskTitled is caption
    //NSWindowStyleMaskMiniaturizable is minimize
    //NSWindowStyleMaskResizable is resize
    //backing : retain karu nako , buffer madhe thav
    //Defer : NO , maje lagech dakhav , NO ha Objective C madhla Bool
    
    [window setTitle: @"AAV:OpenGL 3D Shapes Texture "]; //create window cha Second Parameter
    [window center]; //centering the window
    
    //view Creation
    myOpenGLView = [[MyOpenGLView alloc] initWithFrame:win_rect]; //NSView madhle methos
    
    [window setContentView: myOpenGLView]; // maza contect view set kar
    [window setDelegate: self]; // objective C mathla self ha c++ this
    [window makeKeyAndOrderFront:self]; //mazaya varti focuse kar , set focuse , get foucuse , set Forground window, z order la sartavar pudhe an
    
 }

//Apan Shevti Ithe Yato
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code
    if(aav_gpFile)
    {
        fprintf(aav_gpFile,"Program Ended SuccessFully \n");
        fclose(aav_gpFile);
        aav_gpFile = NULL;
    }
    
}

//NSWindow deligate  che window
-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
    
}

-(void)dealloc
{
    //code
    [myOpenGLView release];
    [window release];
    [super dealloc]; //mazya super la dealloc kar
}
@end


//MyView Implementation
@implementation MyOpenGLView
{
    //code
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint aav_gVertexShaderObject;
    GLuint aav_gFragmentShaderObject;
    GLuint aav_gShaderProgramObject;
    
    GLuint aav_gVao_pyramid;
    GLuint aav_gVbo_position_pyramid;
    GLuint aav_gVbo_texture_pyramid;
    
    GLuint aav_gVao_cube;
    GLuint aav_gVbo_position_cube;
    GLuint aav_gVbo_texture_cube;
    
    GLuint aav_mvp_MatrixUniform;
    GLuint aav_textureSamplerUniform;
    
    //Texture
    GLuint aav_stone_texture;
    GLuint aav_kundali_texture;
    
    mat4 aav_PerspectiveProjectionMatrix;
    
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        //Pixel Format Attribute
        NSOpenGLPixelFormatAttribute attributes[] =
        {
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };
        
        NSOpenGLPixelFormat * pixelFormal = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
        if(pixelFormal == nil)
        {
            fprintf(aav_gpFile,"Can Not Get pixelFormat Attribute");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormal shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormal];
        [self setOpenGLContext:glContext];
        
    }
    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)outputType
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init]; //New Thread is Created.
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

//initialize prepare OpenGL
-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];
    
    [[self openGLContext]makeCurrentContext];
    
    //swap Interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
    
    //**VERTEX SHADER***
    aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* aav_vertexShaserSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec4 vPosition;" \
    "in vec2 vTexCoord;" \
    "uniform mat4 aav_u_mvpMatrix;" \
    "out vec2 aav_out_TexCoord;" \
    "void main(void)" \
    "{" \
    "gl_Position = aav_u_mvpMatrix * vPosition;" \
    "aav_out_TexCoord = vTexCoord;" \
    "}";
    glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(aav_gVertexShaderObject);
    
    GLint aav_infoLogLength = 0;
    GLint aav_shaderCompiledStatus = 0;
    char* aav_szBuffer = NULL;
    
    glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
                                   &aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**FRAGMENT SHADER**
    aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec2 aav_out_TexCoord;" \
    "uniform sampler2D u_texture_sampler;" \
    "out vec4 aav_FragColor;" \
    "void main(void)" \
    "{" \
    "aav_FragColor = texture(u_texture_sampler,aav_out_TexCoord);" \
    "}";
    glShaderSource(aav_gFragmentShaderObject, 1,
                   (const GLchar**)&fragmentShaderSourceCode,NULL);
    //compile shader
    glCompileShader(aav_gFragmentShaderObject);
    
    
    aav_szBuffer = NULL;
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    
    glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS,
                  &aav_shaderCompiledStatus);
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
                                   &aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**SHADER PROGRAM**
    //Create
    aav_gShaderProgramObject = glCreateProgram();
    
    glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
    
    glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_TEXCOORD, "vTexCoord");
    
    glLinkProgram(aav_gShaderProgramObject);
    
    aav_infoLogLength = 0;
    GLint aav_shaderProgramLinkStatus = 0;
    aav_szBuffer = NULL;
    
    glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
    if (aav_shaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            aav_szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                GLsizei aav_aav_written;
                glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
                                    &aav_aav_written, aav_szBuffer);
                fprintf(aav_gpFile, "Shader Program Link Log: %s\n", aav_szBuffer);
                free(aav_szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //Post Linking Information
    aav_mvp_MatrixUniform         = glGetUniformLocation(aav_gShaderProgramObject, "aav_u_mvpMatrix");
    
    aav_textureSamplerUniform     = glGetUniformLocation(aav_gShaderProgramObject, "u_texture_sampler");
    
    
    const GLfloat aav_pyramidVertices[] =
    {
        0.0f,0.5f,0.0f,
        -0.5f,-0.5f,0.5f,
        0.5f,-0.5f,0.5f,
        
        0.0f,0.5f,0.0f,
        0.5f,-0.5,0.5f,
        0.5f,-0.5f,-0.5f,
        
        0.0f,0.5f,0.0f,
        -0.5f,-0.5f,-0.5f,
        0.5f,-0.5f,-0.5f,
        
        0.0f,0.5f,0.0f,
        -0.5f,-0.5f,0.5f,
        -0.5f,-0.5f,-0.5f,
    };
    
    const GLfloat aav_pyramidTexCoord[] =
    {
        0.5f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,
        
        0.5f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,
        
        0.5f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,
        
        0.5f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f
    };
    
    const GLfloat aav_cubeVertex[] =
    {
        0.5f,0.5f,0.5f,
        -0.5f,0.5f,0.5f,
        -0.5f,-0.5f,0.5f,
        0.5f,-0.5f,0.5f,
        
        0.5f, 0.5f, -0.5f,
        0.5f, 0.5f, 0.5f,
        0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, -0.5f,
        
        0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        0.5f, -0.5f, -0.5f,
        
        -0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, 0.5f,
        -0.5f, -0.5f, 0.5f,
        -0.5f, -0.5f, -0.5f,
        
        0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, -0.5f,
        -0.5f, 0.5f, 0.5f,
        0.5f, 0.5f, 0.5f,
        
        0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, 0.5f,
        0.5f, -0.5f, 0.5f
    };
    
    const GLfloat aav_cubeTexCoord[] =
    {
        0.0f,0.0f,
        1.0f,0.0f,
        1.0f,1.0f,
        0.0f,1.0f,
        
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };
    
    glGenVertexArrays(1, &aav_gVao_pyramid);
    glBindVertexArray(aav_gVao_pyramid);
    
    //Record Triangle
    glGenBuffers(1, &aav_gVbo_position_pyramid);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position_pyramid);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pyramidVertices), aav_pyramidVertices, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &aav_gVbo_texture_pyramid);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_texture_pyramid);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pyramidTexCoord), aav_pyramidTexCoord, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_TEXCOORD, 2, GL_FLOAT,GL_FALSE,0,NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_TEXCOORD);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Record Off / Pause
    glBindVertexArray(0);
    
    //Record Square
    glGenVertexArrays(1,&aav_gVao_cube);
    glBindVertexArray(aav_gVao_cube);
    
    glGenBuffers(1,&aav_gVbo_position_cube);
    glBindBuffer(GL_ARRAY_BUFFER,aav_gVbo_position_cube);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeVertex), aav_cubeVertex,GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    glGenBuffers(1, &aav_gVbo_texture_cube);
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_texture_cube);
    glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeTexCoord), aav_cubeTexCoord, GL_STATIC_DRAW);
    glVertexAttribPointer(AAV_ATTRIBUTE_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(AAV_ATTRIBUTE_TEXCOORD);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    //Record Off / Pause
    glBindVertexArray(0);
   
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
   
    //
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    
    aav_stone_texture = [self loadTextureFromBmpFile:"Stone.bmp"];
    if(aav_stone_texture == 0)
    {
        [self release];
        [NSApp terminate:self];
    }
    aav_kundali_texture = [self loadTextureFromBmpFile:"Sir_Kundali.bmp"];
    if(aav_kundali_texture == 0)
    {
        [self release];
        [NSApp terminate:self];
    }
    aav_PerspectiveProjectionMatrix = mat4::identity();
    
    //create displayLink
    //CallBackSet karne
    //NSPixelFormat to Core
    //CGL
    //DisplayLinkStart
    
    //CV and CGL related Code
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDispalyLinkCallBack,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPiexelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPiexelFormat);
    CVDisplayLinkStart(displayLink);
}

-(GLuint)loadTextureFromBmpFile:(const char *)imageFileName
{
    //code
    
    GLuint aav_texture;
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; //ha path return hoto /User/UserName/Desktop/RTR/log/Window.app
    
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];
    
    NSString *imageFileNameWithPath = [NSString stringWithFormat:@"%@/%s",parentDirPath,imageFileName];
    
    NSImage *bmpImage = [[NSImage alloc]initWithContentsOfFile:imageFileNameWithPath];//Get Image Representation of our image file.
    
    if(!bmpImage)
    {
        fprintf(aav_gpFile, "NSImage File Conversion Fail\n");
        return 0;
    }
    // Now get CGImageRepresentation Of NSImage
    CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil context:nil hints:nil];
    //get Width and height of CGImage
    int imageWidth = (int)CGImageGetWidth(cgImage);
    int imageHeight = (int)CGImageGetHeight(cgImage);
    
    //Get CoreFoundation Representation of Image
    CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));
    
    //Convert CFDataRef to void * generic Data
    void *pixels = (void *)CFDataGetBytePtr(imageData); //pixel CFData cha pointer miltoi.
    
    //
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    
    glGenTextures(1, &aav_texture); // Gatu
    glBindTexture(GL_TEXTURE_2D, aav_texture);
    
    //Setting Texture Paramter
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    
    
    glTexImage2D(GL_TEXTURE_2D,0,
                 GL_RGBA,imageWidth,imageHeight,
                 0,GL_RGBA,GL_UNSIGNED_BYTE,pixels);
    glGenerateMipmap(GL_TEXTURE_2D);
    
    CFRelease(imageData);
    
    return aav_texture;
}

-(void)reshape
{
    //code
    [super reshape];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    if(rect.size.height < 0)
        rect.size.height = 1;
    glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);
    
   aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)rect.size.width / (GLfloat)rect.size.height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//invalidateRect
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    mat4 aav_scaMatrix;
    mat4 aav_translateMatrix;
    mat4 aav_rotationMatrixA;
    mat4 aav_rotationMatrixX;
    mat4 aav_rotationMatrixY;
    mat4 aav_rotationMatrixZ;
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    //triangle
    glUseProgram(aav_gShaderProgramObject);
    
    
    //OpenGL Drawing
    mat4 aav_modelViewMateix = mat4::identity();
    mat4 aav_modelViewProjectMatrix = mat4::identity();
    aav_translateMatrix = vmath::translate(-1.0f, 0.0f, -3.0f);
    aav_rotationMatrixY = vmath::rotate(aav_anglePyramid, 0.0f, 1.0f, 0.0f);
    
    aav_modelViewMateix = aav_translateMatrix * aav_rotationMatrixY;
    aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform,1,GL_FALSE, aav_modelViewProjectMatrix);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, aav_stone_texture);
    glUniform1i(aav_textureSamplerUniform, 0);
    
    glBindVertexArray(aav_gVao_pyramid);
    
    glDrawArrays(GL_TRIANGLES, 0, 12);
    
    glBindVertexArray(0);
    
    //square
    aav_modelViewMateix = mat4::identity();
    aav_modelViewProjectMatrix = mat4::identity();
    aav_translateMatrix = vmath::translate(1.0f, 0.0f, -3.0f);
    aav_rotationMatrixX = vmath::rotate(aav_angleCube, 1.0f, 0.0f, 0.0f);
    aav_rotationMatrixY = vmath::rotate(aav_angleCube, 0.0f, 1.0f, 0.0f);
    aav_rotationMatrixZ = vmath::rotate(aav_angleCube, 0.0f, 0.0f, 1.0f);
    aav_rotationMatrixA = aav_rotationMatrixX * aav_rotationMatrixY * aav_rotationMatrixZ;
    aav_scaMatrix = vmath::scale(0.75f,0.75f,0.75f);
    
    aav_modelViewMateix = aav_translateMatrix * aav_scaMatrix *aav_rotationMatrixA;
    aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, aav_kundali_texture);
    glUniform1i(aav_textureSamplerUniform,0);
    
    glBindVertexArray(aav_gVao_cube);
    
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
    glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    aav_anglePyramid = aav_anglePyramid + 1.0f;
    if(aav_anglePyramid >= 360.0f)
        aav_anglePyramid = 0.0f;
    aav_angleCube = aav_angleCube + 1.0f;
    if(aav_angleCube >= 360.0f)
        aav_angleCube = 0.0f;
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//NSResponder methos
-(BOOL)acceptsFirstResponder
{
    //code
    //[[self window] makeFirstResponder ];
    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
             
            break;
        case 'F':
        case 'f':
        [[self window] toggleFullScreen:self]; //Full Screen
            break;
        
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
 
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    //code
   
}





-(void)dealloc
{
    //code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    if (aav_gVao_pyramid)
    {
        glDeleteVertexArrays(1, &aav_gVao_pyramid);
        aav_gVao_pyramid = 0;
    }
    
    if (aav_gVbo_position_pyramid)
    {
        glDeleteBuffers(1, &aav_gVbo_position_pyramid);
        aav_gVbo_position_pyramid = 0;
    }
    
    if (aav_gVbo_texture_pyramid)
    {
        glDeleteBuffers(1, &aav_gVbo_texture_pyramid);
        aav_gVbo_texture_pyramid = 0;
    }
    
    if (aav_gVao_cube)
    {
        glDeleteVertexArrays(1, &aav_gVao_cube);
        aav_gVao_cube = 0;
    }
    
    if (aav_gVbo_position_cube)
    {
        glDeleteBuffers(1, &aav_gVbo_position_cube);
        aav_gVbo_position_cube = 0;
    }
    
    if (aav_gVbo_texture_cube)
    {
        glDeleteBuffers(1, &aav_gVbo_texture_cube);
        aav_gVbo_texture_cube = 0;
    }
    
    
    glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
    glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);
    
    glDeleteShader(aav_gVertexShaderObject);
    aav_gVertexShaderObject = 0;
    glDeleteShader(aav_gFragmentShaderObject);
    aav_gFragmentShaderObject = 0;
    
    glDeleteProgram(aav_gShaderProgramObject);
    aav_gShaderProgramObject = 0;
    
    glUseProgram(0);
    
    [super dealloc];
}
@end


//Global Function
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp * outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut,void *displayLinkContext)
{
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return result;
}


