#import <Foundation/Foundation.h>
#import <cocoa/cocoa.h>             //analogous to windows.h xlib.h

#import <QuartzCore/CVDisplayLink.h> // Audio and Display , Core Vidio
#import <OpenGL/gl3.h>             //gl.h
#import"vmath.h"

using namespace vmath;

enum
{
    AAV_ATTRIBUTE_POSITION = 0,
    AAV_ATTRIBUTE_COLOR,
    AAV_ATTRIBUTE_NORMAL,
    AAV_ATTRIBUTE_TEXCORD,
};

//ProtoType
//CallBackFunction
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef, const CVTimeStamp *, const CVTimeStamp * , CVOptionFlags, CVOptionFlags *,void *);

FILE *aav_gpFile = NULL;

GLfloat aav_anglePyramid = 0.0f;
GLfloat aav_angleCube = 0.0f;


@interface appDelegate:NSObject <NSApplicationDelegate, NSWindowDelegate>    //NeStep
//forwardDeclaration  extens  and implement
 @end


//main function
int main(int argc, char *argv[])
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    
    //Baap Object , magaicha , maza hinstace de
    //InBuilt Global object , jo NSApplication cha variable ahe
    NSApp = [NSApplication sharedApplication]; //asnara re NSapplication la
    
    [NSApp setDelegate:[[appDelegate alloc]init]];                          //^
    
    //Event loop , Message Loop
    [NSApp run]; // This is Called as Run Loop
    
    [pool release]; //1. program samtoi, all the memeory is reclamed
    
    return 0;
}

@interface MyOpenGLView: NSOpenGLView

@end

//implementation
@implementation appDelegate
{
    //class variables
    @private
    NSWindow *window;
    MyOpenGLView *myOpenGLView;
}

//instance method
-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    //code
    
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *appDirPath = [appBundle bundlePath]; //ha path return hoto /User/UserName/Desktop/RTR/log/Window.app
    
    NSString *parentDirPath = [appDirPath stringByDeletingLastPathComponent];
    
    NSString *logFileNameWithPath = [NSString stringWithFormat:@"%@/log.txt",parentDirPath];
    
    const char *pszLogFileNameWithPath = [logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];
    
    
    aav_gpFile = fopen(pszLogFileNameWithPath,"w");
    if(aav_gpFile == NULL)
    {
        [self release];
        [NSApp terminate:self];
    }
    
    fprintf(aav_gpFile,"Program Started SuccessFully \n");
    
    
    //NSPoints Struct(x,y), NSSize Structure(width, height)  CG Point , CG Size
    NSRect win_rect = NSMakeRect(0.0,0.0,800.0,600.0); //internal CG Rect :Code Graphics as this "c" library so this is calling is C style
    window = [[NSWindow alloc]initWithContentRect:win_rect
                                        styleMask:NSWindowStyleMaskTitled |
                                                    NSWindowStyleMaskClosable |
                                                    NSWindowStyleMaskMiniaturizable |
                                                    NSWindowStyleMaskResizable
                                          backing: NSBackingStoreBuffered
                                          defer:NO];
    //styleMask is OVERLAPPEDWINDOW
    //NSWindowStyleMaskTitled is caption
    //NSWindowStyleMaskMiniaturizable is minimize
    //NSWindowStyleMaskResizable is resize
    //backing : retain karu nako , buffer madhe thav
    //Defer : NO , maje lagech dakhav , NO ha Objective C madhla Bool
    
    [window setTitle: @"AAV:OpenGL TessellationShader "]; //create window cha Second Parameter
    [window center]; //centering the window
    
    //view Creation
    myOpenGLView = [[MyOpenGLView alloc] initWithFrame:win_rect]; //NSView madhle methos
    
    [window setContentView: myOpenGLView]; // maza contect view set kar
    [window setDelegate: self]; // objective C mathla self ha c++ this
    [window makeKeyAndOrderFront:self]; //mazaya varti focuse kar , set focuse , get foucuse , set Forground window, z order la sartavar pudhe an
    
 }

//Apan Shevti Ithe Yato
-(void)applicationWillTerminate:(NSNotification *)aNotification
{
    //code
    
    
    if(aav_gpFile)
    {
        fprintf(aav_gpFile,"Program Ended SuccessFully \n");
        fclose(aav_gpFile);
        aav_gpFile = NULL;
    }
    
}

//NSWindow deligate  che window
-(void)windowWillClose:(NSNotification *)aNotification
{
    //code
    [NSApp terminate:self];
    
}

-(void)dealloc
{
    //code
    [myOpenGLView release];
    [window release];
    [super dealloc]; //mazya super la dealloc kar
}
@end


//MyView Implementation
@implementation MyOpenGLView
{
    //code
    @private
    CVDisplayLinkRef displayLink;
    
    GLuint gVertexShaderObject;
    GLuint glTeselationControlShaderObject;        //Teselation Control Object
    GLuint glTeselationEvalutationShaderObject;    //Teselation Evaluation Shader
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    GLuint aav_gVao;
    GLuint aav_gVbo_position;
    GLuint aav_mvp_MatrixUniform;
    
    //Teselation Shader unifroms
    GLuint aav_NumberOfSegmentUniform;
    GLuint aav_NumberOfStripsUniform;
    GLuint aav_lineColorUniform;
    
    mat4 aav_PerspectiveProjectionMatrix;
    
    //Teselation
    unsigned int uiNumberOfSegment;
    
}

-(id)initWithFrame:(NSRect)frame
{
    //code
    self = [super initWithFrame:frame];
    if(self)
    {
        //Pixel Format Attribute
        NSOpenGLPixelFormatAttribute attributes[] =
        {
            NSOpenGLPFAOpenGLProfile,NSOpenGLProfileVersion4_1Core,
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),
            NSOpenGLPFANoRecovery,
            NSOpenGLPFAAccelerated,
            NSOpenGLPFAColorSize,24,
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,
            NSOpenGLPFADoubleBuffer,
            0
        };
        
        NSOpenGLPixelFormat * pixelFormal = [[[NSOpenGLPixelFormat alloc]initWithAttributes:attributes]autorelease];
        if(pixelFormal == nil)
        {
            fprintf(aav_gpFile,"Can Not Get pixelFormat Attribute");
            [self release];
            [NSApp terminate:self];
        }
        
        NSOpenGLContext *glContext = [[[NSOpenGLContext alloc]initWithFormat:pixelFormal shareContext:nil]autorelease];
        
        [self setPixelFormat:pixelFormal];
        [self setOpenGLContext:glContext];
        
    }
    return self;
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)outputType
{
    //code
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init]; //New Thread is Created.
    [self drawView];
    [pool release];
    return(kCVReturnSuccess);
}

//initialize prepare OpenGL
-(void)prepareOpenGL
{
    //code
    [super prepareOpenGL];
    
    [[self openGLContext]makeCurrentContext];
    
    //swap Interval
    GLint swapInt = 1;
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];
    
   
    //**VERTEX SHADER***
    gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
    const GLchar* vertexShaserSourceCode =
    "#version 410 core" \
    "\n" \
    "in vec2 vPosition;" \
    "void main(void)" \
    "{" \
    "gl_Position =  vec4(vPosition,0.0,1.0);" \
    "}";
    glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaserSourceCode, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(gVertexShaderObject);
    
    GLint aav_infoLogLength = 0;
    GLint aav_shaderCompiledStatus = 0;
    char* szBuffer = NULL;
    
    glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(gVertexShaderObject, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    /*TeselationControlShader*/
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    glTeselationControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);
    const GLchar* teselationShaserSourceCode =
    "#version 410 core" \
    "\n" \
    "layout(vertices = 4)out;" \
    "uniform int numberOfSegments;"\
    "uniform int numberOfStrips;"\
    "void main(void)" \
    "{" \
    "    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;"\
    "    gl_TessLevelOuter[0] = float(numberOfStrips); " \
    "    gl_TessLevelOuter[1] = float(numberOfSegments); " \
    "}";
    glShaderSource(glTeselationControlShaderObject, 1, (const GLchar**)&teselationShaserSourceCode, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(glTeselationControlShaderObject);
    
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    szBuffer = NULL;
    
    glGetShaderiv(glTeselationControlShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(glTeselationControlShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(glTeselationControlShaderObject, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(aav_gpFile, "Teselation Control Shader Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    
    /******Teselation Evaluation Shader*******/
    glTeselationEvalutationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);
    const GLchar* teselationEvalutaionShaserSourceCode =
    "#version 410 core" \
    "\n" \
    "layout(isolines)in;" \
    "uniform mat4 u_mvpMatrix;" \
    "void main(void)" \
    "{" \
    "    float tessCoord = gl_TessCoord.x;" \
    "    vec3  p0 = gl_in[0].gl_Position.xyz;"\
    "    vec3  p1 = gl_in[1].gl_Position.xyz;"\
    "    vec3  p2 = gl_in[2].gl_Position.xyz;"\
    "    vec3  p3 = gl_in[3].gl_Position.xyz;"\
    "    vec3 p = p0 * (1.0 - tessCoord) * (1.0 - tessCoord) * (1.0 - tessCoord) + p1 * 3.0 * tessCoord  * (1.0 - tessCoord) * (1.0 - tessCoord)  + p2 * 3.0 * tessCoord * tessCoord *(1.0-tessCoord) + p3 *  tessCoord * tessCoord * tessCoord ;" \
    "    gl_Position = u_mvpMatrix * vec4(p,1.0);"\
    "}";
    glShaderSource(glTeselationEvalutationShaderObject, 1, (const GLchar**)&teselationEvalutaionShaserSourceCode, NULL);
    
    //compile shader , Error checking of Compilation
    glCompileShader(glTeselationEvalutationShaderObject);
    
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    szBuffer = NULL;
    
    glGetShaderiv(glTeselationEvalutationShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);
    
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(glTeselationEvalutationShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(glTeselationEvalutationShaderObject, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(aav_gpFile, "Teselation Shader Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    //**FRAGMENT SHADER**
    gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
    const GLchar *fragmentShaderSourceCode =
    "#version 410 core" \
    "\n" \
    "out vec4 FragColor;" \
    "uniform vec4 LineColor;" \
    "void main(void)" \
    "{" \
    "FragColor = LineColor;" \
    "}";
    glShaderSource(gFragmentShaderObject, 1,
                   (const GLchar**)&fragmentShaderSourceCode,NULL);
    //compile shader
    glCompileShader(gFragmentShaderObject);
    
    
    szBuffer = NULL;
    aav_infoLogLength = 0;
    aav_shaderCompiledStatus = 0;
    
    glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS,
                  &aav_shaderCompiledStatus);
    if (aav_shaderCompiledStatus == GL_FALSE)
    {
        glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (szBuffer != NULL)
            {
                GLsizei aav_written;
                glGetShaderInfoLog(gFragmentShaderObject, aav_infoLogLength,
                                   &aav_written, szBuffer);
                fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //**SHADER PROGRAM**
    //Create
    gShaderProgramObject = glCreateProgram();
    
    glAttachShader(gShaderProgramObject, gVertexShaderObject);
    glAttachShader(gShaderProgramObject, glTeselationControlShaderObject);        //
    glAttachShader(gShaderProgramObject, glTeselationEvalutationShaderObject);    //
    glAttachShader(gShaderProgramObject, gFragmentShaderObject);
    
    glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");
    
    glLinkProgram(gShaderProgramObject);
    
    aav_infoLogLength = 0;
    GLint aav_shaderProgramLinkStatus = 0;
    szBuffer = NULL;
    
    glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
    if (aav_shaderProgramLinkStatus == GL_FALSE)
    {
        glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
        if (aav_infoLogLength > 0)
        {
            szBuffer = (char*)malloc(aav_infoLogLength);
            if (aav_infoLogLength > 0)
            {
                GLsizei aav_aav_written;
                glGetProgramInfoLog(gShaderProgramObject, aav_infoLogLength,
                                    &aav_aav_written, szBuffer);
                fprintf(aav_gpFile, "Shader Program Link Log: %s\n", szBuffer);
                free(szBuffer);
                [self release];
                [NSApp terminate:self];
            }
        }
    }
    
    //Post Linking Information
    aav_mvp_MatrixUniform =  glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");
    
    aav_NumberOfSegmentUniform = glGetUniformLocation(gShaderProgramObject, "numberOfSegments");
    
    aav_NumberOfStripsUniform = glGetUniformLocation(gShaderProgramObject, "numberOfStrips");
    
    aav_lineColorUniform = glGetUniformLocation(gShaderProgramObject, "LineColor");
    
    
    
    //vertices array declation
    const GLfloat vertices[] =
    {
        -1.0f,    -1.0f,
        -0.5f,     1.0f,
        0.5f,    -1.0f,
        1.0f,     1.0f
    };
    
    glGenVertexArrays(1, &aav_gVao);
    glBindVertexArray(aav_gVao);
    //Record
    glGenBuffers(1, &aav_gVbo_position); // Named Buffer object
    glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position); // Postion , TexCord, Normal
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    
    glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    
    glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //Record Off / Pause
    glBindVertexArray(0);
    
    
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
   
    //
    glClearColor(0.0f,0.0f,0.0f,1.0f);
    
    aav_PerspectiveProjectionMatrix = mat4::identity();
    
    //create displayLink
    //CallBackSet karne
    //NSPixelFormat to Core
    //CGL
    //DisplayLinkStart
    
    //CV and CGL related Code
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);
    CVDisplayLinkSetOutputCallback(displayLink,&MyDispalyLinkCallBack,self);
    CGLContextObj cglContext = (CGLContextObj)[[self openGLContext]CGLContextObj];
    CGLPixelFormatObj cglPiexelFormat = (CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];
    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPiexelFormat);
    CVDisplayLinkStart(displayLink);
}

-(void)reshape
{
    //code
    [super reshape];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    NSRect rect = [self bounds];
    if(rect.size.height < 0)
        rect.size.height = 1;
    glViewport(0,0,(GLsizei)rect.size.width,(GLsizei)rect.size.height);
    
   aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)rect.size.width / (GLfloat)rect.size.height, 0.1f, 100.0f);
    
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//invalidateRect
-(void)drawRect:(NSRect)dirtyRect
{
    //code
    [self drawView];
}

-(void)drawView
{
    //code
    [[self openGLContext]makeCurrentContext];
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    // code
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    glUseProgram(gShaderProgramObject);
    
    //OpenGL Drawing
    mat4 modelViewMateix = mat4::identity();
    mat4 modelViewProjectMatrix = mat4::identity();
    mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
    modelViewMateix = translateMatrix;
    modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * modelViewMateix;
    
    glUniformMatrix4fv(aav_mvp_MatrixUniform,1,GL_FALSE, modelViewProjectMatrix);
    
    glUniform1i(aav_NumberOfSegmentUniform, uiNumberOfSegment);//
    
    glUniform1i(aav_NumberOfStripsUniform, 1);//
    
    glUniform4fv(aav_lineColorUniform, 1, vmath::vec4(1.0f,1.0f,0.0f,1.0f));//
    
    glBindVertexArray(aav_gVao);
    glPatchParameteri(GL_PATCH_VERTICES, 4);
    glDrawArrays(GL_PATCHES, 0, 4);
    
    glBindVertexArray(0);
    
    glUseProgram(0);
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

//NSResponder methos
-(BOOL)acceptsFirstResponder
{
    //code
    //[[self window] makeFirstResponder ];
    return YES;
}

-(void)keyDown:(NSEvent*)theEvent
{
    //code
    int key = [[theEvent characters] characterAtIndex:0];
    switch(key)
    {
        case 27:
            [self release];
            [NSApp terminate:self];
             
            break;
        case 'F':
        case 'f':
        [[self window] toggleFullScreen:self]; //Full Screen
            break;
        case NSUpArrowFunctionKey:
            uiNumberOfSegment++;
            if (uiNumberOfSegment >= 30)
            {
                uiNumberOfSegment = 30;
            }
            break;
        case NSDownArrowFunctionKey:
            uiNumberOfSegment--;
            if (uiNumberOfSegment <= 0)
            {
                uiNumberOfSegment = 1;
            }
            break;
        default:
            break;
        
    }
}

-(void)mouseDown:(NSEvent*)theEvent
{
    //code
 
}

-(void)rightMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)otherMouseDown:(NSEvent*)theEvent
{
    //code
   
}

-(void)dealloc
{
    //code
    CVDisplayLinkStop(displayLink);
    CVDisplayLinkRelease(displayLink);
    
    if (aav_gVao)
    {
        glDeleteVertexArrays(1, &aav_gVao);
        aav_gVao = 0;
    }
    
    if (aav_gVbo_position)
    {
        glDeleteBuffers(1, &aav_gVbo_position);
        aav_gVbo_position = 0;
    }
    
    glDetachShader(gShaderProgramObject, gVertexShaderObject);
    
    glDetachShader(gShaderProgramObject, glTeselationControlShaderObject); //
    glDetachShader(gShaderProgramObject, glTeselationEvalutationShaderObject); //
    
    glDetachShader(gShaderProgramObject, gFragmentShaderObject);
    
    glDeleteShader(gVertexShaderObject);
    gVertexShaderObject = 0;
    
    glDeleteShader(glTeselationControlShaderObject);//
    glTeselationControlShaderObject = 0;//
    
    glDeleteShader(glTeselationEvalutationShaderObject);//
    glTeselationEvalutationShaderObject = 0;//
    
    glDeleteShader(gFragmentShaderObject);
    gFragmentShaderObject = 0;
    
    glDeleteProgram(gShaderProgramObject);
    gShaderProgramObject = 0;
    
    glUseProgram(0);
    [super dealloc];
}
@end


//Global Function
CVReturn MyDispalyLinkCallBack(CVDisplayLinkRef displayLink, const CVTimeStamp *now, const CVTimeStamp * outputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut,void *displayLinkContext)
{
    //code
    CVReturn result = [(MyOpenGLView *)displayLinkContext getFrameForTime:outputTime];
    return result;
}
