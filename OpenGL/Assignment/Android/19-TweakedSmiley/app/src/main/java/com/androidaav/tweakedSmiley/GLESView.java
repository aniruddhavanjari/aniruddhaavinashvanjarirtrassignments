package com.androidaav.tweakedSmiley;

import android.content.Context;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.view.MotionEvent;

import android.view.GestureDetector;

import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

//
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
//
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnDoubleTapListener , OnGestureListener 
{
	
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] aav_vao_pyramid 		= new int[1];
	private int[] aav_vbo_position_pyramid 		= new int[1];
	private int[] aav_vbo_color_pyramid = new int[1];
	private int[] aav_vbo_texture_pyramid = new int[1]; //texture changes

	private int[] aav_vao_cube = new int[1];
	private int[] aav_vbo_position_cube = new int[1];
	private int[] aav_vbo_color_cube 	= new int[1];
	private int[] aav_vbo_texture_cube = new int[1]; //texture changes

	private int[] aav_stone_texture = new int[1]; // texture changes 
	private int[] aav_kundali_texture = new int[1]; // texture changes 
	private int aav_textureSamplerUniform; // texture changes 
	private int aav_texCoreFlag; //texture Flag

	private int mvpUniform;
	private boolean rotationFlag 			= false;
	private float rotationAngle 			= 0.0f;
	private float rectangleRotationAngle	= 0.0f;

	private int touchCount = 0;

	private float perspectiveProjectionMatrix[] = new float[16]; //4x4 matrix

	final float cubeTexture[] = new float[]
		{
			0.0f,0.0f,
			1.0f,0.0f,
			1.0f,1.0f,
			0.0f,1.0f
		};

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context, this, null , false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
			String glesVersion = gl.glGetString(GL10.GL_VERSION);
			System.out.println("AAV: "+glesVersion);
			//get GLSL version
			String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
			System.out.println("AAV: GLSL Version = "+glslVersion);
			initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused , int width, int height)
	{
		resize(width, height);
	}


	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}


	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}


	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("AAV: "+"Double Tap");
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		if(rotationFlag == false)
		{
			rotationFlag = true;
		}
		else
		{
			rotationFlag = false;
		}
		return(true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("AAV: "+"Single Tap");
		touchCount = touchCount + 1;
		if(touchCount > 4)
		{
			touchCount = 0;
		}
		return(true);
	}

	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("AAV: "+"Long Press");
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("AAV: "+"Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override
	public void onShowPress(MotionEvent e)
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}


	private void initialize(GL10 gl)
	{

		//Vertex Shader 
		//Create Shader

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		//vertes shader source code
		final String vertexShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec4 vPosition;"+
			"in vec2 vTexCoord;"+
			"uniform mat4 u_mvp_matrix;"+
			"out vec2 aav_out_TexCoord;"+
			"void main(void)"+
			"{"+
			"	gl_Position = u_mvp_matrix * vPosition;"+
			"	aav_out_TexCoord = vTexCoord;"+
			"}"
			);

		//provide source code to shader
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		//compile shader and check for errors 
		GLES32.glCompileShader(vertexShaderObject);

		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog= null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("AAV: Vertex Shader Complication log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment Shader 
		//Create Shader 
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//fragment shader source code
		final String fragmentShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec2 aav_out_TexCoord;"+
			"uniform highp sampler2D u_texture_sampler;" +
			"uniform int u_texCoredFlag;" +
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"	if(u_texCoredFlag == 0)"+
			"	{"+
			"		FragColor = vec4(1.0f,1.0f,1.0f,1.0f);"+
			"	}"+
			"	else"+
			"	{"+
			"		FragColor = texture(u_texture_sampler,aav_out_TexCoord);"+
			"	}"+
			
			"}"
			);

		//provide sorce code to shader 
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compiler shader and check for error 
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("AAV : Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Create shader program
		shaderProgramObject = GLES32.glCreateProgram();

		//Attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
	

		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//pre-linki binding of shader  program object with vertex shader attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_TEXTURE0,"vTexCoord");
		
		//link the two shader together to shader program object
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("AAV : Shader Program Link Log ="+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get MVP uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		aav_texCoreFlag = GLES32.glGetUniformLocation(shaderProgramObject,"u_texCoredFlag");
		aav_textureSamplerUniform	 = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture_sampler");
		// vertices , colors , shader , attribs , vbo , vao initialization

		final float cubeVertices[] = new float[]
		{
			0.5f,0.5f,0.5f,
			-0.5f,0.5f,0.5f,
			-0.5f,-0.5f,0.5f,
			0.5f,-0.5f,0.5f
		};

		

		//rectangle
		/****************************************************/

		GLES32.glGenVertexArrays(1,aav_vao_cube,0);
		GLES32.glBindVertexArray(aav_vao_cube[0]);

		GLES32.glGenBuffers(1,aav_vbo_position_cube,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_vbo_position_cube[0]);

		ByteBuffer rectangleByteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		rectangleByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer rectangleVerticesBuffer =rectangleByteBuffer.asFloatBuffer();

		rectangleVerticesBuffer.put(cubeVertices);
		rectangleVerticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							cubeVertices.length * 4,
							rectangleVerticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
									3,
									GLES32.GL_FLOAT,
									false,0,0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);


		//Texture 
		GLES32.glGenBuffers(1,aav_vbo_texture_cube,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_vbo_texture_cube[0]);

		ByteBuffer rectanglebyteColorBuffer = ByteBuffer.allocateDirect(cubeTexture.length * 4);
		rectanglebyteColorBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer rectangleColorBufferR = rectanglebyteColorBuffer.asFloatBuffer();

		rectangleColorBufferR.put(cubeTexture);
		rectangleColorBufferR.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
								cubeTexture.length * 4,
								rectangleColorBufferR,
								GLES32.GL_DYNAMIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_TEXTURE0,
									2,
									GLES32.GL_FLOAT,
									false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_TEXTURE0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);

		aav_stone_texture[0] = LoadGLTexture(R.raw.stone);
		aav_kundali_texture[0] = LoadGLTexture(R.raw.smiley);

		//GLES32.glBindVertexArray(0);
		/***************************************************/
		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		//depth test to do
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//we will always cull back faces for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);

		//Set the background color
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);

		//set projectionMatrix to identitu matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private int LoadGLTexture(int aav_imageFileResourceID)
	{
		int[] aav_texture = new int[1];
		BitmapFactory.Options aav_options = new BitmapFactory.Options();
		aav_options.inScaled = false;// 
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
													aav_imageFileResourceID,
													aav_options);

		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT,4);
		GLES32.glGenTextures(1, aav_texture,0); // Gatu
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, aav_texture[0]);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D,0,bitmap,0);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		return(aav_texture[0]);

	}

	private void resize(int width,int height)
	{
		//code
		GLES32.glViewport(0,0,width, height);

		//orthographic projection -> left, right, bottom , top , near, far
			Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f, 100.0f);
		}

	public void display()
	{
		//code 
		GLES32.glClear((GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT));

		//use shader program
		GLES32.glUseProgram(shaderProgramObject);

		//OpenGL - ES drawing
		float modelViewMatrix[] 			= new float[16];
		float modelViewProjectionMatrix[] 	= new float[16];
		float translateMatrix[] 			= new float[16];
		float rotationMatrix[] 				= new float[16];
		//set modelciew & modelviewprojection matrices to identity 
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(translateMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		//pass above modelviewprojection matrix to the vertex shade on 'u_mvp_matrix ' shader variable 
		//who position value we alrady calcuated in iniWithFrame() by using glGetUniformLocation()
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		
		//draw , either by glDrawTriangle() or glDrawArray() or glDrawElement()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES,0,12); // 3(x,y,z) vertices in pyramidVertices array 

		//unbind vao
		GLES32.glBindVertexArray(0);

		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(translateMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		Matrix.translateM(translateMatrix,0,0.0f,0.0f,-3.6f);
		Matrix.rotateM(rotationMatrix,0,rotationAngle,1.0f,0.0f,0.0f);
		Matrix.rotateM(rotationMatrix,0,rotationAngle,0.0f,1.0f,0.0f);
		Matrix.rotateM(rotationMatrix,0,rotationAngle,0.0f,0.0f,1.0f);
		Matrix.multiplyMM(modelViewMatrix,0,translateMatrix,0,rotationMatrix,0);
		
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);

		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, aav_kundali_texture[0]);
		GLES32.glUniform1i(aav_textureSamplerUniform, 0);

		//bind vao
		GLES32.glBindVertexArray(aav_vao_cube[0]);
		if(touchCount == 0)
		{
			
			GLES32.glUniform1i(aav_texCoreFlag, 0);
		}
		if(touchCount == 1)
		{
			
			cubeTexture[0] = 1.0f;
			cubeTexture[1] = 1.0f;
			cubeTexture[2] = 0.0f;
			cubeTexture[3] = 1.0f;
			cubeTexture[4] = 0.0f;
			cubeTexture[5] = 0.0f;
			cubeTexture[6] = 1.0f;
			cubeTexture[7] = 0.0f;

			GLES32.glUniform1i(aav_texCoreFlag, 1);
		}
		if(touchCount == 2)
		{
			
			cubeTexture[0] = 0.5f;
			cubeTexture[1] = 0.5f;
			cubeTexture[2] = 0.0f;
			cubeTexture[3] = 0.5f;
			cubeTexture[4] = 0.0f;
			cubeTexture[5] = 0.0f;
			cubeTexture[6] = 0.5f;
			cubeTexture[7] = 0.0f;

			GLES32.glUniform1i(aav_texCoreFlag, 1);
		}
		if(touchCount == 3)
		{
			cubeTexture[0] = 2.0f;
			cubeTexture[1] = 2.0f;
			cubeTexture[2] = 0.0f;
			cubeTexture[3] = 2.0f;
			cubeTexture[4] = 0.0f;
			cubeTexture[5] = 0.0f;
			cubeTexture[6] = 2.0f;
			cubeTexture[7] = 0.0f;

			GLES32.glUniform1i(aav_texCoreFlag, 1);

		}
		if(touchCount == 4)
		{
			cubeTexture[0] = 0.5f;
			cubeTexture[1] = 0.5f;
			cubeTexture[2] = 0.5f;
			cubeTexture[3] = 0.5f;
			cubeTexture[4] = 0.5f;
			cubeTexture[5] = 0.5f;
			cubeTexture[6] = 0.5f;
			cubeTexture[7] = 0.5f;

			GLES32.glUniform1i(aav_texCoreFlag, 1);
		}

		
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_vbo_texture_cube[0]);

		ByteBuffer rectanglebyteColorBuffer = ByteBuffer.allocateDirect(cubeTexture.length * 4);
		rectanglebyteColorBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer rectangleColorBufferR = rectanglebyteColorBuffer.asFloatBuffer();

		rectangleColorBufferR.put(cubeTexture);
		rectangleColorBufferR.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
								cubeTexture.length * 4,
								rectangleColorBufferR,
								GLES32.GL_DYNAMIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_TEXTURE0,
									2,
									GLES32.GL_FLOAT,
									false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_TEXTURE0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		//draw , either by glDrawTriangle() or glDrawArray() or glDrawElement()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,0,4); 
		
		//unbind vao
		GLES32.glBindVertexArray(0);

		//un-use shader program
		GLES32.glUseProgram(0);

		if(rotationFlag == true)
		{
			//update();	
		}

		//render/flush
		requestRender();
	}

	void update()
	{
		rotationAngle = rotationAngle + 1.0f;
	}

	void uninitialize()
	{
		//code 
		//destroy vao 
		if(aav_vao_pyramid[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_vao_pyramid,0);
			aav_vao_pyramid[0] = 0;
		}

		//destroy vao
		if(aav_vbo_position_pyramid[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_vbo_position_pyramid,0);
			aav_vbo_position_pyramid[0] =0;
		}

		if(aav_vbo_color_pyramid[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_vbo_color_pyramid,0);
			aav_vbo_color_pyramid[0] =0;
		}

		//destroy vao 
		if(aav_vao_cube[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_vao_cube,0);
			aav_vao_cube[0] = 0;
		}

		//destroy vao
		if(aav_vbo_position_cube[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_vbo_position_cube,0);
			aav_vbo_position_cube[0] =0;
		}

		if(aav_vbo_color_cube[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_vbo_color_cube,0);
			aav_vbo_color_cube[0] =0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject!= 0)
			{
				//detech vetex shader from shaer program object 
				GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
				//delete vertex shader object
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				//detech fragment shader from shader program object 
				GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
				//delete fargement shader object 
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		//delte shader prograqm object 
		if(shaderProgramObject !=0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
