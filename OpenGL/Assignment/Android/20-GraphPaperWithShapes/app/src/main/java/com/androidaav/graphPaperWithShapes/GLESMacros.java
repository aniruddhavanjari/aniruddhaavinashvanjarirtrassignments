package com.androidaav.graphPaperWithShapes;

public class GLESMacros
{
	//attribute index
	public static final int AAV_ATTRIBUTE_VERTEX = 0;
	public static final int AAV_ATTRIBUTE_COLOR = 1;
	public static final int AAV_ATTRIBUTE_NORMAL = 2;
	public static final int AAV_ATTRIBUTE_TEXTURE0=3;
}

