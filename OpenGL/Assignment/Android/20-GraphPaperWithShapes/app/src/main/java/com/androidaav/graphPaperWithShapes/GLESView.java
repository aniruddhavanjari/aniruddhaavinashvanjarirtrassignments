package com.androidaav.graphPaperWithShapes;

import android.content.Context;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.view.MotionEvent;

import android.view.GestureDetector;

import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;


import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

import java.lang.Math;
//
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnDoubleTapListener , OnGestureListener 
{
	
	private final Context context;

	private GestureDetector gestureDetector;


	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	//Tignalge Vao
	private int[] aav_gVao_triangle = new int[1];
	private int[] aav_gVbo_position_triangle = new int[1];
	private int[] aav_gVbo_color_triangle = new int[1];

	//Plance Quad Vao
	private int[] aav_vao_square = new int[1];
	private int[] aav_vbo_position_square = new int[1];
	private int[] aav_vbo_color_square 	= new int[1];

	//Inner Circle
	private int[] aav_gVao_circleInner = new int[1];
	private int[] aav_gVbo_circleInner_position = new int[1];
	private int[] aav_aVbo_circleInner_color = new int[1];

	//Outer Circle
	private int[] aav_gVao_circleOuter = new int[1];
	private int[] aav_gVbo_circleOuter_position = new int[1];
	private int[] aav_aVbo_circleOuter_color = new int[1];

	//Lines
	private int[] aav_gVao_line = new int[1];
	private int[] aav_gVbo_line_position = new int[1];
	private int[] aav_gVbo_line_color = new int[1];

	//Point
	private int[] aav_gVao_point = new int[1];
	private int[] aav_gVbo_point_position = new int[1];
	private int[] aav_gVbo_point_color = new int[1];

	private int mvpUniform;
	private boolean rotationFlag 			= false;
	private float rotationAngle 			= 0.0f;
	private float rectangleRotationAngle	= 0.0f;

	private int touchCount = 0;

	private float perspectiveProjectionMatrix[] = new float[16]; //4x4 matrix

	
	private float  aav_circleVertices[];
	private float aav_circleColor[];

	private float aav_circleVerticesInner[];
	private float aav_circleColorInner[];

	private float r;
	private int count = 0;

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context, this, null , false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
			String glesVersion = gl.glGetString(GL10.GL_VERSION);
			System.out.println("AAV: "+glesVersion);
			//get GLSL version
			String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
			System.out.println("AAV: GLSL Version = "+glslVersion);
			initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused , int width, int height)
	{
		resize(width, height);
	}

	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}

	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("AAV: "+"Double Tap");
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		if(rotationFlag == false)
		{
			rotationFlag = true;
		}
		else
		{
			rotationFlag = false;
		}
		return(true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("AAV: "+"Single Tap");
		touchCount = touchCount + 1;
		if(touchCount > 4)
		{
			touchCount = 0;
		}
		return(true);
	}

	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("AAV: "+"Long Press");
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("AAV: "+"Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override
	public void onShowPress(MotionEvent e)
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}


	private void initialize(GL10 gl)
	{

		//Vertex Shader 
		//Create Shader

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		//vertes shader source code
		final String vertexShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec4 vPosition;"+
			"in vec4 vColor;"+
			"uniform mat4 u_mvp_matrix;"+
			"out vec4 out_color;"+
			"void main(void)"+
			"{"+
			"	gl_Position = u_mvp_matrix * vPosition;"+
			"	out_color = vColor;"+
			"	gl_PointSize=1.5;" +
			"}"
			);

		//provide source code to shader
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		//compile shader and check for errors 
		GLES32.glCompileShader(vertexShaderObject);

		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog= null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("AAV: Vertex Shader Complication log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment Shader 
		//Create Shader 
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//fragment shader source code
		final String fragmentShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"in vec4 out_color;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"		FragColor = out_color;"+
			"}"
			);

		//provide sorce code to shader 
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compiler shader and check for error 
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("AAV : Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Create shader program
		shaderProgramObject = GLES32.glCreateProgram();

		//Attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
	

		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//pre-linki binding of shader  program object with vertex shader attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_COLOR,"vColor");
		
		//link the two shader together to shader program object
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("AAV : Shader Program Link Log ="+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get MVP uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");
		//aav_texCoreFlag = GLES32.glGetUniformLocation(shaderProgramObject,"u_texCoredFlag");
		//aav_textureSamplerUniform	 = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture_sampler");
		// vertices , colors , shader , attribs , vbo , vao initialization

		final float cubeVertices[] = new float[]
		{
			0.5f, 0.5f, 0.0f,
	        -0.5f, 0.5f, 0.0f,

	        -0.5f, 0.5f, 0.0f,
	        -0.5f, -0.5f, 0.0f,

	        -0.5f, -0.5f, 0.0f,
	        0.5f, -0.5f, 0.0f,

	        0.5f, -0.5f, 0.0f,
	        0.5f, 0.5f, 0.0f
		};

		final float cubeColor[] = new float[]
		{
			1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f
		};

		final float aav_triangleVertices[] = new float[]
		{
	        0.0f, 0.5f, 0.0f,
	        -0.5f, -0.5f, 0.0f,

	        -0.5f, -0.5f, 0.0f,
	        0.5f, -0.5f, 0.0f,

	        0.5f, -0.5f, 0.0f,
	        0.0f, 0.5f, 0.0f
   		 };

   		final float aav_trianglecolors[] = new float[]
   		{
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f,
	        1.0f, 1.0f, 1.0f
    	};

    	final float aav_lineVertex[] = new float[]
   		{
	         0.2f, -1.3f, 0.0f,
	        0.2f, 1.3f, 0.0f,


	        0.4f, -1.3f, 0.0f,
	        0.4f, 1.3f, 0.0f,


	        0.6f, -1.3f, 0.0f,
	        0.6f, 1.3f, 0.0f,

	        0.8f, -1.3f, 0.0f,
	        0.8f, 1.3f, 0.0f,


	        1.0f, -1.3f, 0.0f,
	        1.0f, 1.3f, 0.0f,

	        1.2f, -1.3f, 0.0f,
	        1.2f, 1.3f, 0.0f,


	        1.4f, -1.3f, 0.0f,
	        1.4f, 1.3f, 0.0f,


	        1.6f, -1.3f, 0.0f,
	        1.6f, 1.3f, 0.0f,


	        1.8f, -1.3f, 0.0f,
	        1.8f, 1.3f, 0.0f,

	        2.0f, -1.3f, 0.0f,
	        2.0f, 1.3f, 0.0f,


	        //x negative
	        -0.2f, -1.3f, 0.0f,
	        -0.2f, 1.3f, 0.0f,


	        -0.4f, -1.3f, 0.0f,
	        -0.4f, 1.3f, 0.0f,


	        -0.6f, -1.3f, 0.0f,
	        -0.6f, 1.3f, 0.0f,

	        -0.8f, -1.3f, 0.0f,
	        -0.8f, 1.3f, 0.0f,


	        -1.0f, -1.3f, 0.0f,
	        -1.0f, 1.3f, 0.0f,

	        -1.2f, -1.3f, 0.0f,
	        -1.2f, 1.3f, 0.0f,


	        -1.4f, -1.3f, 0.0f,
	        -1.4f, 1.3f, 0.0f,


	        -1.6f, -1.3f, 0.0f,
	        -1.6f, 1.3f, 0.0f,


	        -1.8f, -1.3f, 0.0f,
	        -1.8f, 1.3f, 0.0f,

	        -2.0f, -1.3f, 0.0f,
	        -2.0f, 1.3f, 0.0f,

	        //y axies
	        -2.0f, 0.13f, 0.0f,
	        2.0f, 0.13f, 0.0f,


	        -2.0f, 0.26f, 0.0f,
	        2.0f, 0.26f, 0.0f,


	        -2.0f, 0.39f, 0.0f,
	        2.0f, 0.39f, 0.0f,


	        -2.0f, 0.52f, 0.0f,
	        2.0f, 0.52f, 0.0f,

	        -2.0f, 0.65f, 0.0f,
	        2.0f, 0.65f, 0.0f,


	        -2.0f, 0.78f, 0.0f,
	        2.0f, 0.78f, 0.0f,


	        -2.0f, 0.91f, 0.0f,
	        2.0f, 0.91f, 0.0f,


	        -2.0f, 1.04f, 0.0f,
	        2.0f, 1.04f, 0.0f,

	        -2.0f, 1.17f, 0.0f,
	        2.0f, 1.17f, 0.0f,

	        -2.0f, 1.3f, 0.0f,
	        2.0f, 1.3f, 0.0f,

	        //
	        -2.0f, -0.13f, 0.0f,
	        2.0f, -0.13f, 0.0f,


	        -2.0f, -0.26f, 0.0f,
	        2.0f, -0.26f, 0.0f,


	        -2.0f, -0.39f, 0.0f,
	        2.0f, -0.39f, 0.0f,


	        -2.0f, -0.52f, 0.0f,
	        2.0f, -0.52f, 0.0f,

	        -2.0f, -0.65f, 0.0f,
	        2.0f, -0.65f, 0.0f,


	        -2.0f, -0.78f, 0.0f,
	        2.0f, -0.78f, 0.0f,


	        -2.0f, -0.91f, 0.0f,
	        2.0f, -0.91f, 0.0f,


	        -2.0f, -1.04f, 0.0f,
	        2.0f, -1.04f, 0.0f,

	        -2.0f, -1.17f, 0.0f,
	        2.0f, -1.17f, 0.0f,

	        -2.0f, -1.3f, 0.0f,
	        2.0f, -1.3f, 0.0f,

	        0.0f, -1.3f, 0.0f,
	        0.0f, 1.3f, 0.0f,
	        -2.0f, 0.0f, 0.0f,
	        2.0f, 0.0f, 0.0f
    	};

    	final float aav_lineColor[] = new float[]
    	{
	    	0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        //
	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        0.0f, 0.0f, 1.0f,
	        0.0f, 0.0f, 1.0f,

	        //0.0f, 0.0f, 1.0f,
	        //0.0f, 0.0f, 1.0f,

	        //0.0f, 0.0f, 1.0f,
	        //0.0f, 0.0f, 1.0f,

	        //0.0f, 0.0f, 1.0f,
	        //0.0f, 0.0f, 1.0f,

	        //0.0f, 0.0f, 1.0f,
	        //0.0f, 0.0f, 1.0f,


	        1.0f, 0.0f, 0.0f,
	        1.0f, 0.0f, 0.0f,
	        0.0f, 1.0f, 0.0f,
	        0.0f, 1.0f, 0.0f
    	};

    	final float aav_pointVertex[] = new float[]
    	{
    		 0.0f, 0.0f, 0.0f
    	};

    	final float aav_pointColor[] = new float[]
    	{
    		1.0f, 1.0f, 1.0f
    	};

		//Square
		/****************************************************/

		GLES32.glGenVertexArrays(1,aav_vao_square,0);
		GLES32.glBindVertexArray(aav_vao_square[0]);

		GLES32.glGenBuffers(1,aav_vbo_position_square,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_vbo_position_square[0]);

		ByteBuffer rectangleByteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		rectangleByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer rectangleVerticesBuffer =rectangleByteBuffer.asFloatBuffer();

		rectangleVerticesBuffer.put(cubeVertices);
		rectangleVerticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							cubeVertices.length * 4,
							rectangleVerticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
									3,
									GLES32.GL_FLOAT,
									false,0,0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		//Color
		GLES32.glGenBuffers(1,aav_vbo_color_square,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_vbo_color_square[0]);

		ByteBuffer byteColorBuffer = ByteBuffer.allocateDirect(cubeColor.length * 4);
		byteColorBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer ColorBufferR =byteColorBuffer.asFloatBuffer();

		ColorBufferR.put(cubeColor);
		ColorBufferR.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
								cubeColor.length * 4,
								ColorBufferR,
								GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_COLOR,
									3,
									GLES32.GL_FLOAT,
									false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);


		//Triangle Vao
		GLES32.glGenVertexArrays(1,aav_gVao_triangle,0);
		GLES32.glBindVertexArray(aav_gVao_triangle[0]);

		GLES32.glGenBuffers(1,aav_gVbo_position_triangle,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_gVbo_position_triangle[0]);

		ByteBuffer triangleByteBuffer = ByteBuffer.allocateDirect(aav_triangleVertices.length * 4);
		triangleByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer triangleVerticesBuffer =triangleByteBuffer.asFloatBuffer();

		triangleVerticesBuffer.put(aav_triangleVertices);
		triangleVerticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							aav_triangleVertices.length * 4,
							triangleVerticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
									3,
									GLES32.GL_FLOAT,
									false,0,0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		//Color
		GLES32.glGenBuffers(1,aav_gVbo_color_triangle,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_gVbo_color_triangle[0]);

		ByteBuffer trianglebyteColorBuffer = ByteBuffer.allocateDirect(aav_trianglecolors.length * 4);
		trianglebyteColorBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer triangleColorBufferR =trianglebyteColorBuffer.asFloatBuffer();

		triangleColorBufferR.put(aav_trianglecolors);
		triangleColorBufferR.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
								aav_trianglecolors.length * 4,
								triangleColorBufferR,
								GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_COLOR,
									3,
									GLES32.GL_FLOAT,
									false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);


		Calculation();

		//Inner Circle
		GLES32.glGenVertexArrays(1,aav_gVao_circleInner,0);
		GLES32.glBindVertexArray(aav_gVao_circleInner[0]);

		GLES32.glGenBuffers(1,aav_gVbo_circleInner_position,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_gVbo_circleInner_position[0]);

		ByteBuffer innerCircleByteBuffer = ByteBuffer.allocateDirect(aav_circleVerticesInner.length * 4);
		innerCircleByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer innerCircleVerticesBuffer =innerCircleByteBuffer.asFloatBuffer();

		innerCircleVerticesBuffer.put(aav_circleVerticesInner);
		innerCircleVerticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							aav_circleVerticesInner.length * 4,
							innerCircleVerticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
									3,
									GLES32.GL_FLOAT,
									false,0,0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		//Color
		GLES32.glGenBuffers(1,aav_aVbo_circleInner_color,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_aVbo_circleInner_color[0]);

		ByteBuffer innerCirclebyteColorBuffer = ByteBuffer.allocateDirect(aav_circleColorInner.length * 4);
		innerCirclebyteColorBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer innerCircleColorBufferR =innerCirclebyteColorBuffer.asFloatBuffer();

		innerCircleColorBufferR.put(aav_circleColorInner);
		innerCircleColorBufferR.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
								aav_circleColorInner.length * 4,
								innerCircleColorBufferR,
								GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_COLOR,
									3,
									GLES32.GL_FLOAT,
									false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);


		//Outer Circle
		GLES32.glGenVertexArrays(1,aav_gVao_circleOuter,0);
		GLES32.glBindVertexArray(aav_gVao_circleOuter[0]);

		GLES32.glGenBuffers(1,aav_gVbo_circleOuter_position,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_gVbo_circleOuter_position[0]);

		ByteBuffer outerCircleByteBuffer = ByteBuffer.allocateDirect(aav_circleVertices.length * 4);
		outerCircleByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer outerCircleVerticesBuffer =outerCircleByteBuffer.asFloatBuffer();

		outerCircleVerticesBuffer.put(aav_circleVertices);
		outerCircleVerticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							aav_circleVertices.length * 4,
							outerCircleVerticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
									3,
									GLES32.GL_FLOAT,
									false,0,0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		//Color
		GLES32.glGenBuffers(1,aav_aVbo_circleOuter_color,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_aVbo_circleOuter_color[0]);

		ByteBuffer outerCirclebyteColorBuffer = ByteBuffer.allocateDirect(aav_circleColor.length * 4);
		outerCirclebyteColorBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer outerCircleColorBufferR =outerCirclebyteColorBuffer.asFloatBuffer();

		outerCircleColorBufferR.put(aav_circleColor);
		outerCircleColorBufferR.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
								aav_circleColor.length * 4,
								outerCircleColorBufferR,
								GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_COLOR,
									3,
									GLES32.GL_FLOAT,
									false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);

		//Lines
		GLES32.glGenVertexArrays(1,aav_gVao_line,0);
		GLES32.glBindVertexArray(aav_gVao_line[0]);

		GLES32.glGenBuffers(1,aav_gVbo_line_position,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_gVbo_line_position[0]);

		ByteBuffer lineByteBuffer = ByteBuffer.allocateDirect(aav_lineVertex.length * 4);
		lineByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer lineVerticesBuffer =lineByteBuffer.asFloatBuffer();

		lineVerticesBuffer.put(aav_lineVertex);
		lineVerticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							aav_lineVertex.length * 4,
							lineVerticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
									3,
									GLES32.GL_FLOAT,
									false,0,0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		//Color
		GLES32.glGenBuffers(1,aav_gVbo_line_color,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_gVbo_line_color[0]);

		ByteBuffer linebyteColorBuffer = ByteBuffer.allocateDirect(aav_lineColor.length * 4);
		linebyteColorBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer lineColorBufferR =linebyteColorBuffer.asFloatBuffer();

		lineColorBufferR.put(aav_lineColor);
		lineColorBufferR.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
								aav_lineColor.length * 4,
								lineColorBufferR,
								GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_COLOR,
									3,
									GLES32.GL_FLOAT,
									false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);

		//Point
		GLES32.glGenVertexArrays(1,aav_gVao_point,0);
		GLES32.glBindVertexArray(aav_gVao_point[0]);

		GLES32.glGenBuffers(1,aav_gVbo_point_position,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_gVbo_point_position[0]);

		ByteBuffer pointByteBuffer = ByteBuffer.allocateDirect(aav_pointVertex.length * 4);
		pointByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer pointVerticesBuffer =pointByteBuffer.asFloatBuffer();

		pointVerticesBuffer.put(aav_pointVertex);
		pointVerticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							aav_pointVertex.length * 4,
							pointVerticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
									3,
									GLES32.GL_FLOAT,
									false,0,0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		//Color
		GLES32.glGenBuffers(1,aav_gVbo_point_color,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_gVbo_point_color[0]);

		ByteBuffer pointbyteColorBuffer = ByteBuffer.allocateDirect(aav_pointColor.length * 4);
		pointbyteColorBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer pointColorBufferR =pointbyteColorBuffer.asFloatBuffer();

		pointColorBufferR.put(aav_pointColor);
		pointColorBufferR.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
								aav_pointColor.length * 4,
								pointColorBufferR,
								GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_COLOR,
									3,
									GLES32.GL_FLOAT,
									false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_COLOR);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);

		/***************************************************/
		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		//depth test to do
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//we will always cull back faces for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);

		//Set the background color
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);

		//set projectionMatrix to identitu matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width,int height)
	{
		//code
		GLES32.glViewport(0,0,width, height);

		//orthographic projection -> left, right, bottom , top , near, far
			Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f, 100.0f);
	}

	public void Calculation()
	{
		int Line = 0;
		float fLine = 0.5f;
		float side;
		float Squareside;
		float OutSideCircleR;

		float a; //Consider side 1
		float b; //Consider side 2
		float c; //Consider side 3

		float xcenterCor;
		float ycenterCor;

		//Code
		a = (float)Math.sqrt(((0.5f - (-0.5f)) * (0.5f - (-0.5f))) + (((-0.5f) - (-0.5f)) * ((-0.5f) - (-0.5f)))); //1
		b = (float)Math.sqrt(((0.0f - 0.5f) * (0.0f - 0.5f)) + ((0.5f - (-0.5f)) * (0.5f - (-0.5f)))); //1.118033989
		c = (float)Math.sqrt((((-0.5f) - 0.0f) * ((-0.5f) - 0.0f)) + (((-0.5f) - 0.5f) * ((-0.5f) - 0.5f))); //1.118033989
		
		//Formula For x,y Cordinate for Center Point of Triangle To Draw the Circle.
		xcenterCor = ((a * (0.0f)) + (b * (-0.5f)) + (c * (0.5f))) / (a + b + c);
		ycenterCor = ((a * (0.5f)) + (b * (-0.5f)) + (c * (-0.5f))) / (a + b + c);

		side = (a + b + c) / 2;
		r = (float)Math.sqrt(side * (side - a) * (side - b) * (side - c)) / side;  //Formmula For inCircle or inscribed Circle Radius. 

		//Circle OutSide Rectangle
		Squareside = (float)Math.sqrt((((-0.5f) - (-0.5f)) * ((-0.5f) - (-0.5f))) + (((-0.5f) - 0.5f) * ((-0.5f) - 0.5f))); // We Get The Length of One Size of Square
		OutSideCircleR = (float)((Squareside * Math.sqrt(2)) / 2); // We Get Radius of Circle 

		
		for (float angle = 0; angle < 2 * Math.PI; angle = angle + 0.001f)
		{
			count = count + 1;
		}

		aav_circleVertices = new float[4 * count * 3];
    	aav_circleColor = new float[4* count * 3];

		int i = 0;
		for (float angle = 0; angle < 2 * Math.PI; angle = angle + 0.001f)
		{
			aav_circleVertices[i] = (float)(OutSideCircleR * Math.sin(angle));
			aav_circleColor[i] = 1.0f; 
			i = i + 1;
			aav_circleVertices[i] = (float)(OutSideCircleR * Math.cos(angle));
			aav_circleColor[i] = 1.0f;
			i = i + 1;
			aav_circleVertices[i] = 0.0f;
			aav_circleColor[i] = 1.0f;
			i = i + 1;
		}

		i = 0; 
		 aav_circleVerticesInner = new float[4 * count * 3];
   		 aav_circleColorInner = new float[4 * count * 3];
		for (float angle = 0; angle < 2 * Math.PI; angle = angle + 0.001f)
		{
			aav_circleVerticesInner[i] = (float)(r * Math.sin(angle)+  +xcenterCor);
			aav_circleColorInner[i] = 1.0f;
			i = i + 1;
			aav_circleVerticesInner[i] = (float)(r * Math.cos(angle)+ +ycenterCor);
			aav_circleColorInner[i] = 1.0f;
			i = i + 1;
			aav_circleVerticesInner[i] = 0.0f;
			aav_circleColorInner[i] = 1.0f;
			i = i + 1;
		}

	}

	public void display()
	{
		//code 
		GLES32.glClear((GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT));

		//use shader program
		GLES32.glUseProgram(shaderProgramObject);

		//OpenGL - ES drawing
		float modelViewMatrix[] 			= new float[16];
		float modelViewProjectionMatrix[] 	= new float[16];
		float translateMatrix[] 			= new float[16];
		float rotationMatrix[] 				= new float[16];
		//set modelciew & modelviewprojection matrices to identity 
		
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(translateMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		Matrix.translateM(translateMatrix,0,0.0f,0.0f,-3.0f);
	
		Matrix.multiplyMM(modelViewMatrix,0,translateMatrix,0,rotationMatrix,0);
		
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);



		//Line vao
		GLES32.glBindVertexArray(aav_gVao_line[0]);
		GLES32.glDrawArrays(GLES32.GL_LINES, 0, 84);
		GLES32.glBindVertexArray(0);

		//bind vao
		GLES32.glBindVertexArray(aav_vao_square[0]);
		
		//draw , either by glDrawTriangle() or glDrawArray() or glDrawElement()
		GLES32.glDrawArrays(GLES32.GL_LINE_LOOP,0,8); 
		
		//unbind vao
		GLES32.glBindVertexArray(0);

		//inner circle
		GLES32.glBindVertexArray(aav_gVao_circleInner[0]);
		GLES32.glDrawArrays(GLES32.GL_POINTS, 0, count);
		GLES32.glBindVertexArray(0);

		//Outer circle
		GLES32.glBindVertexArray(aav_gVao_circleOuter[0]);
		GLES32.glDrawArrays(GLES32.GL_POINTS, 0, count);
		GLES32.glBindVertexArray(0);

		//Triangle 
		GLES32.glBindVertexArray(aav_gVao_triangle[0]);
		GLES32.glDrawArrays(GLES32.GL_LINE_LOOP, 0, 6);
		GLES32.glBindVertexArray(0);

		//Point
		GLES32.glBindVertexArray(aav_gVao_point[0]);
		GLES32.glDrawArrays(GLES32.GL_POINTS, 0, 1);
		GLES32.glBindVertexArray(0);

		//un-use shader program
		GLES32.glUseProgram(0);

		//render/flush
		requestRender();
	}

	void uninitialize()
	{
		//code 
		
		//destroy vao 
		if(aav_vao_square[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_vao_square,0);
			aav_vao_square[0] = 0;
		}

		//Triangle
		if(aav_gVao_triangle[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_gVao_triangle,0);
			aav_gVao_triangle[0] = 0;
		}

		//Line vao
		if(aav_gVao_line[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_gVao_line,0);
			aav_gVao_line[0] = 0;
		}

		//Point vao
		if(aav_gVao_point[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_gVao_point,0);
			aav_gVao_point[0] = 0;
		}

		//InnerCircle vao
		if(aav_gVao_circleInner[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_gVao_circleInner,0);
			aav_gVao_circleInner[0] = 0;
		}

		//outerCircle vao
		if(aav_gVao_circleOuter[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_gVao_circleOuter,0);
			aav_gVao_circleOuter[0] = 0;
		}

		//destroy vbo
		if(aav_vbo_position_square[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_vbo_position_square,0);
			aav_vbo_position_square[0] =0;
		}

		if(aav_vbo_color_square[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_vbo_color_square,0);
			aav_vbo_color_square[0] =0;
		}

		//Trinagle vbo 
		if(aav_gVbo_position_triangle[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_gVbo_position_triangle,0);
			aav_gVbo_position_triangle[0] =0;
		}

		if(aav_gVbo_color_triangle[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_gVbo_color_triangle,0);
			aav_gVbo_color_triangle[0] =0;
		}

		//Line vbo
		if(aav_gVbo_line_position[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_gVbo_line_position,0);
			aav_gVbo_line_position[0] =0;
		}

		if(aav_gVbo_line_color[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_gVbo_line_color,0);
			aav_gVbo_line_color[0] =0;
		}

		//Incircle vbo 
		if(aav_gVbo_circleInner_position[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_gVbo_circleInner_position,0);
			aav_gVbo_circleInner_position[0] =0;
		}

		if(aav_aVbo_circleInner_color[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_aVbo_circleInner_color,0);
			aav_aVbo_circleInner_color[0] =0;
		}


		//outCircle vbo
		if(aav_gVbo_circleOuter_position[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_gVbo_circleOuter_position,0);
			aav_gVbo_circleOuter_position[0] =0;
		}

		if(aav_aVbo_circleOuter_color[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_aVbo_circleOuter_color,0);
			aav_aVbo_circleOuter_color[0] =0;
		}

		//Point vbo
		if(aav_gVbo_point_position[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_gVbo_point_position,0);
			aav_gVbo_point_position[0] =0;
		}

		if(aav_gVbo_point_color[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_gVbo_point_color,0);
			aav_gVbo_point_color[0] =0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject!= 0)
			{
				//detech vetex shader from shaer program object 
				GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
				//delete vertex shader object
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				//detech fragment shader from shader program object 
				GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
				//delete fargement shader object 
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		//delte shader prograqm object 
		if(shaderProgramObject !=0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
