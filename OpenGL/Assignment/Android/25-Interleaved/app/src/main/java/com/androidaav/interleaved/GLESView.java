package com.androidaav.interleaved;

import android.content.Context;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.view.MotionEvent;

import android.view.GestureDetector;

import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

import java.nio.ShortBuffer;

import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.opengl.GLUtils;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnDoubleTapListener , OnGestureListener 
{
	
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int aav_modelMatrixUniform;
	private int aav_viewMatrixUniform;
	private int aav_PerspectiveProjectionUniform;
	
	private int aav_LkeyPressedUniform;

	private int aav_laUniform;
	private int aav_ldUniform;
	private int aav_lsUniform;

	private int aav_lightPositionUniform;

	private int aav_kaUniform;
	private int aav_kdUniform;
	private int aav_ksUniform;

	private int aav_kShininessUniform;

	private boolean aav_bLightFlag 			= false;
	private float rotationAngle 			= 0.0f;
	private float rectangleRotationAngle	= 0.0f;

	private float perspectiveProjectionMatrix[] = new float[16]; //4x4 matrix

	private int[] aav_vao_cube = new int[1];
	private int[] aav_vbo_position_cube = new int[1];

    //Light Array
    private float aav_lightAmbiant[] = new float[3];	//la
    private float aav_lightDiffuse[] = new float[3];	//ld
    private float aav_lightSpecular[] = new float[3];	//ls

    private float aav_lightPosition[] = new float[4];

    //Material Array
    private float aav_materialAmbiant[] = new float[3];		//ka
    private float aav_materialDiffuse[] = new float[3];		//kd
    private float aav_materialSpecular[] = new float[3];	//ks 

    private float materialShininess = 128.0f;

    private int[] aav_stone_texture = new int[1]; // texture changes 
	private int[] aav_kundali_texture = new int[1]; // texture changes 
	private int aav_textureSamplerUniform; // texture changes 

   // private int rotationAngle = 0;
	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context, this, null , false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
			String glesVersion = gl.glGetString(GL10.GL_VERSION);
			System.out.println("AAV: "+glesVersion);
			//get GLSL version
			String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
			System.out.println("AAV: GLSL Version = "+glslVersion);
			initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused , int width, int height)
	{
		resize(width, height);
	}


	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}


	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}


	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("AAV: "+"Double Tap");
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		if(aav_bLightFlag == false)
		{
			aav_bLightFlag = true;
		}
		else
		{
			aav_bLightFlag = false;
		}
		return(true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("AAV: "+"Single Tap");
		return(true);
	}

	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("AAV: "+"Long Press");
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("AAV: "+"Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override
	public void onShowPress(MotionEvent e)
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{

		//Vertex Shader 
		//Create Shader

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		//vertes shader source code
		final String vertexShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
	    	"in vec4 vPosition;"+
			"in vec3 vNormal;" +
			"in vec4 vColor;"+
			"in vec2 vTexture;"+
  	  		"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_lKeyPressed;" +
			"uniform vec4 u_lightPosistion;" +
			"out vec3 tranformed_normal;"+
			"out vec3 lightDirection;" +
			"out vec3 view_vector;" +
			"out vec4 out_vColor;"+
			"out vec2 out_vTexture;"+
			"void main(void)" +
			"{" +
			"		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" +
			"		tranformed_normal = (mat3(u_view_matrix * u_model_matrix) * vNormal);" +
			"		lightDirection = (vec3(u_lightPosistion - eyeCordinate));" +
			"		view_vector = (-eyeCordinate.xyz);" +
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"out_vColor = vColor;"+
			"out_vTexture =vTexture;"+
			"}"
			);

		//provide source code to shader
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		//compile shader and check for errors 
		GLES32.glCompileShader(vertexShaderObject);

		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog= null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("AAV: Vertex Shader Complication log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment Shader 
		//Create Shader 
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//fragment shader source code
		final String fragmentShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"uniform highp sampler2D u_texture_sampler;" +
    		"in vec3 tranformed_normal;"+
    		"in vec3 lightDirection;" +
    		"in vec3 view_vector;" +
    		"in vec4 out_vColor;"+
			"in vec2 out_vTexture;"+
    		"uniform mediump int u_lKeyPressed;" +
    		"uniform vec3 u_la;" +
    		"uniform vec3 u_ld;" +
    		"uniform vec3 u_ls;" +

    		"uniform vec3 u_ka;" +
    		"uniform vec3 u_kd;" +
    		"uniform vec3 u_ks;" +
    		"uniform float u_kShineness;"+

    		"out vec4 FragColor;" +
    		"vec3 fong_ads_light;" +
    		"void main(void)" +
    		"{" +
    		"		vec3 normalize_tranformed_normal = normalize(tranformed_normal);" +
    		"		vec3 normalize_lightDirection = normalize(lightDirection);" +
    		"		vec3 normalize_view_vector = normalize(view_vector);" +
    		"		vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" +
    		"		vec3 ambiant = u_la * u_ka;" +
    		"		vec3 diffuse = u_ld * u_kd * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" +
    		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness);" +
    		"		fong_ads_light = ambiant + diffuse + specular;" +
    		"	FragColor = vec4(fong_ads_light,1.0f) * vec4(texture(u_texture_sampler,out_vTexture)) *out_vColor ;" +
    		"}"
			);
		
		//provide sorce code to shader 
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compiler shader and check for error 
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("AAV : Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Create shader program
		shaderProgramObject = GLES32.glCreateProgram();

		//Attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
	

		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//pre-linki binding of shader  program object with vertex shader attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_COLOR,"vColor");
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_NORMAL,"vNormal");
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_TEXTURE0,"vTexture");
		
		//link the two shader together to shader program object
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("AAV : Shader Program Link Log ="+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get MVP uniform location
		
		aav_modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_model_matrix");
		aav_viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_view_matrix");
		aav_PerspectiveProjectionUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_projection_matrix");

		aav_LkeyPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lKeyPressed");
	
		aav_laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
		aav_ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		aav_lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");

		aav_lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosistion");;

		aav_kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
		aav_kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		aav_ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		
		aav_kShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kShineness");

		aav_textureSamplerUniform	 = GLES32.glGetUniformLocation(shaderProgramObject, "u_texture_sampler");

		/**************Sphere*****************************/

		final float cubeVertices[] = new float[]
		{
			 0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
	        -0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
	        -0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
	        0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,

	        0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
	        0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
	        0.5f, -0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
	        0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

	        0.5f, 0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
	        -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,
	        -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
	        0.5f, -0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,

	        -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
	        -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
	        -0.5f, -0.5f, 0.5f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
	        -0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 1.0f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,

	        0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
	        -0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
	        -0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
	        0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,

	        0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,
	        -0.5f, -0.5f, -0.5f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
	        -0.5f, -0.5f, 0.5f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
	        0.5f, -0.5f, 0.5f, 1.0f, 1.0f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f
		};

		GLES32.glGenVertexArrays(1,aav_vao_cube,0);
		GLES32.glBindVertexArray(aav_vao_cube[0]);

		GLES32.glGenBuffers(1,aav_vbo_position_cube,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_vbo_position_cube[0]);

		ByteBuffer rectangleByteBuffer = ByteBuffer.allocateDirect(cubeVertices.length * 4);
		rectangleByteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer rectangleVerticesBuffer =rectangleByteBuffer.asFloatBuffer();

		rectangleVerticesBuffer.put(cubeVertices);
		rectangleVerticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							cubeVertices.length * 4,
							rectangleVerticesBuffer,
							GLES32.GL_STATIC_DRAW);

		//Vertex
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
									3,
									GLES32.GL_FLOAT,
									false,11*4,0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);


		//Color
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_COLOR,
									3,
									GLES32.GL_FLOAT,
									false,11*4,3*4);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_COLOR);

		//Normal
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_NORMAL,
									3,
									GLES32.GL_FLOAT,
									false,11*4,6*4);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_NORMAL);

		//Texture
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_TEXTURE0,
									3,
									GLES32.GL_FLOAT,
									false,11*4,9*4);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_TEXTURE0);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);


		GLES32.glBindVertexArray(0);

		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		//depth test to do
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//we will always cull back faces for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);

		aav_lightAmbiant[0] = 0.0f;
		aav_lightAmbiant[1] = 0.0f;
		aav_lightAmbiant[2] = 0.0f;
		
		aav_lightDiffuse[0] = 1.0f;
		aav_lightDiffuse[1] = 1.0f;
		aav_lightDiffuse[2] = 1.0f;

		aav_lightSpecular[0] = 1.0f;
		aav_lightSpecular[1] = 1.0f;
		aav_lightSpecular[2] = 1.0f;

		aav_lightPosition[0] = 100.0f;
		aav_lightPosition[1] = 100.0f;
		aav_lightPosition[2] = 100.0f;
		aav_lightPosition[3] = 1.0f;

		aav_materialAmbiant[0] = 0.0f;
		aav_materialAmbiant[1] = 0.0f;
		aav_materialAmbiant[2] = 0.0f;

		aav_materialDiffuse[0] = 1.0f;
		aav_materialDiffuse[1] = 1.0f;
		aav_materialDiffuse[2] = 1.0f;

		aav_materialSpecular[0] = 1.0f;
		aav_materialSpecular[1] = 1.0f;
		aav_materialSpecular[2] = 1.0f;

		aav_stone_texture[0] = LoadGLTexture(R.raw.marble);
		//Set the background color
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);

		//set projectionMatrix to identitu matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private int LoadGLTexture(int aav_imageFileResourceID)
	{
		int[] aav_texture = new int[1];
		BitmapFactory.Options aav_options = new BitmapFactory.Options();
		aav_options.inScaled = false;// 
		Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
													aav_imageFileResourceID,
													aav_options);

		GLES32.glPixelStorei(GLES32.GL_UNPACK_ALIGNMENT,4);
		GLES32.glGenTextures(1, aav_texture,0); // Gatu
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, aav_texture[0]);

		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MAG_FILTER, GLES32.GL_LINEAR);
		GLES32.glTexParameteri(GLES32.GL_TEXTURE_2D, GLES32.GL_TEXTURE_MIN_FILTER, GLES32.GL_LINEAR_MIPMAP_LINEAR);

		GLUtils.texImage2D(GLES32.GL_TEXTURE_2D,0,bitmap,0);
		GLES32.glGenerateMipmap(GLES32.GL_TEXTURE_2D);
		return(aav_texture[0]);

	}


	private void resize(int width,int height)
	{
		//code
		GLES32.glViewport(0,0,width, height);

		//orthographic projection -> left, right, bottom , top , near, far
			Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f, 100.0f);
		}

	public void display()
	{
		//code 
		GLES32.glClear((GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT));

		//use shader program
		GLES32.glUseProgram(shaderProgramObject);

		//OpenGL - ES drawing
		float aav_modelMateix[] 			= new float[16];
		float aav_viewMatrix[] 				= new float[16];

		float translateMatrix[] 			= new float[16];
		float rotationMatrix[] 				= new float[16];

		//set modelciew & modelviewprojection matrices to identity
		Matrix.setIdentityM(aav_modelMateix,0);
		Matrix.setIdentityM(aav_viewMatrix,0);
		Matrix.setIdentityM(translateMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		
		//Light Enable
		GLES32.glUniform1i(aav_LkeyPressedUniform,1);
		GLES32.glUniform3fv(aav_laUniform,1,aav_lightAmbiant,0);
		GLES32.glUniform3fv(aav_ldUniform,1,aav_lightDiffuse,0);
		GLES32.glUniform3fv(aav_lsUniform,1,aav_lightSpecular,0);
		GLES32.glUniform4fv(aav_lightPositionUniform,1,aav_lightPosition,0); // Light Position

		//matrial
		GLES32.glUniform3fv(aav_kaUniform,1,aav_materialAmbiant,0);	// ka
		GLES32.glUniform3fv(aav_kdUniform,1,aav_materialDiffuse,0);	// kd
		GLES32.glUniform3fv(aav_ksUniform,1, aav_materialSpecular,0);	//ks
		GLES32.glUniform1f(aav_kShininessUniform,128.0f);
	


		Matrix.translateM(translateMatrix,0,0.0f,0.0f,-2.0f);
		Matrix.rotateM(rotationMatrix,0,rotationAngle,1.0f,0.0f,0.0f);
		Matrix.rotateM(rotationMatrix,0,rotationAngle,0.0f,1.0f,0.0f);
		Matrix.rotateM(rotationMatrix,0,rotationAngle,0.0f,0.0f,1.0f);
		Matrix.multiplyMM(aav_modelMateix,0,translateMatrix,0,rotationMatrix,0);
		
		
		//pass above modelviewprojection matrix to the vertex shade on 'u_mvp_matrix ' shader variable 
		//who position value we alrady calcuated in iniWithFrame() by using glGetUniformLocation()
		GLES32.glUniformMatrix4fv(aav_modelMatrixUniform,1,false,aav_modelMateix,0);
		GLES32.glUniformMatrix4fv(aav_viewMatrixUniform,1,false,aav_viewMatrix,0);

		GLES32.glUniformMatrix4fv(aav_PerspectiveProjectionUniform,1,false,perspectiveProjectionMatrix,0);
		
		//Sphere Draw
		 // bind vao
		GLES32.glActiveTexture(GLES32.GL_TEXTURE0);
		GLES32.glBindTexture(GLES32.GL_TEXTURE_2D, aav_stone_texture[0]);
		GLES32.glUniform1i(aav_textureSamplerUniform, 0);

        GLES32.glBindVertexArray(aav_vao_cube[0]);
       
		//draw , either by glDrawTriangle() or glDrawArray() or glDrawElement()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,0,4); 
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,4,4); 
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,8,4); 
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,12,4); 
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,16,4); 
		GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,20,4); 
		//GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,24,4); 
        // unbind vao
        GLES32.glBindVertexArray(0);
		
		//un-use shader program
		GLES32.glUseProgram(0);

		rotationAngle = rotationAngle + 1.0f;
		if(rotationAngle > 360)
		{
			rotationAngle =  1;
		}
		
		//render/flush
		requestRender();
	}

	
	void uninitialize()
	{
		//code 
		// destroy vao
      	if(aav_vao_cube[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_vao_cube,0);
			aav_vao_cube[0] = 0;
		}


		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject!= 0)
			{
				//detech vetex shader from shaer program object 
				GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
				//delete vertex shader object
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				//detech fragment shader from shader program object 
				GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
				//delete fargement shader object 
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		//delte shader prograqm object 
		if(shaderProgramObject !=0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
