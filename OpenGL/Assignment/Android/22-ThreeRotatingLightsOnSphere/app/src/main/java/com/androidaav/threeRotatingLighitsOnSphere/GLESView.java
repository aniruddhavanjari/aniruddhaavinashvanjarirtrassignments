package com.androidaav.threeRotatingLighitsOnSphere;

import android.content.Context;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.view.MotionEvent;

import android.view.GestureDetector;

import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

import java.nio.ShortBuffer;


public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnDoubleTapListener , OnGestureListener 
{
	
	 private class Light
	{
		float aav_lightAmbiant[] = new float[3];	//la
	    float aav_lightDiffuse[] = new float[3];	//ld
	    float aav_lightSpecular[] = new float[3];	//ls

	    float aav_lightPosition[] = new float[4];
	}	

	private class Material
	{
		//Material Array
		float aav_materialAmbiant[] = new float[3];		//ka
		float aav_materialDiffuse[] = new float[3];		//kd
		float aav_materialSpecular[] = new float[3];	//ks 

		float materialShininess = 128.0f;
	}

	private final Context context;

	private GestureDetector gestureDetector;

	private int aav_vertexShaderObject_pv;
	private int aav_fragmentShaderObject_pv;
	private int aav_shaderProgramObject_pv;

	private int aav_vertexShaderObject_pf;
	private int aav_fragmentShaderObject_pf;
	private int aav_shaderProgramObject_pf;

	//vertex Shader Uniform
	private int aav_modelMatrixUniform_pv;
	private int aav_viewMatrixUniform_pv;
	private int aav_ProjectionMatrixUniform_pv;
	
	private int aav_LkeyPressedUniform;

	private int aav_laUniform_pv[] = new int[3];
	private int aav_ldUniform_pv[] = new int[3];
	private int aav_lsUniform_pv[] = new int[3];

	private int aav_lightPositionUniform_pv[] = new int[3];

	private int aav_kaUniform_pv;
	private int aav_kdUniform_pv;
	private int aav_ksUniform_pv;

	private int aav_kShininessUniform_pv;

	//fragment shader
	private int aav_viewMatrixUniform_pf;
	private int aav_modelMatrixUniform_pf;
	private int aav_ProjectionMatrixUniform_pf;

	private int aav_laUniform_pf[] = new int[3];
	private int aav_ldUniform_pf[] = new int[3];
	private int aav_lsUniform_pf[] = new int[3];
	private int aav_lightPositionUniform_pf[] = new int[3];

	private int aav_kaUniform_pf;
	private int aav_kdUniform_pf;
	private int aav_ksUniform_pf;

	private int aav_kShininessUniform_pf;
	private int aav_lKeyPressedUniform_pf;

	private boolean rotationFlag 			= false;
	private boolean aav_bLightFlag 			= true;
	private boolean aav_bPerVertex			= true;
	private boolean aav_bPerFragment		= false;
	private float aav_lightAngle1 			= 0.0f;
	private float rectangleRotationAngle	= 0.0f;

	private float perspectiveProjectionMatrix[] = new float[16]; //4x4 matrix

	//Sphere
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
    private int numElements = 0;
    private int numVertices = 0;

    //Light Array
    private float aav_lightAmbiant[] = new float[3];	//la
    private float aav_lightDiffuse[] = new float[3];	//ld
    private float aav_lightSpecular[] = new float[3];	//ls

    private float aav_lightPosition[] = new float[4];

    //Material Array
    private float aav_materialAmbiant[] = new float[3];		//ka
    private float aav_materialDiffuse[] = new float[3];		//kd
    private float aav_materialSpecular[] = new float[3];	//ks 

    private float materialShininess = 128.0f;

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context, this, null , false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
			String glesVersion = gl.glGetString(GL10.GL_VERSION);
			System.out.println("AAV: "+glesVersion);
			//get GLSL version
			String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
			System.out.println("AAV: GLSL Version = "+glslVersion);
			initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused , int width, int height)
	{
		resize(width, height);
	}


	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}


	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}


	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("AAV: "+"Double Tap");
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		if(rotationFlag == false)
		{
			rotationFlag = true;
		}
		else
		{
			rotationFlag = false;
		}
		return(true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		if (aav_bPerVertex == false)
        {
            aav_bPerFragment = false;
            aav_bPerVertex = true;
        }
        else if (aav_bPerFragment == false)
        {
            aav_bPerVertex = false;
            aav_bPerFragment = true;
        }
		System.out.println("AAV: "+"Single Tap");
		return(true);
	}

	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("AAV: "+"Long Press");
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("AAV: "+"Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override
	public void onShowPress(MotionEvent e)
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{

		//code
		 Sphere sphere=new Sphere();
		float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];

        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

		//Vertex Shader 
		//Create Shader

		aav_vertexShaderObject_pv = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		//vertes shader source code
		final String aav_vertexShaderSourcedCode_pv = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;" +
	        "precision mediump int;" +
	        "in vec4 vPosition;" +
	        "in vec3 vNormal;" +
	        "uniform mat4 u_view_matrix;" +
	        "uniform mat4 u_model_matrix;" +
	        "uniform mat4 u_projection_matrix;" +

	        "uniform int u_lKeyPressed;" +
	        "uniform vec3 u_la_pv[3];" +
	        "uniform vec3 u_ld_pv[3];" +
	        "uniform vec3 u_ls_pv[3];" +
	        "uniform vec4 u_lightPosistion_pv[3];" +
	        "uniform vec3 u_ka_pv;" +
	        "uniform vec3 u_kd_pv;" +
	        "uniform vec3 u_ks_pv;" +
	        "uniform float u_kShineness_pv;" +
	        "vec3 lightDirection[3];" +
	        "vec3 reflection_vector[3];" +
	        "vec3 ambiant[3];" +
	        "vec3 diffuse[3];" +
	        "vec3 specular[3];" +
	        "vec3 temp = vec3(0.0f,0.0f,0.0f);"+
	        "out vec3 fong_ads_light_pv;" +
	        "void main(void)" +
	        "{" +
	        "	if(u_lKeyPressed == 1)" +
	        "	{" +
	        "		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" +
	        "		vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
	        "		vec3 view_vector = normalize(-eyeCordinate.xyz);" +

	        "		for(int i = 0 ; i < 3; i++)" +
	        "		{" +
	        "			lightDirection[i] = normalize(vec3(u_lightPosistion_pv[i] - eyeCordinate));" +
	        "			reflection_vector[i] = reflect(-lightDirection[i],tranformed_normal);" +
	        "			ambiant[i] = u_la_pv[i] * u_ka_pv;" +
	        "			diffuse[i] = u_ld_pv[i] * u_kd_pv * max(dot(lightDirection[i],tranformed_normal),0.0f);" +
	        "			specular[i] = u_ls_pv[i] * u_ks_pv * pow(max(dot(reflection_vector[i],view_vector),0.0f),u_kShineness_pv);" +
	        "			fong_ads_light_pv = fong_ads_light_pv + ambiant[i] + diffuse[i] + specular[i];" +
	        "		}" +
	        "	}" +
	        "	else" +
	        "	{" +
	        "		fong_ads_light_pv = vec3(1.0f,1.0f,1.0f);" +
	        "	}" +
	        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}"
			);

		//provide source code to Vertex shader
		GLES32.glShaderSource(aav_vertexShaderObject_pv,aav_vertexShaderSourcedCode_pv);

		//compile shader and check for errors 
		GLES32.glCompileShader(aav_vertexShaderObject_pv);

		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog= null;

		GLES32.glGetShaderiv(aav_vertexShaderObject_pv, GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(aav_vertexShaderObject_pv,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(aav_vertexShaderObject_pv);
				System.out.println("AAV: Vertex Shader Complication log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment Shader 
		//Create Shader 
		aav_fragmentShaderObject_pv = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//fragment shader source code
		final String aav_fragmentShaderSourceCode_pv = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;" +
	        "in vec3 fong_ads_light_pv;" +
	        "out vec4 FragColor_pv;" +
	        "void main(void)" +
	        "{" +
	        "	FragColor_pv = vec4(fong_ads_light_pv,1.0f);" +
			"}"
			);

		//provide sorce code to Fragement shader 
		GLES32.glShaderSource(aav_fragmentShaderObject_pv, aav_fragmentShaderSourceCode_pv);

		//compiler shader and check for error 
		GLES32.glCompileShader(aav_fragmentShaderObject_pv);
		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(aav_fragmentShaderObject_pv,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(aav_fragmentShaderObject_pv,
				GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(aav_fragmentShaderObject_pv);
				System.out.println("AAV : Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Create shader program
		aav_shaderProgramObject_pv = GLES32.glCreateProgram();

		//Attach vertex shader to shader program
		GLES32.glAttachShader(aav_shaderProgramObject_pv,aav_vertexShaderObject_pv);
	

		GLES32.glAttachShader(aav_shaderProgramObject_pv,aav_fragmentShaderObject_pv);
		
		//pre-linki binding of shader  program object with vertex shader attributes
		GLES32.glBindAttribLocation(aav_shaderProgramObject_pv,GLESMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");

		GLES32.glBindAttribLocation(aav_shaderProgramObject_pv,GLESMacros.AAV_ATTRIBUTE_NORMAL,"vNormal");
		
		//link the two shader together to shader program object
		GLES32.glLinkProgram(aav_shaderProgramObject_pv);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(aav_shaderProgramObject_pv,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(aav_shaderProgramObject_pv,GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(aav_shaderProgramObject_pv);
				System.out.println("AAV : Shader Program Link Log ="+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}


		//Create Shader Program PerFragment
		/**********************************************/
		//PerFragment Shader 
		//Create Shader
		aav_vertexShaderObject_pf = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		//vertes shader source code
		final String aav_vertexShaderSourcedCode_pf = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;" +
	        "precision mediump int;" +
	        "in vec4 vPosition;" +
	        "in vec3 vNormal;" +
	        "uniform mat4 u_view_matrix;" +
	        "uniform mat4 u_model_matrix;" +
	        "uniform mat4 u_projection_matrix;" +
	        "uniform int u_lKeyPressed;" +
	        "uniform vec4 u_lightPosistion_pf[3];" +
	        "out vec3 tranformed_normal_pf;" +
	        "out vec3 lightDirection_pf[3];" +
	        "out vec3 view_vector_pf;" +
	        "void main(void)" +
	        "{" +
	        "	if(u_lKeyPressed == 1)" +
	        "	{" +
	        "		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" +
	        "		tranformed_normal_pf = (mat3(u_view_matrix* u_model_matrix) * vNormal);" +
	        "		view_vector_pf = (-eyeCordinate.xyz);" +
	        "		for(int i = 0 ; i < 3 ; i++)" +
	        "		{" +
	        "			lightDirection_pf[i] = (vec3(u_lightPosistion_pf[i] - eyeCordinate));" +
	        "		}" +
	        "	}" +
	        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}"
			);

		//provide source code to Vertex shader
		GLES32.glShaderSource(aav_vertexShaderObject_pf,aav_vertexShaderSourcedCode_pf);

		//compile shader and check for errors 
		GLES32.glCompileShader(aav_vertexShaderObject_pf);

		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;

		GLES32.glGetShaderiv(aav_vertexShaderObject_pf, GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(aav_vertexShaderObject_pf,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(aav_vertexShaderObject_pf);
				System.out.println("AAV: Vertex Shader Complication log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment Shader 
		//Create Shader 
		aav_fragmentShaderObject_pf = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//fragment shader source code
		final String aav_fragmentShaderSourceCode_pf = String.format(
			"#version 320 es"+
			"\n"+
			 "precision highp float;" +
	        "precision mediump int;" +
	        "in vec3 tranformed_normal_pf;" +
	        "in vec3 lightDirection_pf[3];" +
	        "in vec3 view_vector_pf;" +
	        "uniform int u_lKeyPressed;" +
	        "uniform vec3 u_la_pf[3];" +
	        "uniform vec3 u_ld_pf[3];" +
	        "uniform vec3 u_ls_pf[3];" +
	        "uniform vec3 u_ka_pf;" +
	        "uniform vec3 u_kd_pf;" +
	        "uniform vec3 u_ks_pf;" +
	        "uniform float u_kShineness_pf;" +

	        "vec3 normalize_tranformed_normal;" +
	        "vec3 normalize_lightDirection[3];" +
	        "vec3 normalize_view_vector;" +
	        "vec3 reflection_vector[3];" +
	        "vec3 ambiant[3];" +
	        "vec3 diffuse[3];" +
	        "vec3 specular[3];" +

	        "out vec4 FragColor_pf;" +
	        "vec3 fong_ads_light_pf;" +
	        "void main(void)" +
	        "{" +
	        "	if(u_lKeyPressed == 1)" +
	        "	{" +
	        "		normalize_tranformed_normal = normalize(tranformed_normal_pf);" +
	        "		normalize_view_vector = normalize(view_vector_pf);" +
	        "		for(int i = 0; i < 3 ; i++)" +
	        "		{" +
	        "			normalize_lightDirection[i] = normalize(lightDirection_pf[i]);" +
	        "			reflection_vector[i] = reflect(-normalize_lightDirection[i],normalize_tranformed_normal);" +
	        "			ambiant[i] = u_la_pf[i] * u_ka_pf;" +
	        "			diffuse[i] = u_ld_pf[i] * u_kd_pf * max(dot(normalize_lightDirection[i],normalize_tranformed_normal),0.0f);" +
	        "			specular[i] = u_ls_pf[i] * u_ks_pf * pow(max(dot(reflection_vector[i],normalize_view_vector),0.0f),u_kShineness_pf);" +
	        "			fong_ads_light_pf = fong_ads_light_pf + ambiant[i] + diffuse[i] + specular[i];" +
	        "		}" +
	        "	}" +
	        "	else" +
	        "	{" +
	        "		fong_ads_light_pf = vec3(1.0f,1.0f,1.0f);" +
	        "	}" +
	        "	FragColor_pf = vec4(fong_ads_light_pf,1.0f);" +
			"}"
			);

		//provide sorce code to Fragement shader 
		GLES32.glShaderSource(aav_fragmentShaderObject_pf, aav_fragmentShaderSourceCode_pf);

		//compiler shader and check for error 
		GLES32.glCompileShader(aav_fragmentShaderObject_pf);
		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(aav_fragmentShaderObject_pf,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(aav_fragmentShaderObject_pf,
				GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(aav_fragmentShaderObject_pf);
				System.out.println("AAV : Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Create shader program
		aav_shaderProgramObject_pf = GLES32.glCreateProgram();

		//Attach vertex shader to shader program
		GLES32.glAttachShader(aav_shaderProgramObject_pf,aav_vertexShaderObject_pf);
	

		GLES32.glAttachShader(aav_shaderProgramObject_pf,aav_fragmentShaderObject_pf);
		
		//pre-linki binding of shader  program object with vertex shader attributes
		GLES32.glBindAttribLocation(aav_shaderProgramObject_pf,GLESMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");

		GLES32.glBindAttribLocation(aav_shaderProgramObject_pf,GLESMacros.AAV_ATTRIBUTE_NORMAL,"vNormal");
		
		//link the two shader together to shader program object
		GLES32.glLinkProgram(aav_shaderProgramObject_pf);
		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(aav_shaderProgramObject_pf,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(aav_shaderProgramObject_pf,GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(aav_shaderProgramObject_pf);
				System.out.println("AAV : Shader Program Link Log ="+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}
		/*********************************************/

		//Get MVP uniform location
		//PerVertex Shader.
		aav_modelMatrixUniform_pv = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv,"u_model_matrix");
		aav_viewMatrixUniform_pv = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv,"u_view_matrix");
		aav_ProjectionMatrixUniform_pv = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv,"u_projection_matrix");

		aav_LkeyPressedUniform = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_lKeyPressed");
	
		 //Red Light
	    aav_laUniform_pv[0] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_la_pv[0]");
	    aav_ldUniform_pv[0] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_ld_pv[0]");
	    aav_lsUniform_pv[0] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_ls_pv[0]");
	    aav_lightPositionUniform_pv[0] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_lightPosistion_pv[0]");

	    //Green Light
	    aav_laUniform_pv[1] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_la_pv[1]");
	    aav_ldUniform_pv[1] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_ld_pv[1]");
	    aav_lsUniform_pv[1] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_ls_pv[1]");
	    aav_lightPositionUniform_pv[1] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_lightPosistion_pv[1]");

	    //Blue Light
	    aav_laUniform_pv[2] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_la_pv[2]");
	    aav_ldUniform_pv[2] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_ld_pv[2]");
	    aav_lsUniform_pv[2] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_ls_pv[2]");
	    aav_lightPositionUniform_pv[2] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_lightPosistion_pv[2]");

		 aav_kaUniform_pv = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_ka_pv");
	    aav_kdUniform_pv = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_kd_pv");
	    aav_ksUniform_pv = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_ks_pv");
			
		aav_kShininessUniform_pv = GLES32.glGetUniformLocation(aav_shaderProgramObject_pv, "u_kShineness_pv");

		//PerPixel Shader Uniforms Location
		aav_modelMatrixUniform_pf = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_model_matrix");
    	aav_viewMatrixUniform_pf = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_view_matrix");
    	aav_ProjectionMatrixUniform_pf = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_projection_matrix");

    	//Red Light
	    aav_laUniform_pf[0] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_la_pf[0]");
	    aav_ldUniform_pf[0] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_ld_pf[0]");
	    aav_lsUniform_pf[0] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_ls_pf[0]");
	    aav_lightPositionUniform_pf[0] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_lightPosistion_pf[0]");

	    //Green Light
	    aav_laUniform_pf[1] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_la_pf[1]");
	    aav_ldUniform_pf[1] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_ld_pf[1]");
	    aav_lsUniform_pf[1] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_ls_pf[1]");
	    aav_lightPositionUniform_pf[1] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_lightPosistion_pf[1]");

	    //Blue aav_light
	    aav_laUniform_pf[2] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_la_pf[2]");
	    aav_ldUniform_pf[2] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_ld_pf[2]");
	    aav_lsUniform_pf[2] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_ls_pf[2]");
	    aav_lightPositionUniform_pf[2] = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_lightPosistion_pf[2]");

    	aav_kaUniform_pf = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_ka_pf");
    	aav_kdUniform_pf = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_kd_pf");
    	aav_ksUniform_pf = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_ks_pf");

    	aav_kShininessUniform_pf = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_kShineness_pf");

    	aav_lKeyPressedUniform_pf = GLES32.glGetUniformLocation(aav_shaderProgramObject_pf, "u_lKeyPressed");


		/**************Sphere*****************************/
		// vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);
		/*************************************************/

		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		//depth test to do
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//we will always cull back faces for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);

		


		//aav_light[1] = new Light();
		


		/***************************************************************/
		/*
		aav_lightAmbiant[0] = 0.0f;
		aav_lightAmbiant[1] = 0.0f;
		aav_lightAmbiant[2] = 0.0f;
		
		aav_lightDiffuse[0] = 1.0f;
		aav_lightDiffuse[1] = 1.0f;
		aav_lightDiffuse[2] = 1.0f;

		aav_lightSpecular[0] = 1.0f;
		aav_lightSpecular[1] = 1.0f;
		aav_lightSpecular[2] = 1.0f;

		aav_lightPosition[0] = 100.0f;
		aav_lightPosition[1] = 100.0f;
		aav_lightPosition[2] = 100.0f;
		aav_lightPosition[3] = 1.0f;
		*/
		/*aav_materialAmbiant[0] = 0.0f;
		aav_materialAmbiant[1] = 0.0f;
		aav_materialAmbiant[2] = 0.0f;

		aav_materialDiffuse[0] = 1.0f;
		aav_materialDiffuse[1] = 1.0f;
		aav_materialDiffuse[2] = 1.0f;

		aav_materialSpecular[0] = 1.0f;
		aav_materialSpecular[1] = 1.0f;
		aav_materialSpecular[2] = 1.0f;
		*/
		//Set the background color
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);

		//set projectionMatrix to identitu matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width,int height)
	{
		//code
		GLES32.glViewport(0,0,width, height);

		//orthographic projection -> left, right, bottom , top , near, far
			Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f, 100.0f);
		}

	public void display()
	{
		//code 
		
		Light aav_light[]= new Light[3];
		
		//Red
		aav_light[0] = new Light();

		aav_light[0].aav_lightAmbiant[0] = 0.0f;
		aav_light[0].aav_lightAmbiant[1] = 0.0f;
		aav_light[0].aav_lightAmbiant[2] = 0.0f;


		aav_light[0].aav_lightDiffuse[0] = 1.0f;
		aav_light[0].aav_lightDiffuse[1] = 0.0f;
		aav_light[0].aav_lightDiffuse[2] = 0.0f;


		aav_light[0].aav_lightSpecular[0] = 1.0f;
		aav_light[0].aav_lightSpecular[1] = 0.0f;
		aav_light[0].aav_lightSpecular[2] = 0.0f;


		aav_light[0].aav_lightPosition[0] = 0.0f;
		aav_light[0].aav_lightPosition[1] = 0.0f;
		aav_light[0].aav_lightPosition[2] = 0.0f;
		aav_light[0].aav_lightPosition[3] = 1.0f;

		
		//Green
		aav_light[1] = new Light();
		aav_light[1].aav_lightAmbiant[0] = 0.0f;
		aav_light[1].aav_lightAmbiant[1] = 0.0f;
		aav_light[1].aav_lightAmbiant[2] = 0.0f;

		aav_light[1].aav_lightDiffuse[0] = 0.0f;
		aav_light[1].aav_lightDiffuse[1] = 1.0f;
		aav_light[1].aav_lightDiffuse[2] = 0.0f;

		aav_light[1].aav_lightSpecular[0] = 0.0f;
		aav_light[1].aav_lightSpecular[1] = 1.0f;
		aav_light[1].aav_lightSpecular[2] = 0.0f;

		aav_light[1].aav_lightPosition[0] = 0.0f;
		aav_light[1].aav_lightPosition[1] = 0.0f;
		aav_light[1].aav_lightPosition[2] = 0.0f;
		aav_light[1].aav_lightPosition[3] = 1.0f;

		//Blue
		aav_light[2] = new Light();
		aav_light[2].aav_lightAmbiant[0] = 0.0f;
		aav_light[2].aav_lightAmbiant[1] = 0.0f;
		aav_light[2].aav_lightAmbiant[2] = 0.0f;

		aav_light[2].aav_lightDiffuse[0] = 0.0f;
		aav_light[2].aav_lightDiffuse[1] = 0.0f;
		aav_light[2].aav_lightDiffuse[2] = 1.0f;

		aav_light[2].aav_lightSpecular[0] = 0.0f;
		aav_light[2].aav_lightSpecular[1] = 0.0f;
		aav_light[2].aav_lightSpecular[2] = 1.0f;

		aav_light[2].aav_lightPosition[0] = 0.0f;
		aav_light[2].aav_lightPosition[1] = 0.0f;
		aav_light[2].aav_lightPosition[2] = 0.0f;
		aav_light[2].aav_lightPosition[3] = 1.0f;

		Material material[]= new Material[1];
		material[0] = new Material();

		material[0].aav_materialAmbiant[0] = 0.0f;
		material[0].aav_materialAmbiant[1] = 0.0f;
		material[0].aav_materialAmbiant[2] = 0.0f;

		material[0].aav_materialDiffuse[0] = 1.0f;
		material[0].aav_materialDiffuse[1] = 1.0f;
		material[0].aav_materialDiffuse[2] = 1.0f;

		material[0].aav_materialSpecular[0] = 1.0f;
		material[0].aav_materialSpecular[1] = 1.0f;
		material[0].aav_materialSpecular[2] = 1.0f;


		//Rotation of Lights.
		aav_light[0].aav_lightPosition[0] = 0.0f;
		aav_light[0].aav_lightPosition[1] = 5.0f * (float)Math.sin(aav_lightAngle1);
		aav_light[0].aav_lightPosition[2] = 5 * (float)Math.cos(aav_lightAngle1);
		aav_light[0].aav_lightPosition[3] = 1.0f;

		aav_light[1].aav_lightPosition[0] = 5 * (float)Math.sin(aav_lightAngle1);
		aav_light[1].aav_lightPosition[1] = 0.0f;
		aav_light[1].aav_lightPosition[2] = 5 * (float)Math.cos(aav_lightAngle1);
		aav_light[1].aav_lightPosition[3] = 1.0f;

		aav_light[2].aav_lightPosition[0] = 5 * (float)Math.sin(aav_lightAngle1);
		aav_light[2].aav_lightPosition[1] = 5 * (float)Math.cos(aav_lightAngle1);
		aav_light[2].aav_lightPosition[2] = 0.0f;
		aav_light[2].aav_lightPosition[3] = 1.0f;


		GLES32.glClear((GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT));

		//OpenGL - ES drawing
		float aav_modelMateix[] 			= new float[16];
		float aav_viewMatrix[] 				= new float[16];

		float translateMatrix[] 			= new float[16];
		float rotationMatrix[] 				= new float[16];

		if(aav_bPerVertex == true)
		{
			//use shader program
			GLES32.glUseProgram(aav_shaderProgramObject_pv);

			//set modelciew & modelviewprojection matrices to identity
			Matrix.setIdentityM(aav_modelMateix,0);
			Matrix.setIdentityM(aav_viewMatrix,0);
			Matrix.setIdentityM(translateMatrix,0);
			Matrix.setIdentityM(rotationMatrix,0);

			if ( aav_bLightFlag == true)
			{
				//Light Enable
				GLES32.glUniform1i(aav_LkeyPressedUniform,1);
				GLES32.glUniform3fv(aav_laUniform_pv[0],1,aav_light[0].aav_lightAmbiant,0);
				GLES32.glUniform3fv(aav_ldUniform_pv[0],1,aav_light[0].aav_lightDiffuse,0);
				GLES32.glUniform3fv(aav_lsUniform_pv[0],1,aav_light[0].aav_lightSpecular,0);
				GLES32.glUniform4fv(aav_lightPositionUniform_pv[0],1,aav_light[0].aav_lightPosition,0); // Light Position

				//green
				GLES32.glUniform1i(aav_LkeyPressedUniform,1);
				GLES32.glUniform3fv(aav_laUniform_pv[1],1,aav_light[1].aav_lightAmbiant,0);
				GLES32.glUniform3fv(aav_ldUniform_pv[1],1,aav_light[1].aav_lightDiffuse,0);
				GLES32.glUniform3fv(aav_lsUniform_pv[1],1,aav_light[1].aav_lightSpecular,0);
				GLES32.glUniform4fv(aav_lightPositionUniform_pv[1],1,aav_light[1].aav_lightPosition,0); // Light Position


				//blue
				GLES32.glUniform1i(aav_LkeyPressedUniform,1);
				GLES32.glUniform3fv(aav_laUniform_pv[2],1,aav_light[2].aav_lightAmbiant,0);
				GLES32.glUniform3fv(aav_ldUniform_pv[2],1,aav_light[2].aav_lightDiffuse,0);
				GLES32.glUniform3fv(aav_lsUniform_pv[2],1,aav_light[2].aav_lightSpecular,0);
				GLES32.glUniform4fv(aav_lightPositionUniform_pv[2],1,aav_light[2].aav_lightPosition,0); // Light Position

				//matrial
				GLES32.glUniform3fv(aav_kaUniform_pv,1,material[0].aav_materialAmbiant,0);	// ka
				GLES32.glUniform3fv(aav_kdUniform_pv,1,material[0].aav_materialDiffuse,0);	// kd
				GLES32.glUniform3fv(aav_ksUniform_pv,1, material[0].aav_materialSpecular,0);	//ks
	 			GLES32.glUniform1f(aav_kShininessUniform_pv,128.0f);
			}
			else 
			{
				GLES32.glUniform1i(aav_LkeyPressedUniform,0);
			}


			Matrix.translateM(translateMatrix,0,0.0f,0.0f,-2.0f);
			Matrix.multiplyMM(aav_modelMateix,0,aav_modelMateix,0,translateMatrix,0);
			
			//pass above modelviewprojection matrix to the vertex shade on 'u_mvp_matrix ' shader variable 
			//who position value we alrady calcuated in iniWithFrame() by using glGetUniformLocation()
			GLES32.glUniformMatrix4fv(aav_modelMatrixUniform_pv,1,false,aav_modelMateix,0);
			GLES32.glUniformMatrix4fv(aav_viewMatrixUniform_pv,1,false,aav_viewMatrix,0);

			GLES32.glUniformMatrix4fv(aav_ProjectionMatrixUniform_pv,1,false,perspectiveProjectionMatrix,0);
			
			//Sphere Draw
				 // bind vao
	        GLES32.glBindVertexArray(vao_sphere[0]);
	        
	        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
	        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        	// unbind vao
        	GLES32.glBindVertexArray(0);
		
			//un-use shader program
			GLES32.glUseProgram(0);
		}

		else if(aav_bPerFragment == true)
		{
			//use shader program
			GLES32.glUseProgram(aav_shaderProgramObject_pf);

			//set modelciew & modelviewprojection matrices to identity
			Matrix.setIdentityM(aav_modelMateix,0);
			Matrix.setIdentityM(aav_viewMatrix,0);
			Matrix.setIdentityM(translateMatrix,0);
			Matrix.setIdentityM(rotationMatrix,0);

			if ( aav_bLightFlag == true)
			{
				//Light Enable
				GLES32.glUniform1i(aav_lKeyPressedUniform_pf,1);
				GLES32.glUniform3fv(aav_laUniform_pf[0],1,aav_light[0].aav_lightAmbiant,0);
				GLES32.glUniform3fv(aav_ldUniform_pf[0],1,aav_light[0].aav_lightDiffuse,0);
				GLES32.glUniform3fv(aav_lsUniform_pf[0],1,aav_light[0].aav_lightSpecular,0);
				GLES32.glUniform4fv(aav_lightPositionUniform_pf[0],1,aav_light[0].aav_lightPosition,0); // Light Position

				//Green
				GLES32.glUniform1i(aav_lKeyPressedUniform_pf,1);
				GLES32.glUniform3fv(aav_laUniform_pf[1],1,aav_light[1].aav_lightAmbiant,0);
				GLES32.glUniform3fv(aav_ldUniform_pf[1],1,aav_light[1].aav_lightDiffuse,0);
				GLES32.glUniform3fv(aav_lsUniform_pf[1],1,aav_light[1].aav_lightSpecular,0);
				GLES32.glUniform4fv(aav_lightPositionUniform_pf[1],1,aav_light[1].aav_lightPosition,0); // Light Position

				//blue
				GLES32.glUniform1i(aav_lKeyPressedUniform_pf,1);
				GLES32.glUniform3fv(aav_laUniform_pf[2],1,aav_light[2].aav_lightAmbiant,0);
				GLES32.glUniform3fv(aav_ldUniform_pf[2],1,aav_light[2].aav_lightDiffuse,0);
				GLES32.glUniform3fv(aav_lsUniform_pf[2],1,aav_light[2].aav_lightSpecular,0);
				GLES32.glUniform4fv(aav_lightPositionUniform_pf[2],1,aav_light[2].aav_lightPosition,0); // Light Position

				//matrial
				GLES32.glUniform3fv(aav_kaUniform_pf,1,material[0].aav_materialAmbiant,0);	// ka
				GLES32.glUniform3fv(aav_kdUniform_pf,1,material[0].aav_materialDiffuse,0);	// kd
				GLES32.glUniform3fv(aav_ksUniform_pf,1, material[0].aav_materialSpecular,0);	//ks
	 			GLES32.glUniform1f(aav_kShininessUniform_pf,128.0f);
			}
			else 
			{
				GLES32.glUniform1i(aav_lKeyPressedUniform_pf,0);
			}


			Matrix.translateM(translateMatrix,0,0.0f,0.0f,-2.0f);
			Matrix.multiplyMM(aav_modelMateix,0,aav_modelMateix,0,translateMatrix,0);
			
			//pass above modelviewprojection matrix to the vertex shade on 'u_mvp_matrix ' shader variable 
			//who position value we alrady calcuated in iniWithFrame() by using glGetUniformLocation()
			GLES32.glUniformMatrix4fv(aav_modelMatrixUniform_pf,1,false,aav_modelMateix,0);
			GLES32.glUniformMatrix4fv(aav_viewMatrixUniform_pf,1,false,aav_viewMatrix,0);

			GLES32.glUniformMatrix4fv(aav_ProjectionMatrixUniform_pf,1,false,perspectiveProjectionMatrix,0);
			
			//Sphere Draw
				 // bind vao
	        GLES32.glBindVertexArray(vao_sphere[0]);
	        
	        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
	        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        	// unbind vao
        	GLES32.glBindVertexArray(0);
		
			//un-use shader program
			GLES32.glUseProgram(0);
		}

		if(rotationFlag == true)
		{
			update();	
		}

		//render/flush
		requestRender();
	}

	void update()
	{
		aav_lightAngle1 = aav_lightAngle1 + 0.01f;
		if (aav_lightAngle1 > 360)
		{
			aav_lightAngle1 = 0.0f;
		}
	}

	
	void uninitialize()
	{
		//code 
		// destroy vao
        if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }

		if(aav_shaderProgramObject_pv != 0)
		{
			if(aav_vertexShaderObject_pv!= 0)
			{
				//detech vetex shader from shaer program object 
				GLES32.glDetachShader(aav_shaderProgramObject_pv,aav_vertexShaderObject_pv);
				//delete vertex shader object
				GLES32.glDeleteShader(aav_vertexShaderObject_pv);
				aav_vertexShaderObject_pv = 0;
			}

			if(aav_fragmentShaderObject_pv != 0)
			{
				//detech fragment shader from shader program object 
				GLES32.glDetachShader(aav_shaderProgramObject_pv,aav_fragmentShaderObject_pv);
				//delete fargement shader object 
				GLES32.glDeleteShader(aav_fragmentShaderObject_pv);
				aav_fragmentShaderObject_pv = 0;
			}
		}

		//delte shader prograqm object 
		if(aav_shaderProgramObject_pv !=0)
		{
			GLES32.glDeleteProgram(aav_shaderProgramObject_pv);
			aav_shaderProgramObject_pv = 0;
		}
	}
}
