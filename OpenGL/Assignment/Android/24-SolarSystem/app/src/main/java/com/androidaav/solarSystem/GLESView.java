package com.androidaav.solarSystem;

import android.content.Context;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.view.MotionEvent;

import android.view.GestureDetector;

import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

import java.nio.ShortBuffer;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnDoubleTapListener , OnGestureListener 
{
	
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] aav_vao_cube = new int[1];
	private int[] aav_vbo_position_cube = new int[1];
	private int[] aav_vbo_color_cube 	= new int[1];

	private int mvpUniform;
	private int aav_modelViewMatrixUniform; 
	private int aav_modelViewPerspectiveProjectionUniform;
	private int aav_LkeyPressedUniform;
	private int aav_ldUniform;
	private int aav_kdUniform;
	private int aav_lightPositionUniform;

	private boolean rotationFlag 			= false;
	private float rotationAngle 			= 0.0f;
	private float rotationAngle1			= 0.0f;
	private float rectangleRotationAngle	= 0.0f;

	private float aav_lightPosition[] = new float[4];

	private float perspectiveProjectionMatrix[] = new float[16]; //4x4 matrix

	//Shere
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position 	= new int[1];
    private int[] vbo_sphere_normal		= new int[1];
    private int[] vbo_sphere_element 	= new int[1];
    private int numElements = 0;
    private int numVertices = 0;

    //Matrix Array Creation
	private float matrix[][] = new float[10][16]; 
	private int stacktop = -1;
	
	private int aav_tap = 0;
	private int aav_tap_double = 0;

	private float aav_color[] = new float[3];
	private int aav_colorUniform;
	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context, this, null , false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
			String glesVersion = gl.glGetString(GL10.GL_VERSION);
			System.out.println("AAV: "+glesVersion);
			//get GLSL version
			String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
			System.out.println("AAV: GLSL Version = "+glslVersion);
			initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused , int width, int height)
	{
		resize(width, height);
	}


	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}


	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}


	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("AAV: "+"Double Tap");
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		aav_tap_double = aav_tap_double + 1;

		if(aav_tap_double > 3)
		{
			aav_tap_double = 0;
		}
		return(true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		aav_tap = aav_tap + 1;

		if(aav_tap > 3)
		{
			aav_tap = 0;
		}
		System.out.println("AAV: "+"Single Tap");
		return(true);
	}

	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("AAV: "+"Long Press");
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("AAV: "+"Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override
	public void onShowPress(MotionEvent e)
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{

		//Vertex Shader 
		//Create Shader

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		//vertes shader source code
		final String vertexShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			 "precision highp float;"+
	    	"in vec4 vPosition;"+
			"in vec3 vNormal;" +
  	  		"uniform mat4 u_modelView_matrix;"+
    		"uniform mat4 u_projection_matrix;"+
    		"uniform vec3 u_color;" +
     		"out vec3 fong_ads_light;" +
   		 	"void main(void)"+ 
    		"{"+
    		"	fong_ads_light = u_color;" +
    		"   gl_Position = u_projection_matrix *u_modelView_matrix* vPosition;"+
			"}"
			);

		//provide source code to shader
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		//compile shader and check for errors 
		GLES32.glCompileShader(vertexShaderObject);

		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog= null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("AAV: Vertex Shader Complication log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment Shader 
		//Create Shader 
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//fragment shader source code
		final String fragmentShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"vec4 color;"+
			"in vec3 fong_ads_light;" +
			"uniform int u_lKeyPressed;" +
   			"out vec4 FragColor;"+
    		"void main(void)"+
    		"{"+
    		"	color = vec4(fong_ads_light,1.0f);" +
   			"	FragColor =color;"+
			"}"
			);

		//provide sorce code to shader 
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compiler shader and check for error 
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("AAV : Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Create shader program
		shaderProgramObject = GLES32.glCreateProgram();

		//Attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
	
		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//pre-linki binding of shader  program object with vertex shader attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_NORMAL,"vNormal");
		
		//link the two shader together to shader program object
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("AAV : Shader Program Link Log ="+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get MVP uniform location
		//mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		aav_modelViewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_modelView_matrix");
		aav_modelViewPerspectiveProjectionUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_projection_matrix");

		aav_colorUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_color");	

		/**********Shere***********************/
		 Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

        // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);
		/***********Shere * End*************************/
		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		//depth test to do
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//we will always cull back faces for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);

		aav_lightPosition[0] = 0.0f;
		aav_lightPosition[1] = 0.0f;
		aav_lightPosition[2] = 2.0f;
		aav_lightPosition[3] = 1.0f;

		//Set the background color
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);

		//set projectionMatrix to identitu matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width,int height)
	{
		//code
		GLES32.glViewport(0,0,width, height);

		//orthographic projection -> left, right, bottom , top , near, far
			Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f, 100.0f);
		}

	public void display()
	{
		//code 
		GLES32.glClear((GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT));

		//use shader program
		GLES32.glUseProgram(shaderProgramObject);

		//OpenGL - ES drawing
		float modelViewMatrix[] 			= new float[16];
		float modelViewProjectionMatrix[] 	= new float[16];
		float translateMatrix[] 			= new float[16];
		float rotationMatrix[] 				= new float[16];
		float scaleMatrix[] 				= new float[16];


		aav_color[0] = 1.0f;
	    aav_color[1] = 1.0f;
	   	aav_color[2] = 0.0f;
	    GLES32.glUniform3fv(aav_colorUniform,1,aav_color,0);	


		//set modelciew & modelviewprojection matrices to identity 
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(translateMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);
		Matrix.setIdentityM(scaleMatrix,0);

	
		Matrix.translateM(translateMatrix,0,0.0f,0.0f,-4.0f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,translateMatrix,0);
		//push(modelViewMatrix);

		//Matrix.rotateM(rotationMatrix,0,90,1.0f,0.0f,0.0f);
		//Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);
		
		//push(modelViewMatrix);

		//pass above modelviewprojection matrix to the vertex shade on 'u_mvp_matrix ' shader variable 
		//who position value we alrady calcuated in iniWithFrame() by using glGetUniformLocation()
		GLES32.glUniformMatrix4fv(aav_modelViewMatrixUniform,1,false,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(aav_modelViewPerspectiveProjectionUniform,1,false,perspectiveProjectionMatrix,0);
		
		// bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES32.glBindVertexArray(0);
 		
 		//Earth
 		aav_color[0] = 0.0f;
	    aav_color[1] = 1.0f;
	   	aav_color[2] = 1.0f;
	    GLES32.glUniform3fv(aav_colorUniform,1,aav_color,0);	
 		Matrix.setIdentityM(rotationMatrix,0);
 		Matrix.setIdentityM(scaleMatrix,0);
 		Matrix.setIdentityM(translateMatrix,0);
        //pop(modelViewMatrix);
        Matrix.translateM(translateMatrix,0,-1.7f,0.0f,0.0f);
		//Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,translateMatrix,0);
		
		//Matrix.rotateM(rotationMatrix,0,-90,1.0f,0.0f,0.0f);
		//Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		Matrix.rotateM(rotationMatrix,0,rotationAngle1,0.0f,1.0f,0.0f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,translateMatrix,0);
		push(modelViewMatrix);
		Matrix.scaleM(scaleMatrix,0,-0.5f, -0.5f, -0.5f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);

		Matrix.rotateM(rotationMatrix,0,rotationAngle1,0.0f,1.0f,0.0f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);

		GLES32.glUniformMatrix4fv(aav_modelViewMatrixUniform,1,false,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(aav_modelViewPerspectiveProjectionUniform,1,false,perspectiveProjectionMatrix,0);
		
		// bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_LINES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES32.glBindVertexArray(0);
 	
        //Moon
        aav_color[0] = 1.0f;
	    aav_color[1] = 1.0f;
	   	aav_color[2] = 1.0f;
	    GLES32.glUniform3fv(aav_colorUniform,1,aav_color,0);	
 		Matrix.setIdentityM(rotationMatrix,0);
 		Matrix.setIdentityM(scaleMatrix,0);
 		Matrix.setIdentityM(translateMatrix,0);
        pop(modelViewMatrix);
        Matrix.translateM(translateMatrix,0,-1.0f,0.0f,0.0f);
		//Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,translateMatrix,0);
		
		Matrix.rotateM(rotationMatrix,0,rotationAngle1,0.0f,1.0f,0.0f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,rotationMatrix,0);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,translateMatrix,0);
		//push(modelViewMatrix);
		Matrix.scaleM(scaleMatrix,0,-0.3f, -0.3f, -0.3f);
		Matrix.multiplyMM(modelViewMatrix,0,modelViewMatrix,0,scaleMatrix,0);

		GLES32.glUniformMatrix4fv(aav_modelViewMatrixUniform,1,false,modelViewMatrix,0);

		GLES32.glUniformMatrix4fv(aav_modelViewPerspectiveProjectionUniform,1,false,perspectiveProjectionMatrix,0);
		
		// bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_LINES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES32.glBindVertexArray(0);

         //pop(modelViewMatrix);
		//un-use shader program
		GLES32.glUseProgram(0);

		if(aav_tap > 0 || aav_tap_double > 0)
		{
			update();	
		}

		//render/flush
		requestRender();
	}

	void push(float[] pushMatrix)
	{
	    if (stacktop >= 10) {
	        //return (-1);
	    }
	    stacktop = stacktop + 1;
	    for (int i = 0; i < 16; i++)
	    {
	        matrix[stacktop][i] = pushMatrix[i];
	    }
	}

	void pop(float[] popMatrix)
	{
	    if (stacktop <= -1)
	    {
	        //return (-1);
	    }
	    for (int i = 0; i < 16; i++) {
	        popMatrix[i] = matrix[stacktop][i];
	    }
	    stacktop = stacktop - 1;
	}

	void update()
	{
		if(aav_tap == 1)
		{
			rotationAngle = rotationAngle + 1.0f;
			if(rotationAngle > 360.0f)
			{
				rotationAngle = 0.0f;
			}
		}

		if(aav_tap == 2)
		{
			rotationAngle = rotationAngle - 1.0f;
			if(rotationAngle < -360.0f)
			{
				rotationAngle = 0.0f;
			}
		}

		if(aav_tap_double == 1)
		{
			rotationAngle1 = rotationAngle1 + 1.0f;
			if(rotationAngle1 > 360.0f)
			{
				rotationAngle1 = 0.0f;
			}
		}

		if(aav_tap_double == 2)
		{
			rotationAngle1 = rotationAngle1 - 1.0f;
			if(rotationAngle1 < -360.0f)
			{
				rotationAngle1 = 0.0f;
			}
		}
	}

	void uninitialize()
	{
		//code 

		 // destroy vao
        if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }


		//destroy vao 
		if(aav_vao_cube[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_vao_cube,0);
			aav_vao_cube[0] = 0;
		}

		//destroy vao
		if(aav_vbo_position_cube[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_vbo_position_cube,0);
			aav_vbo_position_cube[0] =0;
		}

		if(aav_vbo_color_cube[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_vbo_color_cube,0);
			aav_vbo_color_cube[0] =0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject!= 0)
			{
				//detech vetex shader from shaer program object 
				GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
				//delete vertex shader object
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				//detech fragment shader from shader program object 
				GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
				//delete fargement shader object 
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		//delte shader prograqm object 
		if(shaderProgramObject !=0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
