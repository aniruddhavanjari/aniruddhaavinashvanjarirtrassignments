package com.example.screentouchevent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.content.pm.ActivityInfo;

import android.view.View;


public class MainActivity extends AppCompatActivity {

	 private MyTextView myTextView ;

    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
       getWindow().getDecorView().setSystemUiVisibility
		(	View.SYSTEM_UI_FLAG_IMMERSIVE		|
			View.SYSTEM_UI_FLAG_LAYOUT_STABLE	|
			View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
												|
			View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
												|
			View.SYSTEM_UI_FLAG_FULLSCREEN
       	);


        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
       
        myTextView = new MyTextView(this);
       
        setContentView(myTextView);
        
        System.out.println("AAV:Hello RTR3 Students");
    }
    
    @Override
    protected void onResume() 
    {
    	super.onResume();
    }

    @Override
    protected void onPause() 
    {
    	super.onPause();
    }
   
}