package com.androidaav.TwoLightsOnPyramid;

import android.content.Context;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.view.MotionEvent;

import android.view.GestureDetector;

import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnDoubleTapListener , OnGestureListener 
{

	private class Light
	{
		float aav_lightAmbiant[] = new float[3];	//la
	    float aav_lightDiffuse[] = new float[3];	//ld
	    float aav_lightSpecular[] = new float[3];	//ls

	    float aav_lightPosition[] = new float[4];
	}	

	private class Material
	{
		//Material Array
		float aav_materialAmbiant[] = new float[3];		//ka
		float aav_materialDiffuse[] = new float[3];		//kd
		float aav_materialSpecular[] = new float[3];	//ks 

		float materialShininess = 128.0f;
	}	

	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int[] aav_vao_pyramid 		= new int[1];
	private int[] aav_vbo_position_pyramid 		= new int[1];
	private int[] aav_vbo_color_pyramid = new int[1];

	private int aav_modelMatrixUniform_pv;
	private int aav_viewMatrixUniform_pv;
	private int aav_ProjectionMatrixUniform_pv;
	
	private int aav_LkeyPressedUniform;

	private int aav_laUniform_pv[] = new int[2];
	private int aav_ldUniform_pv[] = new int[2];
	private int aav_lsUniform_pv[] = new int[2];

	private int aav_lightPositionUniform_pv[] = new int[2];

	private int aav_kaUniform_pv;
	private int aav_kdUniform_pv;
	private int aav_ksUniform_pv;

	private int aav_kShininessUniform_pv;

	private boolean lightFlag 			= false;
	private boolean rotationFlag 			= false;
	private float rotationAngle 			= 0.0f;
	private float rectangleRotationAngle	= 0.0f;

	private float aav_lightPosition[] = new float[4];

	private float perspectiveProjectionMatrix[] = new float[16]; //4x4 matrix

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context, this, null , false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
			String glesVersion = gl.glGetString(GL10.GL_VERSION);
			System.out.println("AAV: "+glesVersion);
			//get GLSL version
			String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
			System.out.println("AAV: GLSL Version = "+glslVersion);
			initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused , int width, int height)
	{
		resize(width, height);
	}


	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}


	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}


	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("AAV: "+"Double Tap");
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		if(rotationFlag == false)
		{
			rotationFlag = true;
		}
		else
		{
			rotationFlag = false;
		}
		return(true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("AAV: "+"Single Tap");
		if(lightFlag == false)
		{
			lightFlag = true;
		}
		else
		{
			lightFlag = false;
		}
		return(true);
	}

	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("AAV: "+"Long Press");
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("AAV: "+"Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override
	public void onShowPress(MotionEvent e)
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{

		//Vertex Shader 
		//Create Shader

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		//vertes shader source code
		final String vertexShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;" +
	        "precision mediump int;" +
	        "in vec4 vPosition;" +
	        "in vec3 vNormal;" +
	        "uniform mat4 u_view_matrix;" +
	        "uniform mat4 u_model_matrix;" +
	        "uniform mat4 u_projection_matrix;" +

	        "uniform int u_lKeyPressed;" +
	        "uniform vec3 u_la_pv[2];" +
	        "uniform vec3 u_ld_pv[2];" +
	        "uniform vec3 u_ls_pv[2];" +
	        "uniform vec4 u_lightPosistion_pv[2];" +
	        "uniform vec3 u_ka_pv;" +
	        "uniform vec3 u_kd_pv;" +
	        "uniform vec3 u_ks_pv;" +
	        "uniform float u_kShineness_pv;" +
	        "vec3 lightDirection[2];" +
	        "vec3 reflection_vector[2];" +
	        "vec3 ambiant[2];" +
	        "vec3 diffuse[2];" +
	        "vec3 specular[2];" +
	        "vec3 temp = vec3(0.0f,0.0f,0.0f);" +
	        "out vec3 fong_ads_light_pv;" +
	        "void main(void)" +
	        "{" +
	        "	if(u_lKeyPressed == 1)" +
	        "	{" +
	        "		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" +
	        "		vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" +
	        "		vec3 view_vector = normalize(-eyeCordinate.xyz);" +

	        "		for(int i = 0 ; i < 2; i++)" +
	        "		{" +
	        "			lightDirection[i] = normalize(vec3(u_lightPosistion_pv[i] - eyeCordinate));" +
	        "			reflection_vector[i] = reflect(-lightDirection[i],tranformed_normal);" +
	        "			ambiant[i] = u_la_pv[i] * u_ka_pv;" +
	        "			diffuse[i] = u_ld_pv[i] * u_kd_pv * max(dot(lightDirection[i],tranformed_normal),0.0f);" +
	        "			specular[i] = u_ls_pv[i] * u_ks_pv * pow(max(dot(reflection_vector[i],view_vector),0.0f),u_kShineness_pv);" +
	        "			fong_ads_light_pv = fong_ads_light_pv + ambiant[i] + diffuse[i] + specular[i];" +
	        "		}" +
	        "	}" +
	        "	else" +
	        "	{" +
	        "		fong_ads_light_pv = vec3(1.0f,1.0f,1.0f);" +
	        "	}" +
	        "gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}"
			);

		//provide source code to shader
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		//compile shader and check for errors 
		GLES32.glCompileShader(vertexShaderObject);

		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog= null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("AAV: Vertex Shader Complication log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment Shader 
		//Create Shader 
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//fragment shader source code
		final String fragmentShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;" +
	        "in vec3 fong_ads_light_pv;" +
	        "out vec4 FragColor_pv;" +
	        "void main(void)" +
	        "{" +
	        "	FragColor_pv = vec4(fong_ads_light_pv,1.0f);" +
			"}"
			);

		//provide sorce code to shader 
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compiler shader and check for error 
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("AAV : Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Create shader program
		shaderProgramObject = GLES32.glCreateProgram();

		//Attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
	

		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//pre-linki binding of shader  program object with vertex shader attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_NORMAL,"vNormal");
		
		//link the two shader together to shader program object
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("AAV : Shader Program Link Log ="+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get MVP uniform location
		//mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		aav_modelMatrixUniform_pv = GLES32.glGetUniformLocation(shaderProgramObject,"u_model_matrix");
		aav_viewMatrixUniform_pv = GLES32.glGetUniformLocation(shaderProgramObject,"u_view_matrix");
		aav_ProjectionMatrixUniform_pv = GLES32.glGetUniformLocation(shaderProgramObject,"u_projection_matrix");

		aav_LkeyPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lKeyPressed");
	
		 //Red Light
	    aav_laUniform_pv[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_la_pv[0]");
	    aav_ldUniform_pv[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld_pv[0]");
	    aav_lsUniform_pv[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls_pv[0]");
	    aav_lightPositionUniform_pv[0] = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosistion_pv[0]");

	    //Green Light
	    aav_laUniform_pv[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_la_pv[1]");
	    aav_ldUniform_pv[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld_pv[1]");
	    aav_lsUniform_pv[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls_pv[1]");
	    aav_lightPositionUniform_pv[1] = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosistion_pv[1]");

		 aav_kaUniform_pv = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka_pv");
	    aav_kdUniform_pv = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd_pv");
	    aav_ksUniform_pv = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks_pv");
			
		aav_kShininessUniform_pv = GLES32.glGetUniformLocation(shaderProgramObject, "u_kShineness_pv");


		// vertices , colors , shader , attribs , vbo , vao initialization
		final float pyramidVertices[] = new float[]
		{
		0.0f,0.5f,0.0f,
		-0.5f,-0.5f,0.5f,
		0.5f,-0.5f,0.5f,
	
		0.0f,0.5f,0.0f,
		0.5f,-0.5f,0.5f,
		0.5f,-0.5f,-0.5f,

		0.0f,0.5f,0.0f,
		-0.5f,-0.5f,-0.5f,
		0.5f,-0.5f,-0.5f,

		0.0f,0.5f,0.0f,
		-0.5f,-0.5f,0.5f,
		-0.5f,-0.5f,-0.5f
		};

		final float pyramidNormal[] = new float[] 
		{
		0.0f,0.447214f,0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		0.894427f,0.447214f,0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		0.0f,0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f,0.447214f,0.0f
		};

		

		GLES32.glGenVertexArrays(1,aav_vao_pyramid,0);
		GLES32.glBindVertexArray(aav_vao_pyramid[0]);

		GLES32.glGenBuffers(1,aav_vbo_position_pyramid,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_vbo_position_pyramid[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(pyramidVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer =byteBuffer.asFloatBuffer();

		verticesBuffer.put(pyramidVertices);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							pyramidVertices.length * 4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
									3,
									GLES32.GL_FLOAT,
									false,0,0);

		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);


		//Color
		GLES32.glGenBuffers(1,aav_vbo_color_pyramid,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,aav_vbo_color_pyramid[0]);

		ByteBuffer byteColorBuffer = ByteBuffer.allocateDirect(pyramidNormal.length * 4);
		byteColorBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer ColorBufferR =byteColorBuffer.asFloatBuffer();

		ColorBufferR.put(pyramidNormal);
		ColorBufferR.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
								pyramidNormal.length * 4,
								ColorBufferR,
								GLES32.GL_STATIC_DRAW);
		GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_NORMAL,
									3,
									GLES32.GL_FLOAT,
									false,0,0);
		GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_NORMAL);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);


		/***************************************************/
		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		//depth test to do
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		
		aav_lightPosition[0] = 0.0f;
		aav_lightPosition[1] = 0.0f;
		aav_lightPosition[2] = 2.0f;
		aav_lightPosition[3] = 1.0f;

		//Set the background color
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);

		//set projectionMatrix to identitu matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width,int height)
	{
		//code
		GLES32.glViewport(0,0,width, height);

		//orthographic projection -> left, right, bottom , top , near, far
			Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f, 100.0f);
		}

	public void display()
	{
		//code 

		Light aav_light[]= new Light[3];
		
		//Red
		aav_light[0] = new Light();

		aav_light[0].aav_lightAmbiant[0] = 0.0f;
		aav_light[0].aav_lightAmbiant[1] = 0.0f;
		aav_light[0].aav_lightAmbiant[2] = 0.0f;


		aav_light[0].aav_lightDiffuse[0] = 1.0f;
		aav_light[0].aav_lightDiffuse[1] = 0.0f;
		aav_light[0].aav_lightDiffuse[2] = 0.0f;


		aav_light[0].aav_lightSpecular[0] = 1.0f;
		aav_light[0].aav_lightSpecular[1] = 0.0f;
		aav_light[0].aav_lightSpecular[2] = 0.0f;


		aav_light[0].aav_lightPosition[0] = -2.0f;
		aav_light[0].aav_lightPosition[1] = 0.0f;
		aav_light[0].aav_lightPosition[2] = 0.0f;
		aav_light[0].aav_lightPosition[3] = 1.0f;

		
		//Blue
		aav_light[1] = new Light();
		aav_light[1].aav_lightAmbiant[0] = 0.0f;
		aav_light[1].aav_lightAmbiant[1] = 0.0f;
		aav_light[1].aav_lightAmbiant[2] = 0.0f;

		aav_light[1].aav_lightDiffuse[0] = 0.0f;
		aav_light[1].aav_lightDiffuse[1] = 0.0f;
		aav_light[1].aav_lightDiffuse[2] = 1.0f;

		aav_light[1].aav_lightSpecular[0] = 0.0f;
		aav_light[1].aav_lightSpecular[1] = 0.0f;
		aav_light[1].aav_lightSpecular[2] = 1.0f;

		aav_light[1].aav_lightPosition[0] = 2.0f;
		aav_light[1].aav_lightPosition[1] = 0.0f;
		aav_light[1].aav_lightPosition[2] = 0.0f;
		aav_light[1].aav_lightPosition[3] = 1.0f;

		//Materila
		Material material[]= new Material[1];
		material[0] = new Material();

		material[0].aav_materialAmbiant[0] = 0.0f;
		material[0].aav_materialAmbiant[1] = 0.0f;
		material[0].aav_materialAmbiant[2] = 0.0f;

		material[0].aav_materialDiffuse[0] = 1.0f;
		material[0].aav_materialDiffuse[1] = 1.0f;
		material[0].aav_materialDiffuse[2] = 1.0f;

		material[0].aav_materialSpecular[0] = 1.0f;
		material[0].aav_materialSpecular[1] = 1.0f;
		material[0].aav_materialSpecular[2] = 1.0f;

		GLES32.glClear((GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT));

		//use shader program
		GLES32.glUseProgram(shaderProgramObject);

		//OpenGL - ES drawing
		float aav_modelMateix[] 			= new float[16];
		float aav_viewMatrix[] 				= new float[16];

		float translateMatrix[] 			= new float[16];
		float rotationMatrix[] 				= new float[16];
		//set modelciew & modelviewprojection matrices to identity 
		Matrix.setIdentityM(aav_modelMateix,0);
		Matrix.setIdentityM(aav_viewMatrix,0);
		Matrix.setIdentityM(translateMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		if ( lightFlag == true)
		{
			//Light Enable
			GLES32.glUniform1i(aav_LkeyPressedUniform,1);
			GLES32.glUniform3fv(aav_laUniform_pv[0],1,aav_light[0].aav_lightAmbiant,0);
			GLES32.glUniform3fv(aav_ldUniform_pv[0],1,aav_light[0].aav_lightDiffuse,0);
			GLES32.glUniform3fv(aav_lsUniform_pv[0],1,aav_light[0].aav_lightSpecular,0);
			GLES32.glUniform4fv(aav_lightPositionUniform_pv[0],1,aav_light[0].aav_lightPosition,0); // Light Position

			//green
			GLES32.glUniform1i(aav_LkeyPressedUniform,1);
			GLES32.glUniform3fv(aav_laUniform_pv[1],1,aav_light[1].aav_lightAmbiant,0);
			GLES32.glUniform3fv(aav_ldUniform_pv[1],1,aav_light[1].aav_lightDiffuse,0);
			GLES32.glUniform3fv(aav_lsUniform_pv[1],1,aav_light[1].aav_lightSpecular,0);
			GLES32.glUniform4fv(aav_lightPositionUniform_pv[1],1,aav_light[1].aav_lightPosition,0); // Light Position


			//matrial
			GLES32.glUniform3fv(aav_kaUniform_pv,1,material[0].aav_materialAmbiant,0);	// ka
			GLES32.glUniform3fv(aav_kdUniform_pv,1,material[0].aav_materialDiffuse,0);	// kd
			GLES32.glUniform3fv(aav_ksUniform_pv,1, material[0].aav_materialSpecular,0);	//ks
	 		GLES32.glUniform1f(aav_kShininessUniform_pv,128.0f);
		}
		else 
		{
			GLES32.glUniform1i(aav_LkeyPressedUniform,0);
		}


		Matrix.translateM(translateMatrix,0,0.0f,0.0f,-3.6f);
		Matrix.rotateM(rotationMatrix,0,rotationAngle,0.0f,1.0f,0.0f);
		Matrix.multiplyMM(aav_modelMateix,0,translateMatrix,0,rotationMatrix,0);
	
		GLES32.glUniformMatrix4fv(aav_modelMatrixUniform_pv,1,false,aav_modelMateix,0);
		GLES32.glUniformMatrix4fv(aav_viewMatrixUniform_pv,1,false,aav_viewMatrix,0);

		GLES32.glUniformMatrix4fv(aav_ProjectionMatrixUniform_pv,1,false,perspectiveProjectionMatrix,0);
			
		//bind vao
		GLES32.glBindVertexArray(aav_vao_pyramid[0]);

		//draw , either by glDrawTriangle() or glDrawArray() or glDrawElement()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES,0,12); // 3(x,y,z) vertices in pyramidVertices array 

		//unbind vao
		GLES32.glBindVertexArray(0);
	

		//un-use shader program
		GLES32.glUseProgram(0);

		if(rotationFlag == true)
		{
			update();	
		}

		//render/flush
		requestRender();
	}

	void update()
	{
		rotationAngle = rotationAngle + 1.0f;
	}

	void uninitialize()
	{
		//code 
		//destroy vao 
		if(aav_vao_pyramid[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,aav_vao_pyramid,0);
			aav_vao_pyramid[0] = 0;
		}

		//destroy vao
		if(aav_vbo_position_pyramid[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_vbo_position_pyramid,0);
			aav_vbo_position_pyramid[0] =0;
		}

		if(aav_vbo_color_pyramid[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,aav_vbo_color_pyramid,0);
			aav_vbo_color_pyramid[0] =0;
		}

	
		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject!= 0)
			{
				//detech vetex shader from shaer program object 
				GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
				//delete vertex shader object
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				//detech fragment shader from shader program object 
				GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
				//delete fargement shader object 
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		//delte shader prograqm object 
		if(shaderProgramObject !=0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
