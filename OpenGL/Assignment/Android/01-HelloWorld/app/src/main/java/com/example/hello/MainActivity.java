package com.example.hello;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatTextView;

import android.graphics.Color;

import android.view.Gravity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        //Set the Back Ground Color of Window  
        getWindow().getDecorView().setBackgroundColor(Color.rgb(0,0,0)); //Is Method is Called "Object Channing" 
       
        AppCompatTextView myTextView = new AppCompatTextView(this);
        myTextView.setText("Hello World !!!");
        myTextView.setTextSize(32);
        myTextView.setTextColor(Color.rgb(0,255,0));
        myTextView.setGravity(Gravity.CENTER);
        myTextView.setBackgroundColor(Color.rgb(0,0,0));

        setContentView(myTextView);
        System.out.println("AAV:Hello RTR3 Students");
    }
    
    @Override
    protected void onResume() 
    {
    	super.onResume();
    }

    @Override
    protected void onPause() 
    {
    	super.onPause();
    }
   
}