package com.androidaav.geometryShader;

import android.content.Context;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.view.MotionEvent;

import android.view.GestureDetector;

import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnDoubleTapListener , OnGestureListener 
{
	
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;
	private int geometryShaderObject;

	private int[] vao = new int[1];
	private int[] vbo = new int[1];
	private int[] vbo_color = new int[1];
	private int mvpUniform;

	private float perspectiveProjectionMatrix[] = new float[16]; //4x4 matrix

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context, this, null , false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
			String glesVersion = gl.glGetString(GL10.GL_VERSION);
			System.out.println("AAV: "+glesVersion);
			//get GLSL version
			String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
			System.out.println("AAV: GLSL Version = "+glslVersion);
			initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused , int width, int height)
	{
		resize(width, height);
	}


	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}


	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}


	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("AAV: "+"Double Tap");
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("AAV: "+"Single Tap");
		return(true);
	}

	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("AAV: "+"Long Press");
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("AAV: "+"Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override
	public void onShowPress(MotionEvent e)
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}








	private void initialize(GL10 gl)
	{

		//Vertex Shader 
		//Create Shader

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		//vertes shader source code
		final String vertexShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"in vec4 vPosition;"+
			"uniform mat4 u_mvp_matrix;"+
			"void main(void)"+
			"{"+
			"	gl_Position = u_mvp_matrix * vPosition;"+
			"}"
			);

		//provide source code to shader
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		//compile shader and check for errors 
		GLES32.glCompileShader(vertexShaderObject);

		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog= null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("AAV: Vertex Shader Complication log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//GeometryShader
		geometryShaderObject = GLES32.glCreateShader(GLES32.GL_GEOMETRY_SHADER);

		//vertes shader source code
		final String geometryShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"layout(triangles)in;" +
			"layout(triangle_strip,max_vertices = 9)out;"+
			"uniform mat4 u_mvp_matrix;"+
			"void main(void)" +
			"{" +
			"	for(int i = 0 ; i < 3 ; i++)" +
			"	{"+
			"		gl_Position = u_mvp_matrix * (gl_in[i].gl_Position + vec4(0.0,1.0,0.0,0.0));"+
			"		EmitVertex();"+
			"		gl_Position = u_mvp_matrix * (gl_in[i].gl_Position + vec4(-1.0,-1.0,0.0,0.0));"+
			"		EmitVertex();" +
			"		gl_Position = u_mvp_matrix * (gl_in[i].gl_Position + vec4(1.0,-1.0,0.0,0.0));"+
			"		EmitVertex();" +
			"		EndPrimitive();"+
			"	}" +
			"}"
			);

		//provide source code to shader
		GLES32.glShaderSource(geometryShaderObject,geometryShaderSourceCode);

		//compile shader and check for errors 
		GLES32.glCompileShader(geometryShaderObject);

		iShaderCompiledStatus = new int[1];
		iInfoLogLength = new int[1];
		szInfoLog= null;

		GLES32.glGetShaderiv(geometryShaderObject, GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(geometryShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(geometryShaderObject);
				System.out.println("AAV: Geometry Shader Complication log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment Shader 
		//Create Shader 
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//fragment shader source code
		final String fragmentShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
			"out vec4 FragColor;"+
			"void main(void)"+
			"{"+
			"	FragColor = vec4(1.0,1.0,1.0,1.0);"+
			"}"
			);

		//provide sorce code to shader 
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compiler shader and check for error 
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("AAV : Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Create shader program
		shaderProgramObject = GLES32.glCreateProgram();

		//Attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
		
		GLES32.glAttachShader(shaderProgramObject,geometryShaderObject);

		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//pre-linki binding of shader  program object with vertex shader attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_VERTEX,"vPosition");

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.VDG_ATTRIBUTE_COLOR,"vColor");
		
		//link the two shader together to shader program object
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("AAV : Shader Program Link Log ="+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get MVP uniform location
		mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_mvp_matrix");

		// vertices , colors , shader , attribs , vbo , vao initialization
		final float triangleVertices[] = new float[]
		{
			0.0f,1.0f,0.0f,
			-1.0f,-1.0f,0.0f,
			1.0f,-1.0f,0.0f
		};

		

		GLES32.glGenVertexArrays(1,vao,0);
		GLES32.glBindVertexArray(vao[0]);

		GLES32.glGenBuffers(1,vbo,0);
		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo[0]);

		ByteBuffer byteBuffer = ByteBuffer.allocateDirect(triangleVertices.length * 4);
		byteBuffer.order(ByteOrder.nativeOrder());
		FloatBuffer verticesBuffer =byteBuffer.asFloatBuffer();

		verticesBuffer.put(triangleVertices);
		verticesBuffer.position(0);

		GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
							triangleVertices.length * 4,
							verticesBuffer,
							GLES32.GL_STATIC_DRAW);

		GLES32.glVertexAttribPointer(GLESMacros.VDG_ATTRIBUTE_VERTEX,
									3,
									GLES32.GL_FLOAT,
									false,0,0);

		GLES32.glEnableVertexAttribArray(GLESMacros.VDG_ATTRIBUTE_VERTEX);

		GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);

		GLES32.glBindVertexArray(0);

		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		//depth test to do
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//we will always cull back faces for better performance
		GLES32.glEnable(GLES32.GL_CULL_FACE);

		//Set the background color
		GLES32.glClearColor(0.0f,0.0f,1.0f,1.0f);

		//set projectionMatrix to identitu matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width,int height)
	{
		//code
		GLES32.glViewport(0,0,width, height);

		//orthographic projection -> left, right, bottom , top , near, far
			Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f, 100.0f);
		}

	public void display()
	{
		//code 
		GLES32.glClear((GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT));

		//use shader program
		GLES32.glUseProgram(shaderProgramObject);

		//OpenGL - ES drawing
		float modelViewMatrix[] 			= new float[16];
		float modelViewProjectionMatrix[] 	= new float[16];
		float translateMatrix[] 			= new float[16];

		//set modelciew & modelviewprojection matrices to identity 
		Matrix.setIdentityM(modelViewMatrix,0);
		Matrix.setIdentityM(modelViewProjectionMatrix,0);
		Matrix.setIdentityM(translateMatrix,0);

		Matrix.translateM(translateMatrix,0,0.0f,0.0f,-4.0f);
		Matrix.multiplyMM(modelViewMatrix,0,translateMatrix,0,modelViewMatrix,0);
		//multiply the model view and projection matrix to get modelviewprojection matrix
		Matrix.multiplyMM(modelViewProjectionMatrix,0,perspectiveProjectionMatrix,0,modelViewMatrix,0);

		//pass above modelviewprojection matrix to the vertex shade on 'u_mvp_matrix ' shader variable 
		//who position value we alrady calcuated in iniWithFrame() by using glGetUniformLocation()
		GLES32.glUniformMatrix4fv(mvpUniform,1,false,modelViewProjectionMatrix,0);
		
		//bind vao
		GLES32.glBindVertexArray(vao[0]);

		//draw , either by glDrawTriangle() or glDrawArray() or glDrawElement()
		GLES32.glDrawArrays(GLES32.GL_TRIANGLES,0,3); // 3(x,y,z) vertices in triangleVertices array 

		//unbind vao
		GLES32.glBindVertexArray(0);

		//un-use shader program
		GLES32.glUseProgram(0);

		//render/flush
		requestRender();
	}

	void uninitialize()
	{
		//code 
		//destroy vao 
		if(vao[0] != 0)
		{
			GLES32.glDeleteVertexArrays(1,vao,0);
			vao[0] = 0;
		}

		//destroy vbo
		if(vbo[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,vbo,0);
			vbo[0] =0;
		}

		if(vbo_color[0] != 0)
		{	
			GLES32.glDeleteBuffers(1,vbo_color,0);
			vbo_color[0] =0;
		}

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject!= 0)
			{
				//detech vetex shader from shaer program object 
				GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
				//delete vertex shader object
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				//detech fragment shader from shader program object 
				GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
				//delete fargement shader object 
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		//delte shader prograqm object 
		if(shaderProgramObject !=0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
