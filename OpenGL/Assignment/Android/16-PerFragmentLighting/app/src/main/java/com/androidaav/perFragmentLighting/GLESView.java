package com.androidaav.perFragmentLighting;

import android.content.Context;

import android.opengl.GLSurfaceView;

import javax.microedition.khronos.opengles.GL10;

import javax.microedition.khronos.egl.EGLConfig;

import android.opengl.GLES32;

import android.view.MotionEvent;

import android.view.GestureDetector;

import android.view.GestureDetector.OnGestureListener;
import android.view.GestureDetector.OnDoubleTapListener;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;

import java.nio.ShortBuffer;

public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer , OnDoubleTapListener , OnGestureListener 
{
	
	private final Context context;

	private GestureDetector gestureDetector;

	private int vertexShaderObject;
	private int fragmentShaderObject;
	private int shaderProgramObject;

	private int aav_modelMatrixUniform;
	private int aav_viewMatrixUniform;
	private int aav_PerspectiveProjectionUniform;
	
	private int aav_LkeyPressedUniform;

	private int aav_laUniform;
	private int aav_ldUniform;
	private int aav_lsUniform;

	private int aav_lightPositionUniform;

	private int aav_kaUniform;
	private int aav_kdUniform;
	private int aav_ksUniform;

	private int aav_kShininessUniform;

	private boolean aav_bLightFlag 			= false;
	private float rotationAngle 			= 0.0f;
	private float rectangleRotationAngle	= 0.0f;

	private float perspectiveProjectionMatrix[] = new float[16]; //4x4 matrix

	//Sphere
	private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
    private int numElements = 0;
    private int numVertices = 0;

    //Light Array
    private float aav_lightAmbiant[] = new float[3];	//la
    private float aav_lightDiffuse[] = new float[3];	//ld
    private float aav_lightSpecular[] = new float[3];	//ls

    private float aav_lightPosition[] = new float[4];

    //Material Array
    private float aav_materialAmbiant[] = new float[3];		//ka
    private float aav_materialDiffuse[] = new float[3];		//kd
    private float aav_materialSpecular[] = new float[3];	//ks 

    private float materialShininess = 128.0f;

	public GLESView(Context drawingContext)
	{
		super(drawingContext);

		context = drawingContext;

		setEGLContextClientVersion(3);

		setRenderer(this);

		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);

		gestureDetector = new GestureDetector(context, this, null , false);
		gestureDetector.setOnDoubleTapListener(this);
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{
			String glesVersion = gl.glGetString(GL10.GL_VERSION);
			System.out.println("AAV: "+glesVersion);
			//get GLSL version
			String glslVersion = gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);
			System.out.println("AAV: GLSL Version = "+glslVersion);
			initialize(gl);
	}

	@Override
	public void onSurfaceChanged(GL10 unused , int width, int height)
	{
		resize(width, height);
	}


	@Override
	public void onDrawFrame(GL10 unused)
	{
		display();
	}


	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(!gestureDetector.onTouchEvent(event))
			super.onTouchEvent(event);
		return(true);
	}


	@Override
	public boolean onDoubleTap(MotionEvent e)
	{
		System.out.println("AAV: "+"Double Tap");
		return(true);
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e)
	{
		if(aav_bLightFlag == false)
		{
			aav_bLightFlag = true;
		}
		else
		{
			aav_bLightFlag = false;
		}
		return(true);
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e)
	{
		System.out.println("AAV: "+"Single Tap");
		return(true);
	}

	
	@Override
	public boolean onDown(MotionEvent e)
	{
		return(true);
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,float velocityY)
	{
		return(true);
	}

	@Override
	public void onLongPress(MotionEvent e)
	{
		System.out.println("AAV: "+"Long Press");
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
	{
		System.out.println("AAV: "+"Scroll");
		uninitialize();
		System.exit(0);
		return(true);
	}

	@Override
	public void onShowPress(MotionEvent e)
	{

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e)
	{
		return(true);
	}

	private void initialize(GL10 gl)
	{

		//code
		 Sphere sphere=new Sphere();
		float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];

        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

		//Vertex Shader 
		//Create Shader

		vertexShaderObject = GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

		//vertes shader source code
		final String vertexShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
	    	"in vec4 vPosition;"+
			"in vec3 vNormal;" +
  	  		"uniform mat4 u_view_matrix;" +
			"uniform mat4 u_model_matrix;" +
			"uniform mat4 u_projection_matrix;" +
			"uniform mediump int u_lKeyPressed;" +
			"uniform vec4 u_lightPosistion;" +
			"out vec3 tranformed_normal;"+
			"out vec3 lightDirection;" +
			"out vec3 view_vector;" +
			"void main(void)" +
			"{" +
			"	if(u_lKeyPressed == 1)" +
			"	{"+
			"		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" +
			"		tranformed_normal = (mat3(u_view_matrix * u_model_matrix) * vNormal);" +
			"		lightDirection = (vec3(u_lightPosistion - eyeCordinate));" +
			"		view_vector = (-eyeCordinate.xyz);" +
			"	}" +
			"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" +
			"}"
			);

		//provide source code to shader
		GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);

		//compile shader and check for errors 
		GLES32.glCompileShader(vertexShaderObject);

		int[] iShaderCompiledStatus = new int[1];
		int[] iInfoLogLength = new int[1];
		String szInfoLog= null;

		GLES32.glGetShaderiv(vertexShaderObject, GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(vertexShaderObject,
				GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(vertexShaderObject);
				System.out.println("AAV: Vertex Shader Complication log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Fragment Shader 
		//Create Shader 
		fragmentShaderObject = GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

		//fragment shader source code
		final String fragmentShaderSourceCode = String.format(
			"#version 320 es"+
			"\n"+
			"precision highp float;"+
    		"in vec3 tranformed_normal;"+
    		"in vec3 lightDirection;" +
    		"in vec3 view_vector;" +
    		"uniform mediump int u_lKeyPressed;" +
    		"uniform vec3 u_la;" +
    		"uniform vec3 u_ld;" +
    		"uniform vec3 u_ls;" +

    		"uniform vec3 u_ka;" +
    		"uniform vec3 u_kd;" +
    		"uniform vec3 u_ks;" +
    		"uniform float u_kShineness;"+

    		"out vec4 FragColor;" +
    		"vec3 fong_ads_light;" +
    		"void main(void)" +
    		"{" +
    		"	if(u_lKeyPressed == 1)" +
    		"	{"+
    		"		vec3 normalize_tranformed_normal = normalize(tranformed_normal);" +
    		"		vec3 normalize_lightDirection = normalize(lightDirection);" +
    		"		vec3 normalize_view_vector = normalize(view_vector);" +
    		"		vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" +
    		"		vec3 ambiant = u_la * u_ka;" +
    		"		vec3 diffuse = u_ld * u_kd * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" +
    		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness);" +
    		"		fong_ads_light = ambiant + diffuse + specular;" +
    		"	}"+
    		"	else"+
    		"	{"+
    		"		fong_ads_light = vec3(1.0f,1.0f,1.0f);"+
    		"	}"+
    		"	FragColor = vec4(fong_ads_light,1.0f);" +
    		"}"
			);
		
		//provide sorce code to shader 
		GLES32.glShaderSource(fragmentShaderObject, fragmentShaderSourceCode);

		//compiler shader and check for error 
		GLES32.glCompileShader(fragmentShaderObject);
		iShaderCompiledStatus[0] = 0; 
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetShaderiv(fragmentShaderObject,GLES32.GL_COMPILE_STATUS,iShaderCompiledStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetShaderiv(fragmentShaderObject,
				GLES32.GL_INFO_LOG_LENGTH,iInfoLogLength,0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetShaderInfoLog(fragmentShaderObject);
				System.out.println("AAV : Fragment Shader Compilation Log = "+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Create shader program
		shaderProgramObject = GLES32.glCreateProgram();

		//Attach vertex shader to shader program
		GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);
	

		GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);
		
		//pre-linki binding of shader  program object with vertex shader attributes
		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_VERTEX,"vPosition");

		GLES32.glBindAttribLocation(shaderProgramObject,GLESMacros.AAV_ATTRIBUTE_NORMAL,"vNormal");
		
		//link the two shader together to shader program object
		GLES32.glLinkProgram(shaderProgramObject);
		int[] iShaderProgramLinkStatus = new int[1];
		iInfoLogLength[0] = 0;
		szInfoLog = null;
		GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_LINK_STATUS,iShaderProgramLinkStatus,0);
		if(iShaderCompiledStatus[0] == GLES32.GL_FALSE)
		{
			GLES32.glGetProgramiv(shaderProgramObject,GLES32.GL_INFO_LOG_LENGTH, iInfoLogLength, 0);
			if(iInfoLogLength[0] > 0)
			{
				szInfoLog = GLES32.glGetProgramInfoLog(shaderProgramObject);
				System.out.println("AAV : Shader Program Link Log ="+szInfoLog);
				uninitialize();
				System.exit(0);
			}
		}

		//Get MVP uniform location
		
		aav_modelMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_model_matrix");
		aav_viewMatrixUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_view_matrix");
		aav_PerspectiveProjectionUniform = GLES32.glGetUniformLocation(shaderProgramObject,"u_projection_matrix");

		aav_LkeyPressedUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lKeyPressed");
	
		aav_laUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_la");
		aav_ldUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ld");
		aav_lsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ls");

		aav_lightPositionUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_lightPosistion");;

		aav_kaUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ka");
		aav_kdUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kd");
		aav_ksUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_ks");
		
		aav_kShininessUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_kShineness");

		/**************Sphere*****************************/
		// vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_VERTEX,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_VERTEX);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AAV_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AAV_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);
		/*************************************************/

		//enable depth testing
		GLES32.glEnable(GLES32.GL_DEPTH_TEST);
		//depth test to do
		GLES32.glDepthFunc(GLES32.GL_LEQUAL);
		//we will always cull back faces for better performance
		//GLES32.glEnable(GLES32.GL_CULL_FACE);

		aav_lightAmbiant[0] = 0.0f;
		aav_lightAmbiant[1] = 0.0f;
		aav_lightAmbiant[2] = 0.0f;
		
		aav_lightDiffuse[0] = 1.0f;
		aav_lightDiffuse[1] = 1.0f;
		aav_lightDiffuse[2] = 1.0f;

		aav_lightSpecular[0] = 1.0f;
		aav_lightSpecular[1] = 1.0f;
		aav_lightSpecular[2] = 1.0f;

		aav_lightPosition[0] = 100.0f;
		aav_lightPosition[1] = 100.0f;
		aav_lightPosition[2] = 100.0f;
		aav_lightPosition[3] = 1.0f;

		aav_materialAmbiant[0] = 0.0f;
		aav_materialAmbiant[1] = 0.0f;
		aav_materialAmbiant[2] = 0.0f;

		aav_materialDiffuse[0] = 1.0f;
		aav_materialDiffuse[1] = 1.0f;
		aav_materialDiffuse[2] = 1.0f;

		aav_materialSpecular[0] = 1.0f;
		aav_materialSpecular[1] = 1.0f;
		aav_materialSpecular[2] = 1.0f;

		//Set the background color
		GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f);

		//set projectionMatrix to identitu matrix
		Matrix.setIdentityM(perspectiveProjectionMatrix,0);
	}

	private void resize(int width,int height)
	{
		//code
		GLES32.glViewport(0,0,width, height);

		//orthographic projection -> left, right, bottom , top , near, far
			Matrix.perspectiveM(perspectiveProjectionMatrix,0,45.0f,(float)width/(float)height,0.1f, 100.0f);
		}

	public void display()
	{
		//code 
		GLES32.glClear((GLES32.GL_COLOR_BUFFER_BIT | GLES32.GL_DEPTH_BUFFER_BIT));

		//use shader program
		GLES32.glUseProgram(shaderProgramObject);

		//OpenGL - ES drawing
		float aav_modelMateix[] 			= new float[16];
		float aav_viewMatrix[] 				= new float[16];

		float translateMatrix[] 			= new float[16];
		float rotationMatrix[] 				= new float[16];

		//set modelciew & modelviewprojection matrices to identity
		Matrix.setIdentityM(aav_modelMateix,0);
		Matrix.setIdentityM(aav_viewMatrix,0);
		Matrix.setIdentityM(translateMatrix,0);
		Matrix.setIdentityM(rotationMatrix,0);

		if ( aav_bLightFlag == true)
		{
			//Light Enable
			GLES32.glUniform1i(aav_LkeyPressedUniform,1);
			GLES32.glUniform3fv(aav_laUniform,1,aav_lightAmbiant,0);
			GLES32.glUniform3fv(aav_ldUniform,1,aav_lightDiffuse,0);
			GLES32.glUniform3fv(aav_lsUniform,1,aav_lightSpecular,0);
			GLES32.glUniform4fv(aav_lightPositionUniform,1,aav_lightPosition,0); // Light Position

			//matrial
			GLES32.glUniform3fv(aav_kaUniform,1,aav_materialAmbiant,0);	// ka
			GLES32.glUniform3fv(aav_kdUniform,1,aav_materialDiffuse,0);	// kd
			GLES32.glUniform3fv(aav_ksUniform,1, aav_materialSpecular,0);	//ks
 			GLES32.glUniform1f(aav_kShininessUniform,128.0f);
		}
		else 
		{
			GLES32.glUniform1i(aav_LkeyPressedUniform,0);
		}


		Matrix.translateM(translateMatrix,0,0.0f,0.0f,-2.0f);
		Matrix.multiplyMM(aav_modelMateix,0,aav_modelMateix,0,translateMatrix,0);
		
		//pass above modelviewprojection matrix to the vertex shade on 'u_mvp_matrix ' shader variable 
		//who position value we alrady calcuated in iniWithFrame() by using glGetUniformLocation()
		GLES32.glUniformMatrix4fv(aav_modelMatrixUniform,1,false,aav_modelMateix,0);
		GLES32.glUniformMatrix4fv(aav_viewMatrixUniform,1,false,aav_viewMatrix,0);

		GLES32.glUniformMatrix4fv(aav_PerspectiveProjectionUniform,1,false,perspectiveProjectionMatrix,0);
		
		//Sphere Draw
		 // bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);
        GLES32.glDrawElements(GLES32.GL_TRIANGLES, numElements, GLES32.GL_UNSIGNED_SHORT, 0);
        
        // unbind vao
        GLES32.glBindVertexArray(0);
		
		//un-use shader program
		GLES32.glUseProgram(0);
		
		//render/flush
		requestRender();
	}

	
	void uninitialize()
	{
		//code 
		// destroy vao
        if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }

		if(shaderProgramObject != 0)
		{
			if(vertexShaderObject!= 0)
			{
				//detech vetex shader from shaer program object 
				GLES32.glDetachShader(shaderProgramObject,vertexShaderObject);
				//delete vertex shader object
				GLES32.glDeleteShader(vertexShaderObject);
				vertexShaderObject = 0;
			}

			if(fragmentShaderObject != 0)
			{
				//detech fragment shader from shader program object 
				GLES32.glDetachShader(shaderProgramObject,fragmentShaderObject);
				//delete fargement shader object 
				GLES32.glDeleteShader(fragmentShaderObject);
				fragmentShaderObject = 0;
			}
		}

		//delte shader prograqm object 
		if(shaderProgramObject !=0)
		{
			GLES32.glDeleteProgram(shaderProgramObject);
			shaderProgramObject = 0;
		}
	}
}
