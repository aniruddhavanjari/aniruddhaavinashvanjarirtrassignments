#include <windows.h>
#include "LightingMaterialTeapot.h"
#include <stdlib.h>
#include <stdio.h>
#include <gl/gl.h>
#include <math.h>
#include <gl/glu.h>
#include "tepod.h" // File Contain the Array values of  TeaPot Normals,Texture Cordinates, Vertex 

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

#define AAV_WIN_WIDTH 800
#define AAV_WIN_HEIGHT 600

//Callback Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Gloabal Variable 
DWORD aav_dwStyle;
WINDOWPLACEMENT aav_gwpPrev = { sizeof(WINDOWPLACEMENT) };
bool aav_gbFullScreen = false;
HWND aav_ghwnd = NULL;
FILE* aav_gpFile = NULL;
bool aav_gbActiveWindow = false;
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

GLfloat aav_cubeAngle = 0.0f;
GLfloat aav_Angle = 0.0f;

//TextureCode
GLuint aav_marbel_texture;
bool aav_bTexture = false;
bool aav_bAnimation = false;


//Lighting
bool aav_bLight = false;
GLfloat aav_lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat aav_lightDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat aav_lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat aav_MaterialAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat aav_MaterialDefuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat aav_MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat aav_MaterialShinesness = 50.0f;
/////////////////////////////////////////////////////

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	void Initialize(void);
	void Display(void);

	//local Variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szAppName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	INT aav_iHeight, aav_iWidth;

	//Code
	//File IO fileOpen 
	if (fopen_s(&aav_gpFile, "LogFile.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("fopen_s Fail to Open LogFile.txt"), TEXT("Message"), MB_OK | MB_ICONEXCLAMATION);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Success to Open LogFile.txt \n");
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.lpszClassName = aav_szAppName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_iWidth = 800;
	aav_iHeight = 600;
	aav_ix = (aav_ix / 2) - (aav_iWidth / 2);
	aav_iy = (aav_iy / 2) - (aav_iHeight / 2);

	RegisterClassEx(&aav_wndclass);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szAppName,
		TEXT("Aniruddha Avinash Vanjari : LightTextureTeaPot"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		aav_iWidth,
		aav_iHeight,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	aav_ghwnd = aav_hwnd;

	Initialize();

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	//GameLoop
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
			{
				aav_bDone = true;
			}
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			if (aav_gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return((int)aav_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void Unitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		aav_gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
	{	switch (wParam)
	{
	case VK_ESCAPE:
		DestroyWindow(hwnd);
		break;
	case 0x46:
	case 0x66:
		ToggleFullScreen();
		break;
	default:
		break;
	}
	}
	break;
	case  WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
	{
		Unitialize();
		PostQuitMessage(0);
	}
	break;
	case WM_CHAR:
	{
		switch (wParam)
		{
			case 'L':
			case 'l':
			{
				if (aav_bLight == false)
				{
					aav_bLight = true;
					glEnable(GL_LIGHTING);
				}
				else
				{
					aav_bLight = false;
					glDisable(GL_LIGHTING);
				}
			}
			break;
		case 't':
		case 'T':
		{
			if (aav_bTexture == false)
			{
				aav_bTexture = true;
				glEnable(GL_TEXTURE_2D);
			}
			else
			{
				aav_bTexture = false;
				glDisable(GL_TEXTURE_2D);
			}
		}
		break;
		case 'a':
		case 'A':
		{
			if (aav_bAnimation == false)
			{
				aav_bAnimation = true;
			}
			else
			{
				aav_bAnimation = false;
			}
		}
		break;
		}
	}
	break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	MONITORINFO aav_mi = { sizeof(MONITORINFO) };

	if (aav_gbFullScreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & (WS_OVERLAPPEDWINDOW))
		{
			if (GetWindowPlacement(aav_ghwnd, &aav_gwpPrev)
				&& GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi)
				)
			{
				SetWindowLong(aav_ghwnd,
					GWL_STYLE,
					aav_dwStyle & (~WS_OVERLAPPEDWINDOW));
				SetWindowPos(aav_ghwnd,
					HWND_TOP,
					aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.top,
					aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		aav_gbFullScreen = true;

	}
	else
	{
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(aav_ghwnd, &aav_gwpPrev);
		SetWindowPos(aav_ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		aav_gbFullScreen = false;
	}
}


void Initialize(void)
{
	//Function declaration
	void ReSize(int, int);
	bool loadGLTexture(GLuint *,TCHAR []);

	//variable Declaration
	PIXELFORMATDESCRIPTOR aav_pfd;
	INT aav_iPixelFormatIndex;
	
	//Code
	aav_ghdc = GetDC(aav_ghwnd);
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cDepthBits = 32;								
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);

	if (aav_iPixelFormatIndex == 0)
	{
		fprintf(aav_gpFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(aav_ghwnd);
	}
	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == FALSE)
	{
		fprintf(aav_gpFile, "SetPixelFormat() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		fprintf(aav_gpFile, "wglCreateContext() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == FALSE)
	{
		fprintf(aav_gpFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//Loading Necessaty 
	loadGLTexture(&aav_marbel_texture,MAKEINTRESOURCE(MARBLE_BITMAP));

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//Lighting
	glLightfv(GL_LIGHT0, GL_AMBIENT, aav_lightAmbiant);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, aav_lightDefuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, aav_lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, aav_lightPosition);
	glEnable(GL_LIGHT0);

	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_MaterialAmbiant);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_MaterialDefuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_MaterialSpecular);
	glMaterialf(GL_FRONT, GL_SHININESS, aav_MaterialShinesness);

	ReSize(AAV_WIN_WIDTH, AAV_WIN_HEIGHT);
}

bool loadGLTexture(GLuint* aav_texture, TCHAR aav_resourceID[])
{
	//Variable Declaration
	bool aav_bResult = false;
	HBITMAP aav_hBitmap = NULL; // OS Image Loading
	BITMAP aav_bmp;

	//Code
	aav_hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL),
		aav_resourceID,
		IMAGE_BITMAP,
		0,
		0,
		LR_CREATEDIBSECTION);

	if (aav_hBitmap)
	{
		aav_bResult = true;
		GetObject(aav_hBitmap, sizeof(BITMAP), &aav_bmp);

		//From Here Starts OpenGL Texture Code.
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

		glGenTextures(1, aav_texture); // Gatu
		glBindTexture(GL_TEXTURE_2D, *aav_texture);

		//Setting Texture Paramter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);

		//Following will Push Data into Memory
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3,
			aav_bmp.bmWidth,
			aav_bmp.bmHeight,
			GL_BGR_EXT,
			GL_UNSIGNED_BYTE,
			aav_bmp.bmBits);
		DeleteObject(aav_hBitmap);
	}
	return aav_bResult;
}

void ReSize(int aav_iwidth, int  aav_iheight)
{
	//Code
	if (aav_iheight == 0)
		aav_iheight = 1;

	glViewport(0, 0, (GLsizei)aav_iwidth, (GLsizei)aav_iheight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)aav_iwidth / (GLfloat)aav_iheight, 0.1f, 100.0f);
}

void Display(void)
{
	//Fucntion Declaration
	void UpdateAngle();

	//Code
	glClear((GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, -1.0f);
	glRotatef(aav_Angle, 0.0f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, aav_marbel_texture);
	
	glBegin(GL_TRIANGLES);

	for (int aav_i = 0; aav_i < sizeof(face_indicies) / sizeof(face_indicies[0]); i++)
	{
		for (int aav_j = 0; aav_j < 3; aav_j++)
		{
			int aav_vertexi = face_indicies[aav_i][aav_j];
			int aav_normali = face_indicies[aav_i][aav_j + 3];
			int aav_texturei = face_indicies[aav_i][aav_j + 6];
			
			glNormal3f(normals[aav_normali][0], normals[aav_normali][1], normals[aav_normali][2]);
			glTexCoord2f(textures[aav_texturei][0], textures[aav_texturei][1]);
			glVertex3f(vertices[aav_vertexi][0],vertices[aav_vertexi][1],vertices[aav_vertexi][2]);
		}
	}
	glEnd();

	if (aav_bAnimation == true)
	{
		UpdateAngle();
	}
	SwapBuffers(aav_ghdc);
}

void UpdateAngle()
{
	aav_Angle = aav_Angle + 0.09f;
	if (aav_Angle > 360)
	{
		aav_Angle = 0.0f;
	}
}

void Unitialize(void)
{
	//Code
	if (aav_gbFullScreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);

		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(aav_ghwnd, &aav_gwpPrev);
		SetWindowPos(aav_ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);

		if (wglGetCurrentContext() == aav_ghrc)
		{
			wglMakeCurrent(NULL, NULL);
		}
		if (aav_ghrc)
		{
			wglDeleteContext(aav_ghrc);
			aav_ghrc = NULL;
		}
		if (aav_ghrc)
		{
			wglDeleteContext(aav_ghrc);
			aav_ghrc = NULL;
		}
		if (aav_ghdc)
		{
			ReleaseDC(aav_ghwnd, aav_ghdc);
			aav_ghdc = NULL;
		}
	}

	if (aav_gpFile)
	{
		fprintf(aav_gpFile, "Closing to Open LogFile.txt SuccesFull Completed \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

	if (aav_marbel_texture)
	{
		glDeleteTextures(1, &aav_marbel_texture);
	}
}


