#include <windows.h>			 // standard Window Win32
#include <stdlib.h>				// write
#include <stdio.h>			   // Statdard I/O printf 
#include <gl/gl.h>			  // OpenGL 
#include <math.h>			 // maths Fucntion  sin , cos.
#include <gl/glu.h>			// graphic utality 

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#define TRUE 	1			//True is 1
#define FALSE 	0			//False is 0

#define BUFFER_SIZE 256 	//Maximu length of string in mesh file
#define S_EQUAL 	0		//return value of strcmp when strings are equal 

///////////////////////////

#define NR_POINT_COORDS 		3	// Number of point coordinates
#define NR_TEXTURE_COORDS		2	// Number of Texture Coordinates 
#define NR_NORMAL_COORDS		3	// Number of normal coordinates 
#define NR_FACE_TOKENS			3 	// Number of enteries in face data 
#define NR_TRIANGLE_VERTICES 	3 	// Number of vertices in a  triangle 

#define FOY_ANGLE	45		// Field of view in Y direction 
#define ZNEAR 		0.1		// Distance from viewer to near plane of viewing volume 
#define ZFAR 		200.0	// Disntance from viewer to far plane viewing volume

#define VIEWPORT_BOTTOMLEFT_X	0	// X-coordinate of bottom-left point of viewport rectangle
#define VIEWPORT_BOTTOMLEFT_Y	0	// Y-coodinate of bottom-left point of viewport rectangle

#define MONKEYHEAD_X_TRANSLATE 	0.0f	// X-translation of monkeyhead
#define MONKEYHEAD_Y_TRANSLATE 	-0.0f	// Y-translation of monkeyhead
#define MONKEYHEAD_Z_TRANSLATE 	-5.0f 	// Z-translation of monkeyhead

#define MONKEYHEAD_X_SCALE_FACTOR	1.5f	// X-scale factor of monkeyhead
#define MONKEYHEAD_Y_SCALE_FACTOR	1.5f 	// Y-scale factor of monkeyhead 
#define MONKEYHEAD_Z_SCALE_FACTOR 	1.5f	// Z-scale factor of monkeyhead 

#define	START_ANGLE_POS				0.0f	// X-scale factor of monkeyhead rotation
#define END_ANGLE_POS				360.0f	// Y-sacle factor of monkeyhead rotation
#define MONKEYHEAD_ANGLE_INCREMENT	1.0f	// Increment angle for monkeyhead


//Callback Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Gloabal Variable 
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;
FILE* pgFile = NULL;
bool gbActiveWindow = false;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

/************************************************************/
// structure deinietions for vector of vector integers and vector of vector of flaoting 
//point number 
typedef struct vec_2d_int
{
	GLint** pp_arr;
	size_t size;
}vec_2d_int_t;

typedef struct vec_2d_float
{
	GLfloat** pp_arr;
	size_t size;
}vec_2d_float_t;

vec_2d_float_t* gp_vertices;
vec_2d_float_t* gp_texture;
vec_2d_float_t* gp_normals;

vec_2d_int_t* gp_face_tri;
vec_2d_int_t* gp_face_texture;
vec_2d_int_t* gp_face_normals;

FILE* g_fp_meshfile = NULL;
FILE* g_fp_logfile = NULL;

char g_line[BUFFER_SIZE];

/************************************************************/

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	void Initialize(void);
	void Display(void);

	//local Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Aniruddha");
	bool bDone = false;
	INT i_aav_y, i_aav_x;
	INT i_aav_Height, i_aav_Width;

	//Code

	//File IO fileOpen 
	if (fopen_s(&pgFile, "LogFile.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("fopen_s Fail to Open LogFile.txt"), TEXT("Message"), MB_OK | MB_ICONEXCLAMATION);
		exit(0);
	}
	else
	{
		fprintf(pgFile, "Success to Open LogFile.txt \n");
		fclose(pgFile);
	}

	//wndclass Registration
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon((HINSTANCE)NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon((HINSTANCE)NULL,IDI_APPLICATION);

	i_aav_y = GetSystemMetrics(SM_CYSCREEN);
	i_aav_x = GetSystemMetrics(SM_CXSCREEN);

	i_aav_Width = 800;
	i_aav_Height = 600;
	i_aav_x = (i_aav_x / 2) - (i_aav_Width / 2);
	i_aav_y = (i_aav_y / 2) - (i_aav_Height / 2);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Aniruddha Avinash Vanjari : MeshLoading "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		i_aav_x,
		i_aav_y,
		i_aav_Width,
		i_aav_Height,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//GameLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Display();
			}
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Fuction Declaraton
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void Unitialize(void);

	//code

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 'F':
		case 0x66:
			ToggleFullScreen();
			break;
		}
	}
	break;

	case  WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_DESTROY:
	{
		Unitialize();
		PostQuitMessage(0);
	}
	break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	//Local Variable
	MONITORINFO mi = { sizeof(MONITORINFO) };

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & (WS_OVERLAPPEDWINDOW))
		{
			if (GetWindowPlacement(ghwnd, &wpPrev)
				&& GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)
				)
			{
				SetWindowLong(ghwnd,
					GWL_STYLE,
					dwStyle & (~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}


void Initialize(void)
{
	//Function Declaration
	void ReSize(int, int);
	void LoadMeshData(void); // Load data from mesh file and populate global vectors

	//variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	INT iPixelFormatIndex;

	//Code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		fprintf(pgFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(ghwnd);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(pgFile, "SetPixelFormat() Failed\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(pgFile, "wglCreateContext() Failed\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(pgFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(ghwnd);
	}

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);


	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// Read mesh file and load global vector with appropriate data
	LoadMeshData();

	ReSize(WIN_WIDTH, WIN_HEIGHT); // warmup call to resize
}


void LoadMeshData(void)
{
	void Unitialize(void);

	vec_2d_int_t* create_vec_2d_int(void);
	vec_2d_float_t* create_vec_2d_float(void);

	void push_back_vec_2d_int(vec_2d_int_t * p_vec, int* p_arr);
	void push_back_vec_2d_float(vec_2d_float_t * p_vec, float* p_arr);

	void* xcalloc(int, size_t);


	g_fp_logfile = fopen("MonkeyLog.txt", "w");
	if (!g_fp_logfile)
	{
		Unitialize();
		exit(0);
	}

	g_fp_meshfile = fopen("MonkeyHead.OBJ", "r");
	if (!g_fp_meshfile)
	{
		Unitialize();
		exit(0);
	}

	gp_vertices = create_vec_2d_float();
	gp_texture = create_vec_2d_float();
	gp_normals = create_vec_2d_float();

	gp_face_tri = create_vec_2d_int();
	gp_face_texture = create_vec_2d_int();
	gp_face_normals = create_vec_2d_int();

	char* sep_space = " ";
	char* sep_fslash = "/";

	char* first_token = NULL;
	char* token = NULL;

	char* face_tokens[NR_FACE_TOKENS];
	int nr_tokens;

	char* token_vertex_index = NULL;
	char* token_texture_index = NULL;
	char* token_normal_index = NULL;

	while (fgets(g_line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
		first_token = strtok(g_line, sep_space);

		if (strcmp(first_token, "v") == S_EQUAL)
		{
			GLfloat* pvec_point_coord = (GLfloat*)xcalloc(3, sizeof(GLfloat));
			for (int i = 0; i != 3; i++)
			{
				pvec_point_coord[i] = atof(strtok(NULL, sep_space));
			}
			push_back_vec_2d_float(gp_vertices, pvec_point_coord);
		}
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			GLfloat* pvec_texture_coord = (GLfloat*)xcalloc(2, sizeof(GLfloat));

			for (int i = 0; i != 2; i++)
			{
				pvec_texture_coord[i] = atof(strtok(NULL, sep_space));
			}
			push_back_vec_2d_float(gp_texture, pvec_texture_coord);
		}
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			GLfloat* pvec_normal_coord = (GLfloat*)xcalloc(3, sizeof(GLfloat));

			for (int i = 0; i != 3; i++)
			{
				pvec_normal_coord[i] = atof(strtok(NULL, sep_space));
			}
			push_back_vec_2d_float(gp_normals, pvec_normal_coord);
		}
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			GLint* pvec_vertex_indices = (GLint*)xcalloc(3, sizeof(GLint));
			GLint* pvec_texture_indices = (GLint*)xcalloc(3, sizeof(GLint));
			GLint* pvec_normal_indices = (GLint*)xcalloc(3, sizeof(GLint));

			memset((void*)face_tokens, 0, 3);

			nr_tokens = 0;

			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}
			for (int i = 0; i != 3; i++)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);
				pvec_vertex_indices[i] = atoi(token_vertex_index);
				pvec_texture_indices[i] = atoi(token_texture_index);
				pvec_normal_indices[i] = atoi(token_normal_index);
			}
			push_back_vec_2d_int(gp_face_tri, pvec_vertex_indices);
			push_back_vec_2d_int(gp_face_texture, pvec_texture_indices);
			push_back_vec_2d_int(gp_face_normals, pvec_normal_indices);
		}
		memset((void*)g_line, (int)'\0', BUFFER_SIZE);
	}
	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	fprintf(g_fp_logfile, "gp_vertices->size:%zu gp_texture->size:%zu g_normals:%zu g_face_tri:%zu\n", gp_vertices->size, gp_texture->size, gp_normals->size, gp_face_tri->size);
}


void ReSize(int width, int  height)
{
	//Code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}


void Display(void)
{
	//Function Declaration
	void Cube(void);
	void Pyramid(void);

	//Variable Declaration
	static GLfloat cubeangle = 0.0f;
	static GLfloat pyramidAngle = 0.0f;

	//Code
	glClear((GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));   //8.
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.6f);
	glScalef(0.80f, 0.80f, 0.80f);
	glRotatef(cubeangle, 1.0f, 1.0f, 1.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	for (int i = 0; i != gp_face_tri->size; i++)
	{
		glBegin(GL_TRIANGLES);
		
		for (int j = 0; j != 3; j++)
		{
			int vi = gp_face_tri->pp_arr[i][j] - 1;
			glVertex3f(gp_vertices->pp_arr[vi][0], gp_vertices->pp_arr[vi][1], gp_vertices->pp_arr[vi][2]);
		}
		glEnd();
	}

	SwapBuffers(ghdc);
}

void Unitialize(void)
{
	// Clean up vector of vector of intergers and floating point numbers 
	void clean_vec_2d_int(vec_2d_int_t * *pp_vec);
	void clean_vec_2d_float(vec_2d_float_t * *pp_vec);

	//Code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);

		if (wglGetCurrentContext() == ghrc)
		{
			wglMakeCurrent(NULL, NULL);
		}
		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

	}
	
	clean_vec_2d_float(&gp_vertices);
	clean_vec_2d_float(&gp_normals);
	clean_vec_2d_float(&gp_texture);

	clean_vec_2d_int(&gp_face_tri);
	clean_vec_2d_int(&gp_face_texture);
	clean_vec_2d_int(&gp_face_normals);

	if (g_fp_logfile)
	{
		fclose(g_fp_logfile);
		g_fp_logfile = NULL;
	}
	if(pgFile)
	{
		fprintf(pgFile, "Success to Close LogFile.txt \n");
		fclose(pgFile);
	}
}

vec_2d_int_t* create_vec_2d_int(void)
{
	void* xcalloc(int nr_elements, size_t size_per_element);

	return(vec_2d_int_t*)xcalloc(1, sizeof(vec_2d_int_t));
}

vec_2d_float_t* create_vec_2d_float(void)
{
	void* xcalloc(int nr_elements, size_t size_per_element);

	return(vec_2d_float_t*)xcalloc(1, sizeof(vec_2d_float_t));
}

void push_back_vec_2d_int(vec_2d_int_t* p_vec, GLint* p_arr)
{
	void* xrealloc(void* p, size_t new_size);

	p_vec->pp_arr = (GLint**)xrealloc(p_vec->pp_arr, (p_vec->size + 1) * sizeof(int**));
	p_vec->size++;
	p_vec->pp_arr[p_vec->size - 1] = p_arr;
}

void push_back_vec_2d_float(vec_2d_float_t* p_vec, GLfloat* p_arr)
{
	void* xrealloc(void* p, size_t new_size);

	p_vec->pp_arr = (GLfloat**)xrealloc(p_vec->pp_arr, (p_vec->size + 1) * sizeof(float**));
	p_vec->size++;
	p_vec->pp_arr[p_vec->size - 1] = p_arr;
}

void clean_vec_2d_int(vec_2d_int_t** pp_vec)
{
	vec_2d_int_t* p_vec = *pp_vec;
	for (size_t i = 0; i != p_vec->size; i++)
	{
		free(p_vec->pp_arr[i]);
	}
	free(p_vec);
	*pp_vec = NULL;
}

void clean_vec_2d_float(vec_2d_float_t** pp_vec)
{
	vec_2d_float_t* p_vec = *pp_vec;
	for (size_t i = 0; i != p_vec->size; i++)
	{
		free(p_vec->pp_arr[i]);
	}
	free(p_vec);
	*pp_vec = NULL;
}

void* xcalloc(int nr_elements, size_t size_per_element)
{
	void Unitialize(void);

	void* p = calloc(nr_elements, size_per_element);

	if (!p)
	{
		fprintf(g_fp_logfile, "calloc:fatal:out of memeory\n");
		Unitialize();
	}
	//printf("xcalloc Sucess\n");
	return(p);
}

void* xrealloc(void* p, size_t new_size)
{
	void Unitialize(void);

	void* ptr = realloc(p, new_size);
	if (!ptr)
	{
		fprintf(g_fp_logfile, "realloc:fatal:out of memory\n");
		Unitialize();
	}
	return(ptr);
}


