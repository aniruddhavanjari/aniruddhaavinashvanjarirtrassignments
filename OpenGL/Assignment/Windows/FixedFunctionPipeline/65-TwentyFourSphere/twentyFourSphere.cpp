#include <windows.h>
#include "twentyFourSphere.h"
#include <stdlib.h>
#include <stdio.h>
#include <gl/gl.h>
#include <math.h>
#include <gl/glu.h>

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

#define AAV_WIN_WIDTH 800
#define AAV_WIN_HEIGHT 600
#define CHECKER_IMAGE_WIDTH 64
#define CHECKER_IMAGE_HEIGHT 64

//Callback Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Gloabal Variable 
DWORD aav_dwStyle;
WINDOWPLACEMENT aav_gwpPrev = { sizeof(WINDOWPLACEMENT) };
bool aav_gbFullScreen = false;
HWND aav_ghwnd = NULL;
FILE* aav_gpFile = NULL;
bool aav_gbActiveWindow = false;
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;
GLfloat aav_cubeangle = 0.0f;


//Light Loggle
BOOL aav_bLight = FALSE; // LIGHT ON/OFF 

//Rotation axis
GLfloat aav_angleForXRotation = 0.0f;
GLfloat aav_angleForYRotation = 0.0f;
GLfloat aav_angleForZRotation = 0.0f;

//Light
GLfloat aav_lightAmbiant[] =  { 0.0f,0.0f,0.0f,1.0f };
GLfloat aav_lightDefuse[] =   { 1.0f,1.0f,1.0f,1.0f };
GLfloat aav_lightPosition[] = { 0.0f,3.0f,3.0f,0.0f }; // Directional Light

//Light Model
GLfloat  aav_light_Model_ambiant[] = { 0.2f,0.2f,0.2f,1.0f };
GLfloat  aav_light_Model_local_viewer[] = { 0.0f };

//For Sphere
GLUquadric *aav_gpquadric[24];
//Key Press 
GLint aav_keyPressed;

//Use for Translate when fullScreen
GLfloat aav_move = 0.0f;

//fflag for Trnaslate For FullScreen ON/OFF
bool aav_fflag = false;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	void Initialize(void);
	void Display(void);

	//local Variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szAppName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	INT aav_iHeight, aav_iWidth;

	//Code
	//File IO fileOpen 
	if (fopen_s(&aav_gpFile, "LogFile.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("fopen_s Fail to Open LogFile.txt"), TEXT("Message"), MB_OK | MB_ICONEXCLAMATION);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Success to Open LogFile.txt \n");
	}

	//wndclass Registration
	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.lpszClassName = aav_szAppName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_iWidth = 800;
	aav_iHeight = 600;
	aav_ix = (aav_ix / 2) - (aav_iWidth / 2);
	aav_iy = (aav_iy / 2) - (aav_iHeight / 2);

	RegisterClassEx(&aav_wndclass);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szAppName,
		TEXT("Aniruddha Avinash Vanjari : twentyFourSphere"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		aav_iWidth,
		aav_iHeight,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	aav_ghwnd = aav_hwnd;

	Initialize();

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	//GameLoop
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
			{
				aav_bDone = true;
			}
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			if (aav_gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return((int)aav_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void Unitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		aav_gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
	{	switch (wParam)
	{
	case VK_ESCAPE:
		DestroyWindow(hwnd);
		break;
	case 0x46:
	case 0x66:
		ToggleFullScreen();
		if (aav_fflag == false)
		{
			aav_move = 3.5f;
			aav_fflag = true;
		}
		else
		{
			aav_move = 0.0f;
			aav_fflag = false;
		}
		break;
	default:
		break;
	}
	}
	break;
	case  WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_CHAR:
	{
		switch (wParam) 
		{
		case 'L':
		case 'l':
		{
			if (aav_bLight == FALSE)
			{
				aav_bLight = TRUE;
				glEnable(GL_LIGHTING);
			}
			else
			{
				aav_bLight = FALSE;
				glDisable(GL_LIGHTING);
			}
		}
		break;
		case 'x':
		case 'X':
			aav_keyPressed = 1;
			aav_angleForXRotation = 0.0f;
			break;
		case 'y':
		case 'Y':
			aav_keyPressed = 2;
			aav_angleForYRotation = 0.0f;
			break;
		case 'z':
		case 'Z':
			aav_keyPressed = 3;
			aav_angleForZRotation = 0.0f;
			break;
		}
	}
	break;
	case WM_DESTROY:
	{
		Unitialize();
		PostQuitMessage(0);
	}
	break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	MONITORINFO aav_mi = { sizeof(MONITORINFO) };

	if (aav_gbFullScreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & (WS_OVERLAPPEDWINDOW))
		{
			if (GetWindowPlacement(aav_ghwnd, &aav_gwpPrev)
				&& GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi)
				)
			{
				SetWindowLong(aav_ghwnd,
					GWL_STYLE,
					aav_dwStyle & (~WS_OVERLAPPEDWINDOW));
				SetWindowPos(aav_ghwnd,
					HWND_TOP,
					aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.top,
					aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		aav_gbFullScreen = true;

	}
	else
	{
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(aav_ghwnd, &aav_gwpPrev);
		SetWindowPos(aav_ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		aav_gbFullScreen = false;
	}
}

void Initialize(void)
{
	//Function declaration
	void ReSize(int, int);

	//variable Declaration
	PIXELFORMATDESCRIPTOR aav_pfd;
	INT aav_iPixelFormatIndex;

	//Code
	aav_ghdc = GetDC(aav_ghwnd);
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cDepthBits = 32;
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);

	if (aav_iPixelFormatIndex == 0)
	{
		fprintf(aav_gpFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(aav_ghwnd);
	}
	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == FALSE)
	{
		fprintf(aav_gpFile, "SetPixelFormat() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		fprintf(aav_gpFile, "wglCreateContext() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == FALSE)
	{
		fprintf(aav_gpFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	//Depth Statements
	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);


	glEnable(GL_AUTO_NORMAL);// For Given Geomatry Give the Normal Automatic
	glEnable(GL_NORMALIZE); //  if Normal are out of range ,MAke the Normal in range of 1 and -1

	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);


	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, aav_light_Model_ambiant);
	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER,aav_light_Model_local_viewer);
	//Light 1
	glLightfv(GL_LIGHT0, GL_AMBIENT, aav_lightAmbiant);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, aav_lightDefuse);
	glLightfv(GL_LIGHT0, GL_POSITION, aav_lightPosition);
	glEnable(GL_LIGHT0);
	

	//Creating 24 GLuquadric 
	for (int i = 0 ; i < 24 ; i++)
	{
		aav_gpquadric[i] = gluNewQuadric();
	}
	
	ReSize(AAV_WIN_WIDTH, AAV_WIN_HEIGHT);
}


void ReSize(int aav_iwidth, int  aav_iheight)
{
	//Code
	if (aav_iheight == 0)
		aav_iheight = 1;

	glViewport(0, 0, (GLsizei)aav_iwidth, (GLsizei)aav_iheight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (aav_iwidth <= aav_iheight)
	{
		glOrtho(0.0f, 15.5f, 0.0f, 15.5f * (GLfloat)aav_iheight / (GLfloat)aav_iwidth, -10.0f, 10.0f);
	}
	else
	{
		glOrtho(0.0f, 15.5f* (GLfloat)aav_iwidth / (GLfloat)aav_iheight, 0.0f, 15.5f, -10.0f, 10.0f);
	}
}

void Display(void)
{
	//fucntion declaration
	void Update(void);
	void DrawTwentyFourSphere(void);
	//Code
	glClear((GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	aav_lightPosition[0] = 0.0f;
	aav_lightPosition[1] = 3.0f;
	aav_lightPosition[2] = 3.0f;
	aav_lightPosition[3] = 0.0f;
	if (aav_keyPressed == 1)
	{
		glRotatef(aav_angleForXRotation, 1.0f,0.0f,0.0f);
		aav_lightPosition[1] = aav_angleForXRotation;
	}
	else if (aav_keyPressed == 2)
	{
		glRotatef(aav_angleForYRotation, 0.0f, 1.0f, 0.0f);
		aav_lightPosition[2] = aav_angleForYRotation;
	}
	else if(aav_keyPressed == 3)
	{
		glRotatef(aav_angleForZRotation, 0.0f, 0.0f, 1.0f);
		aav_lightPosition[0] = aav_angleForZRotation;
	}

	glLightfv(GL_LIGHT0, GL_POSITION, aav_lightPosition);

	DrawTwentyFourSphere();
	
	Update();
	//Setting the move variable 
	if (aav_fflag == false)
		aav_move = 3.5f;
	else
		aav_move = 0.0f;
	SwapBuffers(aav_ghdc);
}

void DrawTwentyFourSphere(void)
{

	//local variable declaration
	GLfloat aav_materialAmbiant[4];
	GLfloat aav_materialDefuse[4];
	GLfloat aav_materialSpecular[4];
	GLfloat aav_materialShineness;

	//code
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	//1st Sppere in 1st Column , emerald 
	aav_materialAmbiant[0] = 0.0215f;
	aav_materialAmbiant[1] = 0.1745f;
	aav_materialAmbiant[2] = 0.0215f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT,GL_AMBIENT,aav_materialAmbiant);

	aav_materialDefuse[0] = 0.07568f;
	aav_materialDefuse[1] = 0.61424f;
	aav_materialDefuse[2] = 0.07568f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.633f;
	aav_materialSpecular[1] = 0.727811f;
	aav_materialSpecular[2] = 0.633f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.6f * 128.0f;
	glMaterialf(GL_FRONT,GL_SHININESS,aav_materialShineness);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 14.0f, 0.0f);
	gluSphere(aav_gpquadric[0], 1.0f, 30, 30);

	//2nd Sphere in 1st column, jade 
	aav_materialAmbiant[0] = 0.135f;
	aav_materialAmbiant[1] = 0.2225f;
	aav_materialAmbiant[2] = 0.1575f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.54f;
	aav_materialDefuse[1] = 0.89f;
	aav_materialDefuse[2] = 0.63f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.316228f;
	aav_materialSpecular[1] = 0.316228f;
	aav_materialSpecular[2] = 0.316228f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 11.5f, 0.0f);
	gluSphere(aav_gpquadric[1], 1.0f, 30, 30);
	
	//3nd Sphere in 1st column, obsidian
	aav_materialAmbiant[0] = 0.05375f;
	aav_materialAmbiant[1] = 0.05f;
	aav_materialAmbiant[2] = 0.06625f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.18275f;
	aav_materialDefuse[1] = 0.17f;
	aav_materialDefuse[2] = 0.22525f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.332741f;
	aav_materialSpecular[1] = 0.328634f;
	aav_materialSpecular[2] = 0.346435f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.3f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 9.0f, 0.0f);
	gluSphere(aav_gpquadric[2], 1.0f, 30, 30);

	//4th Sphere in 1st column, pearl
	aav_materialAmbiant[0] = 0.25f;
	aav_materialAmbiant[1] = 0.20725f;
	aav_materialAmbiant[2] = 0.20725f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 1.0f;
	aav_materialDefuse[1] = 0.829f;
	aav_materialDefuse[2] = 0.829f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.296648f;
	aav_materialSpecular[1] = 0.296648f;
	aav_materialSpecular[2] = 0.296648f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.088f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 6.5f, 0.0f);
	gluSphere(aav_gpquadric[3], 1.0f, 30, 30);

	//5th sphere in 1st column ,ruby 
	aav_materialAmbiant[0] = 0.1745f;
	aav_materialAmbiant[1] = 0.01175f;
	aav_materialAmbiant[2] = 0.01175f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.61424f;
	aav_materialDefuse[1] = 0.04136f;
	aav_materialDefuse[2] = 0.04136f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.727811f;
	aav_materialSpecular[1] = 0.626959f;
	aav_materialSpecular[2] = 0.626959f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 4.0f, 0.0f);
	gluSphere(aav_gpquadric[4], 1.0f, 30, 30);

	////6th sphere in 1st column , turquoise
	aav_materialAmbiant[0] = 0.1f;
	aav_materialAmbiant[1] = 0.18725f;
	aav_materialAmbiant[2] = 0.1745f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.396f;
	aav_materialDefuse[1] = 0.74151f;
	aav_materialDefuse[2] = 0.69102f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.297254f;
	aav_materialSpecular[1] = 0.30829f;
	aav_materialSpecular[2] = 0.306678f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(1.5f + aav_move, 1.5f, 0.0f);
	gluSphere(aav_gpquadric[5], 1.0f, 30, 30);

	//1st sphere on 2nd column, brass////////////////////////////////////////////////////////////	
	aav_materialAmbiant[0] = 0.329412f;
	aav_materialAmbiant[1] = 0.223529f;
	aav_materialAmbiant[2] = 0.027451f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.780398f;
	aav_materialDefuse[1] = 0.568627f;
	aav_materialDefuse[2] = 0.113725f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.992157f;
	aav_materialSpecular[1] = 0.947776f;
	aav_materialSpecular[2] = 0.807843f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.21794872f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 14.0f, 0.0f);
	gluSphere(aav_gpquadric[6], 1.0f, 30, 30);


	//2nd sphere on 2nd column,bronze
	aav_materialAmbiant[0] = 0.2425f;
	aav_materialAmbiant[1] = 0.1275f;
	aav_materialAmbiant[2] = 0.054f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.714f;
	aav_materialDefuse[1] = 0.4284f;
	aav_materialDefuse[2] = 0.18144f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.393548f;
	aav_materialSpecular[1] = 0.271906f;
	aav_materialSpecular[2] = 0.166721f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.2f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 11.5f, 0.0f);
	gluSphere(aav_gpquadric[7], 1.0f, 30, 30);

	//3rd sphere on 2rd column,chrome
	aav_materialAmbiant[0] = 0.25f;
	aav_materialAmbiant[1] = 0.25f;
	aav_materialAmbiant[2] = 0.25f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.4f;
	aav_materialDefuse[1] = 0.4f;
	aav_materialDefuse[2] = 0.4f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.774597f;
	aav_materialSpecular[1] = 0.774597f;
	aav_materialSpecular[2] = 0.774597f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 9.0f, 0.0f);
	gluSphere(aav_gpquadric[8], 1.0f, 30, 30); 

	//4th sphere on 2nd column, copper
	aav_materialAmbiant[0] = 0.19125f;
	aav_materialAmbiant[1] = 0.0735f;
	aav_materialAmbiant[2] = 0.0225f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.7038f;
	aav_materialDefuse[1] = 0.27048f;
	aav_materialDefuse[2] = 0.0828f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.256777f;
	aav_materialSpecular[1] = 0.137622f;
	aav_materialSpecular[2] = 0.086014f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 6.5f, 0.0f);
	gluSphere(aav_gpquadric[9], 1.0f, 30, 30); 

	//5th Sphere on 2nd Column, gold
	aav_materialAmbiant[0] = 0.24725f;
	aav_materialAmbiant[1] = 0.1995f;
	aav_materialAmbiant[2] = 0.0745f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.75164f;
	aav_materialDefuse[1] = 0.60648f;
	aav_materialDefuse[2] = 0.22648f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.628281f;
	aav_materialSpecular[1] = 0.555802f;
	aav_materialSpecular[2] = 0.366065f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.4f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 4.0f, 0.0f);
	gluSphere(aav_gpquadric[10], 1.0f, 30, 30);

	//6th Sphere on 2nd Column , silver
	aav_materialAmbiant[0] = 0.19225f;
	aav_materialAmbiant[1] = 0.19225f;
	aav_materialAmbiant[2] = 0.19225f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.50754f;
	aav_materialDefuse[1] = 0.50754f;
	aav_materialDefuse[2] = 0.50754f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.508273f;
	aav_materialSpecular[1] = 0.508273f;
	aav_materialSpecular[2] = 0.508273f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.4f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(9.5f, 1.5f, 0.0f);
	gluSphere(aav_gpquadric[11], 1.0f, 30, 30);


	//1st sphere on 3nd column,black//////////////////////////////////////////////////////////////////////////////////////////
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.01f;
	aav_materialDefuse[1] = 0.01f;
	aav_materialDefuse[2] = 0.01f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.50f;
	aav_materialSpecular[1] = 0.50f;
	aav_materialSpecular[2] = 0.50f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 14.0f, 0.0f);
	gluSphere(aav_gpquadric[12], 1.0f, 30, 30);

	//2nd Sphere on 3nd column,cyan
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.1f;
	aav_materialAmbiant[2] = 0.06f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.0f;
	aav_materialDefuse[1] = 0.50980392f;
	aav_materialDefuse[2] = 0.50980392f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.50196078f;
	aav_materialSpecular[1] = 0.50196078f;
	aav_materialSpecular[2] = 0.50196078f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 11.5f, 0.0f);
	gluSphere(aav_gpquadric[13], 1.0f, 30, 30);

	
	//3rd Sphere on 3th column ,green
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.1f;
	aav_materialDefuse[1] = 0.35f;
	aav_materialDefuse[2] = 0.1f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.45f;
	aav_materialSpecular[1] = 0.55f;
	aav_materialSpecular[2] = 0.45f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 9.0f, 0.0f);
	gluSphere(aav_gpquadric[14], 1.0f, 30, 30);

	//4th Sphere on 3rd ,red
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.5f;
	aav_materialDefuse[1] = 0.0f;
	aav_materialDefuse[2] = 0.0f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.7f;
	aav_materialSpecular[1] = 0.6f;
	aav_materialSpecular[2] = 0.6f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 6.5f, 0.0f);
	gluSphere(aav_gpquadric[15], 1.0f, 30, 30);

	//5th Sphere on 3rd , white
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.55f;
	aav_materialDefuse[1] = 0.55f;
	aav_materialDefuse[2] = 0.55f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.70f;
	aav_materialSpecular[1] = 0.70f;
	aav_materialSpecular[2] = 0.70f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 4.0f, 0.0f);
	gluSphere(aav_gpquadric[16], 1.0f, 30, 30);

	//6th Sphere on 3rd ,yello plastic 
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.5f;
	aav_materialDefuse[1] = 0.5f;
	aav_materialDefuse[2] = 0.0f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.60f;
	aav_materialSpecular[1] = 0.60f;
	aav_materialSpecular[2] = 0.50f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(17.5f - aav_move, 1.5f, 0.0f);
	gluSphere(aav_gpquadric[17], 1.0f, 30, 30);

	//Translste Setting for last Column 
	if (aav_fflag == false)
		aav_move = aav_move + 3.5f;
	else
		aav_move = 0.0f;
	//1st sphere on 4th column,black////////////////////////////////////////////////////////////////////////////////////////////
	aav_materialAmbiant[0] = 0.02f;
	aav_materialAmbiant[1] = 0.02f;
	aav_materialAmbiant[2] = 0.02f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.01f;
	aav_materialDefuse[1] = 0.01f;
	aav_materialDefuse[2] = 0.01f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.4f;
	aav_materialSpecular[1] = 0.4f;
	aav_materialSpecular[2] = 0.4f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 14.0f, 0.0f);
	gluSphere(aav_gpquadric[18], 1.0f, 30, 30);


	//2nd Shpere on 4th column,cyan
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.05f;
	aav_materialAmbiant[2] = 0.05f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.4f;
	aav_materialDefuse[1] = 0.5f;
	aav_materialDefuse[2] = 0.5f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.04f;
	aav_materialSpecular[1] = 0.7f;
	aav_materialSpecular[2] = 0.7f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 11.5f, 0.0f);
	gluSphere(aav_gpquadric[19], 1.0f, 30, 30);

	//3rd Sphere on 4th column , green
	aav_materialAmbiant[0] = 0.0f;
	aav_materialAmbiant[1] = 0.05f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.4f;
	aav_materialDefuse[1] = 0.5f;
	aav_materialDefuse[2] = 0.4f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.04f;
	aav_materialSpecular[1] = 0.7f;
	aav_materialSpecular[2] = 0.04f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 9.0f, 0.0f);
	gluSphere(aav_gpquadric[20], 1.0f, 30, 30);

	//4th Sphere on 4th column, red
	aav_materialAmbiant[0] = 0.05f;
	aav_materialAmbiant[1] = 0.0f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.5f;
	aav_materialDefuse[1] = 0.4f;
	aav_materialDefuse[2] = 0.4f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.7f;
	aav_materialSpecular[1] = 0.04f;
	aav_materialSpecular[2] = 0.04f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 6.5f, 0.0f);
	gluSphere(aav_gpquadric[21], 1.0f, 30, 30);

	//5th Sphere on 4th column, white
	aav_materialAmbiant[0] = 0.05f;
	aav_materialAmbiant[1] = 0.05f;
	aav_materialAmbiant[2] = 0.05f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.5f;
	aav_materialDefuse[1] = 0.5f;
	aav_materialDefuse[2] = 0.5f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.7f;
	aav_materialSpecular[1] = 0.7f;
	aav_materialSpecular[2] = 0.7f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 4.0f, 0.0f);
	gluSphere(aav_gpquadric[22], 1.0f, 30, 30);

	//6th Sphere on 4th column , yello rubber
	aav_materialAmbiant[0] = 0.05f;
	aav_materialAmbiant[1] = 0.05f;
	aav_materialAmbiant[2] = 0.0f;
	aav_materialAmbiant[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, aav_materialAmbiant);

	aav_materialDefuse[0] = 0.5f;
	aav_materialDefuse[1] = 0.5f;
	aav_materialDefuse[2] = 0.4f;
	aav_materialDefuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, aav_materialDefuse);

	aav_materialSpecular[0] = 0.7f;
	aav_materialSpecular[1] = 0.7f;
	aav_materialSpecular[2] = 0.04f;
	aav_materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, aav_materialSpecular);

	aav_materialShineness = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, aav_materialShineness);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(25.5f - aav_move, 1.5f, 0.0f);
	gluSphere(aav_gpquadric[23], 1.0f, 30, 30);

	//End of Fucntion
}

void Update(void)
{
	if (aav_keyPressed == 1)
	{
		aav_angleForXRotation = aav_angleForXRotation + 0.5f;
	}
	else if (aav_keyPressed == 2)
	{
		aav_angleForYRotation = aav_angleForYRotation + 0.5f;
	}
	else if (aav_keyPressed == 3)
	{
		aav_angleForZRotation = aav_angleForZRotation + 0.5f;
	}
}
  
void Unitialize(void)
{
	//Code
	if (aav_gbFullScreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);

		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(aav_ghwnd, &aav_gwpPrev);
		SetWindowPos(aav_ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);

		if (wglGetCurrentContext() == aav_ghrc)
		{
			wglMakeCurrent(NULL, NULL);
		}
		if (aav_ghrc)
		{
			wglDeleteContext(aav_ghrc);
			aav_ghrc = NULL;
		}
		if (aav_ghrc)
		{
			wglDeleteContext(aav_ghrc);
			aav_ghrc = NULL;
		}
		if (aav_ghdc)
		{
			ReleaseDC(aav_ghwnd, aav_ghdc);
			aav_ghdc = NULL;
		}

	}
	
	//Deleteing 24 quadric that we have create in Initialize
	for (int i = 0; i < 24; i++)
	{
		gluDeleteQuadric(aav_gpquadric[i]);
	}

	if (aav_gpFile)
	{
		fprintf(aav_gpFile, "Closing to Open LogFile.txt SuccesFull Completed \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

}


