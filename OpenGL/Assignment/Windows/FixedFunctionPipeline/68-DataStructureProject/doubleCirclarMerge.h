#define TRUE  1
#define FALSE 0
#define SUCCESS 1
#define FAILURE 0
#define LIST_DATA_NOT_FOUND 2
#define LIST_EMPTY 3

typedef int data_t;
typedef int status_t;
typedef int len_c;
typedef struct node node_t;
typedef struct treeBranch node_tree;
typedef node_t list_t;

//tree
typedef float len_t;
typedef int data_b;
typedef float len_b;
typedef int status_b;

/*node layout definition*/
struct node {
	data_t data;
	len_b branchLength;
	len_b leavesLeft;
	len_b leavesRight;
	struct node* prev;
	struct node* next;
};

struct treeBranch {
	data_t rotation;
	data_t branches;
	len_t branchTotalLength;
	data_t leaves;
	node_t* head;
};

extern FILE* aav_gpFile;

/*interface routines declarations*/
list_t* create_list(void);
node_tree* create_treehead(void);

status_t insert_beg(list_t* p_list, len_b new_data);
status_t insert_end(list_t* p_list, len_b new_data);
void show_list(list_t* p_list, const char* msg);

status_t destroy_list(list_t** pp_list);

//tree
node_tree* gettreeHeadNode(void);

/*List auxillary routines*/
void generic_insert(node_t* p_beg, node_t* p_mid, node_t* p_end);
node_t* get_list_node(len_b new_data);

/*memory allocation routines*/
void* xmalloc(size_t size_in_bytes);
