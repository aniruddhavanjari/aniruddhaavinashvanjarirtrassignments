#include <stdio.h>
#include <stdlib.h>
#include "doubleCirclarMerge.h"

node_tree* create_treehead(void)
{
	node_tree* p_new_node = NULL;
	p_new_node = gettreeHeadNode();
	p_new_node->branches = 7;
	p_new_node->rotation = 45.0f;
	p_new_node->branchTotalLength = 1.5f;
	p_new_node->leaves = 30;
	p_new_node->head = NULL;

	return(p_new_node);
}

node_tree* gettreeHeadNode(void)
{
	node_tree* p_new_node = NULL;
	
	p_new_node = (node_tree*)xmalloc(sizeof(node_tree));
	p_new_node->branchTotalLength = 0.0f;
	p_new_node->branches = 0.0f;
	p_new_node->head = NULL;
	p_new_node->branchTotalLength = 0.0f;
	p_new_node->leaves = 0.0f;
	p_new_node->rotation = 0;

	return(p_new_node);
}

list_t* create_list(void)
{
	node_t* p_new_node = NULL;
	p_new_node = get_list_node(0);
	p_new_node->next = p_new_node;
	p_new_node->prev = p_new_node;
	return(p_new_node);
}

status_t insert_beg(list_t* p_list, len_b new_data)
{
	generic_insert(p_list, get_list_node(new_data), p_list->next);
	++p_list->data;
	return(SUCCESS);
}

status_t insert_end(list_t* p_list, len_b new_data)
{
	generic_insert(p_list->prev, get_list_node(new_data), p_list);
	return(SUCCESS);
}

void generic_insert(node_t* p_beg, node_t* p_mid, node_t* p_end)
{
	p_mid->next = p_end;
	p_mid->prev = p_beg;
	p_beg->next = p_mid;
	p_end->prev = p_mid;
}

status_t destroy_list(list_t** pp_list)
{
	list_t* p_list = *pp_list;
	node_t* p_run, * p_run_next;
	for (p_run = p_list->next; p_run != p_list; p_run = p_run_next)
	{
		p_run_next = p_run->next;
		free(p_run);
	}
	free(p_list);
	*pp_list = NULL;
	return(SUCCESS);
}

node_t* get_list_node(len_b new_data)
{
	static float aav_leaf = 0.0f;
	node_t* p_new_node = NULL;
	p_new_node = (node_t*)xmalloc(sizeof(node_t));
	p_new_node->branchLength = new_data;
	p_new_node->leavesLeft = 0.5f - aav_leaf;
	p_new_node->leavesRight = 0.5f - aav_leaf;
	p_new_node->prev = NULL;
	p_new_node->next = NULL;

	aav_leaf = aav_leaf - 0.014f;
	return(p_new_node);
}

void* xmalloc(size_t size_in_bytes)
{
	void* p = NULL;
	p = malloc(size_in_bytes);
	if (p == NULL)
		fprintf(aav_gpFile, "Failed to Alocate Memory \n");
	return(p);
}
