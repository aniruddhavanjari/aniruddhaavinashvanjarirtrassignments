#include <windows.h>
#include "MainProjectIntegration.h"
#include <stdlib.h>
#include <stdio.h>
#include <gl/gl.h>
#include <math.h>
#include <gl/glu.h> // graphic utality 
#include "doubleCirclarMerge.h"

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Callback Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Globla function declaration
void human(void);
void Tree(void);
void Bird(void);
void walkingHumanNew(void);

//Gloabal Variable 
DWORD aav_dwStyle;
WINDOWPLACEMENT aav_gwpPrev = { sizeof(WINDOWPLACEMENT) };
bool aav_gbFullScreen = false;
HWND aav_ghwnd = NULL;
FILE* aav_gpFile = NULL;
bool aav_gbActiveWindow = false;
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

GLfloat aav_cubeAngle = 0.0f;
GLfloat aav_pyramidAngle = 0.0f;
GLfloat aav_treanslateSceen1 = -10.0f;

//quadric
GLUquadric* aav_gpquadric = NULL;

GLint aav_gisholderLeft = -90; //Sholder Rotation
GLint aav_gielboLeft = 0;		//elbo Rotation
GLint aav_giLegLeft = 0;	//leg thies to be Done
GLint aav_giLeg2Left = 0;	// bottom of thies to be Done
GLint aav_giToeLeft = 0;		// to (talve ) to be Done/
GLint aav_giPamLeft = 0;

GLint aav_gisholderRight = -90; //Sholder Rotation
GLint aav_gielboRight = 0;		//elbo Rotation
GLint aav_giLegRight = 0;	//leg thies to be Done
GLint aav_giLeg2Right = 0;	// bottom of thies to be Done
GLint aav_giToeRight = 0;		// to (talve ) to be Done/
GLint aav_giPamRight = 0;

GLfloat aav_gRotate = 0.0f; // Full Human Rotate.

//Action Flag
BOOL aav_gbwalkFlag = FALSE;
BOOL aav_gbBirdFlyFlag = TRUE;
BOOL aav_gbSoluteFlag = FALSE;

GLfloat aav_translatemove = -12.0f;

//DoublyCircular Needed Variable 
status_t s;
list_t* p_list = NULL;
node_tree* p_treehead = NULL;
GLfloat Distance = 0.0f;

//Texture Valiable
GLuint aav_tree_textureNew;
GLuint aav_treeTextureNew1;
GLuint aav_tree_texture;
GLuint aav_bird_texture;
GLuint aav_wings_texture;
GLuint aav_grass_texture_flag;
GLuint aav_sky_texture_flag;
GLuint aav_flag;
GLuint aav_stone_texture;
GLuint aav_steel_texture;
GLuint aav_white_texture;
GLuint aav_seenFirestGround_texture;
GLuint aav_seenFirestSky_texture;
GLuint aav_aryDress_texture;
GLuint aav_seenSecondGround_texture;
GLuint aav_book_texture;
GLuint aav_shirt_texture;
GLuint aav_pant_texture;
GLuint aav_armyBuilding_texture;
GLuint aav_brownGround_texture;

//Sceen flag
bool aav_lastsceenflag = false;
BOOL sceenFirstFlag = TRUE;
BOOL sceenSecondFlag = FALSE;
BOOL sceenThirdFlag = FALSE;
BOOL sceenFinalFlag = FALSE;

//camara variable 
GLfloat aav_cameray = 20.0f;
GLfloat aav_cameraz = 30.0f;
GLfloat aav_cameraMovex = 0.0f;
GLfloat aav_cameraMovey = 0.0f;
GLfloat aav_cameraMovez = 0.0f;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	void Initialize(void);
	void Display(void);

	//local Variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szAppName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	INT aav_iHeight, aav_iWidth;

	//Code
	//File IO fileOpen 
	if (fopen_s(&aav_gpFile, "LogFile.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("fopen_s Fail to Open LogFile.txt"), TEXT("Message"), MB_OK | MB_ICONEXCLAMATION);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Success to Open LogFile.txt \n");
	}

	//wndclass Registration
	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.lpszClassName = aav_szAppName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_iWidth = 800;
	aav_iHeight = 600;
	aav_ix = (aav_ix / 2) - (aav_iWidth / 2);
	aav_iy = (aav_iy / 2) - (aav_iHeight / 2);

	RegisterClassEx(&aav_wndclass);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szAppName,
		TEXT("Aniruddha Avinash Vanjari : DataStructureProject"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		aav_iWidth,
		aav_iHeight,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	aav_ghwnd = aav_hwnd;

	Initialize();

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	//GameLoop
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
			{
				aav_bDone = true;
			}
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			if (aav_gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return((int)aav_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void Unitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		aav_gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
	{	switch (wParam)
	{
	case VK_ESCAPE:
		DestroyWindow(hwnd);
		break;
	case 0x46:
	case 0x66:
		ToggleFullScreen();
		break;
	default:
		break;
	}
	}
	break;
	case  WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
	{
		Unitialize();
		PostQuitMessage(0);
	}
	break;
	case WM_CHAR:
	{
		switch (wParam)
		{
		case 'r':
			aav_gRotate = aav_gRotate + 0.9f;
			break;
		case 'R':
			aav_gRotate = aav_gRotate - 0.9f;
			break;
		case 'S':
			aav_gisholderLeft = (aav_gisholderLeft + 3) % 360;
			aav_gisholderRight = (aav_gisholderRight + 3) % 360;
			break;
		case 's':
			aav_gisholderLeft = (aav_gisholderLeft - 3) % 360;
			aav_gisholderRight = (aav_gisholderRight - 3) % 360;
			break;
		case 'E':
			aav_gielboLeft = (aav_gielboLeft + 3) % 360;
			aav_gielboRight = (aav_gielboRight + 3) % 360;
			break;
		case 'e':
			aav_gielboLeft = (aav_gielboLeft - 3) % 360;
			aav_gielboRight = (aav_gielboRight - 3) % 360;
			break;
		case 'l':
			aav_giLegLeft = (aav_giLegLeft + 3) % 360;
			aav_giLegRight = (aav_giLegRight + 3) % 360;
			break;
		case 'L':
			aav_giLegLeft = (aav_giLegLeft - 3) % 360;
			aav_giLegRight = (aav_giLegRight - 3) % 360;
			break;
		case 'o':
			aav_giLeg2Left = (aav_giLeg2Left - 3) % 360;
			aav_giLeg2Right = (aav_giLeg2Right - 3) % 360;
			break;
		case 'O':
			aav_giLeg2Left = (aav_giLeg2Left + 3) % 360;
			aav_giLeg2Right = (aav_giLeg2Right + 3) % 360;
			break;
		case 'p':
			aav_giToeLeft = (aav_giToeLeft + 3) % 360;
			aav_giToeRight = (aav_giToeRight + 3) % 360;

			break;
		case 'P':
			aav_giToeLeft = (aav_giToeLeft - 3) % 360;
			aav_giToeRight = (aav_giToeRight - 3) % 360;
			break;
		case 'i':
			aav_giPamRight = (aav_giPamRight - 3) % 360;
			aav_giPamLeft = (aav_giPamLeft - 3) % 360;
			break;
		case 'I':
			aav_giPamRight = (aav_giPamRight + 3) % 360;
			aav_giPamLeft = (aav_giPamLeft + 3) % 360;
			break;
		case 'w':
		case 'W':
			if (aav_gbwalkFlag == FALSE)
			{
				aav_gbwalkFlag = TRUE;
			}
			else
			{
				aav_gbwalkFlag = FALSE;
			}
			break;
		case 'c':
			aav_cameray = aav_cameray + 0.1f;
			break;
		case 'C':
			aav_cameray = aav_cameray - 0.1f;
			break;
		case 'm':
			aav_translatemove = aav_translatemove + 0.05f;
			break;
		case 'M':
			aav_translatemove = aav_translatemove - 0.05f;
			break;
		}
	}
	break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	MONITORINFO aav_mi = { sizeof(MONITORINFO) };

	if (aav_gbFullScreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & (WS_OVERLAPPEDWINDOW))
		{
			if (GetWindowPlacement(aav_ghwnd, &aav_gwpPrev)
				&& GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi)
				)
			{
				SetWindowLong(aav_ghwnd,
					GWL_STYLE,
					aav_dwStyle & (~WS_OVERLAPPEDWINDOW));
				SetWindowPos(aav_ghwnd,
					HWND_TOP,
					aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.top,
					aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		aav_gbFullScreen = true;

	}
	else
	{
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(aav_ghwnd, &aav_gwpPrev);
		SetWindowPos(aav_ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		aav_gbFullScreen = false;
	}
}

//Function Celaration
void Initialize(void)
{
	//Function declaration
	void ReSize(int, int);
	bool loadGLTexture(GLuint*, TCHAR[]);

	//variable Declaration
	PIXELFORMATDESCRIPTOR aav_pfd;
	INT aav_iPixelFormatIndex;
	//Code
	PlaySound(MAKEINTRESOURCE(MYWAVE),GetModuleHandle(NULL),SND_RESOURCE | SND_ASYNC);
	aav_ghdc = GetDC(aav_ghwnd);
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cDepthBits = 32;							
	aav_pfd.cRedBits   = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits  = 8;
	aav_pfd.cAlphaBits = 8;

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);

	if (aav_iPixelFormatIndex == 0)
	{
		fprintf(aav_gpFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(aav_ghwnd);
	}
	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == FALSE)
	{
		fprintf(aav_gpFile, "SetPixelFormat() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		fprintf(aav_gpFile, "wglCreateContext() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == FALSE)
	{
		fprintf(aav_gpFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//Loading Necessaty 
	loadGLTexture(&aav_shirt_texture, MAKEINTRESOURCE(SHIRST_BITMAP));
	loadGLTexture(&aav_pant_texture, MAKEINTRESOURCE(PANT_BITMAP));
	loadGLTexture(&aav_book_texture, MAKEINTRESOURCE(BOOK_BITMAP));// book
	loadGLTexture(&aav_stone_texture, MAKEINTRESOURCE(WHITE_BITMAP));
	loadGLTexture(&aav_armyBuilding_texture, MAKEINTRESOURCE(ARMYBUILDING_BITMAP));
	loadGLTexture(&aav_brownGround_texture, MAKEINTRESOURCE(BROWNgROUND_BITMAP));
	loadGLTexture(&aav_seenSecondGround_texture, MAKEINTRESOURCE(SCEENTWOGRASS_BITMAP));
	loadGLTexture(&aav_tree_texture, MAKEINTRESOURCE(TREE_BITMAP));
	loadGLTexture(&aav_tree_textureNew, MAKEINTRESOURCE(TREENEW_BITMAP));
	loadGLTexture(&aav_treeTextureNew1, MAKEINTRESOURCE(TREENEWONE_BITMAP));
	loadGLTexture(&aav_bird_texture,MAKEINTRESOURCE(BIRD_BITMAP));
	loadGLTexture(&aav_wings_texture,MAKEINTRESOURCE(WINGS_BITMAP));
	loadGLTexture(&aav_grass_texture_flag, MAKEINTRESOURCE(GRASSE_BITMAP));
	loadGLTexture(&aav_sky_texture_flag, MAKEINTRESOURCE(SKY_BITMAP));
	loadGLTexture(&aav_flag, MAKEINTRESOURCE(FLAG_BITMAP));
	loadGLTexture(&aav_stone_texture, MAKEINTRESOURCE(STONE_BITMAP));
	loadGLTexture(&aav_steel_texture, MAKEINTRESOURCE(STEEL_BITMAP));
	loadGLTexture(&aav_seenFirestGround_texture, MAKEINTRESOURCE(SEENFIRSTG_BITMAP));
	loadGLTexture(&aav_seenFirestSky_texture, MAKEINTRESOURCE(SEENFIRSTS_BITMAP));
	loadGLTexture(&aav_aryDress_texture, MAKEINTRESOURCE(ARMYDRESS_BITMAP));
	//Enable 2D texture One of the Impotant lineff
	glEnable(GL_TEXTURE_2D);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	//linkListUsing Tree
	p_treehead = create_treehead();
	p_list = create_list();
	p_treehead->head = p_list;
	Distance = 0.0f;
	for (int i = 0; i < p_treehead->leaves; i++)
	{
		s = insert_end(p_treehead->head, Distance);
		fprintf(aav_gpFile, "Inidalize Distance values : %f", Distance);
		if (s != SUCCESS)
			printf("Insert Failed\n");
		Distance = Distance + (1.5f / 30.0f);
	}
	if (s != SUCCESS)
		printf("Insert Failed\n");
	show_list(p_list, "After insert_beg()");
	
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	ReSize(WIN_WIDTH, WIN_HEIGHT);
}

bool loadGLTexture(GLuint* aav_texture, TCHAR aav_resourceID[])
{
	//Variable Declaration
	bool aav_bResult = false;
	HBITMAP aav_hBitmap = NULL; // OS Image Loading
	BITMAP aav_bmp;

	//Code
	aav_hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL),
		aav_resourceID,
		IMAGE_BITMAP,
		0,
		0,
		LR_CREATEDIBSECTION);

	if (aav_hBitmap)
	{
		aav_bResult = true;
		GetObject(aav_hBitmap, sizeof(BITMAP), &aav_bmp);

		//From Here Starts OpenGL Texture Code.
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

		glGenTextures(1, aav_texture); // Gatu
		glBindTexture(GL_TEXTURE_2D, *aav_texture);

		//Setting Texture Paramter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//Following will Push Data into Memory
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3,
			aav_bmp.bmWidth,
			aav_bmp.bmHeight,
			GL_BGR_EXT,
			GL_UNSIGNED_BYTE,
			aav_bmp.bmBits);
		DeleteObject(aav_hBitmap);
	}
	return aav_bResult;
}

void ReSize(int aav_iwidth, int  aav_iheight)
{
	//Code
	if (aav_iheight == 0)
		aav_iheight = 1;

	glViewport(0, 0, (GLsizei)aav_iwidth, (GLsizei)aav_iheight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)aav_iwidth / (GLfloat)aav_iheight, 0.1f, 100.0f);
}

void Display(void)
{
	//function declaration
	void UpdateAngle();
	void Tree(void);
	void Bird(void);
	void lastSceen(void);
	void SceenFirst(void);
	void sceenSecond(void);
	void sceenThird(void);
	
	//code
	glClear((GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	if (sceenFirstFlag == TRUE)
	{
		aav_cameray = aav_cameray - 0.1f;
		if (aav_cameray < 0.0f)
			aav_cameray = 0.0f;
		aav_cameraz = 20.0f;
	}
	if (sceenSecondFlag == TRUE)
	{
		aav_cameraz = aav_cameraz - 0.001f;
		if (aav_cameraz < 15.0f)
		{
			aav_cameraz = 15.0f;
			sceenSecondFlag = FALSE;
			sceenThirdFlag = TRUE;
			aav_cameray = 0.0f;
			aav_cameraMovex = 0.0f;
		}
		else
		{
			aav_cameray = 0.0f;
			aav_cameraMovex = 5.0f;
		}
	}
	else if (sceenThirdFlag == TRUE)
	{
		aav_cameray = 0.0f;
		aav_cameraz = 20.0f;
	}
	else if (sceenFinalFlag == TRUE)
	{
		aav_cameray = 0.0f;
		aav_cameraz = aav_cameraz + 0.0009f;
		if (aav_cameraz > 20.0f)
			aav_cameraz = 20.0f;
	}
	gluLookAt(0.0f, aav_cameray, aav_cameraz,aav_cameraMovex,0.0f,0.0f, 0.0f, 1.0f, 0.0f);
	glPushMatrix();

	//First Sceen
	if (sceenFirstFlag == TRUE)
	{
		glPushMatrix();
		SceenFirst();
		glPopMatrix();
		sceenFirstFlag = TRUE;
	}

	//Second Sceen
	if (aav_treanslateSceen1 == 22.0f)
	{
		if (sceenSecondFlag == TRUE)
		{
			sceenFirstFlag = FALSE;
			aav_gbwalkFlag = FALSE;
			sceenSecondFlag = TRUE;

			glPushMatrix();
			sceenSecond();
			glPopMatrix();
		}
	}
	
	//third Sceen
	if (sceenThirdFlag == TRUE)
	{
		aav_gbwalkFlag = FALSE;
		sceenThird();
	}

	//forth sceen
	if (sceenFinalFlag == TRUE)
	{
		sceenFirstFlag = FALSE;
		glPushMatrix();
		lastSceen();
		glPopMatrix();
		sceenFinalFlag = TRUE;
	}
	
	glPopMatrix();
	SwapBuffers(aav_ghdc);
}

void SceenFirst(void)
{
	//variable declaration
	static float aav_birdTranslatef = -15.0f;

	//code
	glPushMatrix();
	glTranslatef(aav_birdTranslatef, 2.0f, 0.0f);
	Bird();
	glPopMatrix();

	//bottom
	glBindTexture(GL_TEXTURE_2D, aav_seenFirestGround_texture);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(35.0f, -4.6f, -20.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-35.0f, -4.6f, -20.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-35.0f, -4.6f, 20.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(35.0f, -4.6f, 20.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, aav_seenFirestGround_texture);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(15.0f, -4.5f, -20.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-15.0f, -4.5f, -20.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-15.0f, -4.5f, 20.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(15.0f, -4.5f, 20.0f);
	glEnd();

	//top sky
	glBindTexture(GL_TEXTURE_2D, aav_seenFirestSky_texture);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(50.0f, 30.1f, -30.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-50.0f, 30.1f, -30.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-50.0f, -30.1f, -30.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(50.0f, -30.1f, -30.0f);
	glEnd();

	glPushMatrix();
	glTranslatef(0.0f,0.0f,aav_treanslateSceen1);
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glTranslatef(0.0f, -0.5f, 0.0f);
	glScalef(0.5f, 0.5, 0.5f);
	human();
	glPopMatrix();

	aav_treanslateSceen1 = aav_treanslateSceen1 + 0.1f;
	if (aav_treanslateSceen1 > 22.0f)
	{
		aav_treanslateSceen1 = 22.0f;
		aav_cameraz = 20.0f;
		sceenSecondFlag = TRUE;
	}
	float x = -10.0f;
	glPushMatrix();
	glTranslatef(0.0f,0.0f,-20.0f);
	for (int i = 0 ,x = 2.0f; i < 3; i++,x = x + 10.0f)
	{
		glPushMatrix();
		glTranslatef(7.0f, 0.0f, x);
		Tree();
		glPopMatrix();
	}

	for (int i = 0, x = 2.0f; i < 3; i++, x = x + 10.0f)
	{
		glPushMatrix();
		glTranslatef(-7.0f, 0.0f, x);
		Tree();
		glPopMatrix();
	}
	glPopMatrix();

	aav_birdTranslatef = aav_birdTranslatef + 0.5f;
	if (aav_birdTranslatef > 15.0f)
	{
		aav_birdTranslatef = 20.0f;
	}
	aav_gbwalkFlag = TRUE;
}

void sceenSecond()
{
	//fucntion declaration
	void treeCustom(void);

	//Code
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, aav_seenSecondGround_texture);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(35.0f, -4.6f, -20.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-35.0f, -4.6f, -20.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-35.0f, -4.6f, 20.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(35.0f, -4.6f, 20.0f);
	glEnd();
	glPopMatrix();

	//top sky
	glPushMatrix();
	glTranslatef(20.0f,0.0f,0.0f);
	glRotatef(-10.0f,0.0f,1.0f,0.0f);
	glBindTexture(GL_TEXTURE_2D, aav_seenFirestSky_texture);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(50.0f, 40.1f, -30.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-50.0f, 40.1f, -30.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-50.0f, -40.1f, -30.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(50.0f, -40.1f, -30.0f);
	glEnd();
	glPopMatrix();
	
	/*Book*/
	glPushMatrix();
	glTranslatef(3.8f,-3.5f,3.6f);
	glRotatef(20.0f,1.0f,0.0f,0.0f);
	glRotatef(-20.0f, 0.0f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, aav_book_texture);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.4f, 0.5f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-0.5f, 0.5f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-0.5f, -0.5f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.4f, -0.5f, 0.0f);
	glEnd();
	glPopMatrix();

	glColor3f(1.0f, 1.0f, 1.0f);

	glPushMatrix();
	glTranslatef(4.0f, -3.5f, 3.0f);
	glRotatef(-20.0f, 0.0f, 1.0f, 0.0f);
	sceenFirstFlag = FALSE;
	sceenSecondFlag = TRUE;
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	human();
	glPopMatrix();

	glColor3f(1.0f, 1.0f, 1.0f);

	glPushMatrix();
	glTranslatef(5.0f,1.0f,0.0f);
	treeCustom();
	glPopMatrix();
}

void treeCustom(void)
{
	//local variable declaration
	GLfloat aav_angle = 0.0f;

	//Code
	glPushMatrix();
	glTranslatef(0.0f, 0.0f, 0.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	aav_gpquadric = gluNewQuadric();
	glBindTexture(GL_TEXTURE_2D, aav_tree_textureNew);
	gluQuadricOrientation(aav_gpquadric, GLU_OUTSIDE);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluCylinder(aav_gpquadric, 0.6f, 0.6f, 8, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 1.0f, 0.0f);
	glScalef(1.0f,2.0f,1.0f);
	aav_gpquadric = gluNewQuadric();
	glBindTexture(GL_TEXTURE_2D, aav_treeTextureNew1);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric,2.0f,20,20);
	glPopMatrix();

	glPushMatrix();
	glRotatef(-10.0f, 0.0f, 0.0f, 1.0f);
	glTranslatef(1.0f, 3.0f, 1.0f);
	glScalef(1.0f, 2.0f, 1.0f);
	aav_gpquadric = gluNewQuadric();
	glBindTexture(GL_TEXTURE_2D, aav_treeTextureNew1);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 1.0f, 10, 10);
	glPopMatrix();

	for (int i = 0; i < 10; i++, aav_angle = aav_angle + 25.0f)
	{
		glPushMatrix();
		glRotatef(aav_angle, 0.0f, 1.0f, 0.0f);
		glRotatef(10.0f, 0.0f, 0.0f, 1.0f);
		glTranslatef(-2.0f, 3.0f, 1.0f);
		glScalef(1.0f, 2.0f, 1.0f);
		aav_gpquadric = gluNewQuadric();
		glBindTexture(GL_TEXTURE_2D, aav_treeTextureNew1);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
		gluSphere(aav_gpquadric, 1.0f, 10, 10);
		glPopMatrix();
	}

	for (int i = 0; i < 10; i++, aav_angle = aav_angle + 25.0f)
	{
		glPushMatrix();
		glRotatef(aav_angle, 0.0f, 1.0f, 0.0f);
		glRotatef(10.0f, 0.0f, 0.0f, 1.0f);
		glTranslatef(-2.0f, 1.0f, 1.0f);
		glScalef(1.0f, 2.0f, 1.0f);
		aav_gpquadric = gluNewQuadric();
		glBindTexture(GL_TEXTURE_2D, aav_treeTextureNew1);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
		gluSphere(aav_gpquadric, 1.0f, 10, 10);
		glPopMatrix();
	}

	for (int i = 0; i < 13; i++, aav_angle = aav_angle + 25.0f)
	{
		glPushMatrix();
		glRotatef(aav_angle, 0.0f, 1.0f, 0.0f);
		glRotatef(-10.0f, 0.0f, 0.0f, 1.0f);
		glTranslatef(-2.0f, 4.0f, 1.0f);
		glScalef(1.0f, 2.0f, 1.0f);
		aav_gpquadric = gluNewQuadric();
		glBindTexture(GL_TEXTURE_2D, aav_treeTextureNew1);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
		gluSphere(aav_gpquadric, 1.0f, 10, 10);
		glPopMatrix();
	}
}

void sceenThird(void)
{
	//local variable declaration
	static GLfloat aav_translatef = 25.0f;
	//code
	if (aav_gbwalkFlag == FALSE)
		aav_gbwalkFlag = TRUE;
	
	if(aav_translatef == 0.0f)
		aav_gbwalkFlag = FALSE;
	
	/*Ground Texture*/
	glPushMatrix();
	glBindTexture(GL_TEXTURE_2D, aav_brownGround_texture);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(35.0f, -4.6f, -10.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-35.0f, -4.6f, -10.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-35.0f, -4.6f, 10.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(35.0f, -4.6f, 10.0f);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.5f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, aav_armyBuilding_texture);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(30.0f, 20.1f, -20.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-30.0f, 20.1f, -20.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-30.0f, -20.1f, -20.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(30.0f, -20.1f, -20.0f);
	glEnd();
	glPopMatrix();
	
	glPushMatrix();
	glTranslatef(0.0f,-1.0f,aav_translatef);
	glRotatef(180.0f,0.0f,1.0f,0.0f);
	walkingHumanNew();
	glPopMatrix();

	aav_translatef = aav_translatef - 0.0008f;
	if (aav_translatef < 0.0f)
	{
		aav_translatef = 0.0f;
		aav_gbwalkFlag = FALSE;
		aav_cameraz = 10.0f;
		sceenThirdFlag = FALSE;
		sceenFinalFlag = TRUE;
	}
	glColor3f(1.0f,1.0f,1.0f);
}

void lastSceen(void)
{
	//Function declaration
	void flag(void);
	void human(void);

	//Code 
	if(aav_gbwalkFlag == TRUE)
		aav_gbwalkFlag = FALSE;
	
	glColor3f(1.0f,1.0f,1.0f);
	glBindTexture(GL_TEXTURE_2D, aav_grass_texture_flag);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f,0.0f);
	glVertex3f(30.0f,-4.5f,-20.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-30.0f,-4.5f,-20.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-30.0f,-4.5f,20.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(30.0f,-4.5f,20.0f);
	glEnd();
	
	glBindTexture(GL_TEXTURE_2D, aav_sky_texture_flag);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f,0.0f);
	glVertex3f(40.0f,40.1f,-20.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-40.0f,40.1f,-20.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-40.0f,-40.1f,-20.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(40.0f,-40.1f,-20.0f);
	glEnd();
	
	glPushMatrix();
	glColor3f(1.0f,1.0f,1.0f);
	glTranslatef(1.0f,2.5f,-5.0f);
	glBindTexture(GL_TEXTURE_2D, aav_flag);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0f,1.0f);
	glVertex3f(0.9f,1.0f,0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, -0.1f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.9f, -0.1f, 0.0f);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f,1.0f,0.0f);
	flag();
	glPopMatrix();

	glPushMatrix();
	glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glTranslatef(0.0f,-0.5f,-10.0f);
	glScalef(0.5f, 0.5, 0.5f);
	human();
	glPopMatrix();

	aav_gbSoluteFlag = TRUE;
}

void flag()
{
	glPushMatrix();
	glTranslatef(0.0f,2.6f,-5.0f);
	glRotatef(90.0f,1.0f,0.0f,0.0f);
	glBindTexture(GL_TEXTURE_2D, aav_steel_texture);
	aav_gpquadric = gluNewQuadric();
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluCylinder(aav_gpquadric,0.05f,0.05f,20,20,20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -4.6f, -5.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, aav_stone_texture);
	aav_gpquadric = gluNewQuadric();
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluCylinder(aav_gpquadric, 0.2f, 0.2f, 0.4f, 20, 20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -4.8f, -5.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, aav_stone_texture);
	aav_gpquadric = gluNewQuadric();
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluCylinder(aav_gpquadric, 0.4f, 0.4f, 0.4f, 20, 20);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, -5.0f, -5.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	glBindTexture(GL_TEXTURE_2D, aav_stone_texture);
	aav_gpquadric = gluNewQuadric();
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluCylinder(aav_gpquadric, 0.8f, 0.8f, 0.4f, 20, 20);
	glPopMatrix();
}

void human(void)
{
	//Fucntion Declaration
	void Walk(void);
	// mandya.

	glColor3f(1.0f,1.0f,1.0f);
	static int v1 = 1;
	static int v2 = 3;
	static float variable = 0.0f;
	static float variable1 = 0.0f;

	static int lh = 1;
	static int rh = 1;
	static float lefthand = 0.0f;
	static float righthand = 0.0f;

	static int el = 3;
	static float leftelbo = 0;
	static int rl = 1;
	static float rightelbo = 0;

	//leg bottom part 
	static int lgb = 1;
	static float leftleg = 0;
	static int rgb = 1;
	static float rightleg = 0;

	GLfloat handSpeed = 0.0f;
	GLfloat legSpeed = 0.0f;

	if (sceenFirstFlag == TRUE)
	{
		handSpeed = 2.5f;
		legSpeed = 1.4f;
	}
	else if (sceenFinalFlag == TRUE)
	{
		handSpeed = 0.07f;
		legSpeed = 0.04f;
	}
	else if (sceenThirdFlag == TRUE)
	{
		handSpeed = 0.025f;
		legSpeed = 0.014f;
	}

	//Code
	glPushMatrix();//m

	//head
	if (sceenFinalFlag == TRUE || sceenThirdFlag == TRUE)
	{
		glColor3f(0.0f, 0.0f, 0.0f);
	}
	else
	{
		glColor3f(1.0f, 1.0f, 1.0f);
	}
	glTranslatef(0.0f, 1.2f, 0.0f);
	glScalef(1.0f, 1.5f, 1.0f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.3f, 10, 10);
	glPopMatrix();//m

	//MainBody 
	glPushMatrix();
	glTranslatef(0.0f, 0.3f, 0.0f);
	glScalef(1.5f, -1.0f, 1.0f);

	//Main Body
	/*************Texture Enable and Disable Code As Per the Sceen***************/
	if (sceenFirstFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	else if (sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	else if (sceenFinalFlag == TRUE)
	{
		glEnable(GL_BLEND);
		glBindTexture(GL_TEXTURE_2D, aav_aryDress_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
		glDisable(GL_BLEND);
	}
	gluSphere(aav_gpquadric, 0.5f, 8, 8);
	glPopMatrix();//m


	glPushMatrix();
	glTranslatef(0.0f, -0.3f, 0.0f);
	glScalef(1.3f, 2.8f, 1.0f);
	if (sceenFirstFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	else if (sceenFinalFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_aryDress_texture);
		aav_gpquadric = gluNewQuadric();
		glColor3f(1.0f, 1.0f, 1.0f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	gluSphere(aav_gpquadric, 0.4f, 20, 20);
	glPopMatrix();//m
	glColor3f(1.0f, 1.0f, 1.0f);

	/*********************************Left Hand **************************************/
	//sholder
	glPushMatrix();//m
	glTranslatef(0.8f, 0.4f, 0.0f);
	glRotatef(-(GLfloat)90, 0.0f, 0.0f, 1.0f);
	
	/***********************Pendulam Moment*****************************/
	if (aav_gbwalkFlag)
	{
		if (lh == 1)
		{
			lefthand = lefthand + handSpeed;
			if (lefthand >= 45.0f)
			{
				lh = 2;
			}
		}
		else if (lh == 2)
		{
			lefthand = lefthand - handSpeed;
			if (lefthand >= 0.0f)
			{
				lh = 3;
			}
		}
		else if (lh == 3)
		{
			lefthand = lefthand - handSpeed;
			if (lefthand <= -45.0f)
			{
				lh = 4;
			}
		}
		else if (lh == 4)
		{
			lefthand = lefthand + handSpeed;
			if (lefthand >= 0.0f)
			{
				lh = 1;
				lefthand = 0.0f;
			}
		}
		glRotatef((GLfloat)lefthand, 0.0f, 1.0f, 0.0f);
	}
	else if (sceenSecondFlag == TRUE)
	{
		glRotatef(-(GLfloat)30.0f, 0.0f, 1.0f, 0.0f);		
	}
	glTranslatef(0.4f, 0.0f, 0.0f);
	glPushMatrix();//s
	glScalef(1.0f, 0.5f, 0.5f);
	if (sceenFirstFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	else if (sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	else if (sceenFinalFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_aryDress_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	gluSphere(aav_gpquadric, 0.5f, 30, 30);

	/**************************************Elbo of left hand**********************/
	glPopMatrix();//s
	glTranslatef(0.6f, 0.0f, 0.0f);
	glColor3f(0.94509f, 0.76078f, 0.49019f);
	
	/**************elbo rotation*****************/
	if (aav_gbwalkFlag)
	{
		if (sceenThirdFlag == TRUE)
			glRotatef(0.0f, 0.0f, 0.0f, 1.0f);
		else	
			glRotatef(-(GLfloat)90.0f, 0.0f, 1.0f, 0.0f);
	}
	else if (sceenSecondFlag)
	{
		glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
		glRotatef(-30.0f, 0.0f, 0.0f, 1.0f);
	}
	
	else
		glRotatef((GLfloat)aav_gielboLeft, 0.0f, 1.0f, 0.0f);
	glTranslatef(0.25f, 0.0f, 0.0f);
	glPushMatrix();//e
	glScalef(1.0f, 0.5f, 0.5f);
	if (sceenFirstFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	else if (sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	else if (sceenFinalFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_aryDress_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	gluSphere(aav_gpquadric, 0.4f, 30, 30);

	//********************pam of left hand*****************************/
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glPopMatrix();
	glTranslatef(0.4f, 0.0f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)aav_giPamLeft, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.15f, 0.0f, 0.0f);
	glPushMatrix();//p
	glColor3f(0.94509f, 0.76078f, 0.49019f);
	glScalef(1.0f, 0.5f, 0.5f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.2f, 30, 30);

	glPopMatrix();
	glPopMatrix();

	/***************************************Right hand ****************************/
	//sholder
	glPushMatrix();//m
	glTranslatef(-0.8f, 0.4f, 0.0f);
	glRotatef(-(GLfloat)aav_gisholderRight, 0.0f, 0.0f, 1.0f);
	if (aav_gbwalkFlag)
	{
		if (rh == 1)
		{
			righthand = righthand + handSpeed;
			if (righthand >= 45.0f)
			{
				rh = 2;
			}
		}
		else if (rh == 2)
		{
			righthand = righthand - handSpeed;
			if (righthand >= 0.0f)
			{

				rh = 3;
			}
		}
		else if (rh == 3)
		{
			righthand = righthand - handSpeed;
			if (righthand <= -45.0f)
			{

				rh = 4;
			}
		}
		else if (rh == 4)
		{
			righthand = righthand + handSpeed;
			if (righthand >= 0.0f)
			{
				rh = 1;
				righthand = 0.0f;

			}
		}
		glRotatef((GLfloat)righthand, 0.0f, 1.0f, 0.0f);
	}
	else if (aav_gbSoluteFlag)
	{
		if (rh == 1)
		{
			righthand = righthand + handSpeed;
			if (righthand >= 0.0f)
			{
				rh = 2;
			}
		}
		else if (rh == 2)
		{
			righthand = righthand - handSpeed;
			if (righthand >= 0.0f)
			{
				rh = 3;
			}
		}
		else if (rh == 3)
		{
			righthand = righthand - handSpeed;
			if (righthand <= -150.0f)
			{
				rh = 5;
			}
		}
		else if (rh == 4)
		{
			righthand = righthand + handSpeed;
			if (righthand >= 0.0f)
			{
				rh = 1;
				righthand = 0.0f;
			}
		}
		glRotatef((GLfloat)righthand, 0.0f, 0.0f, 1.0f);
	}
	else if (sceenSecondFlag == TRUE)
	{
		glRotatef(30.0f,0.0f,1.0f,0.0f);
	}
	//////////////////////////////////////////////////////////////

	glTranslatef(-0.4f, 0.0f, 0.0f);
	glPushMatrix();//s
	glScalef(1.0f, 0.5f, 0.5f);
	glColor3f(1.0f, 1.0f, 1.0f);
	if (sceenFirstFlag == TRUE || sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
	{
		if (sceenFirstFlag == TRUE)
			glColor3f(0.94509f, 0.76078f, 0.49019f);
		else if (sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
			glColor3f(0.0f, 0.0f, 0.5f);


		aav_gpquadric = gluNewQuadric();
		glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	else if (sceenFinalFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_aryDress_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	gluSphere(aav_gpquadric, 0.5f, 30, 30);

	/************************Right hand elbo*********************************/
	glPopMatrix();//s
	glTranslatef(-0.6f, 0.0f, 0.0f);
	glRotatef(180, 1.0f, 0.0f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	if (aav_gbwalkFlag)
	{
		if (sceenThirdFlag == TRUE)
			glRotatef(0.0f, 0.0f, 0.0f, 1.0f);
		else
			glRotatef(-(GLfloat)90.0f, 0.0f, 1.0f, 0.0f);
	}
	else if (aav_gbSoluteFlag)
	{
		 if (rl == 1)
		{
			rightelbo = rightelbo - 0.03f;
			if (rightelbo <= -110.0f)
			{
				rl = 4;
			}
		}
		glRotatef(-(GLfloat)rightelbo, 0.0f, 0.0f, 1.0f);
	}
	else if (sceenSecondFlag == TRUE)
	{
		glRotatef(-90.0f, 0.0f, 1.0f, 0.0f);
		glRotatef(-30.0f, 0.0f, 0.0f, 1.0f);
	}
	else
		glRotatef(-(GLfloat)0.0f, 0.0f, 1.0f, 0.0f);

	glTranslatef(-0.25f, 0.0f, 0.0f);
	glPushMatrix();//e
	glColor3f(1.0f, 1.0f, 0.0f);
	glScalef(1.0f, 0.5f, 0.5f);//
	if (sceenFirstFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	else if (sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	else if (sceenFinalFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_aryDress_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	gluSphere(aav_gpquadric, 0.4f, 30, 30);

	/***********************right hand Pam**************************/
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glPopMatrix();
	glTranslatef(-0.4f, -0.0f, 0.0f);
	glColor3f(0.94509f, 0.76078f, 0.49019f);//skin color
	glRotatef(-(GLfloat)aav_giPamRight, 0.0f, 0.0f, 1.0f);
	glTranslatef(-0.15f, 0.0f, 0.0f);
	glPushMatrix();//p
	glScalef(1.0f, 0.5f, 0.5f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.2f, 30, 30);

	glPopMatrix();
	glPopMatrix();

	/************************************Right Leg************************/
	glPushMatrix();
	glTranslatef(-0.4f, -1.0f, 0.0f);

	if (aav_gbwalkFlag)
	{
		if (v1 == 1)
		{
			variable = variable + legSpeed;
			if (variable >= 25.0f)
			{
				v1 = 2;
			}
		}
		else if (v1 == 2)
		{
			variable = variable - legSpeed;
			if (variable >= 0.0f)
			{
				v1 = 3;
			}
		}
		else if (v1 == 3)
		{
			variable = variable - legSpeed;
			if (variable <= -25.0f)
			{
				v1 = 4;
			}
		}
		else if (v1 == 4)
		{
			variable = variable + legSpeed;
			if (variable >= 0.0f)
			{
				v1 = 1;
				variable = 0.0f;
			}
		}
		glRotatef((GLfloat)variable, 1.0f, 0.0f, 0.0f);
	}
	else if (sceenSecondFlag == TRUE)
	{
		glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
		glRotatef(-90.0f,1.0f,0.0f,0.0f);
	}
	else
		glRotatef((GLfloat)aav_giLegRight, 1.0f, 0.0f, 0.0f);

	glTranslatef(-0.0f, -0.5f, 0.0f);
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);
	glScalef(0.5f, 1.5f, 0.5f);
	if (sceenFirstFlag == TRUE || sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
	{
		if (sceenFirstFlag == TRUE)
			glColor3f(0.f, 0.0f, 0.0f);
		else if (sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
			glColor3f(0.49019f, 0.49019f, 0.36470f);
		aav_gpquadric = gluNewQuadric();
	}
	else if (sceenFinalFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_aryDress_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	gluSphere(aav_gpquadric, 0.5f, 30, 30);
	glPopMatrix();

	/*****************Right leg bottom****************/
	glTranslatef(-0.0f, -0.55f, -0.0f);
	if (aav_gbwalkFlag)
	{
		Walk();
	}
	else if (sceenSecondFlag == TRUE)
	{
		glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
	}
	else
	{
		glRotatef((GLfloat)aav_giLeg2Right, 1.0f, 0.0f, 0.0f);
	}

	glTranslatef(-0.0f, -0.6f, 0.0f);
	glPushMatrix();
	glScalef(0.5f, 1.5f, 0.5f);
	if (sceenFirstFlag == TRUE || sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
	{
		if (sceenFirstFlag == TRUE)
			glColor3f(0.94509f, 0.76078f, 0.49019f);
		else if (sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
			glColor3f(0.49019f, 0.49019f, 0.36470f);
		aav_gpquadric = gluNewQuadric();
	}
	else if (sceenFinalFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_aryDress_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	gluSphere(aav_gpquadric, 0.4f, 30, 30);

	glPopMatrix();
	glTranslatef(0.0f, -0.4f, 0.0f);

	if (aav_gbwalkFlag)
		Walk();
	else
		glRotatef((GLfloat)aav_giToeRight, 1.0f, 0.0f, 0.0f);

	/***************Right leg Talve*****************/
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glTranslatef(0.0f, -0.2f, 0.2f);
	glScalef(0.5f, 0.4f, 1.2f);
	glColor3f(0.5882f, 0.29411f, 0.0f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.3f, 20, 20);
	glPopMatrix();

	/********************************Left Leg**********************/
	glPushMatrix();
	glTranslatef(0.4f, -1.0f, 0.0f);
	if (aav_gbwalkFlag)
	{
		if (v2 == 1)
		{
			variable1 = variable1 + legSpeed;
			if (variable1 >= 25.0f)
			{
				v2 = 2;
			}
		}
		else if (v2 == 2)
		{
			variable1 = variable1 - legSpeed;
			if (variable1 >= 0.0f)
			{
				v2 = 3;
			}
		}
		else if (v2 == 3)
		{
			variable1 = variable1 - legSpeed;
			if (variable1 <= -25.0f)
			{
				v2 = 4;
			}
		}
		else if (v2 == 4)
		{
			variable1 = variable1 + legSpeed;
			if (variable1 >= 0.0f)
			{
				v2 = 1;
				variable1 = 0.0f;
			}
		}
		glRotatef((GLfloat)variable1, 1.0f, 0.0f, 0.0f);
	}
	else if (sceenSecondFlag == TRUE)
	{
		glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
		glRotatef(-90.0f,1.0f,0.0f,0.0f);
	}
	else
		glRotatef((GLfloat)aav_giLegLeft, 1.0f, 0.0f, 0.0f);

	glTranslatef(-0.0f, -0.5f, 0.0f);
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 0.0f);
	glScalef(0.5f, 1.5f, 0.5f);
	if (sceenFirstFlag == TRUE)
	{
		aav_gpquadric = gluNewQuadric();
		glColor3f(0.0f, 0.0f, 0.0f);
	}
	else if (sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
	{
		glColor3f(0.49019f, 0.49019f, 0.36470f);
		aav_gpquadric = gluNewQuadric();
	}
	else if (sceenFinalFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_aryDress_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	gluSphere(aav_gpquadric, 0.5f, 30, 30);

	/**************************left Leg Bottom***************************/
	//left leg bottom
	glPopMatrix();
	glPushMatrix();
	glTranslatef(0.0f, -0.55f, -0.0f);
	if (sceenSecondFlag == TRUE)
	{
		glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
	}
	else
	{
		glRotatef((GLfloat)aav_giLeg2Left, 1.0f, 0.0f, 0.0f);
	}
	glTranslatef(-0.0f, -0.6f, 0.0f);
	glPushMatrix();
	glColor3f(1.0f, 0.0f, 1.0f);
	glScalef(0.5f, 1.5f, 0.5f);
	if (sceenFirstFlag == TRUE)
	{
		aav_gpquadric = gluNewQuadric();
		glColor3f(0.94509f, 0.76078f, 0.49019f);//skin color
	}
	else if (sceenSecondFlag == TRUE || sceenThirdFlag == TRUE)
	{
		glColor3f(0.49019f, 0.49019f, 0.36470f);
		aav_gpquadric = gluNewQuadric();
	}
	else if (sceenFinalFlag == TRUE)
	{
		glBindTexture(GL_TEXTURE_2D, aav_aryDress_texture);
		aav_gpquadric = gluNewQuadric();
		glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
		gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
		gluQuadricTexture(aav_gpquadric, TRUE);
	}
	gluSphere(aav_gpquadric, 0.4f, 30, 30);
	glPopMatrix();
	
	/*********************left leg Talve **********************************/
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glTranslatef(0.0f, -0.4f, 0.0f);
	glRotatef((GLfloat)aav_giToeLeft, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.2f, 0.2f);
	glScalef(0.5f, 0.4f, 1.2f);
	glColor3f(0.5882f, 0.29411f, 0.0f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.3f, 20, 20);
	glPopMatrix();

	glPopMatrix();
	glPopMatrix();
}

void Walk(void)
{
	if (aav_giLegLeft > 180)
	{
		aav_giLegLeft = (aav_gielboLeft - 3) % 360;
	}
	else if (aav_giLegLeft < 180)
	{
		aav_giLegLeft = (aav_gielboLeft + 3) % 360;
	}
}

void UpdateAngle()
{
	aav_pyramidAngle = aav_pyramidAngle + 0.009f;
	if (aav_pyramidAngle > 360)
	{
		aav_pyramidAngle = 0.0f;
	}
	aav_cubeAngle = aav_cubeAngle + 0.009f;
	if (aav_cubeAngle > 360)
	{
		aav_cubeAngle = 0.0f;
	}
}

void Tree(void)
{
	static GLfloat line = 0.0f;
	list_t* p_run;
	GLfloat aav_Angle = 0.0f;
	int i, j = 0;

	glPushMatrix();
	glTranslatef(0.0f, 3.0f, 0.0f);
	glRotatef(90.0f, 1.0f, 0.0f, 0.0f);

	aav_gpquadric = gluNewQuadric();
	glBindTexture(GL_TEXTURE_2D, aav_tree_texture);
	gluQuadricOrientation(aav_gpquadric, GLU_OUTSIDE);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluCylinder(aav_gpquadric, 0.3f, 0.3f, 8, 30, 30);
	glPopMatrix();

	glPointSize(3.0f);
	glLineWidth(3.0f);

	glPushMatrix();
	for (j = 0; j < p_treehead->rotation; j++)
	{
		glRotatef(aav_Angle, 0.0f, 1.0f, 0.0f);
		glColor3f(1.0f, 1.0f, 1.0f);

		for (line = 0.0f; line < p_treehead->branchTotalLength; line = line + 0.001f)
		{
			glBegin(GL_POINTS);
			glVertex3f(cos(line) * 3.0f, sin(line) * 3.0f, 0.0f);
			glEnd();
		}
		glBegin(GL_LINES);
		for (p_run = p_list->next; p_run != p_list; p_run = p_run->next)
		{
			glColor3f(0.2f, 0.9f, 0.0f);
			fprintf(aav_gpFile, "Distance : %f\n", p_run->branchLength);
			glVertex3f(cos(p_run->branchLength) * 3.0f, sin(p_run->branchLength) * 3.0f, 0.0f);
			glVertex3f(cos(p_run->branchLength) * 3.0f + p_run->leavesLeft, sin(p_run->branchLength) * 3.0f - p_run->leavesRight, p_run->leavesLeft);
			glVertex3f(cos(p_run->branchLength) * 3.0f, sin(p_run->branchLength) * 3.0f, 0.0f);
			glVertex3f(cos(p_run->branchLength) * 3.0f + p_run->leavesLeft, sin(p_run->branchLength) * 3.0f - p_run->leavesRight, -p_run->leavesLeft);
		}
		glEnd();
		aav_Angle = 45.0f;
	}
	glPopMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);
}

void Bird(void)
{
	//local variable
	static GLfloat aav_wings = 0.0f;
	static GLfloat aav_birdAngleLeft = 0.0f;
	static GLfloat aav_birdAngleRight = 0.0f;
	static GLint aav_bl = 1;
	static GLint aav_br = 1;
	static GLfloat legSpeed = 4.0f;

	//code
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	glPushMatrix();
	glScalef(1.0f, 0.6f, 2.0f);
	aav_gpquadric = gluNewQuadric();
	glBindTexture(GL_TEXTURE_2D, aav_bird_texture);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 0.5f, 10, 10);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.2f, 0.2f, 0.2f);

	if (aav_gbBirdFlyFlag)
	{
		if (aav_br == 1)
		{
			aav_birdAngleRight = aav_birdAngleRight + legSpeed;
			if (aav_birdAngleRight >= 20.0f)
			{
				aav_br = 2;
			}
		}
		else if (aav_br == 2)
		{
			aav_birdAngleRight = aav_birdAngleRight - 0.01f;
			if (aav_birdAngleRight >= 0.0f)
			{
				aav_br = 3;
			}
		}
		else if (aav_br == 3)
		{
			aav_birdAngleRight = aav_birdAngleRight - legSpeed;
			if (aav_birdAngleRight <= -25.0f)
			{
				aav_br = 4;
			}
		}
		else if (aav_br == 4)
		{
			aav_birdAngleRight = aav_birdAngleRight + legSpeed;
			if (aav_birdAngleRight >= 0.0f)
			{
				aav_br = 1;
				aav_birdAngleRight = 0.0f;
			}
		}
		glRotatef((GLfloat)aav_birdAngleRight, 0.0f, 0.0f, 1.0f);
	}
	else
		glRotatef(aav_wings, 0.0f, 0.0f, 1.0f);

	glTranslatef(-1.0f, 0.0f, 0.0f);
	glScalef(2.0f, 0.2f, 1.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	aav_gpquadric = gluNewQuadric();
	glBindTexture(GL_TEXTURE_2D, aav_wings_texture);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 0.5f, 3, 3);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(-0.2f, 0.2f, 0.2f);
	glRotatef(-aav_birdAngleRight, 0.0f, 0.0f, 1.0f);
	glTranslatef(1.0f, 0.0f, 0.0f);
	glScalef(2.0f, 0.2f, 1.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	aav_gpquadric = gluNewQuadric();
	glBindTexture(GL_TEXTURE_2D, aav_wings_texture);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 0.5f, 3, 3);
	glPopMatrix();

	glPushMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);
	glTranslatef(0.0f, 0.1f, 1.0f);
	aav_gpquadric = gluNewQuadric();
	glBindTexture(GL_TEXTURE_2D, aav_wings_texture);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 0.3f, 30, 30);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.1f, 1.0f);
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0f, 0.0f, 0.5f);
	glVertex3f(0.0f, -0.2f, 0.0f);
	glVertex3f(0.0f, 0.2f, 0.0f);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0f, 0.0f, -1.9f);
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0f, 0.0f, 1.5f);
	glVertex3f(-1.0f, 0.3f, 0.0f);
	glVertex3f(1.0f, 0.3f, 0.0f);

	glEnd();
	glPopMatrix();
}

void walkingHumanNew(void)
{
	//Fucntion Declaration
	void UpdateAngle();
	void Walk(void);

	// //Upper leg .
	static int aav_v1 = 1;
	static int aav_v2 = 3;
	static float aav_variable = 0.0f;
	static float aav_variable1 = 0.0f;

	static int aav_lh = 1;
	static int aav_rh = 1;
	static float aav_lefthand = 0.0f;
	static float aav_righthand = 0.0f;

	static int el = 3;
	static float leftelbo = 0;
	static int rl = 1;
	static float rightelbo = 0;

	//leg bottom part 
	static int lgb = 1;
	static float leftleg = 0;
	static int rgb = 1;
	static float rightleg = 0;
	
	//Right leg Bottom
	static int aav_br = 3;
	static GLfloat aav_RightLegbottom;

	//left leg Bottom 
	static int aav_bl = 1;
	static GLfloat aav_leftLegbottom;

	GLfloat aav_handSpeed = 0.024f;
	GLfloat aav_legSpeed = 0.04f;

	//Code
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glColor3f(1.0f, 1.0f, 1.0f);

	glPushMatrix();//m

	//head
	glColor3f(0.0f,0.0f,0.0f);
	glTranslatef(0.0f, 1.2f, 0.0f);
	glScalef(1.0f, 1.5f, 1.0f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.3f, 10, 10);
	glPopMatrix();//m

	//MainBody 
	glPushMatrix();
	glTranslatef(0.0f, 0.3f, 0.0f);
	glScalef(1.5f, -1.0f, 1.0f);

	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
	aav_gpquadric = gluNewQuadric();
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 0.5f, 8, 8);

	glPopMatrix();//m

	glPushMatrix();
	glTranslatef(0.0f, -0.3f, 0.0f);
	glScalef(1.3f, 2.8f, 1.0f);
	glColor3f(1.0f, 2.0f, 0.5f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
	aav_gpquadric = gluNewQuadric();
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 0.4f, 20, 20);
	glPopMatrix();//m

	/*****************************************left hand ************************************************/
	//sholder
	glPushMatrix();//m
	glTranslatef(0.8f, 0.4f, 0.0f);
	glRotatef((GLfloat)aav_gisholderLeft, 0.0f, 0.0f, 1.0f);
	if (aav_gbwalkFlag)
	{
		if (aav_lh == 1)
		{
			aav_lefthand = aav_lefthand + aav_handSpeed;
			if (aav_lefthand >= 25.0f)
			{
				aav_lh = 2;
			}
		}
		else if (aav_lh == 2)
		{
			aav_lefthand = aav_lefthand - aav_handSpeed;
			if (aav_lefthand >= 0.0f)
			{

				aav_lh = 3;
			}
		}
		else if (aav_lh == 3)
		{
			aav_lefthand = aav_lefthand - aav_handSpeed;
			if (aav_lefthand <= -5.0f)
			{

				aav_lh = 4;
			}
		}
		else if (aav_lh == 4)
		{
			aav_lefthand = aav_lefthand + aav_handSpeed;
			if (aav_lefthand >= 0.0f)
			{
				aav_lh = 1;
				aav_lefthand = 0.0f;
			}
		}
		glRotatef((GLfloat)aav_lefthand, 0.0f, 1.0f, 0.0f);
	}
	else
		glRotatef((GLfloat)aav_gisholderLeft, 0.0f, 0.0f, 1.0f);
	
	glTranslatef(0.4f, 0.0f, 0.0f);
	glPushMatrix();//s
	glScalef(1.0f, 0.5f, 0.5f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
	aav_gpquadric = gluNewQuadric();
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 0.5f, 30, 30);
	glPopMatrix();//s

	/*********************Elbo of left hand***************************/
	glTranslatef(0.6f, 0.0f, 0.0f);
	//elbo rotation
	if (aav_gbwalkFlag)
	{
		glRotatef(-(GLfloat)25.0f, 0.0f, 1.0f, 0.0f);
	}
	else
		glRotatef((GLfloat)aav_gielboLeft, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.25f, 0.0f, 0.0f);
	glPushMatrix();//e
	glScalef(1.0f, 0.5f, 0.5f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
	aav_gpquadric = gluNewQuadric();
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 0.4f, 30, 30);
	glPopMatrix();

	/*********************left hand pange**********************************/
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glTranslatef(0.4f, 0.0f, 0.0f);
	glRotatef((GLfloat)aav_giPamLeft, 0.0f, 0.0f, 1.0f);
	glTranslatef(0.15f, 0.0f, 0.0f);
	glPushMatrix();//p
	glColor3f(0.94509f, 0.76078f, 0.49019f);
	glScalef(1.0f, 0.5f, 0.5f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.2f, 30, 30);

	glPopMatrix();
	glPopMatrix();

	/*****************************Right Hand ******************************/
	//sholder
	glPushMatrix();//m
	glTranslatef(-0.8f, 0.4f, 0.0f);
	glRotatef(-(GLfloat)aav_gisholderRight, 0.0f, 0.0f, 1.0f);
	if (aav_gbwalkFlag)
	{
		if (aav_rh == 1)
		{
			aav_righthand = aav_righthand + aav_handSpeed;
			if (aav_righthand >= 25.0f)
			{
				aav_rh = 2;
			}
		}
		else if (aav_rh == 2)
		{
			aav_righthand = aav_righthand - aav_handSpeed;
			if (aav_righthand >= 0.0f)
			{
				aav_rh = 3;
			}
		}
		else if (aav_rh == 3)
		{
			aav_righthand = aav_righthand - aav_handSpeed;
			if (aav_righthand <= -5.0f)
			{
				aav_rh = 4;
			}
		}
		else if (aav_rh == 4)
		{
			aav_righthand = aav_righthand + aav_handSpeed;
			if (aav_righthand >= 0.0f)
			{
				aav_rh = 1;
				aav_righthand = 0.0f;
			}
		}
		glRotatef((GLfloat)aav_righthand, 0.0f, 1.0f, 0.0f);
	}
	
	glTranslatef(-0.4f, 0.0f, 0.0f);
	glPushMatrix();//s
	glScalef(1.0f, 0.5f, 0.5f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
	aav_gpquadric = gluNewQuadric();
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 0.5f, 30, 30);

	/*****************************Right hand Elbo*******************************/
	//elbo Right 
	glPopMatrix();//s
	glTranslatef(-0.6f, 0.0f, 0.0f);
	glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	if (aav_gbwalkFlag)
	{
		glRotatef(-(GLfloat)20.0f, 0.0f, 1.0f, 0.0f);
	}
	else
	{
		glRotatef(-(GLfloat)0.0f, 0.0f, 1.0f, 0.0f);
	}
	glTranslatef(-0.25f, 0.0f, 0.0f);
	glPushMatrix();//e
	glScalef(1.0f, 0.5f, 0.5f);
	glColor3f(1.0f, 1.0f, 1.0f);
	glBindTexture(GL_TEXTURE_2D, aav_shirt_texture);
	aav_gpquadric = gluNewQuadric();
	glColor4f(1.0f, 1.0f, 1.0f, 0.5f);
	gluQuadricDrawStyle(aav_gpquadric, GLU_FILL);
	gluQuadricTexture(aav_gpquadric, TRUE);
	gluSphere(aav_gpquadric, 0.4f, 30, 30);

	/************************Right hand panga ***********************************/
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glPopMatrix();
	glTranslatef(-0.4f, -0.0f, 0.0f);
	glRotatef(-(GLfloat)aav_giPamRight, 0.0f, 0.0f, 1.0f);
	glTranslatef(-0.15f, 0.0f, 0.0f);
	glPushMatrix();//p
	glColor3f(0.94509f, 0.76078f, 0.49019f);
	glScalef(1.0f, 0.5f, 0.5f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.2f, 30, 30);

	glPopMatrix();
	glPopMatrix();
	
	/*************************Right Leg*******************************************/
	//right Leg
	glPushMatrix();
	glTranslatef(-0.4f, -1.0f, 0.0f);
	if (aav_gbwalkFlag)
	{
		if (aav_v1 == 1)
		{
			aav_variable = aav_variable + aav_legSpeed;
			if (aav_variable >= 25.0f)
			{
				aav_v1 = 2;
			}
		}
		else if (aav_v1 == 2)
		{
			aav_variable = aav_variable - aav_legSpeed;
			if (aav_variable >= 0.0f)
			{
				aav_v1 = 3;
			}
		}
		else if (aav_v1 == 3)
		{
			aav_variable = aav_variable - aav_legSpeed;
			if (aav_variable <= -25.0f)
			{
				aav_v1 = 4;
			}
		}
		else if (aav_v1 == 4)
		{
			aav_variable = aav_variable + aav_legSpeed;
			if (aav_variable >= 0.0f)
			{
				aav_v1 = 1;
				aav_variable = 0.0f;
			}
		}
		glRotatef((GLfloat)aav_variable, 1.0f, 0.0f, 0.0f);
	}
	else
		glRotatef((GLfloat)aav_giLegRight, 1.0f, 0.0f, 0.0f);

	glTranslatef(-0.0f, -0.5f, 0.0f);
	glPushMatrix();
	glColor3f(0.49019f, 0.49019f, 0.36470f);
	glScalef(0.5f, 1.5f, 0.5f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.5f, 30, 30);
	glPopMatrix();

	/*****************Right leg Bottom**********/
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glTranslatef(-0.0f, -0.55f, -0.0f);
	if (aav_gbwalkFlag)
	{
		if (aav_br == 1 && aav_v1 == 3)
		{
			aav_RightLegbottom = aav_RightLegbottom + 0.05f;
			if (aav_RightLegbottom >= 45.0f)
			{
				aav_br = 2;
			}
		}
		else if (aav_br == 2)
		{
			aav_RightLegbottom = aav_RightLegbottom - 0.05f;
			if (aav_RightLegbottom >= 0.0f)
			{
				aav_br = 3;
			}
		}
		else if (aav_br == 3)
		{
			aav_RightLegbottom = aav_RightLegbottom - 0.05f;
			if (aav_RightLegbottom <= 0.0f)
			{
				aav_br = 4;
			}
		}
		else if (aav_br == 4)
		{
			aav_RightLegbottom = aav_RightLegbottom + aav_legSpeed;
			if (aav_RightLegbottom >= 0.0f)
			{
				aav_br = 1;
				aav_RightLegbottom = 0.0f;
			}
		}
		glRotatef((GLfloat)aav_RightLegbottom, 1.0f, 0.0f, 0.0f);
	}
	else
		glRotatef((GLfloat)aav_giLegLeft, 1.0f, 0.0f, 0.0f);

	glTranslatef(-0.0f, -0.6f, 0.0f);
	glPushMatrix();
	glColor3f(0.49019f, 0.49019f, 0.36470f);
	glScalef(0.5f, 1.5f, 0.5f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.4f, 30, 30);
	glPopMatrix();

	glTranslatef(0.0f, -0.4f, 0.0f);
	if (aav_gbwalkFlag)
		Walk();
	else
		glRotatef((GLfloat)aav_giToeRight, 1.0f, 0.0f, 0.0f);

	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glTranslatef(0.0f, -0.2f, 0.2f);
	glScalef(0.5f, 0.4f, 1.2f);
	glColor3f(0.5882f, 0.29411f, 0.0f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.3f, 20, 20);
	glPopMatrix();

	/************************************Left Leg*************************************/
	//Left Leg
	glPushMatrix();
	glTranslatef(0.4f, -1.0f, 0.0f);
	if (aav_gbwalkFlag)
	{
		if (aav_v2 == 1)
		{
			aav_variable1 = aav_variable1 + aav_legSpeed;
			if (aav_variable1 >= 25.0f)
			{
				aav_v2 = 2;
			}
		}
		else if (aav_v2 == 2)
		{
			aav_variable1 = aav_variable1 - aav_legSpeed;
			if (aav_variable1 >= 0.0f)
			{
				aav_v2 = 3;
			}
		}
		else if (aav_v2 == 3)
		{
			aav_variable1 = aav_variable1 - aav_legSpeed;
			if (aav_variable1 <= -25.0f)
			{
				aav_v2 = 4;
			}
		}
		else if (aav_v2 == 4)
		{
			aav_variable1 = aav_variable1 + aav_legSpeed;
			if (aav_variable1 >= 0.0f)
			{
				aav_v2 = 1;
				aav_variable1 = 0.0f;
			}
		}
		glRotatef((GLfloat)aav_variable1, 1.0f, 0.0f, 0.0f);
	}
	else
		glRotatef((GLfloat)aav_giLegLeft, 1.0f, 0.0f, 0.0f);

	glTranslatef(-0.0f, -0.5f, 0.0f);
	glPushMatrix();
	glColor3f(0.49019f, 0.49019f, 0.36470f);
	glScalef(0.5f, 1.5f, 0.5f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.5f, 30, 30);
	glPopMatrix();
	glPushMatrix();

	/**********left leg bottom  ***********/
	glTranslatef(0.0f, -0.55f, -0.0f);

	if (aav_gbwalkFlag)
	{
		if (aav_bl == 1 && aav_v2 == 3)
		{
			aav_leftLegbottom = aav_leftLegbottom + 0.05f;
			if (aav_leftLegbottom >= 45.0f)
			{
				aav_bl = 2;
			}
		}
		else if (aav_bl == 2)
		{
			aav_leftLegbottom = aav_leftLegbottom - 0.05f;
			if (aav_leftLegbottom >= 0.0f)
			{

				aav_bl = 3;
			}
		}
		else if (aav_bl == 3)
		{
			aav_leftLegbottom = aav_leftLegbottom - 0.05f;
			if (aav_leftLegbottom <= 0.0f)
			{

				aav_bl = 4;
			}
		}
		else if (aav_bl == 4)
		{
			aav_leftLegbottom = aav_leftLegbottom + aav_legSpeed;
			if (aav_leftLegbottom >= 0.0f)
			{
				aav_bl = 1;
				aav_leftLegbottom = 0.0f;

			}
		}
		glRotatef((GLfloat)aav_leftLegbottom, 1.0f, 0.0f, 0.0f);
	}
	else
		glRotatef((GLfloat)aav_giLegLeft, 1.0f, 0.0f, 0.0f);

	glTranslatef(-0.0f, -0.6f, 0.0f);
	glPushMatrix();
	glColor3f(0.49019f, 0.49019f, 0.36470f);
	glScalef(0.5f, 1.5f, 0.5f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.4f, 30, 30);
	glPopMatrix();

	/******************left leg talve***********************/
	glBindTexture(GL_TEXTURE_2D, aav_white_texture);
	glTranslatef(0.0f, -0.4f, 0.0f);
	glRotatef((GLfloat)aav_giToeLeft, 1.0f, 0.0f, 0.0f);
	glTranslatef(0.0f, -0.2f, 0.2f);
	glScalef(0.5f, 0.4f, 1.2f);
	glColor3f(0.5882f, 0.29411f, 0.0f);
	aav_gpquadric = gluNewQuadric();
	gluSphere(aav_gpquadric, 0.3f, 20, 20);

	glPopMatrix();
	glPopMatrix();

	aav_translatemove = aav_translatemove + 0.001f;
	if (aav_translatemove >= 12.2f)
	{
		aav_translatemove = -12.0f;
	}
}

void show_list(list_t* p_list, const char* msg)
{
	node_t* p_run;
	if (msg)
	{
		puts(msg);
	}
	fprintf(aav_gpFile, "\n[BEG]<->");
	for (p_run = p_list->next; p_run != p_list; p_run = p_run->next)
	{
		fprintf(aav_gpFile, "[%d-%f-%f-braanchlength:%f]<->", p_run->data, p_run->leavesLeft, p_run->leavesRight, p_run->branchLength);
	}
	fprintf(aav_gpFile, "[END]\n");
}

void Unitialize(void)
{
	//Code
	if (aav_gbFullScreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);

		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(aav_ghwnd, &aav_gwpPrev);
		SetWindowPos(aav_ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);

		if (wglGetCurrentContext() == aav_ghrc)
		{
			wglMakeCurrent(NULL, NULL);
		}
		if (aav_ghrc)
		{
			wglDeleteContext(aav_ghrc);
			aav_ghrc = NULL;
		}
		if (aav_ghrc)
		{
			wglDeleteContext(aav_ghrc);
			aav_ghrc = NULL;
		}
		if (aav_ghdc)
		{
			ReleaseDC(aav_ghwnd, aav_ghdc);
			aav_ghdc = NULL;
		}
	}

	if (aav_gpFile)
	{
		fprintf(aav_gpFile, "Closing to Open LogFile.txt SuccesFull Completed \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

	if (aav_tree_texture)
		glDeleteTextures(1, &aav_tree_texture);
	if (aav_shirt_texture)
		glDeleteTextures(1, &aav_shirt_texture);
	if (aav_pant_texture)
		glDeleteTextures(1, &aav_pant_texture);
	if (aav_book_texture)
		glDeleteTextures(1, &aav_book_texture);
	if(aav_stone_texture)
		glDeleteTextures(1,&aav_stone_texture);
	if (aav_armyBuilding_texture)
		glDeleteTextures(1,&aav_armyBuilding_texture);
	if (aav_brownGround_texture)
		glDeleteTextures(1,&aav_brownGround_texture);
	if (aav_seenSecondGround_texture)
		glDeleteTextures(1,&aav_seenSecondGround_texture);
	if (aav_tree_texture)
		glDeleteTextures(1,&aav_tree_texture);
	if (aav_tree_textureNew)
		glDeleteTextures(1,&aav_tree_textureNew);	
	if (aav_treeTextureNew1)
		glDeleteTextures(1,&aav_treeTextureNew1);
	if (aav_bird_texture)
		glDeleteTextures(1,&aav_bird_texture);
	if (aav_wings_texture)
		glDeleteTextures(1,&aav_wings_texture);
	if (aav_grass_texture_flag)
		glDeleteTextures(1,&aav_grass_texture_flag);
	if (aav_sky_texture_flag)
		glDeleteTextures(1,&aav_sky_texture_flag);
	if (aav_flag)
		glDeleteTextures(1,&aav_flag);
	if (aav_stone_texture)
		glDeleteTextures(1,&aav_stone_texture);
	if (aav_steel_texture)
		glDeleteTextures(1,&aav_steel_texture);
	if (aav_seenFirestGround_texture)
		glDeleteTextures(1,&aav_seenFirestGround_texture);
	if (aav_seenFirestSky_texture)
		glDeleteTextures(1,&aav_seenFirestSky_texture);
	if (aav_aryDress_texture)
		glDeleteTextures(1,&aav_aryDress_texture);
	
	if (p_list)
	{
		destroy_list(&p_list);
		p_list = NULL;
	}
}


