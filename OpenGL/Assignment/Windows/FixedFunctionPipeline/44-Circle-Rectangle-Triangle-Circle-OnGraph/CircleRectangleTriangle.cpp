#include <windows.h>
#include "CircleRectangleTriangle.h"
#include <stdlib.h>
#include <stdio.h>
#include <gl/gl.h>
#include <math.h>
#include <gl/glu.h> // graphic utality 

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define PI  3.14

//Callback Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Gloabal Variable 
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;
FILE* pgFile = NULL;
bool gbActiveWindow = false;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
// For Calculating the x,y Corcidinate and Center of Triangle To Plot the InCircle
GLdouble xcenterCor;
GLdouble ycenterCor;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	void Initialize(void);
	void Display(void);

	//local Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Aniruddha");
	bool bDone = false;
	INT i_aav_y, i_aav_x;
	INT i_aav_Height, i_aav_Width;

	//Code
	//File IO fileOpen 
	if (fopen_s(&pgFile, "LogFile.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("fopen_s Fail to Open LogFile.txt"), TEXT("Message"), MB_OK | MB_ICONEXCLAMATION);
		exit(0);
	}
	else
	{
		fprintf(pgFile, "Success to Open LogFile.txt \n");
	}

	//wndclass Registration
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	i_aav_y = GetSystemMetrics(SM_CYSCREEN);
	i_aav_x = GetSystemMetrics(SM_CXSCREEN);

	i_aav_Width = 800;
	i_aav_Height = 600;
	i_aav_x = (i_aav_x / 2) - (i_aav_Width / 2);
	i_aav_y = (i_aav_y / 2) - (i_aav_Height / 2);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Aniruddha Avinash Vanjari : CircleRectangleTriangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		i_aav_x,
		i_aav_y,
		i_aav_Width,
		i_aav_Height,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	Initialize();

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//GameLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//Here Should You Call Update Function For OpenGL Rendering
				//Here Should You Call Display Function For OpenGL Rendering
				Display();
			}
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Function Declaration
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void Unitialize(void);
	//Code
	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
	{	switch (wParam)
	{
	case VK_ESCAPE:
		DestroyWindow(hwnd);
		break;
	case 0x46:
	case 0x66:
		ToggleFullScreen();
		break;
	default:
		break;
	}
	}
	break;
	case  WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
	{
		Unitialize();
		PostQuitMessage(0);
	}
	break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	//Code
	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & (WS_OVERLAPPEDWINDOW))
		{
			if (GetWindowPlacement(ghwnd, &wpPrev)
				&& GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)
				)
			{
				SetWindowLong(ghwnd,
					GWL_STYLE,
					dwStyle & (~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//Function Definition
void Initialize(void)
{
	//Function Declaration
	void ReSize(int, int);

	//variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	INT iPixelFormatIndex;
	
	//Code
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		fprintf(pgFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(ghwnd);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(pgFile, "SetPixelFormat() Failed\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(pgFile, "wglCreateContext() Failed\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(pgFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ReSize(WIN_WIDTH, WIN_HEIGHT); // warmup call to resize
}

void ReSize(int width, int  height)
{
	//Code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
}

void Display(void)
{
	//Function Ceclaration
	void Triangle(GLfloat);
	void Circle(GLfloat);
	void OutCircle(GLfloat);
	void Rectangle(GLfloat);
	//varaible declaration and Some Vaiable are Initaialized
	GLint Line = 0.0f;
	GLfloat fLine = 0.5f;
	GLfloat side;
	GLfloat Squareside;
	GLfloat OutSideCircleR;
	GLfloat r; //  Radius of inCircle 
	GLfloat fXPostiveLine = 0.0f;
	GLfloat fXNegativeLine = 0.0f;
	GLfloat fYPostiveLine = 0.0f;
	GLfloat fYNegativeLine = 0.0f;

	GLdouble a; //Consider side 1
	GLdouble b; //Consider side 2
	GLdouble c; //Consider side 3
	
	//Code
	a = sqrt(((0.5 - (-0.5)) * (0.5 - (-0.5))) + (((-0.5) - (-0.5)) * ((-0.5) - (-0.5)))); //1
	b = sqrt(((0.0 - 0.5) * (0.0 - 0.5)) + ((0.5 - (-0.5)) * (0.5 - (-0.5)))); //1.118033989
	c = sqrt((((-0.5) - 0.0) * ((-0.5) - 0.0)) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); //1.118033989
	fprintf(pgFile, "a = %lf\n", a);
	fprintf(pgFile, "b = %lf\n", b);
	fprintf(pgFile, "c = %lf\n", c);

	//Formula For x,y Cordinate for Center Point of Triangle To Draw the Circle.
	xcenterCor = ((a * (0)) + (b * (-0.5)) + (c * (0.5))) / (a + b + c);
	ycenterCor = ((a * (0.5f)) + (b * (-0.5f)) + (c * (-0.5f))) / (a + b + c);
	fprintf(pgFile, "xcenterCor = %lf\n", xcenterCor);
	fprintf(pgFile, "ycenterCor = %lf\n\n", ycenterCor);

	side = (a + b + c) / 2;
	r = sqrt(side * (side - a) * (side - b) * (side - c)) / side;  //Formmula For inCircle or inscribed Circle Radius. 

	//Circle OutSide Rectangle
	Squareside = sqrt((((-0.5) - (-0.5)) * ((-0.5) - (-0.5))) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); // We Get The Length of One Size of Square
	OutSideCircleR = (Squareside * sqrt(2)) / 2; // We Get Radius of Circle 
	glClear((GL_COLOR_BUFFER_BIT));
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glTranslatef(0.0f, 0.0f, -3.0f);
	
	glBegin(GL_LINES);   // Use to Draw the Lines
	glColor3f(1.0f, 0.0f, 0.0f);
	/*Centeral Line Drawing*/
	glVertex3f(-3.0f,0.0f,0.0f); 
	glVertex3f(3.0f, 0.0f, 0.0f);
	glEnd();

	//Above x-axis 20 Line Drawing
	glBegin(GL_LINES);
	for (Line = 0; Line < 20; Line++)
	{
		fYPostiveLine = fYPostiveLine + 0.06f;
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-3.0f, fYPostiveLine, 0.0f);
		glVertex3f(3.0f, fYPostiveLine, 0.0f);
		
	}
	glEnd();
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	//Bellow x-axis 20 Line Drawing
	glBegin(GL_LINES);
	for (Line = 0; Line < 20; Line++)
	{
		fYNegativeLine = fYNegativeLine - 0.06f;
		   // Use to Draw the Lines
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-3.0f, fYNegativeLine, 0.0f);
		glVertex3f(3.0f, fYNegativeLine, 0.0f);
		
	}
	glEnd();

	//////////////////////////////////////////////////////////////////////////////

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	glBegin(GL_LINES);   
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -3.0f, 0.0f);
	glVertex3f(0.0f, 3.0f, 0.0f);
	glEnd();

	
	// x-axis Postive  20 Line Drawing
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	for (Line = 0; Line < 20; Line++)
	{
		fXPostiveLine = fXPostiveLine + 0.106f;
		glVertex3f(fXPostiveLine, -3.0f, 0.0f);
		glVertex3f(fXPostiveLine, 3.0f, 0.0f);
	}
	glEnd();

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	// x-axis Negative 20 Line Drawing
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	for (Line = 0; Line < 20; Line++)
	{
		fXNegativeLine = fXNegativeLine - 0.106f;
		glVertex3f(fXNegativeLine, -3.0f, 0.0f);
		glVertex3f(fXNegativeLine, 3.0f, 0.0f);
	}
	glEnd();

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	glPointSize(5.0f); //Setting the Size of Point 

	//Center Point "O"
	glBegin(GL_POINTS);   
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f); // At the Center Cordinates
	glEnd();

	Triangle(fLine);
	glPointSize(1.0f);
	Circle(r);
	
	Rectangle(fLine);
	OutCircle(OutSideCircleR);
	SwapBuffers(ghdc);
	
}
void Triangle(GLfloat Line)
{
	glBegin(GL_LINES);
	glVertex3f(0.0f, Line, 0.0f);
	glVertex3f(-Line, -Line, 0.0f);

	glVertex3f(-Line, -Line, 0.0f);
	glVertex3f(Line, -Line, 0.0f);

	glVertex3f(Line, -Line, 0.0f);
	glVertex3f(0.0f, Line, 0.0f);
	glEnd();
}

void Rectangle(GLfloat Line)
{
	glBegin(GL_LINES);
	glVertex3f(Line, Line, 0.0f);
	glVertex3f(-Line, Line, 0.0f);

	glVertex3f(-Line, Line, 0.0f);
	glVertex3f(-Line, -Line, 0.0f);

	glVertex3f(-Line, -Line, 0.0f);
	glVertex3f(Line, -Line, 0.0f);

	glVertex3f(Line, -Line, 0.0f);
	glVertex3f(Line, Line, 0.0f);
	glEnd();
}

void Circle(GLfloat fRadius)
{
	GLfloat angle = 0;
	glBegin(GL_POINTS);
	for (angle = 0; angle < 2 * PI; angle = angle + 0.001)
	{
		glVertex3f(fRadius * sin(angle)+ xcenterCor, fRadius * cos(angle)+ycenterCor, 0.0f);
	}
	glEnd();
}
void OutCircle(GLfloat fRadius)
{
	GLfloat angle = 0;
	glBegin(GL_POINTS);
	for (angle = 0; angle < 2 * PI; angle = angle + 0.001)
	{
		glVertex3f(fRadius * sin(angle), fRadius * cos(angle), 0.0f);
	}
	glEnd();
}


void Unitialize(void)
{
	//Code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);

		if (wglGetCurrentContext() == ghrc)
		{
			wglMakeCurrent(NULL, NULL);
		}
		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

	}

	if (pgFile)
	{
		fprintf(pgFile, "Closing to Open LogFile.txt SuccesFull Completed \n");
		fclose(pgFile);
		pgFile = NULL;
	}
}


