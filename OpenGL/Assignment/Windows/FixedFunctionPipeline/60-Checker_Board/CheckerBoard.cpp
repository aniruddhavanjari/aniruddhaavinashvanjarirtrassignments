#include <windows.h>
#include "CheckerBoard.h"
#include <stdlib.h>
#include <stdio.h>
#include <gl/gl.h>
#include <math.h>
#include <gl/glu.h>

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

#define AAV_WIN_WIDTH 800
#define AAV_WIN_HEIGHT 600
#define CHECKER_IMAGE_WIDTH 64
#define CHECKER_IMAGE_HEIGHT 64

//Callback Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Gloabal Variable 
DWORD aav_dwStyle;
WINDOWPLACEMENT aav_gwpPrev = { sizeof(WINDOWPLACEMENT) };
bool aav_gbFullScreen = false;
HWND aav_ghwnd = NULL;
FILE* aav_gpFile = NULL;
bool aav_gbActiveWindow = false;
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

GLubyte aav_checkImage[CHECKER_IMAGE_WIDTH][CHECKER_IMAGE_HEIGHT][4];

GLuint aav_uiTextImage;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	void Initialize(void);
	void Display(void);

	//local Variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szAppName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	INT aav_iHeight, aav_iWidth;

	//Code
	//File IO fileOpen 
	if (fopen_s(&aav_gpFile, "LogFile.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("fopen_s Fail to Open LogFile.txt"), TEXT("Message"), MB_OK | MB_ICONEXCLAMATION);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Success to Open LogFile.txt \n");
	}

	//wndclass Registration
	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.lpszClassName = aav_szAppName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_iWidth = 800;
	aav_iHeight = 600;
	aav_ix = (aav_ix / 2) - (aav_iWidth / 2);
	aav_iy = (aav_iy / 2) - (aav_iHeight / 2);

	RegisterClassEx(&aav_wndclass);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szAppName,
		TEXT("Aniruddha Avinash Vanjari : Checker Board"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		aav_iWidth,
		aav_iHeight,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	aav_ghwnd = aav_hwnd;

	Initialize();

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	//GameLoop
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
			{
				aav_bDone = true;
			}
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			if (aav_gbActiveWindow == true)
			{
				Display();
			}
		}
	}

	return((int)aav_msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void Unitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		aav_gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
	{	switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;
		default:
			break;	
		}
	}
	break;
	case  WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
	{
		Unitialize();
		PostQuitMessage(0);
	}
	break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	MONITORINFO aav_mi = { sizeof(MONITORINFO) };

	if (aav_gbFullScreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & (WS_OVERLAPPEDWINDOW))
		{
			if (GetWindowPlacement(aav_ghwnd, &aav_gwpPrev)
				&& GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi)
				)
			{
				SetWindowLong(aav_ghwnd,
					GWL_STYLE,
					aav_dwStyle & (~WS_OVERLAPPEDWINDOW));
				SetWindowPos(aav_ghwnd,
					HWND_TOP,
					aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.top,
					aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		aav_gbFullScreen = true;

	}
	else
	{
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(aav_ghwnd, &aav_gwpPrev);
		SetWindowPos(aav_ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		aav_gbFullScreen = false;
	}
}

void Initialize(void)
{
	//Function declaration
	void ReSize(int, int);
	void loadGLTexture(void);

	//variable Declaration
	PIXELFORMATDESCRIPTOR aav_pfd;
	INT aav_iPixelFormatIndex;
	
	//Code
	aav_ghdc = GetDC(aav_ghwnd);
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cDepthBits = 32;								//6.
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);

	if (aav_iPixelFormatIndex == 0)
	{
		fprintf(aav_gpFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(aav_ghwnd);
	}
	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == FALSE)
	{
		fprintf(aav_gpFile, "SetPixelFormat() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		fprintf(aav_gpFile, "wglCreateContext() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == FALSE)
	{
		fprintf(aav_gpFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(aav_ghwnd);
	}

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_TEXTURE_2D);
	loadGLTexture();

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	ReSize(AAV_WIN_WIDTH, AAV_WIN_HEIGHT);
}

void loadGLTexture(void)
{
	//Fucntion declaration
	void MakeCheckImage(void);

	MakeCheckImage();
	glGenTextures(1,&aav_uiTextImage);
	glBindTexture(GL_TEXTURE_2D, aav_uiTextImage);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);


	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECKER_IMAGE_WIDTH, CHECKER_IMAGE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, aav_checkImage);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

void MakeCheckImage(void)
{
	//variable Declaration
	int aav_i, aav_j, aav_c;
	//Code
	for (aav_i = 0; aav_i < CHECKER_IMAGE_HEIGHT; aav_i++)
	{
		for (aav_j = 0; aav_j < CHECKER_IMAGE_WIDTH; aav_j++)
		{
			aav_c = (((aav_i & 0x8) == 0) ^ ((aav_j & 0x8) == 0)) * 255;
			aav_checkImage[aav_i][aav_j][0] = (GLubyte)aav_c;
			aav_checkImage[aav_i][aav_j][1] = (GLubyte)aav_c;
			aav_checkImage[aav_i][aav_j][2] = (GLubyte)aav_c;
			aav_checkImage[aav_i][aav_j][3] = 255;
		}
	}
}

void ReSize(int aav_iwidth, int  aav_iheight)
{
	//Code
	if (aav_iheight == 0)
		aav_iheight = 1;

	glViewport(0, 0, (GLsizei)aav_iwidth, (GLsizei)aav_iheight);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0f, (GLfloat)aav_iwidth / (GLfloat)aav_iheight, 0.1f, 30.0f);
}

void Display(void)
{
 	
	//Code
	glClear((GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));
	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	glTranslatef(0.0f,0.0f,-3.6f);
	
	glBindTexture(GL_TEXTURE_2D, aav_uiTextImage);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f, -1.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-2.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, aav_uiTextImage);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(2.41421f, 1.0f, -1.41421f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(2.41421f, -1.0f, -1.41421f);
	glEnd();
	
	SwapBuffers(aav_ghdc);
}


void Unitialize(void)
{
	//Code
	if (aav_gbFullScreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);

		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(aav_ghwnd, &aav_gwpPrev);
		SetWindowPos(aav_ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);

		if (wglGetCurrentContext() == aav_ghrc)
		{
			wglMakeCurrent(NULL, NULL);
		}
		if (aav_ghrc)
		{
			wglDeleteContext(aav_ghrc);
			aav_ghrc = NULL;
		}
		if (aav_ghrc)
		{
			wglDeleteContext(aav_ghrc);
			aav_ghrc = NULL;
		}
		if (aav_ghdc)
		{
			ReleaseDC(aav_ghwnd, aav_ghdc);
			aav_ghdc = NULL;
		}

	}

	if (aav_gpFile)
	{
		fprintf(aav_gpFile, "Closing to Open LogFile.txt SuccesFull Completed \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

	if(aav_uiTextImage)
		glDeleteTextures(1, &aav_uiTextImage);
}


