#include <windows.h>
//Callback Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Gloabal Variable 
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;


//WinMain
int WINAPI WinMain(HINSTANCE hInstance , HINSTANCE hPrevInstance, LPSTR lpszCmdLine , int iCmdShow)
{
	//local Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Aniruddha");
	INT i_aav_y, i_aav_x;
	INT i_aav_Height, i_aav_Width;
	
	//wndclass Registration
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	i_aav_y = GetSystemMetrics(SM_CYSCREEN);
	i_aav_x = GetSystemMetrics(SM_CXSCREEN);

	i_aav_Width = 800;
	i_aav_Height = 600;
	i_aav_x = (i_aav_x / 2) - (i_aav_Width / 2);
	i_aav_y = (i_aav_y / 2) - (i_aav_Height / 2);
	
	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName,
		TEXT("Aniruddha Avinash Vanjari"),
		WS_OVERLAPPEDWINDOW,
		i_aav_x,
		i_aav_y,
		i_aav_Width,
		i_aav_Height,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam , LPARAM lParam)
{
	void ToggleFullScreen(void);

	switch (iMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
		switch (wParam) 
		{
		case 0x46:
		case 0x66:
			ToggleFullScreen();
			break;

		}
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	//Local variable eclaration
	MONITORINFO mi = {sizeof(MONITORINFO)};
	//Code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & (WS_OVERLAPPEDWINDOW))
		{
			if (
				GetWindowPlacement(ghwnd, &wpPrev) 
				&& 
				GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)
				)
			{
				SetWindowLong(ghwnd,
					GWL_STYLE,
					dwStyle&(~WS_OVERLAPPEDWINDOW));

				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}

		ShowCursor(false);
		gbFullScreen = true;
	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE |
			SWP_NOOWNERZORDER | SWP_NOZORDER |
			SWP_FRAMECHANGED);

		ShowCursor(true);
		gbFullScreen = false;
	}
}
