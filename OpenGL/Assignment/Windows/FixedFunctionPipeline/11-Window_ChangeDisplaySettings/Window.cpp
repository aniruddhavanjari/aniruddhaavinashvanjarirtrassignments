#include <windows.h>
#define WIN_WIDTH 800
#define WIN_HEIGHT 600
//Callback Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Gloabal Variable 
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;
int gi_aav_x, gi_aav_y;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//local Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Aniruddha");
	INT i_aav_y, i_aav_x;
	INT i_aav_Height, i_aav_Width;

	//wndclass Registration
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Getting System Display Information and Centering the Window
	i_aav_y = GetSystemMetrics(SM_CYSCREEN);
	i_aav_x = GetSystemMetrics(SM_CXSCREEN);

	i_aav_Width = 800;
	i_aav_Height = 600;
	i_aav_x = (i_aav_x / 2) - (i_aav_Width / 2);
	i_aav_y = (i_aav_y / 2) - (i_aav_Height / 2);
	gi_aav_x = i_aav_x;
	gi_aav_y = i_aav_y;
	RegisterClassEx(&wndclass);

	hwnd = CreateWindow(szAppName,
		TEXT("Aniruddha Avinash Vanjari"),
		WS_OVERLAPPEDWINDOW,
		i_aav_x,
		i_aav_y,
		i_aav_Width,
		i_aav_Height,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	DEVMODE devMod = { sizeof(DEVMODE) };

	//Getting Display Information And Filling devMode Structure
	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &devMod);

	switch (iMsg)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x46:
		case 0x66:	
			if (gbFullScreen == false)
			{
				dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
				ChangeDisplaySettings(&devMod, CDS_FULLSCREEN);
				SetWindowLong(hwnd,GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW | WS_VISIBLE);
				SetWindowPos(hwnd, HWND_TOP, 0, 0, devMod.dmPelsWidth,devMod.dmPelsHeight, SWP_NOZORDER | SWP_FRAMECHANGED);
				ShowCursor(FALSE);
				gbFullScreen = true;
			} 
			else
			{
				
				ChangeDisplaySettings(NULL, 0);
				SetWindowLong(hwnd, GWL_STYLE, dwStyle|WS_OVERLAPPEDWINDOW | WS_VISIBLE);
				SetWindowPos(hwnd, HWND_TOP,gi_aav_x,gi_aav_y,WIN_WIDTH,WIN_HEIGHT, SWP_NOZORDER | SWP_FRAMECHANGED);
				ShowCursor(TRUE);
				gbFullScreen = false;
			}
		}
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}
