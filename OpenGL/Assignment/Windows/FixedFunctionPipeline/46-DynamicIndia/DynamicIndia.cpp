#include <windows.h>
#include "DynamicIndia.h"
#include <stdlib.h>
#include <stdio.h>
#include <gl/gl.h>
#include <math.h>
#include <gl/glu.h> // graphic utality 

#pragma comment(lib,"OpenGL32.lib")
#pragma comment(lib,"GLU32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//Callback Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


//Gloabal Variable 
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
bool gbFullScreen = false;
HWND ghwnd = NULL;
FILE* pgFile = NULL;
bool gbActiveWindow = false;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
HANDLE ghLoadResource;

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Declaration
	void Initialize(void);
	void Display(void);
	BOOL PlayResource(HINSTANCE,LPSTR);
	

	//local Variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("Aniruddha");
	bool bDone = false;
	INT i_aav_y, i_aav_x;
	INT i_aav_Height, i_aav_Width;

	//Code
	//File IO fileOpen 
	if (fopen_s(&pgFile, "LogFile.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("fopen_s Fail to Open LogFile.txt"), TEXT("Message"), MB_OK | MB_ICONEXCLAMATION);
		exit(0);
	}
	else
	{
		fprintf(pgFile, "Success to Open LogFile.txt \n");
	}

	//wndclass Registration
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbWndExtra = 0;
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	i_aav_y = GetSystemMetrics(SM_CYSCREEN);
	i_aav_x = GetSystemMetrics(SM_CXSCREEN);

	i_aav_Width = 800;
	i_aav_Height = 600;
	i_aav_x = (i_aav_x / 2) - (i_aav_Width / 2);
	i_aav_y = (i_aav_y / 2) - (i_aav_Height / 2);

	RegisterClassEx(&wndclass);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Aniruddha Avinash Vanjari : DynamicIndia"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		i_aav_x,
		i_aav_y,
		i_aav_Width,
		i_aav_Height,
		NULL,
		NULL,
		hInstance,
		NULL
	);

	ghwnd = hwnd;

	Initialize();
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//GameLoop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				//Here Should You Call Update Function For OpenGL Rendering
				//Here Should You Call Display Function For OpenGL Rendering
				Display();
			}
		}
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	void ToggleFullScreen(void);
	void ReSize(int, int);
	void Unitialize(void);

	switch (iMsg)
	{
	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		ReSize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
	{	switch (wParam)
	{
	case VK_ESCAPE:
		DestroyWindow(hwnd);
		break;
	case 0x46:
	case 0x66:
		ToggleFullScreen();
		break;
	default:
		break;
	}
	}
	break;
	case  WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
	{
		Unitialize();
		PostQuitMessage(0);
	}
	break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen()
{
	MONITORINFO mi = { sizeof(MONITORINFO) };

	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & (WS_OVERLAPPEDWINDOW))
		{
			if (GetWindowPlacement(ghwnd, &wpPrev)
				&& GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi)
				)
			{
				SetWindowLong(ghwnd,
					GWL_STYLE,
					dwStyle & (~WS_OVERLAPPEDWINDOW));
				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
		gbFullScreen = true;

	}
	else
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
		gbFullScreen = false;
	}
}

//Function Celaration
void Initialize(void)
{
	//Function Declaration.
	void ReSize(int, int);
	

	//variable Declaration
	PIXELFORMATDESCRIPTOR pfd;
	INT iPixelFormatIndex;
	//Code
	PlaySound(MAKEINTRESOURCE(MYWAVE), GetModuleHandle(NULL), SND_RESOURCE |SND_ASYNC);
	ghdc = GetDC(ghwnd);
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0)
	{
		fprintf(pgFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(ghwnd);
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(pgFile, "SetPixelFormat() Failed\n");
		DestroyWindow(ghwnd);
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		fprintf(pgFile, "wglCreateContext() Failed\n");
		DestroyWindow(ghwnd);
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(pgFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(ghwnd);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	ReSize(WIN_WIDTH, WIN_HEIGHT); // warmup call to resize
}

void ReSize(int width, int  height)
{
	//Code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void Display(void)
{
	
	void BharatViman(void);

	//Variable 

	static GLfloat I = -2.5f;
	static GLfloat N = 2.5f;
	static GLfloat D = 0.0f;
	static GLfloat ISend = -2.5f;
	static GLfloat A = 1.5f;
	static GLfloat G = 0.0f;
	static GLfloat B = 0.0f;

	/*static GLfloat I = 0.0f;
	static GLfloat N = 0.0f;
	static GLfloat D = 1.0f;
	static GLfloat ISend = 0.0f;
	static GLfloat A = 0.0f;*/
	static GLfloat PlaneTranslate = -2.0f;

	static GLfloat T1 = 0.0f;

	GLfloat size = 0.3f;
	static GLfloat  angle = 0.0f;
	static GLfloat  angle1 = 360.0f;
	static GLfloat angle3 = 90.0f;
	static GLfloat angle4 = -90.0f;
	GLint flag = 0;
	//Code
	glClear((GL_COLOR_BUFFER_BIT));
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(I, 0.0f, -3.0f);
	//I
	glBegin(GL_QUADS);
	//Top 
	glColor3f(1.0f, 0.3921f, 0.0f);
	glVertex3f(-1.1f, 0.8f, 0.0f);
	glVertex3f(-1.4f, 0.8f, 0.0f);
	glVertex3f(-1.4f, 0.7f, 0.0f);
	glVertex3f(-1.1f, 0.7f, 0.0f);

	//Mid
	glColor3f(1.0f, 0.3921f, 0.0f);
	glVertex3f(-1.2f, 0.7f, 0.0f);
	glVertex3f(-1.3f, 0.7f, 0.0f);
	glColor3f(0.0f, 0.3921f, 0.2352f);
	glVertex3f(-1.3f, -0.8f, 0.0f);
	glVertex3f(-1.2f, -0.8f, 0.0f);

	//Bottom
	glColor3f(0.0f, 0.3921f, 0.2352f);
	glVertex3f(-1.1f, -0.8f, 0.0f);
	glVertex3f(-1.4f, -0.8f, 0.0f);
	glVertex3f(-1.4f, -0.9f, 0.0f);
	glVertex3f(-1.1f, -0.9f, 0.0f);
	glEnd();
	I = I + 0.0001f;
	if (I >= 0.0f)
	{
		I = 0.0f;
	}

	if ( I >= 0.0f)
	{
		glLoadIdentity();
		glTranslatef(0.0f, N, -3.0f);
		//N
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(-0.8f, 0.8f, 0.0f);
		glVertex3f(-0.9f, 0.8f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(-0.9f, -0.9f, 0.0f);
		glVertex3f(-0.8f, -0.9f, 0.0f);

		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(-0.8f, 0.8f, 0.0f);
		glVertex3f(-0.9f, 0.8f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(-0.6f, -0.9f, 0.0f);
		glVertex3f(-0.5f, -0.9f, 0.0f);

		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(-0.5f, 0.8f, 0.0f);
		glVertex3f(-0.6f, 0.8f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(-0.6f, -0.9f, 0.0f);
		glVertex3f(-0.5f, -0.9f, 0.0f);
		glEnd();
		N = N - 0.0001f;
		if (N <= 0.0f)
		{
			N = 0.0f;
		}
	}

	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);
	//D
	//TOP
	//glColor3f(1.0f, 0.3921f, 0.0f); Kesri
	//glColor3f(0.0f, 0.3921f, 0.2352f);
	glBegin(GL_QUADS);
	glColor3f(D, G, 0.0f);
	glVertex3f(0.3f, 0.8f, 0.0f);
	glVertex3f(-0.3f, 0.8f, 0.0f);
	glVertex3f(-0.3f, 0.7f, 0.0f);
	glVertex3f(0.3f, 0.7f, 0.0f);

	//First Vertical
	glVertex3f(-0.08f, 0.7f, 0.0f);
	glVertex3f(-0.18f, 0.7f, 0.0f);
	glColor3f(0.0f, G, B);
	glVertex3f(-0.18f, -0.8f, 0.0f);
	glVertex3f(-0.08f, -0.8f, 0.0f);


	//Second
	glColor3f(D, G, 0.0f);
	glVertex3f(0.3f, 0.7f, 0.0f);
	glVertex3f(0.2f, 0.7f, 0.0f);
	glColor3f(0.0f, G, B);
	glVertex3f(0.2f, -0.8f, 0.0f);
	glVertex3f(0.3f, -0.8f, 0.0f);


	//Bottom
	glColor3f(0.0f, G, B);
	glVertex3f(0.3f, -0.8f, 0.0f);
	glVertex3f(-0.3f, -0.8f, 0.0f);
	glVertex3f(-0.3f, -0.9f, 0.0f);
	glVertex3f(0.3f, -0.9f, 0.0f);
	glEnd();
	D = D + 0.0000075f;
	if (D >= 1.0f)
	{
		D = 1.0f;
	}
	G = G + 0.0000045f;
	if (G >= 0.3921f)
	{
		G = 0.3921f;
	}
	B = B + 0.000005f;
	if (B >= 0.2352f)
	{
		B = 0.2352f;
	}

	if (N <= 0.0f)
	{
		glTranslatef(0.0f, ISend, 0.0f);
		//Second I
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(0.8f, 0.8f, 0.0f);
		glVertex3f(0.5f, 0.8f, 0.0f);
		glVertex3f(0.5f, 0.7f, 0.0f);
		glVertex3f(0.8f, 0.7f, 0.0f);

		//Mid
		glVertex3f(0.7f, 0.7f, 0.0f);
		glVertex3f(0.6f, 0.7f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(0.6f, -0.8f, 0.0f);
		glVertex3f(0.7f, -0.8f, 0.0f);

		//Bottom
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(0.8f, -0.8f, 0.0f);
		glVertex3f(0.5f, -0.8f, 0.0f);
		glVertex3f(0.5f, -0.9f, 0.0f);
		glVertex3f(0.8f, -0.9f, 0.0f);
		glEnd();
		ISend = ISend + 0.0001f;
		if (ISend >= 0)
		{
			ISend = 0.0f;
		}
	}

	if (ISend >= 0.0f)
	{
		glLoadIdentity();
		glTranslatef(A, 0.0f, -3.0f);
		//A
		//Left.
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(1.3f, 0.8f, 0.0f);
		glVertex3f(1.2f, 0.8f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(1.0f, -0.9f, 0.0f);
		glVertex3f(1.1f, -0.9f, 0.0f);

		//Mid
		//0.0333
		if (PlaneTranslate > 1.2f)
		{
			glColor3f(1.0f, 0.3921f, 0.0f);
			glVertex3f(1.295f, 0.0f, 0.0f);
			glVertex3f(1.205f, 0.0f, 0.0f);
			glVertex3f(1.203f, -0.033f, 0.0f);
			glVertex3f(1.30f, -0.033f, 0.0f);

			glColor3f(1.0f, 01.0f, 1.0f);
			glVertex3f(1.30f, -0.033f, 0.0f);
			glVertex3f(1.202f, -0.033f, 0.0f);
			glVertex3f(1.198f, -0.066f, 0.0f);
			glVertex3f(1.305f, -0.066f, 0.0f);

			glColor3f(0.0f, 0.3921f, 0.2352f);
			glVertex3f(1.305f, -0.066f, 0.0f);
			glVertex3f(1.198f, -0.066f, 0.0f);
			glVertex3f(1.194f, -0.099f, 0.0f);
			glVertex3f(1.35f, -0.099f, 0.0f);
		}

		//Right
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(1.2f, 0.8f, 0.0f);
		glVertex3f(1.3f, 0.8f, 0.0f);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(1.5f, -0.9f, 0.0f);
		glVertex3f(1.4f, -0.9f, 0.0f);
		A = A - 0.0001f;
		if (A <= 0)
		{
			A = 0.0f;
		}
		glEnd();
	}
	if (A == 0 && angle != 90.0f)
	{
		glLoadIdentity();
		glTranslatef(-1.0f, 1.0f - 0.0495f, -3.0f);
		glRotatef(angle, 0.0f, 0.0f, 1.0f);
		glTranslatef(-1.0f, 0.0f, 0.0f);

		glScalef(-0.5, -0.5f, -0.5f);

		BharatViman();
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(-0.033, -0.6f, 0.0f);
		glVertex3f(-0.099f, -0.6f, 0.0f);
		glVertex3f(-0.099f, -0.9f, 0.0f);
		glVertex3f(-0.033f, -0.9f, 0.0f);
		glEnd();

		angle = angle + 0.01f;
		if (angle > 90)
		{
			angle = 90.0f;
		}
	}
	/////////////////////////////////////////////////////////////////////////////
		//Stage = 2;
	if (A == 0 && angle1 != -90.0f)
	{
		glLoadIdentity();
		glTranslatef(-1.0f, -1.0495f, -3.0f);
		glRotatef(angle1, 0.0f, 0.0f, 1.0f);
		glTranslatef(-1.0f, 0.0f, 0.0f);
		glRotatef(180, 0.0f, 0.0f, 1.0f);
		glScalef(-0.5, -0.5f, -0.5f);

		BharatViman();
		glBegin(GL_QUADS);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(0.033f, -0.6f, 0.0f);
		glVertex3f(0.099f, -0.6f, 0.0f);
		glVertex3f(0.099f, -0.9f, 0.0f);
		glVertex3f(0.033f, -0.9f, 0.0f);
		glEnd();


		angle1 = angle1 - 0.01f;
		if (angle1 < 360 - 90)
		{
			angle1 = -90.0f;
		}
	}
	////////////////////////////////////////////////////////////////////////////////
		//Stage = 3;//0.0165
	if (A == 0)
	{
		glLoadIdentity();
		glTranslatef(PlaneTranslate, -0.0495f, -3.0f);
		glRotatef(90, 0.0f, 0.0f, 1.0f);
		glScalef(-0.5, -0.5f, -0.5f);
		BharatViman();
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.3921f, 1.2352f);
		glVertex3f(0.033f, -0.6f, 0.0f);
		glVertex3f(-0.033f, -0.6f, 0.0f);
		glVertex3f(-0.033f, -0.9f, 0.0f);
		glVertex3f(0.033f, -0.9f, 0.0f);
		glEnd();

		if (angle == 90 && PlaneTranslate < 1.6f)
		{
			glBegin(GL_QUADS);
			glColor3f(0.0f, 0.3921f, 0.2352f);
			glVertex3f(0.033f, -0.6f, 0.0f);
			glVertex3f(0.099f, -0.6f, 0.0f);
			glVertex3f(0.099f, -0.9f, 0.0f);
			glVertex3f(0.033f, -0.9f, 0.0f);
			glEnd();

			glBegin(GL_QUADS);
			glColor3f(1.0f, 0.3921f, 0.0f);
			glVertex3f(-0.033, -0.6f, 0.0f);
			glVertex3f(-0.099f, -0.6f, 0.0f);
			glVertex3f(-0.099f, -0.9f, 0.0f);
			glVertex3f(-0.033f, -0.9f, 0.0f);
			glEnd();
		}

		PlaneTranslate = PlaneTranslate + 0.000111;
		if (PlaneTranslate > 2.8f)
		{
			PlaneTranslate = 2.8f;
		}
	}

	//PlaneTranslate = 2.8f;
	//////////////////////////////////////////////////////////////////////////
	//Vimanas
	//Stage = 1.1;
	if (PlaneTranslate > 1.6f)
	{
		glLoadIdentity();
		glTranslatef(1.6f, 1.0f - 0.0495f, -3.0f);
		glRotatef(angle3, 0.0f, 0.0f, 1.0f);
		glTranslatef(-1.0f, 0.0f, 0.0f);

		glScalef(-0.5, -0.5f, -0.5f);

		BharatViman();
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.3921f, 0.0f);
		glVertex3f(-0.033, -0.6f, 0.0f);
		glVertex3f(-0.099f, -0.6f, 0.0f);
		glVertex3f(-0.099f, -0.9f, 0.0f);
		glVertex3f(-0.033f, -0.9f, 0.0f);
		glEnd();

		angle3 = angle3 + 0.01f;
		if (angle3 >= 180)
		{
			angle3 = 180.0f;
		}
	}
	/////////////////////////////////////////////////////////////////////////////
		//Stage = 2.1;
	if (PlaneTranslate > 1.6f)
	{
		glLoadIdentity();
		glTranslatef(1.6f, -1.0495f, -3.0f);
		glRotatef(angle4, 0.0f, 0.0f, 1.0f);
		glTranslatef(-1.0f, 0.0f, 0.0f);
		glRotatef(180, 0.0f, 0.0f, 1.0f);
		glScalef(-0.5, -0.5f, -0.5f);

		BharatViman();
		glBegin(GL_QUADS);
		glColor3f(0.0f, 0.3921f, 0.2352f);
		glVertex3f(0.033f, -0.6f, 0.0f);
		glVertex3f(0.099f, -0.6f, 0.0f);
		glVertex3f(0.099f, -0.9f, 0.0f);
		glVertex3f(0.033f, -0.9f, 0.0f);
		glEnd();


		angle4 = angle4 - 0.01f;
		if (angle4 <= -180)
		{
			angle4 = -180.0f;
		}
	}
	SwapBuffers(ghdc);
}



void Unitialize(void)
{
	//Code
	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);

		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | (WS_OVERLAPPEDWINDOW));
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);

		if (wglGetCurrentContext() == ghrc)
		{
			wglMakeCurrent(NULL, NULL);
		}
		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}

	}

	if (ghLoadResource)
	{
		FreeResource(ghLoadResource);
	}

	if (pgFile)
	{
		fprintf(pgFile, "Closing to Open LogFile.txt SuccesFull Completed \n");
		fclose(pgFile);
		pgFile = NULL;
	}
}

void BharatViman()
{
	void Circle(GLfloat r, GLfloat x, GLfloat y);

	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	glVertex3f(-0.4, -0.3f, 0.0f);
	glVertex3f(-0.45f, -0.3f, 0.0f);
	glVertex3f(-0.45f, -0.5f, 0.0f);
	glVertex3f(-0.4f, -0.5f, 0.0f);
	glEnd();

	glBegin(GL_QUADS);
	glVertex3f(0.45, -0.3f, 0.0f);
	glVertex3f(0.4f, -0.3f, 0.0f);
	glVertex3f(0.4f, -0.5f, 0.0f);
	glVertex3f(0.45f, -0.5f, 0.0f);
	glEnd();

	//Upper Triangle
	glColor3f(0.3921f, 0.7058f, 0.9019f);
	glBegin(GL_TRIANGLES);
	glColor3f(0.0f, 0.5f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glColor3f(0.33f, 0.8f, 1.0f);
	glVertex3f(0.5f, -0.5, 0.0f);
	glVertex3f(-0.5f, -0.5, 0.0f);
	glEnd();

	//Top Triangle
	glBegin(GL_TRIANGLES);
	glVertex3f(0.0f, 0.5f, 0.0f);
	glColor3f(0.0f, 0.5f, 1.0f);
	glVertex3f(0.08f, 0.2f, 0.0f);
	glVertex3f(-0.08f, 0.2f, 0.0f);
	glEnd();

	//Mid Left Triangle 
	glBegin(GL_TRIANGLES);
	glColor3f(0.3921f, 0.7058f, 0.9019f);
	glVertex3f(-0.1f, 0.1f, 0.0f);
	glColor3f(0.0f, 0.5f, 1.0f);
	glVertex3f(-0.2f, -0.2f, 0.0f);
	glVertex3f(-0.1f, -0.2f, 0.0f);
	glEnd();

	//Mid Right Triangle 
	glBegin(GL_TRIANGLES);
	glColor3f(0.33f, 0.8f, 1.0f);
	glVertex3f(0.1f, 0.1f, 0.0f);
	glColor3f(0.0f, 0.5f, 1.0f);
	glVertex3f(0.2f, -0.2f, 0.0f);
	glVertex3f(0.1f, -0.2f, 0.0f);
	glEnd();

	//Upper Quad
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(-0.1, 0.2f, 0.0f);
	glVertex3f(0.1f, 0.2f, 0.0f);
	glVertex3f(0.1f, -0.2f, 0.0f);
	glVertex3f(-0.1f, -0.2f, 0.0f);
	glEnd();

	//Bottom Quad
	glColor3f(0.0f, 0.5f, 1.0f);
	glBegin(GL_QUADS);
	glVertex3f(0.2f, -0.5f, 0.0f);
	glVertex3f(-0.2f, -0.5f, 0.0f);
	glColor3f(0.3921f, 0.7058f, 0.9019f);
	glVertex3f(-0.3f, -0.6f, 0.0f);
	glVertex3f(0.3f, -0.6f, 0.0f);
	glEnd();

	//Innder Cabinate
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_QUADS);
	glVertex3f(-0.05, 0.1f, 0.0f);
	glVertex3f(0.051f, 0.1f, 0.0f);
	glVertex3f(0.05f, -0.1f, 0.0f);
	glVertex3f(-0.05f, -0.1f, 0.0f);
	glEnd();


	glLineWidth(1.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.0f, -0.25f, 0.0f);
	glVertex3f(0.0f, -0.45f, 0.0f);
	glVertex3f(0.0f, -0.6f, 0.0f);
	glEnd();


	glPointSize(5.0f);
	glBegin(GL_POINTS);
	glVertex3f(0.0f, 0.0F, 0.0f);
	glEnd();

	glColor3f(1.0f, 0.3f, 0.0f);
	Circle(0.03f, -0.135f, -0.1f);
	glColor3f(1.0f, 1.0f, 1.0f);
	Circle(0.02f, -0.135f, -0.1f);
	glColor3f(0.0f, 0.4f, 0.3f);
	Circle(0.01f, -0.135f, -0.1f);


	glColor3f(1.0f, 0.3f, 0.0f);
	Circle(0.03f, 0.135f, -0.1f);
	glColor3f(1.0f, 1.0f, 1.0f);
	Circle(0.02f, 0.135f, -0.1f);
	glColor3f(0.0f, 0.4f, 0.3f);
	Circle(0.01f, 0.135f, -0.1f);

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	//I
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.03f, -0.4f, 0.0f);
	glVertex3f(-0.03f, -0.4f, 0.0f);
	glVertex3f(0.03f, -0.38f, 0.0f);
	glVertex3f(0.03f, -0.42f, 0.0f);
	glVertex3f(-0.03f, -0.38f, 0.0f);
	glVertex3f(-0.03f, -0.42f, 0.0f);

	////A
	//glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(0.03f, -0.360f, 0.0f);
	glVertex3f(-0.03f, -0.346f, 0.0f);
	glVertex3f(0.03f, -0.330f, 0.0f);
	glVertex3f(-0.03f, -0.346f, 0.0f);
	glVertex3f(0.0f, -0.335f, 0.0f);
	glVertex3f(0.0f, -0.355f, 0.0f);

	////F
	//glColor3f(0.0f, 0.5f, 0.0f);
	glVertex3f(0.03f, -0.31f, 0.0f);
	glVertex3f(-0.03f, -0.31f, 0.0f);
	glVertex3f(-0.03f, -0.275f, 0.0f);
	glVertex3f(-0.03f, -0.31f, 0.0f);
	glVertex3f(0.0f, -0.285f, 0.0f);
	glVertex3f(0.0f, -0.31f, 0.0f);
	glEnd();

}

void Circle(GLfloat r, GLfloat x, GLfloat y)
{
	glBegin(GL_LINES);
	GLfloat angle = 0.0f;
	for (angle = 0.0f; angle < 2 * 3.14; angle = angle + 0.01f)
	{
		glVertex3f(x, y, 0.0f);
		glVertex3f(r * sin(angle) + x, r * cos(angle) + y, 0.0f);
	}
	glEnd();
}
