#include <windows.h>
#include <stdio.h>
#include "roboticArm.h"
#include  <gl\glew.h> // This must be include before GL.h
#include <gl\GL.h>
#include "vmath.h"
#include "Sphere-dotH.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL; 
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

mat4 aav_PerspectiveProjectionMatrix;

//Sphere Variable
GLfloat aav_sphere_vertices[1146];
GLfloat aav_sphere_normals[1146];
GLfloat aav_sphere_texture[764];
unsigned short aav_sphere_elements[2280];
GLuint aav_numSphereVertices;
GLuint aav_numSphereElements;

GLuint aav_gVao_sphere;
GLuint aav_Vbo_sphere_position;
GLuint aav_Vbo_sphere_color;
GLuint aav_Vbo_sphere_elements;

bool aav_bLight = false;

GLfloat aav_color[3];


GLuint aav_viewMatrixUniform;
GLuint aav_modelMatrixUniform;
GLuint aav_ProjectionMatrixUniform;
GLuint aav_colorUniform;


//statck 
mat4 matrixStack[10];
int stacktop = -1;
int stackSize = 10;
int top = -1;

GLfloat aav_sholderRotate = 0.0f;
GLfloat aav_elboRotate = 0.0f;


//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	

	//code
	if (fopen_s(&aav_gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("Aniruddha Avinash Vanjari : OpenGL Programmable Pipeline : RoboticArm"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	initialize();
	
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
					aav_bDone = true;
			}
		}
		
	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			aav_gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 's':
			aav_sholderRotate = aav_sholderRotate + 1.0f;
			break;
		case 'S':
			aav_sholderRotate = aav_sholderRotate - 1.0f;
			break;
		case 'e':
			aav_elboRotate = aav_elboRotate +1.0f;
			break;
		case 'E':
			aav_elboRotate = aav_elboRotate -1.0f;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO aav_mi;

	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
						SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// function prototype
	void uninitialize(void);
	void resize(int ,int);

	//variable decalrations 
	PIXELFORMATDESCRIPTOR aav_pfd;
	int aav_iPixelFormatIndex;

	//code
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;
	aav_pfd.cDepthBits = 32;

	aav_ghdc = GetDC(aav_ghwnd);

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);
	if (aav_iPixelFormatIndex == 0)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == false)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == false)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	GLenum aav_glew_error = glewInit();
	if (aav_glew_error != GLEW_OK)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL Enable Extensions 
	GLint  aav_numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &aav_numExtension);
	for (int i = 0; i < aav_numExtension; i++)
	{
		fprintf(aav_gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	//**VERTEX SHADER***
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform vec3 u_color;"\
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"out vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"	fong_ads_light = u_color;" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gVertexShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**FRAGMENT SHADER**
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"in vec3 fong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	FragColor = vec4(fong_ads_light,1.0f);" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(gFragmentShaderObject);


	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gFragmentShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//Post Linking Information	
	/**************************************/
	aav_modelMatrixUniform		= glGetUniformLocation(gShaderProgramObject, "u_model_matrix"		);
	aav_viewMatrixUniform		= glGetUniformLocation(gShaderProgramObject, "u_view_matrix"		);
	aav_ProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix"	);
	aav_colorUniform			= glGetUniformLocation(gShaderProgramObject, "u_color"				);

	/**************************************/

	//vertices array declation
	getSphereVertexData(aav_sphere_vertices, aav_sphere_normals, aav_sphere_texture, aav_sphere_elements);
	
	aav_numSphereVertices = getNumberOfSphereVertices();
	aav_numSphereElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &aav_gVao_sphere);
	glBindVertexArray(aav_gVao_sphere);

	//Record Sphere
	glGenBuffers(1, &aav_Vbo_sphere_position); 
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_vertices), aav_sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	

	//elements
	glGenBuffers(1, &aav_Vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements); // Elemtnes Drawing is Also Called As Index Drawing, Elements Drawing
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(aav_sphere_elements), aav_sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//Record Off / Pause 
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{

	//fuction declartion
	void update();
	void push(mat4);
	void pop(mat4 *);

	//variable declaration
	mat4 aav_modelMatrix;
	mat4 aav_viewMatrix;
	mat4 aav_translateMatrix;
	mat4 aav_rotationMatrix;
	mat4 aav_scaleMatrix;
	mat4 aav_popMatrix;

	aav_popMatrix = mat4::identity();
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 
	//triangle
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing
	aav_color[0] = 0.5f;
	aav_color[1] = 0.35f;
	aav_color[2] = 0.05f;
	glUniform3fv(aav_colorUniform,1, (GLfloat *)aav_color);	
	
	aav_modelMatrix = mat4::identity();
	aav_viewMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(1.0f, 0.0f, -12.0f);
	aav_rotationMatrix = vmath::rotate(aav_sholderRotate, 0.0f, 0.0f, 1.0f);
	aav_modelMatrix =  aav_rotationMatrix * aav_translateMatrix;

	push(aav_modelMatrix);

	aav_scaleMatrix = vmath::scale(2.0f, 0.5f, 1.0f);
	aav_modelMatrix = aav_modelMatrix * aav_scaleMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform,1,GL_FALSE, aav_modelMatrix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES,aav_numSphereElements,GL_UNSIGNED_SHORT,0);
	glBindVertexArray(0);

	//elbo
	pop(&aav_modelMatrix);
	
	aav_rotationMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(1.0f, 0.0f, 0.0f);
	aav_modelMatrix = aav_modelMatrix *  aav_translateMatrix;

	aav_rotationMatrix = vmath::rotate(aav_elboRotate, 0.0f, 0.0f, 1.0f);
	aav_translateMatrix = vmath::translate(1.0f, 0.0f, 0.0f);
	aav_modelMatrix = aav_modelMatrix * aav_rotationMatrix * aav_translateMatrix;
	
	aav_scaleMatrix = vmath::scale(2.0f, 0.5f, 1.5f);
	aav_modelMatrix = aav_modelMatrix * aav_scaleMatrix;
	
	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMatrix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);

	glUseProgram(0);

	SwapBuffers(aav_ghdc);
}


void push(mat4 pushMatrix)
{
	void uninitialize(void);

	if (top == stackSize)
	{
		fprintf(aav_gpFile, "Stack Full");
		uninitialize();
		DestroyWindow(aav_ghwnd);
	}
	top = top + 1;
	matrixStack[top] = pushMatrix;
}

void pop(mat4* popMatrix)
{
	void uninitialize(void);

	if (top == -1)
	{
		fprintf(aav_gpFile, "Stack Empty");
		uninitialize();
		DestroyWindow(aav_ghwnd);
	}
	*popMatrix = matrixStack[top];
	top = top - 1;
}


void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}



void uninitialize(void)
{
	//code 
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (aav_gVao_sphere)
	{
		glDeleteVertexArrays(1, &aav_gVao_sphere);
		aav_gVao_sphere = 0;
	}

	if (aav_Vbo_sphere_position)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_position);
		aav_Vbo_sphere_position = 0;
	}

	if (aav_Vbo_sphere_elements)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_elements);
		aav_Vbo_sphere_elements = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(aav_ghrc);
	aav_ghrc = NULL;

	ReleaseDC(aav_ghwnd, aav_ghdc);
	aav_ghdc = NULL;

	if (aav_gpFile)
	{
	fprintf(aav_gpFile, "Log File is Successfully Closed. \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}
}
