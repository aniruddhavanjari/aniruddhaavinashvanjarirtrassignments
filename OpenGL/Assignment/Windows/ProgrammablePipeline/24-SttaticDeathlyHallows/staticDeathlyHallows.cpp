#include <windows.h>
#include <stdio.h>
#include "staticDeathlyHallows.h"
#include  <gl\glew.h> // This must be include before GL.h
#include <gl\GL.h>
#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define PI  3.14

using namespace vmath;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL; 
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

GLuint aav_gVertexShaderObject;
GLuint aav_gFragmentShaderObject;
GLuint aav_gShaderProgramObject;

GLuint aav_gVao_triangle;
GLuint aav_gVbo_position_triangle;
GLuint aav_gVbo_color_triangle;

GLuint aav_gVao_square;
GLuint aav_gVbo_position_square;
GLuint aav_gVbo_color_square;

GLuint aav_gVao_line;
GLuint aav_gVbo_line_position;
GLuint aav_aVbo_line_color;

GLuint aav_gVao_circle;
GLuint aav_gVbo_circle_position;
GLuint aav_aVbo_circle_color;

GLuint aav_gVao_circleInner;
GLuint aav_gVbo_circleInner_position;
GLuint aav_aVbo_circleInner_color;

GLuint aav_gVao_point;
GLuint aav_gVbo_point_position;
GLuint aav_gVbo_point_color;

GLuint aav_mvp_MatrixUniform;

GLfloat aav_anglePyramid = 0.0f;
GLfloat aav_angleCube = 0.0f;

mat4 aav_PerspectiveProjectionMatrix;

GLfloat* aav_circleVertices;
GLfloat* aav_circleColor;

GLfloat* aav_circleVerticesInner;
GLfloat* aav_circleColorInner; 

GLfloat r;	   // radius of innder circle 
int count = 0; // Number of interation in Circle Loop

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	

	//code
	if (fopen_s(&aav_gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("Aniruddha Avinash Vanjari : OpenGL Programmable Pipeline : Static Deathly Hallows"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	initialize();
	
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
					aav_bDone = true;
			}
		}
		
	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			aav_gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO aav_mi;

	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
						SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// function prototype
	void uninitialize(void);
	void resize(int ,int);
	void Calculation(void);

	//variable decalrations 
	PIXELFORMATDESCRIPTOR aav_pfd;
	int aav_iPixelFormatIndex;

	//code
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;
	aav_pfd.cDepthBits = 32;

	aav_ghdc = GetDC(aav_ghwnd);

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);
	if (aav_iPixelFormatIndex == 0)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == false)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == false)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	GLenum aav_glew_error = glewInit();
	if (aav_glew_error != GLEW_OK)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL Enable Extensions 
	GLint  aav_numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &aav_numExtension);
	for (int i = 0; i < aav_numExtension; i++)
	{
		fprintf(aav_gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	//**VERTEX SHADER***
	aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 aav_u_mvpMatrix;" \
		"out vec4 aav_out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = aav_u_mvpMatrix * vPosition;" \
		"aav_out_color = vColor;" \
		"}";
	glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(aav_gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* aav_szBuffer = NULL;

	glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
					&aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**FRAGMENT SHADER**
	aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"in vec4 aav_out_color;" \
		"out vec4 aav_FragColor;" \
		"void main(void)" \
		"{" \
		"aav_FragColor = aav_out_color;" \
		"}";
	glShaderSource(aav_gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(aav_gFragmentShaderObject);


	aav_szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
					&aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	aav_gShaderProgramObject = glCreateProgram();

	glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_COLOR, "vColor");

	glLinkProgram(aav_gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	aav_szBuffer = NULL;
	
	glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//Post Linking Information
	aav_mvp_MatrixUniform =  glGetUniformLocation(aav_gShaderProgramObject, "aav_u_mvpMatrix");

	//vertices array declation
	const GLfloat aav_triangleVertices[] =
	{
	0.0f,0.5f,0.0f,
	-0.5f,-0.5f,0.0f,

	-0.5f,-0.5f,0.0f,
	0.5f,-0.5f,0.0f,

	0.5f,-0.5f,0.0f,
	0.0f,0.5f,0.0f,
	};

	const GLfloat aav_trianglecolors[] =
	{ 
	1.0f,1.0f,1.0f,
	1.0f,1.0f,1.0f,
	1.0f,1.0f,1.0f,
	1.0f,1.0f,1.0f,
	1.0f,1.0f,1.0f,
	1.0f,1.0f,1.0f
	};

	const GLfloat aav_lineVertex[] =
	{
		0.0f,-0.5f,0.0f,
		0.0f,0.5f,0.0f
	};

	const GLfloat aav_lineColor[] =
	{
		1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f
	};
	Calculation();

	glGenVertexArrays(1, &aav_gVao_triangle);
	glBindVertexArray(aav_gVao_triangle);

	//Record Triangle
	glGenBuffers(1, &aav_gVbo_position_triangle);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position_triangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_triangleVertices), aav_triangleVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &aav_gVbo_color_triangle);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_color_triangle);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_trianglecolors), aav_trianglecolors, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT,GL_FALSE,0,NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Line midle Line X and Y 
	glGenVertexArrays(1, &aav_gVao_line);
	glBindVertexArray(aav_gVao_line);

	glGenBuffers(1, &aav_gVbo_line_position);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_line_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_lineVertex), aav_lineVertex, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &aav_aVbo_line_color);
	glBindBuffer(GL_ARRAY_BUFFER, aav_aVbo_line_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_lineColor), aav_lineColor, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Inner Circle
	glGenVertexArrays(1, &aav_gVao_circleInner);
	glBindVertexArray(aav_gVao_circleInner);

	glGenBuffers(1, &aav_gVbo_circleInner_position);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_circleInner_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* count * 3, aav_circleVerticesInner, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &aav_aVbo_circleInner_color);
	glBindBuffer(GL_ARRAY_BUFFER, aav_aVbo_circleInner_color);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat)* count * 3, aav_circleColorInner, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Record Off / Pause 
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void Calculation(void)
{
	GLint Line = 0.0f;
	GLfloat fLine = 0.5f;
	GLfloat side;
	GLfloat Squareside;
	GLfloat OutSideCircleR;

	GLdouble a; //Consider side 1
	GLdouble b; //Consider side 2
	GLdouble c; //Consider side 3

	GLdouble xcenterCor;
	GLdouble ycenterCor;

	//Code
	a = sqrt(((0.5 - (-0.5)) * (0.5 - (-0.5))) + (((-0.5) - (-0.5)) * ((-0.5) - (-0.5)))); //1
	b = sqrt(((0.0 - 0.5) * (0.0 - 0.5)) + ((0.5 - (-0.5)) * (0.5 - (-0.5)))); //1.118033989
	c = sqrt((((-0.5) - 0.0) * ((-0.5) - 0.0)) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); //1.118033989
	
	//Formula For x,y Cordinate for Center Point of Triangle To Draw the Circle.
	xcenterCor = ((a * (0)) + (b * (-0.5)) + (c * (0.5))) / (a + b + c);
	ycenterCor = ((a * (0.5f)) + (b * (-0.5f)) + (c * (-0.5f))) / (a + b + c);

	side = (a + b + c) / 2;
	r = sqrt(side * (side - a) * (side - b) * (side - c)) / side;  //Formmula For inCircle or inscribed Circle Radius. 

	//Circle OutSide Rectangle
	Squareside = sqrt((((-0.5) - (-0.5)) * ((-0.5) - (-0.5))) + (((-0.5) - 0.5) * ((-0.5) - 0.5))); // We Get The Length of One Size of Square
	OutSideCircleR = (Squareside * sqrt(2)) / 2; // We Get Radius of Circle 

	
	for (GLfloat angle = 0; angle < 2 * PI; angle = angle + 0.001)
	{
		count = count + 1;
	}


	int i = 0; 
	aav_circleVerticesInner = (GLfloat*)malloc(sizeof(GLfloat) * count * 3);
	aav_circleColorInner = (GLfloat*)malloc(sizeof(GLfloat) * count * 3);
	for (GLfloat angle = 0; angle < 2 * PI; angle = angle + 0.001)
	{
		aav_circleVerticesInner[i] = r * sin(angle)  + +xcenterCor;
		aav_circleColorInner[i] = 1.0f;
		i = i + 1;
		aav_circleVerticesInner[i] = r * cos(angle)+ +ycenterCor;
		aav_circleColorInner[i] = 1.0f;
		i = i + 1;
		aav_circleVerticesInner[i] = 0.0f;
		aav_circleColorInner[i] = 1.0f;
		i = i + 1;
	}

}

void display(void)
{

	//fuction declartion
	void update();

	mat4 aav_scaMatrix;
	mat4 aav_translateMatrix;
	mat4 aav_modelViewMateix;
	mat4 aav_modelViewProjectMatrix;
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 
	//triangle
	glUseProgram(aav_gShaderProgramObject);

	//OpenGL Drawing
	glPointSize(1.0f);
	//Lines 
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);

	glBindVertexArray(aav_gVao_line);
	glDrawArrays(GL_LINES, 0, 84);
	glBindVertexArray(0);

	//rectangle
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform,1,GL_FALSE, aav_modelViewProjectMatrix);

	glBindVertexArray(aav_gVao_triangle);
	glDrawArrays(GL_LINES, 0, 8);
	glBindVertexArray(0);

	//square
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();
	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);

	glBindVertexArray(aav_gVao_square);
	glDrawArrays(GL_LINES, 0, 8);
	glBindVertexArray(0);
	
	//Outer circle
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
	glBindVertexArray(aav_gVao_circle);
	glDrawArrays(GL_POINTS, 0,count);
	glBindVertexArray(0);

	//Inner
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);

	glBindVertexArray(aav_gVao_circleInner);
	glDrawArrays(GL_POINTS, 0, count);
	glBindVertexArray(0);

	//Center Point 
	aav_modelViewMateix = mat4::identity();
	aav_modelViewProjectMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);
	glPointSize(5.0f);
	glBindVertexArray(aav_gVao_point);
	glDrawArrays(GL_POINTS, 0, 1);
	glBindVertexArray(0);


	glUseProgram(0);
	update();
	SwapBuffers(aav_ghdc);
}

void update()
{	
	aav_anglePyramid = aav_anglePyramid + 0.01f;
	if(aav_anglePyramid >= 360.0f)
		aav_anglePyramid = 0.0f;
	aav_angleCube = aav_angleCube + 0.01f;
	if(aav_angleCube >= 360.0f)
		aav_angleCube = 0.0f;
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code 
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (aav_gVao_triangle)
	{
		glDeleteVertexArrays(1, &aav_gVao_triangle);
		aav_gVao_triangle = 0;
	}

	if (aav_gVbo_position_triangle)
	{
		glDeleteBuffers(1, &aav_gVbo_position_triangle);
		aav_gVbo_position_triangle = 0;
	}

	if (aav_gVbo_color_triangle)
	{
		glDeleteBuffers(1, &aav_gVbo_color_triangle);
		aav_gVbo_color_triangle = 0;
	}

	if (aav_gVao_square)
	{
		glDeleteVertexArrays(1, &aav_gVao_square);
		aav_gVao_square = 0;
	}

	if (aav_gVbo_position_square)
	{
		glDeleteBuffers(1, &aav_gVbo_position_square);
		aav_gVbo_position_square = 0;
	}

	if (aav_gVbo_color_square)
	{
		glDeleteBuffers(1, &aav_gVbo_color_square);
		aav_gVbo_color_square = 0;
	}


	if (aav_gVao_circle)
	{
		glDeleteVertexArrays(1, &aav_gVao_circle);
		aav_gVao_circle = 0;
	}

	if (aav_gVbo_circle_position)
	{
		glDeleteBuffers(1, &aav_gVbo_circle_position);
		aav_gVbo_circle_position = 0;
	}

	if (aav_aVbo_circle_color)
	{
		glDeleteBuffers(1, &aav_aVbo_circle_color);
		aav_aVbo_circle_color = 0;
	}

	if (aav_gVao_circle)
	{
		glDeleteVertexArrays(1, &aav_gVao_circle);
		aav_gVao_circle = 0;
	}

	if (aav_gVbo_circleInner_position)
	{
		glDeleteBuffers(1, &aav_gVbo_circleInner_position);
		aav_gVbo_circleInner_position = 0;
	}

	if (aav_aVbo_circleInner_color)
	{
		glDeleteBuffers(1, &aav_aVbo_circleInner_color);
		aav_aVbo_circleInner_color = 0;
	}




	glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glDeleteShader(aav_gVertexShaderObject);
	aav_gVertexShaderObject = 0;
	glDeleteShader(aav_gFragmentShaderObject);
	aav_gFragmentShaderObject = 0;

	glDeleteProgram(aav_gShaderProgramObject);
	aav_gShaderProgramObject = 0;

	glUseProgram(0);


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(aav_ghrc);
	aav_ghrc = NULL;

	ReleaseDC(aav_ghwnd, aav_ghdc);
	aav_ghdc = NULL;

	if (aav_circleVertices)
	{
		free(aav_circleVertices);
	}

	if (aav_circleColor)
	{
		free(aav_circleColor);
	}

	if (aav_circleVerticesInner)
	{
		free(aav_circleVerticesInner);
	}

	if (aav_circleColorInner)
	{
		free(aav_circleColorInner);
	}


	if (aav_gpFile)
	{
	fprintf(aav_gpFile, "Log File is Successfully Closed. \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}
}