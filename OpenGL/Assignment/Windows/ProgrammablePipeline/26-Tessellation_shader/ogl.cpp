#include <windows.h>
#include <stdio.h>
#include "ogl.h"
#include  <gl\glew.h> 
#include <gl\GL.h>
#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL; 
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint glTeselationControlShaderObject;		//Teselation Control Object
GLuint glTeselationEvalutationShaderObject;	//Teselation Evaluation Shader
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint aav_gVao;
GLuint aav_gVbo_position;
GLuint aav_mvp_MatrixUniform;

//Teselation Shader unifroms
GLuint aav_NumberOfSegmentUniform;
GLuint aav_NumberOfStripsUniform;
GLuint aav_lineColorUniform;

mat4 aav_PerspectiveProjectionMatrix;

//Teselation
unsigned int uiNumberOfSegment;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	

	//code
	if (fopen_s(&aav_gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("Aniruddha Avinash Vanjari : OpenGL Programmable Pipeline : Tessellation Shader"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	initialize();
	
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
					aav_bDone = true;
			}
		}
		
	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			aav_gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
			break;
		case VK_UP:
			uiNumberOfSegment++;
			if (uiNumberOfSegment >= 30)
			{
				uiNumberOfSegment = 30;
			}
			break;
		case VK_DOWN:
			uiNumberOfSegment--;
			if (uiNumberOfSegment <= 0)
			{
				uiNumberOfSegment = 1;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO aav_mi;

	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
						SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// function prototype
	void uninitialize(void);
	void resize(int ,int);

	//variable decalrations 
	PIXELFORMATDESCRIPTOR aav_pfd;
	int aav_iPixelFormatIndex;

	//code
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;
	aav_pfd.cDepthBits = 32;

	aav_ghdc = GetDC(aav_ghwnd);

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);
	if (aav_iPixelFormatIndex == 0)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == false)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == false)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	GLenum aav_glew_error = glewInit();
	if (aav_glew_error != GLEW_OK)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION)); //gRAPHICS Library shadinng language

	//OpenGL Enable Extensions 
	GLint  numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExtension);
	for (int i = 0; i < numExtension; i++)
	{
		fprintf(aav_gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	//**VERTEX SHADER***
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec2 vPosition;" \
		"void main(void)" \
		"{" \
		"gl_Position =  vec4(vPosition,0.0,1.0);" \
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gVertexShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}
	/*TeselationControlShader*/
	//////////////////////////////////////////////////////////////////////////////////////////////////////////
	glTeselationControlShaderObject = glCreateShader(GL_TESS_CONTROL_SHADER);
	const GLchar* teselationShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"layout(vertices = 4)out;" \
		"uniform int numberOfSegments;"\
		"uniform int numberOfStrips;"\
		"void main(void)" \
		"{" \
		"	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;"\
		"	gl_TessLevelOuter[0] = float(numberOfStrips); " \
		"	gl_TessLevelOuter[1] = float(numberOfSegments); " \
		"}";
	glShaderSource(glTeselationControlShaderObject, 1, (const GLchar**)&teselationShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(glTeselationControlShaderObject);

	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;
	szBuffer = NULL;

	glGetShaderiv(glTeselationControlShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(glTeselationControlShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(glTeselationControlShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Teselation Control Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}


	/******Teselation Evaluation Shader*******/
	glTeselationEvalutationShaderObject = glCreateShader(GL_TESS_EVALUATION_SHADER);
	const GLchar* teselationEvalutaionShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"layout(isolines)in;" \
		"uniform mat4 u_mvpMatrix;" \
		"void main(void)" \
		"{" \
		"	float tessCoord = gl_TessCoord.x;" \
		"	vec3  p0 = gl_in[0].gl_Position.xyz;"\
		"	vec3  p1 = gl_in[1].gl_Position.xyz;"\
		"	vec3  p2 = gl_in[2].gl_Position.xyz;"\
		"	vec3  p3 = gl_in[3].gl_Position.xyz;"\
		"	vec3 p = p0 * (1.0 - tessCoord) * (1.0 - tessCoord) * (1.0 - tessCoord) + p1 * 3.0 * tessCoord  * (1.0 - tessCoord) * (1.0 - tessCoord)  + p2 * 3.0 * tessCoord * tessCoord *(1.0-tessCoord) + p3 *  tessCoord * tessCoord * tessCoord ;" \
		"	gl_Position = u_mvpMatrix * vec4(p,1.0);"\
		"}";
	glShaderSource(glTeselationEvalutationShaderObject, 1, (const GLchar**)&teselationEvalutaionShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(glTeselationEvalutationShaderObject);

	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;
	szBuffer = NULL;

	glGetShaderiv(glTeselationEvalutationShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(glTeselationEvalutationShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(glTeselationEvalutationShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Teselation Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////////////


	//**FRAGMENT SHADER**
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"uniform vec4 LineColor;" \
		"void main(void)" \
		"{" \
		"FragColor = LineColor;" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(gFragmentShaderObject);


	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gFragmentShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, glTeselationControlShaderObject);		//
	glAttachShader(gShaderProgramObject, glTeselationEvalutationShaderObject);	//
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);
	
	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glLinkProgram(gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//Post Linking Information
	aav_mvp_MatrixUniform =  glGetUniformLocation(gShaderProgramObject, "u_mvpMatrix");

	aav_NumberOfSegmentUniform = glGetUniformLocation(gShaderProgramObject, "numberOfSegments");

	aav_NumberOfStripsUniform = glGetUniformLocation(gShaderProgramObject, "numberOfStrips");

	aav_lineColorUniform = glGetUniformLocation(gShaderProgramObject, "LineColor");

	

	//vertices array declation
	const GLfloat vertices[] =
	{
	-1.0f,	-1.0f,
	-0.5f,	 1.0f,
	 0.5f,	-1.0f,
	 1.0f,	 1.0f
	};

	glGenVertexArrays(1, &aav_gVao);
	glBindVertexArray(aav_gVao);
	//Record
	glGenBuffers(1, &aav_gVbo_position); // Named Buffer object
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position); // Postion , TexCord, Normal
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//Record Off / Pause 
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_CULL_FACE);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();

	uiNumberOfSegment = 1; //

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing
	mat4 modelViewMateix = mat4::identity();
	mat4 modelViewProjectMatrix = mat4::identity();
	mat4 translateMatrix = vmath::translate(0.0f, 0.0f, -4.0f);
	modelViewMateix = translateMatrix;
	modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform,1,GL_FALSE, modelViewProjectMatrix);

	glUniform1i(aav_NumberOfSegmentUniform, uiNumberOfSegment);//

	glUniform1i(aav_NumberOfStripsUniform, 1);//

	glUniform4fv(aav_lineColorUniform, 1, vmath::vec4(1.0f,1.0f,0.0f,1.0f));//

	glBindVertexArray(aav_gVao);
	glPatchParameteri(GL_PATCH_VERTICES, 4);
	glDrawArrays(GL_PATCHES, 0, 4);

	glBindVertexArray(0);

	glUseProgram(0);
	SwapBuffers(aav_ghdc);
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code 
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (aav_gVao)
	{
		glDeleteVertexArrays(1, &aav_gVao);
		aav_gVao = 0;
	}

	if (aav_gVbo_position)
	{
		glDeleteBuffers(1, &aav_gVbo_position);
		aav_gVbo_position = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);

	glDetachShader(gShaderProgramObject, glTeselationControlShaderObject); //
	glDetachShader(gShaderProgramObject, glTeselationEvalutationShaderObject); //

	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;

	glDeleteShader(glTeselationControlShaderObject);//
	glTeselationControlShaderObject = 0;//

	glDeleteShader(glTeselationEvalutationShaderObject);//
	glTeselationEvalutationShaderObject = 0;//

	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(aav_ghrc);
	aav_ghrc = NULL;

	ReleaseDC(aav_ghwnd, aav_ghdc);
	aav_ghdc = NULL;

	if (aav_gpFile)
	{
	fprintf(aav_gpFile, "Log File is Successfully Closed. \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}
}