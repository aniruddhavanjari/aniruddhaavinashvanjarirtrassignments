#include <windows.h>
#include <stdio.h>
#include "threeRotatingLightOnSphere.h"
#include  <gl\glew.h> // This must be include before GL.h
#include <gl\GL.h>
#include "vmath.h"
#include "Sphere-dotH.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL; 
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

GLuint gVertexShaderObject_pv;
GLuint gFragmentShaderObject_pv;
GLuint gShaderProgramObject_pv;

GLuint gVertexShaderObject_pf;
GLuint gFragmentShaderObject_pf;
GLuint gShaderProgramObject_pf;

GLuint aav_mvp_MatrixUniform;

mat4 aav_PerspectiveProjectionMatrix;

//Sphere Variable
GLfloat aav_sphere_vertices[1146];
GLfloat aav_sphere_normals[1146];
GLfloat aav_sphere_texture[764];
unsigned short aav_sphere_elements[2280];
GLuint aav_numSphereVertices;
GLuint aav_numSphereElements;

GLuint aav_gVao_sphere;
GLuint aav_Vbo_sphere_position;
GLuint aav_Vbo_sphere_normals;
GLuint aav_Vbo_sphere_elements;

bool aav_bLight = false;
bool aav_bPerFragment = false;
bool aav_bPerVertex = true;
bool aav_bAnimation = false;

//Light Array
GLfloat aav_lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };	//la
GLfloat aav_lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	// ld
GLfloat aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };	//ls

GLfloat aav_lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

struct Light
{
	GLfloat aav_lightAmbiant[4];
	GLfloat aav_lightDiffuse[4];
	GLfloat aav_lightSpecular[4];
	GLfloat aav_lightPosition[4];
};

struct Light light[3];

struct Material
{
	GLfloat aav_materialAmbiant[4];
	GLfloat aav_materialDiffuse[4];
	GLfloat aav_materialSpecular[4];
	GLfloat aav_materialShininess;
};

struct Material material;

GLuint aav_viewMatrixUniform_pf;
GLuint aav_modelMatrixUniform_pf;
GLuint aav_ProjectionMatrixUniform_pf;

GLuint aav_laUniform_pf[3];
GLuint aav_ldUniform_pf[3];
GLuint aav_lsUniform_pf[3];
GLuint aav_lightPositionUniform_pf[3];

GLuint aav_kaUniform_pf;
GLuint aav_kdUniform_pf;
GLuint aav_ksUniform_pf;

GLuint aav_kShininessUniform_pf;


//VertexShader Uniforms

GLuint aav_viewMatrixUniform_pv;
GLuint aav_modelMatrixUniform_pv;
GLuint aav_ProjectionMatrixUniform_pv;

GLuint aav_laUniform_pv[3];
GLuint aav_ldUniform_pv[3];
GLuint aav_lsUniform_pv[3];
GLuint aav_lightPositionUniform_pv[3];

GLuint aav_kaUniform_pv;
GLuint aav_kdUniform_pv;
GLuint aav_ksUniform_pv;

GLuint aav_kShininessUniform_pv;

GLuint aav_lKeyPressedUniform;

GLfloat aav_lightAngle1 = 0.0f;
GLfloat aav_lightAngle2 = 0.0f;
GLfloat aav_lightAngle3 = 0.0f;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	

	//code
	if (fopen_s(&aav_gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("Aniruddha Avinash Vanjari : OpenGL Programmable Pipeline : ThreeRotatingLightsOnSphere"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	initialize();
	
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
					aav_bDone = true;
			}
		}
		
	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'Q':
		case 'q':
			aav_gbEscapeKeyIsPressed = true;
		break;
		case 'l':
		case 'L':
			if (aav_bLight == false)
			{
				aav_bLight = true;
			}
			else
			{
				aav_bLight = false;
			}
			break;
		case 'f':
		case 'F':
			if (aav_bPerFragment == false)
			{
				aav_bPerVertex = false;
				aav_bPerFragment = true;
			}
			
		break;
		case 'v':
		case 'V':
			if (aav_bPerVertex == false)
			{
				aav_bPerFragment = false;
				aav_bPerVertex = true;
			}
		break;
		case 'a':
		case 'A':
			if (aav_bAnimation == false)
			{
				aav_bAnimation = true;
			}
			else
			{
				aav_bAnimation = false;
			}
		break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO aav_mi;

	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
						SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// function prototype
	void uninitialize(void);
	void resize(int ,int);

	//variable decalrations 
	PIXELFORMATDESCRIPTOR aav_pfd;
	int aav_iPixelFormatIndex;

	//code
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;
	aav_pfd.cDepthBits = 32;

	aav_ghdc = GetDC(aav_ghwnd);

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);
	if (aav_iPixelFormatIndex == 0)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == false)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == false)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	GLenum aav_glew_error = glewInit();
	if (aav_glew_error != GLEW_OK)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL Enable Extensions 
	GLint  aav_numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &aav_numExtension);
	for (int i = 0; i < aav_numExtension; i++)
	{
		fprintf(aav_gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	//PerVertex
	//**VERTEX SHADER***
	gVertexShaderObject_pv = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode_pv =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la_pv[3];" \
		"uniform vec3 u_ld_pv[3];" \
		"uniform vec3 u_ls_pv[3];" \
		"uniform vec4 u_lightPosistion_pv[3];" \
		"uniform vec3 u_ka_pv;" \
		"uniform vec3 u_kd_pv;" \
		"uniform vec3 u_ks_pv;" \
		"uniform float u_kShineness_pv;"\
		"vec3 lightDirection[3];"\
		"vec3 reflection_vector[3];"\
		"vec3 ambiant[3];"\
		"vec3 diffuse[3];"\
		"vec3 specular[3];"\
		"out vec3 fong_ads_light_pv;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{" \
		"		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"		vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"		vec3 view_vector = normalize(-eyeCordinate.xyz);" \
		
		"		for(int i = 0 ; i < 3; i++)" \
		"		{" \
		"			lightDirection[i] = normalize(vec3(u_lightPosistion_pv[i] - eyeCordinate));" \
		"			reflection_vector[i] = reflect(-lightDirection[i],tranformed_normal);" \
		"			ambiant[i] = u_la_pv[i] * u_ka_pv;" \
		"			diffuse[i] = u_ld_pv[i] * u_kd_pv * max(dot(lightDirection[i],tranformed_normal),0.0f);" \
		"			specular[i] = u_ls_pv[i] * u_ks_pv * pow(max(dot(reflection_vector[i],view_vector),0.0f),u_kShineness_pv);" \
		"			fong_ads_light_pv = fong_ads_light_pv + ambiant[i] + diffuse[i] + specular[i];" \
		"		}" \
		"	}" \
		"	else" \
		"	{" \
		"		fong_ads_light_pv = vec3(1.0f,1.0f,1.0f);" \
		"	}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gVertexShaderObject_pv, 1, (const GLchar**)&aav_vertexShaserSourceCode_pv, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(gVertexShaderObject_pv);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_pv, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gVertexShaderObject_pv, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader PerVertex Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//PerVertex
	//**FRAGMENT SHADER**
	gFragmentShaderObject_pv = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode_pv = 
		"#version 450 core" \
		"\n" \
		"in vec3 fong_ads_light_pv;" \
		"out vec4 FragColor_pv;" \
		"void main(void)" \
		"{" \
		"	FragColor_pv = vec4(fong_ads_light_pv,1.0f);" \
		"}";
	glShaderSource(gFragmentShaderObject_pv, 1, 
		(const GLchar**)&fragmentShaderSourceCode_pv,NULL);
	//compile shader
	glCompileShader(gFragmentShaderObject_pv);

	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(gFragmentShaderObject_pv, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gFragmentShaderObject_pv, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader PerVertex Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	/****************************************************************/
	//PerFragment
	//**VERTEX SHADER***
	gVertexShaderObject_pf = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec4 u_lightPosistion_pf[3];" \
		"out vec3 tranformed_normal_pf;"\
		"out vec3 lightDirection_pf[3];" \
		"out vec3 view_vector_pf;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{"\
		"		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"		tranformed_normal_pf = (mat3(u_view_matrix* u_model_matrix) * vNormal);" \
		"		view_vector_pf = (-eyeCordinate.xyz);" \
		"		for(int i = 0 ; i < 3 ; i++)"\
		"		{"\
		"			lightDirection_pf[i] = (vec3(u_lightPosistion_pf[i] - eyeCordinate));" \
		"		}"\
		"	}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gVertexShaderObject_pf, 1, (const GLchar**)&aav_vertexShaserSourceCode_pf, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(gVertexShaderObject_pf);
	
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;
	szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject_pf, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gVertexShaderObject_pf, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader PerFramgment Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	
	//PerFragment
	//**FRAGMENT SHADER**
	gFragmentShaderObject_pf = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fragmentShaderSourceCode_pf =
		"#version 450 core" \
		"\n" \
		"in vec3 tranformed_normal_pf;"\
		"in vec3 lightDirection_pf[3];" \
		"in vec3 view_vector_pf;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la_pf[3];" \
		"uniform vec3 u_ld_pf[3];" \
		"uniform vec3 u_ls_pf[3];" \
		"uniform vec3 u_ka_pf;" \
		"uniform vec3 u_kd_pf;" \
		"uniform vec3 u_ks_pf;" \
		"uniform float u_kShineness_pf;"\

		"vec3 normalize_tranformed_normal;"\
		"vec3 normalize_lightDirection[3];"\
		"vec3 normalize_view_vector;"\
		"vec3 reflection_vector[3];"\
		"vec3 ambiant[3];"\
		"vec3 diffuse[3];"\
		"vec3 specular[3];"\

		"out vec4 FragColor_pf;" \
		"vec3 fong_ads_light_pf;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{"\
		"		normalize_tranformed_normal = normalize(tranformed_normal_pf);" \
		"		normalize_view_vector = normalize(view_vector_pf);" \
		"		for(int i = 0; i < 3 ; i++)"\
		"		{" \
		"			normalize_lightDirection[i] = normalize(lightDirection_pf[i]);" \
		"			reflection_vector[i] = reflect(-normalize_lightDirection[i],normalize_tranformed_normal);" \
		"			ambiant[i] = u_la_pf[i] * u_ka_pf;" \
		"			diffuse[i] = u_ld_pf[i] * u_kd_pf * max(dot(normalize_lightDirection[i],normalize_tranformed_normal),0.0f);" \
		"			specular[i] = u_ls_pf[i] * u_ks_pf * pow(max(dot(reflection_vector[i],normalize_view_vector),0.0f),u_kShineness_pf);" \
		"			fong_ads_light_pf = fong_ads_light_pf + ambiant[i] + diffuse[i] + specular[i];" \
		"		}" \
		"	}" \
		"	else"\
		"	{"\
		"		fong_ads_light_pf = vec3(1.0f,1.0f,1.0f);"\
		"	}"\
		"	FragColor_pf = vec4(fong_ads_light_pf,1.0f);" \
		"}";
	glShaderSource(gFragmentShaderObject_pf, 1, 
		(const GLchar**)&fragmentShaderSourceCode_pf,NULL);
	//compile shader
	glCompileShader(gFragmentShaderObject_pf);

	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(gFragmentShaderObject_pf, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gFragmentShaderObject_pf, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader of PerFragment Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	/****************************************************************/

	//**SHADER PROGRAM For PerVertex**
	//Create 
	gShaderProgramObject_pv = glCreateProgram();

	glAttachShader(gShaderProgramObject_pv, gVertexShaderObject_pv);
	glAttachShader(gShaderProgramObject_pv, gFragmentShaderObject_pv);
		
	glBindAttribLocation(gShaderProgramObject_pv, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject_pv, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject_pv);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	
	glGetProgramiv(gShaderProgramObject_pv, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_pv, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(gShaderProgramObject_pv, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program PerVertex Link Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}


	/*************************Shader Program For PerFrament***********************/
	//Create 
	gShaderProgramObject_pf = glCreateProgram();

	glAttachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);
	glAttachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);

	glBindAttribLocation(gShaderProgramObject_pf, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject_pf, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject_pf);

	aav_infoLogLength = 0;
	aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;

	glGetProgramiv(gShaderProgramObject_pf, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject_pf, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(gShaderProgramObject_pf, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program PerFragment Link Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}
	
	/*****************************************************************************/
	//Post Linking Information
	//PerVertex
	aav_modelMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_model_matrix");
	aav_viewMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_view_matrix");
	aav_ProjectionMatrixUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_projection_matrix");
	
	//Red Light
	aav_laUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_la_pv[0]");
	aav_ldUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_pv[0]");
	aav_lsUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_pv[0]");
	aav_lightPositionUniform_pv[0] = glGetUniformLocation(gShaderProgramObject_pv, "u_lightPosistion_pv[0]");
	
	//Green Light
	aav_laUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_la_pv[1]");
	aav_ldUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_pv[1]");
	aav_lsUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_pv[1]");
	aav_lightPositionUniform_pv[1] = glGetUniformLocation(gShaderProgramObject_pv, "u_lightPosistion_pv[1]");

	//Blue Light
	aav_laUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_la_pv[2]");
	aav_ldUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_ld_pv[2]");
	aav_lsUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_ls_pv[2]");
	aav_lightPositionUniform_pv[2] = glGetUniformLocation(gShaderProgramObject_pv, "u_lightPosistion_pv[2]");

	aav_kaUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_ka_pv");
	aav_kdUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_kd_pv");
	aav_ksUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_ks_pv");

	aav_kShininessUniform_pv = glGetUniformLocation(gShaderProgramObject_pv, "u_kShineness_pv");

	aav_lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject_pv, "u_lKeyPressed");

	/*************************************************************************************/
	//PerFragment
	aav_modelMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_model_matrix");
	aav_viewMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_view_matrix");
	aav_ProjectionMatrixUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_projection_matrix");
	
	//Red Light
	aav_laUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_la_pf[0]");
	aav_ldUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_ld_pf[0]");
	aav_lsUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_ls_pf[0]");
	aav_lightPositionUniform_pf[0] = glGetUniformLocation(gShaderProgramObject_pf, "u_lightPosistion_pf[0]");

	//Green Light
	aav_laUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_la_pf[1]");
	aav_ldUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_ld_pf[1]");
	aav_lsUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_ls_pf[1]");
	aav_lightPositionUniform_pf[1] = glGetUniformLocation(gShaderProgramObject_pf, "u_lightPosistion_pf[1]");

	//Blue light
	aav_laUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_la_pf[2]");
	aav_ldUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_ld_pf[2]");
	aav_lsUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_ls_pf[2]");
	aav_lightPositionUniform_pf[2] = glGetUniformLocation(gShaderProgramObject_pf, "u_lightPosistion_pf[2]");

	aav_kaUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ka_pf");
	aav_kdUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_kd_pf");
	aav_ksUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_ks_pf");

	aav_kShininessUniform_pf = glGetUniformLocation(gShaderProgramObject_pf, "u_kShineness_pf");

	aav_lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject_pf, "u_lKeyPressed");

	/*****************************************************************************************/

	//vertices array declation
	getSphereVertexData(aav_sphere_vertices, aav_sphere_normals, aav_sphere_texture, aav_sphere_elements);
	
	aav_numSphereVertices = getNumberOfSphereVertices();
	aav_numSphereElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &aav_gVao_sphere);
	glBindVertexArray(aav_gVao_sphere);

	//Record Sphere
	glGenBuffers(1, &aav_Vbo_sphere_position); 
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_vertices), aav_sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//normals
	glGenBuffers(1, &aav_Vbo_sphere_normals);
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_normals), aav_sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE,0,NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//elements
	glGenBuffers(1, &aav_Vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements); // Elemtnes Drawing is Also Called As Index Drawing, Elements Drawing
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(aav_sphere_elements), aav_sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//Record Off / Pause 
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();


	//material 
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 1.0f;
	material.aav_materialDiffuse[1] = 1.0f;
	material.aav_materialDiffuse[2] = 1.0f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 1.0f;
	material.aav_materialSpecular[1] = 1.0f;
	material.aav_materialSpecular[2] = 1.0f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 128.0f;

	
	//Light 
	//Red
	light[0].aav_lightAmbiant[0] = 0.0f;
	light[0].aav_lightAmbiant[1] = 0.0f;
	light[0].aav_lightAmbiant[2] = 0.0f;
	light[0].aav_lightAmbiant[3] = 1.0f;

	light[0].aav_lightDiffuse[0] = 1.0f;
	light[0].aav_lightDiffuse[1] = 0.0f;
	light[0].aav_lightDiffuse[2] = 0.0f;
	light[0].aav_lightDiffuse[3] = 1.0f;

	light[0].aav_lightSpecular[0] = 1.0f;
	light[0].aav_lightSpecular[1] = 0.0f;
	light[0].aav_lightSpecular[2] = 0.0f;
	light[0].aav_lightSpecular[3] = 1.0f;

	light[0].aav_lightPosition[0] = 0.0f;
	light[0].aav_lightPosition[1] = 0.0f;
	light[0].aav_lightPosition[2] = 0.0f;
	light[0].aav_lightPosition[3] = 1.0f;

	//Green
	light[1].aav_lightAmbiant[0] = 0.0f;
	light[1].aav_lightAmbiant[1] = 0.0f;
	light[1].aav_lightAmbiant[2] = 0.0f;
	light[1].aav_lightAmbiant[3] = 1.0f;

	light[1].aav_lightDiffuse[0] = 0.0f;
	light[1].aav_lightDiffuse[1] = 1.0f;
	light[1].aav_lightDiffuse[2] = 0.0f;
	light[1].aav_lightDiffuse[3] = 1.0f;

	light[1].aav_lightSpecular[0] = 0.0f;
	light[1].aav_lightSpecular[1] = 1.0f;
	light[1].aav_lightSpecular[2] = 0.0f;
	light[1].aav_lightSpecular[3] = 1.0f;

	light[1].aav_lightPosition[0] = 0.0f;
	light[1].aav_lightPosition[1] = 0.0f;
	light[1].aav_lightPosition[2] = 0.0f;
	light[1].aav_lightPosition[3] = 1.0f;

	//Blue
	light[2].aav_lightAmbiant[0] = 0.0f;
	light[2].aav_lightAmbiant[1] = 0.0f;
	light[2].aav_lightAmbiant[2] = 0.0f;
	light[2].aav_lightAmbiant[3] = 1.0f;

	light[2].aav_lightDiffuse[0] = 0.0f;
	light[2].aav_lightDiffuse[1] = 0.0f;
	light[2].aav_lightDiffuse[2] = 1.0f;
	light[2].aav_lightDiffuse[3] = 1.0f;

	light[2].aav_lightSpecular[0] = 0.0f;
	light[2].aav_lightSpecular[1] = 0.0f;
	light[2].aav_lightSpecular[2] = 1.0f;
	light[2].aav_lightSpecular[3] = 1.0f;

	light[2].aav_lightPosition[0] = 0.0f;
	light[2].aav_lightPosition[1] = 0.0f;
	light[2].aav_lightPosition[2] = 0.0f;
	light[2].aav_lightPosition[3] = 1.0f;

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{

	//fuction declartion
	void update(void);

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	mat4 aav_modelMateix;
	mat4 aav_viewMatrix;
	mat4 aav_translateMatrix;
	 
	light[0].aav_lightPosition[0] = 0.0f;
	light[0].aav_lightPosition[1] = 5 * sin(aav_lightAngle1);
	light[0].aav_lightPosition[2] = 5 * cos(aav_lightAngle1);
	light[0].aav_lightPosition[3] = 1.0f;

	light[1].aav_lightPosition[0] = 5 * sin(aav_lightAngle1);
	light[1].aav_lightPosition[1] = 0.0f;
	light[1].aav_lightPosition[2] = 5 * cos(aav_lightAngle1);
	light[1].aav_lightPosition[3] = 1.0f;

	light[2].aav_lightPosition[0] = 5 * sin(aav_lightAngle1);
	light[2].aav_lightPosition[1] = 5 * cos(aav_lightAngle1);
	light[2].aav_lightPosition[2] = 0.0f;
	light[2].aav_lightPosition[3] = 1.0f;

	if (aav_bPerVertex == true)
	{
		glUseProgram(gShaderProgramObject_pv);

		if (aav_bLight == true)
		{
			glUniform1i(aav_lKeyPressedUniform, 1);
			//Light
			//Red
			glUniform3fv(aav_laUniform_pv[0], 1, (GLfloat*)light[0].aav_lightAmbiant);				
			glUniform3fv(aav_ldUniform_pv[0], 1, (GLfloat*)light[0].aav_lightDiffuse);				
			glUniform3fv(aav_lsUniform_pv[0], 1, (GLfloat*)light[0].aav_lightSpecular);			
			glUniform4fv(aav_lightPositionUniform_pv[0], 1, (GLfloat*)light[0].aav_lightPosition);	
			//Green
			glUniform3fv(aav_laUniform_pv[1], 1, (GLfloat*)light[1].aav_lightAmbiant);
			glUniform3fv(aav_ldUniform_pv[1], 1, (GLfloat*)light[1].aav_lightDiffuse);
			glUniform3fv(aav_lsUniform_pv[1], 1, (GLfloat*)light[1].aav_lightSpecular);
			glUniform4fv(aav_lightPositionUniform_pv[1], 1, (GLfloat*)light[1].aav_lightPosition);
			//Blue
			glUniform3fv(aav_laUniform_pv[2], 1, (GLfloat*)light[2].aav_lightAmbiant);
			glUniform3fv(aav_ldUniform_pv[2], 1, (GLfloat*)light[2].aav_lightDiffuse);
			glUniform3fv(aav_lsUniform_pv[2], 1, (GLfloat*)light[2].aav_lightSpecular);
			glUniform4fv(aav_lightPositionUniform_pv[2], 1, (GLfloat*)light[2].aav_lightPosition);

			//material
			glUniform3fv(aav_kaUniform_pv, 1, (GLfloat*)material.aav_materialAmbiant);	
			glUniform3fv(aav_kdUniform_pv, 1, (GLfloat*)material.aav_materialDiffuse);	
			glUniform3fv(aav_ksUniform_pv, 1, (GLfloat*)material.aav_materialSpecular);	
			glUniform1f(aav_kShininessUniform_pv, material.aav_materialShininess);
		}
		else
		{
			glUniform1i(aav_lKeyPressedUniform, 0);
		}

		//OpenGL Drawing
		aav_modelMateix = mat4::identity();

		aav_viewMatrix = mat4::identity();

		aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

		aav_modelMateix = aav_translateMatrix;

		glUniformMatrix4fv(aav_modelMatrixUniform_pv, 1, GL_FALSE, aav_modelMateix);
		glUniformMatrix4fv(aav_viewMatrixUniform_pv, 1, GL_FALSE, aav_viewMatrix);
		glUniformMatrix4fv(aav_ProjectionMatrixUniform_pv, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

		glBindVertexArray(aav_gVao_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
		glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);

		glUseProgram(0);
	}
	if(aav_bPerFragment == true)
	{
		glUseProgram(gShaderProgramObject_pf);

		if (aav_bLight == true)
		{
			glUniform1i(aav_lKeyPressedUniform, 1);
			//Light
			//Red
			glUniform3fv(aav_laUniform_pf[0], 1, (GLfloat*)light[0].aav_lightAmbiant);
			glUniform3fv(aav_ldUniform_pf[0], 1, (GLfloat*)light[0].aav_lightDiffuse);
			glUniform3fv(aav_lsUniform_pf[0], 1, (GLfloat*)light[0].aav_lightSpecular);
			glUniform4fv(aav_lightPositionUniform_pf[0], 1, (GLfloat*)light[0].aav_lightPosition);
			//Green
			glUniform3fv(aav_laUniform_pf[1], 1, (GLfloat*)light[1].aav_lightAmbiant);
			glUniform3fv(aav_ldUniform_pf[1], 1, (GLfloat*)light[1].aav_lightDiffuse);
			glUniform3fv(aav_lsUniform_pf[1], 1, (GLfloat*)light[1].aav_lightSpecular);
			glUniform4fv(aav_lightPositionUniform_pf[1], 1, (GLfloat*)light[1].aav_lightPosition);
			//Blue
			glUniform3fv(aav_laUniform_pf[2], 1, (GLfloat*)light[2].aav_lightAmbiant);
			glUniform3fv(aav_ldUniform_pf[2], 1, (GLfloat*)light[2].aav_lightDiffuse);
			glUniform3fv(aav_lsUniform_pf[2], 1, (GLfloat*)light[2].aav_lightSpecular);
			glUniform4fv(aav_lightPositionUniform_pf[2], 1, (GLfloat*)light[2].aav_lightPosition);

			//material
			glUniform3fv(aav_kaUniform_pf, 1, (GLfloat*)material.aav_materialAmbiant);
			glUniform3fv(aav_kdUniform_pf, 1, (GLfloat*)material.aav_materialDiffuse);	
			glUniform3fv(aav_ksUniform_pf, 1, (GLfloat*)material.aav_materialSpecular);	
			glUniform1f(aav_kShininessUniform_pf, material.aav_materialShininess);
		}
		else
		{
			glUniform1i(aav_lKeyPressedUniform, 0);
		}

		//OpenGL Drawing
		aav_modelMateix = mat4::identity();

		aav_viewMatrix = mat4::identity();

		aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);

		aav_modelMateix = aav_translateMatrix;

		glUniformMatrix4fv(aav_modelMatrixUniform_pf, 1, GL_FALSE, aav_modelMateix);
		glUniformMatrix4fv(aav_viewMatrixUniform_pf, 1, GL_FALSE, aav_viewMatrix);
		glUniformMatrix4fv(aav_ProjectionMatrixUniform_pf, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

		glBindVertexArray(aav_gVao_sphere);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
		glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
		glBindVertexArray(0);

		glUseProgram(0);
	}

	if (aav_bAnimation == true)
	{
		update();
	}
	SwapBuffers(aav_ghdc);
}

void update(void)
{

	aav_lightAngle1 = aav_lightAngle1 + 0.01f;
	if (aav_lightAngle1 > 360)
	{
		aav_lightAngle1 = 0.0f;
	}

	aav_lightAngle2 = aav_lightAngle2 + 0.01f;
	if (aav_lightAngle2 > 360)
	{
		aav_lightAngle2 = 0.0f;
	}

	aav_lightAngle3 = aav_lightAngle3  + 0.01f;
	if (aav_lightAngle3 > 360)
	{
		aav_lightAngle3 = 0.0f;
	}
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code 
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (aav_gVao_sphere)
	{
		glDeleteVertexArrays(1, &aav_gVao_sphere);
		aav_gVao_sphere = 0;
	}

	if (aav_Vbo_sphere_position)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_position);
		aav_Vbo_sphere_position = 0;
	}

	if (aav_Vbo_sphere_normals)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_normals);
		aav_Vbo_sphere_normals = 0;
	}

	if (aav_Vbo_sphere_elements)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_elements);
		aav_Vbo_sphere_elements = 0;
	}


	glDetachShader(gShaderProgramObject_pv, gVertexShaderObject_pv);
	glDetachShader(gShaderProgramObject_pv, gFragmentShaderObject_pv);

	glDeleteShader(gVertexShaderObject_pv);
	gVertexShaderObject_pv = 0;
	glDeleteShader(gFragmentShaderObject_pv);
	gFragmentShaderObject_pv = 0;

	glDeleteProgram(gShaderProgramObject_pv);
	gShaderProgramObject_pv = 0;


	glDetachShader(gShaderProgramObject_pf, gVertexShaderObject_pf);
	glDetachShader(gShaderProgramObject_pf, gFragmentShaderObject_pf);

	glDeleteShader(gVertexShaderObject_pf);
	gVertexShaderObject_pv = 0;
	glDeleteShader(gFragmentShaderObject_pf);
	gFragmentShaderObject_pf = 0;

	glDeleteProgram(gShaderProgramObject_pf);
	gShaderProgramObject_pf = 0;

	glUseProgram(0);


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(aav_ghrc);
	aav_ghrc = NULL;

	ReleaseDC(aav_ghwnd, aav_ghdc);
	aav_ghdc = NULL;

	if (aav_gpFile)
	{
	fprintf(aav_gpFile, "Log File is Successfully Closed. \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}
}