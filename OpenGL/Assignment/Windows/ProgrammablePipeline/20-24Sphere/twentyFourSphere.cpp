 #include <windows.h>
#include <stdio.h>
#include "twentyFourSphere.h"
#include  <gl\glew.h> 
#include <gl\GL.h>
#include "vmath.h"
#include "Sphere-dotH.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


MONITORINFO aav_mi;

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL; 
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint aav_mvp_MatrixUniform;

mat4 aav_PerspectiveProjectionMatrix;

mat4 aav_orthographicProjectionMatrix;

//Sphere Variable
GLfloat aav_sphere_vertices[1146];
GLfloat aav_sphere_normals[1146];
GLfloat aav_sphere_texture[764];
unsigned short aav_sphere_elements[2280];
GLuint aav_numSphereVertices;
GLuint aav_numSphereElements;

GLuint aav_gVao_sphere;
GLuint aav_Vbo_sphere_position;
GLuint aav_Vbo_sphere_normals;
GLuint aav_Vbo_sphere_elements;

bool aav_bLight = false;
 
struct Light
{
	GLfloat  aav_lightAmbiant[4];
	GLfloat aav_lightDiffuse[4];
	GLfloat aav_lightSpecular[4];
	GLfloat aav_lightPosition[4];

};

struct Light light = { { 0.0f,0.0f,0.0f,1.0f } ,{ 1.0f,1.0f,1.0f,1.0f } , { 1.0f,1.0f,1.0f,1.0f } , { 0.0f,0.0f,0.0f,1.0f } };

struct Material
{
	GLfloat aav_materialAmbiant[4];
	GLfloat aav_materialDiffuse[4];
	GLfloat aav_materialSpecular[4];
	GLfloat aav_materialShininess;
};

struct Material material;

GLuint aav_viewMatrixUniform;
GLuint aav_modelMatrixUniform;
GLuint aav_ProjectionMatrixUniform;

GLuint aav_laUniform;
GLuint aav_ldUniform;
GLuint aav_lsUniform;
GLuint aav_lightPositionUniform;

GLuint aav_kaUniform;
GLuint aav_kdUniform;
GLuint aav_ksUniform;

GLuint aav_kShininessUniform;

//KeyPress
GLuint aav_lKeyPressedUniform;
bool aav_xKeyPress = false;
bool aav_yKeyPress = false;
bool aav_zKeyPress = false;
bool aav_animation = false;
//Light Angles
GLfloat aav_lightangle = 0.0f;

//height And width
GLint aav_gHeight = 0;
GLint aav_gWidth = 0;

GLint aav_divideHeight = 0;
GLint aav_divideWidth = 0;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	

	//code
	if (fopen_s(&aav_gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("Aniruddha Avinash Vanjari : OpenGL Programmable Pipeline : 24Sphere"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	initialize();
	
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
					aav_bDone = true;
			}
		}
		
	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			aav_gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'a':
		case 'A':
			if (aav_animation == false)
			{
				aav_animation = true;
			}
			else
			{
				aav_animation = false;
			}
			break;
		case 'l':
		case 'L':
			if (aav_bLight == false)
			{
				aav_bLight = true;
			}
			else
			{
				aav_bLight = false;
			}
			break;
		case 'x':
		case 'X':
			aav_lightangle = 0.0f;
			aav_yKeyPress = false;
			aav_zKeyPress = false;
			aav_xKeyPress = true;
			break;
		case 'y':
		case 'Y':
			aav_lightangle = 0.0f;
			aav_xKeyPress = false;
			aav_zKeyPress = false;
			aav_yKeyPress = true;
			break;
		case 'z':
		case 'Z':
			aav_lightangle = 0.0f;
			aav_xKeyPress = false;
			aav_yKeyPress = false;
			aav_zKeyPress = true;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	
	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
						SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// function prototype
	void uninitialize(void);
	void resize(int ,int);

	//variable decalrations 
	PIXELFORMATDESCRIPTOR aav_pfd;
	int aav_iPixelFormatIndex;

	//code
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;
	aav_pfd.cDepthBits = 32;

	aav_ghdc = GetDC(aav_ghwnd);

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);
	if (aav_iPixelFormatIndex == 0)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == false)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == false)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	GLenum aav_glew_error = glewInit();
	if (aav_glew_error != GLEW_OK)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL Enable Extensions 
	GLint  aav_numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &aav_numExtension);
	for (int i = 0; i < aav_numExtension; i++)
	{
		fprintf(aav_gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	//**VERTEX SHADER***
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec4 u_lightPosistion;" \
		"out vec3 tranformed_normal;"\
		"out vec3 lightDirection;" \
		"out vec3 view_vector;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{"\
		"		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"		tranformed_normal = (mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"		lightDirection = (vec3(u_lightPosistion - eyeCordinate));" \
		"		view_vector = (-eyeCordinate.xyz);" \
		"	}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gVertexShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**FRAGMENT SHADER**
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 tranformed_normal;"\
		"in vec3 lightDirection;" \
		"in vec3 view_vector;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \

		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_kShineness;"\

		"out vec4 FragColor;" \
		"vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{"\
		"		vec3 normalize_tranformed_normal = normalize(tranformed_normal);" \
		"		vec3 normalize_lightDirection = normalize(lightDirection);" \
		"		vec3 normalize_view_vector = normalize(view_vector);" \
		"		vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" \
		"		vec3 ambiant = u_la * u_ka;" \
		"		vec3 diffuse = u_ld * u_kd * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" \
		"		vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness);" \
		"		fong_ads_light = ambiant + diffuse + specular;" \
		"	}"\
		"	else"\
		"	{"\
		"		fong_ads_light = vec3(1.0f,1.0f,1.0f);"\
		"	}"\
		"	FragColor = vec4(fong_ads_light,1.0f);" \
		"}";
	glShaderSource(gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(gFragmentShaderObject);


	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gFragmentShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//Post Linking Information

	/**************************************/
	aav_modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	aav_viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	aav_ProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	aav_laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	aav_ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	aav_lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	aav_lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosistion");
	
	aav_kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	aav_kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	aav_ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");

	aav_kShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_kShineness");

	aav_lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	/**************************************/

	//vertices array declation
	getSphereVertexData(aav_sphere_vertices, aav_sphere_normals, aav_sphere_texture, aav_sphere_elements);
	
	aav_numSphereVertices = getNumberOfSphereVertices();
	aav_numSphereElements = getNumberOfSphereElements();

	glGenVertexArrays(1, &aav_gVao_sphere);
	glBindVertexArray(aav_gVao_sphere);

	//Record Sphere
	glGenBuffers(1, &aav_Vbo_sphere_position); 
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_vertices), aav_sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	//normals
	glGenBuffers(1, &aav_Vbo_sphere_normals);
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_sphere_normals), aav_sphere_normals, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE,0,NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//elements
	glGenBuffers(1, &aav_Vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements); // Elemtnes Drawing is Also Called As Index Drawing, Elements Drawing
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(aav_sphere_elements), aav_sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();


	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{

	//fuction declartion
	void update();


	mat4 aav_modelMateix;
	mat4 aav_viewMatrix;
	mat4 aav_translateMatrix;
	mat4  aav_scaleMatrix;
	mat4 aav_frustomMatrix;
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	if (aav_xKeyPress == true)
	{
		light.aav_lightPosition[0] = 0.0f;
		light.aav_lightPosition[1] = 100 * sin(aav_lightangle);
		light.aav_lightPosition[2] = 100 * cos(aav_lightangle);
		light.aav_lightPosition[3] = 1.0f;
	}
	else if (aav_yKeyPress == true)
	{
		light.aav_lightPosition[0] = 100 * sin(aav_lightangle);
		light.aav_lightPosition[1] = 0.0f;
		light.aav_lightPosition[2] = 100 * cos(aav_lightangle);
		light.aav_lightPosition[3] = 1.0f;
	}
	else if (aav_zKeyPress == true)
	{
		light.aav_lightPosition[0] = 100 * sin(aav_lightangle);
		light.aav_lightPosition[1] = 100 * cos(aav_lightangle);
		light.aav_lightPosition[2] = 0.0f;
		light.aav_lightPosition[3] = 1.0f;
	}

	//OpenGL Drawing
	//1
	glUseProgram(gShaderProgramObject);

	//1st Sppere in 1st Column , emerald 
	material.aav_materialAmbiant[0] = 0.0215f;
	material.aav_materialAmbiant[1] = 0.1745f;
	material.aav_materialAmbiant[2] = 0.0215f;
	material.aav_materialAmbiant[3] = 1.0f;
	

	material.aav_materialDiffuse[0] = 0.07568f;
	material.aav_materialDiffuse[1] = 0.61424f;
	material.aav_materialDiffuse[2] = 0.07568f;
	material.aav_materialDiffuse[3] = 1.0f;
	

	material.aav_materialSpecular[0] = 0.633f;
	material.aav_materialSpecular[1] = 0.727811f;
	material.aav_materialSpecular[2] = 0.633f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.6f * 128.0f;

	if (aav_bLight == true)
	{ 
		glUniform1i(aav_lKeyPressedUniform,1);
		//Light
		glUniform3fv(aav_laUniform,1,(GLfloat *)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform,1,(GLfloat *)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform,1,(GLfloat *)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform,1,(GLfloat *)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform,1,(GLfloat *)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform,1,(GLfloat *)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform,1, (GLfloat *)material.aav_materialSpecular);			//ks
 		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, aav_divideHeight*5, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();
	
	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix ;
	
	glUniformMatrix4fv(aav_modelMatrixUniform,1,GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
	
	//2
	//2nd Sphere in 1st column, jade 
	material.aav_materialAmbiant[0] = 0.135f;
	material.aav_materialAmbiant[1] = 0.2225f;
	material.aav_materialAmbiant[2] = 0.1575f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.54f;
	material.aav_materialDiffuse[1] = 0.89f;
	material.aav_materialDiffuse[2] = 0.63f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.316228f;
	material.aav_materialSpecular[1] = 0.316228f;
	material.aav_materialSpecular[2] = 0.316228f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.1f *128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, aav_divideHeight * 4, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//3nd Sphere in 1st column, obsidian
	material.aav_materialAmbiant[0] = 0.05375f;
	material.aav_materialAmbiant[1] = 0.05f;
	material.aav_materialAmbiant[2] = 0.06625f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.18275f;
	material.aav_materialDiffuse[1] = 0.17f;
	material.aav_materialDiffuse[2] = 0.22525f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.332741f;
	material.aav_materialSpecular[1] = 0.328634f;
	material.aav_materialSpecular[2] = 0.346435f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.3f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, aav_divideHeight * 2, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//4th Sphere in 1st column, pearl
	material.aav_materialAmbiant[0] = 0.25f;
	material.aav_materialAmbiant[1] = 0.20725f;
	material.aav_materialAmbiant[2] = 0.20725f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 1.0f;
	material.aav_materialDiffuse[1] = 0.829f;
	material.aav_materialDiffuse[2] = 0.829f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.296648f;
	material.aav_materialSpecular[1] = 0.296648f;
	material.aav_materialSpecular[2] = 0.296648f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.088f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, aav_divideHeight * 3, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//5th sphere in 1st column ,ruby 
	material.aav_materialAmbiant[0] = 0.1745f;
	material.aav_materialAmbiant[1] = 0.01175f;
	material.aav_materialAmbiant[2] = 0.01175f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.61424f;
	material.aav_materialDiffuse[1] = 0.04136f;
	material.aav_materialDiffuse[2] = 0.04136f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.727811f;
	material.aav_materialSpecular[1] = 0.626959f;
	material.aav_materialSpecular[2] = 0.626959f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.6f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, aav_divideHeight, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	////6th sphere in 1st column , turquoise
	material.aav_materialAmbiant[0] = 0.1f;
	material.aav_materialAmbiant[1] = 0.18725f;
	material.aav_materialAmbiant[2] = 0.1745f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.396f;
	material.aav_materialDiffuse[1] = 0.74151f;
	material.aav_materialDiffuse[2] = 0.69102f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.297254f;
	material.aav_materialSpecular[1] = 0.30829f;
	material.aav_materialSpecular[2] = 0.306678f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.1f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(0, 0, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//1st sphere on 2nd column, brass
	material.aav_materialAmbiant[0] = 0.329412f;
	material.aav_materialAmbiant[1] = 0.223529f;
	material.aav_materialAmbiant[2] = 0.027451f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.780398f;
	material.aav_materialDiffuse[1] = 0.568627f;
	material.aav_materialDiffuse[2] = 0.113725f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.992157f;
	material.aav_materialSpecular[1] = 0.947776f;
	material.aav_materialSpecular[2] = 0.807843f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.21794872f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth , aav_divideHeight * 5, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//2nd sphere on 2nd column,bronze
	material.aav_materialAmbiant[0] = 0.2425f;
	material.aav_materialAmbiant[1] = 0.1275f;
	material.aav_materialAmbiant[2] = 0.054f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.714f;
	material.aav_materialDiffuse[1] = 0.4284f;
	material.aav_materialDiffuse[2] = 0.18144f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.393548f;
	material.aav_materialSpecular[1] = 0.271906f;
	material.aav_materialSpecular[2] = 0.166721f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.2f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth, aav_divideHeight * 4, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//3rd sphere on 2rd column,chrome
	material.aav_materialAmbiant[0] = 0.25f;
	material.aav_materialAmbiant[1] = 0.25f;
	material.aav_materialAmbiant[2] = 0.25f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.4f;
	material.aav_materialDiffuse[1] = 0.4f;
	material.aav_materialDiffuse[2] = 0.4f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.774597f;
	material.aav_materialSpecular[1] = 0.774597f;
	material.aav_materialSpecular[2] = 0.774597f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.6f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth, aav_divideHeight * 3, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//4th sphere on 2nd column, 
	material.aav_materialAmbiant[0] = 0.19125f;
	material.aav_materialAmbiant[1] = 0.0735f;
	material.aav_materialAmbiant[2] = 0.0225f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.7038f;
	material.aav_materialDiffuse[1] = 0.27048f;
	material.aav_materialDiffuse[2] = 0.0828f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.256777f;
	material.aav_materialSpecular[1] = 0.137622f;
	material.aav_materialSpecular[2] = 0.086014f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.1f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth, aav_divideHeight * 2, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//5th Sphere on 2nd Column, gold
	material.aav_materialAmbiant[0] = 0.24725f;
	material.aav_materialAmbiant[1] = 0.1995f;
	material.aav_materialAmbiant[2] = 0.0745f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.75164f;
	material.aav_materialDiffuse[1] = 0.60648f;
	material.aav_materialDiffuse[2] = 0.22648f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.628281f;
	material.aav_materialSpecular[1] = 0.555802f;
	material.aav_materialSpecular[2] = 0.366065f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.4f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth, aav_divideHeight , (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//6th Sphere on 2nd Column 
	material.aav_materialAmbiant[0] = 0.19225f;
	material.aav_materialAmbiant[1] = 0.19225f;
	material.aav_materialAmbiant[2] = 0.19225f;
	material.aav_materialAmbiant[3] = 1.0f;


	material.aav_materialDiffuse[0] = 0.50754f;
	material.aav_materialDiffuse[1] = 0.50754f;
	material.aav_materialDiffuse[2] = 0.50754f;
	material.aav_materialDiffuse[3] = 1.0f;


	material.aav_materialSpecular[0] = 0.508273f;
	material.aav_materialSpecular[1] = 0.508273f;
	material.aav_materialSpecular[2] = 0.508273f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.4f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth, 0 , (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//1st sphere on 3nd column
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.01f;
	material.aav_materialDiffuse[1] = 0.01f;
	material.aav_materialDiffuse[2] = 0.01f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.50f;
	material.aav_materialSpecular[1] = 0.50f;
	material.aav_materialSpecular[2] = 0.50f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, aav_divideHeight * 5, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//2nd Sphere on 3nd column,cyan
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.1f;
	material.aav_materialAmbiant[2] = 0.06f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.0f;
	material.aav_materialDiffuse[1] = 0.50980392f;
	material.aav_materialDiffuse[2] = 0.50980392f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.50196078f;
	material.aav_materialSpecular[1] = 0.50196078f;
	material.aav_materialSpecular[2] = 0.50196078f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);			//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, aav_divideHeight * 4, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//3rd Sphere on 3th column 
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.1f;
	material.aav_materialDiffuse[1] = 0.35f;
	material.aav_materialDiffuse[2] = 0.1f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.45f;
	material.aav_materialSpecular[1] = 0.55f;
	material.aav_materialSpecular[2] = 0.45f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, aav_divideHeight * 3, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//4th Sphere on 3rd ,red
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.5f;
	material.aav_materialDiffuse[1] = 0.0f;
	material.aav_materialDiffuse[2] = 0.0f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.7f;
	material.aav_materialSpecular[1] = 0.6f;
	material.aav_materialSpecular[2] = 0.6f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, aav_divideHeight * 2, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//5th Sphere on 3rd , white
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.55f;
	material.aav_materialDiffuse[1] = 0.55f;
	material.aav_materialDiffuse[2] = 0.55f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.70f;
	material.aav_materialSpecular[1] = 0.70f;
	material.aav_materialSpecular[2] = 0.70f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, aav_divideHeight , (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//6th Sphere on 3rd ,yello plastic
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.5f;
	material.aav_materialDiffuse[1] = 0.5f;
	material.aav_materialDiffuse[2] = 0.0f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.60f;
	material.aav_materialSpecular[1] = 0.60f;
	material.aav_materialSpecular[2] = 0.50f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.25f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 2, 0 , (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//1st sphere on 4th column,black
	material.aav_materialAmbiant[0] = 0.02f;
	material.aav_materialAmbiant[1] = 0.02f;
	material.aav_materialAmbiant[2] = 0.02f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.01f;
	material.aav_materialDiffuse[1] = 0.01f;
	material.aav_materialDiffuse[2] = 0.01f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.4f;
	material.aav_materialSpecular[1] = 0.4f;
	material.aav_materialSpecular[2] = 0.4f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.078125f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth *3 , aav_divideHeight * 5, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//2nd Shpere on 4th column,cyan
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.05f;
	material.aav_materialAmbiant[2] = 0.05f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.4f;
	material.aav_materialDiffuse[1] = 0.5f;
	material.aav_materialDiffuse[2] = 0.5f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.04f;
	material.aav_materialSpecular[1] = 0.7f;
	material.aav_materialSpecular[2] = 0.7f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.078125f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 3, aav_divideHeight * 4, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//3rd Sphere on 4th column , green
	material.aav_materialAmbiant[0] = 0.0f;
	material.aav_materialAmbiant[1] = 0.05f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.4f;
	material.aav_materialDiffuse[1] = 0.5f;
	material.aav_materialDiffuse[2] = 0.04f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.04f;
	material.aav_materialSpecular[1] = 0.7f;
	material.aav_materialSpecular[2] = 0.04f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.6f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 3, aav_divideHeight * 3, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//4th Sphere on 4th column, red
	material.aav_materialAmbiant[0] = 0.05f;
	material.aav_materialAmbiant[1] = 0.0f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.5f;
	material.aav_materialDiffuse[1] = 0.4f;
	material.aav_materialDiffuse[2] = 0.4f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.7f;
	material.aav_materialSpecular[1] = 0.04f;
	material.aav_materialSpecular[2] = 0.04f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.078125f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 3, aav_divideHeight * 2, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	//5th Sphere on 4th column, white
	material.aav_materialAmbiant[0] = 0.05f;
	material.aav_materialAmbiant[1] = 0.05f;
	material.aav_materialAmbiant[2] = 0.05f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.5f;
	material.aav_materialDiffuse[1] = 0.5f;
	material.aav_materialDiffuse[2] = 0.5f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.7f;
	material.aav_materialSpecular[1] = 0.7f;
	material.aav_materialSpecular[2] = 0.7f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.6f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 3, aav_divideHeight , (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);


	//6th Sphere on 4th column , rubber
	material.aav_materialAmbiant[0] = 0.05f;
	material.aav_materialAmbiant[1] = 0.05f;
	material.aav_materialAmbiant[2] = 0.0f;
	material.aav_materialAmbiant[3] = 1.0f;

	material.aav_materialDiffuse[0] = 0.5f;
	material.aav_materialDiffuse[1] = 0.5f;
	material.aav_materialDiffuse[2] = 0.4f;
	material.aav_materialDiffuse[3] = 1.0f;

	material.aav_materialSpecular[0] = 0.7f;
	material.aav_materialSpecular[1] = 0.7f;
	material.aav_materialSpecular[2] = 0.04f;
	material.aav_materialSpecular[3] = 1.0f;

	material.aav_materialShininess = 0.078125f * 128.0f;
	if (aav_bLight == true)
	{
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light
		glUniform3fv(aav_laUniform, 1, (GLfloat*)light.aav_lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform, 1, (GLfloat*)light.aav_lightDiffuse);				// ld
		glUniform3fv(aav_lsUniform, 1, (GLfloat*)light.aav_lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform, 1, (GLfloat*)light.aav_lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, (GLfloat*)material.aav_materialAmbiant);			// ka
		glUniform3fv(aav_kdUniform, 1, (GLfloat*)material.aav_materialDiffuse);			// kd
		glUniform3fv(aav_ksUniform, 1, (GLfloat*)material.aav_materialSpecular);		//ks
		glUniform1f(aav_kShininessUniform, material.aav_materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}
	glViewport(aav_divideWidth * 3, 0, (GLsizei)aav_gWidth / (GLsizei)4, (GLsizei)aav_gHeight / (GLsizei)6);
	aav_modelMateix = mat4::identity();
	aav_viewMatrix = mat4::identity();
	aav_translateMatrix = mat4::identity();
	aav_scaleMatrix = mat4::identity();

	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	aav_modelMateix = aav_translateMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, aav_numSphereElements, GL_UNSIGNED_SHORT, 0);

	glBindVertexArray(0);

	glUseProgram(0);

	if (aav_animation == true)
	{
		update();
	}

	SwapBuffers(aav_ghdc);
}

void update(void)
{
	aav_lightangle = aav_lightangle + 0.05f;
	if (aav_lightangle > 360)
	{
		aav_lightangle = 0.0f;
	}
}

void resize(int width, int height)
{
	//code 
	aav_gHeight = height;
	aav_gWidth = width;

	aav_divideHeight = height / 6;
	aav_divideWidth = width / 4;

	fprintf(aav_gpFile, "height = %d, width = %d\n", aav_gHeight, aav_gWidth);
	fprintf(aav_gpFile, "aav_divideHeight = %d, aav_divideWidth = %d\n", aav_divideHeight, aav_divideWidth);
	if (height == 0)
		height = 1;

	aav_gHeight = height;
	aav_gWidth = width;

	aav_divideHeight = height / 6;
	aav_divideWidth = width / 4;


	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

}

void uninitialize(void)
{
	//code 
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (aav_gVao_sphere)
	{
		glDeleteVertexArrays(1, &aav_gVao_sphere);
		aav_gVao_sphere = 0;
	}

	if (aav_Vbo_sphere_position)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_position);
		aav_Vbo_sphere_position = 0;
	}

	if (aav_Vbo_sphere_normals)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_normals);
		aav_Vbo_sphere_normals = 0;
	}

	if (aav_Vbo_sphere_elements)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_elements);
		aav_Vbo_sphere_elements = 0;
	}



	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(aav_ghrc);
	aav_ghrc = NULL;

	ReleaseDC(aav_ghwnd, aav_ghdc);
	aav_ghdc = NULL;

	if (aav_gpFile)
	{
	fprintf(aav_gpFile, "Log File is Successfully Closed. \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}
}