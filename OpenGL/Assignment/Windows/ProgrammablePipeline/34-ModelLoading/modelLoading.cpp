 #include <windows.h>
#include <stdio.h>
#include "modelLoading.h"
#include  <gl\glew.h> 
#include <gl\GL.h>
#include "vmath.h"


#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL; 
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;


GLuint aav_mvp_MatrixUniform;

mat4 aav_PerspectiveProjectionMatrix;

//Sphere Variable
GLfloat aav_sphere_vertices[1146];
GLfloat aav_sphere_normals[1146];
GLfloat aav_sphere_texture[764];
unsigned short aav_sphere_elements[2280];
GLuint aav_numSphereVertices;
GLuint aav_numSphereElements;

GLuint aav_gVao_sphere;
GLuint aav_Vbo_sphere_position;
GLuint aav_Vbo_sphere_normals;
GLuint aav_Vbo_sphere_texture;
GLuint aav_Vbo_sphere_elements;

bool aav_bLight = false;
 
//Light Array
GLfloat aav_lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };	//la
GLfloat aav_lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	// ld
GLfloat aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };//ls

GLfloat aav_lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

//material Array 
GLfloat aav_materialAmbiant[]	= { 0.0f, 0.0f, 0.0f ,0.0f}; // ka 
GLfloat aav_materialDiffuse[]	= {1.0f,1.0f,1.0f,1.0f};	// kd
GLfloat aav_materialSpecular[]	= {1.0f,1.0f,1.0f,1.0f};	// ks


GLuint aav_viewMatrixUniform;
GLuint aav_modelMatrixUniform;
GLuint aav_ProjectionMatrixUniform;

GLuint aav_laUniform;
GLuint aav_ldUniform;
GLuint aav_lsUniform;
GLuint aav_lightPositionUniform;

GLuint aav_kaUniform;
GLuint aav_kdUniform;
GLuint aav_ksUniform;

GLuint aav_kShininessUniform;

GLuint aav_lKeyPressedUniform;

GLuint aav_textureSamplerUniform;

BOOL aav_AnimationFlag = FALSE;
//////////////////////////////////////////////////////////////////////////////////
/**********************************************************************************************************/

#define TRUE 	1			//True is 1
#define FALSE 	0			//False is 0

#define BUFFER_SIZE 256 	//Maximu length of string in mesh file
#define S_EQUAL 	0		//return value of strcmp when strings are equal 


#define NR_POINT_COORDS 		3	// Number of point coordinates
#define NR_TEXTURE_COORDS		2	// Number of Texture Coordinates 
#define NR_NORMAL_COORDS		3	// Number of normal coordinates 
#define NR_FACE_TOKENS			3 	// Number of enteries in face data 
#define NR_TRIANGLE_VERTICES 	3 	// Number of vertices in a  triangle 

#define FOY_ANGLE	45		// Field of view in Y direction 
#define ZNEAR 		0.1		// Distance from viewer to near plane of viewing volume 
#define ZFAR 		200.0	// Disntance from viewer to far plane viewing volume

#define VIEWPORT_BOTTOMLEFT_X	0	// X-coordinate of bottom-left point of viewport rectangle
#define VIEWPORT_BOTTOMLEFT_Y	0	// Y-coodinate of bottom-left point of viewport rectangle

#define MONKEYHEAD_X_TRANSLATE 	0.0f	// X-translation of monkeyhead
#define MONKEYHEAD_Y_TRANSLATE 	-0.0f	// Y-translation of monkeyhead
#define MONKEYHEAD_Z_TRANSLATE 	-5.0f 	// Z-translation of monkeyhead

#define MONKEYHEAD_X_SCALE_FACTOR	1.5f	// X-scale factor of monkeyhead
#define MONKEYHEAD_Y_SCALE_FACTOR	1.5f 	// Y-scale factor of monkeyhead 
#define MONKEYHEAD_Z_SCALE_FACTOR 	1.5f	// Z-scale factor of monkeyhead 

#define	START_ANGLE_POS				0.0f	// X-scale factor of monkeyhead rotation
#define END_ANGLE_POS				360.0f	// Y-sacle factor of monkeyhead rotation
#define MONKEYHEAD_ANGLE_INCREMENT	1.0f	// Increment angle for monkeyhead


// structure deinietions for vector of vector integers and vector of vector of flaoting 
//point number 

////////////////////////////////////////////////
typedef struct vec_float
{
	GLfloat *p_arr;
	size_t size;
}vec_float_t;

typedef struct vec_int
{
	GLint* p_arr;
	size_t size;
}vec_int_t;


vec_float_t* gp_vertices_new;
vec_float_t* gp_texture_new;
vec_float_t* gp_normals_new;


vec_int_t* gp_face_tri_new;
vec_int_t* gp_face_texture_new;
vec_int_t* gp_face_normals_new;


///////////////////////////////////////////////////


FILE* g_fp_meshfile = NULL;
FILE* g_fp_logfile = NULL;

char g_line[BUFFER_SIZE];

/********************************************************************************************************/

void LoadMeshData(void);

int aav_count = 0;

GLfloat aav_rotation = 0.0f;

GLuint aav_texture;

vec_float_t* aav_gp_vertices_sorted;
vec_float_t* aav_gp_texture_sorted;
vec_float_t* aav_gp_normals_sorted;

GLint* aav_gpvec_vertex_indices;
GLint* aav_gpvec_texture_indices;
GLint* aav_gpvec_normal_indices;
//////////////////////////////////////////////////////////////////////////////////

float* vertexArray = NULL;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	

	//code
	if (fopen_s(&aav_gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
		fclose(aav_gpFile);
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("Aniruddha Avinash Vanjari : OpenGL Programmable Pipeline : ModelLoading"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	initialize();
	
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
					aav_bDone = true;
			}
		}
		
	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			aav_gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'l':
		case 'L':
			if (aav_bLight == false)
			{
				aav_bLight = true;
			}
			else
			{
				aav_bLight = false;
			}
			break;
		case 'A':
		case 'a':
			if (aav_AnimationFlag == FALSE)
			{
				aav_AnimationFlag = TRUE;
			}
			else
			{
				aav_AnimationFlag = FALSE;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO aav_mi;

	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
						SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// function prototype
	void uninitialize(void);
	void resize(int ,int);
	bool loadGLTexture(GLuint*, TCHAR[]);

	//variable decalrations 
	PIXELFORMATDESCRIPTOR aav_pfd;
	int aav_iPixelFormatIndex;

	//code


	fopen_s(&aav_gpFile, "Log.txt", "a+");
	fprintf(aav_gpFile, "initialize() Enter\n");
	fclose(aav_gpFile);





	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;
	aav_pfd.cDepthBits = 32;

	aav_ghdc = GetDC(aav_ghwnd);

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);
	if (aav_iPixelFormatIndex == 0)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == false)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == false)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	GLenum aav_glew_error = glewInit();
	if (aav_glew_error != GLEW_OK)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	//OpenGL Related Log 
	fopen_s(&aav_gpFile, "Log.txt", "a+");
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
	fclose(aav_gpFile);
	//OpenGL Enable Extensions 
	GLint  aav_numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &aav_numExtension);
	for (int i = 0; i < aav_numExtension; i++)
	{
		fopen_s(&aav_gpFile, "Log.txt", "a+");
		fprintf(aav_gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
		fclose(aav_gpFile);
	}

	//**VERTEX SHADER***
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \

		"void main(void)" \
		"{" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gVertexShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fopen_s(&aav_gpFile, "Log.txt", "a+");
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
				fclose(aav_gpFile);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**FRAGMENT SHADER**
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	FragColor = vec4(1.0f,1.0f,1.0f,1.0f); " \
		"}";
	glShaderSource(gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(gFragmentShaderObject);


	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(gFragmentShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fopen_s(&aav_gpFile, "Log.txt", "a+");
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
				fclose(aav_gpFile);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	gShaderProgramObject = glCreateProgram();

	glAttachShader(gShaderProgramObject, gVertexShaderObject);
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glBindAttribLocation(gShaderProgramObject, AAV_ATTRIBUTE_TEXCORD, "vTexCoord");

	glLinkProgram(gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fopen_s(&aav_gpFile, "Log.txt", "a+");
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", szBuffer);
				fclose(aav_gpFile);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//Post Linking Information
	/**************************************/
	aav_textureSamplerUniform = glGetUniformLocation(gShaderProgramObject, "u_texture_sampler");

	aav_modelMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	aav_viewMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	aav_ProjectionMatrixUniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	aav_laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	aav_ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	aav_lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	aav_lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_lightPosistion");
	
	aav_kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	aav_kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	aav_ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");

	aav_kShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_kShineness");

	aav_lKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_lKeyPressed");
	/**************************************/

	fopen_s(&aav_gpFile, "Log.txt", "a+");
	fprintf(aav_gpFile, "Before LoadMesshData Log\n" );
	fclose(aav_gpFile);
	LoadMeshData();
	fopen_s(&aav_gpFile, "Log.txt", "a+");
	fprintf(aav_gpFile, "After LoadMesshData Log\n");
	fclose(aav_gpFile);

	////////////////////////////////////////////////////////////////////////////////////////////////////////
	glGenVertexArrays(1, &aav_gVao_sphere);
	glBindVertexArray(aav_gVao_sphere);

	//Record Sphere
	glGenBuffers(1, &aav_Vbo_sphere_position); 
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * aav_gp_vertices_sorted->size, aav_gp_vertices_sorted->p_arr, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	

	glGenBuffers(1, &aav_Vbo_sphere_elements);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * gp_face_tri_new->size, gp_face_tri_new->p_arr, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);


	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

bool loadGLTexture(GLuint* aav_texture, TCHAR aav_resourceID[])
{
	//Variable Declaration
	bool aav_bResult = false;
	HBITMAP aav_hBitmap = NULL; // OS Image Loading
	BITMAP aav_bmp;

	//Code
	aav_hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL),
		aav_resourceID,
		IMAGE_BITMAP,
		0,
		0,
		LR_CREATEDIBSECTION);

	if (aav_hBitmap)
	{
		aav_bResult = true;
		GetObject(aav_hBitmap, sizeof(BITMAP), &aav_bmp);

		//From Here Starts OpenGL Texture Code.
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glGenTextures(1, aav_texture); // Gatu
		glBindTexture(GL_TEXTURE_2D, *aav_texture);

		//Setting Texture Paramter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//Following will Push Data into Memory
		glTexImage2D(GL_TEXTURE_2D, 0,
			GL_RGB, aav_bmp.bmWidth, aav_bmp.bmHeight,
			0, GL_BGR_EXT, GL_UNSIGNED_BYTE, aav_bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(aav_hBitmap);
	}
	return aav_bResult;
}


void display(void)
{

	//fuction declartion
	void update();

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 
	//triangle
	glUseProgram(gShaderProgramObject);

	//OpenGL Drawing
	mat4 aav_modelMateix = mat4::identity();
	mat4 aav_viewMatrix = mat4::identity();
	mat4 aav_rotationMatrix = mat4::identity();
	mat4 aav_scaleMatrix = mat4::identity();

	mat4 aav_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	aav_rotationMatrix = vmath::rotate(aav_rotation,0.0f,1.0f,0.0f);
	aav_modelMateix = aav_translateMatrix *aav_scaleMatrix * aav_rotationMatrix;

	glUniformMatrix4fv(aav_modelMatrixUniform,1,GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glBindVertexArray(aav_gVao_sphere);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, aav_Vbo_sphere_elements);
	glDrawElements(GL_TRIANGLES, (gp_face_tri_new->size), GL_UNSIGNED_INT, NULL);
	glBindVertexArray(0);
	glUseProgram(0);

	if (aav_AnimationFlag == TRUE)
	{
		update();
	}
	SwapBuffers(aav_ghdc);
}

void update()
{
	aav_rotation = aav_rotation + 0.01f;
	if (aav_rotation > 360)
	{
		aav_rotation = 0.0f;
	}
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code 
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (aav_gVao_sphere)
	{
		glDeleteVertexArrays(1, &aav_gVao_sphere);
		aav_gVao_sphere = 0;
	}

	if (aav_Vbo_sphere_position)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_position);
		aav_Vbo_sphere_position = 0;
	}

	if (aav_Vbo_sphere_normals)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_normals);
		aav_Vbo_sphere_normals = 0;
	}

	if (aav_Vbo_sphere_elements)
	{
		glDeleteBuffers(1, &aav_Vbo_sphere_elements);
		aav_Vbo_sphere_elements = 0;
	}

	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	glUseProgram(0);


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(aav_ghrc);
	aav_ghrc = NULL;

	ReleaseDC(aav_ghwnd, aav_ghdc);
	aav_ghdc = NULL;

	if (aav_gpFile)
	{
		fopen_s(&aav_gpFile, "Log.txt", "a+");
		fprintf(aav_gpFile, "Log File is Successfully Closed. \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

}

void LoadMeshData(void)
{
	void uninitialize(void);

	void push_back_float(vec_float_t * p_vec, GLfloat p_arr);
	

	void push_back_int(vec_int_t* p_vec, int p_arr);
	

	vec_int_t* create_vec_int(void);
	

	vec_float_t* create_vec_float(void);
	

	void clean_vec_int(vec_int_t** pp_vec);
	

	void clean_vec_float(vec_float_t** pp_vec);

	
	void* xcalloc(int, size_t);

	g_fp_meshfile = fopen("MonkeyHead.OBJ", "r");
	if (!g_fp_meshfile)
	{
		uninitialize();
		exit(0);
	}

	fopen_s(&aav_gpFile, "Log.txt", "a+");
	fprintf(aav_gpFile, " Before First gp_vertrices_new create_vec_float\n");
	fclose(aav_gpFile);

	gp_vertices_new = create_vec_float();

	fopen_s(&aav_gpFile, "Log.txt", "a+");
	fprintf(aav_gpFile, " After First gp_vertrices_new create_vec_float\n");
	fclose(aav_gpFile);

	gp_texture_new = create_vec_float();
	gp_normals_new = create_vec_float();

	gp_face_tri_new = create_vec_int();
	gp_face_texture_new = create_vec_int();
	gp_face_normals_new = create_vec_int();

	char* sep_space = " ";
	char* sep_fslash = "/";

	char* first_token = NULL;
	char* token = NULL;

	char* face_tokens[NR_FACE_TOKENS];
	int nr_tokens;

	char* token_vertex_index = NULL;
	char* token_texture_index = NULL;
	char* token_normal_index = NULL;

	while (fgets(g_line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
		first_token = strtok(g_line, sep_space);

		if (strcmp(first_token, "v") == S_EQUAL)
		{ 
			for (int i = 0; i != 3; i++)
			{
				push_back_float(gp_vertices_new,atof(strtok(NULL, sep_space)));
			}
		}
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			for (int i = 0; i != 2; i++)
			{
				push_back_float(gp_texture_new,atof(strtok(NULL, sep_space)));
			}
		}
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			for (int i = 0; i != 3; i++)
			{
				push_back_float(gp_normals_new,atof(strtok(NULL, sep_space)));
			}
		}
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			memset((void*)face_tokens, 0, 3);

			nr_tokens = 0;

			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}
			for (int i = 0; i != 3; i++)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash);

				push_back_int(gp_face_tri_new, atoi(token_vertex_index)-1);

				token_texture_index = strtok(NULL, sep_fslash);
				push_back_int(gp_face_texture_new, atoi(token_texture_index)-1);

				token_normal_index = strtok(NULL, sep_fslash);
				push_back_int(gp_face_normals_new, atoi(token_normal_index)-1);
			}
		}
		memset((void*)g_line, (int)'\0', BUFFER_SIZE);
	}

	fopen_s(&aav_gpFile, "Log.txt", "a+");
	fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
	fclose(aav_gpFile);

	fopen_s(&aav_gpFile, "Log.txt", "a+");
	fprintf(aav_gpFile, "%d\n", gp_face_tri_new->size);
	fclose(aav_gpFile);

	aav_gp_vertices_sorted = create_vec_float();
	for (int i = 0; i < gp_face_tri_new->size; i++)
	{
		push_back_float(aav_gp_vertices_sorted, gp_vertices_new->p_arr[i]);
	}
	
	aav_gp_texture_sorted = create_vec_float();
	for (int i = 0; i < gp_face_texture_new->size; i++)
	{
		push_back_float(aav_gp_texture_sorted, gp_texture_new->p_arr[i]);
	}
	
	aav_gp_normals_sorted = create_vec_float();
	for (int i = 0; i < gp_face_normals_new->size; i++)
	{
		push_back_float(aav_gp_normals_sorted, gp_normals_new->p_arr[i]);
	}

	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;
	
}


void push_back_float(vec_float_t* p_vec, GLfloat arr)
{
	void* xrealloc(void* p, size_t new_size);
	
	p_vec->p_arr = (GLfloat*)xrealloc(p_vec->p_arr, (p_vec->size + 1) * sizeof(float));
	p_vec->size++;
	p_vec->p_arr[p_vec->size - 1] = arr;

}

void push_back_int(vec_int_t* p_vec, int p_arr)
{
	void* xrealloc(void* p, size_t new_size);

	p_vec->p_arr = (GLint*)xrealloc(p_vec->p_arr, (p_vec->size + 1) * sizeof(int));
	p_vec->size++;
	p_vec->p_arr[p_vec->size - 1] = p_arr;
}

vec_int_t* create_vec_int(void)
{
	void* xcalloc(int nr_elements, size_t size_per_element);

	return(vec_int_t*)xcalloc(1, sizeof(vec_int_t));
}

vec_float_t* create_vec_float(void)
{
	fopen_s(&aav_gpFile, "Log.txt", "a+");
	fprintf(aav_gpFile, "Create_vec_float 123 \n");
	fclose(aav_gpFile);
	void* xcalloc(int nr_elements, size_t size_per_element);
	
	return(vec_float_t*)xcalloc(1, sizeof(vec_float_t));
}

void clean_vec_int(vec_int_t** pp_vec)
{
	vec_int_t* p_vec = *pp_vec;
	free(p_vec->p_arr);
	
	free(p_vec);
	*pp_vec = NULL;
}

void clean_vec_float(vec_float_t** pp_vec)
{
	vec_float_t* p_vec = *pp_vec;
	
	free(p_vec->p_arr);
	
	free(p_vec);
	*pp_vec = NULL;
}

void* xcalloc(int nr_elements, size_t size_per_element)
{
	void uninitialize(void);

	void* p = calloc(nr_elements, size_per_element);

	if (!p)
	{
		fopen_s(&aav_gpFile, "Log.txt", "a+");
		fprintf(g_fp_logfile, "calloc:fatal:out of memeory\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	return(p);
}

void* xrealloc(void* p, size_t new_size)
{
	void uninitialize(void);

	void* ptr = realloc(p, new_size);
	if (!ptr)
	{
		fopen_s(&aav_gpFile, "Log.txt", "a+");
		fprintf(g_fp_logfile, "realloc:fatal:out of memory\n");
		fclose(aav_gpFile);
		
		uninitialize();
	}
	return(ptr);
}