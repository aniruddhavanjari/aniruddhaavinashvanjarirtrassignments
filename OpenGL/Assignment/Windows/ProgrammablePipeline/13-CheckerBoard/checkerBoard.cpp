#include <windows.h>
#include <stdio.h>
#include "checkerBoard.h"
#include  <gl\glew.h> // This must be include before GL.h
#include <gl\GL.h>
#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define CHECKER_IMAGE_WIDTH 64
#define CHECKER_IMAGE_HEIGHT 64

using namespace vmath;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};

GLuint aav_keySmilePress;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL; 
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

//shader object
GLuint aav_gVertexShaderObject;
GLuint aav_gFragmentShaderObject;
GLuint aav_gShaderProgramObject;

GLuint aav_gVao_cube;
GLuint aav_gVbo_position_cube;
GLuint aav_gVbo_texture_cube;

GLuint aav_mvp_MatrixUniform;
GLuint aav_PostionUniform;
GLuint aav_textureSamplerUniform;


//Texture


mat4 aav_PerspectiveProjectionMatrix;

GLubyte aav_checkImage[CHECKER_IMAGE_WIDTH][CHECKER_IMAGE_HEIGHT][4];

GLuint aav_uiTextImage;


//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	

	//code
	if (fopen_s(&aav_gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("OpenGL Programmable Pipeline : CheckerBoard"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	initialize();
	
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
					aav_bDone = true;
			}
		}
		
	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			aav_gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO aav_mi;

	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
						SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// function prototype
	void uninitialize(void);
	void resize(int ,int);
	void loadGLTexture();

	//variable decalrations 
	PIXELFORMATDESCRIPTOR aav_pfd;
	int aav_iPixelFormatIndex;

	//code
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;
	aav_pfd.cDepthBits = 32;

	aav_ghdc = GetDC(aav_ghwnd);

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);
	if (aav_iPixelFormatIndex == 0)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == false)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == false)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	GLenum aav_glew_error = glewInit();
	if (aav_glew_error != GLEW_OK)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL Enable Extensions 
	GLint  aav_numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &aav_numExtension);
	for (int i = 0; i < aav_numExtension; i++)
	{
		fprintf(aav_gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	//**VERTEX SHADER***
	aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 aav_u_mvpMatrix;" \
		"out vec2 aav_out_TexCoord;" \
		"void main(void)" \
		"{" \
		"gl_Position = aav_u_mvpMatrix * vPosition;" \
		"aav_out_TexCoord = vTexCoord;" \
		"}";
	glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(aav_gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* aav_szBuffer = NULL;

	glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
					&aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**FRAGMENT SHADER**
	aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"in vec2 aav_out_TexCoord;" \
		"uniform sampler2D u_texture_sampler;" \
		"out vec4 aav_FragColor;" \
		"void main(void)" \
		"{" \
		"	aav_FragColor = texture(u_texture_sampler,aav_out_TexCoord);" \
		"}";
	glShaderSource(aav_gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(aav_gFragmentShaderObject);

	aav_szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
					&aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	aav_gShaderProgramObject = glCreateProgram();

	glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_TEXCORD, "vTexCoord");

	glLinkProgram(aav_gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	aav_szBuffer = NULL;
	
	glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//Post Linking Information
	aav_mvp_MatrixUniform		 = glGetUniformLocation(aav_gShaderProgramObject, "aav_u_mvpMatrix");

	
	const GLfloat aav_cubeTexCoord[] =
	{ 
	1.0f,1.0f,
	0.0f,1.0f,
	0.0f,0.0f,
	1.0f,0.0f,

	1.0f,1.0f,
	0.0f,1.0f,
	0.0f,0.0f,
	1.0f,0.0f
	};

	//Record Square
	glGenVertexArrays(1,&aav_gVao_cube);
	glBindVertexArray(aav_gVao_cube);

	glGenBuffers(1,&aav_gVbo_position_cube);
	glBindBuffer(GL_ARRAY_BUFFER,aav_gVbo_position_cube);
	glBufferData(GL_ARRAY_BUFFER, 4*3*sizeof(GLfloat), NULL,GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &aav_gVbo_texture_cube);
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_texture_cube);
	glBufferData(GL_ARRAY_BUFFER,sizeof(aav_cubeTexCoord), aav_cubeTexCoord, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_TEXCORD, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_TEXCORD);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Record Off / Pause 
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_TEXTURE_2D);
	loadGLTexture();
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void loadGLTexture(void)
{
	//Fucntion declaration
	void MakeCheckImage(void);

	MakeCheckImage();
	glGenTextures(1,&aav_uiTextImage);
	glBindTexture(GL_TEXTURE_2D, aav_uiTextImage);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);


	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, CHECKER_IMAGE_WIDTH, CHECKER_IMAGE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, aav_checkImage);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}

void MakeCheckImage(void)
{
	//variable Declaration
	int aav_i, aav_j, aav_c;
	//Code
	for (aav_i = 0; aav_i < CHECKER_IMAGE_HEIGHT; aav_i++)
	{
		for (aav_j = 0; aav_j < CHECKER_IMAGE_WIDTH; aav_j++)
		{
			aav_c = (((aav_i & 0x8) == 0) ^ ((aav_j & 0x8) == 0)) * 255;
			aav_checkImage[aav_i][aav_j][0] = (GLubyte)aav_c;
			aav_checkImage[aav_i][aav_j][1] = (GLubyte)aav_c;
			aav_checkImage[aav_i][aav_j][2] = (GLubyte)aav_c;
			aav_checkImage[aav_i][aav_j][3] = 255;
		}
	}
}

void display(void)
{

	//fuction declartion
	void update();

	GLfloat aav_texCord[8] = {NULL};
	mat4 aav_scaMatrix;
	mat4 aav_translateMatrix;
	

	const GLfloat aav_cubeVertex[] =
	{
	-2.0f, -1.0f, 0.0f,
	-2.0f, 1.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, -1.0f, 0.0f,
	};

	const GLfloat aav_cubeVertexAngle[] =
	{
		1.0f, -1.0f, 0.0f,
	1.0f, 1.0f, 0.0f,
	2.41421f, 1.0f, -1.41421f,
	2.41421f, -1.0f, -1.41421f
	};

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 
	glUseProgram(aav_gShaderProgramObject);
	
	mat4 aav_modelViewMateix = mat4::identity();
	mat4 aav_modelViewProjectMatrix = mat4::identity();
	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -3.6f);
	
	aav_modelViewMateix = aav_translateMatrix;
	aav_modelViewProjectMatrix = aav_PerspectiveProjectionMatrix * aav_modelViewMateix;

	glUniformMatrix4fv(aav_mvp_MatrixUniform, 1, GL_FALSE, aav_modelViewProjectMatrix);

	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, aav_uiTextImage);
	glUniform1i(aav_textureSamplerUniform, 0);
	
	//Plane Quad
	glBindVertexArray(aav_gVao_cube);
	
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position_cube);
	glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), aav_cubeVertex, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glBindVertexArray(0);

	//Second Quad
	glBindVertexArray(aav_gVao_cube);
	
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_position_cube);
	glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), aav_cubeVertexAngle, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

	glBindVertexArray(0);
	
	glUseProgram(0);

	SwapBuffers(aav_ghdc);
}


void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code 
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (aav_gVao_cube)
	{
		glDeleteVertexArrays(1, &aav_gVao_cube);
		aav_gVao_cube = 0;
	}

	if (aav_gVbo_position_cube)
	{
		glDeleteBuffers(1, &aav_gVbo_position_cube);
		aav_gVbo_position_cube = 0;
	}

	if (aav_gVbo_texture_cube)
	{
		glDeleteBuffers(1, &aav_gVbo_texture_cube);
		aav_gVbo_texture_cube = 0;
	}

	glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glDeleteShader(aav_gVertexShaderObject);
	aav_gVertexShaderObject = 0;
	glDeleteShader(aav_gFragmentShaderObject);
	aav_gFragmentShaderObject = 0;

	glDeleteProgram(aav_gShaderProgramObject);
	aav_gShaderProgramObject = 0;

	glUseProgram(0);


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(aav_ghrc);
	aav_ghrc = NULL;

	ReleaseDC(aav_ghwnd, aav_ghdc);
	aav_ghdc = NULL;

	
	if (aav_gpFile)
	{
	fprintf(aav_gpFile, "Log File is Successfully Closed. \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}
}