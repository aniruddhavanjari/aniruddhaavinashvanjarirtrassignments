#include <windows.h>
#include <stdio.h>
#include "twoLightsOnPyramid.h"
#include  <gl\glew.h> // This must be include before GL.h
#include <gl\GL.h>
#include "vmath.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL; 
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

//shader
GLuint aav_gVertexShaderObject;
GLuint aav_gFragmentShaderObject;
GLuint aav_gShaderProgramObject;

GLuint aav_Vao_pyramid;
GLuint aav_Vbo_pyramid_position;
GLuint aav_Vbo_pyramid_normals;

GLfloat aav_anglePyramid = 0.0f;

bool aav_bAnimationflag = false;
bool aav_bLight = false;

//matrix
mat4 aav_PerspectiveProjectionMatrix;


//uniforms
GLuint aav_viewMatrixUniform;
GLuint aav_modelMatrixUniform;
GLuint aav_ProjectionMatrixUniform;

GLuint aav_laUniform[2];
GLuint aav_ldUniform[2];
GLuint aav_lsUniform[2];
GLuint aav_lightPositionUniform[2];

GLuint aav_kaUniform;
GLuint aav_kdUniform;
GLuint aav_ksUniform;

GLuint aav_kShininessUniform;

GLuint aav_lKeyPressedUniform;


struct Light
{
	vec4 lightAmbiant;
	vec4 lightDefuse;
	vec4 lightSpecular;
	vec4 lightPosition;
};



struct Matrial
{
	vec4 materialAmbiant;
	vec4 materialDiffuse;
	vec4 materialSpecular;
	float materialShininess;
};

struct Light light[2] = { {{1.0f,0.0f,0.0f,1.0f},{1.0f,0.0f,0.0f,1.0f},{1.0f,0.0f,0.0f,1.0f},{-2.0f,0.0f,-0.0f,1.0f}},
						  {{0.0f,0.0f,1.0f,1.0f},{0.0f,0.0f,1.0f,1.0f},{0.0f,0.0f,1.0f,1.0f},{2.0f,0.0f,-0.0f,1.0f}}};


struct Matrial material = { {0.0f,0.0f,0.0f,1.0f},{1.0f,1.0f,1.0f,1.0f},{1.0f,1.0f,1.0f,1.0f},128.0f};
//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	

	//code
	if (fopen_s(&aav_gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("OpenGL Programmable Pipeline : Two Lights On Pyramid"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	initialize();
	
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
					aav_bDone = true;
			}
		}
		
	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			aav_gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'a':
		case 'A':
			if (aav_bAnimationflag == false)
			{
				aav_bAnimationflag = true;
			}
			else
			{
				aav_bAnimationflag = false;
			}
		break;
		case 'l':
		case 'L':
			if (aav_bLight == false)
			{
				aav_bLight = true;
			}
			else
			{
				aav_bLight = false;
			}
		break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO aav_mi;

	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
						SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// function prototype
	void uninitialize(void);
	void resize(int ,int);

	//variable decalrations 
	PIXELFORMATDESCRIPTOR aav_pfd;
	int aav_iPixelFormatIndex;

	//code
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;
	aav_pfd.cDepthBits = 32;

	aav_ghdc = GetDC(aav_ghwnd);

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);
	if (aav_iPixelFormatIndex == 0)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == false)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == false)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	GLenum aav_glew_error = glewInit();
	if (aav_glew_error != GLEW_OK)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL Enable Extensions 
	GLint  aav_numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &aav_numExtension);
	for (int i = 0; i < aav_numExtension; i++)
	{
		fprintf(aav_gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	//**VERTEX SHADER***
	aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la[2];" \
		"uniform vec3 u_ld[2];" \
		"uniform vec3 u_ls[2];" \
		"uniform vec4 u_lightPosistion[2];" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_kShineness;"\

		"vec3 lightDirection[2];" \
		"vec3 reflection_vector[2];" \
		"vec3 ambiant[2];" \
		"vec3 diffuse[2];" \
		"vec3 specular[2];" \

		"out vec3 fong_ads_light;" \
		"void main(void)" \
		"{" \
		"	if(u_lKeyPressed == 1)" \
		"	{"\
		"		vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition; " \
		"		vec3 tranformed_normal = normalize(mat3(u_view_matrix * u_model_matrix) * vNormal); " \
		"		vec3 view_vector = normalize(-eyeCordinate.xyz);" \
		"		for(int i = 0 ; i < 2 ; i++)"\
		"		{"\
		"			lightDirection[i] = normalize(vec3(u_lightPosistion[i] - eyeCordinate));" \
		"			reflection_vector[i] = reflect(-lightDirection[i],tranformed_normal);" \
		"			ambiant[i] = u_la[i] * u_ka;" \
		"			diffuse[i] = u_ld[i] * u_kd * max(dot(lightDirection[i],tranformed_normal),0.0f);" \
		"			specular[i] = u_ls[i] * u_ks * pow(max(dot(reflection_vector[i],view_vector),0.0f),u_kShineness);" \
		"			fong_ads_light = fong_ads_light + ambiant[i] + diffuse[i] + specular[i];" \
		"		}"\
		"	}" \
		"	else" \
		"	{" \
		"		fong_ads_light = vec3(1.0f,1.0f,1.0f);" \
		"	}" \
		"gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(aav_gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* aav_szBuffer = NULL;

	glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
					&aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**FRAGMENT SHADER**
	aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar *fragmentShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"in vec3 fong_ads_light;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"	FragColor = vec4(fong_ads_light,1.0f);" \
		"}";
	glShaderSource(aav_gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(aav_gFragmentShaderObject);


	aav_szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
					&aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	aav_gShaderProgramObject = glCreateProgram();

	glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glLinkProgram(aav_gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	aav_szBuffer = NULL;
	
	glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			aav_szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, aav_szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", aav_szBuffer);
				free(aav_szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//Post Linking Information
	aav_modelMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_model_matrix");
	aav_viewMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_view_matrix");
	aav_ProjectionMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_projection_matrix");

	aav_laUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_la[0]");
	aav_ldUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_ld[0]");
	aav_lsUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_ls[0]");
	aav_lightPositionUniform[0] = glGetUniformLocation(aav_gShaderProgramObject, "u_lightPosistion[0]");

	aav_laUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_la[1]");
	aav_ldUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_ld[1]");
	aav_lsUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_ls[1]");
	aav_lightPositionUniform[1] = glGetUniformLocation(aav_gShaderProgramObject, "u_lightPosistion[1]");

	aav_kaUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ka");
	aav_kdUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kd");
	aav_ksUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ks");

	aav_kShininessUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kShineness");

	aav_lKeyPressedUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_lKeyPressed");

	//vertices array declation
	const GLfloat aav_pyramidVertex[] =
	{
	0.0f,1.0f,0.0f,
	-1.0f,-1.0f,1.0f,
	1.0f,-1.0f,1.0f,

	0.0f,1.0f,0.0f,
	1.0f,-1.0,1.0f,
	1.0f,-1.0f,-1.0f,

	0.0f,1.0f,0.0f,
	-1.0f,-1.0f,-1.0f,
	1.0f,-1.0f,-1.0f,

	0.0f,1.0f,0.0f,
	-1.0f,-1.0f,1.0f,
	-1.0f,-1.0f,-1.0f
	};
	
	const GLfloat aav_pyramidNormals[] =
	{ 
		0.0f,0.447214f,0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,

		0.894427f,0.447214f,0.0f,
		0.894427f, 0.447214f, 0.0f,
		0.894427f, 0.447214f, 0.0f,

		0.0f,0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,
		0.0f, 0.447214f, -0.894427f,

		-0.894427f, 0.447214f, 0.0f,
		-0.894427f, 0.447214f, 0.0f,
		-0.894427f,0.447214f,0.0f
	};


	//Record Square
	glGenVertexArrays(1,&aav_Vao_pyramid);
	glBindVertexArray(aav_Vao_pyramid);

	glGenBuffers(1,&aav_Vbo_pyramid_position);
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_pyramid_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pyramidVertex), aav_pyramidVertex,GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &aav_Vbo_pyramid_normals);
	glBindBuffer(GL_ARRAY_BUFFER, aav_Vbo_pyramid_normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_pyramidNormals), aav_pyramidNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Record Off / Pause 
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();

	aav_bAnimationflag = false;
	aav_bLight = false;

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{

	//fuction declartion
	void update();

	//GLfloat aav_lightPosition[] = {0.0f,0.0f,2.0f,1.0f};

	mat4 aav_scaMatrix;
	mat4 aav_translateMatrix;
	mat4 aav_rotationMatrixA;
	mat4 aav_rotationMatrixY;

	vec4 aav_lightPosition;
	aav_lightPosition = { 0.0f,0.0f,2.0f,1.0f };
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 
	//triangle
	glUseProgram(aav_gShaderProgramObject);

	if (aav_bLight == true)
	{
		//Light Enable
		glUniform1i(aav_lKeyPressedUniform, 1);
		//Light-1
		glUniform3fv(aav_laUniform[0], 1, light[0].lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform[0], 1, light[0].lightDefuse);				// ld
		glUniform3fv(aav_lsUniform[0], 1, light[0].lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform[0], 1,light[0].lightPosition);	//lightPosition

		//Light-2
		glUniform3fv(aav_laUniform[1], 1, light[1].lightAmbiant);				// la 
		glUniform3fv(aav_ldUniform[1], 1, light[1].lightDefuse);				// ld
		glUniform3fv(aav_lsUniform[1], 1, light[1].lightSpecular);				// ls
		glUniform4fv(aav_lightPositionUniform[1], 1, light[1].lightPosition);	//lightPosition

		//material
		glUniform3fv(aav_kaUniform, 1, material.materialAmbiant);	// ka
		glUniform3fv(aav_kdUniform, 1, material.materialDiffuse);	// kd
		glUniform3fv(aav_ksUniform, 1, material.materialSpecular);	//ks
		glUniform1f(aav_kShininessUniform,material.materialShininess);
	}
	else
	{
		glUniform1i(aav_lKeyPressedUniform, 0);
	}


	//OpenGL Drawing
	mat4 aav_modelMateix = mat4::identity();
	mat4 aav_viewMatrix = mat4::identity();
	
	aav_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	
	aav_rotationMatrixY = vmath::rotate(aav_anglePyramid, 0.0f, 1.0f, 0.0f);
	
	aav_rotationMatrixA =  aav_rotationMatrixY ;
	
	aav_modelMateix = aav_translateMatrix *aav_rotationMatrixA;

	glUniformMatrix4fv(aav_modelMatrixUniform, 1, GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);


	glBindVertexArray(aav_Vao_pyramid);
	glDrawArrays(GL_TRIANGLES, 0, 12);

	glBindVertexArray(0);
	
	glUseProgram(0);

	if (aav_bAnimationflag == TRUE)
	{
		update();
	}
	SwapBuffers(aav_ghdc);
}

void update()
{	
	aav_anglePyramid = aav_anglePyramid + 1.00f;
	if(aav_anglePyramid >= 360.0f)
		aav_anglePyramid = 0.0f;
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code 
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	
	if (aav_Vao_pyramid)
	{
		glDeleteVertexArrays(1, &aav_Vao_pyramid);
		aav_Vao_pyramid = 0;
	}

	if (aav_Vbo_pyramid_position)
	{
		glDeleteBuffers(1, &aav_Vbo_pyramid_position);
		aav_Vbo_pyramid_position = 0;
	}

	if (aav_Vbo_pyramid_normals)
	{
		glDeleteBuffers(1, &aav_Vbo_pyramid_normals);
		aav_Vbo_pyramid_normals = 0;
	}


	glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glDeleteShader(aav_gVertexShaderObject);
	aav_gVertexShaderObject = 0;
	glDeleteShader(aav_gFragmentShaderObject);
	aav_gFragmentShaderObject = 0;

	glDeleteProgram(aav_gShaderProgramObject);
	aav_gShaderProgramObject = 0;

	glUseProgram(0);


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(aav_ghrc);
	aav_ghrc = NULL;

	ReleaseDC(aav_ghwnd, aav_ghdc);
	aav_ghdc = NULL;

	if (aav_gpFile)
	{
	fprintf(aav_gpFile, "Log File is Successfully Closed. \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}
}