 #include <windows.h>
#include <stdio.h>
#include "interleaved.h"
#include  <gl\glew.h> 
#include <gl\GL.h>
#include "vmath.h"


#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")


#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	AAV_ATTRIBUTE_POSITION = 0,
	AAV_ATTRIBUTE_COLOR,
	AAV_ATTRIBUTE_NORMAL,
	AAV_ATTRIBUTE_TEXCORD,
};

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL; 
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

GLuint aav_gVertexShaderObject;
GLuint aav_gFragmentShaderObject;
GLuint aav_gShaderProgramObject;


GLuint aav_mvp_MatrixUniform;

mat4 aav_PerspectiveProjectionMatrix;

//Sphere Variable
GLfloat aav_sphere_vertices[1146];
GLfloat aav_sphere_normals[1146];
GLfloat aav_sphere_texture[764];
unsigned short aav_sphere_elements[2280];
GLuint aav_numSphereVertices;
GLuint aav_numSphereElements;

GLuint aav_gVao_cube;
GLuint aav_gVbo_cubeAll;


bool aav_bLight = false;
 
//Light Array
GLfloat aav_lightAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };	//la
GLfloat aav_lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	// ld
GLfloat aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };//ls

GLfloat aav_lightPosition[] = { 0.0f,0.0f,2.0f,1.0f };

//material Array 
GLfloat aav_materialAmbiant[]	= { 0.0f, 0.0f, 0.0f ,0.0f}; // ka 
GLfloat aav_materialDiffuse[]	= {1.0f,1.0f,1.0f,1.0f};	// kd
GLfloat aav_materialSpecular[]	= {1.0f,1.0f,1.0f,1.0f};	// ks


GLuint aav_viewMatrixUniform;
GLuint aav_modelMatrixUniform;
GLuint aav_ProjectionMatrixUniform;

GLuint aav_laUniform;
GLuint aav_ldUniform;
GLuint aav_lsUniform;
GLuint aav_lightPositionUniform;

GLuint aav_kShininessUniform;

GLuint aav_lKeyPressedUniform;

GLuint aav_textureSamplerUniform;

BOOL aav_AnimationFlag = FALSE;

GLfloat aav_rotation = 0.0f;

GLuint aav_texture;
//////////////////////////////////////////////////////////////////////////////////

float* vertexArray = NULL;

//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	

	//code
	if (fopen_s(&aav_gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("Aniruddha Avinash Vanjari : OpenGL Programmable Pipeline : InterLeaved"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	initialize();
	
	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
					aav_bDone = true;
			}
		}
		
	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			aav_gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'l':
		case 'L':
			if (aav_bLight == false)
			{
				aav_bLight = true;
			}
			else
			{
				aav_bLight = false;
			}
			break;
		case 'A':
		case 'a':
			if (aav_AnimationFlag == FALSE)
			{
				aav_AnimationFlag = TRUE;
			}
			else
			{
				aav_AnimationFlag = FALSE;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO aav_mi;

	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
						aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
						SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// function prototype
	void uninitialize(void);
	void resize(int ,int);
	bool loadGLTexture(GLuint*, TCHAR[]);

	//variable decalrations 
	PIXELFORMATDESCRIPTOR aav_pfd;
	int aav_iPixelFormatIndex;

	//code
	ZeroMemory(&aav_pfd, sizeof(PIXELFORMATDESCRIPTOR));

	aav_pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	aav_pfd.nVersion = 1;
	aav_pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	aav_pfd.iPixelType = PFD_TYPE_RGBA;
	aav_pfd.cColorBits = 32;
	aav_pfd.cRedBits = 8;
	aav_pfd.cGreenBits = 8;
	aav_pfd.cBlueBits = 8;
	aav_pfd.cAlphaBits = 8;
	aav_pfd.cDepthBits = 32;

	aav_ghdc = GetDC(aav_ghwnd);

	aav_iPixelFormatIndex = ChoosePixelFormat(aav_ghdc, &aav_pfd);
	if (aav_iPixelFormatIndex == 0)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (SetPixelFormat(aav_ghdc, aav_iPixelFormatIndex, &aav_pfd) == false)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	aav_ghrc = wglCreateContext(aav_ghdc);
	if (aav_ghrc == NULL)
	{
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	if (wglMakeCurrent(aav_ghdc, aav_ghrc) == false)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	GLenum aav_glew_error = glewInit();
	if (aav_glew_error != GLEW_OK)
	{
		wglDeleteContext(aav_ghrc);
		aav_ghrc = NULL;
		ReleaseDC(aav_ghwnd, aav_ghdc);
		aav_ghdc = NULL;
	}

	//OpenGL Related Log 
	fprintf(aav_gpFile, "OpenGL Vender : %s\n", glGetString(GL_VENDOR));
	fprintf(aav_gpFile, "OpenGL Renderer: %s\n", glGetString(GL_RENDERER));
	fprintf(aav_gpFile, "OpenGL Version: %s\n", glGetString(GL_VERSION));
	fprintf(aav_gpFile, "OpenGL GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	//OpenGL Enable Extensions 
	GLint  aav_numExtension;
	
	glGetIntegerv(GL_NUM_EXTENSIONS, &aav_numExtension);
	for (int i = 0; i < aav_numExtension; i++)
	{
		fprintf(aav_gpFile, "%s\n", glGetStringi(GL_EXTENSIONS,i));
	}

	//**VERTEX SHADER***
	aav_gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);
	const GLchar* aav_vertexShaserSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;"	\
		"in vec3 vNormal;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_lightPosistion;" \
		"out vec3 tranformed_normal;"\
		"out vec3 lightDirection;" \
		"out vec3 view_vector;" \
		"out vec2 aav_out_TexCoord;" \
		"out vec4 aav_out_color;" \
		"void main(void)" \
		"{" \
		"	vec4 eyeCordinate = u_view_matrix * u_model_matrix * vPosition;" \
		"	tranformed_normal = (mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		"	lightDirection = (vec3(u_lightPosistion - eyeCordinate));" \
		"	view_vector = (-eyeCordinate.xyz);" \
		"	aav_out_TexCoord = vTexCoord; " \
		"	aav_out_color = vColor;" \
		"	gl_Position = u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";
	glShaderSource(aav_gVertexShaderObject, 1, (const GLchar**)&aav_vertexShaserSourceCode, NULL);

	//compile shader , Error checking of Compilation
	glCompileShader(aav_gVertexShaderObject);
	
	GLint aav_infoLogLength = 0;
	GLint aav_shaderCompiledStatus = 0;
	char* szBuffer = NULL;

	glGetShaderiv(aav_gVertexShaderObject, GL_COMPILE_STATUS, &aav_shaderCompiledStatus);

	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gVertexShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gVertexShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Vertex Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**FRAGMENT SHADER**
	aav_gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 aav_out_color;" \
		"in vec3 tranformed_normal;"\
		"in vec3 lightDirection;" \
		"in vec3 view_vector;" \
		"in vec2 aav_out_TexCoord;" \
		"uniform int u_lKeyPressed;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \

		"uniform float u_kShineness;"\

		"out vec4 FragColor;" \
		"vec3 fong_ads_light;" \
		"uniform sampler2D u_texture_sampler;" \
		"void main(void)" \
		"{" \
		"		vec3 normalize_tranformed_normal = normalize(tranformed_normal);" \
		"		vec3 normalize_lightDirection = normalize(lightDirection);" \
		"		vec3 normalize_view_vector = normalize(view_vector);" \
		"		vec3 reflection_vector = reflect(-normalize_lightDirection,normalize_tranformed_normal);" \
		"		vec3 ambiant = u_la ;" \
		"		vec3 diffuse = u_ld  * max(dot(normalize_lightDirection,normalize_tranformed_normal),0.0f);" \
		"		vec3 specular = u_ls  * pow(max(dot(reflection_vector,normalize_view_vector),0.0f),u_kShineness);" \
		"		fong_ads_light = ambiant + diffuse + specular;" \
		"		fong_ads_light = fong_ads_light * vec3(texture(u_texture_sampler,aav_out_TexCoord));" \
		"		FragColor = vec4(fong_ads_light,1.0f)  *  aav_out_color; " \
		"}";
	glShaderSource(aav_gFragmentShaderObject, 1, 
		(const GLchar**)&fragmentShaderSourceCode,NULL);
	//compile shader
	glCompileShader(aav_gFragmentShaderObject);


	szBuffer = NULL;
	aav_infoLogLength = 0;
	aav_shaderCompiledStatus = 0;

	glGetShaderiv(aav_gFragmentShaderObject, GL_COMPILE_STATUS, 
		&aav_shaderCompiledStatus);
	if (aav_shaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(aav_gFragmentShaderObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (szBuffer != NULL)
			{
				GLsizei aav_written;
				glGetShaderInfoLog(aav_gFragmentShaderObject, aav_infoLogLength,
					&aav_written, szBuffer);
				fprintf(aav_gpFile, "Fragment Shader Compilation Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//**SHADER PROGRAM**
	//Create 
	aav_gShaderProgramObject = glCreateProgram();

	glAttachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glAttachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_POSITION, "vPosition");

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_NORMAL, "vNormal");

	glBindAttribLocation(aav_gShaderProgramObject, AAV_ATTRIBUTE_TEXCORD, "vTexCoord");

	glLinkProgram(aav_gShaderProgramObject);

	aav_infoLogLength = 0;
	GLint aav_shaderProgramLinkStatus = 0;
	szBuffer = NULL;
	
	glGetProgramiv(aav_gShaderProgramObject, GL_LINK_STATUS, &aav_shaderProgramLinkStatus);
	if (aav_shaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(aav_gShaderProgramObject, GL_INFO_LOG_LENGTH, &aav_infoLogLength);
		if (aav_infoLogLength > 0)
		{
			szBuffer = (char*)malloc(aav_infoLogLength);
			if (aav_infoLogLength > 0)
			{
				GLsizei aav_aav_written;
				glGetProgramInfoLog(aav_gShaderProgramObject, aav_infoLogLength,
					&aav_aav_written, szBuffer);
				fprintf(aav_gpFile, "Shader Program Link Log: %s\n", szBuffer);
				free(szBuffer);
				DestroyWindow(aav_ghwnd);
			}
		}
	}

	//Post Linking Information
	/**************************************/
	aav_textureSamplerUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_texture_sampler");

	aav_modelMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_model_matrix");
	aav_viewMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_view_matrix");
	aav_ProjectionMatrixUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_projection_matrix");

	aav_laUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_la");
	aav_ldUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ld");
	aav_lsUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_ls");
	aav_lightPositionUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_lightPosistion");

	aav_kShininessUniform = glGetUniformLocation(aav_gShaderProgramObject, "u_kShineness");

	/**************************************/
	
	const GLfloat aav_cubeVertex[] =
	{
	0.5f,0.5f,0.5f,		1.0f,0.0f,0.0f,		0.0f, 0.0f, 1.0f,	0.0f,0.0f,
	-0.5f,0.5f,0.5f,	1.0f,0.0f,0.0f,		0.0f, 0.0f, 1.0f,	1.0f,0.0f,
	-0.5f,-0.5f,0.5f,	1.0f,0.0f,0.0f,		0.0f, 0.0f, 1.0f,	1.0f,1.0f,
	0.5f,-0.5f,0.5f,	1.0f,0.0f,0.0f,		0.0f, 0.0f, 1.0f,	0.0f,1.0f,

	0.5f, 0.5f, -0.5f,	0.0f,1.0f,0.0f,		1.0, 0.0f, 0.0f,	0.0f, 0.0f,
	0.5f, 0.5f, 0.5f,	0.0f,1.0f,0.0f,		1.0, 0.0f, 0.0f,	1.0f, 0.0f,
	0.5f, -0.5f, 0.5f,	0.0f,1.0f,0.0f,		1.0, 0.0f, 0.0f,	1.0f, 1.0f,
	0.5f, -0.5f, -0.5f,	0.0f,1.0f,0.0f,		1.0, 0.0f, 0.0f,	0.0f, 1.0f,

	0.5f, 0.5f, -0.5f,		0.0f,0.0f,1.0f,		0.0f, 0.0, -1.0f,	0.0f, 0.0f,
	-0.5f, 0.5f, -0.5f,		0.0f,0.0f,1.0f,		0.0f, 0.0, -1.0f,	1.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,	0.0f,0.0f,1.0f,		0.0f, 0.0, -1.0f,	1.0f, 1.0f,
	0.5f, -0.5f, -0.5f,		0.0f,0.0f,1.0f,		0.0f, 0.0, -1.0f,	0.0f, 1.0f,
	 
	-0.5f, 0.5f, -0.5f,		0.0f, 1.0f, 1.0f,	 -1.0f, 0.0f, 0.0f,		0.0f, 0.0f,
	-0.5f, 0.5f, 0.5f,		0.0f, 1.0f, 1.0f,	 -1.0f, 0.0f, 0.0f,		1.0f, 0.0f,
	-0.5f, -0.5f, 0.5f,		0.0f, 1.0f, 1.0f,	 -1.0f, 0.0f, 0.0f,		1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,	0.0f, 1.0f, 1.0f,	 -1.0f, 0.0f, 0.0f,		0.0f, 1.0f,

	0.5f, 0.5f, -0.5f,	1.0f, 0.0f, 1.0f,	0.0f, 1.0f, 0.0f,	0.0f, 0.0f,
	-0.5f, 0.5f, -0.5f,	1.0f, 0.0f, 1.0f,	0.0f, 1.0f, 0.0f,	1.0f, 0.0f,
	-0.5f, 0.5f, 0.5f,	1.0f, 0.0f, 1.0f,	 0.0f, 1.0f, 0.0f,	1.0f, 1.0f,
	0.5f, 0.5f, 0.5f,	1.0f, 0.0f, 1.0f,	0.0f, 1.0f, 0.0f,	0.0f, 1.0f,

	0.5f, -0.5f, -0.5f,		1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,	0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,	1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,	1.0f, 0.0f,
	-0.5f, -0.5f, 0.5f,		1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,	1.0f, 1.0f,
	0.5f, -0.5f, 0.5f,		1.0f, 1.0f, 0.0f, 0.0, -1.0, 0.0f,	0.0f, 1.0f
	};



	glGenVertexArrays(1, &aav_gVao_cube);
	glBindVertexArray(aav_gVao_cube);

	//Record Sphere
	glGenBuffers(1, &aav_gVbo_cubeAll); 
	glBindBuffer(GL_ARRAY_BUFFER, aav_gVbo_cubeAll);
	glBufferData(GL_ARRAY_BUFFER, sizeof(aav_cubeVertex), aav_cubeVertex, GL_STATIC_DRAW);
	glVertexAttribPointer(AAV_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(AAV_ATTRIBUTE_POSITION);
	
	//color
	glVertexAttribPointer(AAV_ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(AAV_ATTRIBUTE_COLOR);

	//normals
	glVertexAttribPointer(AAV_ATTRIBUTE_NORMAL, 3, GL_FLOAT,GL_FALSE, 11 * sizeof(float), (void*)(6 * sizeof(float)));
	glEnableVertexAttribArray(AAV_ATTRIBUTE_NORMAL);

	//texture
	glVertexAttribPointer(AAV_ATTRIBUTE_TEXCORD, 2, GL_FLOAT, GL_FALSE, 11 * sizeof(float), (void*)(9 * sizeof(float)));
	glEnableVertexAttribArray(AAV_ATTRIBUTE_TEXCORD);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_TEXTURE_2D);
	loadGLTexture(&aav_texture, MAKEINTRESOURCE(TEXTURE_BBITMAP));
	
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	aav_PerspectiveProjectionMatrix = mat4::identity();

	resize(WIN_WIDTH, WIN_HEIGHT);
}

bool loadGLTexture(GLuint* aav_texture, TCHAR aav_resourceID[])
{
	//Variable Declaration
	bool aav_bResult = false;
	HBITMAP aav_hBitmap = NULL; // OS Image Loading
	BITMAP aav_bmp;

	//Code
	aav_hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL),
		aav_resourceID,
		IMAGE_BITMAP,
		0,
		0,
		LR_CREATEDIBSECTION);

	if (aav_hBitmap)
	{
		aav_bResult = true;
		GetObject(aav_hBitmap, sizeof(BITMAP), &aav_bmp);

		//From Here Starts OpenGL Texture Code.
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		glGenTextures(1, aav_texture); // Gatu
		glBindTexture(GL_TEXTURE_2D, *aav_texture);

		//Setting Texture Paramter
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

		//Following will Push Data into Memory
		glTexImage2D(GL_TEXTURE_2D, 0,
			GL_RGB, aav_bmp.bmWidth, aav_bmp.bmHeight,
			0, GL_BGR_EXT, GL_UNSIGNED_BYTE, aav_bmp.bmBits);
		glGenerateMipmap(GL_TEXTURE_2D);

		DeleteObject(aav_hBitmap);
	}
	return aav_bResult;
}


void display(void)
{
	//variable Declaration
	mat4 aav_rotationMatrixA;
	mat4 aav_rotationMatrixX;
	mat4 aav_rotationMatrixY;
	mat4 aav_rotationMatrixZ;

	//fuction declartion
	void update();

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	 
	//triangle
	glUseProgram(aav_gShaderProgramObject);

	//Light
	glUniform3fv(aav_laUniform,1,(GLfloat *)aav_lightAmbiant);				// la 
	glUniform3fv(aav_ldUniform,1,(GLfloat *)aav_lightDiffuse);				// ld
	glUniform3fv(aav_lsUniform,1,(GLfloat *)aav_lightSpecular);				// ls
	glUniform4fv(aav_lightPositionUniform,1,(GLfloat *)aav_lightPosition);	//lightPosition

 	glUniform1f(aav_kShininessUniform,128.0f);
	
	
	//OpenGL Drawing
	mat4 aav_modelMateix = mat4::identity();
	mat4 aav_viewMatrix = mat4::identity();
	mat4 aav_rotationMatrix = mat4::identity();
	mat4 aav_scaleMatrix = mat4::identity();

	mat4 aav_translateMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	aav_rotationMatrixX = vmath::rotate(aav_rotation, 1.0f, 0.0f, 0.0f);
	aav_rotationMatrixY = vmath::rotate(aav_rotation, 0.0f, 1.0f, 0.0f);
	aav_rotationMatrixZ = vmath::rotate(aav_rotation, 0.0f, 0.0f, 1.0f);

	aav_rotationMatrixA = aav_rotationMatrixX * aav_rotationMatrixY * aav_rotationMatrixZ;

	aav_modelMateix = aav_translateMatrix *aav_scaleMatrix * aav_rotationMatrixA;

	glUniformMatrix4fv(aav_modelMatrixUniform,1,GL_FALSE, aav_modelMateix);
	glUniformMatrix4fv(aav_viewMatrixUniform, 1, GL_FALSE, aav_viewMatrix);
	glUniformMatrix4fv(aav_ProjectionMatrixUniform, 1, GL_FALSE, aav_PerspectiveProjectionMatrix);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, aav_texture);
	glUniform1i(aav_textureSamplerUniform, 0);


	glBindVertexArray(aav_gVao_cube);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 8, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 12, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 16, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 20, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 24, 4);
	glBindVertexArray(0);

	update();

	SwapBuffers(aav_ghdc);
}

void update()
{
	aav_rotation = aav_rotation + 0.01f;
	if (aav_rotation > 360)
	{
		aav_rotation = 0.0f;
	}
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	aav_PerspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//code 
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (aav_gVao_cube)
	{
		glDeleteVertexArrays(1, &aav_gVao_cube);
		aav_gVao_cube = 0;
	}

	if (aav_gVbo_cubeAll)
	{
		glDeleteBuffers(1, &aav_gVbo_cubeAll);
		aav_gVbo_cubeAll = 0;
	}

	glDetachShader(aav_gShaderProgramObject, aav_gVertexShaderObject);
	glDetachShader(aav_gShaderProgramObject, aav_gFragmentShaderObject);

	glDeleteShader(aav_gVertexShaderObject);
	aav_gVertexShaderObject = 0;
	glDeleteShader(aav_gFragmentShaderObject);
	aav_gFragmentShaderObject = 0;

	glDeleteProgram(aav_gShaderProgramObject);
	aav_gShaderProgramObject = 0;

	glUseProgram(0);


	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(aav_ghrc);
	aav_ghrc = NULL;

	ReleaseDC(aav_ghwnd, aav_ghdc);
	aav_ghdc = NULL;

	if (aav_gpFile)
	{
	fprintf(aav_gpFile, "Log File is Successfully Closed. \n");
		fclose(aav_gpFile);
		aav_gpFile = NULL;
	}

}

