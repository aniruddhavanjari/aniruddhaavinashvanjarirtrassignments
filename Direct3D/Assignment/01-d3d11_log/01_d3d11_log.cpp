#include <stdio.h>
#include <d3d11.h> //this header file is analog to gl.h
#include <math.h>

#include <stdlib.h>

#pragma comment(lib,"d3d11.lib" )
#pragma comment(lib, "dxgi.lib")


FILE* aav_gpFile = NULL;


//dirext x graphics infrastructure
//dxgi is analogous to wgl

int main(void)
{
	//local varaible declaration
	//pure abstract base class 
	//pratic ek interface 

	IDXGIFactory* aav_pIDXGIFactory = NULL; // Interface denarya la factory 

	IDXGIAdapter* aav_pIDXGIAdapter = NULL; // All these four tecnologies represet physical decie la adapter.


	DXGI_ADAPTER_DESC aav_dxgiAdapterDesc;

	HRESULT aav_hr;

	char aav_str[255];

	//code

	if (fopen_s(&aav_gpFile, "Log.txt", "w") != 0)
	{
		fprintf(aav_gpFile, "Log File Failed To Create.\n");
		goto cleanUP;
	}
	else
	{
		fprintf(aav_gpFile, "Log File Is Successfully Opened.\n");
	}


	aav_hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&aav_pIDXGIFactory);
	//
	if (FAILED(aav_hr))
	{
		fprintf(aav_gpFile, "CreateDXGIFactory Failde\n");
		goto cleanUP;
	}

	if (aav_pIDXGIFactory->EnumAdapters(0, &aav_pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{
		fprintf(aav_gpFile, "DXGIAdapter Can Not be Found\n");
		goto cleanUP; //labled jumping
	}

	ZeroMemory((void*)&aav_dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));

	aav_hr = aav_pIDXGIAdapter->GetDesc(&aav_dxgiAdapterDesc);
	//mal ki nahe mala graphica la discriba karnrya variable la bharun phatav

	WideCharToMultiByte(CP_ACP, 0, aav_dxgiAdapterDesc.Description, 255, aav_str, 255, NULL, NULL);
	//CP_ACP :code page ANSCII code pag

	fprintf(aav_gpFile, "Graphic Card Name = %s.\n", aav_str);

	fprintf(aav_gpFile, "Graphic Memory VRAM = %I64d bytes.\n", (__int64)aav_dxgiAdapterDesc.DedicatedVideoMemory);

	fprintf(aav_gpFile, "VRAM in GB = %d GB\n", int(ceil(aav_dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0)));

cleanUP:
	//
	if (aav_pIDXGIAdapter)
	{
		aav_pIDXGIAdapter->Release();
		aav_pIDXGIAdapter = NULL;
	}

	if (aav_pIDXGIFactory)
	{
		aav_pIDXGIFactory->Release();
		aav_pIDXGIFactory = NULL;
	}

	if (aav_gpFile)
	{
		fprintf(aav_gpFile, "Log File is Successfully Closed. \n");
		fclose(aav_gpFile);
	}


	exit(0);

}

