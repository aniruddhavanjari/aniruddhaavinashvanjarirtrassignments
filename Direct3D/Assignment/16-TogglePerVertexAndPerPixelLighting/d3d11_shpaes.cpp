#include <windows.h>

#include <d3dcompiler.h>  //1

#include <stdio.h>
#include <math.h>

#include <d3d11.h>
#include "d3d11_shpaes.h"

#pragma warning(disable: 4838)
#include "XNAMath\xnamath.h" //2
#include "Sphere-dotH.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "D3dcompiler.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL;
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

ID3D11DeviceContext* aav_gpID3D11DeviceContext = NULL;
ID3D11Device* aav_gpID3D11Device = NULL;
ID3D11RenderTargetView* aav_gpID3D11RenderTargetView = NULL;
IDXGISwapChain* aav_gpIDXGISwapChain = NULL;

float aav_gClearColor[4]; //RGB

IDXGIFactory *aav_pIDXGIFactory = NULL; 

IDXGIAdapter *aav_pIDXGIAdapter = NULL; 

ID3D11VertexShader *aav_gpID3D11VertexShader_PerVertex = NULL;//1
ID3D11PixelShader *aav_gpID3D11PixelShader_PerVertex = NULL;//1

ID3D11VertexShader	*aav_gpID3D11VertexShader_PerPixel = NULL;//2
ID3D11PixelShader *aav_gpID3D11PixelShader_PerPixel = NULL;//2

ID3D11Buffer *aav_gpID3D11Buffer_VertexBuffer_Position = NULL;
ID3D11Buffer *aav_gpID3D11Buffer_VertexBuffer_Normal = NULL;
ID3D11Buffer *aav_gpID3D11Buffer_IndexBuffer = NULL;


ID3D11InputLayout *aav_gpID3D11InputLayout_PerVertex = NULL; //1 Inputlayout 
ID3D11Buffer *aav_gpID3D11Buffer_ConstatntBuffer_PerVertex = NULL;//1

ID3D11InputLayout *aav_gpID3D11InputLayout_PerPixel = NULL; //2
ID3D11Buffer *aav_gpID3D11Buffer_ConstatntBuffer_PerPixel = NULL;//2

ID3D11RasterizerState *aav_gpId3d11RasterizerState = NULL;	
ID3D11DepthStencilView *aav_gpID3D11DepthStencilView = NULL; // Depth 

struct CBUFFER
{
	XMMATRIX WorldMatrix;					//Model in OpenGL
	XMMATRIX ViewMatrix;					// View Matrix
	XMMATRIX ProjectionMatrix;				//Projection Matrix

	XMVECTOR La;
	XMVECTOR Ld;
	XMVECTOR Ls;

	XMVECTOR Ka;
	XMVECTOR Kd;
	XMVECTOR Ks;

	float MaterialShineNess;

	XMVECTOR LightPosition;
};

XMMATRIX aav_gPrespectiveProjectionMatrix; //6

//Sphere Variable
float aav_sphere_vertices[1146];
float aav_sphere_normals[1146];
float aav_sphere_texture[764];
unsigned short aav_sphere_elements[2280];
unsigned int aav_numSphereVertices;
unsigned int aav_numSphereElements;

float aav_lightAmbiant[] = {0.0f,0.0f,0.0f,1.0f};
float aav_lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float aav_lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
float aav_lightPosition[] = { 100.0f,100.0f,-100.0f,1.0f };

float aav_materialAmbiant[] = { 0.0f,0.0f,0.0f,1.0f };
float aav_materialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
float aav_materialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };

float aav_materialShineNess = 50.0f;

BOOL aav_bPerPixel = FALSE;
BOOL aav_bPerVertex = TRUE;

//Entry Point
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	HRESULT aav_hr;

	//code
	if (fopen_s(&aav_gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fclose(aav_gpFile);
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "Log File Is Successfully Opened.\n");
		fclose(aav_gpFile);
	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("Aniruddha Avinash Vanjari : Dirext3D : Toggle PerVertex And PerPixel Lighting  "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	aav_hr = initialize();
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "initialize() failed .Exitting Now...\n");
		fclose(aav_gpFile);

		uninitialize();
		
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "initialize() Success...\n");
		fclose(aav_gpFile);
	}

	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
				{
					aav_bDone = true;
				}
			}
		}

	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	HRESULT resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//variable declaration
	HRESULT aav_hr;
	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		if (aav_gpID3D11DeviceContext)
		{
			aav_hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(aav_hr))
			{
				fopen_s(&aav_gpFile, "log.txt", "a+");
				fprintf_s(aav_gpFile, "initialize() failed .Exitting Now...\n");
				fclose(aav_gpFile);

				uninitialize();

			}
			else
			{
				fopen_s(&aav_gpFile, "log.txt", "a+");
				fprintf_s(aav_gpFile, "resize() Success...\n");
				fclose(aav_gpFile);
			}
		}
		
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			aav_gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_CHAR:
		switch (wParam)
		{
		case 'p':
		case 'P':
			if (aav_bPerPixel == false)
			{
				aav_bPerVertex = false;
				aav_bPerPixel = true;
			}

			break;
		case 'v':
		case 'V':
			if (aav_bPerVertex == false)
			{
				aav_bPerPixel = false;
				aav_bPerVertex = true;
			}

			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO aav_mi;

	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	//fucntion protype 
	HRESULT resize(int, int);
	HRESULT gpuDeteail(void);
	void uninitialize(void);

	//printf3DInfo(); 
	//variable declaration
	HRESULT aav_hr;
	DXGI_SWAP_CHAIN_DESC aav_dxgiSwapChainDesc;
	
	D3D_FEATURE_LEVEL aav_d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL aav_d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default low dumy

	D3D_DRIVER_TYPE aav_d3dDriverType;
	D3D_DRIVER_TYPE aav_d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};

	UINT aav_createDeviceFlags = 0;
	UINT aav_numDriverTypes = 0;
	UINT aav_numFeatureLevels = 1; // base upon d3dFeatureLevel_required

	//code
	//GPU details
	aav_hr = gpuDeteail();
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "gpuDeteail() failed.\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "gpuDeteail() Success.\n");
		fclose(aav_gpFile);
	}

	aav_numDriverTypes = sizeof(aav_d3dDriverTypes) / sizeof(aav_d3dDriverTypes[0]); //calculating size of array

	ZeroMemory((void*)&aav_dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	
	aav_dxgiSwapChainDesc.BufferCount = 1;

	aav_dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	aav_dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	aav_dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	aav_dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; //Refresh rate
	aav_dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

	aav_dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; //enum
	aav_dxgiSwapChainDesc.OutputWindow = aav_ghwnd;
	aav_dxgiSwapChainDesc.SampleDesc.Count = 1;
	aav_dxgiSwapChainDesc.SampleDesc.Quality = 0;
	aav_dxgiSwapChainDesc.Windowed = TRUE; // maze default window dakhav

	for (UINT driverTypeIndex = 0; driverTypeIndex < aav_numDriverTypes; driverTypeIndex++)
	{
		aav_d3dDriverType = aav_d3dDriverTypes[driverTypeIndex];
		aav_hr = D3D11CreateDeviceAndSwapChain(
			NULL,							// 1.Adapter
			aav_d3dDriverType,				// 2.Driver type
			NULL,							// 3.Software
			aav_createDeviceFlags,			// 4.Flags
			&aav_d3dFeatureLevel_required,	// 5.Feature Levels
			aav_numFeatureLevels,				// 6.Num Feature Levels
			D3D11_SDK_VERSION,				// 7.SDK Version
			&aav_dxgiSwapChainDesc,			// 8.Swap Chain Desc
			&aav_gpIDXGISwapChain,			// 9.Swap Chain
			&aav_gpID3D11Device,			// 10.Device
			&aav_d3dFeatureLevel_acquired,	// 11.Feature Level
			&aav_gpID3D11DeviceContext		// 12.Device Context
		);	
		
		if (SUCCEEDED(aav_hr))
			break;
	}
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "D3D11CreateDeviceAndSwapChain() failed.\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");
		fprintf_s(aav_gpFile, "The Chosen Driver Is of ");

		if (aav_d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(aav_gpFile, "Hardware Type.\n");
		}
		else if (aav_d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(aav_gpFile, "Warp Type.\n");
		}
		else if (aav_d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(aav_gpFile, "Reference Type.\n");
		}
		else
		{
			fprintf_s(aav_gpFile, "Unknown Type.\n");
		}

		fprintf_s(aav_gpFile, "The Supported Hightest Feature Level Is");
		if (aav_d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(aav_gpFile, "11.0\n");
		}
		else if (aav_d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(aav_gpFile, "10.1.\n");
		}
		else if (aav_d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(aav_gpFile, "10.0.\n");
		}
		else
		{
			fprintf_s(aav_gpFile, "Unknown.\n");
		}

		fclose(aav_gpFile);
	}

	//shader start from here

	//PerPixel  Vertex Shader

	//declare the vertex shader source code
	//In DirextX shader variable nahe , inbuilt consstruct ahe 
	// glsl  : hlsl : his
	//SV_POSITION : shader variable 
	const char* aav_vertexShaderSourceCode_PerPixel =
		"cbuffer ConstantBuffer"\
		"{"	\
		"float4x4 worldMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		"float4 la;"\
		"float4 ld;"\
		"float4 ls;"\
		"float4 ka;"\
		"float4 kd;"\
		"float4 ks;"\
		"float materialShineness;"\
		"float4 lightPosition;"\
		"uint keyPress;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float3 tranformed_normal:NORMAL0;"\
		"float3 light_direction:NORMAL1;"\
		"float3 viwer_vector:NORMAL2;"\
		"};"\
		"vertex_output main(float4 pos : POSITION, float4 normal:NORMAL)"\
		"{"	\
		"	vertex_output output;"\
		
		"		float4 iPosition = mul(worldMatrix, pos); "\
		"		iPosition = mul(viewMatrix, iPosition); "\
		"		output.tranformed_normal = mul((float3x3)worldMatrix,(float3)normal);"\
		"		output.light_direction = (float3)lightPosition - iPosition.xyz;"\
		"		output.viwer_vector =-iPosition.xyz;"\
	
		"	float4 position = mul(worldMatrix,pos);"\
		"	position = mul(viewMatrix,position);"\
		"	position = mul(projectionMatrix, position);"\
		"	output.position = position;"\
		"	return(output);"\
		"}";

	//Blob is any data stroe in variable
	ID3DBlob *aav_pID3DBlob_VertexShaderCode_PerPixel = NULL;
	ID3DBlob *aav_pID3DBlob_Error_PerPixel = NULL;

	aav_hr = D3DCompile(
		aav_vertexShaderSourceCode_PerPixel,				//1. Shader Code
		lstrlenA(aav_vertexShaderSourceCode_PerPixel)+ 1,	//2. Shader Length
		"VS",												//3. Vertex Shader
		NULL,												//4. MACRO array for shader 
		D3D_COMPILE_STANDARD_FILE_INCLUDE,					//5. use shandard variable
		"main",												//6. Entry Point fucntion vertex shader
		"vs_5_0",											//7. My Vertex shader feature level
		0,													//8. Shader compile 
		0,													//9. compiler flag any for Effect
		&aav_pID3DBlob_VertexShaderCode_PerPixel,			//10. No Error this variable is Fulled 
		&aav_pID3DBlob_Error_PerPixel						//11. if Error is there then it is Fulled 
		);

	if (FAILED(aav_hr))
	{
		if (aav_pID3DBlob_Error_PerPixel != NULL)
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "%s", (char*)aav_pID3DBlob_Error_PerPixel->GetBufferPointer());
			fclose(aav_gpFile);
			aav_pID3DBlob_Error_PerPixel->Release();
			aav_pID3DBlob_Error_PerPixel = NULL;
			return(aav_hr);
			//set are device context varti call karavi lagtat
		}
		else
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "%s", (char*)aav_pID3DBlob_Error_PerPixel->GetBufferPointer());
			fclose(aav_gpFile);
			aav_pID3DBlob_Error_PerPixel->Release();
			aav_pID3DBlob_Error_PerPixel = NULL;

			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "D3DCompile() Succeeded For Vertex Shader PerPixel .\n");
			fclose(aav_gpFile);
		}
	}

	aav_hr = aav_gpID3D11Device->CreateVertexShader(aav_pID3DBlob_VertexShaderCode_PerPixel->GetBufferPointer(), // gpu understandable code de
		aav_pID3DBlob_VertexShaderCode_PerPixel->GetBufferSize(),
		NULL,	//ID3D11instance*(pointer) : ID3Dd11ClassLinkage*(pointer)
		&aav_gpID3D11VertexShader_PerPixel); // vertex shader kashat barun dau

	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "gpID3D11Device::CreateVertexShader PerPixel Failed");
		fclose(aav_gpFile);
		return(aav_hr);
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "gpID3D11Device::CreateVertexShader PerPixel Succes\n");
		fclose(aav_gpFile);
	}

	
	//Pixel Shader
	const char* aav_pixelShaderSourceCode_PerPixel =
		"cbuffer ConstantBuffer"\
		"{"	\
		"float4x4 worldMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		"float4 la;"\
		"float4 ld;"\
		"float4 ls;"\
		"float4 ka;"\
		"float4 kd;"\
		"float4 ks;"\
		"float materialShineness;"\
		"float4 lightPosition;"\
		"uint keyPress;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float3 tranformed_normal:NORMAL0;"\
		"float3 light_direction:NORMAL1;"\
		"float3 viwer_vector:NORMAL2;"\
		"};"\
		"float4 main(struct vertex_output input) : SV_TARGET"\
		"{"	\
		"	float3 phong_ads_light;"\
		"	float3 normalize_tranformed_normal = normalize(input.tranformed_normal);"\
		"	float3 normalize_light_direction = normalize(input.light_direction);"\
		"	float3 normalize_view_vector = normalize(input.viwer_vector);"\
		"	float3 ambiant = (float3)(la * ka);"\
		"	float3 diffuse = (float3)(ld * kd) * max(dot(normalize_light_direction,normalize_tranformed_normal),0.0);"\
		"	float3 reflection_vector = reflect(-normalize_light_direction,normalize_tranformed_normal);"\
		"	float3 specular = (float3)ls * ks * pow(max(dot(reflection_vector,normalize_view_vector),0.0),materialShineness);"\
		"	phong_ads_light = ambiant+ diffuse + specular;"\
		"	float4 color = float4((phong_ads_light),1.0f);"\
		"	return(color);"	\
		"}";

	ID3DBlob *aav_pID3DBlob_PixelShaderCode_PerPixel = NULL;
	aav_pID3DBlob_Error_PerPixel = NULL;
	aav_hr = D3DCompile(
		aav_pixelShaderSourceCode_PerPixel,
		lstrlenA(aav_pixelShaderSourceCode_PerPixel)+1,
			"PS",
			NULL,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			"main",
			"ps_5_0",
			0,
			0,
			&aav_pID3DBlob_PixelShaderCode_PerPixel,
			&aav_pID3DBlob_Error_PerPixel
		);

	if (FAILED(aav_hr))
	{
		if (aav_pID3DBlob_Error_PerPixel != NULL)
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "%s", (char*)aav_pID3DBlob_Error_PerPixel->GetBufferPointer());
			fclose(aav_gpFile);
			aav_pID3DBlob_Error_PerPixel->Release();
			aav_pID3DBlob_Error_PerPixel = NULL;
			return(aav_hr);
		}
		else
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "%s", (char*)aav_pID3DBlob_Error_PerPixel->GetBufferPointer());
			fclose(aav_gpFile);
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "COM Error.\n");
			fclose(aav_gpFile);
			return(aav_hr);
		}
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "D3DCompile() Succeeded For Pixel Shader PerPixel .\n");
		fclose(aav_gpFile);
	}

	aav_hr = aav_gpID3D11Device->CreatePixelShader(aav_pID3DBlob_PixelShaderCode_PerPixel->GetBufferPointer(),
		aav_pID3DBlob_PixelShaderCode_PerPixel->GetBufferSize(), NULL, &aav_gpID3D11PixelShader_PerPixel);

	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreatePixelShader() Failed PerPixel");
		fclose(aav_gpFile);
		return(aav_hr);

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreatePixelShader() Succeeded PerPixel\n");
		fclose(aav_gpFile);
	}

	if (aav_pID3DBlob_PixelShaderCode_PerPixel)
	{
		aav_pID3DBlob_PixelShaderCode_PerPixel->Release();
		aav_pID3DBlob_PixelShaderCode_PerPixel = NULL;
	}

	if (aav_pID3DBlob_Error_PerPixel)
	{
		aav_pID3DBlob_Error_PerPixel->Release();
		aav_pID3DBlob_Error_PerPixel = NULL;
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	//Vertex Shader PreVertetx lighting
	const char* aav_vertexShaderSourceCode_PerVertex =
		"cbuffer ConstantBuffer"\
		"{"	\
		"float4x4 worldMatrix;"\
		"float4x4 viewMatrix;"\
		"float4x4 projectionMatrix;"\
		"float4 la;"\
		"float4 ld;"\
		"float4 ls;"\
		"float4 ka;"\
		"float4 kd;"\
		"float4 ks;"\
		"float materialShineNess;"\
		"float4 lightPosition;"\
		"uint keyPress;"\
		"}"\
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float3 phong_ads_light:COLOR;"\
		"};"\
		"vertex_output main(float4 pos : POSITION, float4 normal:NORMAL)"\
		"{"	\
		"	vertex_output output;"\
		"		float4 iPosition = mul(worldMatrix, pos); "\
		"		iPosition = mul(viewMatrix, iPosition); "\
		"		float3 tranformed_normal = (float3)normalize(mul((float3x3)worldMatrix,(float3)normal));"\
		"		float3 light_direction = (float3)normalize(lightPosition - iPosition);"\
		"		float3 ambiant = la * ka ;"\
		"		float3 diffuse = ld * kd * max(dot(light_direction,tranformed_normal),0.0);"\
		"		float3 reflection_vector = reflect(-light_direction,tranformed_normal);"\
		"		float3 viwer_Vector = normalize(-iPosition.xyz);"\
		"		float3 specular = ls * ks * pow(max(dot(reflection_vector, viwer_Vector),0.0),materialShineNess);"\
		"		output.phong_ads_light = ambiant + diffuse+ specular;"\
		"	float4 position = mul(worldMatrix,pos);"\
		"	position = mul(viewMatrix,position);"\
		"	position = mul(projectionMatrix, position);"\
		"	output.position = position;"\
		"	return(output);"\
		"}";

	//Blob is any data stroe in variable
	ID3DBlob* aav_pID3DBlob_VertexShaderCode_PerVertex = NULL;
	ID3DBlob* aav_pID3DBlob_Error_PerVertex = NULL;

	aav_hr = D3DCompile(
		aav_vertexShaderSourceCode_PerVertex,				//1. Shader Code
		lstrlenA(aav_vertexShaderSourceCode_PerVertex) + 1,	//2. Shader Length
		"VS",												//3. Vertex Shader
		NULL,												//4. MACRO array for shader 
		D3D_COMPILE_STANDARD_FILE_INCLUDE,					//5. use shandard variable
		"main",												//6. Entry Point fucntion vertex shader
		"vs_5_0",											//7. My Vertex shader feature level
		0,													//8. Shader compile 
		0,													//9. compiler flag any for Effect
		&aav_pID3DBlob_VertexShaderCode_PerVertex,			//10. No Error this variable is Fulled 
		&aav_pID3DBlob_Error_PerVertex						//11. if Error is there then it is Fulled 
	);

	if (FAILED(aav_hr))
	{
		if (aav_pID3DBlob_Error_PerVertex != NULL)
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "%s", (char*)aav_pID3DBlob_Error_PerVertex->GetBufferPointer());
			fclose(aav_gpFile);
			aav_pID3DBlob_Error_PerVertex->Release();
			aav_pID3DBlob_Error_PerVertex = NULL;
			return(aav_hr);
			//set are device context varti call karavi lagtat
		}
		else
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "%s", (char*)aav_pID3DBlob_Error_PerVertex->GetBufferPointer());
			fclose(aav_gpFile);
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "D3DCompile() Succeeded For Vertex Shader PerVertex .\n");
			fclose(aav_gpFile);
		}
	}

	aav_hr = aav_gpID3D11Device->CreateVertexShader(aav_pID3DBlob_VertexShaderCode_PerVertex->GetBufferPointer(), // gpu understandable code de
		aav_pID3DBlob_VertexShaderCode_PerVertex->GetBufferSize(),
		NULL,	//ID3D11instance*(pointer) : ID3Dd11ClassLinkage*(pointer)
		&aav_gpID3D11VertexShader_PerVertex); // vertex shader kashat barun dau

	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "gpID3D11Device::CreateVertexShader Failed PerVertex");
		fclose(aav_gpFile);
		return(aav_hr);
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "gpID3D11Device::CreateVertexShader Succes PerVertex\n");
		fclose(aav_gpFile);
	}

	//aav_gpID3D11DeviceContext->VSSetShader(aav_gpID3D11VertexShader_PerVertex, NULL, 0);

	//Pixel Shader PerVertex
	const char* aav_pixelShaderSourceCode_PerVertex =
		"struct vertex_output"\
		"{"\
		"float4 position:SV_POSITION;"\
		"float3 phong_ads_light:COLOR;"\
		"};"
		"float4 main(struct vertex_output input) : SV_TARGET"\
		"{"	\
		"	float4 color = float4((input.phong_ads_light),1.0f);"\
		"	return(color);"	\
		"}";

	ID3DBlob* aav_pID3DBlob_PixelShaderCode = NULL;
	aav_pID3DBlob_Error_PerVertex = NULL;
	aav_hr = D3DCompile(
		aav_pixelShaderSourceCode_PerVertex,
		lstrlenA(aav_pixelShaderSourceCode_PerVertex) + 1,
		"PS",
		NULL,
		D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main",
		"ps_5_0",
		0,
		0,
		&aav_pID3DBlob_PixelShaderCode,
		&aav_pID3DBlob_Error_PerVertex
	);

	if (FAILED(aav_hr))
	{
		if (aav_pID3DBlob_Error_PerVertex != NULL)
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "%s", (char*)aav_pID3DBlob_Error_PerVertex->GetBufferPointer());
			fclose(aav_gpFile);
			aav_pID3DBlob_Error_PerVertex->Release();
			aav_pID3DBlob_Error_PerVertex = NULL;
			return(aav_hr);
		}
		else
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "COM Error.\n");
			fclose(aav_gpFile);
			return(aav_hr);
		}
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "D3DCompile() Succeeded For Pixel Shader  PerVertex .\n");
		fclose(aav_gpFile);
	}

	aav_hr = aav_gpID3D11Device->CreatePixelShader(aav_pID3DBlob_PixelShaderCode->GetBufferPointer(),
		aav_pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &aav_gpID3D11PixelShader_PerVertex);

	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreatePixelShader() Failed PerVertex \n");
		fclose(aav_gpFile);
		return(aav_hr);

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreatePixelShader() Succeeded  PerVertex \n");
		fclose(aav_gpFile);
	}

	//aav_gpID3D11DeviceContext->PSSetShader(aav_gpID3D11PixelShader_PerVertex, NULL, 0);
	if (aav_pID3DBlob_PixelShaderCode)
	{
		aav_pID3DBlob_PixelShaderCode->Release();
		aav_pID3DBlob_PixelShaderCode = NULL;
	}

	if (aav_pID3DBlob_Error_PerVertex)
	{
		aav_pID3DBlob_Error_PerVertex->Release();
		aav_pID3DBlob_Error_PerVertex = NULL;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////


	//9
	//(a)initialize and ()create and set input lay out

	//(a) initailize intput lay out structure
	D3D11_INPUT_ELEMENT_DESC aav_d3d11inputElementDesc[2];
	
	//ZeroMemory((void*)&aav_d3d11inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC));

	aav_d3d11inputElementDesc[0].SemanticName			= "POSITION";					//vPosition glBindAttribute Location
	aav_d3d11inputElementDesc[0].SemanticIndex			= 0;							// if we are going to sent multiple geometry in same semantics they are seperate by indices
	aav_d3d11inputElementDesc[0].Format					= DXGI_FORMAT_R32G32B32_FLOAT;	//glVertexAttribPointer 3, GL_FLOAT forth parameter
	aav_d3d11inputElementDesc[0].AlignedByteOffset		= 0;
	aav_d3d11inputElementDesc[0].InputSlot				= 0;							// alpla enum ekadcha input slot
	aav_d3d11inputElementDesc[0].InputSlotClass			= D3D11_INPUT_PER_VERTEX_DATA;	// Me jo pathavto ahe to vertex cha data ahe. 
	aav_d3d11inputElementDesc[0].InstanceDataStepRate	= 0;							
	
	aav_d3d11inputElementDesc[1].SemanticName			= "NORMAL";					//vPosition glBindAttribute Location
	aav_d3d11inputElementDesc[1].SemanticIndex			= 0;							// if we are going to sent multiple geometry in same semantics they are seperate by indices
	aav_d3d11inputElementDesc[1].Format					= DXGI_FORMAT_R32G32B32_FLOAT;	//glVertexAttribPointer 3, GL_FLOAT forth parameter
	aav_d3d11inputElementDesc[1].AlignedByteOffset		= 0;
	aav_d3d11inputElementDesc[1].InputSlot				= 1;							// alpla enum ekadcha input slot
	aav_d3d11inputElementDesc[1].InputSlotClass			= D3D11_INPUT_PER_VERTEX_DATA;	// Me jo pathavto ahe to vertex cha data ahe. 
	aav_d3d11inputElementDesc[1].InstanceDataStepRate	= 0;

	//B create the input
	//InputLayout of PerPixel 
	aav_hr = aav_gpID3D11Device->CreateInputLayout(aav_d3d11inputElementDesc, _ARRAYSIZE(aav_d3d11inputElementDesc),
								aav_pID3DBlob_VertexShaderCode_PerPixel->GetBufferPointer(), 
								aav_pID3DBlob_VertexShaderCode_PerPixel->GetBufferSize(),
								&aav_gpID3D11InputLayout_PerPixel);

	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "CreateInputLayout Failed");
		fclose(aav_gpFile);
		return(aav_hr);
		if (aav_pID3DBlob_VertexShaderCode_PerPixel)
		{
			aav_pID3DBlob_VertexShaderCode_PerPixel->Release();
			aav_pID3DBlob_VertexShaderCode_PerPixel = NULL;
		}
	}
	else
	{
		if (aav_pID3DBlob_VertexShaderCode_PerPixel)
		{
			aav_pID3DBlob_VertexShaderCode_PerPixel->Release();
			aav_pID3DBlob_VertexShaderCode_PerPixel = NULL;
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "ID3D11Device::CreateInputLayout() Succeeded  PerPixel \n");
			fclose(aav_gpFile);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////
	//InputLayout for PerVertex Shader
	aav_hr = aav_gpID3D11Device->CreateInputLayout(aav_d3d11inputElementDesc, _ARRAYSIZE(aav_d3d11inputElementDesc),
		aav_pID3DBlob_VertexShaderCode_PerVertex->GetBufferPointer(),
		aav_pID3DBlob_VertexShaderCode_PerVertex->GetBufferSize(),
		&aav_gpID3D11InputLayout_PerVertex);

	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "CreateInputLayout Failed");
		fclose(aav_gpFile);
		return(aav_hr);
		if (aav_pID3DBlob_VertexShaderCode_PerVertex)
		{
			aav_pID3DBlob_VertexShaderCode_PerVertex->Release();
			aav_pID3DBlob_VertexShaderCode_PerVertex = NULL;
		}
	}
	else
	{
		if (aav_pID3DBlob_VertexShaderCode_PerPixel)
		{
			aav_pID3DBlob_VertexShaderCode_PerVertex->Release();
			aav_pID3DBlob_VertexShaderCode_PerVertex = NULL;
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "ID3D11Device::CreateInputLayout() Succeeded  PerVertex \n");
			fclose(aav_gpFile);
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	//declate vertices , color , texture , normals
	//Dirext3D is Clock Wise
	
	getSphereVertexData(aav_sphere_vertices, aav_sphere_normals, aav_sphere_texture, aav_sphere_elements);

	aav_numSphereVertices = getNumberOfSphereVertices();
	aav_numSphereElements = getNumberOfSphereElements();

	//direct fabers dymanic draw
	//11
	//create Vertex buffer
	//initialize buffer disctiption structure  and Create the buffer
	//12
	//push the data in this buffer by MaP(memcpy(&UnmapMethod))

	//11
	D3D11_BUFFER_DESC aav_d3d11BufferDesk;

	//Rectangle Vertex
	ZeroMemory((void*)&aav_d3d11BufferDesk, sizeof(D3D11_BUFFER_DESC));
	aav_d3d11BufferDesk.ByteWidth = aav_numSphereVertices * 3 * sizeof(float); //glBufferData cha ha 2nd parameter
	aav_d3d11BufferDesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;			// bind Attribute loacation la support , use this as vertex buffer
	aav_d3d11BufferDesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	aav_d3d11BufferDesk.Usage = D3D11_USAGE_DYNAMIC;

	aav_hr = aav_gpID3D11Device->CreateBuffer(&aav_d3d11BufferDesk, NULL,
		&aav_gpID3D11Buffer_VertexBuffer_Position);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreateBuffer()  Vertex buffer Failed");
		fclose(aav_gpFile);
		return(aav_hr);
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreateBuffer() Vertex buffer Succeeded.\n");
		fclose(aav_gpFile);
	}

	//aav_normalCube
	ZeroMemory((void*)&aav_d3d11BufferDesk, sizeof(D3D11_BUFFER_DESC));
	aav_d3d11BufferDesk.ByteWidth = aav_numSphereVertices * 3 * sizeof(float); //glBufferData cha ha 2nd parameter
	aav_d3d11BufferDesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;					// bind Attribute loacation la support , use this as vertex buffer
	aav_d3d11BufferDesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	aav_d3d11BufferDesk.Usage = D3D11_USAGE_DYNAMIC;

	aav_hr = aav_gpID3D11Device->CreateBuffer(&aav_d3d11BufferDesk, NULL,
		&aav_gpID3D11Buffer_VertexBuffer_Normal);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreateBuffer()  Color buffer Failed");
		fclose(aav_gpFile);
		return(aav_hr);
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreateBuffer() Color buffer Succeeded.\n");
		fclose(aav_gpFile);
	}

	//Triangle Vertex Mapping
	D3D11_MAPPED_SUBRESOURCE aav_d3d11MappedSubresource;
	
	////////////////////////////////////////////////
	//Rectangle Vertex Mapping
	ZeroMemory((void*)&aav_d3d11MappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	aav_gpID3D11DeviceContext->Map(aav_gpID3D11Buffer_VertexBuffer_Position, NULL,
		D3D11_MAP_WRITE_DISCARD, NULL, &aav_d3d11MappedSubresource);

	memcpy(aav_d3d11MappedSubresource.pData, aav_sphere_vertices, aav_numSphereVertices * 3 * sizeof(float));
	aav_gpID3D11DeviceContext->Unmap(aav_gpID3D11Buffer_VertexBuffer_Position, NULL);

	//Rectangle colors Mapping
	ZeroMemory((void*)&aav_d3d11MappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	aav_gpID3D11DeviceContext->Map(aav_gpID3D11Buffer_VertexBuffer_Normal, NULL,
		D3D11_MAP_WRITE_DISCARD, NULL, &aav_d3d11MappedSubresource);

	memcpy(aav_d3d11MappedSubresource.pData, aav_sphere_normals, aav_numSphereVertices * 3 * sizeof(float));
	aav_gpID3D11DeviceContext->Unmap(aav_gpID3D11Buffer_VertexBuffer_Normal, NULL);


	//Index
	ZeroMemory((void*)&aav_d3d11BufferDesk, sizeof(D3D11_BUFFER_DESC));
	aav_d3d11BufferDesk.ByteWidth = aav_numSphereElements * sizeof(short); //glBufferData cha ha 2nd parameter
	aav_d3d11BufferDesk.BindFlags = D3D11_BIND_INDEX_BUFFER;			// bind Attribute loacation la support , use this as vertex buffer
	aav_d3d11BufferDesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	aav_d3d11BufferDesk.Usage = D3D11_USAGE_DYNAMIC;

	aav_hr = aav_gpID3D11Device->CreateBuffer(&aav_d3d11BufferDesk, NULL,
		&aav_gpID3D11Buffer_IndexBuffer);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreateBuffer()  IndexBuffer buffer Failed");
		fclose(aav_gpFile);
		return(aav_hr);
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreateBuffer() IndexBuffer buffer Succeeded.\n");
		fclose(aav_gpFile);
	}

	ZeroMemory((void*)&aav_d3d11MappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	aav_gpID3D11DeviceContext->Map(aav_gpID3D11Buffer_IndexBuffer, NULL,
		D3D11_MAP_WRITE_DISCARD, NULL, &aav_d3d11MappedSubresource);

	memcpy(aav_d3d11MappedSubresource.pData, aav_sphere_elements, aav_numSphereElements * sizeof(short));
	aav_gpID3D11DeviceContext->Unmap(aav_gpID3D11Buffer_IndexBuffer, NULL);


	//here we don't the buffer in pipe line we to set it in Draw

	//create anothrer 
	//set this "constatnt buffer" into the pipe line
	ZeroMemory((void*)&aav_d3d11BufferDesk, sizeof(D3D11_BUFFER_DESC));
	aav_d3d11BufferDesk.ByteWidth = sizeof(CBUFFER);
	aav_d3d11BufferDesk.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	aav_d3d11BufferDesk.CPUAccessFlags = 0;
	aav_d3d11BufferDesk.Usage = D3D11_USAGE_DEFAULT;

	aav_hr = aav_gpID3D11Device->CreateBuffer(&aav_d3d11BufferDesk, NULL,
		&aav_gpID3D11Buffer_ConstatntBuffer_PerPixel);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreateBuffer()  constatnt buffer  Failed PerPixel");
		fclose(aav_gpFile);
		return(aav_hr);
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreateBuffer() constatnt buffer Succeeded PerPixel.\n");
		fclose(aav_gpFile);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	//"Constant Buffer" PerVertex Shader lighting.
	ZeroMemory((void*)&aav_d3d11BufferDesk, sizeof(D3D11_BUFFER_DESC));
	aav_d3d11BufferDesk.ByteWidth = sizeof(CBUFFER);
	aav_d3d11BufferDesk.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	aav_d3d11BufferDesk.CPUAccessFlags = 0;
	aav_d3d11BufferDesk.Usage = D3D11_USAGE_DEFAULT;

	aav_hr = aav_gpID3D11Device->CreateBuffer(&aav_d3d11BufferDesk, NULL,
		&aav_gpID3D11Buffer_ConstatntBuffer_PerVertex);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreateBuffer()  constatnt buffer  Failed PerVertex ");
		fclose(aav_gpFile);
		return(aav_hr);
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreateBuffer() constatnt buffer Succeeded PerVertex.\n");
		fclose(aav_gpFile);
	}

	//////////////////////////////////////////////////////////////////////////////////////////////////


	//Create and set Rasterizer state
	D3D11_RASTERIZER_DESC aav_d3d11RaseterizerDesk;
	ZeroMemory((void*)&aav_d3d11RaseterizerDesk, sizeof(D3D11_RASTERIZER_DESC));

	aav_d3d11RaseterizerDesk.AntialiasedLineEnable = FALSE;
	aav_d3d11RaseterizerDesk.CullMode = D3D11_CULL_NONE;
	aav_d3d11RaseterizerDesk.DepthBias = 0;
	aav_d3d11RaseterizerDesk.DepthBiasClamp = 0.0f;
	aav_d3d11RaseterizerDesk.DepthClipEnable = TRUE;
	aav_d3d11RaseterizerDesk.FillMode = D3D11_FILL_SOLID; // Wire Frame we give here
	aav_d3d11RaseterizerDesk.FrontCounterClockwise = FALSE;
	aav_d3d11RaseterizerDesk.MultisampleEnable = FALSE;
	aav_d3d11RaseterizerDesk.ScissorEnable = FALSE;
	aav_d3d11RaseterizerDesk.SlopeScaledDepthBias = 0.0f;

	aav_hr = aav_gpID3D11Device->CreateRasterizerState(&aav_d3d11RaseterizerDesk, &aav_gpId3d11RasterizerState);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "CreateRasterizerState() failed .Exitting Now...\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "CreateRasterizerState() Success...\n");
		fclose(aav_gpFile);
	}

	aav_gpID3D11DeviceContext->RSSetState(aav_gpId3d11RasterizerState);

	//d3d clear aav_colorPyramid (blue)
	aav_gClearColor[0] = 0.0f;
	aav_gClearColor[1] = 0.0f;
	aav_gClearColor[2] = 0.0f;
	aav_gClearColor[3] = 1.0f;

	//set projection matrix to identity matrix
	aav_gPrespectiveProjectionMatrix = XMMatrixIdentity();

	//call resize for first time
	aav_hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "resize() failed .Exitting Now...\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "resize() Success...\n");
		fclose(aav_gpFile);
	}
	return(S_OK);
}

HRESULT gpuDeteail(void)
{
	//fucntion declaration
	void uninitialize(void);

	//variable declaraiotn
	DXGI_ADAPTER_DESC aav_dxgiAdapterDesc;

	HRESULT aav_hr;

	char aav_str[255];

	//code
	aav_hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&aav_pIDXGIFactory);
	//
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "CreateDXGIFactory Failde\n");
		fclose(aav_gpFile);
		uninitialize();
	}

	if (aav_pIDXGIFactory->EnumAdapters(0, &aav_pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{
		
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "DXGIAdapter Can Not be Found\n");
		fclose(aav_gpFile);
		uninitialize();
	}

	ZeroMemory((void*)&aav_dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));

	aav_hr = aav_pIDXGIAdapter->GetDesc(&aav_dxgiAdapterDesc);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "DXGIADAPTERDESC Failde\n");
		fclose(aav_gpFile);
		uninitialize();
	}

	WideCharToMultiByte(CP_ACP, 0, aav_dxgiAdapterDesc.Description, 255, aav_str, 255, NULL, NULL);
	

	fopen_s(&aav_gpFile, "log.txt", "a+");
	fprintf(aav_gpFile, "Graphic Card Name = %s.\n", aav_str);

	fprintf(aav_gpFile, "Graphic Memory VRAM = %I64d bytes.\n", 
						(__int64)aav_dxgiAdapterDesc.DedicatedVideoMemory);

	fprintf(aav_gpFile, "VRAM in GB = %d GB\n", 
						int(ceil(aav_dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0)));
	fclose(aav_gpFile);

	return(aav_hr);
}

void display(void)
{
	//Steps 
	//14 :Set vertex buffer to pipeline 
	//15 :Set premitive 
	//16 : Do tranformation as needed
	//17 : push transformation in shader
	//18 : Draw

	//vatiable Initialization.
	
	XMMATRIX aav_scaleMatrix = XMMatrixIdentity();
	XMMATRIX aav_translation = XMMatrixIdentity();
	XMMATRIX aav_rotation = XMMatrixIdentity();
	XMMATRIX aav_worldMatrix = XMMatrixIdentity();
	XMMATRIX aav_viewMatrix = XMMatrixIdentity();
	XMMATRIX aav_wvMatrix = XMMatrixIdentity();
	//Set  Vertex buffer to pipline 
	UINT aav_stride = sizeof(float) * 3;
	UINT aav_offset = 0;

	CBUFFER aav_constantBuffer_PerPixel;
	CBUFFER aav_constantBuffer_PerVertex;
	// code

	//clear render target view to a chose aav_colorPyramid
	aav_gpID3D11DeviceContext->ClearRenderTargetView(aav_gpID3D11RenderTargetView, aav_gClearColor);

	aav_gpID3D11DeviceContext->ClearDepthStencilView(aav_gpID3D11DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	if (aav_bPerVertex == TRUE)
	{
		aav_gpID3D11DeviceContext->VSSetShader(aav_gpID3D11VertexShader_PerVertex, NULL, 0);
		aav_gpID3D11DeviceContext->PSSetShader(aav_gpID3D11PixelShader_PerVertex, NULL, 0);
		aav_gpID3D11DeviceContext->IASetInputLayout(aav_gpID3D11InputLayout_PerVertex);
		aav_gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &aav_gpID3D11Buffer_ConstatntBuffer_PerVertex);

	}
	if (aav_bPerPixel == TRUE)
	{
		aav_gpID3D11DeviceContext->VSSetShader(aav_gpID3D11VertexShader_PerPixel, NULL, 0);
		aav_gpID3D11DeviceContext->PSSetShader(aav_gpID3D11PixelShader_PerPixel, NULL, 0);
		aav_gpID3D11DeviceContext->IASetInputLayout(aav_gpID3D11InputLayout_PerPixel);
		aav_gpID3D11DeviceContext->VSSetConstantBuffers(0, 1, &aav_gpID3D11Buffer_ConstatntBuffer_PerPixel);
		aav_gpID3D11DeviceContext->PSSetConstantBuffers(0, 1, &aav_gpID3D11Buffer_ConstatntBuffer_PerPixel);
	}

	aav_stride = sizeof(float) * 3;
	aav_offset = 0;
	aav_gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &aav_gpID3D11Buffer_VertexBuffer_Position, &aav_stride, &aav_offset);


	aav_stride = sizeof(float) * 3;
	aav_offset = 0;
	aav_gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &aav_gpID3D11Buffer_VertexBuffer_Normal, &aav_stride, &aav_offset);

	aav_gpID3D11DeviceContext->IASetIndexBuffer(aav_gpID3D11Buffer_IndexBuffer,DXGI_FORMAT_R16_UINT,0);

	aav_gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	aav_translation = XMMatrixTranslation(0.0f, 0.0f, 1.5f);
	
	aav_worldMatrix =  aav_translation;
	aav_viewMatrix = XMMatrixIdentity();

	
	if (aav_bPerVertex == TRUE)
	{
		ZeroMemory((void*)&aav_constantBuffer_PerVertex, sizeof(CBUFFER));
		aav_constantBuffer_PerVertex.WorldMatrix = aav_worldMatrix;
		aav_constantBuffer_PerVertex.ViewMatrix = aav_viewMatrix;
		aav_constantBuffer_PerVertex.ProjectionMatrix = aav_gPrespectiveProjectionMatrix;


		aav_constantBuffer_PerVertex.La = XMVectorSet(aav_lightAmbiant[0], aav_lightAmbiant[1], aav_lightAmbiant[2], aav_lightAmbiant[3]);
		aav_constantBuffer_PerVertex.Ld = XMVectorSet(aav_lightDiffuse[0], aav_lightDiffuse[1], aav_lightDiffuse[2], aav_lightDiffuse[3]);
		aav_constantBuffer_PerVertex.Ls = XMVectorSet(aav_lightSpecular[0], aav_lightSpecular[1], aav_lightSpecular[2], aav_lightSpecular[3]);
		aav_constantBuffer_PerVertex.LightPosition = XMVectorSet(aav_lightPosition[0], aav_lightPosition[1], aav_lightPosition[2], aav_lightPosition[3]);

		aav_constantBuffer_PerVertex.Ka = XMVectorSet(aav_materialAmbiant[0], aav_materialAmbiant[1], aav_materialAmbiant[2], aav_materialAmbiant[3]);
		aav_constantBuffer_PerVertex.Kd = XMVectorSet(aav_materialDiffuse[0], aav_materialDiffuse[1], aav_materialDiffuse[2], aav_materialDiffuse[3]);
		aav_constantBuffer_PerVertex.Ks = XMVectorSet(aav_materialSpecular[0], aav_materialSpecular[1], aav_materialSpecular[2], aav_materialSpecular[3]);
		aav_constantBuffer_PerVertex.MaterialShineNess = aav_materialShineNess;

		aav_gpID3D11DeviceContext->UpdateSubresource(
			aav_gpID3D11Buffer_ConstatntBuffer_PerVertex,
			0,
			NULL,
			&aav_constantBuffer_PerVertex,				
			0,								//5.Bounding Box , Row Pitch
			0								//6.Bounding Box , Depth Pitch
		);
	}

	if (aav_bPerPixel == TRUE)
	{
		ZeroMemory((void*)&aav_constantBuffer_PerPixel, sizeof(CBUFFER));
		aav_constantBuffer_PerPixel.WorldMatrix = aav_worldMatrix;
		aav_constantBuffer_PerPixel.ViewMatrix = aav_viewMatrix;
		aav_constantBuffer_PerPixel.ProjectionMatrix = aav_gPrespectiveProjectionMatrix;


		aav_constantBuffer_PerPixel.La = XMVectorSet(aav_lightAmbiant[0], aav_lightAmbiant[1], aav_lightAmbiant[2], aav_lightAmbiant[3]);
		aav_constantBuffer_PerPixel.Ld = XMVectorSet(aav_lightDiffuse[0], aav_lightDiffuse[1], aav_lightDiffuse[2], aav_lightDiffuse[3]);
		aav_constantBuffer_PerPixel.Ls = XMVectorSet(aav_lightSpecular[0], aav_lightSpecular[1], aav_lightSpecular[2], aav_lightSpecular[3]);
		aav_constantBuffer_PerPixel.LightPosition = XMVectorSet(aav_lightPosition[0], aav_lightPosition[1], aav_lightPosition[2], aav_lightPosition[3]);

		aav_constantBuffer_PerPixel.Ka = XMVectorSet(aav_materialAmbiant[0], aav_materialAmbiant[1], aav_materialAmbiant[2], aav_materialAmbiant[3]);
		aav_constantBuffer_PerPixel.Kd = XMVectorSet(aav_materialDiffuse[0], aav_materialDiffuse[1], aav_materialDiffuse[2], aav_materialDiffuse[3]);
		aav_constantBuffer_PerPixel.Ks = XMVectorSet(aav_materialSpecular[0], aav_materialSpecular[1], aav_materialSpecular[2], aav_materialSpecular[3]);
		aav_constantBuffer_PerPixel.MaterialShineNess = aav_materialShineNess;

		aav_gpID3D11DeviceContext->UpdateSubresource(
			aav_gpID3D11Buffer_ConstatntBuffer_PerPixel,
			0,
			NULL,
			&aav_constantBuffer_PerPixel,				
			0,								//5.Bounding Box , Row Pitch
			0								//6.Bounding Box , Depth Pitch
		);

	}

	aav_gpID3D11DeviceContext->DrawIndexed(aav_numSphereElements,0,			0);
															//   /\			/\	
															//  Start      Kontya Vertex Buffer madhe Baghu
	
	//switch between front and back buffers
	aav_gpIDXGISwapChain->Present(0, 0);
}

HRESULT resize(int aav_width, int aav_height)
{
	//fucntion declaration
	void uninitialize(void);

	//varaible 
	HRESULT aav_hr = S_OK;
	ID3D11Texture2D* aav_pID3D11Texture2D_BackBuffer;
	D3D11_VIEWPORT aav_d3dViewPort;
	
	//code
	//relse of previos depth Stencil View
	if (aav_gpID3D11DepthStencilView)
	{
		aav_gpID3D11DepthStencilView->Release();
		aav_gpID3D11DepthStencilView = NULL;
	}

	//free any size-dependant resources
	if (aav_gpID3D11RenderTargetView)
	{
		aav_gpID3D11RenderTargetView->Release();
		aav_gpID3D11RenderTargetView = NULL;
	}

	//resize swap chain buffer accordingly
	aav_gpIDXGISwapChain->ResizeBuffers(1, aav_width, aav_height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	//agin get back buffer from swap chain
	aav_gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&aav_pID3D11Texture2D_BackBuffer);

	//again get render target view from d3d11 device using device above back buffer
	aav_hr = aav_gpID3D11Device->CreateRenderTargetView(aav_pID3D11Texture2D_BackBuffer, NULL, &aav_gpID3D11RenderTargetView);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "ID3D11Device::CreateRenderTargetView() failed .Exitting Now...\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "ID3D11Device::CreateRenderTargetView() Success...\n");
		fclose(aav_gpFile);
	}

	aav_pID3D11Texture2D_BackBuffer->Release();
	aav_pID3D11Texture2D_BackBuffer = NULL;

	//Depth Code Should be after RTV and before OMSetRenderTargets
	//Depth Buffer Code , Depth Code
	//ASV also need Texture buffer
	D3D11_TEXTURE2D_DESC d3d11Texture2DDesc;
	ZeroMemory((void*)&d3d11Texture2DDesc, sizeof(D3D11_TEXTURE2D_DESC));

	d3d11Texture2DDesc.Width = (UINT)aav_width;
	d3d11Texture2DDesc.Height = (UINT)aav_height;
	d3d11Texture2DDesc.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11Texture2DDesc.Usage = D3D11_USAGE_DEFAULT;
	d3d11Texture2DDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	d3d11Texture2DDesc.SampleDesc.Count = 1; //This can be 1to 4
	d3d11Texture2DDesc.SampleDesc.Quality = 0; //Default quality
	d3d11Texture2DDesc.ArraySize = 1;
	d3d11Texture2DDesc.MipLevels = 1;//MipLevels in OpenGL we Give 0.
	d3d11Texture2DDesc.CPUAccessFlags = 0;
	d3d11Texture2DDesc.MiscFlags = 0;

	ID3D11Texture2D* pID3D11Texture2D_DepthBuffer = NULL;

	//SubResource Data
	//		V
	aav_hr = aav_gpID3D11Device->CreateTexture2D(&d3d11Texture2DDesc, NULL, &pID3D11Texture2D_DepthBuffer);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "ID3D11Device::CreateTexture2D() failed .Exitting Now...\n");
		fclose(aav_gpFile);

		uninitialize();

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "ID3D11Device::CreateTexture2D() Success...\n");
		fclose(aav_gpFile);
	}


	D3D11_DEPTH_STENCIL_VIEW_DESC d3d11DepthStencilViewDesk;
	ZeroMemory((void*)&d3d11DepthStencilViewDesk, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));
	d3d11DepthStencilViewDesk.Format = DXGI_FORMAT_D32_FLOAT;
	d3d11DepthStencilViewDesk.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;//MS - multi-sampling

	aav_hr = aav_gpID3D11Device->CreateDepthStencilView(pID3D11Texture2D_DepthBuffer, &d3d11DepthStencilViewDesk, &aav_gpID3D11DepthStencilView);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "ID3D11Device::CreateDepthStencilView() failed .Exitting Now...\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "ID3D11Device::CreateDepthStencilView() Success...\n");
		fclose(aav_gpFile);
	}
	//DethCode End

	//sen render to target view as render target
	aav_gpID3D11DeviceContext->OMSetRenderTargets(1, &aav_gpID3D11RenderTargetView, aav_gpID3D11DepthStencilView);

	//Set View Port 
	ZeroMemory((void*)&aav_d3dViewPort, sizeof(D3D11_VIEWPORT));
	aav_d3dViewPort.TopLeftX = 0;
	aav_d3dViewPort.TopLeftY = 0;
	aav_d3dViewPort.Width = (float)aav_width;
	aav_d3dViewPort.Height = (float)aav_height;
	aav_d3dViewPort.MinDepth = 0.0f;
	aav_d3dViewPort.MaxDepth = 1.0f; //clear depth
	aav_gpID3D11DeviceContext->RSSetViewports(1, &aav_d3dViewPort);//raterization state

	//set othographic matrix
	aav_gPrespectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f),(float)aav_width/(float)aav_height,0.1f,100.0f);
	
	return(aav_hr);
}

void uninitialize(void)
{
	//code
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (aav_gpID3D11Buffer_ConstatntBuffer_PerVertex)
	{
		aav_gpID3D11Buffer_ConstatntBuffer_PerVertex->Release();
		aav_gpID3D11Buffer_ConstatntBuffer_PerVertex = NULL;
	}

	if (aav_gpID3D11InputLayout_PerVertex)
	{
		aav_gpID3D11InputLayout_PerVertex->Release();
		aav_gpID3D11InputLayout_PerVertex = NULL;
	}


	if (aav_gpID3D11Buffer_ConstatntBuffer_PerPixel)
	{
		aav_gpID3D11Buffer_ConstatntBuffer_PerPixel->Release();
		aav_gpID3D11Buffer_ConstatntBuffer_PerPixel = NULL;
	}

	if (aav_gpID3D11InputLayout_PerPixel)
	{
		aav_gpID3D11InputLayout_PerPixel->Release();
		aav_gpID3D11InputLayout_PerPixel = NULL;
	}

	if (aav_gpID3D11Buffer_IndexBuffer)
	{
		aav_gpID3D11Buffer_IndexBuffer->Release();
		aav_gpID3D11Buffer_IndexBuffer = NULL;
	}

	if (aav_gpID3D11Buffer_VertexBuffer_Normal)
	{
		aav_gpID3D11Buffer_VertexBuffer_Normal->Release();
		aav_gpID3D11Buffer_VertexBuffer_Normal = NULL;
	}

	if (aav_gpID3D11Buffer_VertexBuffer_Position)
	{
		aav_gpID3D11Buffer_VertexBuffer_Position->Release();
		aav_gpID3D11Buffer_VertexBuffer_Position = NULL;
	}

	if (aav_gpID3D11PixelShader_PerVertex)
	{
		aav_gpID3D11PixelShader_PerVertex->Release();
		aav_gpID3D11PixelShader_PerVertex = NULL;
	}

	if (aav_gpID3D11VertexShader_PerVertex)
	{
		aav_gpID3D11VertexShader_PerVertex->Release();
		aav_gpID3D11VertexShader_PerVertex = NULL;
	}

	if (aav_gpID3D11PixelShader_PerPixel)
	{
		aav_gpID3D11PixelShader_PerPixel->Release();
		aav_gpID3D11PixelShader_PerPixel = NULL;
	}

	if (aav_gpID3D11VertexShader_PerPixel)
	{
		aav_gpID3D11VertexShader_PerPixel->Release();
		aav_gpID3D11VertexShader_PerPixel = NULL;
	}


	if (aav_gpID3D11RenderTargetView)
	{
		aav_gpID3D11RenderTargetView->Release();
		aav_gpID3D11RenderTargetView = NULL;
	}

	if (aav_gpIDXGISwapChain)
	{
		aav_gpIDXGISwapChain->Release();
		aav_gpIDXGISwapChain = NULL;
	}

	if (aav_gpID3D11DeviceContext)
	{
		aav_gpID3D11DeviceContext->Release();
		aav_gpID3D11DeviceContext = NULL;
	}

	if (aav_gpID3D11Device)
	{
		aav_gpID3D11Device->Release();
		aav_gpID3D11Device = NULL;
	}

	if (aav_pIDXGIAdapter)
	{
		aav_pIDXGIAdapter->Release();
		aav_pIDXGIAdapter = NULL;
	}

	if (aav_pIDXGIFactory)
	{
		aav_pIDXGIFactory->Release();
		aav_pIDXGIFactory = NULL;
	}

	if (aav_gpFile)
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "uninitialize() Succeeded\n");
		fprintf_s(aav_gpFile, "Log File Is Successfully Closed.\n");
		fclose(aav_gpFile);
	}
}