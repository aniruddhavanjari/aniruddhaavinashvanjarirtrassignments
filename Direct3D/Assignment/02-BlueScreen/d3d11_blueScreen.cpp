#include <windows.h>
#include <stdio.h>
#include <math.h>

#include <d3d11.h>
#include "d3d11_blueScreen.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib, "dxgi.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND aav_ghwnd = NULL;
HDC aav_ghdc = NULL;
HGLRC aav_ghrc = NULL;

DWORD aav_dwStyle;
WINDOWPLACEMENT aav_wpPrev = { sizeof(WINDOWPLACEMENT) };

bool aav_gbActiveWindow = false;
bool aav_gbEscapeKeyIsPressed = false;
bool aav_gbFullscreen = false;

ID3D11DeviceContext* aav_gpID3D11DeviceContext = NULL;
ID3D11Device* aav_gpID3D11Device = NULL;
ID3D11RenderTargetView* aav_gpID3D11RenderTargetView = NULL;
IDXGISwapChain* aav_gpIDXGISwapChain = NULL;

float aav_gClearColor[4]; //RGB

IDXGIFactory* pIDXGIFactory = NULL;

IDXGIAdapter* pIDXGIAdapter = NULL;



//main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX aav_wndclass;
	HWND aav_hwnd;
	MSG aav_msg;
	TCHAR aav_szClassName[] = TEXT("Aniruddha");
	bool aav_bDone = false;
	INT aav_iy, aav_ix;
	HRESULT aav_hr;


	//code
	if (fopen_s(&aav_gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fclose(aav_gpFile);
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "Log File Is Successfully Opened.\n");
		fclose(aav_gpFile);

	}

	aav_wndclass.cbSize = sizeof(WNDCLASSEX);
	aav_wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	aav_wndclass.cbClsExtra = 0;
	aav_wndclass.cbWndExtra = 0;
	aav_wndclass.hInstance = hInstance;
	aav_wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	aav_wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	aav_wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	aav_wndclass.lpfnWndProc = WndProc;
	aav_wndclass.lpszClassName = aav_szClassName;
	aav_wndclass.lpszMenuName = NULL;
	aav_wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&aav_wndclass);

	aav_iy = GetSystemMetrics(SM_CYSCREEN);
	aav_ix = GetSystemMetrics(SM_CXSCREEN);

	aav_ix = (aav_ix / 2) - (WIN_WIDTH / 2);
	aav_iy = (aav_iy / 2) - (WIN_HEIGHT / 2);

	aav_hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		aav_szClassName,
		TEXT("Aniruddha Avinash Vanjari : Dirext3D : BlueScreen"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		aav_ix,
		aav_iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	aav_ghwnd = aav_hwnd;

	ShowWindow(aav_hwnd, iCmdShow);
	SetForegroundWindow(aav_hwnd);
	SetFocus(aav_hwnd);

	aav_hr = initialize();
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "initialize() failed .Exitting Now...\n");
		fclose(aav_gpFile);

		uninitialize();

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "initialize() Success...\n");
		fclose(aav_gpFile);
	}

	while (aav_bDone == false)
	{
		if (PeekMessage(&aav_msg, NULL, 0, 0, PM_REMOVE))
		{
			if (aav_msg.message == WM_QUIT)
				aav_bDone = true;
			else
			{
				TranslateMessage(&aav_msg);
				DispatchMessage(&aav_msg);
			}
		}
		else
		{
			display();
			if (aav_gbActiveWindow == true)
			{
				if (aav_gbEscapeKeyIsPressed == true)
				{
					aav_bDone = true;
				}
			}
		}

	}
	uninitialize();

	return((int)aav_msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	HRESULT resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//variable declaration
	HRESULT aav_hr;
	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			aav_gbActiveWindow = true;
		else
			aav_gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		if (aav_gpID3D11DeviceContext)
		{
			aav_hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(aav_hr))
			{
				fopen_s(&aav_gpFile, "log.txt", "a+");
				fprintf_s(aav_gpFile, "initialize() failed .Exitting Now...\n");
				fclose(aav_gpFile);

				uninitialize();

			}
			else
			{
				fopen_s(&aav_gpFile, "log.txt", "a+");
				fprintf_s(aav_gpFile, "resize() Success...\n");
				fclose(aav_gpFile);
			}
		}

		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			aav_gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (aav_gbFullscreen == false)
			{
				ToggleFullscreen();
				aav_gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				aav_gbFullscreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO aav_mi;

	//code
	if (aav_gbFullscreen == false)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		if (aav_dwStyle & WS_OVERLAPPEDWINDOW)
		{
			aav_mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(aav_ghwnd, &aav_wpPrev) && GetMonitorInfo(MonitorFromWindow(aav_ghwnd, MONITORINFOF_PRIMARY), &aav_mi))
			{
				SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(aav_ghwnd, HWND_TOP, aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.top, aav_mi.rcMonitor.right - aav_mi.rcMonitor.left,
					aav_mi.rcMonitor.bottom - aav_mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	//fucntion protype 
	HRESULT resize(int, int);
	HRESULT gpuDeteail(void);
	void uninitialize(void);

	//printf3DInfo(); 
	//variable declaration
	HRESULT aav_hr;
	DXGI_SWAP_CHAIN_DESC aav_dxgiSwapChainDesc;

	D3D_FEATURE_LEVEL aav_d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL aav_d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; // default low dumy

	D3D_DRIVER_TYPE aav_d3dDriverType;
	D3D_DRIVER_TYPE aav_d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};

	UINT aav_createDeviceFlags = 0;
	UINT aav_numDriverTypes = 0;
	UINT aav_numFeatureLevels = 1; // base upon d3dFeatureLevel_required


	//code

	//GPU details

	aav_hr = gpuDeteail();
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "gpuDeteail() failed.\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "gpuDeteail() Success.\n");
		fclose(aav_gpFile);
	}

	aav_numDriverTypes = sizeof(aav_d3dDriverTypes) / sizeof(aav_d3dDriverTypes[0]); //calculating size of array

	ZeroMemory((void*)&aav_dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	aav_dxgiSwapChainDesc.BufferCount = 1;

	aav_dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	aav_dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	aav_dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	aav_dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; //Refresh rate
	aav_dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

	aav_dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; //enum
	aav_dxgiSwapChainDesc.OutputWindow = aav_ghwnd;
	aav_dxgiSwapChainDesc.SampleDesc.Count = 1;
	aav_dxgiSwapChainDesc.SampleDesc.Quality = 0;
	aav_dxgiSwapChainDesc.Windowed = TRUE; // maze default window dakhav

	for (UINT driverTypeIndex = 0; driverTypeIndex < aav_numDriverTypes; driverTypeIndex++)
	{
		aav_d3dDriverType = aav_d3dDriverTypes[driverTypeIndex];
		aav_hr = D3D11CreateDeviceAndSwapChain(
			NULL,							// 1.Adapter
			aav_d3dDriverType,				// 2.Driver type
			NULL,							// 3.Software
			aav_createDeviceFlags,			// 4.Flags
			&aav_d3dFeatureLevel_required,	// 5.Feature Levels
			aav_numFeatureLevels,				// 6.Num Feature Levels
			D3D11_SDK_VERSION,				// 7.SDK Version
			&aav_dxgiSwapChainDesc,			// 8.Swap Chain Desc
			&aav_gpIDXGISwapChain,			// 9.Swap Chain
			&aav_gpID3D11Device,			// 10.Device
			&aav_d3dFeatureLevel_acquired,	// 11.Feature Level
			&aav_gpID3D11DeviceContext		// 12.Device Context
		);

		if (SUCCEEDED(aav_hr))
			break;
	}
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "D3D11CreateDeviceAndSwapChain() failed.\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");
		fprintf_s(aav_gpFile, "The Chosen Driver Is of ");

		if (aav_d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(aav_gpFile, "Hardware Type.\n");
		}
		else if (aav_d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(aav_gpFile, "Warp Type.\n");
		}
		else if (aav_d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(aav_gpFile, "Reference Type.\n");
		}
		else
		{
			fprintf_s(aav_gpFile, "Unknown Type.\n");
		}

		fprintf_s(aav_gpFile, "The Supported Hightest Feature Level Is");
		if (aav_d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(aav_gpFile, "11.0\n");
		}
		else if (aav_d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(aav_gpFile, "10.0.\n");
		}
		else
		{
			fprintf_s(aav_gpFile, "Unknown.\n");
		}

		fclose(aav_gpFile);
	}

	//d3d clear color (blue)
	aav_gClearColor[0] = 0.0f;
	aav_gClearColor[1] = 0.0f;
	aav_gClearColor[2] = 1.0f;
	aav_gClearColor[3] = 1.0f;

	//call resize for first time
	aav_hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "resize() failed .Exitting Now...\n");
		fclose(aav_gpFile);

		uninitialize();

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "resize() Success...\n");
		fclose(aav_gpFile);
	}

	return(S_OK);
}

HRESULT gpuDeteail(void)
{
	//fucntion declaration
	void uninitialize(void);

	//variable declaraiotn
	DXGI_ADAPTER_DESC aav_dxgiAdapterDesc;

	HRESULT aav_hr;

	char aav_str[255];

	//code
	aav_hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);
	//
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "CreateDXGIFactory Failde\n");
		fclose(aav_gpFile);
		uninitialize();
	}

	if (pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{

		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "DXGIAdapter Can Not be Found\n");
		fclose(aav_gpFile);
		uninitialize();
	}

	ZeroMemory((void*)&aav_dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));

	aav_hr = pIDXGIAdapter->GetDesc(&aav_dxgiAdapterDesc);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "DXGIADAPTERDESC Failde\n");
		fclose(aav_gpFile);
		uninitialize();
	}

	WideCharToMultiByte(CP_ACP, 0, aav_dxgiAdapterDesc.Description, 255, aav_str, 255, NULL, NULL);


	fopen_s(&aav_gpFile, "log.txt", "a+");
	fprintf(aav_gpFile, "Graphic Card Name = %s.\n", aav_str);

	fprintf(aav_gpFile, "Graphic Memory VRAM = %I64d bytes.\n",
		(__int64)aav_dxgiAdapterDesc.DedicatedVideoMemory);

	fprintf(aav_gpFile, "VRAM in GB = %d GB\n",
		int(ceil(aav_dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0)));
	fclose(aav_gpFile);

	return(aav_hr);
}

void display(void)
{
	//code
	//clear render target view to a chose color
	aav_gpID3D11DeviceContext->ClearRenderTargetView(aav_gpID3D11RenderTargetView, aav_gClearColor);

	//switch between front and back buffers
	aav_gpIDXGISwapChain->Present(0, 0);
}

HRESULT resize(int aav_width, int aav_height)
{
	//fucntion declaration
	void uninitialize(void);

	//varaible 
	HRESULT aav_hr = S_OK;
	ID3D11Texture2D* aav_pID3D11Texture2D_BackBuffer;
	D3D11_VIEWPORT aav_d3dViewPort;

	//code

	//free any size-dependant resources
	if (aav_gpID3D11RenderTargetView)
	{
		aav_gpID3D11RenderTargetView->Release();
		aav_gpID3D11RenderTargetView = NULL;
	}

	//resize swap chain buffer accordingly
	aav_gpIDXGISwapChain->ResizeBuffers(1, aav_width, aav_height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	//agin get back buffer from swap chain
	aav_gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&aav_pID3D11Texture2D_BackBuffer);

	//again get render target view from d3d11 device using device above back buffer
	aav_hr = aav_gpID3D11Device->CreateRenderTargetView(aav_pID3D11Texture2D_BackBuffer, NULL, &aav_gpID3D11RenderTargetView);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "ID3D11Device::CreateRenderTargetView() failed .Exitting Now...\n");
		fclose(aav_gpFile);

		uninitialize();

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "ID3D11Device::CreateRenderTargetView() Success...\n");
		fclose(aav_gpFile);
	}

	aav_pID3D11Texture2D_BackBuffer->Release();
	aav_pID3D11Texture2D_BackBuffer = NULL;

	//sen render to target view as render target
	aav_gpID3D11DeviceContext->OMSetRenderTargets(1, &aav_gpID3D11RenderTargetView, NULL);

	//Set View Port 
	ZeroMemory((void*)&aav_d3dViewPort, sizeof(D3D11_VIEWPORT));
	aav_d3dViewPort.TopLeftX = 0;
	aav_d3dViewPort.TopLeftY = 0;
	aav_d3dViewPort.Width = (float)aav_width;
	aav_d3dViewPort.Height = (float)aav_height;
	aav_d3dViewPort.MinDepth = 0.0f;
	aav_d3dViewPort.MaxDepth = 1.0f; //clear depth
	aav_gpID3D11DeviceContext->RSSetViewports(1, &aav_d3dViewPort);

	return(aav_hr);
}

void uninitialize(void)
{
	//code
	if (aav_gbFullscreen == true)
	{
		aav_dwStyle = GetWindowLong(aav_ghwnd, GWL_STYLE);
		SetWindowLong(aav_ghwnd, GWL_STYLE, aav_dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(aav_ghwnd, &aav_wpPrev);
		SetWindowPos(aav_ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}


	if (aav_gpID3D11RenderTargetView)
	{
		aav_gpID3D11RenderTargetView->Release();
		aav_gpID3D11RenderTargetView = NULL;
	}

	if (aav_gpIDXGISwapChain)
	{
		aav_gpIDXGISwapChain->Release();
		aav_gpIDXGISwapChain = NULL;
	}

	if (aav_gpID3D11DeviceContext)
	{
		aav_gpID3D11DeviceContext->Release();
		aav_gpID3D11DeviceContext = NULL;
	}

	if (aav_gpID3D11Device)
	{
		aav_gpID3D11Device->Release();
		aav_gpID3D11Device = NULL;
	}

	if (pIDXGIAdapter)
	{
		pIDXGIAdapter->Release();
		pIDXGIAdapter = NULL;
	}

	if (pIDXGIFactory)
	{
		pIDXGIFactory->Release();
		pIDXGIFactory = NULL;
	}

	if (aav_gpFile)
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "uninitialize() Succeeded\n");
		fprintf_s(aav_gpFile, "Log File Is Successfully Closed.\n");
		fclose(aav_gpFile);
	}
}