#include <windows.h>

#include <d3dcompiler.h>  //1

#include <stdio.h>
#include <math.h>

#include <d3d11.h>
#include "d3d11_blueScreen.h"

#pragma warning(disable: 4838)


#include "XNAMath\xnamath.h" //2

#include "WICTextureLoader.h"

#pragma comment(lib,"d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "D3dcompiler.lib")
#pragma comment(lib,"DirectXTK.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable 
FILE* aav_gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

ID3D11DeviceContext* gpID3D11DeviceContext = NULL;
ID3D11Device* gpID3D11Device = NULL;
ID3D11RenderTargetView* gpID3D11RenderTargetView = NULL;
IDXGISwapChain* gpIDXGISwapChain = NULL;

ID3D11ShaderResourceView *gpiD3D11ShaderResourceView_Texture = NULL;
ID3D11ShaderResourceView *gpiD3D11ShaderResourceView_StoneTexture = NULL;
ID3D11SamplerState *gpiD311SamplerState_Texture = NULL;
float gClearColor[4]; //RGB

IDXGIFactory* pIDXGIFactory = NULL; 

IDXGIAdapter* pIDXGIAdapter = NULL; 

//////////////////////////////////////////////
//1
ID3D11VertexShader	*gpID3D11VertexShader = NULL;
ID3D11PixelShader* gpID3D11PixelShader = NULL;
ID3D11Buffer* gpID3D11Buffer_VertexBuffer_Position = NULL;
ID3D11Buffer* gpID3D11Buffer_VertexBuffer_Texcoord = NULL;
ID3D11InputLayout* gpID3D11InputLayout = NULL;
ID3D11Buffer* gpID3D11Buffer_ConstatntBuffer = NULL;

ID3D11RasterizerState* gpId3d11RasterizerState = NULL; //

ID3D11DepthStencilView* gpID3D11DepthStencilView = NULL; // Depth 

struct CBUFFER
{
	XMMATRIX WorldViewProjectMatrix;			//Model in OpenGL
};

XMMATRIX gPrespectiveProjectionMatrix; //6

float angleCube = 0.0f;

D3D11_MAPPED_SUBRESOURCE d3d11MappedSubresource;


float aav_cubeTexcoord[8] = {
	0.0f,1.0f,
	1.0f,1.0f,
	0.0f,0.0f,
	1.0f,0.0f
};

int aav_textureKeyPress;
int aav_keySmilePress;

//Entry Point
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Fuction prototype
	HRESULT initialize(void);
	void uninitialize(void);
	void display(void);

	//variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("Aniruddha");
	bool bDone = false;
	INT iy, ix;
	HRESULT aav_hr;

	//code
	if (fopen_s(&aav_gpFile, "log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\n Exitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fclose(aav_gpFile);
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "Log File Is Successfully Opened.\n");
		fclose(aav_gpFile);
		
	}

	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	RegisterClassEx(&wndclass);

	iy = GetSystemMetrics(SM_CYSCREEN);
	ix = GetSystemMetrics(SM_CXSCREEN);

	ix = (ix / 2) - (WIN_WIDTH / 2);
	iy = (iy / 2) - (WIN_HEIGHT / 2);

	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("Aniruddha Avinash Vanjari : Dirext3D : Smiley"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		ix,
		iy,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	aav_hr = initialize();
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "initialize() failed .Exitting Now...\n");
		fclose(aav_gpFile);

		uninitialize();
		
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "initialize() Success...\n");
		fclose(aav_gpFile);
	}

	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			display();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
				{
					bDone = true;
				}
			}
		}

	}
	uninitialize();

	return((int)msg.wParam);
}

//WndProc
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//fucntion prototype
	HRESULT resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//variable declaration
	HRESULT aav_hr;
	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		if (gpID3D11DeviceContext)
		{
			aav_hr = resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(aav_hr))
			{
				fopen_s(&aav_gpFile, "log.txt", "a+");
				fprintf_s(aav_gpFile, "initialize() failed .Exitting Now...\n");
				fclose(aav_gpFile);

				uninitialize();

			}
			else
			{
				fopen_s(&aav_gpFile, "log.txt", "a+");
				fprintf_s(aav_gpFile, "resize() Success...\n");
				fclose(aav_gpFile);
			}
		}
		
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46:
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case VK_NUMPAD0:
			aav_keySmilePress = 0;
			break;
		case 49:
		case VK_NUMPAD1:
			aav_keySmilePress = 1;
			break;
		case 50:
		case VK_NUMPAD2:
			aav_keySmilePress = 2;
			break;
		case 51:
		case VK_NUMPAD3:
			aav_keySmilePress = 3;
			break;
		case 52:
		case VK_NUMPAD4:
			aav_keySmilePress = 4;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//varible declarations
	MONITORINFO mi;

	//code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left,
					mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}

		}
		ShowCursor(FALSE);
	}
	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

HRESULT initialize(void)
{
	//fucntion protype 
	HRESULT resize(int, int);
	HRESULT gpuDeteail(void);
	void uninitialize(void);
	HRESULT loadD3DTexture(const wchar_t*, ID3D11ShaderResourceView**);

	//printf3DInfo(); 
	//variable declaration
	HRESULT aav_hr;
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;
	
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0; 

	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};

	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1; // base upon d3dFeatureLevel_required


	//code

	//GPU details

	aav_hr = gpuDeteail();
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "gpuDeteail() failed.\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "gpuDeteail() Success.\n");
		fclose(aav_gpFile);
	}

	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]); //calculating size of array

	ZeroMemory((void*)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	
	dxgiSwapChainDesc.BufferCount = 1;

	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60; //Refresh rate
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT; //enum
	dxgiSwapChainDesc.OutputWindow = ghwnd;
	dxgiSwapChainDesc.SampleDesc.Count = 1;
	dxgiSwapChainDesc.SampleDesc.Quality = 0;
	dxgiSwapChainDesc.Windowed = TRUE; // maze default window dakhav

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];
		aav_hr = D3D11CreateDeviceAndSwapChain(
			NULL,							// 1.Adapter
			d3dDriverType,				// 2.Driver type
			NULL,							// 3.Software
			createDeviceFlags,			// 4.Flags
			&d3dFeatureLevel_required,	// 5.Feature Levels
			numFeatureLevels,				// 6.Num Feature Levels
			D3D11_SDK_VERSION,				// 7.SDK Version
			&dxgiSwapChainDesc,			// 8.Swap Chain Desc
			&gpIDXGISwapChain,			// 9.Swap Chain
			&gpID3D11Device,			// 10.Device
			&d3dFeatureLevel_acquired,	// 11.Feature Level
			&gpID3D11DeviceContext		// 12.Device Context
		);	
		
		if (SUCCEEDED(aav_hr))
			break;
	}
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "D3D11CreateDeviceAndSwapChain() failed.\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");
		fprintf_s(aav_gpFile, "The Chosen Driver Is of ");

		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(aav_gpFile, "Hardware Type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(aav_gpFile, "Warp Type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(aav_gpFile, "Reference Type.\n");
		}
		else
		{
			fprintf_s(aav_gpFile, "Unknown Type.\n");
		}

		fprintf_s(aav_gpFile, "The Supported Hightest Feature Level Is");
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(aav_gpFile, "11.0\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(aav_gpFile, "10.1.\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(aav_gpFile, "10.0.\n");
		}
		else
		{
			fprintf_s(aav_gpFile, "Unknown.\n");
		}

		fclose(aav_gpFile);
	}

	//shader start from here

	//declare the vertex shader source code
	//In DirextX shader variable nahe , inbuilt consstruct ahe 
	// glsl  : hlsl : his
	//SV_POSITION : shader variable 
	const char* vertexShaderSourceCode =
		"cbuffer ConstantBuffer"	\
		"{"	\
		"float4x4 worldViewProjectionMatrix;"	\
		"}"	\
		"struct vertex_output"\
		"{"\
		"	float4 position:SV_POSITION;"\
		"	float2 texcoord:TEXCOORD; "\
		"};"\
		"vertex_output main(float4 pos : POSITION,float2 tex:TEXCOORD)"	\
		"{"	\
		"	vertex_output output;"\
		"	output.position = mul(worldViewProjectionMatrix,pos);"	\
		"	output.texcoord = tex;"\
		"	return(output);"	\
		"}";

	//Blob is any data stroe in variable
	ID3DBlob *pID3DBlob_VertexShaderCode = NULL;
	ID3DBlob* pID3DBlob_Error = NULL;

	aav_hr = D3DCompile(
		vertexShaderSourceCode,					//1. Shader Code
		lstrlenA(vertexShaderSourceCode)+ 1,	//2. Shader Length
		"VS",									//3. Vertex Shader
		NULL,									//4. MACRO array for shader 
		D3D_COMPILE_STANDARD_FILE_INCLUDE,		//5. use shandard variable
		"main",									//6. Entry Point fucntion vertex shader
		"vs_5_0",								//7. My Vertex shader feature level
		0,										//8. Shader compile 
		0,										//9. compiler flag any for Effect
		&pID3DBlob_VertexShaderCode,			//10. No Error this variable is Fulled 
		&pID3DBlob_Error						//11. if Error is there then it is Fulled 
		);

	if (FAILED(aav_hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "%s", (char*)pID3DBlob_Error->GetBufferPointer());
			fclose(aav_gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(aav_hr);
			//set are device context varti call karavi lagtat
		}
		else
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "D3DCompile() Succeeded For Vertex Shader .\n");
			fclose(aav_gpFile);
		}
	}

	aav_hr = gpID3D11Device->CreateVertexShader(pID3DBlob_VertexShaderCode->GetBufferPointer(), // gpu understandable code de
		pID3DBlob_VertexShaderCode->GetBufferSize(),
		NULL,	//ID3D11instance*(pointer) : ID3Dd11ClassLinkage*(pointer)
		&gpID3D11VertexShader); // vertex shader kashat barun dau

	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "gpID3D11Device::CreateVertexShader Failed");
		fclose(aav_gpFile);
		return(aav_hr);
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "gpID3D11Device::CreateVertexShader Succes\n");
		fclose(aav_gpFile);
	}

	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader, NULL, 0);

	//Pixel Shader
	const char* pixelShaderSourceCode =
		"struct vertex_output"\
		"{"\
		"	float4 position:SV_POSITION;"\
		"	float2 texcoord:TEXCOORD; "\
		"};"\
		"Texture2D myTexture2D;"\
		"SamplerState mySamplerState;"\
		"float4 main(vertex_output input) : SV_TARGET"\
		"{"	\
		"	float4 color = myTexture2D.Sample(mySamplerState,input.texcoord);"\
		"	return(color);"	\
		"}";

	ID3DBlob *pID3DBlob_PixelShaderCode = NULL;
	pID3DBlob_Error = NULL;
	aav_hr = D3DCompile(
		pixelShaderSourceCode,
		lstrlenA(pixelShaderSourceCode)+1,
			"PS",
			NULL,
			D3D_COMPILE_STANDARD_FILE_INCLUDE,
			"main",
			"ps_5_0",
			0,
			0,
			&pID3DBlob_PixelShaderCode,
			&pID3DBlob_Error
		);

	if (FAILED(aav_hr))
	{
		if (pID3DBlob_Error != NULL)
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "%s", (char*)pID3DBlob_Error->GetBufferPointer());
			fclose(aav_gpFile);
			pID3DBlob_Error->Release();
			pID3DBlob_Error = NULL;
			return(aav_hr);
		}
		else
		{
			fopen_s(&aav_gpFile, "log.txt", "a+");
			fprintf(aav_gpFile, "COM Error.\n");
			fclose(aav_gpFile);
			return(aav_hr);
		}
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "D3DCompile() Succeeded For Pixel Shader .\n");
		fclose(aav_gpFile);
	}

	aav_hr = gpID3D11Device->CreatePixelShader(pID3DBlob_PixelShaderCode->GetBufferPointer(),
		pID3DBlob_PixelShaderCode->GetBufferSize(), NULL, &gpID3D11PixelShader);

	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreatePixelShader() Failed");
		fclose(aav_gpFile);
		return(aav_hr);

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "ID3D11Device::CreatePixelShader() Succeeded");
		fclose(aav_gpFile);
	}

	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader, NULL, 0);
	if (pID3DBlob_PixelShaderCode)
	{
		pID3DBlob_PixelShaderCode->Release();
		pID3DBlob_PixelShaderCode = NULL;
	}

	if (pID3DBlob_Error)
	{
		pID3DBlob_Error->Release();
		pID3DBlob_Error = NULL;
	}

	//9
	//(a)initialize and ()create and set input lay out

	//(a) initailize intput lay out structure
	D3D11_INPUT_ELEMENT_DESC d3d11inputElementDesc[2];
	
	//ZeroMemory((void*)&d3d11inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC));

	d3d11inputElementDesc[0].SemanticName			= "POSITION";					//vPosition glBindAttribute Location
	d3d11inputElementDesc[0].SemanticIndex			= 0;							// if we are going to sent multiple geometry in same semantics they are seperate by indices
	d3d11inputElementDesc[0].Format				= DXGI_FORMAT_R32G32B32_FLOAT;	//glVertexAttribPointer 3, GL_FLOAT forth parameter
	d3d11inputElementDesc[0].AlignedByteOffset		= 0;
	d3d11inputElementDesc[0].InputSlot				= 0;							// alpla enum ekadcha input slot
	d3d11inputElementDesc[0].InputSlotClass		= D3D11_INPUT_PER_VERTEX_DATA;	// Me jo pathavto ahe to vertex cha data ahe. 
	d3d11inputElementDesc[0].InstanceDataStepRate	= 0;							
	
	d3d11inputElementDesc[1].SemanticName	= "TEXCOORD";					//vPosition glBindAttribute Location
	d3d11inputElementDesc[1].SemanticIndex	= 0;							// if we are going to sent multiple geometry in same semantics they are seperate by indices
	d3d11inputElementDesc[1].Format			= DXGI_FORMAT_R32G32_FLOAT;	//glVertexAttribPointer 3, GL_FLOAT forth parameter
	d3d11inputElementDesc[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT; //PixelStorei unpaked allignment.
	d3d11inputElementDesc[1].InputSlot = 1;							// alpla enum ekadcha input slot
	d3d11inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;	// Me jo pathavto ahe to vertex cha data ahe. 
	d3d11inputElementDesc[1].InstanceDataStepRate = 0;

	//B create the input
	aav_hr = gpID3D11Device->CreateInputLayout(d3d11inputElementDesc, _ARRAYSIZE(d3d11inputElementDesc),
								pID3DBlob_VertexShaderCode->GetBufferPointer(), 
								pID3DBlob_VertexShaderCode->GetBufferSize(),
								&gpID3D11InputLayout);

	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "CreateInputLayout Failed");
		fclose(aav_gpFile);
		return(aav_hr);
		if (pID3DBlob_VertexShaderCode)
		{
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
		}
	}
	else
	{
		if (pID3DBlob_VertexShaderCode)
		{
			pID3DBlob_VertexShaderCode->Release();
			pID3DBlob_VertexShaderCode = NULL;
		}
	}


	//Set the Created input in pipe Line.
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout); 

	//declate vertices , color , texture , normals
	//Dirext3D is Clock Wise
	float aav_cubeVertices[] = 
	{
	-1.0f,1.0f,0.0f,
	1.0f,1.0f,0.0f,
	-1.0f,-1.0f,0.0f,
	1.0f,-1.0f,0.0f
				
	};

	//direct fabers dymanic draw
	//11
	//create Vertex buffer
	//initialize buffer disctiption structure  and Create the buffer
	//12
	//push the data in this buffer by MaP(memcpy(&UnmapMethod))

	//11
	D3D11_BUFFER_DESC d3d11BufferDesk;

	ZeroMemory((void*)&d3d11BufferDesk, sizeof(D3D11_BUFFER_DESC));
	d3d11BufferDesk.ByteWidth		= sizeof(float) * _ARRAYSIZE(aav_cubeVertices); //glBufferData cha ha 2nd parameter
	d3d11BufferDesk.BindFlags		= D3D11_BIND_VERTEX_BUFFER;			// bind Attribute loacation la support , use this as vertex buffer
	d3d11BufferDesk.CPUAccessFlags	= D3D11_CPU_ACCESS_WRITE;
	d3d11BufferDesk.Usage			= D3D11_USAGE_DYNAMIC;				

	aav_hr = gpID3D11Device->CreateBuffer(&d3d11BufferDesk, NULL,
		&gpID3D11Buffer_VertexBuffer_Position);

	//color 
	ZeroMemory((void*)&d3d11BufferDesk, sizeof(D3D11_BUFFER_DESC));
	d3d11BufferDesk.ByteWidth = sizeof(float) * _ARRAYSIZE(aav_cubeTexcoord);//glBufferData cha ha 2nd parameter
	d3d11BufferDesk.BindFlags = D3D11_BIND_VERTEX_BUFFER;	// bind Attribute loacation la support , use this as vertex buffer
	d3d11BufferDesk.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	d3d11BufferDesk.Usage = D3D11_USAGE_DYNAMIC;
	

	aav_hr = gpID3D11Device->CreateBuffer(&d3d11BufferDesk,NULL,
		&gpID3D11Buffer_VertexBuffer_Texcoord);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "CreateBuffer() failed .Exitting Now... Texture Buffer\n");
		fclose(aav_gpFile);

		uninitialize();

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "CreateBuffer() Success... Texture Buffer\n");
		fclose(aav_gpFile);
	}


	///////////////////////////////////////////////////////////////////////////////

	ZeroMemory(&d3d11MappedSubresource,sizeof(D3D11_MAPPED_SUBRESOURCE));
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Position,
		NULL,
		D3D11_MAP_WRITE_DISCARD,
		NULL,
		&d3d11MappedSubresource);

	memcpy(d3d11MappedSubresource.pData,aav_cubeVertices,sizeof(aav_cubeVertices));

	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Position,NULL);


	ZeroMemory(&d3d11BufferDesk,sizeof(D3D11_BUFFER_DESC));
	d3d11BufferDesk.Usage = D3D11_USAGE_DEFAULT;
	d3d11BufferDesk.ByteWidth = sizeof(CBUFFER);
	d3d11BufferDesk.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	d3d11BufferDesk.CPUAccessFlags = 0;
	aav_hr = gpID3D11Device->CreateBuffer(&d3d11BufferDesk,NULL,&gpID3D11Buffer_ConstatntBuffer);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "CreateBuffer() failed .Exitting Now... Texture Buffer\n");
		fclose(aav_gpFile);

		uninitialize();

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "CreateBuffer() Success... Texture Buffer\n");
		fclose(aav_gpFile);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(0,
		1,
		&gpID3D11Buffer_ConstatntBuffer);

	aav_hr = loadD3DTexture(L"Smiley.bmp", &gpiD3D11ShaderResourceView_Texture);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "loadD3DTexture() failed .Exitting Now...\n");
		fclose(aav_gpFile);
		uninitialize();
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "loadD3DTexture() Success...\n");
		fclose(aav_gpFile);
	}


	D3D11_SAMPLER_DESC d3d11SamplerDesk;
	ZeroMemory((void*)& d3d11SamplerDesk, sizeof(D3D11_SAMPLER_DESC));


	d3d11SamplerDesk.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	d3d11SamplerDesk.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	d3d11SamplerDesk.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	d3d11SamplerDesk.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;

	aav_hr = gpID3D11Device->CreateSamplerState(&d3d11SamplerDesk,&gpiD311SamplerState_Texture);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "CreateSamplerState() failed .Exitting Now...\n");
		fclose(aav_gpFile);
		uninitialize();

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "CreateSamplerState() Success...\n");
		fclose(aav_gpFile);
	}

	//d3d clear color (blue)
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0f;
	gClearColor[3] = 1.0f;

	//set projection matrix to identity matrix
	gPrespectiveProjectionMatrix = XMMatrixIdentity();

	//call resize for first time
	//Depth is Size dependent
	aav_hr = resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "resize() failed .Exitting Now...\n");
		fclose(aav_gpFile);

		uninitialize();

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "resize() Success...\n");
		fclose(aav_gpFile);
	}

	return(S_OK);
}

HRESULT loadD3DTexture(const wchar_t* textureFileName, ID3D11ShaderResourceView** ppID3D11ShaderResourceView)
{
	//code
	HRESULT aav_hr;

	aav_hr =  DirectX::CreateWICTextureFromFile(gpID3D11Device,
		gpID3D11DeviceContext,textureFileName,NULL,ppID3D11ShaderResourceView);

	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "loadD3DTexture() failed .Exitting Now...\n");
		fclose(aav_gpFile);
	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "loadD3DTexture() Success...\n");
		fclose(aav_gpFile);
	}

	return(aav_hr);
}

HRESULT gpuDeteail(void)
{
	//fucntion declaration
	void uninitialize(void);

	//variable declaraiotn
	DXGI_ADAPTER_DESC dxgiAdapterDesc;

	HRESULT aav_hr;

	char str[255];

	//code
	aav_hr = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pIDXGIFactory);
	//
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "CreateDXGIFactory Failde\n");
		fclose(aav_gpFile);
		uninitialize();
	}

	if (pIDXGIFactory->EnumAdapters(0, &pIDXGIAdapter) == DXGI_ERROR_NOT_FOUND)
	{
		
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "DXGIAdapter Can Not be Found\n");
		fclose(aav_gpFile);
		uninitialize();
	}

	ZeroMemory((void*)&dxgiAdapterDesc, sizeof(DXGI_ADAPTER_DESC));

	aav_hr = pIDXGIAdapter->GetDesc(&dxgiAdapterDesc);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf(aav_gpFile, "DXGIADAPTERDESC Failde\n");
		fclose(aav_gpFile);
		uninitialize();
	}

	WideCharToMultiByte(CP_ACP, 0, dxgiAdapterDesc.Description, 255, str, 255, NULL, NULL);
	

	fopen_s(&aav_gpFile, "log.txt", "a+");
	fprintf(aav_gpFile, "Graphic Card Name = %s.\n", str);

	fprintf(aav_gpFile, "Graphic Memory VRAM = %I64d bytes.\n", 
						(__int64)dxgiAdapterDesc.DedicatedVideoMemory);

	fprintf(aav_gpFile, "VRAM in GB = %d GB\n", 
						int(ceil(dxgiAdapterDesc.DedicatedVideoMemory / 1024.0 / 1024.0 / 1024.0)));
	fclose(aav_gpFile);

	return(aav_hr);
}

void display(void)
{
	//Steps 
	//14 :Set vertex buffer to pipeline 
	//15 :Set premitive 
	//16 : Do tranformation as needed
	//17 : push transformation in shader
	//18 : Draw
	XMMATRIX rotationX = XMMatrixIdentity();
	XMMATRIX rotationY = XMMatrixIdentity();
	XMMATRIX rotationZ = XMMatrixIdentity();
	XMMATRIX rotation = XMMatrixIdentity();
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX translation = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();
	XMMATRIX wvpMatrix = XMMatrixIdentity();

	//vatiable Initialization.
	//Set  Vertex buffer to pipline 
	UINT aav_stride = sizeof(float) * 3;
	UINT aav_offset = 0;

	// code
	//clear render target view to a chose color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);

	//colors

	gpID3D11DeviceContext->IASetVertexBuffers(0, 1, &gpID3D11Buffer_VertexBuffer_Position, &aav_stride, &aav_offset);

	ZeroMemory((void*)&d3d11MappedSubresource, sizeof(D3D11_MAPPED_SUBRESOURCE));

	
	//	aav_cubeTexcoord[0] = 0.0f;
	//	aav_cubeTexcoord[1] = 2.0f;
	//	aav_cubeTexcoord[2] = 2.0f;
	//	aav_cubeTexcoord[3] = 2.0f;
	//	aav_cubeTexcoord[4] = 0.0f;
	//	aav_cubeTexcoord[5] = 0.0f;
	//	aav_cubeTexcoord[6] = 2.0f;
	//	aav_cubeTexcoord[7] = 0.0f;
	
	

	if (aav_keySmilePress == 1)
	{
		aav_cubeTexcoord[0] = 0.0f;
		aav_cubeTexcoord[1] = 1.0f;
		aav_cubeTexcoord[2] = 1.0f;
		aav_cubeTexcoord[3] = 1.0f;
		aav_cubeTexcoord[4] = 0.0f;
		aav_cubeTexcoord[5] = 0.0f;
		aav_cubeTexcoord[6] = 1.0f;
		aav_cubeTexcoord[7] = 0.0f;
	}
	else if (aav_keySmilePress == 2)
	{
		aav_cubeTexcoord[0] = 0.0f;
		aav_cubeTexcoord[1] = 0.5f;
		aav_cubeTexcoord[2] = 0.5f;
		aav_cubeTexcoord[3] = 0.5f;
		aav_cubeTexcoord[4] = 0.0f;
		aav_cubeTexcoord[5] = 0.0f;
		aav_cubeTexcoord[6] = 0.5f;
		aav_cubeTexcoord[7] = 0.0f;
	}
	else if (aav_keySmilePress == 3)
	{
		aav_cubeTexcoord[0] = 0.0f;
		aav_cubeTexcoord[1] = 2.0f;
		aav_cubeTexcoord[2] = 2.0f;
		aav_cubeTexcoord[3] = 2.0f;
		aav_cubeTexcoord[4] = 0.0f;
		aav_cubeTexcoord[5] = 0.0f;
		aav_cubeTexcoord[6] = 2.0f;
		aav_cubeTexcoord[7] = 0.0f;
	}
	else if (aav_keySmilePress == 4)
	{
		aav_cubeTexcoord[0] = 0.5f;
		aav_cubeTexcoord[1] = 0.5f;
		aav_cubeTexcoord[2] = 0.5f;
		aav_cubeTexcoord[3] = 0.5f;
		aav_cubeTexcoord[4] = 0.5f;
		aav_cubeTexcoord[5] = 0.5f;
		aav_cubeTexcoord[6] = 0.5f;
		aav_cubeTexcoord[7] = 0.5f;
	}
	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Texcoord, 0,
		D3D11_MAP_WRITE_DISCARD, 0, &d3d11MappedSubresource);
	memcpy(d3d11MappedSubresource.pData, aav_cubeTexcoord, sizeof(aav_cubeTexcoord));
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Texcoord, 0);



	aav_stride = sizeof(float) * 2;
	aav_offset = 0;
	gpID3D11DeviceContext->IASetVertexBuffers(1, 1, &gpID3D11Buffer_VertexBuffer_Texcoord, &aav_stride, &aav_offset);
	//Set primitive , glDraw cha pahela parameter 

	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	
	translation = XMMatrixTranslation(0.0f, 0.0f, 3.0f);
	 
	worldMatrix = translation;
	
	wvpMatrix = worldMatrix * viewMatrix * gPrespectiveProjectionMatrix;

	

	/*gpID3D11DeviceContext->PSSetShaderResources(0,1,&gpiD3D11ShaderResourceView_StoneTexture);
	gpID3D11DeviceContext->PSGetSamplers(0, 2, &gpiD311SamplerState_Texture);*/
	CBUFFER constantBuffer;
	ZeroMemory((void*)&constantBuffer, sizeof(constantBuffer));
	constantBuffer.WorldViewProjectMatrix = wvpMatrix;

	gpID3D11DeviceContext->UpdateSubresource(
												gpID3D11Buffer_ConstatntBuffer, 
												0,
												NULL,
												&constantBuffer,				//
												0,								//5.Bounding Box , Row Pitch
												0								//6.Bounding Box , Depth Pitch
											);
	
	//Texture Code
	gpID3D11DeviceContext->PSSetShaderResources(0, 1, &gpiD3D11ShaderResourceView_Texture);
	gpID3D11DeviceContext->PSSetSamplers(0, 1, &gpiD311SamplerState_Texture);

	gpID3D11DeviceContext->Draw(4,0);

	//switch between front and back buffers
	
	gpIDXGISwapChain->Present(0, 0);
}

HRESULT resize(int width, int height)
{
	//fucntion declaration
	void uninitialize(void);

	//varaible 
	HRESULT aav_hr = S_OK;
	ID3D11Texture2D* pID3D11Texture2D_BackBuffer = NULL;
	D3D11_VIEWPORT d3dViewPort;


	//free any size-dependant resources
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	//resize swap chain buffer accordingly
	gpIDXGISwapChain->ResizeBuffers(1, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	//agin get back buffer from swap chain
	gpIDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pID3D11Texture2D_BackBuffer);

	//again get render target view from d3d11 device using device above back buffer
	aav_hr = gpID3D11Device->CreateRenderTargetView(pID3D11Texture2D_BackBuffer, NULL, &gpID3D11RenderTargetView);
	if (FAILED(aav_hr))
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "ID3D11Device::CreateRenderTargetView() failed .Exitting Now...\n");
		fclose(aav_gpFile);

		uninitialize();

	}
	else
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "ID3D11Device::CreateRenderTargetView() Success...\n");
		fclose(aav_gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();
	pID3D11Texture2D_BackBuffer = NULL;

	//sen render to target view as render target
	gpID3D11DeviceContext->OMSetRenderTargets(1, &gpID3D11RenderTargetView, NULL);

	//Set View Port 
	ZeroMemory((void*)&d3dViewPort, sizeof(D3D11_VIEWPORT));
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;
	d3dViewPort.MaxDepth = 1.0f; //clear depth
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);//raterization state

	//set othographic matrix
	gPrespectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45.0f),(float)width/(float)height,0.1f,100.0f);
	
	return(aav_hr);
}

void uninitialize(void)
{
	//code
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	if (gpiD311SamplerState_Texture)
	{
		gpiD311SamplerState_Texture->Release();
		gpiD311SamplerState_Texture = NULL;
	}

	if (gpId3d11RasterizerState)
	{
		gpId3d11RasterizerState->Release();
		gpId3d11RasterizerState = NULL;
	}

	if (gpID3D11Buffer_ConstatntBuffer)
	{
		gpID3D11Buffer_ConstatntBuffer->Release();
		gpID3D11Buffer_ConstatntBuffer = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Texcoord)
	{
		gpID3D11Buffer_VertexBuffer_Texcoord->Release();
		gpID3D11Buffer_VertexBuffer_Texcoord = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Position)
	{
		gpID3D11Buffer_VertexBuffer_Position->Release();
		gpID3D11Buffer_VertexBuffer_Position = NULL;
	}


	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (pIDXGIAdapter)
	{
		pIDXGIAdapter->Release();
		pIDXGIAdapter = NULL;
	}

	if (pIDXGIFactory)
	{
		pIDXGIFactory->Release();
		pIDXGIFactory = NULL;
	}

	if (aav_gpFile)
	{
		fopen_s(&aav_gpFile, "log.txt", "a+");
		fprintf_s(aav_gpFile, "uninitialize() Succeeded\n");
		fprintf_s(aav_gpFile, "Log File Is Successfully Closed.\n");
		fclose(aav_gpFile);
	}
}